#include "TROOT.h"
#include "TSystem.h"
#include "TProof.h"
#include "TChain.h"
#include "TString.h"
#include "TFileCollection.h"
#include "THashList.h"
#include <vector>
#include <iostream>

void run(TString option=""){
  
  TString CodeDirectory("/home/rafal/ATLAS_analysis/DataAnalysis/GitLabRepos/cep13tev_subanalyses/DataVsMC/");
  
  std::vector<TString> sourceFiles;
  sourceFiles.push_back(TString("DataVsMC.cxx"));
  
  for(unsigned int i=0; i<sourceFiles.size(); ++i)
    gROOT->ProcessLine(".L "+CodeDirectory+sourceFiles[i]+"+g");

  for(unsigned int i=0; i<sourceFiles.size(); ++i)
    gSystem->Load( (CodeDirectory+sourceFiles[i]).ReplaceAll(".", "_")+".so" );
  
  std::cout << "Starting DataVsMC..." << std::endl;
  gROOT->ProcessLine(".x run2.C+(\""+CodeDirectory+"\")");
}
