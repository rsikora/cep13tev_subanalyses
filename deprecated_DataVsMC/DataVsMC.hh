#ifndef DataVsMC_hh
#define DataVsMC_hh

#include <TROOT.h>
#include <TFile.h>
#include <TString.h>
#include <TLatex.h>
#include <vector>
#include "../../InclusiveDiffractionAnalysis/InclusiveDiffractionAnalysis/InclusiveDiffractionAnalysis/Cep/CepUtil.h"

class TH2;
class TH1;
class TH1F;
class TH2F;
class TAxis;
struct DrawingOptions;

class DataVsMC{
public:

  DataVsMC(TString opt="");
  ~DataVsMC();

  void analyze();
 
  static void setBlackAndWhiteColorScale();
  static void setWhiteAndBlackColorScale();
  static void setBlueWhiteRedColorScale();

  
private:

  enum FILES { DATA, MC_SIGNAL, MC_PYTHIA_CD_BKGD_NEUTRALS, MC_PYTHIA_CD_BKGD, nFiles };  
  static constexpr double kEps = 1e-6;

  void loadHistograms();
  void setupDrawingStyle() const;
  Double_t integratePtMiss(const TH1*, Double_t) const;
  std::vector<double> getBinsVectorD(const TAxis *) const;
  std::vector<float> getBinsVectorF(const TAxis *) const;
  void drawFinalResult(TH1F*, DrawingOptions &, TH1F* = nullptr);
  TH1F* rebinAndGetTH1F(const TH1*, const std::vector<double> &, Bool_t = kFALSE) const;
  TH1F* TH1F_from_TH1(const TH1*) const;
    
  TH1F* mhMissingPt[nFiles][CepUtil::kNumCuts][CepUtil::nDeltaPhiRanges];
  TH1F* mhMissingPt_SameSign[nFiles][CepUtil::kNumCuts][CepUtil::nDeltaPhiRanges];
  TH1F* mhDeltaZVx[nFiles][CepUtil::kNumCuts];
  TH1F* mhDeltaZVxVsDeltaPhiRange[nFiles][CepUtil::kNumCuts][CepUtil::nDeltaPhiRanges];
  TH1F* mhZVx[nFiles][CepUtil::kNumCuts];
  TH1F* mhNTofClusters[nFiles][CepUtil::kNumCuts];
  TH1F* mhCollinearityVsDeltaPhiRange[nFiles][CepUtil::kNumCuts][CepUtil::nDeltaPhiRanges];
  TH2F* mhHitPosition[nFiles][CepUtil::kNumCuts][CepUtil::nRomanPots];
  TH1F* mhDeltaPhiDegrees[nFiles][CepUtil::kNumCuts][CepUtil::nCharges2Trks][CepUtil::nDeltaPhiRanges];
  TH1F* mhDeltaPhiDegrees_DATA;
  TH2F* mhMissingPtVsDeltaPhiDegrees_DATA;
  
  TH1F *mhInvMassPid[nFiles][CepUtil::nCharges2Trks][CepUtil::nDefinedParticles];
  TH1F *mhMissingPtPid[nFiles][CepUtil::nCharges2Trks][CepUtil::nDefinedParticles];
  TH2F *mhMissingPtPidVsInvMass[nFiles][CepUtil::nCharges2Trks][CepUtil::nDefinedParticles];
  TH2F *mhMissingPtPidVsInvMass_ModifiedBinning[nFiles][CepUtil::nCharges2Trks][CepUtil::nDefinedParticles];
  TH1F* mhInvMassPion[nFiles][CepUtil::kNumCuts][CepUtil::nCharges2Trks][CepUtil::nDeltaPhiRanges];
  
  TH1F* mhMissingPtPidVsDeltaPhiRangeVsTrueLevelTopology[nFiles][CepUtil::nCharges2Trks][CepUtil::nDeltaPhiRanges][CepUtil::nCentralStateTopologies];
  
  //--------------------
  TH1F * hNSigmaMissingPt[nFiles][CepUtil::nCharges2Trks]; //!
  TH1F * hNSigmaMissingPtVsTrueLevelTopology[nFiles][CepUtil::nCharges2Trks][CepUtil::nCentralStateTopologies]; //!
  
  TH1F * hMissingPx[nFiles][CepUtil::nCharges2Trks]; //!
  TH1F * hMissingPy[nFiles][CepUtil::nCharges2Trks]; //!
  TH1F * hMissingPxVsTrueLevelTopology[nFiles][CepUtil::nCharges2Trks][CepUtil::nCentralStateTopologies]; //!
  TH1F * hMissingPyVsTrueLevelTopology[nFiles][CepUtil::nCharges2Trks][CepUtil::nCentralStateTopologies]; //!
  
  TH2F * hHitMapExclusivePiPi[nFiles][CepUtil::nRomanPots]; //!
  TH2F * hHitMapExclusivePiPiVsTrueLevelTopology[nFiles][CepUtil::nRomanPots][CepUtil::nCentralStateTopologies]; //!
  
  TH1F *mh_ExclusivePiPi_d0[nFiles]; //!
  TH1F *mh_ExclusivePiPi_d0_VsCentralStateTopology[nFiles][CepUtil::nCentralStateTopologies]; //!
  TH1F *mh_ExclusivePiPi_z0SinTheta[nFiles]; //!
  TH1F *mh_ExclusivePiPi_z0SinTheta_VsCentralStateTopology[nFiles][CepUtil::nCentralStateTopologies]; //!
  TH1F *mh_ExclusivePiPi_Delta_z0[nFiles]; //!
  TH1F *mh_ExclusivePiPi_Delta_z0_VsCentralStateTopology[nFiles][CepUtil::nCentralStateTopologies]; //!
  
  TH1F *mh_Exclusive4Pi_d0[nFiles]; //!
  TH1F *mh_Exclusive4Pi_d0_VsCentralStateTopology[nFiles][CepUtil::nCentralStateTopologies]; //!
  TH1F *mh_Exclusive4Pi_z0SinTheta[nFiles]; //!
  TH1F *mh_Exclusive4Pi_z0SinTheta_VsCentralStateTopology[nFiles][CepUtil::nCentralStateTopologies]; //!
  TH1F *mh_Exclusive4Pi_Delta_z0[nFiles]; //!
  TH1F *mh_Exclusive4Pi_Delta_z0_VsCentralStateTopology[nFiles][CepUtil::nCentralStateTopologies]; //!
  //--------------------
  
  TH1F *mh_YminusYExtr_BeforeMomentumBalanceCut[nFiles][CepUtil::nBranches]; //!
  TH1F *mh_YminusYExtr_BeforeMomentumBalanceCut_VsCentralStateTopology[nFiles][CepUtil::nBranches][CepUtil::nCentralStateTopologies]; //!
  TH1F *mh_YminusYExtr[nFiles][CepUtil::nBranches]; //!
  TH1F *mh_YminusYExtr_VsCentralStateTopology[nFiles][CepUtil::nBranches][CepUtil::nCentralStateTopologies]; //!
  
  
  double mSpecialLegendMarginFactor;
  bool mInverseStackDrawing;
  bool mSpecialDataDrawing;
  bool mSpecialDataDrawing2;
  double mRatioFactor;
  double mRatioYmin;
  double mRatioYmax;
  double mRatioYTitleOffsetFactor;
  TString mRatioYTitle;
  
  bool mSubtractNeutrals;
  double mNeutralsFracToPreserve;
  bool mBkgdSubtractionDemonstration;
  TString mSpecialDataLegendDesctiption;
  bool mDrawSpecialRatio;
  bool mDrawSpecialRatio2;
  bool mDrawSpecialRatio3;
  TH1F *mAuxiliaryHist;
  
  ClassDef(DataVsMC,0);
};


struct DrawingOptions{
  DrawingOptions(): mCanvasWidth(800.), mCanvasHeight(800.), mScale(kFALSE), mMaxDigits(3), mSetDivisionsX(kFALSE), mForceDivisionsX(kFALSE), mYlog(kFALSE), mSetDivisionsY(kFALSE), mForceDivisionsY(kFALSE), mInsert(kFALSE), mYMaxInstert(-9999), mNColumns(1), mLegendTitle(TString("")), mLegendTitle2(TString("")), mSystErrorBoxWidthAdjustmentFactor(1.0) {};
  
  Double_t mCanvasWidth;
  Double_t mCanvasHeight;
  
  Bool_t mScale;
  Double_t mScalingFactor;
  
  Double_t mLeftMargin;
  Double_t mRightMargin;
  Double_t mTopMargin;
  Double_t mBottomMargin;
  
  Int_t mMaxDigits;
  
  //x axis
  Double_t mXmin;
  Double_t mXmax;
  TString mXaxisTitle;
  Double_t mXaxisTitleSize;
  Double_t mXaxisTitleOffset;
  Double_t mXaxisLabelSize;
  Double_t mXaxisLabelOffset;
  Double_t mXaxisTickLength;
  Bool_t mSetDivisionsX;
  Bool_t mForceDivisionsX;
  Int_t mXnDivisionA;
  Int_t mXnDivisionB;
  Int_t mXnDivisionC;
  
  //y axis
  Bool_t mYlog;
  Double_t mYmin;
  Double_t mYmax;
  TString mYaxisTitle;
  Double_t mYaxisTitleSize;
  Double_t mYaxisTitleOffset;
  Double_t mYaxisLabelSize;
  Double_t mYaxisLabelOffset;
  Double_t mYaxisTickLength;
  Bool_t mSetDivisionsY;
  Bool_t mForceDivisionsY;
  Int_t mYnDivisionA;
  Int_t mYnDivisionB;
  Int_t mYnDivisionC;
  
  
  Bool_t mInsert;
  Double_t mX1Insert;
  Double_t mX2Insert;
  Double_t mY1Insert;
  Double_t mY2Insert;
  Double_t mXMinInstert;
  Double_t mXMaxInstert;
  Double_t mYMaxInstert;
  
  
  Int_t mMarkerStyle;
  Int_t mMarkerColor;
  Double_t mMarkerSize;
  Int_t mLineStyle;
  Int_t mLineColor;
  Double_t mLineWidth;
  
  Int_t mMarkerStyle2;
  Int_t mMarkerColor2;
  Double_t mMarkerSize2;
  Int_t mLineStyle2;
  Int_t mLineColor2;
  Double_t mLineWidth2;
  
  TString mPdfName;
  
  std::vector<TH1F*> mHistTH1F;
  std::vector<TString> mHistTH1F_DrawingOpt;
  
  std::vector<TH1F*> mHistTH1F2;
  std::vector<TString> mHistTH1F_DrawingOpt2;
  
  Int_t mNColumns;
  Double_t mLegendX;
  Double_t mLegendXwidth;
  Double_t mLegendY;
  Double_t mLegendYwidth;
  Double_t mLegendTextSize;
  std::vector<TObject*> mObjForLegend;
  std::vector<TString> mDesriptionForLegend;
  std::vector<TString> mDrawTypeForLegend;
  TString mLegendTitle;
  
  Double_t mLegendX2;
  Double_t mLegendXwidth2;
  Double_t mLegendY2;
  Double_t mLegendYwidth2;
  Double_t mLegendTextSize2;
  std::vector<TObject*> mObjForLegend2;
  std::vector<TString> mDesriptionForLegend2;
  std::vector<TString> mDrawTypeForLegend2;
  TString mLegendTitle2;
  
  Double_t mSystErrorBoxWidthAdjustmentFactor;
  
  Double_t mXwidthSystErrorBox;
  Int_t mSystErrorBoxColor;
  Int_t mSystErrorTotalBoxColor;
  Double_t mSystErrorBoxOpacity;
  std::vector<Double_t> mXpositionSystErrorBox;
  
  Double_t mXwidthSystErrorBox2;
  Int_t mSystErrorBoxColor2;
  Double_t mSystErrorBoxOpacity2;
  std::vector<Double_t> mXpositionSystErrorBox2;
  
  std::vector<TLatex> mTextToDraw;
  std::vector<TObject*> mObjectToDraw;
};

#endif
