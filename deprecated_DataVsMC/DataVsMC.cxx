#define DataVsMC_cxx

#include "DataVsMC.hh"
#include <TH2.h> 
#include <TF1.h> 
#include <TF2.h> 
#include <THStack.h> 
#include <TStyle.h> 
#include <TMultiGraph.h> 
#include <TGraph.h> 
#include <TGraph2D.h> 
#include <TGraphAsymmErrors.h> 
#include <TEfficiency.h> 
#include <TFractionFitter.h>
#include <TObjArray.h>
#include <iostream>
#include <utility>
#include <sstream> 
#include <algorithm> 
#include <stdio.h> 
#include <stdlib.h> 
#include <TCanvas.h> 
#include <TNamed.h>
#include <TLegend.h> 
#include <TBox.h>
#include <TGaxis.h> 
#include <vector> 
#include <fstream> 
#include <TString.h> 
#include <TColor.h> 
#include <TLine.h> 
#include <cmath> 
#include <TExec.h>
#include <TEllipse.h>
#include <TFitResultPtr.h> 
#include <TFitResult.h> 
#include <TLatex.h> 
#include <TExec.h>
#include <TMath.h>
#include <TArrow.h>
#include <TPaletteAxis.h>
#include <TRandom3.h>


ClassImp(DataVsMC)


DataVsMC::DataVsMC(TString option){
  mSubtractNeutrals = true;
//   mNeutralsFracToPreserve = 1.0;
//   mNeutralsFracToPreserve = 0.6;
//   mNeutralsFracToPreserve = 0.3;
  mNeutralsFracToPreserve = 0.4;
  mBkgdSubtractionDemonstration = false;
}

DataVsMC::~DataVsMC(){
}

void DataVsMC::analyze(){
  
  const double maxNSigmaMissingPt = 3.0;
  const double maxEta = 2.5;
  const double minInDetTrackPt = 0.1;
  
  loadHistograms();
  setupDrawingStyle();
  
  mDrawSpecialRatio = false;
  mDrawSpecialRatio2 = false;
  mDrawSpecialRatio3 = false;
  mSpecialDataDrawing = false;
  mSpecialDataDrawing2 = false;
  mRatioFactor = 0.25;
  mRatioYmin = 0.2;
  mRatioYmax = 1.8;
  mRatioYTitleOffsetFactor = 1.0;
  mRatioYTitle = TString("Data / MC");
  
  mAuxiliaryHist = nullptr;
  
  TH1F* referenceHist;
  TH1F* referenceHist_SameSign;
    
  int histColor[nFiles];
  histColor[DATA] = kBlack;
  histColor[MC_SIGNAL] = kYellow-7;
  histColor[MC_PYTHIA_CD_BKGD] = kGreen-1/*kGreen+2*/;
  histColor[MC_PYTHIA_CD_BKGD_NEUTRALS] = kGreen+1/*kAzure+7*/;
  
  int histColor2[nFiles];
  histColor2[DATA] = kRed;
  histColor2[MC_SIGNAL] = kYellow-7;
  histColor2[MC_PYTHIA_CD_BKGD] = kCyan;
  histColor2[MC_PYTHIA_CD_BKGD_NEUTRALS] = kWhite;
  
  TString legendText[nFiles];
  legendText[DATA] = TString("Data");
  legendText[MC_SIGNAL] = TString("Exclusive #pi^{+}#pi^{-}");
  legendText[MC_PYTHIA_CD_BKGD] = TString("CD w/o h^{+}h^{-}N");
  legendText[MC_PYTHIA_CD_BKGD_NEUTRALS] = TString( (mSubtractNeutrals && mNeutralsFracToPreserve<1.0) ? Form("CD h^{+}h^{-}N #times %g", mNeutralsFracToPreserve) : "CD h^{+}h^{-}N");
  
  TString legendText2[nFiles];
  legendText2[DATA] = TString("Data");
  legendText2[MC_SIGNAL] = TString("no such process");
  legendText2[MC_PYTHIA_CD_BKGD] = TString("CD");
  legendText2[MC_PYTHIA_CD_BKGD_NEUTRALS] = TString("CD h^{+}h^{-}N");
  

  double normalizationFactor[nFiles];
  
  const double missingPtToNormalizeCD_min = 5;
  const double missingPtToNormalizeCD_max = 10;
  
  
  normalizationFactor[DATA] = 1.0;

  TH1F *hData_missingPt = hNSigmaMissingPt[DATA][CepUtil::OPPO];
  TH1F *hCEP_missingPt = hNSigmaMissingPt[MC_SIGNAL][CepUtil::OPPO];
  TH1F *hCD_missingPt = new TH1F( *hNSigmaMissingPt[MC_PYTHIA_CD_BKGD][CepUtil::OPPO] );
  hCD_missingPt->Add( hNSigmaMissingPt[MC_PYTHIA_CD_BKGD_NEUTRALS][CepUtil::OPPO] );

  normalizationFactor[MC_PYTHIA_CD_BKGD] = (hData_missingPt->Integral(hData_missingPt->GetXaxis()->FindBin(missingPtToNormalizeCD_min), hData_missingPt->GetXaxis()->FindBin(missingPtToNormalizeCD_max-kEps))) / hCD_missingPt->Integral(hCD_missingPt->GetXaxis()->FindBin(missingPtToNormalizeCD_min), hCD_missingPt->GetXaxis()->FindBin(missingPtToNormalizeCD_max-kEps));
  normalizationFactor[MC_PYTHIA_CD_BKGD_NEUTRALS] = normalizationFactor[MC_PYTHIA_CD_BKGD];
  

  normalizationFactor[MC_SIGNAL] = ( hData_missingPt->Integral(0, hData_missingPt->GetXaxis()->FindBin(maxNSigmaMissingPt)-kEps)
                                          - normalizationFactor[MC_PYTHIA_CD_BKGD] * hCD_missingPt->Integral(0, hCD_missingPt->GetXaxis()->FindBin(maxNSigmaMissingPt-kEps)) )
                                          / hCEP_missingPt->Integral(0, hCEP_missingPt->GetXaxis()->FindBin(maxNSigmaMissingPt-kEps));

  
//   TFile *fOutput = new TFile("DataVsMCDataVsMC.root", "RECREATE");
  //BEGIN ---------------- ---------------- ---------------- ---------------- ---------------- ---------------- ---------------- ---------------- ---------------- ---------------- ----------------
  
  TH1F *totalData;
  TH1F *totalData_SameSign;
  TH1F *totalMC[nFiles];
  TH1F *totalMC_SameSign[nFiles];



  double integralInSignalRegion[nFiles][CepUtil::nCharges2Trks];
  double integralNSigmaMissingPt[nFiles][CepUtil::nCharges2Trks];
//   
//   
  //BEGIN                                       MISSING PT                                                        

  {
    referenceHist = hNSigmaMissingPt[DATA][CepUtil::OPPO];
    integralInSignalRegion[DATA][CepUtil::OPPO] = integratePtMiss( referenceHist, maxNSigmaMissingPt );
    integralNSigmaMissingPt[DATA][CepUtil::OPPO] = referenceHist->Integral();
    referenceHist->Scale(referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) ), "width");
    
    totalData = new TH1F( *referenceHist );
    
    referenceHist_SameSign = hNSigmaMissingPt[DATA][CepUtil::SAME];
    integralInSignalRegion[DATA][CepUtil::SAME] = integratePtMiss( referenceHist_SameSign, maxNSigmaMissingPt );
    integralNSigmaMissingPt[DATA][CepUtil::SAME] = referenceHist_SameSign->Integral();
    referenceHist_SameSign->Scale(referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) ), "width");
    
    totalData_SameSign = new TH1F( *referenceHist_SameSign );
    
    DrawingOptions opts;
    
    for(unsigned int fileId=DATA+1; fileId<nFiles; ++fileId){
      
      TH1F *mcHist = hNSigmaMissingPt[fileId][CepUtil::OPPO];
      integralInSignalRegion[fileId][CepUtil::OPPO] = normalizationFactor[fileId] * integratePtMiss( mcHist, maxNSigmaMissingPt );
      integralNSigmaMissingPt[fileId][CepUtil::OPPO] = normalizationFactor[fileId] * mcHist->Integral();
      mcHist->Scale( normalizationFactor[fileId] * mcHist->GetBinWidth( mcHist->FindBin(0.0001) ), "width");
      
      mcHist->SetLineColor( kBlack );
      mcHist->SetLineWidth( 2 );
      mcHist->SetFillColor( histColor[fileId] );
      
      totalMC[fileId] = new TH1F( *mcHist );
      
      opts.mHistTH1F.push_back( mcHist );
      opts.mHistTH1F_DrawingOpt.push_back( "HIST" );
      
      opts.mObjForLegend.push_back( mcHist );
      opts.mDesriptionForLegend.push_back( legendText[fileId] );
      opts.mDrawTypeForLegend.push_back( "f" );
      
      if( fileId==MC_PYTHIA_CD_BKGD_NEUTRALS ) continue;
      
      TH1F *mcHist_SameSign = hNSigmaMissingPt[fileId][CepUtil::SAME];
      integralInSignalRegion[fileId][CepUtil::SAME] = normalizationFactor[fileId] * integratePtMiss( mcHist_SameSign, maxNSigmaMissingPt );
      integralNSigmaMissingPt[fileId][CepUtil::SAME] = normalizationFactor[fileId] * mcHist_SameSign->Integral();
      mcHist_SameSign->Scale( normalizationFactor[fileId] * mcHist->GetBinWidth( mcHist->FindBin(0.0001) ), "width");
      
      mcHist_SameSign->SetLineColor( histColor2[fileId]/*+2*/ );
      mcHist_SameSign->SetLineWidth( 2 );
      mcHist_SameSign->SetFillColor( histColor2[fileId] );
      mcHist_SameSign->SetFillStyle( 3345 );
      
      totalMC_SameSign[fileId] = new TH1F( *mcHist_SameSign );
      
      opts.mHistTH1F2.push_back( mcHist_SameSign );
      opts.mHistTH1F_DrawingOpt2.push_back( "HIST" );
      
      opts.mObjForLegend2.push_back( fileId==MC_SIGNAL ? static_cast<TObject*>(nullptr) : mcHist_SameSign );
      opts.mDesriptionForLegend2.push_back( fileId==MC_SIGNAL ? "" : legendText2[fileId] );
      opts.mDrawTypeForLegend2.push_back( fileId==MC_SIGNAL ? "" : "f" );
    }
    
    opts.mCanvasWidth = 900;
    opts.mCanvasHeight = 800;
    
    opts.mLeftMargin = 0.12;
    opts.mRightMargin = 0.035;
    opts.mTopMargin = 0.06;
    opts.mBottomMargin = 0.12;
    
    //x axis
    opts.mXmin = 0;
    opts.mXmax = 30;
    opts.mXaxisTitle = TString("n#sigma(p_{T}^{miss})");
    opts.mXaxisTitleSize = 0.05;
    opts.mXaxisTitleOffset = 1.04;
    opts.mXaxisLabelSize = 0.05;
    opts.mXaxisLabelOffset = 0.007;
    opts.mXaxisTickLength = 0.015;
    
    //y axis
    opts.mYmin = 0;
    opts.mYmax = 1.35 * referenceHist->GetMaximum();
    opts.mYaxisTitle = TString(Form("Number of events / %g", referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) )) );
    opts.mYaxisTitleSize = 0.05;
    opts.mYaxisTitleOffset = 1.25;
    opts.mYaxisLabelSize = 0.05;
    opts.mYaxisLabelOffset = 0.007;
    opts.mYaxisTickLength = 0.015;
    opts.mYlog = kFALSE;
    
    opts.mMarkerStyle = 20;
    opts.mMarkerColor = kBlack;
    opts.mMarkerSize = 1.4;
    opts.mLineStyle = 1;
    opts.mLineColor = kBlack;
    opts.mLineWidth = 2;
    
    opts.mMarkerStyle2 = 20;
    opts.mMarkerColor2 = kRed;
    opts.mMarkerSize2 = 1.4;
    opts.mLineStyle2 = 1;
    opts.mLineColor2 = kRed;
    opts.mLineWidth2 = 2;
    
    opts.mPdfName = TString("NSigmaMissingPt");
    
    opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
    opts.mNColumns = 2;
    opts.mLegendX = true ? 0.42 : 0.4;
    opts.mLegendXwidth = 0.55;
    opts.mLegendY = 0.45;
    opts.mLegendYwidth = 0.14;
    mSpecialLegendMarginFactor = 1.2;
    
    
    opts.mLegendTextSize2 = 0.7 * opts.mXaxisTitleSize;
    opts.mLegendX2 = opts.mLegendX;
    opts.mLegendXwidth2 = opts.mLegendXwidth;
    opts.mLegendY2 = opts.mLegendY - 0.1;
    opts.mLegendYwidth2 = opts.mLegendYwidth;
    
    
    double labelShiftDown = 0.04;
    
    mInverseStackDrawing = true;
    
    
    TLine *cutLine = new TLine( maxNSigmaMissingPt, 0, maxNSigmaMissingPt, 0.4*referenceHist->GetMaximum() );
    cutLine->SetLineWidth(4);
    cutLine->SetLineColor(kRed);
    cutLine->SetLineStyle(7);
    cutLine->Draw();
    opts.mObjectToDraw.push_back(cutLine);
    
    TArrow *cutArrow = new TArrow( maxNSigmaMissingPt, 0.4*referenceHist->GetMaximum(), maxNSigmaMissingPt-2, 0.4*referenceHist->GetMaximum(), 0.035, "|>");
    cutArrow->SetAngle(30);
    cutArrow->SetLineWidth(4);
    cutArrow->SetLineColor( kRed );
    cutArrow->SetFillColor( kRed );
    opts.mObjectToDraw.push_back(cutArrow);    
    
    TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.26]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+#pi^{+}#pi^{-}+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
    opts.mTextToDraw.push_back( lReaction );
    TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //opts.mTextToDraw.push_back( lATLAS );
    
    TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", minInDetTrackPt );
    TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
    TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
//     TString rpKinematicRegion3("p_{x} > -0.2 GeV");
//     TString rpKinematicRegion2("(p_{x} + 0.3 GeV)^{2} + p_{y}^{2} < 0.25 GeV^{2}"); //opts.mTextToDraw.push_back( lATLAS );
    TLatex lCutsA(.16,.84-labelShiftDown, "#pi^{+}, #pi^{-}:"); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
    TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
    TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
    TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
    TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
    TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
//     TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
    TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
//     TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );   
    
//     TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
//     TLatex lDeltaPhi(.25,.677, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
    
    drawFinalResult( referenceHist, opts );
    
    TLatex lOppo(true ? 0.22 : .2, .635-labelShiftDown, "Opposite sign:"); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
    TLatex lSame(true ? .256 : .236, .635-labelShiftDown - 0.15, "Same sign:"); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
    
    double hatchesSpacing = gStyle->GetHatchesSpacing();
    double hatchesLineWidth = gStyle->GetHatchesLineWidth();
    gStyle->SetHatchesSpacing(3);
    gStyle->SetHatchesLineWidth(3);
    opts.mPdfName = TString("NSigmaMissingPt_OppositeAndSameSign");
    opts.mLegendY = 0.5;
    drawFinalResult( referenceHist, opts, referenceHist_SameSign );
    
    {
      opts.mTopMargin -= 0.04;
      opts.mLegendY += 0.04 + 0.04;
      opts.mLegendY2 = opts.mLegendY - 0.15;
      mRatioFactor = 0.15;
      mRatioYmin = 0.62;
      mRatioYmax = 1.38;
      mRatioYTitleOffsetFactor = 0.5;
      mRatioYTitle = TString("#kern[-0.2]{Data/MC}");
      //log-y scale
      opts.mPdfName = TString("LogY_NSigmaMissingPt_OppositeAndSameSign");
      opts.mYlog = kTRUE;
      opts.mYmin = 20;
      opts.mYmax = 10000 * referenceHist->GetMaximum();
      opts.mObjectToDraw.clear();
      cutLine = new TLine( maxNSigmaMissingPt, opts.mYmin, maxNSigmaMissingPt, 2*referenceHist->GetMaximum() );
      cutLine->SetLineWidth(4);
      cutLine->SetLineColor(kRed);
      cutLine->SetLineStyle(7);
      cutLine->Draw();
      opts.mObjectToDraw.push_back(cutLine);
      cutArrow = new TArrow( maxNSigmaMissingPt, 2*referenceHist->GetMaximum(), maxNSigmaMissingPt-2, 2*referenceHist->GetMaximum(), 0.035, "|>");
      cutArrow->SetAngle(30);
      cutArrow->SetLineWidth(4);
      cutArrow->SetLineColor( kRed );
      cutArrow->SetFillColor( kRed );
      opts.mObjectToDraw.push_back(cutArrow);  
      
      opts.mTextToDraw.clear();
      
      labelShiftDown -= 0.04;
      
      TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.26]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+#pi^{+}#pi^{-}+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
      opts.mTextToDraw.push_back( lReaction );
      TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
      //opts.mTextToDraw.push_back( lATLAS );
      
      TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", minInDetTrackPt );
      TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
      TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
//       TString rpKinematicRegion3("p_{x} > -0.2 GeV");
//       TString rpKinematicRegion2("(p_{x} + 0.3 GeV)^{2} + p_{y}^{2} < 0.25 GeV^{2}"); //opts.mTextToDraw.push_back( lATLAS );
      TLatex lCutsA(.16,.84-labelShiftDown, "#pi^{+}, #pi^{-}:"); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
      TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
      TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
      TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
        TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
      TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
//       TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
      TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
//       TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );
      
      
      TLatex lOppo(true ? 0.22 : .2, .635-labelShiftDown + 0.04, "Opposite sign:"); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
      TLatex lSame(true ? .256 : .236, .635-labelShiftDown - 0.15 + 0.04, "Same sign:"); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
      
      
//       TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
//       TLatex lDeltaPhi(/*.25*/.16,.677 + 0.04 + 0.02, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
      
      drawFinalResult( referenceHist, opts, referenceHist_SameSign );
      
      // back to default
      mRatioFactor = 0.25;
      mRatioYmin = 0.2;
      mRatioYmax = 1.8;
      mRatioYTitleOffsetFactor = 1.0;
      mRatioYTitle = TString("Data / MC");
    }
    
    gStyle->SetHatchesSpacing(hatchesSpacing);
    gStyle->SetHatchesLineWidth(hatchesLineWidth);
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  //BEGIN                                       MISSING PX                                                        
  
  {
    referenceHist = hMissingPx[DATA][CepUtil::OPPO];
    referenceHist->Scale(referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) ), "width");
    
    totalData = new TH1F( *referenceHist );
    
    referenceHist_SameSign = hMissingPx[DATA][CepUtil::SAME];
    referenceHist_SameSign->Scale(referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) ), "width");
    
    totalData_SameSign = new TH1F( *referenceHist_SameSign );
    
    DrawingOptions opts;
    
    for(unsigned int fileId=DATA+1; fileId<nFiles; ++fileId){
      
      TH1F *mcHist = hMissingPx[fileId][CepUtil::OPPO];
      mcHist->Scale( normalizationFactor[fileId] * mcHist->GetBinWidth( mcHist->FindBin(0.0001) ), "width");
      
      mcHist->SetLineColor( kBlack );
      mcHist->SetLineWidth( 2 );
      mcHist->SetFillColor( histColor[fileId] );
      
      totalMC[fileId] = new TH1F( *mcHist );
      
      opts.mHistTH1F.push_back( mcHist );
      opts.mHistTH1F_DrawingOpt.push_back( "HIST" );
      
      opts.mObjForLegend.push_back( mcHist );
      opts.mDesriptionForLegend.push_back( legendText[fileId] );
      opts.mDrawTypeForLegend.push_back( "f" );
      
      if( fileId==MC_PYTHIA_CD_BKGD_NEUTRALS || fileId==MC_SIGNAL ) continue;
      
      TH1F *mcHist_SameSign = hMissingPx[fileId][CepUtil::SAME];
      mcHist_SameSign->Scale( normalizationFactor[fileId] * mcHist->GetBinWidth( mcHist->FindBin(0.0001) ), "width");
      
      mcHist_SameSign->SetLineColor( histColor2[fileId]/*+2*/ );
      mcHist_SameSign->SetLineWidth( 2 );
      mcHist_SameSign->SetFillColor( histColor2[fileId] );
      mcHist_SameSign->SetFillStyle( 3345 );
      
      totalMC_SameSign[fileId] = new TH1F( *mcHist_SameSign );
      
      opts.mHistTH1F2.push_back( mcHist_SameSign );
      opts.mHistTH1F_DrawingOpt2.push_back( "HIST" );
      
      opts.mObjForLegend2.push_back( /*fileId==MC_SIGNAL ? static_cast<TObject*>(nullptr) :*/ mcHist_SameSign );
      opts.mDesriptionForLegend2.push_back( /*fileId==MC_SIGNAL ? "" :*/ legendText2[fileId] );
      opts.mDrawTypeForLegend2.push_back( /*fileId==MC_SIGNAL ? "" :*/ "f" );
    }

    
    
    for(int t=0; t<2; ++t){
      opts.mObjForLegend2.push_back( static_cast<TObject*>(nullptr) );
      opts.mDesriptionForLegend2.push_back(  "" );
      opts.mDrawTypeForLegend2.push_back( "" );
    }
    
    
    opts.mCanvasWidth = 900;
    opts.mCanvasHeight = 800;
    
    opts.mLeftMargin = 0.12;
    opts.mRightMargin = 0.035;
    opts.mTopMargin = 0.06;
    opts.mBottomMargin = 0.12;
    
    //x axis
    opts.mXmin = -1;
    opts.mXmax = 1;
    opts.mXaxisTitle = TString("p_{x}^{miss} [GeV]");
    opts.mXaxisTitleSize = 0.05;
    opts.mXaxisTitleOffset = 1.04;
    opts.mXaxisLabelSize = 0.05;
    opts.mXaxisLabelOffset = 0.007;
    opts.mXaxisTickLength = 0.015;
    
    //y axis
    opts.mYmin = 0;
    opts.mYmax = 1.35 * referenceHist->GetMaximum();
    opts.mYaxisTitle = TString(Form("Number of events / %d MeV", static_cast<int>(1e3*referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) ))));
    opts.mYaxisTitleSize = 0.05;
    opts.mYaxisTitleOffset = 1.25;
    opts.mYaxisLabelSize = 0.05;
    opts.mYaxisLabelOffset = 0.007;
    opts.mYaxisTickLength = 0.015;
    opts.mYlog = kFALSE;
    
    opts.mMarkerStyle = 20;
    opts.mMarkerColor = kBlack;
    opts.mMarkerSize = 1.4;
    opts.mLineStyle = 1;
    opts.mLineColor = kBlack;
    opts.mLineWidth = 2;
    
    opts.mMarkerStyle2 = 20;
    opts.mMarkerColor2 = kRed;
    opts.mMarkerSize2 = 1.4;
    opts.mLineStyle2 = 1;
    opts.mLineColor2 = kRed;
    opts.mLineWidth2 = 2;
    
    opts.mPdfName = TString("MissingPx");
    
    opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
    opts.mNColumns = 1;
    opts.mLegendX = true ? 0.2 : 0.4;
    opts.mLegendXwidth = 0.2;
    opts.mLegendY = 0.3;
    opts.mLegendYwidth = 0.14*2.2;
    mSpecialLegendMarginFactor = 1.2;
    
    
    opts.mLegendTextSize2 = 0.7 * opts.mXaxisTitleSize;
    opts.mLegendX2 = opts.mLegendX + 0.45;
    opts.mLegendXwidth2 = opts.mLegendXwidth;
    opts.mLegendY2 = opts.mLegendY;
    opts.mLegendYwidth2 = opts.mLegendYwidth;
    
    
    double labelShiftDown = 0.04;
    
    mInverseStackDrawing = true;
    
    TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.26]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+#pi^{+}#pi^{-}+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
    opts.mTextToDraw.push_back( lReaction );
    TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //opts.mTextToDraw.push_back( lATLAS );
    
    TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", minInDetTrackPt );
    TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
    TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
    TLatex lCutsA(.16,.84-labelShiftDown, "#pi^{+}, #pi^{-}:"); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
    TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
    TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
    TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
    TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
    TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
    //     TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
    TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
    //     TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );   
    
    //     TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
    //     TLatex lDeltaPhi(.25,.677, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
    
    drawFinalResult( referenceHist, opts );
    
    TLatex lOppo(true ? 0.18 : .2, .67-labelShiftDown, "Opposite sign:"); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
    TLatex lSame(true ? .65 : .236, .67-labelShiftDown, "Same sign:"); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
    
    double hatchesSpacing = gStyle->GetHatchesSpacing();
    double hatchesLineWidth = gStyle->GetHatchesLineWidth();
    gStyle->SetHatchesSpacing(3);
    gStyle->SetHatchesLineWidth(3);
    opts.mPdfName = TString("MissingPx_OppositeAndSameSign");
    drawFinalResult( referenceHist, opts, referenceHist_SameSign );
    
    {
      opts.mTopMargin -= 0.04;
      opts.mLegendY += 0.13;
      opts.mLegendY2 = opts.mLegendY;
      mRatioFactor = 0.15;
      mRatioYmin = 0.62;
      mRatioYmax = 1.38;
      mRatioYTitleOffsetFactor = 0.5;
      mRatioYTitle = TString("#kern[-0.2]{Data/MC}");
      //log-y scale
      opts.mPdfName = TString("LogY_MissingPx_OppositeAndSameSign");
      opts.mYlog = kTRUE;
      opts.mYmin = 200;
      opts.mYmax = 1000 * referenceHist->GetMaximum();
      opts.mObjectToDraw.clear(); 
      
      opts.mTextToDraw.clear();
      
      labelShiftDown -= 0.04;
      
      TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.26]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+#pi^{+}#pi^{-}+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
      opts.mTextToDraw.push_back( lReaction );
      TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
      //opts.mTextToDraw.push_back( lATLAS );
      
      TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", minInDetTrackPt );
      TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
      TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
      TLatex lCutsA(.16,.84-labelShiftDown, "#pi^{+}, #pi^{-}:"); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
      TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
      TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
      TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
        TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
      TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
      //       TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
      TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
      //       TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );
      
      
      TLatex lOppo(true ? 0.18 : .2, .67-labelShiftDown + 0.07, "Opposite sign:"); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
      TLatex lSame(true ? .65 : .236, .67-labelShiftDown + 0.07, "Same sign:"); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
      
      
      //       TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
      //       TLatex lDeltaPhi(/*.25*/.16,.677 + 0.04 + 0.02, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
      
      drawFinalResult( referenceHist, opts, referenceHist_SameSign );
      
      // back to default
      mRatioFactor = 0.25;
      mRatioYmin = 0.2;
      mRatioYmax = 1.8;
      mRatioYTitleOffsetFactor = 1.0;
      mRatioYTitle = TString("Data / MC");
    }
    
    gStyle->SetHatchesSpacing(hatchesSpacing);
    gStyle->SetHatchesLineWidth(hatchesLineWidth);
  }
  
  
  
  
  
  
  
  //BEGIN                                       MISSING PY                                                        

  {
    referenceHist = hMissingPy[DATA][CepUtil::OPPO];
    referenceHist->Scale(referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) ), "width");
    
    totalData = new TH1F( *referenceHist );
    
    referenceHist_SameSign = hMissingPy[DATA][CepUtil::SAME];
    referenceHist_SameSign->Scale(referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) ), "width");
    
    totalData_SameSign = new TH1F( *referenceHist_SameSign );
    
    DrawingOptions opts;
    
    for(unsigned int fileId=DATA+1; fileId<nFiles; ++fileId){
      
      TH1F *mcHist = hMissingPy[fileId][CepUtil::OPPO];
      mcHist->Scale( normalizationFactor[fileId] * mcHist->GetBinWidth( mcHist->FindBin(0.0001) ), "width");
      
      mcHist->SetLineColor( kBlack );
      mcHist->SetLineWidth( 2 );
      mcHist->SetFillColor( histColor[fileId] );
      
      totalMC[fileId] = new TH1F( *mcHist );
      
      opts.mHistTH1F.push_back( mcHist );
      opts.mHistTH1F_DrawingOpt.push_back( "HIST" );
      
      opts.mObjForLegend.push_back( mcHist );
      opts.mDesriptionForLegend.push_back( legendText[fileId] );
      opts.mDrawTypeForLegend.push_back( "f" );
      
      if( fileId==MC_PYTHIA_CD_BKGD_NEUTRALS || fileId==MC_SIGNAL ) continue;
      
      TH1F *mcHist_SameSign = hMissingPy[fileId][CepUtil::SAME];
      mcHist_SameSign->Scale( normalizationFactor[fileId] * mcHist->GetBinWidth( mcHist->FindBin(0.0001) ), "width");
      
      mcHist_SameSign->SetLineColor( histColor2[fileId]/*+2*/ );
      mcHist_SameSign->SetLineWidth( 2 );
      mcHist_SameSign->SetFillColor( histColor2[fileId] );
      mcHist_SameSign->SetFillStyle( 3345 );
      
      totalMC_SameSign[fileId] = new TH1F( *mcHist_SameSign );
      
      opts.mHistTH1F2.push_back( mcHist_SameSign );
      opts.mHistTH1F_DrawingOpt2.push_back( "HIST" );
      
      opts.mObjForLegend2.push_back( /*fileId==MC_SIGNAL ? static_cast<TObject*>(nullptr) :*/ mcHist_SameSign );
      opts.mDesriptionForLegend2.push_back( /*fileId==MC_SIGNAL ? "" :*/ legendText2[fileId] );
      opts.mDrawTypeForLegend2.push_back( /*fileId==MC_SIGNAL ? "" :*/ "f" );
    }
    
    
    
    for(int t=0; t<2; ++t){
      opts.mObjForLegend2.push_back( static_cast<TObject*>(nullptr) );
      opts.mDesriptionForLegend2.push_back(  "" );
      opts.mDrawTypeForLegend2.push_back( "" );
    }
    
    
    opts.mCanvasWidth = 900;
    opts.mCanvasHeight = 800;
    
    opts.mLeftMargin = 0.12;
    opts.mRightMargin = 0.035;
    opts.mTopMargin = 0.06;
    opts.mBottomMargin = 0.12;
    
    //x axis
    opts.mXmin = -1;
    opts.mXmax = 1;
    opts.mXaxisTitle = TString("p_{y}^{miss} [GeV]");
    opts.mXaxisTitleSize = 0.05;
    opts.mXaxisTitleOffset = 1.04;
    opts.mXaxisLabelSize = 0.05;
    opts.mXaxisLabelOffset = 0.007;
    opts.mXaxisTickLength = 0.015;
    
    //y axis
    opts.mYmin = 0;
    opts.mYmax = 1.35 * referenceHist->GetMaximum();
    opts.mYaxisTitle = TString(Form("Number of events / %d MeV", static_cast<int>(1e3*referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) ))));
    opts.mYaxisTitleSize = 0.05;
    opts.mYaxisTitleOffset = 1.25;
    opts.mYaxisLabelSize = 0.05;
    opts.mYaxisLabelOffset = 0.007;
    opts.mYaxisTickLength = 0.015;
    opts.mYlog = kFALSE;
    
    opts.mMarkerStyle = 20;
    opts.mMarkerColor = kBlack;
    opts.mMarkerSize = 1.4;
    opts.mLineStyle = 1;
    opts.mLineColor = kBlack;
    opts.mLineWidth = 2;
    
    opts.mMarkerStyle2 = 20;
    opts.mMarkerColor2 = kRed;
    opts.mMarkerSize2 = 1.4;
    opts.mLineStyle2 = 1;
    opts.mLineColor2 = kRed;
    opts.mLineWidth2 = 2;
    
    opts.mPdfName = TString("MissingPy");
    
    opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
    opts.mNColumns = 1;
    opts.mLegendX = true ? 0.2 : 0.4;
    opts.mLegendXwidth = 0.2;
    opts.mLegendY = 0.3;
    opts.mLegendYwidth = 0.14*2.2;
    mSpecialLegendMarginFactor = 1.2;
    
    
    opts.mLegendTextSize2 = 0.7 * opts.mXaxisTitleSize;
    opts.mLegendX2 = opts.mLegendX + 0.45;
    opts.mLegendXwidth2 = opts.mLegendXwidth;
    opts.mLegendY2 = opts.mLegendY;
    opts.mLegendYwidth2 = opts.mLegendYwidth;
    
    
    double labelShiftDown = 0.04;
    
    mInverseStackDrawing = true;
    
    TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.26]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+#pi^{+}#pi^{-}+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
    opts.mTextToDraw.push_back( lReaction );
    TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //opts.mTextToDraw.push_back( lATLAS );
    
    TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", minInDetTrackPt );
    TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
    TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
    TLatex lCutsA(.16,.84-labelShiftDown, "#pi^{+}, #pi^{-}:"); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
    TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
    TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
    TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
    TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
    TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
    //     TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
    TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
    //     TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );   
    
    //     TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
    //     TLatex lDeltaPhi(.25,.677, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
    
    drawFinalResult( referenceHist, opts );
    
    TLatex lOppo(true ? 0.18 : .2, .67-labelShiftDown, "Opposite sign:"); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
    TLatex lSame(true ? .65 : .236, .67-labelShiftDown, "Same sign:"); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
    
    double hatchesSpacing = gStyle->GetHatchesSpacing();
    double hatchesLineWidth = gStyle->GetHatchesLineWidth();
    gStyle->SetHatchesSpacing(3);
    gStyle->SetHatchesLineWidth(3);
    opts.mPdfName = TString("MissingPy_OppositeAndSameSign");
    drawFinalResult( referenceHist, opts, referenceHist_SameSign );
    
    {
      opts.mTopMargin -= 0.04;
      opts.mLegendY += 0.13;
      opts.mLegendY2 = opts.mLegendY;
      mRatioFactor = 0.15;
      mRatioYmin = 0.62;
      mRatioYmax = 1.38;
      mRatioYTitleOffsetFactor = 0.5;
      mRatioYTitle = TString("#kern[-0.2]{Data/MC}");
      //log-y scale
      opts.mPdfName = TString("LogY_MissingPy_OppositeAndSameSign");
      opts.mYlog = kTRUE;
      opts.mYmin = 200;
      opts.mYmax = 1000 * referenceHist->GetMaximum();
      opts.mObjectToDraw.clear(); 
      
      opts.mTextToDraw.clear();
      
      labelShiftDown -= 0.04;
      
      TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.26]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+#pi^{+}#pi^{-}+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
      opts.mTextToDraw.push_back( lReaction );
      TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
      //opts.mTextToDraw.push_back( lATLAS );
      
      TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", minInDetTrackPt );
      TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
      TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
      TLatex lCutsA(.16,.84-labelShiftDown, "#pi^{+}, #pi^{-}:"); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
      TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
      TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
      TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
        TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
      TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
      //       TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
      TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
      //       TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );
      
      
      TLatex lOppo(true ? 0.18 : .2, .67-labelShiftDown + 0.07, "Opposite sign:"); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
      TLatex lSame(true ? .65 : .236, .67-labelShiftDown + 0.07, "Same sign:"); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
      
      
      //       TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
      //       TLatex lDeltaPhi(/*.25*/.16,.677 + 0.04 + 0.02, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
      
      drawFinalResult( referenceHist, opts, referenceHist_SameSign );
      
      // back to default
      mRatioFactor = 0.25;
      mRatioYmin = 0.2;
      mRatioYmax = 1.8;
      mRatioYTitleOffsetFactor = 1.0;
      mRatioYTitle = TString("Data / MC");
    }
    
    gStyle->SetHatchesSpacing(hatchesSpacing);
    gStyle->SetHatchesLineWidth(hatchesLineWidth);
  }
  
  
  
  
  
  
  
  
  
  
  
  //BEGIN                                     X and Y hit position                                                   
  
  
  for(int rp=0; rp<CepUtil::nRomanPots; ++rp){
    for(int coor=0; coor<CepUtil::nCoordinates; ++coor){
      
      if(coor == CepUtil::X)
        referenceHist = TH1F_from_TH1( hHitMapExclusivePiPi[DATA][rp]->ProjectionX( Form("%s_xyproj_%d_%d", hHitMapExclusivePiPi[DATA][rp]->GetName(), rp, coor) ) );
      else
        referenceHist = TH1F_from_TH1( hHitMapExclusivePiPi[DATA][rp]->ProjectionY( Form("%s_xyproj_%d_%d", hHitMapExclusivePiPi[DATA][rp]->GetName(), rp, coor) ) );
//       referenceHist->Rebin(coor==CepUtil::X ? 4 : 10);
      referenceHist->Scale( normalizationFactor[DATA] );
      
      DrawingOptions opts;
      
      for(unsigned int fileId=DATA+1; fileId<nFiles; ++fileId){
        
        TH1F *mcHist;
        if(coor == CepUtil::X)
          mcHist = TH1F_from_TH1( hHitMapExclusivePiPi[fileId][rp]->ProjectionX( Form("%s_xyproj_%d_%d", hHitMapExclusivePiPi[fileId][rp]->GetName(), rp, coor) ) );
        else
          mcHist = TH1F_from_TH1( hHitMapExclusivePiPi[fileId][rp]->ProjectionY( Form("%s_xyproj_%d_%d", hHitMapExclusivePiPi[fileId][rp]->GetName(), rp, coor) ) );
//         mcHist->Rebin(coor==CepUtil::X ? 4 : 10);
        mcHist->Scale( normalizationFactor[fileId] );
//         mcHist->Scale( mcHist->GetBinWidth( mcHist->FindBin(0.) ), "width");
        
        mcHist->SetLineColor( kBlack );
        mcHist->SetLineWidth( 2 );
        mcHist->SetFillColor( histColor[fileId] );
        
        opts.mHistTH1F.push_back( mcHist );
        opts.mHistTH1F_DrawingOpt.push_back( "HIST" );
        
        opts.mObjForLegend.push_back( mcHist );
        opts.mDesriptionForLegend.push_back( legendText[fileId] );
        opts.mDrawTypeForLegend.push_back( "f" );
      }
      
      opts.mMaxDigits = 3;
      
      opts.mCanvasWidth = 900;
      opts.mCanvasHeight = 800;
      
      opts.mLeftMargin = 0.17;
      opts.mRightMargin = 0.025;
      opts.mTopMargin = 0.075;
      opts.mBottomMargin = 0.14;
      
      //x axis
      opts.mXmin = coor==CepUtil::X ? -8 : ((rp==0 || rp==2 || rp==4 || rp==6) ? 0 : -25);
      opts.mXmax = coor==CepUtil::X ? 8 : ((rp==0 || rp==2 || rp==4 || rp==6) ? 25 : 0);
      opts.mXaxisTitle = TString(coor==CepUtil::X ? "x [mm]" : "y [mm]");
      opts.mXaxisTitleSize = 0.07;
      opts.mXaxisTitleOffset = 0.97;
      opts.mXaxisLabelSize = 0.07;
      opts.mXaxisLabelOffset = 0.007;
      opts.mXaxisTickLength = 0.015;
      
      //y axis
      opts.mYmin = 5;
      opts.mYmax = 10000 * referenceHist->GetMaximum();
      opts.mYaxisTitle = TString(Form("Num. of track points / %g mm#kern[-4.0]{ }",  referenceHist->GetBinWidth( referenceHist->FindBin(0.) )  ));
      opts.mYaxisTitleSize = 0.07;
      opts.mYaxisTitleOffset = 1.145;
      opts.mYaxisLabelSize = 0.07;
      opts.mYaxisLabelOffset = 0.01;
      opts.mYaxisTickLength = 0.015;
      opts.mYlog = kTRUE;
      
      opts.mMarkerStyle = 20;
      opts.mMarkerColor = kBlack;
      opts.mMarkerSize = 1.4;
      
      opts.mLineStyle = 1;
      opts.mLineColor = kBlack;
      opts.mLineWidth = 2;
      
      opts.mPdfName = TString(Form("%s_%d", TString(coor==CepUtil::X ? "x" : "y").Data(), rp));
      
      opts.mLegendTextSize = 0.7 * 0.05/*opts.mXaxisTitleSize*/;
     
      if( coor == CepUtil::X ){
        opts.mNColumns = 1;
        opts.mLegendX = 0.21;
        opts.mLegendXwidth = 0.25;
        opts.mLegendY = 0.24;
        opts.mLegendYwidth = 0.4;
        mSpecialLegendMarginFactor = 1.0;
      } else{
        opts.mNColumns = 1;
        opts.mLegendX = (rp==0 || rp==2 || rp==4 || rp==6) ? 0.73 : 0.19;
        opts.mLegendXwidth = 0.25*0.8;
        opts.mLegendY = 0.4;
        opts.mLegendYwidth = 0.4*0.6;
        mSpecialLegendMarginFactor = 1.0;
      }
      
      mInverseStackDrawing = true;
      
      const double labelShiftDown = 0.04;
      double offs = 0.06;
      
    TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.26]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+#pi^{+}#pi^{-}+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.7 * opts.mXaxisTitleSize);
    opts.mTextToDraw.push_back( lReaction );
//     TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //opts.mTextToDraw.push_back( lATLAS );
    
    TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", minInDetTrackPt );
    TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
    TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
    TLatex lCutsA(.16+offs,.84-labelShiftDown, "#pi^{+}, #pi^{-}:"); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
    TLatex lCutsB(.44+offs,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
    TLatex lCuts1(.26+offs,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
    TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
    TLatex lCuts12(.26+offs,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
    TLatex lCuts11(.26+offs,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
    //     TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
    TLatex lCuts3(.54+offs,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
    //     TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );   

//       
      
      TLatex lRpStr( (coor==CepUtil::Y) ? ((rp==0 || rp==2 || rp==4 || rp==6) ? 0.23 : 0.8 ) : 0.78 , .46, Form("RP %d", rp)); lRpStr.SetNDC(); lRpStr.SetTextFont(42);  lRpStr.SetTextSize(1.5 * 0.05); opts.mTextToDraw.push_back( lRpStr );
      
      drawFinalResult( referenceHist, opts );
      
      
      //linear y scale
      opts.mPdfName = TString(Form("Linear_%s_%d", TString(coor==CepUtil::X ? "x" : "y").Data(), rp));
      opts.mYlog = kFALSE;
      opts.mYmin = 0.0;
      opts.mYmax = 1.65 * referenceHist->GetMaximum();
      
//       mRatioYTitleOffsetFactor = 0.82;
      
      drawFinalResult( referenceHist, opts );
    }
  }
  
  mRatioYTitleOffsetFactor = 1.0;
  
  
  
  
  
  
  
  
  
  
  
  gStyle->SetPalette(69);
  TColor::InvertPalette();
  //BEGIN                                     Y vs. X hit map (2D)                                                   
  
  TH2F *referenceHist2D[CepUtil::nRomanPots] = {nullptr,nullptr,nullptr,nullptr,nullptr,nullptr,nullptr,nullptr};
  TH2F *mcHist2D[CepUtil::nRomanPots] = {nullptr,nullptr,nullptr,nullptr,nullptr,nullptr,nullptr,nullptr};
  
  for(int rp=0; rp<CepUtil::nRomanPots; ++rp){
      
    referenceHist2D[rp] = new TH2F( *hHitMapExclusivePiPi[DATA][rp] );
    
      for(unsigned int fileId=DATA+1; fileId<nFiles; ++fileId){
        TH2F *mcHist2D_single = new TH2F( *hHitMapExclusivePiPi[fileId][rp] );
        mcHist2D_single->Scale( normalizationFactor[fileId] );
        if(!mcHist2D[rp])
          mcHist2D[rp] = new TH2F( *mcHist2D_single );
        else
          mcHist2D[rp]->Add( mcHist2D_single );
      }
      
  }
  
  
  TCanvas *c = new TCanvas("c", "c", 800, 800);
  c->SetBottomMargin(0);
  c->SetTopMargin(0);
  c->SetLeftMargin(0);
  c->SetRightMargin(0);
        
  c->Divide(2);
  
  c->Print("HitMap.pdf[");
  
  TGaxis::SetMaxDigits(4);
  
  for(int rp=0; rp<CepUtil::nRomanPots; rp+=2){
    
    referenceHist2D[rp]->Rebin2D(2,2);
    referenceHist2D[rp+1]->Rebin2D(2,2);
    mcHist2D[rp]->Rebin2D(2,2);
    mcHist2D[rp+1]->Rebin2D(2,2);
    
    const double labelsize = 0.07;
    
        double zAxisMax = max( referenceHist2D[rp]->GetMaximum(),  mcHist2D[rp]->GetMaximum() );
        
        TH2F *histToDraw = referenceHist2D[rp];
        TH2F *histToDraw2 = referenceHist2D[rp+1];
        
        c->cd(1);
        gPad->SetBottomMargin(0.08);
        gPad->SetTopMargin(0.26);
        gPad->SetLeftMargin(0.19);
        gPad->SetRightMargin(0.0);
        
        histToDraw->GetXaxis()->SetLabelSize(labelsize );
        histToDraw->GetXaxis()->SetTitleSize( labelsize);
        histToDraw->GetYaxis()->SetLabelSize( labelsize);
        histToDraw->GetYaxis()->SetTitleSize(labelsize );
        histToDraw->GetZaxis()->SetLabelSize( labelsize );
        histToDraw->GetZaxis()->SetTitleSize( labelsize );
        histToDraw->GetXaxis()->SetTickLength( 0.5 * histToDraw->GetXaxis()->GetTickLength() );
        histToDraw->GetXaxis()->SetNdivisions(18,4,1);
        histToDraw->GetXaxis()->SetLabelOffset( -0.02 );
        histToDraw->GetYaxis()->SetLabelOffset( 0.02 );
        
        histToDraw->GetXaxis()->SetRangeUser(-7.5, 7.5);
        histToDraw->GetXaxis()->SetTitle("x [mm]");
        histToDraw->GetYaxis()->SetRangeUser(-80, 80);
        histToDraw->GetYaxis()->SetTitle("y [mm]");
        histToDraw->GetYaxis()->SetTitleOffset(1.35);
        histToDraw->GetXaxis()->SetTitleOffset(0.6);
        histToDraw->GetZaxis()->SetTitle("");
        histToDraw->Draw("col");
        histToDraw2->Draw("col same");
        
        histToDraw->GetZaxis()->SetRangeUser(0, zAxisMax);
        histToDraw2->GetZaxis()->SetRangeUser(0, zAxisMax);
        
        TLatex lRpStr( 0.25, .38, Form("%s %s, DATA", rp<4 ? "A" : "C", (rp==2 || rp==4) ? "near" : "far" ) ); lRpStr.SetNDC(); lRpStr.SetTextFont(42); lRpStr.SetTextSize(2.0 * 0.05); lRpStr.Draw();
        lRpStr.DrawLatex( 0.73, 0.6, Form("RP %d", rp));
        lRpStr.DrawLatex( 0.73, 0.17, Form("RP %d", rp+1));
        
        gPad->RedrawAxis();
        //---
        
        
        histToDraw = mcHist2D[rp];
        histToDraw2 = mcHist2D[rp+1];
        
        
        c->cd(2);
        gPad->SetBottomMargin(0.08);
        gPad->SetTopMargin(0.26);
        gPad->SetLeftMargin(0.0);
        gPad->SetRightMargin(0.23);
        
        histToDraw->GetXaxis()->SetLabelSize( labelsize );
        histToDraw->GetXaxis()->SetTitleSize( labelsize);
        histToDraw->GetYaxis()->SetLabelSize( labelsize);
        histToDraw->GetYaxis()->SetTitleSize(labelsize );
        histToDraw->GetZaxis()->SetLabelSize( labelsize );
        histToDraw->GetZaxis()->SetTitleSize( labelsize );
        histToDraw->GetXaxis()->SetTickLength( 0.5 * histToDraw->GetXaxis()->GetTickLength() );
        histToDraw->GetXaxis()->SetNdivisions(18,4,1);
        histToDraw->GetXaxis()->SetLabelOffset( -0.02 );
        histToDraw->GetYaxis()->SetLabelOffset( 0.01 );
        
        histToDraw->GetXaxis()->SetRangeUser(-7.5, 7.5);
        histToDraw->GetXaxis()->SetTitle("x [mm]");
        histToDraw->GetYaxis()->SetRangeUser(-80, 80);
        histToDraw->GetYaxis()->SetTitle("");
        histToDraw->GetYaxis()->SetLabelSize(0);
        histToDraw->GetXaxis()->SetTitleOffset(0.6);
        TString xWidth(Form("%.g", (histToDraw->GetXaxis()->GetBinWidth(1)) ) );
        TString yWidth(Form("%.g", (histToDraw->GetYaxis()->GetBinWidth(1)) ) );
        histToDraw->GetZaxis()->SetTitle("Track points / ("+xWidth+" mm #times "+yWidth+" mm)");
        histToDraw->GetZaxis()->SetTitleOffset(1.6);
        histToDraw->GetZaxis()->SetLabelOffset(0.02);
        histToDraw->Draw("colz");
        histToDraw2->Draw("col same");
        
        histToDraw->GetZaxis()->SetRangeUser(0, zAxisMax);
        histToDraw2->GetZaxis()->SetRangeUser(0, zAxisMax);
        
        gPad->Update();
        TPaletteAxis* palette = (TPaletteAxis*)histToDraw->GetListOfFunctions()->FindObject("palette");
        if(palette){
          palette->SetX1NDC(0.825-0.05);
          palette->SetX2NDC(0.85-0.05);
        }
        gPad->Modified();
        
        TLatex lRpStr2( 0.2, .38, Form("%s %s, MC", rp<4 ? "A" : "C", (rp==2 || rp==4) ? "near" : "far" ) ); lRpStr2.SetNDC(); lRpStr2.SetTextFont(42); lRpStr2.SetTextSize(2.0 * 0.05); lRpStr2.Draw();
        lRpStr2.DrawLatex( 0.5, 0.6, Form("RP %d", rp));
        lRpStr2.DrawLatex( 0.5, 0.17, Form("RP %d", rp+1));
        
        const double labelShiftDown = -0.02;
        double offs = -0.04;
        
        c->cd();
        
        TLatex lReaction(.22, .91-labelShiftDown, "#kern[-0.26]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+#pi^{+}#pi^{-}+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.7 * labelsize);
        lReaction.Draw();
    //     TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
        //lATLAS.Draw();
        
        TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", minInDetTrackPt );
        TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
        TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
        TLatex lCutsA(.16+offs,.84-labelShiftDown, "#pi^{+}, #pi^{-}:"); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.5 * labelsize); lCutsA.Draw();
        TLatex lCutsB(.44+offs,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.5 * labelsize); lCutsB.Draw();
        TLatex lCuts1(.26+offs,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.5 * labelsize); lCuts1.Draw();
        TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
        TLatex lCuts12(.26+offs,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.5 * labelsize); lCuts12.Draw();
        TLatex lCuts11(.26+offs,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.5 * labelsize); lCuts11.Draw();
        //     TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.5 * labelsize); lCuts2.Draw();
        TLatex lCuts3(.54+offs,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.5 * labelsize); lCuts3.Draw();
        //     TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.5 * labelsize); lCuts4.Draw();
        
        
        
        c->Print("HitMap.pdf");

  }
  c->Print("HitMap.pdf]");
  
  
  
  
  
  
  
  
  
  
    
  //BEGIN                                     Y - Y extrapolated                                                   
  
  
  for(int br=0; br<CepUtil::nBranches; ++br){
      
    referenceHist = mh_YminusYExtr_BeforeMomentumBalanceCut[DATA][br];
      referenceHist->Rebin(4);
      referenceHist->Scale(normalizationFactor[DATA] );
      
      DrawingOptions opts;
      
      for(unsigned int fileId=DATA+1; fileId<nFiles; ++fileId){
        
        TH1F *mcHist = mh_YminusYExtr_BeforeMomentumBalanceCut[fileId][br];
        mcHist->Rebin(4);
        mcHist->Scale( normalizationFactor[fileId] );
//         mcHist->Scale( mcHist->GetBinWidth( mcHist->FindBin(0.) ), "width");
        
        mcHist->SetLineColor( kBlack );
        mcHist->SetLineWidth( 2 );
        mcHist->SetFillColor( histColor[fileId] );
        
        opts.mHistTH1F.push_back( mcHist );
        opts.mHistTH1F_DrawingOpt.push_back( "HIST" );
        
        opts.mObjForLegend.push_back( mcHist );
        opts.mDesriptionForLegend.push_back( legendText[fileId] );
        opts.mDrawTypeForLegend.push_back( "f" );
      }
      
      opts.mMaxDigits = 5;
      
      opts.mCanvasWidth = 900;
      opts.mCanvasHeight = 800;
      
      opts.mLeftMargin = 0.17;
      opts.mRightMargin = 0.025;
      opts.mTopMargin = 0.075;
      opts.mBottomMargin = 0.14;
      
      //x axis
      opts.mXmin = -1;
      opts.mXmax = 1;
      opts.mXaxisTitle = TString("y_{far} - y_{extr} [mm]");
      opts.mXaxisTitleSize = 0.05;
      opts.mXaxisTitleOffset = 1.04;
      opts.mXaxisLabelSize = 0.05;
      opts.mXaxisLabelOffset = 0.007;
      opts.mXaxisTickLength = 0.015;
      
      //y axis
      opts.mYmin = 5;
      opts.mYmax = 10000 * referenceHist->GetMaximum();
      opts.mYaxisTitle = TString(Form("Num. of track points / %g mm#kern[-4.0]{ }",  referenceHist->GetBinWidth( referenceHist->FindBin(0.) )  ));
      opts.mYaxisTitleSize = 0.05;
      opts.mYaxisTitleOffset = 1.25;
      opts.mYaxisLabelSize = 0.05;
      opts.mYaxisLabelOffset = 0.007;
      opts.mYaxisTickLength = 0.015;
      opts.mYlog = kTRUE;
      
      opts.mMarkerStyle = 20;
      opts.mMarkerColor = kBlack;
      opts.mMarkerSize = 1.4;
      
      opts.mLineStyle = 1;
      opts.mLineColor = kBlack;
      opts.mLineWidth = 2;
      
      opts.mPdfName = TString(Form("%s_Branch%d", TString("YminusYExtrapolated_BeforeMomentumBalanceCut").Data(), br));
      
      opts.mLegendTextSize = 0.7 * 0.05/*opts.mXaxisTitleSize*/;
     
//       if( coor == CepUtil::X ){
        opts.mNColumns = 1;
        opts.mLegendX = 0.21;
        opts.mLegendXwidth = 0.25;
        opts.mLegendY = 0.24;
        opts.mLegendYwidth = 0.4;
        mSpecialLegendMarginFactor = 1.0;
//       } else{
//         opts.mNColumns = 1;
//         opts.mLegendX = /*(br==0 || br==2 || br==4 || br==6) ? 0.73 :*/ 0.19;
//         opts.mLegendXwidth = 0.25*0.8;
//         opts.mLegendY = 0.4;
//         opts.mLegendYwidth = 0.4*0.6;
//         mSpecialLegendMarginFactor = 1.0;
//       }
      
      mInverseStackDrawing = true;
      
      const double labelShiftDown = 0.04;
      double offs = 0.06;
      
    TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.26]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+#pi^{+}#pi^{-}+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.7 * opts.mXaxisTitleSize);
    opts.mTextToDraw.push_back( lReaction );
//     TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //opts.mTextToDraw.push_back( lATLAS );
    
    TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", minInDetTrackPt );
    TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
    TString brKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
    TLatex lCutsA(.16+offs,.84-labelShiftDown, "#pi^{+}, #pi^{-}:"); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
    TLatex lCutsB(.44+offs,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
    TLatex lCuts1(.26+offs,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
    TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
    TLatex lCuts12(.26+offs,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
    TLatex lCuts11(.26+offs,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
    //     TLatex lCuts2(.54,.84-labelShiftDown, brKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
    TLatex lCuts3(.54+offs,.84-labelShiftDown, brKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
    //     TLatex lCuts4(.54,.75-labelShiftDown, brKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );   

//       
      
      TLatex lRpStr( /*(coor==CepUtil::Y) ? ((br==0 || br==2 || br==4 || br==6) ? 0.23 : 0.8 ) :*/ 0.68 , .66, Form("Branch %d", br)); lRpStr.SetNDC(); lRpStr.SetTextFont(42);  lRpStr.SetTextSize(1.2 * 0.05); opts.mTextToDraw.push_back( lRpStr );
      
      drawFinalResult( referenceHist, opts );
      
      
      //linear y scale
      opts.mPdfName = TString(Form("Linear_%s_%d", TString("YminusYExtrapolated_BeforeMomentumBalanceCut").Data(), br));
      opts.mYlog = kFALSE;
      opts.mYmin = 0.0;
      opts.mYmax = 1.65 * referenceHist->GetMaximum();
      
//       mRatioYTitleOffsetFactor = 0.82;
      
      drawFinalResult( referenceHist, opts );
  }
  
  mRatioYTitleOffsetFactor = 1.0;
  
  
  
  
  
  
  
  for(int br=0; br<CepUtil::nBranches; ++br){
      
    referenceHist = mh_YminusYExtr[DATA][br];
      referenceHist->Rebin(4);
      referenceHist->Scale(normalizationFactor[DATA] );
      
      DrawingOptions opts;
      
      for(unsigned int fileId=DATA+1; fileId<nFiles; ++fileId){
        
        TH1F *mcHist = mh_YminusYExtr[fileId][br];
        mcHist->Rebin(4);
        mcHist->Scale( normalizationFactor[fileId] );
//         mcHist->Scale( mcHist->GetBinWidth( mcHist->FindBin(0.) ), "width");
        
        mcHist->SetLineColor( kBlack );
        mcHist->SetLineWidth( 2 );
        mcHist->SetFillColor( histColor[fileId] );
        
        opts.mHistTH1F.push_back( mcHist );
        opts.mHistTH1F_DrawingOpt.push_back( "HIST" );
        
        opts.mObjForLegend.push_back( mcHist );
        opts.mDesriptionForLegend.push_back( legendText[fileId] );
        opts.mDrawTypeForLegend.push_back( "f" );
      }
      
      opts.mMaxDigits = 5;
      
      opts.mCanvasWidth = 900;
      opts.mCanvasHeight = 800;
      
      opts.mLeftMargin = 0.17;
      opts.mRightMargin = 0.025;
      opts.mTopMargin = 0.075;
      opts.mBottomMargin = 0.14;
      
      //x axis
      opts.mXmin = -1;
      opts.mXmax = 1;
      opts.mXaxisTitle = TString("y_{far} - y_{extr} [mm]");
      opts.mXaxisTitleSize = 0.05;
      opts.mXaxisTitleOffset = 1.04;
      opts.mXaxisLabelSize = 0.05;
      opts.mXaxisLabelOffset = 0.007;
      opts.mXaxisTickLength = 0.015;
      
      //y axis
      opts.mYmin = 5;
      opts.mYmax = 10000 * referenceHist->GetMaximum();
      opts.mYaxisTitle = TString(Form("Num. of track points / %g mm#kern[-4.0]{ }",  referenceHist->GetBinWidth( referenceHist->FindBin(0.) )  ));
      opts.mYaxisTitleSize = 0.05;
      opts.mYaxisTitleOffset = 1.25;
      opts.mYaxisLabelSize = 0.05;
      opts.mYaxisLabelOffset = 0.007;
      opts.mYaxisTickLength = 0.015;
      opts.mYlog = kTRUE;
      
      opts.mMarkerStyle = 20;
      opts.mMarkerColor = kBlack;
      opts.mMarkerSize = 1.4;
      
      opts.mLineStyle = 1;
      opts.mLineColor = kBlack;
      opts.mLineWidth = 2;
      
      opts.mPdfName = TString(Form("%s_Branch%d", TString("YminusYExtrapolated").Data(), br));
      
      opts.mLegendTextSize = 0.7 * 0.05/*opts.mXaxisTitleSize*/;
     
//       if( coor == CepUtil::X ){
        opts.mNColumns = 1;
        opts.mLegendX = 0.21;
        opts.mLegendXwidth = 0.25;
        opts.mLegendY = 0.24;
        opts.mLegendYwidth = 0.4;
        mSpecialLegendMarginFactor = 1.0;
//       } else{
//         opts.mNColumns = 1;
//         opts.mLegendX = /*(br==0 || br==2 || br==4 || br==6) ? 0.73 :*/ 0.19;
//         opts.mLegendXwidth = 0.25*0.8;
//         opts.mLegendY = 0.4;
//         opts.mLegendYwidth = 0.4*0.6;
//         mSpecialLegendMarginFactor = 1.0;
//       }
      
      mInverseStackDrawing = true;
      
      const double labelShiftDown = 0.04;
      double offs = 0.06;
      
    TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.26]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+#pi^{+}#pi^{-}+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.7 * opts.mXaxisTitleSize);
    opts.mTextToDraw.push_back( lReaction );
//     TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //opts.mTextToDraw.push_back( lATLAS );
    
    TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", minInDetTrackPt );
    TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
    TString brKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
    TLatex lCutsA(.16+offs,.84-labelShiftDown, "#pi^{+}, #pi^{-}:"); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
    TLatex lCutsB(.44+offs,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
    TLatex lCuts1(.26+offs,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
    TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
    TLatex lCuts12(.26+offs,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
    TLatex lCuts11(.26+offs,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
    //     TLatex lCuts2(.54,.84-labelShiftDown, brKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
    TLatex lCuts3(.54+offs,.84-labelShiftDown, brKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
    //     TLatex lCuts4(.54,.75-labelShiftDown, brKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );   

//       
      
      TLatex lRpStr( /*(coor==CepUtil::Y) ? ((br==0 || br==2 || br==4 || br==6) ? 0.23 : 0.8 ) :*/ 0.68 , .66, Form("Branch %d", br)); lRpStr.SetNDC(); lRpStr.SetTextFont(42);  lRpStr.SetTextSize(1.2 * 0.05); opts.mTextToDraw.push_back( lRpStr );
      
      drawFinalResult( referenceHist, opts );
      
      
      //linear y scale
      opts.mPdfName = TString(Form("Linear_%s_%d", TString("YminusYExtrapolated").Data(), br));
      opts.mYlog = kFALSE;
      opts.mYmin = 0.0;
      opts.mYmax = 1.65 * referenceHist->GetMaximum();
      
//       mRatioYTitleOffsetFactor = 0.82;
      
      drawFinalResult( referenceHist, opts );
  }
  
  mRatioYTitleOffsetFactor = 1.0;
  
  
  
  
  
  
  //BEGIN                                       d_0                                                        

  {
    referenceHist = mh_ExclusivePiPi_d0[DATA];
    referenceHist->Scale(referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) ), "width");
    
    totalData = new TH1F( *referenceHist );
    
    
    DrawingOptions opts;
    
    for(unsigned int fileId=DATA+1; fileId<nFiles; ++fileId){
      
      TH1F *mcHist = mh_ExclusivePiPi_d0[fileId];
      mcHist->Scale( normalizationFactor[fileId] );
      mcHist->Scale(mcHist->GetBinWidth( mcHist->FindBin(0.0001) ), "width");
      
      mcHist->SetLineColor( kBlack );
      mcHist->SetLineWidth( 2 );
      mcHist->SetFillColor( histColor[fileId] );
      
      totalMC[fileId] = new TH1F( *mcHist );
      
      opts.mHistTH1F.push_back( mcHist );
      opts.mHistTH1F_DrawingOpt.push_back( "HIST" );
      
      opts.mObjForLegend.push_back( mcHist );
      opts.mDesriptionForLegend.push_back( legendText[fileId] );
      opts.mDrawTypeForLegend.push_back( "f" );
      
    }

    
    
    opts.mCanvasWidth = 900;
    opts.mCanvasHeight = 800;
    
    opts.mLeftMargin = 0.12;
    opts.mRightMargin = 0.035;
    opts.mTopMargin = 0.06;
    opts.mBottomMargin = 0.12;
    
    //x axis
    opts.mXmin = -4;
    opts.mXmax = 4;
    opts.mXaxisTitle = TString("d_{0} [mm]");
    opts.mXaxisTitleSize = 0.05;
    opts.mXaxisTitleOffset = 1.04;
    opts.mXaxisLabelSize = 0.05;
    opts.mXaxisLabelOffset = 0.007;
    opts.mXaxisTickLength = 0.015;
    
    //y axis
    opts.mYmin = 0;
    opts.mYmax = 1.35 * referenceHist->GetMaximum();
    opts.mYaxisTitle = TString(Form("Number of events / %g mm", referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) )) );
    opts.mYaxisTitleSize = 0.05;
    opts.mYaxisTitleOffset = 1.25;
    opts.mYaxisLabelSize = 0.05;
    opts.mYaxisLabelOffset = 0.007;
    opts.mYaxisTickLength = 0.015;
    opts.mYlog = kFALSE;
    
    opts.mMarkerStyle = 20;
    opts.mMarkerColor = kBlack;
    opts.mMarkerSize = 1.4;
    opts.mLineStyle = 1;
    opts.mLineColor = kBlack;
    opts.mLineWidth = 2;
    
    opts.mPdfName = TString("d0");
    
    opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
    opts.mNColumns = 1;
    opts.mLegendX = true ? 0.2 : 0.4;
    opts.mLegendXwidth = 0.2;
    opts.mLegendY = 0.3;
    opts.mLegendYwidth = 0.14*2.2;
    mSpecialLegendMarginFactor = 1.2;

    {
      TLine *cutLine[2];
      TArrow *cutArrow[2];
      
      for(int i=0; i<2; ++i){
          cutLine[i] = new TLine( (i==0?1:-1)*1.5, opts.mYmin, (i==0?1:-1)*1.5, 0.15*opts.mYmax );
          cutLine[i]->SetLineWidth(4);
          cutLine[i]->SetLineColor(kRed);
          cutLine[i]->SetLineStyle(7);
          opts.mObjectToDraw.push_back( cutLine[i] );
          
          cutArrow[i] = new TArrow( (i==0?1:-1)*1.5, 0.1*opts.mYmax, (i==0?1:-1)*(1.5 - 0.8), 0.1*opts.mYmax, 0.04, "|>");
          cutArrow[i]->SetAngle(30);
          cutArrow[i]->SetLineWidth(4);
          cutArrow[i]->SetLineColor( kRed );
          cutArrow[i]->SetFillColor( kRed );
          opts.mObjectToDraw.push_back( cutArrow[i] );
      }
    }
    
    double labelShiftDown = 0.04;
    
    mInverseStackDrawing = true;
    
    TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.26]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+#pi^{+}#pi^{-}+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
    opts.mTextToDraw.push_back( lReaction );
    TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //opts.mTextToDraw.push_back( lATLAS );
    
    TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", minInDetTrackPt );
    TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
    TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
    TLatex lCutsA(.16,.84-labelShiftDown, "#pi^{+}, #pi^{-}:"); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
    TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
    TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
    TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
    TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
    TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
    //     TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
    TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
    //     TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );   
    
    //     TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
    //     TLatex lDeltaPhi(.25,.677, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
    
    drawFinalResult( referenceHist, opts );
    
//     TLatex lOppo(true ? 0.18 : .2, .67-labelShiftDown, "Opposite sign:"); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
//     TLatex lSame(true ? .65 : .236, .67-labelShiftDown, "Same sign:"); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
    
    {
      opts.mTopMargin -= 0.04;
      opts.mLegendY += 0.05;
      opts.mLegendX -= 0.04;
      opts.mLegendY2 = opts.mLegendY;
      mRatioFactor = 0.15;
      mRatioYmin = 0.62;
      mRatioYmax = 1.38;
      mRatioYTitleOffsetFactor = 0.5;
      mRatioYTitle = TString("#kern[-0.2]{Data/MC}");
      //log-y scale
      opts.mPdfName = TString("LogY_d0");
      opts.mYlog = kTRUE;
      opts.mYmin = 20 / 4;
      opts.mYmax = 100 * referenceHist->GetMaximum() / 4;
      opts.mObjectToDraw.clear(); 
      
      opts.mTextToDraw.clear();
      
      
      TLine *cutLine[2];
      TArrow *cutArrow[2];
      
      for(int i=0; i<2; ++i){
          cutLine[i] = new TLine( (i==0?1:-1)*1.5, opts.mYmin, (i==0?1:-1)*1.5, 0.001*opts.mYmax );
          cutLine[i]->SetLineWidth(4);
          cutLine[i]->SetLineColor(kRed);
          cutLine[i]->SetLineStyle(7);
          opts.mObjectToDraw.push_back( cutLine[i] );
          
          cutArrow[i] = new TArrow( (i==0?1:-1)*1.5, 0.0008*opts.mYmax, (i==0?1:-1)*(1.5 - 0.8), 0.0008*opts.mYmax, 0.04, "|>");
          cutArrow[i]->SetAngle(30);
          cutArrow[i]->SetLineWidth(4);
          cutArrow[i]->SetLineColor( kRed );
          cutArrow[i]->SetFillColor( kRed );
          opts.mObjectToDraw.push_back( cutArrow[i] );
      }
      
      labelShiftDown -= 0.04;
      
      TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.26]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+#pi^{+}#pi^{-}+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
      opts.mTextToDraw.push_back( lReaction );
      TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
      //opts.mTextToDraw.push_back( lATLAS );
      
      TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", minInDetTrackPt );
      TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
      TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
      TLatex lCutsA(.16,.84-labelShiftDown, "#pi^{+}, #pi^{-}:"); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
      TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
      TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
      TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
        TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
      TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
      //       TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
      TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
      //       TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );
      
      
//       TLatex lOppo(true ? 0.18 : .2, .67-labelShiftDown + 0.07, "Opposite sign:"); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
//       TLatex lSame(true ? .65 : .236, .67-labelShiftDown + 0.07, "Same sign:"); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
      
      
      //       TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
      //       TLatex lDeltaPhi(/*.25*/.16,.677 + 0.04 + 0.02, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
      
      drawFinalResult( referenceHist, opts/*, referenceHist_SameSign*/ );
      
      // back to default
      mRatioFactor = 0.25;
      mRatioYmin = 0.2;
      mRatioYmax = 1.8;
      mRatioYTitleOffsetFactor = 1.0;
      mRatioYTitle = TString("Data / MC");
    }
    
  }
  
  
  
  
  
  
  
  
  
  
  //BEGIN                                       d_0     4Pi                                                   

  {
    referenceHist = mh_Exclusive4Pi_d0[DATA];
    referenceHist->Scale(referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) ), "width");
    
    totalData = new TH1F( *referenceHist );
    
    
    DrawingOptions opts;
    
//     for(unsigned int fileId=DATA+1; fileId<nFiles; ++fileId){
//       
//       TH1F *mcHist = mh_Exclusive4Pi_d0[fileId];
//       mcHist->Scale( normalizationFactor[fileId] );
//       mcHist->Scale(mcHist->GetBinWidth( mcHist->FindBin(0.0001) ), "width");
//       
//       mcHist->SetLineColor( kBlack );
//       mcHist->SetLineWidth( 2 );
//       mcHist->SetFillColor( histColor[fileId] );
//       
//       totalMC[fileId] = new TH1F( *mcHist );
//       
//       opts.mHistTH1F.push_back( mcHist );
//       opts.mHistTH1F_DrawingOpt.push_back( "HIST" );
//       
//       opts.mObjForLegend.push_back( mcHist );
//       opts.mDesriptionForLegend.push_back( legendText[fileId] );
//       opts.mDrawTypeForLegend.push_back( "f" );
//       
//     }

    
    
    opts.mCanvasWidth = 900;
    opts.mCanvasHeight = 800;
    
    opts.mLeftMargin = 0.12;
    opts.mRightMargin = 0.035;
    opts.mTopMargin = 0.06;
    opts.mBottomMargin = 0.12;
    
    //x axis
    opts.mXmin = -4;
    opts.mXmax = 4;
    opts.mXaxisTitle = TString("d_{0} [mm]");
    opts.mXaxisTitleSize = 0.05;
    opts.mXaxisTitleOffset = 1.04;
    opts.mXaxisLabelSize = 0.05;
    opts.mXaxisLabelOffset = 0.007;
    opts.mXaxisTickLength = 0.015;
    
    //y axis
    opts.mYmin = 0;
    opts.mYmax = 1.35 * referenceHist->GetMaximum();
    opts.mYaxisTitle = TString(Form("Number of events / %g mm", referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) )) );
    opts.mYaxisTitleSize = 0.05;
    opts.mYaxisTitleOffset = 1.25;
    opts.mYaxisLabelSize = 0.05;
    opts.mYaxisLabelOffset = 0.007;
    opts.mYaxisTickLength = 0.015;
    opts.mYlog = kFALSE;
    
    opts.mMarkerStyle = 20;
    opts.mMarkerColor = kBlack;
    opts.mMarkerSize = 1.4;
    opts.mLineStyle = 1;
    opts.mLineColor = kBlack;
    opts.mLineWidth = 2;
    
    opts.mPdfName = TString("d0_4Pi");
    
    opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
    opts.mNColumns = 1;
    opts.mLegendX = true ? 0.2 : 0.4;
    opts.mLegendXwidth = 0.2;
    opts.mLegendY = 0.3;
    opts.mLegendYwidth = 0.14*2.2;
    mSpecialLegendMarginFactor = 1.2;

    {
      TLine *cutLine[2];
      TArrow *cutArrow[2];
      
      for(int i=0; i<2; ++i){
          cutLine[i] = new TLine( (i==0?1:-1)*1.5, opts.mYmin, (i==0?1:-1)*1.5, 0.15*opts.mYmax );
          cutLine[i]->SetLineWidth(4);
          cutLine[i]->SetLineColor(kRed);
          cutLine[i]->SetLineStyle(7);
          opts.mObjectToDraw.push_back( cutLine[i] );
          
          cutArrow[i] = new TArrow( (i==0?1:-1)*1.5, 0.1*opts.mYmax, (i==0?1:-1)*(1.5 - 0.8), 0.1*opts.mYmax, 0.04, "|>");
          cutArrow[i]->SetAngle(30);
          cutArrow[i]->SetLineWidth(4);
          cutArrow[i]->SetLineColor( kRed );
          cutArrow[i]->SetFillColor( kRed );
          opts.mObjectToDraw.push_back( cutArrow[i] );
      }
    }
    
    double labelShiftDown = 0.04;
    
    mInverseStackDrawing = true;
    
    TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.26]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+2#pi^{+}2#pi^{-}+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
    opts.mTextToDraw.push_back( lReaction );
    TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //opts.mTextToDraw.push_back( lATLAS );
    
    TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", minInDetTrackPt );
    TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
    TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
    TLatex lCutsA(.16,.84-labelShiftDown, "#pi^{+}, #pi^{-}:"); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
    TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
    TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
    TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
    TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
    TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
    //     TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
    TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
    //     TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );   
    
    //     TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
    //     TLatex lDeltaPhi(.25,.677, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
    
    drawFinalResult( referenceHist, opts );
    
//     TLatex lOppo(true ? 0.18 : .2, .67-labelShiftDown, "Opposite sign:"); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
//     TLatex lSame(true ? .65 : .236, .67-labelShiftDown, "Same sign:"); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
    
    {
      opts.mTopMargin -= 0.04;
      opts.mLegendY += 0.05;
      opts.mLegendX -= 0.04;
      opts.mLegendY2 = opts.mLegendY;
      mRatioFactor = 0.15;
      mRatioYmin = 0.62;
      mRatioYmax = 1.38;
      mRatioYTitleOffsetFactor = 0.5;
      mRatioYTitle = TString("#kern[-0.2]{Data/MC}");
      //log-y scale
      opts.mPdfName = TString("LogY_d0_4Pi");
      opts.mYlog = kTRUE;
      opts.mYmin = 20 / 4;
      opts.mYmax = 100 * referenceHist->GetMaximum() / 4;
      opts.mObjectToDraw.clear(); 
      
      opts.mTextToDraw.clear();
      
      
      TLine *cutLine[2];
      TArrow *cutArrow[2];
      
      for(int i=0; i<2; ++i){
          cutLine[i] = new TLine( (i==0?1:-1)*1.5, opts.mYmin, (i==0?1:-1)*1.5, 0.001*opts.mYmax );
          cutLine[i]->SetLineWidth(4);
          cutLine[i]->SetLineColor(kRed);
          cutLine[i]->SetLineStyle(7);
          opts.mObjectToDraw.push_back( cutLine[i] );
          
          cutArrow[i] = new TArrow( (i==0?1:-1)*1.5, 0.0008*opts.mYmax, (i==0?1:-1)*(1.5 - 0.8), 0.0008*opts.mYmax, 0.04, "|>");
          cutArrow[i]->SetAngle(30);
          cutArrow[i]->SetLineWidth(4);
          cutArrow[i]->SetLineColor( kRed );
          cutArrow[i]->SetFillColor( kRed );
          opts.mObjectToDraw.push_back( cutArrow[i] );
      }
      
      labelShiftDown -= 0.04;
      
      TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.26]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+#pi^{+}#pi^{-}+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
      opts.mTextToDraw.push_back( lReaction );
      TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
      //opts.mTextToDraw.push_back( lATLAS );
      
      TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", minInDetTrackPt );
      TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
      TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
      TLatex lCutsA(.16,.84-labelShiftDown, "#pi^{+}, #pi^{-}:"); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
      TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
      TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
      TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
        TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
      TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
      //       TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
      TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
      //       TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );
      
      
//       TLatex lOppo(true ? 0.18 : .2, .67-labelShiftDown + 0.07, "Opposite sign:"); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
//       TLatex lSame(true ? .65 : .236, .67-labelShiftDown + 0.07, "Same sign:"); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
      
      
      //       TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
      //       TLatex lDeltaPhi(/*.25*/.16,.677 + 0.04 + 0.02, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
      
      drawFinalResult( referenceHist, opts/*, referenceHist_SameSign*/ );
      
      // back to default
      mRatioFactor = 0.25;
      mRatioYmin = 0.2;
      mRatioYmax = 1.8;
      mRatioYTitleOffsetFactor = 1.0;
      mRatioYTitle = TString("Data / MC");
    }
    
  }
  
  
  
  
  
  
  
  
  
  
  
  //BEGIN                                       z_0 * sin theta                                                      

  {
    referenceHist = mh_ExclusivePiPi_z0SinTheta[DATA];
    referenceHist->Scale(referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) ), "width");
    
    totalData = new TH1F( *referenceHist );
    
    
    DrawingOptions opts;
    
    for(unsigned int fileId=DATA+1; fileId<nFiles; ++fileId){
      
      TH1F *mcHist = mh_ExclusivePiPi_z0SinTheta[fileId];
      mcHist->Scale( normalizationFactor[fileId] );
      mcHist->Scale(mcHist->GetBinWidth( mcHist->FindBin(0.0001) ), "width");
      
      mcHist->SetLineColor( kBlack );
      mcHist->SetLineWidth( 2 );
      mcHist->SetFillColor( histColor[fileId] );
      
      totalMC[fileId] = new TH1F( *mcHist );
      
      opts.mHistTH1F.push_back( mcHist );
      opts.mHistTH1F_DrawingOpt.push_back( "HIST" );
      
      opts.mObjForLegend.push_back( mcHist );
      opts.mDesriptionForLegend.push_back( legendText[fileId] );
      opts.mDrawTypeForLegend.push_back( "f" );
      
    }

    
    
    opts.mCanvasWidth = 900;
    opts.mCanvasHeight = 800;
    
    opts.mLeftMargin = 0.12;
    opts.mRightMargin = 0.035;
    opts.mTopMargin = 0.06;
    opts.mBottomMargin = 0.12;
    
    //x axis
    opts.mXmin = -6;
    opts.mXmax = 6;
    opts.mXaxisTitle = TString("z_{0} sin #theta [mm]");
    opts.mXaxisTitleSize = 0.05;
    opts.mXaxisTitleOffset = 1.04;
    opts.mXaxisLabelSize = 0.05;
    opts.mXaxisLabelOffset = 0.007;
    opts.mXaxisTickLength = 0.015;
    
    //y axis
    opts.mYmin = 0;
    opts.mYmax = 1.35 * referenceHist->GetMaximum();
    opts.mYaxisTitle = TString(Form("Number of events / %g mm", referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) )) );
    opts.mYaxisTitleSize = 0.05;
    opts.mYaxisTitleOffset = 1.25;
    opts.mYaxisLabelSize = 0.05;
    opts.mYaxisLabelOffset = 0.007;
    opts.mYaxisTickLength = 0.015;
    opts.mYlog = kFALSE;
    
    opts.mMarkerStyle = 20;
    opts.mMarkerColor = kBlack;
    opts.mMarkerSize = 1.4;
    opts.mLineStyle = 1;
    opts.mLineColor = kBlack;
    opts.mLineWidth = 2;
    
    opts.mPdfName = TString("z0SinTheta");
    
    opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
    opts.mNColumns = 1;
    opts.mLegendX = true ? 0.2 : 0.4;
    opts.mLegendXwidth = 0.2;
    opts.mLegendY = 0.3;
    opts.mLegendYwidth = 0.14*2.2;
    mSpecialLegendMarginFactor = 1.2;

    
    

    
    
    double labelShiftDown = 0.04;
    
    mInverseStackDrawing = true;
    
    TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.26]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+#pi^{+}#pi^{-}+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
    opts.mTextToDraw.push_back( lReaction );
    TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //opts.mTextToDraw.push_back( lATLAS );
    
    TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", minInDetTrackPt );
    TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
    TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
    TLatex lCutsA(.16,.84-labelShiftDown, "#pi^{+}, #pi^{-}:"); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
    TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
    TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
    TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
    TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
    TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
    //     TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
    TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
    //     TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );   
    
    //     TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
    //     TLatex lDeltaPhi(.25,.677, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
    
    drawFinalResult( referenceHist, opts );
    
//     TLatex lOppo(true ? 0.18 : .2, .67-labelShiftDown, "Opposite sign:"); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
//     TLatex lSame(true ? .65 : .236, .67-labelShiftDown, "Same sign:"); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
    
    {
      opts.mTopMargin -= 0.04;
      opts.mLegendY += 0.05;
      opts.mLegendX -= 0.04;
      opts.mLegendY2 = opts.mLegendY;
      mRatioFactor = 0.15;
      mRatioYmin = 0.62;
      mRatioYmax = 1.38;
      mRatioYTitleOffsetFactor = 0.5;
      mRatioYTitle = TString("#kern[-0.2]{Data/MC}");
      //log-y scale
      opts.mPdfName = TString("LogY_z0SinTheta");
      opts.mYlog = kTRUE;
      opts.mYmin = 20 / 4;
      opts.mYmax = 100 * referenceHist->GetMaximum() / 4;
      opts.mObjectToDraw.clear(); 
      
      opts.mTextToDraw.clear();
      
      labelShiftDown -= 0.04;
      
      
      TLine *cutLine[2];
      TArrow *cutArrow[2];
      
      for(int i=0; i<2; ++i){
          cutLine[i] = new TLine( (i==0?1:-1)*1.5, opts.mYmin, (i==0?1:-1)*1.5, 0.001*opts.mYmax );
          cutLine[i]->SetLineWidth(4);
          cutLine[i]->SetLineColor(kRed);
          cutLine[i]->SetLineStyle(7);
          opts.mObjectToDraw.push_back( cutLine[i] );
          
          cutArrow[i] = new TArrow( (i==0?1:-1)*1.5, 0.0008*opts.mYmax, (i==0?1:-1)*(1.5 - 0.8), 0.0008*opts.mYmax, 0.04, "|>");
          cutArrow[i]->SetAngle(30);
          cutArrow[i]->SetLineWidth(4);
          cutArrow[i]->SetLineColor( kRed );
          cutArrow[i]->SetFillColor( kRed );
          opts.mObjectToDraw.push_back( cutArrow[i] );
      }
      
      TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.26]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+#pi^{+}#pi^{-}+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
      opts.mTextToDraw.push_back( lReaction );
      TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
      //opts.mTextToDraw.push_back( lATLAS );
      
      TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", minInDetTrackPt );
      TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
      TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
      TLatex lCutsA(.16,.84-labelShiftDown, "#pi^{+}, #pi^{-}:"); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
      TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
      TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
      TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
        TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
      TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
      //       TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
      TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
      //       TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );
      
      
//       TLatex lOppo(true ? 0.18 : .2, .67-labelShiftDown + 0.07, "Opposite sign:"); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
//       TLatex lSame(true ? .65 : .236, .67-labelShiftDown + 0.07, "Same sign:"); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
      
      
      //       TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
      //       TLatex lDeltaPhi(/*.25*/.16,.677 + 0.04 + 0.02, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
      
      drawFinalResult( referenceHist, opts/*, referenceHist_SameSign*/ );
      
      // back to default
      mRatioFactor = 0.25;
      mRatioYmin = 0.2;
      mRatioYmax = 1.8;
      mRatioYTitleOffsetFactor = 1.0;
      mRatioYTitle = TString("Data / MC");
    }
    
  }
  
  
  
  
  //BEGIN                                       z_0 * sin theta          4Pi                                            

  {
    referenceHist = mh_Exclusive4Pi_z0SinTheta[DATA];
    referenceHist->Scale(referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) ), "width");
    
    totalData = new TH1F( *referenceHist );
    
    
    DrawingOptions opts;
    
//     for(unsigned int fileId=DATA+1; fileId<nFiles; ++fileId){
//       
//       TH1F *mcHist = mh_Exclusive4Pi_z0SinTheta[fileId];
//       mcHist->Scale( normalizationFactor[fileId] );
//       mcHist->Scale(mcHist->GetBinWidth( mcHist->FindBin(0.0001) ), "width");
//       
//       mcHist->SetLineColor( kBlack );
//       mcHist->SetLineWidth( 2 );
//       mcHist->SetFillColor( histColor[fileId] );
//       
//       totalMC[fileId] = new TH1F( *mcHist );
//       
//       opts.mHistTH1F.push_back( mcHist );
//       opts.mHistTH1F_DrawingOpt.push_back( "HIST" );
//       
//       opts.mObjForLegend.push_back( mcHist );
//       opts.mDesriptionForLegend.push_back( legendText[fileId] );
//       opts.mDrawTypeForLegend.push_back( "f" );
//       
//     }

    
    
    opts.mCanvasWidth = 900;
    opts.mCanvasHeight = 800;
    
    opts.mLeftMargin = 0.12;
    opts.mRightMargin = 0.035;
    opts.mTopMargin = 0.06;
    opts.mBottomMargin = 0.12;
    
    //x axis
    opts.mXmin = -6;
    opts.mXmax = 6;
    opts.mXaxisTitle = TString("z_{0} sin #theta [mm]");
    opts.mXaxisTitleSize = 0.05;
    opts.mXaxisTitleOffset = 1.04;
    opts.mXaxisLabelSize = 0.05;
    opts.mXaxisLabelOffset = 0.007;
    opts.mXaxisTickLength = 0.015;
    
    //y axis
    opts.mYmin = 0;
    opts.mYmax = 1.35 * referenceHist->GetMaximum();
    opts.mYaxisTitle = TString(Form("Number of events / %g mm", referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) )) );
    opts.mYaxisTitleSize = 0.05;
    opts.mYaxisTitleOffset = 1.25;
    opts.mYaxisLabelSize = 0.05;
    opts.mYaxisLabelOffset = 0.007;
    opts.mYaxisTickLength = 0.015;
    opts.mYlog = kFALSE;
    
    opts.mMarkerStyle = 20;
    opts.mMarkerColor = kBlack;
    opts.mMarkerSize = 1.4;
    opts.mLineStyle = 1;
    opts.mLineColor = kBlack;
    opts.mLineWidth = 2;
    
    opts.mPdfName = TString("z0SinTheta_4Pi");
    
    opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
    opts.mNColumns = 1;
    opts.mLegendX = true ? 0.2 : 0.4;
    opts.mLegendXwidth = 0.2;
    opts.mLegendY = 0.3;
    opts.mLegendYwidth = 0.14*2.2;
    mSpecialLegendMarginFactor = 1.2;

    
    

    
    
    double labelShiftDown = 0.04;
    
    mInverseStackDrawing = true;
    
    TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.26]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+2#pi^{+}2#pi^{-}+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
    opts.mTextToDraw.push_back( lReaction );
    TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //opts.mTextToDraw.push_back( lATLAS );
    
    TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", minInDetTrackPt );
    TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
    TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
    TLatex lCutsA(.16,.84-labelShiftDown, "#pi^{+}, #pi^{-}:"); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
    TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
    TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
    TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
    TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
    TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
    //     TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
    TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
    //     TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );   
    
    //     TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
    //     TLatex lDeltaPhi(.25,.677, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
    
    drawFinalResult( referenceHist, opts );
    
//     TLatex lOppo(true ? 0.18 : .2, .67-labelShiftDown, "Opposite sign:"); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
//     TLatex lSame(true ? .65 : .236, .67-labelShiftDown, "Same sign:"); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
    
    {
      opts.mTopMargin -= 0.04;
      opts.mLegendY += 0.05;
      opts.mLegendX -= 0.04;
      opts.mLegendY2 = opts.mLegendY;
      mRatioFactor = 0.15;
      mRatioYmin = 0.62;
      mRatioYmax = 1.38;
      mRatioYTitleOffsetFactor = 0.5;
      mRatioYTitle = TString("#kern[-0.2]{Data/MC}");
      //log-y scale
      opts.mPdfName = TString("LogY_z0SinTheta_4Pi");
      opts.mYlog = kTRUE;
      opts.mYmin = 20 / 4;
      opts.mYmax = 100 * referenceHist->GetMaximum() / 4;
      opts.mObjectToDraw.clear(); 
      
      opts.mTextToDraw.clear();
      
      labelShiftDown -= 0.04;
      
      
      TLine *cutLine[2];
      TArrow *cutArrow[2];
      
      for(int i=0; i<2; ++i){
          cutLine[i] = new TLine( (i==0?1:-1)*1.5, opts.mYmin, (i==0?1:-1)*1.5, 0.001*opts.mYmax );
          cutLine[i]->SetLineWidth(4);
          cutLine[i]->SetLineColor(kRed);
          cutLine[i]->SetLineStyle(7);
          opts.mObjectToDraw.push_back( cutLine[i] );
          
          cutArrow[i] = new TArrow( (i==0?1:-1)*1.5, 0.0008*opts.mYmax, (i==0?1:-1)*(1.5 - 0.8), 0.0008*opts.mYmax, 0.04, "|>");
          cutArrow[i]->SetAngle(30);
          cutArrow[i]->SetLineWidth(4);
          cutArrow[i]->SetLineColor( kRed );
          cutArrow[i]->SetFillColor( kRed );
          opts.mObjectToDraw.push_back( cutArrow[i] );
      }
      
      TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.26]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+2#pi^{+}2#pi^{-}+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
      opts.mTextToDraw.push_back( lReaction );
      TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
      //opts.mTextToDraw.push_back( lATLAS );
      
      TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", minInDetTrackPt );
      TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
      TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
      TLatex lCutsA(.16,.84-labelShiftDown, "#pi^{+}, #pi^{-}:"); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
      TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
      TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
      TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
        TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
      TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
      //       TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
      TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
      //       TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );
      
      
//       TLatex lOppo(true ? 0.18 : .2, .67-labelShiftDown + 0.07, "Opposite sign:"); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
//       TLatex lSame(true ? .65 : .236, .67-labelShiftDown + 0.07, "Same sign:"); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
      
      
      //       TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
      //       TLatex lDeltaPhi(/*.25*/.16,.677 + 0.04 + 0.02, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
      
      drawFinalResult( referenceHist, opts/*, referenceHist_SameSign*/ );
      
      // back to default
      mRatioFactor = 0.25;
      mRatioYmin = 0.2;
      mRatioYmax = 1.8;
      mRatioYTitleOffsetFactor = 1.0;
      mRatioYTitle = TString("Data / MC");
    }
    
  }
  
  
  
  
  
  
  
  
  
  
  
    
  //BEGIN                                       Delta z_0

  {
    referenceHist = mh_ExclusivePiPi_Delta_z0[DATA];
    referenceHist->Scale(referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) ), "width");
    
    totalData = new TH1F( *referenceHist );
    
    
    DrawingOptions opts;
    
    for(unsigned int fileId=DATA+1; fileId<nFiles; ++fileId){
      
      TH1F *mcHist = mh_ExclusivePiPi_Delta_z0[fileId];
      mcHist->Scale( normalizationFactor[fileId] );
      mcHist->Scale(mcHist->GetBinWidth( mcHist->FindBin(0.0001) ), "width");
      
      mcHist->SetLineColor( kBlack );
      mcHist->SetLineWidth( 2 );
      mcHist->SetFillColor( histColor[fileId] );
      
      totalMC[fileId] = new TH1F( *mcHist );
      
      opts.mHistTH1F.push_back( mcHist );
      opts.mHistTH1F_DrawingOpt.push_back( "HIST" );
      
      opts.mObjForLegend.push_back( mcHist );
      opts.mDesriptionForLegend.push_back( legendText[fileId] );
      opts.mDrawTypeForLegend.push_back( "f" );
      
    }

    
    
    opts.mCanvasWidth = 900;
    opts.mCanvasHeight = 800;
    
    opts.mLeftMargin = 0.12;
    opts.mRightMargin = 0.035;
    opts.mTopMargin = 0.06;
    opts.mBottomMargin = 0.12;
    
    //x axis
    opts.mXmin = 0;
    opts.mXmax = 30;
    opts.mXaxisTitle = TString("|#Deltaz_{0}| [mm]");
    opts.mXaxisTitleSize = 0.05;
    opts.mXaxisTitleOffset = 1.04;
    opts.mXaxisLabelSize = 0.05;
    opts.mXaxisLabelOffset = 0.007;
    opts.mXaxisTickLength = 0.015;
    
    //y axis
    opts.mYmin = 0;
    opts.mYmax = 1.35 * referenceHist->GetMaximum();
    opts.mYaxisTitle = TString(Form("Number of events / %g mm", referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) )) );
    opts.mYaxisTitleSize = 0.05;
    opts.mYaxisTitleOffset = 1.25;
    opts.mYaxisLabelSize = 0.05;
    opts.mYaxisLabelOffset = 0.007;
    opts.mYaxisTickLength = 0.015;
    opts.mYlog = kFALSE;
    
    opts.mMarkerStyle = 20;
    opts.mMarkerColor = kBlack;
    opts.mMarkerSize = 1.4;
    opts.mLineStyle = 1;
    opts.mLineColor = kBlack;
    opts.mLineWidth = 2;
    
    opts.mPdfName = TString("Delta_z0");
    
    opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
    opts.mNColumns = 1;
    opts.mLegendX = 0.6;
    opts.mLegendXwidth = 0.2;
    opts.mLegendY = 0.45;
    opts.mLegendYwidth = 0.14*2.2;
    mSpecialLegendMarginFactor = 1.2;

    
    

    
    
    double labelShiftDown = 0.04;
    
    mInverseStackDrawing = true;
    
    TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.26]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+#pi^{+}#pi^{-}+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
    opts.mTextToDraw.push_back( lReaction );
    TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //opts.mTextToDraw.push_back( lATLAS );
    
    TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", minInDetTrackPt );
    TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
    TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
    TLatex lCutsA(.16,.84-labelShiftDown, "#pi^{+}, #pi^{-}:"); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
    TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
    TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
    TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
    TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
    TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
    //     TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
    TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
    //     TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );   
    
    //     TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
    //     TLatex lDeltaPhi(.25,.677, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
    
    drawFinalResult( referenceHist, opts );
    
//     TLatex lOppo(true ? 0.18 : .2, .67-labelShiftDown, "Opposite sign:"); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
//     TLatex lSame(true ? .65 : .236, .67-labelShiftDown, "Same sign:"); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
    
    {
      opts.mTopMargin -= 0.04;
      opts.mLegendY += 0.05;
      opts.mLegendX -= 0.04;
      opts.mLegendY2 = opts.mLegendY;
      mRatioFactor = 0.15;
      mRatioYmin = 0.62;
      mRatioYmax = 1.38;
      mRatioYTitleOffsetFactor = 0.5;
      mRatioYTitle = TString("#kern[-0.2]{Data/MC}");
      //log-y scale
      opts.mPdfName = TString("LogY_Delta_z0");
      opts.mYlog = kTRUE;
      opts.mYmin = 20 / 4;
      opts.mYmax = 100 * referenceHist->GetMaximum() / 4;
      opts.mObjectToDraw.clear(); 
      
      opts.mTextToDraw.clear();
      
      labelShiftDown -= 0.04;
      
      
      TLine *cutLine[2];
      TArrow *cutArrow[2];
      
      for(int i=0; i<1/*2*/; ++i){
          cutLine[i] = new TLine( (i==0?1:-1)*20, opts.mYmin, (i==0?1:-1)*20, 0.00006*opts.mYmax );
          cutLine[i]->SetLineWidth(4);
          cutLine[i]->SetLineColor(kRed);
          cutLine[i]->SetLineStyle(7);
          opts.mObjectToDraw.push_back( cutLine[i] );
          
          cutArrow[i] = new TArrow( (i==0?1:-1)*20, 0.00006*opts.mYmax, (i==0?1:-1)*(20 - 3), 0.00006*opts.mYmax, 0.04, "|>");
          cutArrow[i]->SetAngle(30);
          cutArrow[i]->SetLineWidth(4);
          cutArrow[i]->SetLineColor( kRed );
          cutArrow[i]->SetFillColor( kRed );
          opts.mObjectToDraw.push_back( cutArrow[i] );
      }
      
      TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.26]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+#pi^{+}#pi^{-}+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
      opts.mTextToDraw.push_back( lReaction );
      TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
      //opts.mTextToDraw.push_back( lATLAS );
      
      TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", minInDetTrackPt );
      TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
      TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
      TLatex lCutsA(.16,.84-labelShiftDown, "#pi^{+}, #pi^{-}:"); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
      TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
      TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
      TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
        TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
      TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
      //       TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
      TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
      //       TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );
      
      
//       TLatex lOppo(true ? 0.18 : .2, .67-labelShiftDown + 0.07, "Opposite sign:"); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
//       TLatex lSame(true ? .65 : .236, .67-labelShiftDown + 0.07, "Same sign:"); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
      
      
      //       TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
      //       TLatex lDeltaPhi(/*.25*/.16,.677 + 0.04 + 0.02, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
      
      drawFinalResult( referenceHist, opts/*, referenceHist_SameSign*/ );
      
      // back to default
      mRatioFactor = 0.25;
      mRatioYmin = 0.2;
      mRatioYmax = 1.8;
      mRatioYTitleOffsetFactor = 1.0;
      mRatioYTitle = TString("Data / MC");
    }
    
  }
  
  
  
  //BEGIN                                       Delta z_0

  {
    referenceHist = mh_Exclusive4Pi_Delta_z0[DATA];
    referenceHist->Scale(referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) ), "width");
    
    totalData = new TH1F( *referenceHist );
    
    
    DrawingOptions opts;
    
//     for(unsigned int fileId=DATA+1; fileId<nFiles; ++fileId){
//       
//       TH1F *mcHist = mh_Exclusive4Pi_Delta_z0[fileId];
//       mcHist->Scale( normalizationFactor[fileId] );
//       mcHist->Scale(mcHist->GetBinWidth( mcHist->FindBin(0.0001) ), "width");
//       
//       mcHist->SetLineColor( kBlack );
//       mcHist->SetLineWidth( 2 );
//       mcHist->SetFillColor( histColor[fileId] );
//       
//       totalMC[fileId] = new TH1F( *mcHist );
//       
//       opts.mHistTH1F.push_back( mcHist );
//       opts.mHistTH1F_DrawingOpt.push_back( "HIST" );
//       
//       opts.mObjForLegend.push_back( mcHist );
//       opts.mDesriptionForLegend.push_back( legendText[fileId] );
//       opts.mDrawTypeForLegend.push_back( "f" );
//       
//     }

    
    
    opts.mCanvasWidth = 900;
    opts.mCanvasHeight = 800;
    
    opts.mLeftMargin = 0.12;
    opts.mRightMargin = 0.035;
    opts.mTopMargin = 0.06;
    opts.mBottomMargin = 0.12;
    
    //x axis
    opts.mXmin = 0;
    opts.mXmax = 30;
    opts.mXaxisTitle = TString("|#Deltaz_{0}| [mm]");
    opts.mXaxisTitleSize = 0.05;
    opts.mXaxisTitleOffset = 1.04;
    opts.mXaxisLabelSize = 0.05;
    opts.mXaxisLabelOffset = 0.007;
    opts.mXaxisTickLength = 0.015;
    
    //y axis
    opts.mYmin = 0;
    opts.mYmax = 1.35 * referenceHist->GetMaximum();
    opts.mYaxisTitle = TString(Form("Number of events / %g mm", referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) )) );
    opts.mYaxisTitleSize = 0.05;
    opts.mYaxisTitleOffset = 1.25;
    opts.mYaxisLabelSize = 0.05;
    opts.mYaxisLabelOffset = 0.007;
    opts.mYaxisTickLength = 0.015;
    opts.mYlog = kFALSE;
    
    opts.mMarkerStyle = 20;
    opts.mMarkerColor = kBlack;
    opts.mMarkerSize = 1.4;
    opts.mLineStyle = 1;
    opts.mLineColor = kBlack;
    opts.mLineWidth = 2;
    
    opts.mPdfName = TString("Delta_z0_4Pi");
    
    opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
    opts.mNColumns = 1;
    opts.mLegendX = 0.6;
    opts.mLegendXwidth = 0.2;
    opts.mLegendY = 0.45;
    opts.mLegendYwidth = 0.14*2.2;
    mSpecialLegendMarginFactor = 1.2;

    
    

    
    
    double labelShiftDown = 0.04;
    
    mInverseStackDrawing = true;
    
    TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.26]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+#pi^{+}#pi^{-}+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
    opts.mTextToDraw.push_back( lReaction );
    TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //opts.mTextToDraw.push_back( lATLAS );
    
    TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", minInDetTrackPt );
    TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
    TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
    TLatex lCutsA(.16,.84-labelShiftDown, "#pi^{+}, #pi^{-}:"); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
    TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
    TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
    TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
    TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
    TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
    //     TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
    TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
    //     TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );   
    
    //     TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
    //     TLatex lDeltaPhi(.25,.677, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
    
    drawFinalResult( referenceHist, opts );
    
//     TLatex lOppo(true ? 0.18 : .2, .67-labelShiftDown, "Opposite sign:"); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
//     TLatex lSame(true ? .65 : .236, .67-labelShiftDown, "Same sign:"); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
    
    {
      opts.mTopMargin -= 0.04;
      opts.mLegendY += 0.05;
      opts.mLegendX -= 0.04;
      opts.mLegendY2 = opts.mLegendY;
      mRatioFactor = 0.15;
      mRatioYmin = 0.62;
      mRatioYmax = 1.38;
      mRatioYTitleOffsetFactor = 0.5;
      mRatioYTitle = TString("#kern[-0.2]{Data/MC}");
      //log-y scale
      opts.mPdfName = TString("LogY_Delta_z0_4Pi");
      opts.mYlog = kTRUE;
      opts.mYmin = 20 / 4;
      opts.mYmax = 100 * referenceHist->GetMaximum() / 4;
      opts.mObjectToDraw.clear(); 
      
      opts.mTextToDraw.clear();
      
      labelShiftDown -= 0.04;
      
      
      TLine *cutLine[2];
      TArrow *cutArrow[2];
      
      for(int i=0; i<1/*2*/; ++i){
          cutLine[i] = new TLine( (i==0?1:-1)*20, opts.mYmin, (i==0?1:-1)*20, 0.00006*opts.mYmax );
          cutLine[i]->SetLineWidth(4);
          cutLine[i]->SetLineColor(kRed);
          cutLine[i]->SetLineStyle(7);
          opts.mObjectToDraw.push_back( cutLine[i] );
          
          cutArrow[i] = new TArrow( (i==0?1:-1)*20, 0.00006*opts.mYmax, (i==0?1:-1)*(20 - 3), 0.00006*opts.mYmax, 0.04, "|>");
          cutArrow[i]->SetAngle(30);
          cutArrow[i]->SetLineWidth(4);
          cutArrow[i]->SetLineColor( kRed );
          cutArrow[i]->SetFillColor( kRed );
          opts.mObjectToDraw.push_back( cutArrow[i] );
      }
      
      TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.26]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+#pi^{+}#pi^{-}+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
      opts.mTextToDraw.push_back( lReaction );
      TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
      //opts.mTextToDraw.push_back( lATLAS );
      
      TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", minInDetTrackPt );
      TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
      TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
      TLatex lCutsA(.16,.84-labelShiftDown, "#pi^{+}, #pi^{-}:"); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
      TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
      TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
      TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
        TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
      TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
      //       TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
      TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
      //       TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );
      
      
//       TLatex lOppo(true ? 0.18 : .2, .67-labelShiftDown + 0.07, "Opposite sign:"); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
//       TLatex lSame(true ? .65 : .236, .67-labelShiftDown + 0.07, "Same sign:"); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
      
      
      //       TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
      //       TLatex lDeltaPhi(/*.25*/.16,.677 + 0.04 + 0.02, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
      
      drawFinalResult( referenceHist, opts/*, referenceHist_SameSign*/ );
      
      // back to default
      mRatioFactor = 0.25;
      mRatioYmin = 0.2;
      mRatioYmax = 1.8;
      mRatioYTitleOffsetFactor = 1.0;
      mRatioYTitle = TString("Data / MC");
    }
    
  }
  
  
}





TH1F* DataVsMC::rebinAndGetTH1F(const TH1* hIn, const std::vector<double> & binningVec, bool copyStyle) const{
  TH1F *hOut;
  TString newName = TString(hIn->GetName())+TString(Form("_rebinned_copyNr_%d", static_cast<int>(1000000*gRandom->Uniform())));
  if(!copyStyle)
    hOut = new TH1F( newName, TString(hIn->GetTitle())+TString(", rebinned"), binningVec.size()-1, &(binningVec[0]) );
  else{
    hOut = dynamic_cast<TH1F*>( hIn->Clone(newName.Data()) );
    hOut->SetBins( binningVec.size()-1, &(binningVec[0]) );
  }
  for(int i=0; i<=hIn->GetNbinsX()+1; ++i){
    int nEntries = hIn->GetBinContent(i);
    for(int k=0; k<nEntries; ++k)
      hOut->Fill( hIn->GetXaxis()->GetBinCenter(i) );
  }
  hOut->SetDirectory(0);
  return hOut;
}


TH1F* DataVsMC::TH1F_from_TH1(const TH1* hIn) const{
    std::vector<float> binningVec = getBinsVectorF(hIn->GetXaxis());
    TH1F *hOut = new TH1F( TString(hIn->GetName())+TString(Form("_TH1FfromTH1_copyNr_%d", static_cast<int>(1000000*gRandom->Uniform()))), TString(hIn->GetTitle())+TString(", from TH1s"), binningVec.size()-1, &(binningVec[0]) );
    for(int i=0; i<=hIn->GetNbinsX()+1; ++i){
        int nEntries = hIn->GetBinContent(i);
        for(int k=0; k<nEntries; ++k)
            hOut->Fill( hIn->GetXaxis()->GetBinCenter(i) );
    }
    return hOut;
}






void DataVsMC::drawFinalResult(TH1F* hMainInput, DrawingOptions & opt, TH1F* hMainInput_2){
  
  TH1F *hMain = new TH1F( *hMainInput );
  hMain->SetName("FinalResult_"+TString(hMain->GetName()));
  
  TH1F *hMain_2 = nullptr;
  if(hMainInput_2){
    hMain_2 = new TH1F( *hMainInput_2 );
    hMain_2->SetName("FinalResult_"+TString(hMain_2->GetName()));
  }
  
  TGaxis::SetMaxDigits(opt.mMaxDigits);
  
  TCanvas *c = new TCanvas("cccccc", "cccccc", opt.mCanvasWidth, opt.mCanvasHeight);
  //   c->SetFrameFillStyle(0);
  //   c->SetFrameLineWidth(2);
  //   c->SetFillColor(-1);
  c->SetLeftMargin(opt.mLeftMargin);
  c->SetRightMargin(opt.mRightMargin);
  c->SetTopMargin(opt.mTopMargin);
  c->SetBottomMargin(opt.mBottomMargin);
  
  c->SetLogy(opt.mYlog);
  
  //   systematicErrorsGraph->SetFillColor( 18 );
  //   //systematicErrorsGraph->SetFillStyle(/*3013*//*3345*/3005);
  //   systematicErrorsGraph->SetLineWidth(0);
  //   systematicErrorsGraph->SetLineColor(kWhite);
  
  if(opt.mScale){
    hMain->Scale( opt.mScalingFactor );
  }
  
  hMain->SetMarkerStyle(opt.mMarkerStyle);
  hMain->SetMarkerSize(opt.mMarkerSize);
  hMain->SetMarkerColor(opt.mMarkerColor);
  hMain->SetLineStyle(opt.mLineStyle);
  hMain->SetLineColor(opt.mLineColor);
  hMain->SetLineWidth(opt.mLineWidth);
  
  if( hMain_2 ){
    hMain_2->SetMarkerStyle(opt.mMarkerStyle2);
    hMain_2->SetMarkerSize(opt.mMarkerSize2);
    hMain_2->SetMarkerColor(opt.mMarkerColor2);
    hMain_2->SetLineStyle(opt.mLineStyle2);
    hMain_2->SetLineColor(opt.mLineColor2);
    hMain_2->SetLineWidth(opt.mLineWidth2);
  }
  
  hMain->GetXaxis()->SetTitle(opt.mXaxisTitle);
  hMain->GetXaxis()->SetTitleSize(opt.mXaxisTitleSize);
  hMain->GetXaxis()->SetTitleOffset(opt.mXaxisTitleOffset);
  hMain->GetXaxis()->SetLabelSize(opt.mXaxisLabelSize);
  hMain->GetXaxis()->SetLabelOffset(opt.mXaxisLabelOffset);
  hMain->GetXaxis()->SetTickLength(opt.mXaxisTickLength);
  hMain->GetXaxis()->SetRangeUser(opt.mXmin, opt.mXmax);
  
  if( opt.mSetDivisionsX )
    hMain->GetXaxis()->SetNdivisions( opt.mXnDivisionA, opt.mXnDivisionB, opt.mXnDivisionC, !opt.mForceDivisionsX );
  
  hMain->GetYaxis()->SetTitleSize(opt.mYaxisTitleSize);
  hMain->GetYaxis()->SetTitle(opt.mYaxisTitle);
  hMain->GetYaxis()->SetTitleOffset(opt.mYaxisTitleOffset);
  hMain->GetYaxis()->SetLabelSize(opt.mYaxisLabelSize);
  hMain->GetYaxis()->SetLabelOffset(opt.mYaxisLabelOffset);
  hMain->GetYaxis()->SetTickLength(opt.mYaxisTickLength);
  hMain->GetYaxis()->SetRangeUser(opt.mYmin, (opt.mScale ? opt.mScalingFactor : 1.0) * opt.mYmax);
  
  if( opt.mSetDivisionsY )
    hMain->GetYaxis()->SetNdivisions( opt.mYnDivisionA, opt.mYnDivisionB, opt.mYnDivisionC, !opt.mForceDivisionsY );
  
  hMain->Draw(mSpecialDataDrawing ? "HIST E" : "PE");
  if(hMain_2){
    hMain_2->GetXaxis()->SetRangeUser(opt.mXmin, opt.mXmax);
    hMain_2->Draw(mSpecialDataDrawing ? "HIST E SAME" : "PE SAME");
  }
  
  
  const double systErrorBoxWidth = (opt.mMarkerSize * 8.0  * opt.mSystErrorBoxWidthAdjustmentFactor / opt.mCanvasWidth*( 1.0 - opt.mLeftMargin - opt.mRightMargin )) * (hMain->GetXaxis()->GetXmax() - hMain->GetXaxis()->GetXmin());
  
  
  TBox *typicalBox = nullptr;
//   if( opt.mXpositionSystErrorBox.size() > 0 ){
//     for(unsigned int i=0; i<opt.mXpositionSystErrorBox.size(); ++i){
//       int binNumber = hMain->GetXaxis()->FindBin( opt.mXpositionSystErrorBox[i] );
//       if( !(hMain->GetBinContent(binNumber)>0) )
//         continue;
//       
//       double xMinSystErrorBox = hMain->GetXaxis()->GetBinCenter(binNumber) - systErrorBoxWidth/2.;
//       double xMaxSystErrorBox = hMain->GetXaxis()->GetBinCenter(binNumber) + systErrorBoxWidth/2.;
//       double yMinSystErrorBox = hMain->GetBinContent(binNumber) * (1.0 - sqrt(pow(syst.mhSystErrorDown->GetBinContent(binNumber),2) + pow(mParams->luminosityUncertainty()*1.0e5,2)));
//       double yMaxSystErrorBox = hMain->GetBinContent(binNumber) * (1.0 + sqrt(pow(syst.mhSystErrorUp->GetBinContent(binNumber),2) + pow(mParams->luminosityUncertainty()*1.0e5,2)));
//       TBox *b = new TBox( xMinSystErrorBox, yMinSystErrorBox, xMaxSystErrorBox, yMaxSystErrorBox );
//       b->SetFillColor(opt.mSystErrorTotalBoxColor);
//       b->SetLineWidth(0);
//       b->Draw("l");
//       
//       yMinSystErrorBox = hMain->GetBinContent(binNumber) * (1.0 - syst.mhSystErrorDown->GetBinContent(binNumber));
//       yMaxSystErrorBox = hMain->GetBinContent(binNumber) * (1.0 + syst.mhSystErrorUp->GetBinContent(binNumber));
//       b = new TBox( xMinSystErrorBox, yMinSystErrorBox, xMaxSystErrorBox, yMaxSystErrorBox );
//       b->SetFillColor(opt.mSystErrorBoxColor);
//       b->SetLineWidth(0);
//       b->Draw("l");
//       
//       if( !typicalBox ){
//         typicalBox = new TBox( xMinSystErrorBox, yMinSystErrorBox, xMaxSystErrorBox, yMaxSystErrorBox );
//         typicalBox->SetFillColor(opt.mSystErrorTotalBoxColor);
//         typicalBox->SetLineColor(opt.mSystErrorBoxColor);
//         typicalBox->SetLineWidth(8);
//       }
//     }
//   }
  
  
  
  if( opt.mHistTH1F.size()>0 ){
    THStack *hs = new THStack( Form("hs_%d", static_cast<int>(gRandom->Uniform()*100000)), "THStack title" );
    
    if( mInverseStackDrawing ){
      for(int i=opt.mHistTH1F.size()-1; i>=0; --i){
        if(opt.mScale){
          opt.mHistTH1F[i]->Scale( opt.mScalingFactor );
        }
        opt.mHistTH1F[i]->GetXaxis()->SetRangeUser(opt.mXmin, opt.mXmax);
        hs->Add( opt.mHistTH1F[i], opt.mHistTH1F_DrawingOpt[i] );
      }
    } else{
      for(unsigned int i=0; i<opt.mHistTH1F.size(); ++i){
        if(opt.mScale){
          opt.mHistTH1F[i]->Scale( opt.mScalingFactor );
        }
        opt.mHistTH1F[i]->GetXaxis()->SetRangeUser(opt.mXmin, opt.mXmax);
        hs->Add( opt.mHistTH1F[i], opt.mHistTH1F_DrawingOpt[i] );
      }
    }
    
    hs->Draw("HIST SAME");
    
    
    
    
    TH1F* histMC = nullptr;
    if(opt.mHistTH1F.size() > 0){
      histMC = new TH1F(*opt.mHistTH1F[0]);
      for(unsigned int fileId=1; fileId<opt.mHistTH1F.size(); ++fileId) 
        histMC->Add( opt.mHistTH1F[fileId] );
    }
    
    TGraphAsymmErrors *graphErr = new TGraphAsymmErrors( histMC );
    graphErr->SetFillStyle( 3005 );
    graphErr->SetFillColor( kBlack );
    graphErr->Draw("02");
  }


  std::vector<TH1F*> hToDrawVec;
  if( hMain_2 && opt.mHistTH1F2.size()>0 ){
    TH1F *stackedUpTo_i = nullptr;
    for(int i=opt.mHistTH1F2.size()-1; i>=0; --i){
      if(opt.mScale){
        opt.mHistTH1F2[i]->Scale( opt.mScalingFactor );
      }
      TH1F *stackedUpTo_i_tmp = new TH1F( *opt.mHistTH1F2[i] );
      stackedUpTo_i_tmp->SetName( Form("stackedUpTo_%d", i) );
      if(i<(static_cast<int>(opt.mHistTH1F2.size())-1))
        stackedUpTo_i_tmp->Add( stackedUpTo_i );
      hToDrawVec.push_back( stackedUpTo_i_tmp );
      stackedUpTo_i = stackedUpTo_i_tmp;
    }
    
    for(int j=hToDrawVec.size()-1; j>=0; --j)
      hToDrawVec[j]->Draw("HIST SAME");
  }
  
  
  TLegend *legend = new TLegend( opt.mLegendX, opt.mLegendY, opt.mLegendX+opt.mLegendXwidth, opt.mLegendY+opt.mLegendYwidth );
  legend->SetNColumns(opt.mNColumns);
  legend->SetBorderSize(0);
  if( mBkgdSubtractionDemonstration ){
    legend->SetFillStyle(1001);
    legend->SetFillColorAlpha(kWhite, 0.6);
  } else
    legend->SetFillStyle(0);
  legend->SetTextSize(opt.mLegendTextSize);
  legend->AddEntry(hMain, mSpecialDataLegendDesctiption==TString("") ? "Data" : mSpecialDataLegendDesctiption, mSpecialDataDrawing ? "f" : "pel");
  //   legend->AddEntry(hMain, "Stat. uncertainty", "el");
  if( mSpecialDataDrawing2 && hMain_2 )
    legend->AddEntry(hMain_2, "Data (non-excl. bkgd)", mSpecialDataDrawing ? "f" : "pel");
  if(typicalBox)
    legend->AddEntry(typicalBox, "Syst. uncertainty", "fl");
  for(unsigned int i=0; i<opt.mObjForLegend.size(); ++i)
    legend->AddEntry( opt.mObjForLegend[i], opt.mDesriptionForLegend[i], opt.mDrawTypeForLegend[i]);
  legend->SetMargin(mSpecialLegendMarginFactor * 0.3 * opt.mLegendXwidth * legend->GetNRows() / legend->GetNColumns() );
  legend->Draw();
  
  TLegend *legend2 = nullptr;
  if( hMain_2 && !mSpecialDataDrawing2 ){
    legend2 = new TLegend( opt.mLegendX2, opt.mLegendY2, opt.mLegendX2+opt.mLegendXwidth2, opt.mLegendY2+opt.mLegendYwidth2 );
    legend2->SetNColumns(opt.mNColumns);
    legend2->SetBorderSize(0);
    legend2->SetFillStyle(0);
    legend2->SetTextSize(opt.mLegendTextSize2);
    legend2->AddEntry(hMain_2, "Data", "pel");
    //   legend2->AddEntry(hMain, "Stat. uncertainty", "el");
    if(typicalBox)
      legend2->AddEntry(typicalBox, "Syst. uncertainty", "fl");
    for(unsigned int i=0; i<opt.mObjForLegend2.size(); ++i)
      legend2->AddEntry( opt.mObjForLegend2[i], opt.mDesriptionForLegend2[i], opt.mDrawTypeForLegend2[i]);
    legend2->SetMargin(mSpecialLegendMarginFactor * 0.3 * opt.mLegendXwidth2 * legend2->GetNRows() / legend2->GetNColumns() );
    legend2->Draw();
  }
  
  
  if( opt.mTextToDraw.size() > 0 ){
    for(unsigned int i=0; i<opt.mTextToDraw.size(); ++i){
      opt.mTextToDraw[i].SetNDC(kTRUE);
      opt.mTextToDraw[i].Draw();
    }
  }
  
  //   TASImage *img = new TASImage("miscellaneous/ATLAS_logo.pdf");
  //   //   TASImage *img = new TASImage("miscellaneous/ATLAS_logo.eps");
  //   img->SetImageQuality(TAttImage::kImgBest);
  
  
  hMain->DrawCopy(mSpecialDataDrawing ? "HIST E SAME" : "PE SAME", "");
  if( hMain_2 )
    hMain_2->DrawCopy("PE SAME", "");
  
  
  for(int i=opt.mObjectToDraw.size()-1; i>=0; --i){
    if( TString(opt.mObjectToDraw[i]->ClassName())==TString("TF1") )
      opt.mObjectToDraw[i]->Draw("SAME");
    else
      opt.mObjectToDraw[i]->Draw();
  }
  
  gPad->RedrawAxis();
  
  
  if( opt.mInsert ){
    TPad *insert = new TPad("insert", "insert", opt.mX1Insert, opt.mY1Insert, opt.mX2Insert, opt.mY2Insert);
    insert->SetBottomMargin(0); // Upper and lower plot are joined
    insert->SetFrameFillStyle(0);
    insert->SetFrameLineWidth(2);
    insert->SetFillColor(-1);
    insert->Draw();             // Draw the upper pad: insert
    insert->cd();               // insert becomes the current pad
    insert->SetLeftMargin(opt.mLeftMargin);
    insert->SetRightMargin(opt.mRightMargin);
    insert->SetTopMargin(opt.mTopMargin);
    insert->SetBottomMargin(opt.mBottomMargin);
    
    TH1F *hMainCopy = new TH1F( *hMain );
    hMainCopy->GetXaxis()->SetTitle("");
    hMainCopy->GetXaxis()->SetLabelSize( 0.9*hMainCopy->GetXaxis()->GetLabelSize() * ( 1. - opt.mLeftMargin - opt.mRightMargin) / (opt.mX2Insert - opt.mX1Insert) );
    hMainCopy->GetYaxis()->SetLabelSize( 0.9*hMainCopy->GetYaxis()->GetLabelSize() * ( 1. - opt.mLeftMargin - opt.mRightMargin) / (opt.mX2Insert - opt.mX1Insert) );
    hMainCopy->GetYaxis()->SetTitle("");
    hMainCopy->GetXaxis()->SetRange( hMainCopy->FindBin(opt.mXMinInstert ), hMainCopy->FindBin(opt.mXMaxInstert ) );
    int binMaxY;
    binMaxY = hMainCopy->GetMaximumBin();
    hMainCopy->GetYaxis()->SetRangeUser(0.01, opt.mYMaxInstert<0 ? (1.1*hMainCopy->GetBinContent( binMaxY )) : opt.mYMaxInstert);
    
    int n1 = hMainCopy->GetYaxis()->GetNdivisions()%100;
    int n2 = hMainCopy->GetYaxis()->GetNdivisions()%10000 - n1;
    int n3 = hMainCopy->GetYaxis()->GetNdivisions() - 100*n2 - n1;
    
    hMainCopy->GetYaxis()->SetNdivisions( n1/2, 2, 1 );
    hMainCopy->GetXaxis()->SetRangeUser(opt.mXMinInstert, opt.mXMaxInstert);
    hMainCopy->GetXaxis()->SetNdivisions(10,5,1);
    
    hMainCopy->GetXaxis()->SetTickLength(0.04);
    hMainCopy->GetYaxis()->SetTickLength(0.02);
    
    hMainCopy->Draw(mSpecialDataDrawing ? "HIST E" : "PE");
    for(unsigned int i=0; i<opt.mHistTH1F.size(); ++i){
      if(opt.mScale){
        opt.mHistTH1F[i]->Scale( opt.mScalingFactor );
      }
      opt.mHistTH1F[i]->DrawCopy( opt.mHistTH1F_DrawingOpt[i], "" );
    }
    
//     if( opt.mXpositionSystErrorBox.size() > 0 ){
//       for(unsigned int i=0; i<opt.mXpositionSystErrorBox.size(); ++i){
//         int binNumber = hMainCopy->GetXaxis()->FindBin( opt.mXpositionSystErrorBox[i] );
//         if( !(hMainCopy->GetBinContent(binNumber)>0) || opt.mXpositionSystErrorBox[i]>opt.mXMaxInstert || opt.mXpositionSystErrorBox[i]<opt.mXMinInstert )
//           continue;
//         
//         double xMinSystErrorBox = hMainCopy->GetXaxis()->GetBinCenter(binNumber) - systErrorBoxWidth/2.;
//         double xMaxSystErrorBox = hMainCopy->GetXaxis()->GetBinCenter(binNumber) + systErrorBoxWidth/2.;
//         double yMinSystErrorBox = hMainCopy->GetBinContent(binNumber) * (1.0 - sqrt(pow(syst.mhSystErrorDown->GetBinContent(binNumber),2) + pow(mParams->luminosityUncertainty()*1.0e5,2)));
//         double yMaxSystErrorBox = hMainCopy->GetBinContent(binNumber) * (1.0 + sqrt(pow(syst.mhSystErrorUp->GetBinContent(binNumber),2) + pow(mParams->luminosityUncertainty()*1.0e5,2)));
//         TBox *b = new TBox( xMinSystErrorBox, yMinSystErrorBox, xMaxSystErrorBox, yMaxSystErrorBox );
//         b->SetFillColor(opt.mSystErrorTotalBoxColor);
//         b->SetLineWidth(0);
//         b->Draw("l");
//         
//         yMinSystErrorBox = hMainCopy->GetBinContent(binNumber) * (1.0 - syst.mhSystErrorDown->GetBinContent(binNumber));
//         yMaxSystErrorBox = hMainCopy->GetBinContent(binNumber) * (1.0 + syst.mhSystErrorUp->GetBinContent(binNumber));
//         b = new TBox( xMinSystErrorBox, yMinSystErrorBox, xMaxSystErrorBox, yMaxSystErrorBox );
//         b->SetFillColor(opt.mSystErrorBoxColor);
//         b->SetLineWidth(0);
//         b->Draw("l");
//       }
//     }
    
    hMainCopy->DrawCopy(mSpecialDataDrawing ? "HIST E SAME" : "PE SAME", "");
    gPad->RedrawAxis();
  }
  
  c->cd();
  c->Modified();
  c->Update();
  
  c->Print(opt.mPdfName+".pdf", "EmbedFonts");
//   c->Print(opt.mPdfName+".png");
 
  TCanvas *c2 = new TCanvas("cccccc2", "cccccc2", opt.mCanvasWidth, (mRatioFactor + 1.0)*opt.mCanvasHeight);
  //   c->SetFrameFillStyle(0);
  //   c->SetFrameLineWidth(2);
  //   c->SetFillColor(-1);
  c2->SetLeftMargin(0);
  c2->SetRightMargin(0);
  c2->SetTopMargin(0);
  c2->SetBottomMargin(0);
  
  // Upper plot will be in pad1
  TPad *pad1 = new TPad("pad1", "pad1", 0, mRatioFactor, 1.0, 1.0);
  pad1->SetBottomMargin(0); // Upper and lower plot are joined
  pad1->SetFrameFillStyle(0);
  pad1->SetFillColor(-1);
  pad1->Draw();             // Draw the upper pad: pad1
  pad1->cd();               // pad1 becomes the current pad
  gPad->SetLeftMargin(0);
  gPad->SetRightMargin(0);
  gPad->SetTopMargin(0);
  gPad->SetBottomMargin(0);
  
  TH1F *ratio_Data_to_Data = new TH1F( *hMain );
  TH1F *ratio_Data_to_Data_2 = nullptr;
  if(hMain_2)
    ratio_Data_to_Data_2 = new TH1F( *hMain_2 );
  hMain->GetXaxis()->SetLabelSize( 0 );
  hMain->GetXaxis()->SetTitle("");
  c->Modified();
  c->Update();
  
  c->DrawClonePad();
  
  const double modifiedRatioFactor = mRatioFactor + 0.54*opt.mBottomMargin;
  
  c2->cd();          // Go back to the main canvas before defining pad2
  TPad *pad2 = new TPad("pad2", "pad2", 0, 0, 1.0, mRatioFactor + 0.65*opt.mBottomMargin);
  pad2->SetFrameFillStyle(0);
  pad2->SetFillColor(-1);
  //   gStyle->SetGridColor(kGray);
  pad2->SetGridy(kTRUE);
  pad2->Draw();
  pad2->cd();       // pad2 becomes the current pad
  gPad->SetLeftMargin(opt.mLeftMargin);
  gPad->SetRightMargin(opt.mRightMargin);
  gPad->SetTopMargin(0.005);
  gPad->SetBottomMargin(opt.mBottomMargin * (1.0-modifiedRatioFactor)/modifiedRatioFactor);
  
  if( !mDrawSpecialRatio2 && !mDrawSpecialRatio3 ){
    
    TH1F* histMC_ratio = nullptr;
    if(opt.mHistTH1F.size() > 0){
      histMC_ratio = new TH1F(*opt.mHistTH1F[0]);
      for(unsigned int fileId=1; fileId<opt.mHistTH1F.size(); ++fileId) 
        histMC_ratio->Add( opt.mHistTH1F[fileId] );
    }
    
    TH1F* histMC_ratio2 = nullptr;
    if( hMain_2 && hToDrawVec.size()>0 )
      histMC_ratio2 = hToDrawVec[ hToDrawVec.size()-1 ];
    
    if( histMC_ratio )
      ratio_Data_to_Data->Divide( histMC_ratio );
    if( ratio_Data_to_Data_2 )
      ratio_Data_to_Data_2->Divide( histMC_ratio2 );
    
    TGraphAsymmErrors *graphErr = new TGraphAsymmErrors( ratio_Data_to_Data );
    TGraphAsymmErrors *graphErr_2 = new TGraphAsymmErrors( ratio_Data_to_Data_2 );
    
    if( histMC_ratio && ratio_Data_to_Data ){
        for(int i=1; i<=ratio_Data_to_Data->GetNbinsX(); ++i){
        if( ratio_Data_to_Data->GetBinError(i)>0 ){
            ratio_Data_to_Data->SetBinError( i, hMain->GetBinError(i)/histMC_ratio->GetBinContent(i) );
        }
        }
    }
    
    if( histMC_ratio2 && ratio_Data_to_Data_2 ){
      for(int i=1; i<=ratio_Data_to_Data_2->GetNbinsX(); ++i){
        if( ratio_Data_to_Data_2->GetBinError(i)>0 ){
          ratio_Data_to_Data_2->SetBinError( i, hMain_2->GetBinError(i)/histMC_ratio2->GetBinContent(i) );
        }
      }
    }
    
    
    TH1F *ratioratio = nullptr;
    if( mDrawSpecialRatio ){
      ratioratio = new TH1F( *hMain );
      ratioratio->SetName("ratioratio");
      TH1F *tmpratioratio = new TH1F( *hMain_2 );
      tmpratioratio->SetName("tmpratioratio");
      tmpratioratio->Scale( ratioratio->Integral(0,-1) / tmpratioratio->Integral(0,-1) );
      ratioratio->Divide( tmpratioratio );
    }
    
    ratio_Data_to_Data->GetYaxis()->SetRangeUser(mRatioYmin, mRatioYmax);
    ratio_Data_to_Data->GetYaxis()->SetTitle(mRatioYTitle);
    ratio_Data_to_Data->GetYaxis()->SetTitleOffset( 0.56 * mRatioYTitleOffsetFactor );
    ratio_Data_to_Data->GetYaxis()->CenterTitle();
    ratio_Data_to_Data->GetYaxis()->SetNdivisions(4,4,1);
    ratio_Data_to_Data->GetYaxis()->SetTitleSize( /*1.6**/ratio_Data_to_Data->GetYaxis()->GetTitleSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
    ratio_Data_to_Data->GetYaxis()->SetLabelSize( /*1.2**/ratio_Data_to_Data->GetYaxis()->GetLabelSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
    ratio_Data_to_Data->GetXaxis()->SetLabelOffset( 0.01 );
    ratio_Data_to_Data->GetYaxis()->SetTickLength( ratio_Data_to_Data->GetYaxis()->GetTickLength() );
    ratio_Data_to_Data->GetXaxis()->SetTitleSize( /*1.4**/ratio_Data_to_Data->GetXaxis()->GetTitleSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
    ratio_Data_to_Data->GetXaxis()->SetLabelSize( /*1.4**/ratio_Data_to_Data->GetXaxis()->GetLabelSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
    ratio_Data_to_Data->GetXaxis()->SetTickLength( (1.0-modifiedRatioFactor)/modifiedRatioFactor* 1.2 *ratio_Data_to_Data->GetXaxis()->GetTickLength() );
    
    ratio_Data_to_Data->Draw("PE0");
    
//     TMultiGraph *mg = new TMultiGraph();
    if( ratio_Data_to_Data ){
      
      graphErr->SetFillStyle( 3005 );
      graphErr->SetFillColor( kBlack );
      graphErr->Draw("02");
    }
    
    if( ratio_Data_to_Data_2 ){
      
      graphErr_2->SetFillStyle( 3004 );
      graphErr_2->SetFillColor( kRed );
      graphErr_2->Draw("02");
    }
    
    for(unsigned int i=0; i<opt.mObjectToDraw.size(); ++i){
      if( TString(opt.mObjectToDraw[i]->ClassName())==TString("TBox") ){
        TBox *b = dynamic_cast<TBox*>( opt.mObjectToDraw[i] );
        double x1 = b->GetX1();
        double x2 = b->GetX2();
        double y1 = b->GetY2() / histMC_ratio->GetBinContent( histMC_ratio->FindBin(0.5*(x1+x2)) );
        double y2 = b->GetY1() / histMC_ratio->GetBinContent( histMC_ratio->FindBin(0.5*(x1+x2)) );
        if(y1>mRatioYmax) y1 = mRatioYmax; else if(y1<mRatioYmin) y1 = mRatioYmin;
        if(y2>mRatioYmax) y2 = mRatioYmax; else if(y2<mRatioYmin) y2 = mRatioYmin;
        if((y1>=mRatioYmax && y2>=mRatioYmax) || (y1<=mRatioYmin && y2<=mRatioYmin)) continue;
        TBox *bb = new TBox( x1, y1, x2, y2 );
        bb->SetFillColorAlpha( b->GetFillColor(), 0.3 );
        bb->Draw();
      }
    }
    
    
    if( ratio_Data_to_Data_2 )
      ratio_Data_to_Data_2->Draw("PE0 SAME");
    if( mDrawSpecialRatio && ratioratio ){
      ratioratio->SetMarkerColor(kBlack);
      ratioratio->SetLineColor(kBlack);
      ratioratio->SetMarkerStyle(24);
      ratioratio->Draw("PE0 SAME");
    }
    
    TLine *line = new TLine(opt.mXmin, 1.0, opt.mXmax, 1.0);
    line->SetLineWidth(2);
    line->SetLineStyle(7);
    line->SetLineColor(kBlack);
    line->Draw();
    
  //   for(unsigned int i=0; i<histMC_ratio.size(); ++i){
  //     histMC_ratio[i]->SetMarkerColor( histMC_ratio[i]->GetLineColor() );
  //     histMC_ratio[i]->SetMarkerStyle( opt.mMarkerStyle );
  //     histMC_ratio[i]->SetMarkerSize( opt.mMarkerSize );
  //     histMC_ratio[i]->Draw( /*opt.mHistTH1F_DrawingOpt[i]*/"PE0 ][ SAME" );
  //   }
    
  } else{
    
    if( mDrawSpecialRatio2 ){
      TString titleX = ratio_Data_to_Data->GetXaxis()->GetTitle();
      double labelSize = ratio_Data_to_Data->GetXaxis()->GetLabelSize();
      ratio_Data_to_Data = new TH1F( *hMain_2 );
      ratio_Data_to_Data->Divide( hMain );
      
      ratio_Data_to_Data->GetYaxis()->SetRangeUser(mRatioYmin, mRatioYmax);
      ratio_Data_to_Data->GetYaxis()->SetTitle(mRatioYTitle);
      ratio_Data_to_Data->GetXaxis()->SetTitle( titleX );
      ratio_Data_to_Data->GetYaxis()->SetTitleOffset( 0.56 * mRatioYTitleOffsetFactor );
      ratio_Data_to_Data->GetYaxis()->CenterTitle();
      ratio_Data_to_Data->GetYaxis()->SetNdivisions(4,4,1);
      ratio_Data_to_Data->GetYaxis()->SetTitleSize( /*1.6**/hMain->GetYaxis()->GetTitleSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
      ratio_Data_to_Data->GetYaxis()->SetLabelSize( /*1.2**/hMain->GetYaxis()->GetLabelSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
      ratio_Data_to_Data->GetXaxis()->SetLabelOffset( 0.01 );
      ratio_Data_to_Data->GetYaxis()->SetTickLength( hMain->GetYaxis()->GetTickLength() );
      ratio_Data_to_Data->GetXaxis()->SetTitleSize( /*1.4**/hMain->GetXaxis()->GetTitleSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
      ratio_Data_to_Data->GetXaxis()->SetLabelSize( /*1.4**/labelSize*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
      ratio_Data_to_Data->GetXaxis()->SetTickLength( (1.0-modifiedRatioFactor)/modifiedRatioFactor* 1.2 *hMain->GetXaxis()->GetTickLength() );
      
      if( opt.mSetDivisionsX )
        ratio_Data_to_Data->GetXaxis()->SetNdivisions( opt.mXnDivisionA, opt.mXnDivisionB, opt.mXnDivisionC, !opt.mForceDivisionsX );
      
      ratio_Data_to_Data->Draw("PE");
      
      
      if( opt.mHistTH1F.size()>0 ){
        THStack *hs = new THStack( Form("hs_%d", static_cast<int>(gRandom->Uniform()*100000)), "THStack title" );
        
        if( mInverseStackDrawing ){
          for(int i=opt.mHistTH1F.size()-1; i>=0; --i){
            TH1F *histCopy = new TH1F( *opt.mHistTH1F[i] );
  //           if(opt.mScale){
  //             histCopy->Scale( opt.mScalingFactor );
  //           }
            histCopy->Divide( hMain );
            hs->Add( histCopy, opt.mHistTH1F_DrawingOpt[i] );
          }
        } else{
          for(unsigned int i=0; i<opt.mHistTH1F.size(); ++i){
            TH1F *histCopy = new TH1F( *opt.mHistTH1F[i] );
  //           if(opt.mScale){
  //             histCopy->Scale( opt.mScalingFactor );
  //           }
            histCopy->Divide( hMain );
            hs->Add( histCopy, opt.mHistTH1F_DrawingOpt[i] );
          }
        }
        
        hs->Draw("HIST SAME");
      }
    } else     
    if( mDrawSpecialRatio3 ){
//       TString titleX = ratio_Data_to_Data->GetXaxis()->GetTitle();
//       double labelSize = ratio_Data_to_Data->GetXaxis()->GetLabelSize();
//       ratio_Data_to_Data = new TH1F( *hMain_2 );
//       ratio_Data_to_Data->Divide( hMain );
//       
//       ratio_Data_to_Data->GetYaxis()->SetRangeUser(mRatioYmin, mRatioYmax);
//       ratio_Data_to_Data->GetYaxis()->SetTitle(mRatioYTitle);
//       ratio_Data_to_Data->GetXaxis()->SetTitle( titleX );
//       ratio_Data_to_Data->GetYaxis()->SetTitleOffset( 0.56 * mRatioYTitleOffsetFactor );
//       ratio_Data_to_Data->GetYaxis()->CenterTitle();
//       ratio_Data_to_Data->GetYaxis()->SetNdivisions(4,4,1);
//       ratio_Data_to_Data->GetYaxis()->SetTitleSize( /*1.6**/hMain->GetYaxis()->GetTitleSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
//       ratio_Data_to_Data->GetYaxis()->SetLabelSize( /*1.2**/hMain->GetYaxis()->GetLabelSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
//       ratio_Data_to_Data->GetXaxis()->SetLabelOffset( 0.01 );
//       ratio_Data_to_Data->GetYaxis()->SetTickLength( hMain->GetYaxis()->GetTickLength() );
//       ratio_Data_to_Data->GetXaxis()->SetTitleSize( /*1.4**/hMain->GetXaxis()->GetTitleSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
//       ratio_Data_to_Data->GetXaxis()->SetLabelSize( /*1.4**/labelSize*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
//       ratio_Data_to_Data->GetXaxis()->SetTickLength( (1.0-modifiedRatioFactor)/modifiedRatioFactor* 1.2 *hMain->GetXaxis()->GetTickLength() );
//       
//       if( opt.mSetDivisionsX )
//         ratio_Data_to_Data->GetXaxis()->SetNdivisions( opt.mXnDivisionA, opt.mXnDivisionB, opt.mXnDivisionC, !opt.mForceDivisionsX );
//       
//       ratio_Data_to_Data->Draw("PE");
//       
//       
//       if( opt.mHistTH1F.size()>0 ){
//         THStack *hs = new THStack( Form("hs_%d", static_cast<int>(gRandom->Uniform()*100000)), "THStack title" );
//         
//         if( mInverseStackDrawing ){
//           for(int i=opt.mHistTH1F.size()-1; i>=0; --i){
//             TH1F *histCopy = new TH1F( *opt.mHistTH1F[i] );
//   //           if(opt.mScale){
//   //             histCopy->Scale( opt.mScalingFactor );
//   //           }
//             histCopy->Divide( hMain );
//             hs->Add( histCopy, opt.mHistTH1F_DrawingOpt[i] );
//           }
//         } else{
//           for(unsigned int i=0; i<opt.mHistTH1F.size(); ++i){
//             TH1F *histCopy = new TH1F( *opt.mHistTH1F[i] );
//   //           if(opt.mScale){
//   //             histCopy->Scale( opt.mScalingFactor );
//   //           }
//             histCopy->Divide( hMain );
//             hs->Add( histCopy, opt.mHistTH1F_DrawingOpt[i] );
//           }
//         }
//         
//         hs->Draw("HIST SAME");
//       }
    }
    
  }
  
  ratio_Data_to_Data->DrawCopy("PE SAME", "");
  gPad->RedrawAxis();
  
  c2->Print("Ratio_"+opt.mPdfName+".pdf", "EmbedFonts");
//   c2->Print("Ratio_"+opt.mPdfName+".png");
  

  delete c;
  delete c2;
}








void DataVsMC::loadHistograms(){
  
  mhDeltaPhiDegrees_DATA = nullptr;
  
  cout << "Loading histograms..." << endl;
  
  TString fileNames[nFiles];

//   fileNames[DATA] = "ROOT_files/Feb1/all_new_Feb1.root";
//   fileNames[MC_SIGNAL] =             "ROOT_files/Feb1/hist-user.ladamczy.mc15_13TeV.364988.DiMe_pipi.evgen.STDM6.e0000.v7_EXT0_V6_EXT0_EXT0.root";
//   fileNames[MC_PYTHIA_CD_BKGD] =            "ROOT_files/Feb1/hist-user.kjanas.mc15_13TeV.364698.Py8EG_A2_MSTW2008LO_CD_minbias_1InDet7.evgen.STDM6.e0000.v7_EXT0_v6_EXT0_EXT0.root";
//   fileNames[MC_PYTHIA_CD_BKGD_NEUTRALS] =   "ROOT_files/Feb1/hist-user.kjanas.mc15_13TeV.364698.Py8EG_A2_MSTW2008LO_CD_minbias_1InDet7.evgen.STDM6.e0000.v7_EXT0_v6_EXT0_EXT0.root";  
  
  fileNames[DATA] = "ROOT_files_2/data.root";
  fileNames[MC_SIGNAL] =             "ROOT_files_2/cep.root";
  fileNames[MC_PYTHIA_CD_BKGD] =            "ROOT_files_2/cd.root";
  fileNames[MC_PYTHIA_CD_BKGD_NEUTRALS] =   "ROOT_files_2/cd.root";
  
  
  
  
  std::vector<double> missingPtBinningVec;
  std::vector<double> missingPtBinningVec_SameSign;
  if( mBkgdSubtractionDemonstration ){
    double binEdge = 0.0;
    do{
      missingPtBinningVec.push_back( binEdge );
      binEdge += 0.01;
    } while( binEdge <= 1.0 );
  } else{
    
    missingPtBinningVec.push_back(  0 );
    missingPtBinningVec.push_back(  0.5 );
    missingPtBinningVec.push_back(  1 );
    missingPtBinningVec.push_back(  1.5 );
    missingPtBinningVec.push_back(  2 );
    missingPtBinningVec.push_back(  2.5 );
    missingPtBinningVec.push_back(  3 );
    missingPtBinningVec.push_back(  4 );
    missingPtBinningVec.push_back(  5 );
    missingPtBinningVec.push_back(  7 );
    missingPtBinningVec.push_back(  10 );
    missingPtBinningVec.push_back(  13 );
    missingPtBinningVec.push_back(  16 );
    missingPtBinningVec.push_back(  20 );
//     missingPtBinningVec.push_back(  25 );
    missingPtBinningVec.push_back(  30 );
    
    missingPtBinningVec_SameSign.push_back(  0 );
    //     missingPtBinningVec_SameSign.push_back(  1 );
    //     missingPtBinningVec_SameSign.push_back(  2 );
    missingPtBinningVec_SameSign.push_back(  3 );
//     missingPtBinningVec_SameSign.push_back(  5 );
    missingPtBinningVec_SameSign.push_back(  10 );
//     missingPtBinningVec_SameSign.push_back(  15 );
    missingPtBinningVec_SameSign.push_back(  20 );
    missingPtBinningVec_SameSign.push_back(  30 );
  }
  
  
  std::vector<double> missingPxBinningVec;
  std::vector<double> missingPxBinningVec_SameSign;
  if( mBkgdSubtractionDemonstration ){
    double binEdge = 0.0;
    do{
      missingPxBinningVec.push_back( binEdge );
      binEdge += 0.01;
    } while( binEdge <= 1.0 );
  } else{
    
    missingPxBinningVec.push_back( -1.0 );
    missingPxBinningVec.push_back( -0.6 );
    missingPxBinningVec.push_back( -0.4 );
    missingPxBinningVec.push_back( -0.3 );
    missingPxBinningVec.push_back( -0.2 );
    missingPxBinningVec.push_back( -0.1 );
    missingPxBinningVec.push_back(  0.0 );
    missingPxBinningVec.push_back(  0.1 );
    missingPxBinningVec.push_back(  0.2 );
    missingPxBinningVec.push_back(  0.3 );
    missingPxBinningVec.push_back(  0.4 );
    missingPxBinningVec.push_back(  0.6 );
    missingPxBinningVec.push_back(  1.0 );
    
    
    missingPxBinningVec_SameSign.push_back( -1.0 );
//     missingPxBinningVec_SameSign.push_back( -0.6 );
    missingPxBinningVec_SameSign.push_back( -0.4 );
    //     missingPxBinningVec_SameSign.push_back( -0.3 );
//     missingPxBinningVec_SameSign.push_back( -0.2 );
    //     missingPxBinningVec_SameSign.push_back( -0.1 );
    missingPxBinningVec_SameSign.push_back(  0.0 );
    //     missingPxBinningVec_SameSign.push_back(  0.1 );
//     missingPxBinningVec_SameSign.push_back(  0.2 );
    //     missingPxBinningVec_SameSign.push_back(  0.3 );
    missingPxBinningVec_SameSign.push_back(  0.4 );
//     missingPxBinningVec_SameSign.push_back(  0.6 );
    missingPxBinningVec_SameSign.push_back(  1.0 );
    
  }
  
  
  std::vector<double> missingPyBinningVec;
  std::vector<double> missingPyBinningVec_SameSign;
  if( mBkgdSubtractionDemonstration ){
    double binEdge = 0.0;
    do{
      missingPyBinningVec.push_back( binEdge );
      binEdge += 0.01;
    } while( binEdge <= 1.0 );
  } else{
    
    missingPyBinningVec.push_back( -1.0 );
    missingPyBinningVec.push_back( -0.6 );
    missingPyBinningVec.push_back( -0.4 );
    missingPyBinningVec.push_back( -0.3 );
    missingPyBinningVec.push_back( -0.2 );
    missingPyBinningVec.push_back( -0.1 );
    missingPyBinningVec.push_back( -0.05 );
    missingPyBinningVec.push_back(  0.0 );
    missingPyBinningVec.push_back(  0.05 );
    missingPyBinningVec.push_back(  0.1 );
    missingPyBinningVec.push_back(  0.2 );
    missingPyBinningVec.push_back(  0.3 );
    missingPyBinningVec.push_back(  0.4 );
    missingPyBinningVec.push_back(  0.6 );
    missingPyBinningVec.push_back(  1.0 );
    
    
    missingPyBinningVec_SameSign.push_back( -1.0 );
//     missingPyBinningVec_SameSign.push_back( -0.6 );
    missingPyBinningVec_SameSign.push_back( -0.4 );
    //     missingPyBinningVec_SameSign.push_back( -0.3 );
//     missingPyBinningVec_SameSign.push_back( -0.2 );
    //     missingPyBinningVec_SameSign.push_back( -0.1 );
    missingPyBinningVec_SameSign.push_back(  0.0 );
    //     missingPyBinningVec_SameSign.push_back(  0.1 );
//     missingPyBinningVec_SameSign.push_back(  0.2 );
    //     missingPyBinningVec_SameSign.push_back(  0.3 );
    missingPyBinningVec_SameSign.push_back(  0.4 );
//     missingPyBinningVec_SameSign.push_back(  0.6 );
    missingPyBinningVec_SameSign.push_back(  1.0 );
    
  }
  
  
      int mRpPerBranch[4][2];
    
    mRpPerBranch[0][0] = 2;
    mRpPerBranch[0][1] = 0;
    mRpPerBranch[1][0] = 3;
    mRpPerBranch[1][1] = 1;
    mRpPerBranch[2][0] = 4;
    mRpPerBranch[2][1] = 6;
    mRpPerBranch[3][0] = 5;
    mRpPerBranch[3][1] = 7;
  
  

  for(int f=0; f<nFiles; ++f){
    
    TFile *file = TFile::Open(fileNames[f], "READ");
    cout << "\tReading file " << fileNames[f] << endl;
    
    TString Id;
    for( int i=0; i<4; ++i){
            for( int j=0; j<2; j++){
                Id.Form("%d", mRpPerBranch[i][j]);
                
                hHitMapExclusivePiPi[f][mRpPerBranch[i][j]] = dynamic_cast<TH2F*>( file->Get("HitMapExclusivePiPi_"+Id) );
                hHitMapExclusivePiPi[f][mRpPerBranch[i][j]]->SetDirectory(0);
                if( f!=DATA ){
                        for(int cst=0; cst<CepUtil::nCentralStateTopologies; ++cst){
                            hHitMapExclusivePiPiVsTrueLevelTopology[f][mRpPerBranch[i][j]][cst] = dynamic_cast<TH2F*>( file->Get( Form("hHitMapExclusivePiPiVsTrueLevelTopology_"+Id+"_%d", cst) ) );
                            hHitMapExclusivePiPiVsTrueLevelTopology[f][mRpPerBranch[i][j]][cst]->SetDirectory(0);
                        }
                        
                        hHitMapExclusivePiPi[f][mRpPerBranch[i][j]]->Reset("ICESM");
                        
                        for(int cst=0; cst<CepUtil::nCentralStateTopologies; ++cst){
                            if( f==MC_SIGNAL ){
                                if( cst==CepUtil::k1ChargedPairNoNeutrals ){
                                    hHitMapExclusivePiPi[f][mRpPerBranch[i][j]]->Add( hHitMapExclusivePiPiVsTrueLevelTopology[f][mRpPerBranch[i][j]][cst] );
                                }
                            } else
                                if( f==MC_PYTHIA_CD_BKGD_NEUTRALS ){
                                    if( cst==CepUtil::k1ChargedPairAndNeutrals ){
                                        hHitMapExclusivePiPi[f][mRpPerBranch[i][j]]->Add( hHitMapExclusivePiPiVsTrueLevelTopology[f][mRpPerBranch[i][j]][cst], (mSubtractNeutrals && cst==CepUtil::k1ChargedPairAndNeutrals) ? mNeutralsFracToPreserve : 1.0 );
                                    }
                                } else
                                {
                                    if( cst!=CepUtil::k1ChargedPairNoNeutrals && cst!=CepUtil::kUnqualified && cst!=CepUtil::k1ChargedPairAndNeutrals ){
                                        hHitMapExclusivePiPi[f][mRpPerBranch[i][j]]->Add( hHitMapExclusivePiPiVsTrueLevelTopology[f][mRpPerBranch[i][j]][cst] );
                                    }
                                }
                        }
                }
            }
            
        
        mh_YminusYExtr_BeforeMomentumBalanceCut[f][i] = dynamic_cast<TH1F*>( file->Get(Form("mh_YminusYExtr_BeforeMomentumBalanceCut_%d", i)) );
        mh_YminusYExtr_BeforeMomentumBalanceCut[f][i]->SetDirectory(0);
        if( f!=DATA ){
            for(int cst=0; cst<CepUtil::nCentralStateTopologies; ++cst){
                mh_YminusYExtr_BeforeMomentumBalanceCut_VsCentralStateTopology[f][i][cst] = dynamic_cast<TH1F*>( file->Get(Form("mh_YminusYExtr_BeforeMomentumBalanceCut_VsCentralStateTopology_%d_%d", i, cst)) );
                mh_YminusYExtr_BeforeMomentumBalanceCut_VsCentralStateTopology[f][i][cst]->SetDirectory(0);   
            }
            mh_YminusYExtr_BeforeMomentumBalanceCut[f][i]->Reset("ICESM");
            for(int cst=0; cst<CepUtil::nCentralStateTopologies; ++cst){
                if( f==MC_SIGNAL ){
                    if( cst==CepUtil::k1ChargedPairNoNeutrals ){
                        mh_YminusYExtr_BeforeMomentumBalanceCut[f][i]->Add( mh_YminusYExtr_BeforeMomentumBalanceCut_VsCentralStateTopology[f][i][cst] );
                    }
                } else
                    if( f==MC_PYTHIA_CD_BKGD_NEUTRALS ){
                        if( cst==CepUtil::k1ChargedPairAndNeutrals ){
                            mh_YminusYExtr_BeforeMomentumBalanceCut[f][i]->Add( mh_YminusYExtr_BeforeMomentumBalanceCut_VsCentralStateTopology[f][i][cst], (mSubtractNeutrals && cst==CepUtil::k1ChargedPairAndNeutrals) ? mNeutralsFracToPreserve : 1.0 );
                        }
                    } else
                    {
                        if( cst!=CepUtil::k1ChargedPairNoNeutrals && cst!=CepUtil::kUnqualified && cst!=CepUtil::k1ChargedPairAndNeutrals ){
                            mh_YminusYExtr_BeforeMomentumBalanceCut[f][i]->Add( mh_YminusYExtr_BeforeMomentumBalanceCut_VsCentralStateTopology[f][i][cst] );
                        }
                    }
            }
        }
        
        mh_YminusYExtr[f][i] = dynamic_cast<TH1F*>( file->Get(Form("mh_YminusYExtr_%d", i)) );
        mh_YminusYExtr[f][i]->SetDirectory(0);
        if( f!=DATA ){
            for(int cst=0; cst<CepUtil::nCentralStateTopologies; ++cst){
                mh_YminusYExtr_VsCentralStateTopology[f][i][cst] = dynamic_cast<TH1F*>( file->Get(Form("mh_YminusYExtr_VsCentralStateTopology_%d_%d", i, cst)) );
                mh_YminusYExtr_VsCentralStateTopology[f][i][cst]->SetDirectory(0);   
            }
            mh_YminusYExtr[f][i]->Reset("ICESM");
            for(int cst=0; cst<CepUtil::nCentralStateTopologies; ++cst){
                if( f==MC_SIGNAL ){
                    if( cst==CepUtil::k1ChargedPairNoNeutrals ){
                        mh_YminusYExtr[f][i]->Add( mh_YminusYExtr_VsCentralStateTopology[f][i][cst] );
                    }
                } else
                    if( f==MC_PYTHIA_CD_BKGD_NEUTRALS ){
                        if( cst==CepUtil::k1ChargedPairAndNeutrals ){
                            mh_YminusYExtr[f][i]->Add( mh_YminusYExtr_VsCentralStateTopology[f][i][cst], (mSubtractNeutrals && cst==CepUtil::k1ChargedPairAndNeutrals) ? mNeutralsFracToPreserve : 1.0 );
                        }
                    } else
                    {
                        if( cst!=CepUtil::k1ChargedPairNoNeutrals && cst!=CepUtil::kUnqualified && cst!=CepUtil::k1ChargedPairAndNeutrals ){
                            mh_YminusYExtr[f][i]->Add( mh_YminusYExtr_VsCentralStateTopology[f][i][cst] );
                        }
                    }
            }
        }
    }
    
    
    
    mh_ExclusivePiPi_d0[f] = dynamic_cast<TH1F*>( file->Get("mh_ExclusivePiPi_d0") );
    mh_ExclusivePiPi_d0[f]->SetDirectory(0);
    mh_ExclusivePiPi_z0SinTheta[f] = dynamic_cast<TH1F*>( file->Get("mh_ExclusivePiPi_z0SinTheta") );
    mh_ExclusivePiPi_z0SinTheta[f]->SetDirectory(0);
    mh_ExclusivePiPi_Delta_z0[f] = dynamic_cast<TH1F*>( file->Get("mh_ExclusivePiPi_Delta_z0") );
    mh_ExclusivePiPi_Delta_z0[f]->SetDirectory(0);
    
    mh_Exclusive4Pi_d0[f] = dynamic_cast<TH1F*>( file->Get("mh_Exclusive4Pi_d0") );
    if(mh_Exclusive4Pi_d0[f]) mh_Exclusive4Pi_d0[f]->SetDirectory(0);
    mh_Exclusive4Pi_z0SinTheta[f] = dynamic_cast<TH1F*>( file->Get("mh_Exclusive4Pi_z0SinTheta") );
    if(mh_Exclusive4Pi_z0SinTheta[f]) mh_Exclusive4Pi_z0SinTheta[f]->SetDirectory(0);
    mh_Exclusive4Pi_Delta_z0[f] = dynamic_cast<TH1F*>( file->Get("mh_Exclusive4Pi_Delta_z0") );
    if(mh_Exclusive4Pi_Delta_z0[f]) mh_Exclusive4Pi_Delta_z0[f]->SetDirectory(0);
    
    if( f!=DATA ){
        
        for(int cst=0; cst<CepUtil::nCentralStateTopologies; ++cst){
            mh_ExclusivePiPi_d0_VsCentralStateTopology[f][cst] = dynamic_cast<TH1F*>( file->Get( Form("mh_ExclusivePiPi_d0_%d", cst) ) );
            mh_ExclusivePiPi_d0_VsCentralStateTopology[f][cst]->SetDirectory(0);
            mh_ExclusivePiPi_z0SinTheta_VsCentralStateTopology[f][cst] = dynamic_cast<TH1F*>( file->Get( Form("mh_ExclusivePiPi_z0SinTheta_%d", cst) ) );
            mh_ExclusivePiPi_z0SinTheta_VsCentralStateTopology[f][cst]->SetDirectory(0);
            mh_ExclusivePiPi_Delta_z0_VsCentralStateTopology[f][cst] = dynamic_cast<TH1F*>( file->Get( Form("mh_ExclusivePiPi_Delta_z0_VsCentralStateTopology_%d", cst) ) );
            mh_ExclusivePiPi_Delta_z0_VsCentralStateTopology[f][cst]->SetDirectory(0);
        }
        
        mh_ExclusivePiPi_d0[f]->Reset("ICESM");
        mh_ExclusivePiPi_z0SinTheta[f]->Reset("ICESM");
        mh_ExclusivePiPi_Delta_z0[f]->Reset("ICESM");
        
        
        for(int cst=0; cst<CepUtil::nCentralStateTopologies; ++cst){
            if( f==MC_SIGNAL ){
                if( cst==CepUtil::k1ChargedPairNoNeutrals ){
                    mh_ExclusivePiPi_d0[f]->Add( mh_ExclusivePiPi_d0_VsCentralStateTopology[f][cst] );
                    mh_ExclusivePiPi_z0SinTheta[f]->Add( mh_ExclusivePiPi_z0SinTheta_VsCentralStateTopology[f][cst] );
                    mh_ExclusivePiPi_Delta_z0[f]->Add( mh_ExclusivePiPi_Delta_z0_VsCentralStateTopology[f][cst] );
                }
            } else
                if( f==MC_PYTHIA_CD_BKGD_NEUTRALS ){
                    if( cst==CepUtil::k1ChargedPairAndNeutrals ){
                        mh_ExclusivePiPi_d0[f]->Add( mh_ExclusivePiPi_d0_VsCentralStateTopology[f][cst], (mSubtractNeutrals && cst==CepUtil::k1ChargedPairAndNeutrals) ? mNeutralsFracToPreserve : 1.0 );
                        mh_ExclusivePiPi_z0SinTheta[f]->Add( mh_ExclusivePiPi_z0SinTheta_VsCentralStateTopology[f][cst], (mSubtractNeutrals && cst==CepUtil::k1ChargedPairAndNeutrals) ? mNeutralsFracToPreserve : 1.0 );
                        mh_ExclusivePiPi_Delta_z0[f]->Add( mh_ExclusivePiPi_Delta_z0_VsCentralStateTopology[f][cst], (mSubtractNeutrals && cst==CepUtil::k1ChargedPairAndNeutrals) ? mNeutralsFracToPreserve : 1.0 );
                    }
                } else
                {
                    if( cst!=CepUtil::k1ChargedPairNoNeutrals && cst!=CepUtil::kUnqualified && cst!=CepUtil::k1ChargedPairAndNeutrals ){
                        mh_ExclusivePiPi_d0[f]->Add( mh_ExclusivePiPi_d0_VsCentralStateTopology[f][cst] );
                        mh_ExclusivePiPi_z0SinTheta[f]->Add( mh_ExclusivePiPi_z0SinTheta_VsCentralStateTopology[f][cst] );
                        mh_ExclusivePiPi_Delta_z0[f]->Add( mh_ExclusivePiPi_Delta_z0_VsCentralStateTopology[f][cst] );
                    }
                }
        }
    }
    
    
    
    
    for(int i=0; i<CepUtil::nCharges2Trks; ++i){
        hNSigmaMissingPt[f][i] = dynamic_cast<TH1F*>( file->Get( Form("hNSigmaMissingPt%sSign", i==CepUtil::OPPO ? "Opposite" : "Same") ) );
        hNSigmaMissingPt[f][i] = rebinAndGetTH1F( hNSigmaMissingPt[f][i], i==CepUtil::OPPO ? missingPtBinningVec : missingPtBinningVec_SameSign );
        hMissingPx[f][i] = dynamic_cast<TH1F*>( file->Get( Form("hMissingPx%sSign", i==CepUtil::OPPO ? "Opposite" : "Same") ) );
        hMissingPx[f][i] = rebinAndGetTH1F( hMissingPx[f][i], i==CepUtil::OPPO ? missingPxBinningVec : missingPxBinningVec_SameSign );
        hMissingPy[f][i] = dynamic_cast<TH1F*>( file->Get( Form("hMissingPy%sSign", i==CepUtil::OPPO ? "Opposite" : "Same") ) );
        hMissingPy[f][i] = rebinAndGetTH1F( hMissingPy[f][i], i==CepUtil::OPPO ? missingPyBinningVec : missingPyBinningVec_SameSign );
        
        if( f!=DATA ){
            for(int j=0; j<CepUtil::nCentralStateTopologies; ++j){
                
                hNSigmaMissingPtVsTrueLevelTopology[f][i][j] = dynamic_cast<TH1F*>( file->Get( Form("hNSigmaMissingPtVsTrueLevelTopology_%d%d", i, j) ) );
                hNSigmaMissingPtVsTrueLevelTopology[f][i][j] = rebinAndGetTH1F( hNSigmaMissingPtVsTrueLevelTopology[f][i][j], i==CepUtil::OPPO ? missingPtBinningVec : missingPtBinningVec_SameSign );
                hMissingPxVsTrueLevelTopology[f][i][j] = dynamic_cast<TH1F*>( file->Get( Form("hMissingPxVsTrueLevelTopology_%d%d", i, j) ) );
                hMissingPxVsTrueLevelTopology[f][i][j] = rebinAndGetTH1F( hMissingPxVsTrueLevelTopology[f][i][j], i==CepUtil::OPPO ? missingPxBinningVec : missingPxBinningVec_SameSign );
                hMissingPyVsTrueLevelTopology[f][i][j] = dynamic_cast<TH1F*>( file->Get( Form("hMissingPyVsTrueLevelTopology_%d%d", i, j) ) );
                hMissingPyVsTrueLevelTopology[f][i][j] = rebinAndGetTH1F( hMissingPyVsTrueLevelTopology[f][i][j], i==CepUtil::OPPO ? missingPyBinningVec : missingPyBinningVec_SameSign );
                
                
                if(j==0){
                    hNSigmaMissingPt[f][i]->Reset("ICESM");
                    hMissingPx[f][i]->Reset("ICESM");
                    hMissingPy[f][i]->Reset("ICESM");
                }
                
                if( f==MC_SIGNAL ){
                    if( j==CepUtil::k1ChargedPairNoNeutrals ){
                        hNSigmaMissingPt[f][i]->Add( hNSigmaMissingPtVsTrueLevelTopology[f][i][j] );
                        hMissingPx[f][i]->Add( hMissingPxVsTrueLevelTopology[f][i][j] );
                        hMissingPy[f][i]->Add( hMissingPyVsTrueLevelTopology[f][i][j] );
                    }
                } else
                    if( f==MC_PYTHIA_CD_BKGD_NEUTRALS ){
                        if( j==CepUtil::k1ChargedPairAndNeutrals ){
                            hNSigmaMissingPt[f][i]->Add( hNSigmaMissingPtVsTrueLevelTopology[f][i][j], (mSubtractNeutrals && j==CepUtil::k1ChargedPairAndNeutrals) ? mNeutralsFracToPreserve : 1.0 );
                            hMissingPx[f][i]->Add( hMissingPxVsTrueLevelTopology[f][i][j], (mSubtractNeutrals && j==CepUtil::k1ChargedPairAndNeutrals) ? mNeutralsFracToPreserve : 1.0 );
                            hMissingPy[f][i]->Add( hMissingPyVsTrueLevelTopology[f][i][j], (mSubtractNeutrals && j==CepUtil::k1ChargedPairAndNeutrals) ? mNeutralsFracToPreserve : 1.0 );
                        }
                    } else
                    {
                        if( j!=CepUtil::k1ChargedPairNoNeutrals && j!=CepUtil::kUnqualified && j!=CepUtil::k1ChargedPairAndNeutrals ){
                            hNSigmaMissingPt[f][i]->Add( hNSigmaMissingPtVsTrueLevelTopology[f][i][j] );
                            hMissingPx[f][i]->Add( hMissingPxVsTrueLevelTopology[f][i][j] );
                            hMissingPy[f][i]->Add( hMissingPyVsTrueLevelTopology[f][i][j] );
                        }
                    }
                    
            }
            
        }
    }
    
/*    
    for(int s=0; s<CepUtil::nCharges2Trks; ++s){
      TH2F* hMissingPtVsDeltaPhiDegrees = dynamic_cast<TH2F*>( file->Get("ExclusiveEventSelector/MissingPtPidVsDeltaPhiDegrees_"+mUtil->qSum2TrksName(s)+"_pion") );
      hMissingPtVsDeltaPhiDegrees->SetDirectory(0);
      hMissingPtVsDeltaPhiDegrees->SetName( Form("%s_%d", hMissingPtVsDeltaPhiDegrees->GetName(), static_cast<int>(gRandom->Uniform()*100000) ) );
      TH1D* hMissingPt[nDeltaPhiRanges];
      hMissingPt[DELTAPHI_1] = hMissingPtVsDeltaPhiDegrees->ProjectionY(Form("missingPt_%s%d%d", mUtil->qSum2TrksName(s).Data(), f, DELTAPHI_1), 0, hMissingPtVsDeltaPhiDegrees->GetNbinsX()/2);
      hMissingPt[DELTAPHI_2] = hMissingPtVsDeltaPhiDegrees->ProjectionY(Form("missingPt_%s%d%d", mUtil->qSum2TrksName(s).Data(), f, DELTAPHI_2), hMissingPtVsDeltaPhiDegrees->GetNbinsX()/2+1, hMissingPtVsDeltaPhiDegrees->GetNbinsX()+1);
      
      
      for(int r=0; r<nDeltaPhiRanges; ++r){
        TH1F *hPtMissTH1F = TH1F_from_TH1( hMissingPt[r] );
        
        for(int i=TWOTOFTRKS; i<nAnalysisCuts; ++i){
          auto hptmis = s==CepUtil::OPPO ? mhMissingPt : mhMissingPt_SameSign;
          hptmis[f][i][r] = new TH1F( *hPtMissTH1F );
          hptmis[f][i][r]->SetName( Form("%s_%d", hptmis[f][i][r]->GetName(), static_cast<int>(gRandom->Uniform()*100000)) );
          hptmis[f][i][r]->SetDirectory(0);
          
//           if( f==TO_BE_DELETED_ONEONEONEONE && s==CepUtil::SAME ){
//             const double prevIntegral = hptmis[f][i][r]->Integral(0,-1);
//             hptmis[f][i][r]->Smooth(2);
//             const double currentIntegral = hptmis[f][i][r]->Integral(0,-1);
//             hptmis[f][i][r]->Scale( prevIntegral/currentIntegral );
//           }
          
          if( f==TO_BE_DELETED_ONEONEONEONE && r==DELTAPHI_1 )
            hptmis[f][i][r]->Reset("ICESM");
          
          
          //-----
          
          if( f!=DATA && f!=MC_SIGNAL ){
            mhInvMassPion[f][i][s][r] = dynamic_cast<TH1F*>( file->Get("CutFlowSelector/InvMassPion_"+mUtil->analysisCutShortName(i)+"_"+mUtil->qSum2TrksName(s)+"_"+Form("deltaPhi_Bin%d", r+1)) );
            mhInvMassPion[f][i][s][r]->SetDirectory(0);
          }
          
          if( f!=DATA ){
            mhDeltaPhiDegrees[f][i][s][r] = dynamic_cast<TH1F*>( file->Get("CutFlowSelector/DeltaPhiDegrees_"+mUtil->analysisCutShortName(i)+"_"+mUtil->qSum2TrksName(s)+"_"+Form("deltaPhi_Bin%d", r+1)) );
            mhDeltaPhiDegrees[f][i][s][r]->SetDirectory(0);
          }
          
        }
      }
    }

      
    for(int i=0; i<CepUtil::nCharges2Trks; ++i)
        for(int l=0; l<nDeltaPhiRanges; ++l)
          for(int j=0; j<CepUtil::nCentralStateTopologies; ++j){
            mhMissingPtPidVsDeltaPhiRangeVsTrueLevelTopology[f][i][l][j] = dynamic_cast<TH1F*>( file->Get("ExclusiveEventSelector/MissingPtPidVsDeltaPhiRangeVsTrueLevelTopology_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(PION)+"_"+Form("deltaPhi_Bin%d", l+1)+"_"+mUtil->trueLevelCentralStateTopologyName(j) ) );
            mhMissingPtPidVsDeltaPhiRangeVsTrueLevelTopology[f][i][l][j]->SetDirectory(0);
          }
    */

    file->Close();
  
  }
  
  
  cout << "done!" << endl;
}


void DataVsMC::setBlackAndWhiteColorScale(){
  const int nSteps = 100;
  int acceptancePalette[nSteps];
  const unsigned int Number = 2;
  double Red[Number]   = { 1.00, 0.00 };
  double Green[Number] = { 1.00, 0.00 };
  double Blue[Number]  = { 1.00, 0.00 };
  double Stops[Number] = { 0.00, 1.00 };
  
  int fi = TColor::CreateGradientColorTable(Number,Stops,Red,Green,Blue, nSteps);
  for(int i=0; i<nSteps; ++i)
    acceptancePalette[i] = fi+i;
  gStyle->SetPalette(nSteps, acceptancePalette);
}

void DataVsMC::setWhiteAndBlackColorScale(){
  const int nSteps = 100;
  int acceptancePalette[nSteps];
  const unsigned int Number = 2;
  double Red[Number]   = { 0.00, 1.00 };
  double Green[Number] = { 0.00, 1.00 };
  double Blue[Number]  = { 0.00, 1.00 };
  double Stops[Number] = { 0.00, 1.00 };
  
  int fi = TColor::CreateGradientColorTable(Number,Stops,Red,Green,Blue, nSteps);
  for(int i=0; i<nSteps; ++i)
    acceptancePalette[i] = fi+i;
  gStyle->SetPalette(nSteps, acceptancePalette);
}

void DataVsMC::setBlueWhiteRedColorScale(){
  const int nSteps = 100;
  int acceptancePalette[nSteps];
  const unsigned int Number = 3;
  double Red[Number]   = { 0.00, 1.00, 1.00 };
  double Green[Number] = { 0.00, 1.00, 0.00 };
  double Blue[Number]  = { 1.00, 1.00, 0.00 };
  double Stops[Number] = { 0.00, 0.50, 1.00 };
  
  int fi = TColor::CreateGradientColorTable(Number,Stops,Red,Green,Blue, nSteps);
  for(int i=0; i<nSteps; ++i)
    acceptancePalette[i] = fi+i;
  gStyle->SetPalette(nSteps, acceptancePalette);
}


void DataVsMC::setupDrawingStyle() const{
  // setup the drawing style
  gStyle->SetLineScalePS();
  gStyle->SetFrameBorderMode(0);
  gStyle->SetFrameFillColor(0);
  gStyle->SetFrameLineWidth(2);
  gStyle->SetLineWidth(2);
  gStyle->SetHatchesLineWidth(2);
  gStyle->SetCanvasBorderMode(0);
  gStyle->SetCanvasColor(0);
  gStyle->SetPadBorderMode(0);
  gStyle->SetPadColor(0);
  gStyle->SetStatColor(0);
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
  gStyle->SetNumberContours(99);
  gStyle->SetPalette(55);
  gStyle->SetHistLineWidth(3.);
  gStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes
  
  setBlackAndWhiteColorScale();
}

Double_t DataVsMC::integratePtMiss(const TH1* hNSigmaMissingPt, Double_t nSigmaCut) const{
  int binNum = 1; 
  for(int i=1; i<=hNSigmaMissingPt->GetNbinsX(); ++i) if(hNSigmaMissingPt->GetXaxis()->GetBinUpEdge(i) >= nSigmaCut ){ binNum = i; break; }
  double integral = hNSigmaMissingPt->Integral(1, binNum-1);
  integral += hNSigmaMissingPt->GetBinContent(binNum) * ( nSigmaCut - hNSigmaMissingPt->GetXaxis()->GetBinLowEdge(binNum) ) / hNSigmaMissingPt->GetXaxis()->GetBinWidth(binNum);
  return integral;
}

std::vector<float> DataVsMC::getBinsVectorF(const TAxis *axis) const{
  std::vector<float> binsVector;
  for( int i=1; i<=axis->GetNbins(); ++i)
    binsVector.push_back( axis->GetBinLowEdge( i ) );
  binsVector.push_back( axis->GetBinUpEdge( axis->GetNbins() ) );
  return binsVector;
}


std::vector<double> DataVsMC::getBinsVectorD(const TAxis *axis) const{
  std::vector<double> binsVector;
  for( int i=1; i<=axis->GetNbins(); ++i)
    binsVector.push_back( axis->GetBinLowEdge( i ) );
  binsVector.push_back( axis->GetBinUpEdge( axis->GetNbins() ) );
  return binsVector;
}
