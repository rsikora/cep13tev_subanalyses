#ifndef ObservableObject_hh
#define ObservableObject_hh

#include <TROOT.h>
#include <TFile.h>
#include <TString.h>
#include "Util.hh"

class TH2;
class TH1F;
class TH2F;

class ObservableObject{
    
public: 
//     ObservableObject(CepHistograms *, TString, std::vector<double> &);
//     inline int nBins() const{ return static_cast<int>(binsVec.size())-1; };
//     void FillQ(double);

    ObservableObject(TFile *, TString);
    ~ObservableObject();
    inline int nBins() const{ return hQ[Util::CH_SUM_0]->GetNbinsX(); };
    
    TH1F *hQ[Util::nChargeSum];
    TH1F *hQ_Weighted[Util::nChargeSum];
    TH1F *hQ_Weighted_Syst[Util::nChargeSum][Util::nSystChecks];
    TH2F *hNSigmaPtMissVsQ[Util::nChargeSum];
    TH2F *hNSigmaPtMissVsQ_Weighted[Util::nChargeSum];
    
//     CepHistograms *mAnalysis;
//     std::vector<double> binsVec;
    TString name;
  
  ClassDef(ObservableObject,0);
};

#endif
