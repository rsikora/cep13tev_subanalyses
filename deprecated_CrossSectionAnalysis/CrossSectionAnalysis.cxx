#define CrossSectionAnalysis_cxx

#include "CrossSectionAnalysis.hh"
#include <TH2.h> 
#include <TF1.h> 
#include <TF2.h> 
#include <THStack.h> 
#include <TStyle.h> 
#include <TMultiGraph.h> 
#include <TGraph.h> 
#include <TGraph2D.h> 
#include <TGraphAsymmErrors.h> 
#include <TEfficiency.h> 
#include <TFractionFitter.h>
#include <TObjArray.h>
#include <iostream>
#include <utility>
#include <sstream> 
#include <algorithm> 
#include <stdio.h> 
#include <stdlib.h> 
#include <TCanvas.h> 
#include <TNamed.h>
#include <TLegend.h> 
#include <TBox.h>
#include <TGaxis.h> 
#include <vector> 
#include <fstream> 
#include <TString.h> 
#include <TColor.h> 
#include <TLine.h> 
#include <cmath> 
#include <TExec.h>
#include <TEllipse.h>
#include <TFitResultPtr.h> 
#include <TFitResult.h> 
#include <TLatex.h> 
#include <TExec.h>
#include <TMath.h>
#include <TArrow.h>
#include <TF12.h>
#include <TPaletteAxis.h>
#include <TRandom3.h>

ClassImp(CrossSectionAnalysis)


CrossSectionAnalysis::CrossSectionAnalysis(TString option){
    mUtil = Util::instance(option);
    //   mParams = Parameters::instance(option);
    //   mEff = Efficiencies::instance(mParams, option);
    
    mSpecialLabelRatio = false;
    mSpecialLabelOffsetFactor = 1.0;
    mEnableLegend = false;
    mSpecialLegendMarginFactor = 1.0;
    
    mSystCheckName = new TString[Util::nSystChecks];
    mSystCheckShortName = new TString[Util::nSystChecks];
    
    mSystCheckName[Util::DEAD_MAT_UP] = TString("IDRecoEff_DeadMaterial_UP");
    mSystCheckName[Util::DEAD_MAT_DOWN] = TString("IDRecoEff_DeadMaterial_DOWN");
    mSystCheckName[Util::ID_MIGR_UP] = TString("IDTracks_Migrations_UP");
    mSystCheckName[Util::ID_MIGR_DOWN] = TString("IDTracks_Migrations_DOWN");
    mSystCheckName[Util::RP_EFF_UP] = TString("RPRecoEff_UP");
    mSystCheckName[Util::RP_EFF_DOWN] = TString("RPRecoEff_DOWN");
    mSystCheckName[Util::RP_MIGR_UP] = TString("RPTracks_Migrations_UP");
    mSystCheckName[Util::RP_MIGR_DOWN] = TString("RPTracks_Migrations_DOWN");
    mSystCheckName[Util::NONEXCL_BKGD_UP] = TString("NonExclBkgd_UP");
    mSystCheckName[Util::NONEXCL_BKGD_DOWN] = TString("NonExclBkgd_DOWN");
    mSystCheckName[Util::SPTRK_EFF_UP] = TString("SpTrkEff_UP");
    mSystCheckName[Util::SPTRK_EFF_DOWN] = TString("SpTrkEff_DOWN");
//     mSystCheckName[Util::PILEUPVETO_EFF_UP] = TString("PileUpVetoEff_UP");
//     mSystCheckName[Util::PILEUPVETO_EFF_DOWN] = TString("PileUpVetoEff_DOWN");
    
    mSystCheckShortName[Util::DEAD_MAT_UP] = TString("#Delta#varepsilon_{ID} (dead mat.)");
    mSystCheckShortName[Util::DEAD_MAT_DOWN] = TString("#Delta#varepsilon_{ID} (dead mat.)");
    mSystCheckShortName[Util::ID_MIGR_UP] = TString("#DeltaC_{m,ID}");
    mSystCheckShortName[Util::ID_MIGR_DOWN] = TString("#DeltaC_{m,ID}");
    mSystCheckShortName[Util::RP_EFF_UP] = TString("#Delta#varepsilon_{ALFA}");
    mSystCheckShortName[Util::RP_EFF_DOWN] = TString("#Delta#varepsilon_{ALFA}");
    mSystCheckShortName[Util::RP_MIGR_UP] = TString("#DeltaC_{m,ALFA}");
    mSystCheckShortName[Util::RP_MIGR_DOWN] = TString("#DeltaC_{m,ALFA}");
    mSystCheckShortName[Util::NONEXCL_BKGD_UP] = TString("#DeltaN_{bkgd}");
    mSystCheckShortName[Util::NONEXCL_BKGD_DOWN] = TString("#DeltaN_{bkgd}");
    mSystCheckShortName[Util::SPTRK_EFF_UP] = TString("#Delta#varepsilon_{SpTrk}");
    mSystCheckShortName[Util::SPTRK_EFF_DOWN] = TString("#Delta#varepsilon_{SpTrk}");
//     mSystCheckShortName[Util::PILEUPVETO_EFF_UP] = TString("#Delta#varepsilon_{PUveto}");
//     mSystCheckShortName[Util::PILEUPVETO_EFF_DOWN] = TString("#Delta#varepsilon_{PUveto}");
}

CrossSectionAnalysis::~CrossSectionAnalysis(){}


void CrossSectionAnalysis::analyze(){
    
    
    const double pion6PythiaScaleFactor = 1./570;
    const double pion8PythiaScaleFactor = 1./570;
    
    gROOT->SetStyle("ATLAS");
    gStyle->SetErrorX(0.5); //to draw horizontal lines despite the ATLAS style
    
    colors.push_back( kBlack );
    colors.push_back( kRed );
    colors.push_back( kGreen+2);
    colors.push_back( kBlue  );
    colors.push_back( kOrange );
    
    TFile *file = TFile::Open( "ROOT_files/all.root" );
    
    mhNEventsVsLumiBlockVsRunNumber = dynamic_cast<TH2F*>(file->Get("mhNEventsVsLumiBlockVsRunNumber"));
    mhNEventsVsLumiBlockVsRunNumber->SetDirectory(0);
    for(int i=0; i<Util::nProtonsConfigurations; ++i){
        mhN2PiCepEventsVsLumiBlockVsRunNumber[i] = dynamic_cast<TH2F*>(file->Get(Form("mhN2PiCepEventsVsLumiBlockVsRunNumber_%d", i)));
        mhN2PiCepEventsVsLumiBlockVsRunNumber[i]->SetDirectory(0);
        mhN2PiCepEventsVsLumiBlockVsRunNumber_Weighted[i] = dynamic_cast<TH2F*>(file->Get(Form("mhN2PiCepEventsVsLumiBlockVsRunNumber_Weighted_%d", i)));
        mhN2PiCepEventsVsLumiBlockVsRunNumber_Weighted[i]->SetDirectory(0);
        mhN4PiCepEventsVsLumiBlockVsRunNumber[i] = dynamic_cast<TH2F*>(file->Get(Form("mhN4PiCepEventsVsLumiBlockVsRunNumber_%d", i)));
        mhN4PiCepEventsVsLumiBlockVsRunNumber[i]->SetDirectory(0);
        mhN4PiCepEventsVsLumiBlockVsRunNumber_Weighted[i] = dynamic_cast<TH2F*>(file->Get(Form("mhN4PiCepEventsVsLumiBlockVsRunNumber_Weighted_%d", i)));
        mhN4PiCepEventsVsLumiBlockVsRunNumber_Weighted[i]->SetDirectory(0);
    }
    
    mhNSigmaMissingPtVsDeltaZ0VsMinPt_2Pi = dynamic_cast<TH3F*>(file->Get("mhNSigmaMissingPtVsDeltaZ0VsMinPt_2Pi"));
    mhNSigmaMissingPtVsDeltaZ0VsMinPt_2Pi->SetDirectory(0);
    mhNSigmaMissingPtVsDeltaZ0VsMinPt_4Pi = dynamic_cast<TH3F*>(file->Get("mhNSigmaMissingPtVsDeltaZ0VsMinPt_4Pi"));
    mhNSigmaMissingPtVsDeltaZ0VsMinPt_4Pi->SetDirectory(0);
    mhNSigmaMissingPtVsDeltaZ0VsMinPt_6Pi = dynamic_cast<TH3F*>(file->Get("mhNSigmaMissingPtVsDeltaZ0VsMinPt_6Pi"));
    mhNSigmaMissingPtVsDeltaZ0VsMinPt_6Pi->SetDirectory(0);
    mhNSigmaMissingPtVsDeltaZ0VsMinPt_8Pi = dynamic_cast<TH3F*>(file->Get("mhNSigmaMissingPtVsDeltaZ0VsMinPt_8Pi"));
    mhNSigmaMissingPtVsDeltaZ0VsMinPt_8Pi->SetDirectory(0);
    
    //-- 2Pi
    
    oo2PiInvMass = new ObservableObject(file, "2PiInvMass");
    oo2PiRapidity = new ObservableObject(file, "2PiRapidity");
    oo2PiDeltaPhi = new ObservableObject(file, "2PiDeltaPhi");
    oo2PiMandelstamTSum = new ObservableObject(file, "2PiMandelstamTSum");
  
    for(int rf=0; rf<Util::nReferenceFrames; ++rf){
        oo2PiCosTheta[rf] = new ObservableObject(file, Form("2PiCosTheta_RF%d", rf));
        oo2PiPhi[rf] = new ObservableObject(file, Form("2PiPhi_RF%d", rf));
    }
    
    for(int i=0; i<Util::nDeltaPhiRanges; ++i)
        oo2PiInvMass_DeltaPhiBins[i] = new ObservableObject(file, Form("2PiInvMass_DeltaPhiBins%d", i));
    
    for(int i=0; i<2; ++i)
        oo2PiInvMass_DPtBins_DeltaPhiLessThan90Deg[i] = new ObservableObject(file, Form("2PiInvMass_DPtBins%d_DeltaPhiLessThan90Deg", i));
    
    for(int i=0; i<Util::nCentralTracksEtaConf; ++i)
        oo2PiInvMass_CentralTracksEtaBins[i] = new ObservableObject(file, Form("2PiInvMass_CentralTracksEtaBins%d", i));
  
    for(int i=0; i<Util::nMassRanges; ++i){
        oo2PiRapidity_MassBins[i] = new ObservableObject(file, Form("2PiRapidity_MassBins%d", i));
        oo2PiDeltaPhi_MassBins[i] = new ObservableObject(file, Form("2PiDeltaPhi_MassBins%d", i));
        oo2PiMandelstamTSum_MassBins[i] = new ObservableObject(file, Form("2PiMandelstamTSum_MassBins%d", i));
        for(int rf=0; rf<Util::nReferenceFrames; ++rf){
            oo2PiCosTheta_MassBins[rf][i] = new ObservableObject(file, Form("2PiCosTheta_RF%d_MassBins%d", rf, i));
            oo2PiPhi_MassBins[rf][i] = new ObservableObject(file, Form("2PiPhi_RF%d_MassBins%d", rf, i));
        }
    }
    
    
    for(int f=0; f<nFilesWithAcceptanceCorr; ++f){
        for(int i=0; i<4; ++i){
            for(int j=0; j<Util::nMassRanges; ++j){
                oo2PiMandelstamT_CVsA_ForSlopeExtraction_DeltaPhiBins_MassBins[f][i][j] = new ObservableObject(file, Form("oo2PiMandelstamT_CVsA_ForSlopeExtraction_DeltaPhiBins%d_MassBins%d_%d", i, j, f) );
            }
        }
    }
    
    oo4PiInvMass = new ObservableObject(file, "4PiInvMass");
    oo4PiInvMass_MinMassOf2Pi = new ObservableObject(file, "4PiInvMass_MinMassOf2Pi");
    oo4PiInvMass_MaxMassOf2Pi = new ObservableObject(file, "4PiInvMass_MaxMassOf2Pi");
    oo4PiRapidity = new ObservableObject(file, "4PiRapidity");
    oo4PiDeltaPhi = new ObservableObject(file, "4PiDeltaPhi");
    oo4PiMandelstamTSum = new ObservableObject(file, "4PiMandelstamTSum");
    
    for(int i=0; i<Util::nDeltaPhiRanges; ++i){
        oo4PiInvMass_DeltaPhiBins[i] = new ObservableObject(file, Form("4PiInvMass_DeltaPhiBins%d", i));
        oo4PiInvMass_MinMassOf2Pi_DeltaPhiBins[i] = new ObservableObject(file, Form("4PiInvMass_MinMassOf2Pi_DeltaPhiBins%d", i));
        oo4PiInvMass_MaxMassOf2Pi_DeltaPhiBins[i] = new ObservableObject(file, Form("4PiInvMass_MaxMassOf2Pi_DeltaPhiBins%d", i));
        oo4PiInvMass_DeltaPhiBins_FineBinning[i] = new ObservableObject(file, Form("4PiInvMass_DeltaPhiBins%d_FineBinning", i));
    }
  
    for(int i=0; i<Util::nMassRanges; ++i){
        oo4PiRapidity_MassBins[i] = new ObservableObject(file, Form("4PiRapidity_MassBins%d", i));
        oo4PiDeltaPhi_MassBins[i] = new ObservableObject(file, Form("4PiDeltaPhi_MassBins%d", i));
        oo4PiMandelstamTSum_MassBins[i] = new ObservableObject(file, Form("4PiMandelstamTSum_MassBins%d", i));
    }
    
    for(int f=0; f<nFilesWithAcceptanceCorr; ++f){
        for(int i=0; i<Util::nDeltaPhiRanges; ++i){
            for(int j=0; j<Util::nMassRanges; ++j){
                oo4PiMandelstamT_CVsA_ForSlopeExtraction_DeltaPhiBins_MassBins[f][i][j] = new ObservableObject(file, Form("oo4PiMandelstamT_CVsA_ForSlopeExtraction_DeltaPhiBins%d_MassBins%d_%d", i, j, f) );
            }
        }
    }
    
    
    oo6PiInvMass = new ObservableObject(file, "6PiInvMass");
    oo6PiRapidity = new ObservableObject(file, "6PiRapidity");
    oo6PiDeltaPhi = new ObservableObject(file, "6PiDeltaPhi");
    oo6PiMandelstamTSum = new ObservableObject(file, "6PiMandelstamTSum");
    
    
    oo8PiInvMass = new ObservableObject(file, "8PiInvMass");
    oo8PiRapidity = new ObservableObject(file, "8PiRapidity");
    oo8PiDeltaPhi = new ObservableObject(file, "8PiDeltaPhi");
    oo8PiMandelstamTSum = new ObservableObject(file, "8PiMandelstamTSum");
    
    
    oo2PiIntegratedXSec = new ObservableObject(file, "2PiIntegratedXSec");
    oo4PiIntegratedXSec = new ObservableObject(file, "4PiIntegratedXSec");
    oo6PiIntegratedXSec = new ObservableObject(file, "6PiIntegratedXSec");
    oo8PiIntegratedXSec = new ObservableObject(file, "8PiIntegratedXSec");
    
    
    file->Close();
    
    getListOfAnalysedRuns();
    readLumiFiles();
    getTotalIntegratedLuminosity();
    
    cout << "Lumi = " << totalIntegratedLuminosity << " mub^-1" <<  endl;

    
    pullAnalysis();
//     return;
    
    calculateDeltaZ0Eff();
    
    drawResolutions();
    
    drawNSigmaMissingPt();
    
    // --------- --------- --------- --------- --------- --------- --------- --------- ---------
    
    
    std::vector< TFile* > filesMC[Util::nDefinedParticles];
    std::vector< int > colorsMC[Util::nDefinedParticles];
    std::vector< TString > legendStrMC[Util::nDefinedParticles];
    
    
    
    filesMC[Util::PION].push_back( new TFile("ROOT_files/Output_pion_DiMe_CepTree_pipi.root", "READ") ); colorsMC[Util::PION].push_back( kGreen+2 );    legendStrMC[Util::PION].push_back(TString("DiMe"));
    filesMC[Util::PION].push_back( new TFile("ROOT_files/Output_pion_GenEx_Lambda1p0.root", "READ") ); colorsMC[Util::PION].push_back( kBlue );    legendStrMC[Util::PION].push_back(TString("GenEx #times 0.1(Abs.)"));
    filesMC[Util::PION].push_back( new TFile("ROOT_files/Output_pion_Pythia8244.root", "READ") ); colorsMC[Util::PION].push_back( kRed );    legendStrMC[Util::PION].push_back(TString("Pythia 8 MBR #times 0.25"));
    
    filesMC[Util::PION].push_back( new TFile("ROOT_files/Output_pion_Pythia8244_4Prong.root", "READ") ); colorsMC[Util::PION].push_back( kRed );    legendStrMC[Util::PION].push_back(TString("Pythia 8 MBR"));
    filesMC[Util::PION].push_back( new TFile("ROOT_files/Output_pion_DiMe_CepTree_rhorho.root", "READ") ); colorsMC[Util::PION].push_back( kGreen+2 );    legendStrMC[Util::PION].push_back(TString("DiMe #rho#rho (#Lambda_{#lower[-0.38]{exp}}^{2} = 1 GeV^{2}) #times 100"));
    filesMC[Util::PION].push_back( new TFile("ROOT_files/Output_pion_DiMe_CepTree_rhorho_b_0p45.root", "READ") ); colorsMC[Util::PION].push_back( kAzure+7 );    legendStrMC[Util::PION].push_back(TString("DiMe #rho#rho (#Lambda_{#lower[-0.38]{exp}}^{2} = 2.2 GeV^{2}) #times 5"));
    
    filesMC[Util::PION].push_back( new TFile("ROOT_files/Output_6Pi_8Pi_Pythia8244.root", "READ") ); colorsMC[Util::PION].push_back( kRed );    legendStrMC[Util::PION].push_back(TString("Pythia 8 MBR"));
    
    
    std::vector< TFile* > filesMC_angularStudies[Util::nDefinedParticles];
    std::vector< int > colorsMC_angularStudies[Util::nDefinedParticles];
    std::vector< TString > legendStrMC_angularStudies[Util::nDefinedParticles];
  
    filesMC_angularStudies[Util::PION].push_back( new TFile("ROOT_files/Output_pion_GenEx_Lambda1p0_noPtEtaCut.root", "READ") ); colorsMC_angularStudies[Util::PION].push_back( kCyan-7 );    legendStrMC_angularStudies[Util::PION].push_back(TString("S_{0} wave"));
    filesMC_angularStudies[Util::PION].push_back( new TFile("ROOT_files/Output_pion_GenEx_Lambda1p0_noPtEtaCut.root", "READ") ); colorsMC_angularStudies[Util::PION].push_back( kOrange -3 );    legendStrMC_angularStudies[Util::PION].push_back(TString("D_{0} wave"));
    
    
    
    const int pid = Util::PION;
    
      
    TString pairStr(/*pid==PION ?*/ "#pi^{+}#pi^{-}" /*: (pid==KAON ? "#it{K}^{+}#it{K}^{-}" : "#it{p}#bar{#it{p}}")*/ );
    TString fourPionStr(/*pid==PION ?*/ "2#pi^{+}2#pi^{-}" /*: (pid==KAON ? "#it{K}^{+}#it{K}^{-}" : "#it{p}#bar{#it{p}}")*/ );
    TString sixPionStr(/*pid==PION ?*/ "3#pi^{+}3#pi^{-}" /*: (pid==KAON ? "#it{K}^{+}#it{K}^{-}" : "#it{p}#bar{#it{p}}")*/ );
    TString eightPionStr(/*pid==PION ?*/ "4#pi^{+}4#pi^{-}" /*: (pid==KAON ? "#it{K}^{+}#it{K}^{-}" : "#it{p}#bar{#it{p}}")*/ );
    TString singleTrkPlusMinusStr;
    TString singleTrkStr[Util::nSigns];
    singleTrkStr[Util::MINUS] = TString(/*pid==PION ? */"#pi^{-}"/* : (pid==KAON ? "#it{K}^{-}" : "#bar{#it{p}}") */);
    singleTrkStr[Util::PLUS] = TString(/*pid==PION ? */"#pi^{+}" /*: (pid==KAON ? "#it{K}^{+}" : "#it{p}") */);
    singleTrkPlusMinusStr = TString(/*pid==PION ? */"#pi^{+}, #pi^{-}:"/* : (pid==KAON ? "#it{K}^{+}, #it{K}^{-}:" : "#it{p}, #bar{#it{p}}:") */);

    
    
    bool skipCrossSections = /*true*/false;
    
    
    
    for(int num=0; num<4; ++num){
        ObservableObject *ooTmp;
        switch(num){
            case 0: ooTmp = oo2PiIntegratedXSec; break;
            case 1: ooTmp = oo4PiIntegratedXSec; break;
            case 2: ooTmp = oo6PiIntegratedXSec; break;
            case 3: ooTmp = oo8PiIntegratedXSec; break;
        }
        
        TH1F *referenceHist = new TH1F( *ooTmp->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *ooTmp->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( ooTmp->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( ooTmp->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, ooTmp->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );
        
        SystematicsOutput Systematics_2PiInvMass = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
//         cout << referenceHist->GetBinContent(1)*referenceHist->GetBinWidth(1) << "\t\t" << referenceHist->GetBinContent( 2)*referenceHist->GetBinWidth( 2) << endl;
//         cout << referenceHist->GetBinError(1)*referenceHist->GetBinWidth(1) << "\t\t" << referenceHist->GetBinError( 2)*referenceHist->GetBinWidth( 2) << endl;
        
        const double lumiError = Util::luminosityUncertainty;
        double syst1_Down = -referenceHist->GetBinContent(1)*referenceHist->GetBinWidth(1) * sqrt(pow(Systematics_2PiInvMass.mhSystErrorDown->GetBinContent(1),2) + pow(lumiError/(1.0+lumiError),2));
        double syst1_Up = referenceHist->GetBinContent(1)*referenceHist->GetBinWidth(1) * sqrt(pow(Systematics_2PiInvMass.mhSystErrorUp->GetBinContent(1),2) + pow(lumiError/(1.0-lumiError),2));
        double syst2_Down = -referenceHist->GetBinContent(2)*referenceHist->GetBinWidth(2) * sqrt(pow(Systematics_2PiInvMass.mhSystErrorDown->GetBinContent(2),2) + pow(lumiError/(1.0+lumiError),2));
        double syst2_Up = referenceHist->GetBinContent(2)*referenceHist->GetBinWidth(2) * sqrt(pow(Systematics_2PiInvMass.mhSystErrorUp->GetBinContent(2),2) + pow(lumiError/(1.0-lumiError),2));
        
//         cout << syst1_Up << " " << syst1_Down << "\t\t" << syst2_Up << " " << syst2_Down << endl;
//         cout << "\n" << endl;
        
        TH1D *nsigptmiss = ooTmp->hNSigmaPtMissVsQ[Util::OPPO]->ProjectionY();
        TF1 * func = new TF1();
        double evntsIntegral = nsigptmiss->Integral(0, nsigptmiss->GetXaxis()->FindBin(2.999));
        mUtil->bkgdFraction(nsigptmiss, 3, 5, 10, func);
        double bkgdFraction = func->Integral(0,3)/nsigptmiss->GetBinWidth(1) / evntsIntegral;
        
        cout << evntsIntegral << " " << bkgdFraction*evntsIntegral << " (" << bkgdFraction << ")" << endl;
    }
    

    
    
    if( !skipCrossSections ){

    {
        TH1F *referenceHist = new TH1F( *oo2PiInvMass->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo2PiInvMass->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo2PiInvMass->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo2PiInvMass->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo2PiInvMass->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_2PiInvMass = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsInvMassPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
        }
        
        //drawing final result
        DrawingOptions opts;
        opts.mSystErrorBoxWidthAdjustmentFactor = 0.7;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.12;
        opts.mRightMargin = 0.035;
        opts.mTopMargin = 0.02;
        opts.mBottomMargin = 0.13;
        
        //x axis
        opts.mXmin = referenceHist->GetXaxis()->GetXmin();
        opts.mXmax = 3.5;
        opts.mXaxisTitle = TString("m("+pairStr+") [GeV]");
        opts.mXaxisTitleSize = 0.05;
        opts.mXaxisTitleOffset = 1.04;
        opts.mXaxisLabelSize = 0.05;
        opts.mXaxisLabelOffset = 0.007;
        opts.mXaxisTickLength = 0.015;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 1.5 * referenceHist->GetMaximum();
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/dm("+pairStr+") [nb/GeV]" : "d#sigma/dm("+pairStr+") [#mub/GeV]");
        opts.mYaxisTitleSize = 0.05;
        opts.mYaxisTitleOffset = 1.21;
        opts.mYaxisLabelSize = 0.05;
        opts.mYaxisLabelOffset = 0.007;
        opts.mYaxisTickLength = 0.015;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.0;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString("FinalResult_InvMass_2Pi");
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
        
        opts.mInsert = true;
        opts.mX1Insert = 0.43;
        opts.mX2Insert = 0.95;
        opts.mY1Insert = 0.15;
        opts.mY2Insert = 0.56;
        opts.mXMinInstert = 1.5;
        opts.mXMaxInstert = 3.5;
        
        opts.mNColumns = 2;
        opts.mLegendX = 0.43;
        opts.mLegendXwidth = 0.45;
        opts.mLegendY = 0.61;
        opts.mLegendYwidth = 0.14;
        opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        
        //       opts.mXpositionSystErrorBox.push_back(0.4);
        opts.mXpositionSystErrorBox.push_back(0.66);
        //       opts.mXpositionSystErrorBox.push_back(0.8);
        opts.mXpositionSystErrorBox.push_back(1.02);
        opts.mXpositionSystErrorBox.push_back(1.26);
        opts.mXpositionSystErrorBox.push_back(2.15);
        
        TLatex lReaction(.4, .91, "#kern[-0.29]{#font[72]{ATLAS} Internal     #it{p}+#it{p}#rightarrow#it{p'}+"+pairStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
    //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //     opts.mTextToDraw.push_back( lATLAS );
        
        TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
        TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}^{+},p_{T}^{-}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
        TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
        TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
        TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
        TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
        TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
        TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
        TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
        TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        drawFinalResult( referenceHist, opts, Systematics_2PiInvMass );
        
    }
    
    
    
    
    
    
    {
        TH1F *referenceHist = new TH1F( *oo4PiInvMass->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo4PiInvMass->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo4PiInvMass->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo4PiInvMass->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo4PiInvMass->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_4PiInvMass = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsInvMassPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
        }
        
        //drawing final result
        DrawingOptions opts;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.12;
        opts.mRightMargin = 0.035;
        opts.mTopMargin = 0.02;
        opts.mBottomMargin = 0.13;
        
        //x axis
        opts.mXmin = referenceHist->GetXaxis()->GetXmin();
        opts.mXmax = 5.0;
        opts.mXaxisTitle = TString("m("+fourPionStr+") [GeV]");
        opts.mXaxisTitleSize = 0.05;
        opts.mXaxisTitleOffset = 1.04;
        opts.mXaxisLabelSize = 0.05;
        opts.mXaxisLabelOffset = 0.007;
        opts.mXaxisTickLength = 0.015;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 1.5 * referenceHist->GetMaximum();
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/dm("+fourPionStr+") [nb/GeV]" : "d#sigma/dm("+fourPionStr+") [#mub/GeV]");
        opts.mYaxisTitleSize = 0.05;
        opts.mYaxisTitleOffset = 1.21;
        opts.mYaxisLabelSize = 0.05;
        opts.mYaxisLabelOffset = 0.007;
        opts.mYaxisTickLength = 0.015;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.2;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString("FinalResult_InvMass_4Pi");
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxWidthAdjustmentFactor = 0.7;
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
        

        opts.mInsert = true;
        opts.mYlogInsert = true;
        opts.mX1Insert = 0.53;
        opts.mX2Insert = 0.95;
        opts.mY1Insert = 0.25;
        opts.mY2Insert = 0.58;
        opts.mXMinInstert = 5.0;
        opts.mXMaxInstert = 15.0;
        opts.mYMinInstert = 0.01;
        
        opts.mNColumns = 1;
        opts.mLegendX = 0.52;
        opts.mLegendXwidth = 0.17;
        opts.mLegendY = 0.61;
        opts.mLegendYwidth = 0.21;
        opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;     
        
//         opts.mNColumns = 1;
//         opts.mLegendX = 0.53;
//         opts.mLegendXwidth = 0.15;
//         opts.mLegendY = 0.41;
//         opts.mLegendYwidth = 0.34;
//         opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        
        //       opts.mXpositionSystErrorBox.push_back(0.4);
        opts.mXpositionSystErrorBox.push_back(0.66);
        //       opts.mXpositionSystErrorBox.push_back(0.8);
        opts.mXpositionSystErrorBox.push_back(1.02);
        opts.mXpositionSystErrorBox.push_back(1.26);
        opts.mXpositionSystErrorBox.push_back(2.15);
        
        TLatex lReaction(.34, .91, "#kern[-0.29]{#font[72]{ATLAS} Internal     #it{p}+#it{p}#rightarrow#it{p'}+"+fourPionStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
    //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //     opts.mTextToDraw.push_back( lATLAS );
        
        TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
        TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
        TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
        TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
        TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
        TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
        TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
        TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
        TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
        TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        drawFinalResult( referenceHist, opts, Systematics_4PiInvMass );
        
    }
    
    

    {
        TH1F *referenceHist = new TH1F( *oo6PiInvMass->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo6PiInvMass->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo6PiInvMass->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo6PiInvMass->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo6PiInvMass->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_6PiInvMass = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsInvMassPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
            histMC[fileId]->Scale(pion6PythiaScaleFactor);
        }
        
        //drawing final result
        DrawingOptions opts;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.12;
        opts.mRightMargin = 0.035;
        opts.mTopMargin = 0.02;
        opts.mBottomMargin = 0.13;
        
        //x axis
        opts.mXmin = referenceHist->GetXaxis()->GetXmin();
        opts.mXmax = 10.0;
        opts.mXaxisTitle = TString("m("+sixPionStr+") [GeV]");
        opts.mXaxisTitleSize = 0.05;
        opts.mXaxisTitleOffset = 1.04;
        opts.mXaxisLabelSize = 0.05;
        opts.mXaxisLabelOffset = 0.007;
        opts.mXaxisTickLength = 0.015;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 1.8 * referenceHist->GetMaximum();
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/dm("+sixPionStr+") [nb/GeV]" : "d#sigma/dm("+sixPionStr+") [#mub/GeV]");
        opts.mYaxisTitleSize = 0.05;
        opts.mYaxisTitleOffset = 1.21;
        opts.mYaxisLabelSize = 0.05;
        opts.mYaxisLabelOffset = 0.007;
        opts.mYaxisTickLength = 0.015;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.6;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString("FinalResult_InvMass_6Pi");
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxWidthAdjustmentFactor = 0.7;
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
        
//         opts.mInsert = true;
//         opts.mYlogInsert = true;
//         opts.mX1Insert = 0.53;
//         opts.mX2Insert = 0.95;
//         opts.mY1Insert = 0.27;
//         opts.mY2Insert = 0.60;
//         opts.mXMinInstert = 5.0;
//         opts.mXMaxInstert = 15.0;
//         opts.mYMinInstert = 0.01;
        
//         opts.mNColumns = 2;
//         opts.mLegendX = 0.43;
//         opts.mLegendXwidth = 0.45;
//         opts.mLegendY = 0.61;
//         opts.mLegendYwidth = 0.14;
//         opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        opts.mNColumns = 1;
        opts.mLegendX = 0.56;
        opts.mLegendXwidth = 0.22;
        opts.mLegendY = 0.4;
        opts.mLegendYwidth = 0.2;
        opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;

        opts.mSystErrorBoxWidthAdjustmentFactor = 0.7;
        opts.mXpositionSystErrorBox.push_back(2.15);
        opts.mXpositionSystErrorBox.push_back(3.15);
        opts.mXpositionSystErrorBox.push_back(5.15);
        
        TLatex lReaction(.4, .91, "#it{p}+#it{p}#rightarrow#it{p'}+"+sixPionStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
    //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //     opts.mTextToDraw.push_back( lATLAS );
        
        TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
        TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
        TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
        TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
        TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
        TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
        TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
        TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
        TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
        TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        drawFinalResult( referenceHist, opts, Systematics_6PiInvMass );
        
    }
    

    
    
    
    

    {
        TH1F *referenceHist = new TH1F( *oo8PiInvMass->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo8PiInvMass->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo8PiInvMass->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo8PiInvMass->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo8PiInvMass->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_8PiInvMass = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsInvMassPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
            histMC[fileId]->Scale(pion8PythiaScaleFactor);
        }
        
        //drawing final result
        DrawingOptions opts;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.12;
        opts.mRightMargin = 0.035;
        opts.mTopMargin = 0.02;
        opts.mBottomMargin = 0.13;
        
        //x axis
        opts.mXmin = referenceHist->GetXaxis()->GetXmin();
        opts.mXmax = 10.0;
        opts.mXaxisTitle = TString("m("+eightPionStr+") [GeV]");
        opts.mXaxisTitleSize = 0.05;
        opts.mXaxisTitleOffset = 1.04;
        opts.mXaxisLabelSize = 0.05;
        opts.mXaxisLabelOffset = 0.007;
        opts.mXaxisTickLength = 0.015;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 1.8 * referenceHist->GetMaximum();
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/dm("+eightPionStr+") [nb/GeV]" : "d#sigma/dm("+eightPionStr+") [#mub/GeV]");
        opts.mYaxisTitleSize = 0.05;
        opts.mYaxisTitleOffset = 1.21;
        opts.mYaxisLabelSize = 0.05;
        opts.mYaxisLabelOffset = 0.007;
        opts.mYaxisTickLength = 0.015;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.6;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString("FinalResult_InvMass_8Pi");
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxWidthAdjustmentFactor = 0.7;
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
        
//         opts.mInsert = true;
//         opts.mYlogInsert = true;
//         opts.mX1Insert = 0.53;
//         opts.mX2Insert = 0.95;
//         opts.mY1Insert = 0.27;
//         opts.mY2Insert = 0.60;
//         opts.mXMinInstert = 5.0;
//         opts.mXMaxInstert = 15.0;
//         opts.mYMinInstert = 0.01;
        
//         opts.mNColumns = 2;
//         opts.mLegendX = 0.43;
//         opts.mLegendXwidth = 0.45;
//         opts.mLegendY = 0.61;
//         opts.mLegendYwidth = 0.14;
//         opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        opts.mNColumns = 1;
        opts.mLegendX = 0.59;
        opts.mLegendXwidth = 0.22;
        opts.mLegendY = 0.49;
        opts.mLegendYwidth = 0.2;
        opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        
        opts.mSystErrorBoxWidthAdjustmentFactor = 0.7;
        
        opts.mXpositionSystErrorBox.push_back(3.15);
        opts.mXpositionSystErrorBox.push_back(5.15);
        opts.mXpositionSystErrorBox.push_back(7.15);
        
        TLatex lReaction(.4, .91, "#it{p}+#it{p}#rightarrow#it{p'}+"+eightPionStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
    //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //     opts.mTextToDraw.push_back( lATLAS );
        
        TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
        TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
        TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
        TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
        TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
        TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
        TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
        TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
        TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
        TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        drawFinalResult( referenceHist, opts, Systematics_8PiInvMass );
        
    }
    
    
    
    
    
    {
        TH1F *referenceHist = new TH1F( *oo4PiInvMass_MinMassOf2Pi->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo4PiInvMass_MinMassOf2Pi->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo4PiInvMass_MinMassOf2Pi->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo4PiInvMass_MinMassOf2Pi->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo4PiInvMass_MinMassOf2Pi->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_4PiInvMass = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsInvMassPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
        }
        
        //drawing final result
        DrawingOptions opts;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.12;
        opts.mRightMargin = 0.035;
        opts.mTopMargin = 0.02;
        opts.mBottomMargin = 0.13;
        
        //x axis
        opts.mXmin = referenceHist->GetXaxis()->GetXmin();
        opts.mXmax = 2.0;
        opts.mXaxisTitle = TString("min(m_{1}(#pi^{+}#pi^{-}), m_{2}(#pi^{+}#pi^{-})) [GeV]");
        opts.mXaxisTitleSize = 0.05;
        opts.mXaxisTitleOffset = 1.04;
        opts.mXaxisLabelSize = 0.05;
        opts.mXaxisLabelOffset = 0.007;
        opts.mXaxisTickLength = 0.015;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = /*1.5 * referenceHist->GetMaximum()*/1.68;
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/dmin(m_{1},m_{2}) [nb/GeV]" : "d#sigma/dmin(m_{1},m_{2}) [#mub/GeV]");
        opts.mYaxisTitleSize = 0.05;
        opts.mYaxisTitleOffset = 1.15;
        opts.mYaxisLabelSize = 0.05;
        opts.mYaxisLabelOffset = 0.007;
        opts.mYaxisTickLength = 0.015;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.2;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString("FinalResult_InvMass_4Pi_MinMassOf2Pi");
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxWidthAdjustmentFactor = 0.7;
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
        
        opts.mInsert = true;
        opts.mYlogInsert = true;
        opts.mX1Insert = 0.46;
        opts.mX2Insert = 0.93;
        opts.mY1Insert = 0.18;
        opts.mY2Insert = 0.58;
        opts.mXMinInstert = 1.5;
        opts.mXMaxInstert = 3.5;
        opts.mYMinInstert = 0.000001;
        opts.mYMaxInstert = 0.009;
        
        opts.mNColumns = 1;
        opts.mLegendX = 0.52;
        opts.mLegendXwidth = 0.17;
        opts.mLegendY = 0.61;
        opts.mLegendYwidth = 0.21;
        opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        
        //       opts.mXpositionSystErrorBox.push_back(0.4);
        opts.mXpositionSystErrorBox.push_back(0.66);
        //       opts.mXpositionSystErrorBox.push_back(0.8);
        opts.mXpositionSystErrorBox.push_back(0.92);
        opts.mXpositionSystErrorBox.push_back(1.46);
//         opts.mXpositionSystErrorBox.push_back(2.15);
        
        TLatex lReaction(.4, .91, "#kern[-0.29]{#font[72]{ATLAS} Internal     #it{p}+#it{p}#rightarrow#it{p'}+"+fourPionStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
    //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //     opts.mTextToDraw.push_back( lATLAS );
        
        TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
        TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
        TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
        TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
        TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
        TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
        TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
        TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
        TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
        TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        drawFinalResult( referenceHist, opts, Systematics_4PiInvMass );
        
    }
    
    
    
    
    
    
    {
        TH1F *referenceHist = new TH1F( *oo4PiInvMass_MaxMassOf2Pi->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo4PiInvMass_MaxMassOf2Pi->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo4PiInvMass_MaxMassOf2Pi->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo4PiInvMass_MaxMassOf2Pi->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo4PiInvMass_MaxMassOf2Pi->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_4PiInvMass = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsInvMassPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
        }
        
        //drawing final result
        DrawingOptions opts;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.12;
        opts.mRightMargin = 0.035;
        opts.mTopMargin = 0.02;
        opts.mBottomMargin = 0.13;
        
        //x axis
        opts.mXmin = referenceHist->GetXaxis()->GetXmin();
        opts.mXmax = 3.0;
        opts.mXaxisTitle = TString("max(m_{1}(#pi^{+}#pi^{-}), m_{2}(#pi^{+}#pi^{-})) [GeV]");
        opts.mXaxisTitleSize = 0.05;
        opts.mXaxisTitleOffset = 1.04;
        opts.mXaxisLabelSize = 0.05;
        opts.mXaxisLabelOffset = 0.007;
        opts.mXaxisTickLength = 0.015;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = /*1.5 * referenceHist->GetMaximum()*/1.68;
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/dmax(m_{1},m_{2}) [nb/GeV]" : "d#sigma/dmax(m_{1},m_{2}) [#mub/GeV]");
        opts.mYaxisTitleSize = 0.05;
        opts.mYaxisTitleOffset = 1.15;
        opts.mYaxisLabelSize = 0.05;
        opts.mYaxisLabelOffset = 0.007;
        opts.mYaxisTickLength = 0.015;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.2;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString("FinalResult_InvMass_4Pi_MaxMassOf2Pi");
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
//             if(fileId>1 && fileId<(filesMC[pid].size()-1)){               
//                 opts.mObjForLegend.push_back( new TH1F() );
//                 opts.mDesriptionForLegend.push_back(  TString("") );
//                 opts.mDrawTypeForLegend.push_back( "" );
//             }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxWidthAdjustmentFactor = 0.7;
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
        
        opts.mInsert = true;
        opts.mYlogInsert = true;
        opts.mX1Insert = 0.46;
        opts.mX2Insert = 0.93;
        opts.mY1Insert = 0.18;
        opts.mY2Insert = 0.58;
        opts.mXMinInstert = 2.5;
        opts.mXMaxInstert = 10.0;
        opts.mYMinInstert = 0.000001;
        opts.mYMaxInstert = 0.009;
        
        opts.mNColumns = 1;
        opts.mLegendX = 0.52;
        opts.mLegendXwidth = 0.17;
        opts.mLegendY = 0.61;
        opts.mLegendYwidth = 0.21;
        opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        
        //       opts.mXpositionSystErrorBox.push_back(0.4);
        opts.mXpositionSystErrorBox.push_back(0.66);
        //       opts.mXpositionSystErrorBox.push_back(0.8);
        opts.mXpositionSystErrorBox.push_back(1.02);
        opts.mXpositionSystErrorBox.push_back(1.26);
        opts.mXpositionSystErrorBox.push_back(2.15);
        
        TLatex lReaction(.4, .91, "#kern[-0.29]{#font[72]{ATLAS} Internal     #it{p}+#it{p}#rightarrow#it{p'}+"+fourPionStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
    //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //     opts.mTextToDraw.push_back( lATLAS );
        
        TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
        TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
        TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
        TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
        TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
        TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
        TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
        TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
        TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
        TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        drawFinalResult( referenceHist, opts, Systematics_4PiInvMass );
        
    }
    
    
    
    
    TFile *fileXSec = new TFile("XSec2Pi.root", "RECREATE");
    
    for(int dphi=0; dphi<Util::nDeltaPhiRanges; ++dphi){
        TH1F *referenceHist = new TH1F( *oo2PiInvMass_DeltaPhiBins[dphi]->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo2PiInvMass_DeltaPhiBins[dphi]->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo2PiInvMass_DeltaPhiBins[dphi]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo2PiInvMass_DeltaPhiBins[dphi]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo2PiInvMass_DeltaPhiBins[dphi]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_2PiInvMass = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsInvMassPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
        }
        
        //drawing final result
        DrawingOptions opts;
        opts.mSystErrorBoxWidthAdjustmentFactor = 0.7;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.12;
        opts.mRightMargin = 0.035;
        opts.mTopMargin = 0.02;
        opts.mBottomMargin = 0.13;
        
        //x axis
        opts.mXmin = referenceHist->GetXaxis()->GetXmin();
        opts.mXmax = 3.5;
        opts.mXaxisTitle = TString("m("+pairStr+") [GeV]");
        opts.mXaxisTitleSize = 0.05;
        opts.mXaxisTitleOffset = 1.04;
        opts.mXaxisLabelSize = 0.05;
        opts.mXaxisLabelOffset = 0.007;
        opts.mXaxisTickLength = 0.015;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = /*1.5 * referenceHist->GetMaximum()*/ 4.7;
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/dm("+pairStr+") [nb/GeV]" : "d#sigma/dm("+pairStr+") [#mub/GeV]");
        opts.mYaxisTitleSize = 0.05;
        opts.mYaxisTitleOffset = 1.21;
        opts.mYaxisLabelSize = 0.05;
        opts.mYaxisLabelOffset = 0.007;
        opts.mYaxisTickLength = 0.015;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.0;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString(Form("FinalResult_InvMass_2Pi_DeltaPhiBin%d", dphi));
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
        
        opts.mInsert = true;
        opts.mX1Insert = 0.43;
        opts.mX2Insert = 0.95;
        opts.mY1Insert = 0.15;
        opts.mY2Insert = 0.56;
        opts.mXMinInstert = 1.5;
        opts.mXMaxInstert = 3.5;
        
        opts.mNColumns = 2;
        opts.mLegendX = 0.43;
        opts.mLegendXwidth = 0.45;
        opts.mLegendY = 0.61;
        opts.mLegendYwidth = 0.14;
        opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        
        //       opts.mXpositionSystErrorBox.push_back(0.4);
        opts.mXpositionSystErrorBox.push_back(0.66);
        //       opts.mXpositionSystErrorBox.push_back(0.8);
        opts.mXpositionSystErrorBox.push_back(1.02);
        opts.mXpositionSystErrorBox.push_back(1.26);
        opts.mXpositionSystErrorBox.push_back(2.15);
        
        TLatex lReaction(.4, .91, "#kern[-0.29]{#font[72]{ATLAS} Internal     #it{p}+#it{p}#rightarrow#it{p'}+"+pairStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
    //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //     opts.mTextToDraw.push_back( lATLAS );
        
        TString deltaPhiStr( (dphi==Util::DELTAPHI_1) ? "#Delta#varphi < 90#circ" : "#Delta#varphi > 90#circ" );
        TLatex lDeltaPhi(.18,.69, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
        
        TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
        TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}^{+},p_{T}^{-}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
        TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
        TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
        TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
        TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
        TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
        TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
        TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
        TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        drawFinalResult( referenceHist, opts, Systematics_2PiInvMass );
        
    }
    
    fileXSec->Write();
    fileXSec->Close();
    
    
    
    
    
    
    for(int dpt=0; dpt<2; ++dpt){
        TH1F *referenceHist = new TH1F( *oo2PiInvMass_DPtBins_DeltaPhiLessThan90Deg[dpt]->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo2PiInvMass_DPtBins_DeltaPhiLessThan90Deg[dpt]->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo2PiInvMass_DPtBins_DeltaPhiLessThan90Deg[dpt]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo2PiInvMass_DPtBins_DeltaPhiLessThan90Deg[dpt]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo2PiInvMass_DPtBins_DeltaPhiLessThan90Deg[dpt]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_2PiInvMass = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsInvMassPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
        }
        
        //drawing final result
        DrawingOptions opts;
        opts.mSystErrorBoxWidthAdjustmentFactor = 0.7;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.12;
        opts.mRightMargin = 0.035;
        opts.mTopMargin = 0.02;
        opts.mBottomMargin = 0.13;
        
        //x axis
        opts.mXmin = referenceHist->GetXaxis()->GetXmin();
        opts.mXmax = 3.5;
        opts.mXaxisTitle = TString("m("+pairStr+") [GeV]");
        opts.mXaxisTitleSize = 0.05;
        opts.mXaxisTitleOffset = 1.04;
        opts.mXaxisLabelSize = 0.05;
        opts.mXaxisLabelOffset = 0.007;
        opts.mXaxisTickLength = 0.015;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = /*1.5 * referenceHist->GetMaximum()*/ 2.99;
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/dm("+pairStr+") [nb/GeV]" : "d#sigma/dm("+pairStr+") [#mub/GeV]");
        opts.mYaxisTitleSize = 0.05;
        opts.mYaxisTitleOffset = 1.21;
        opts.mYaxisLabelSize = 0.05;
        opts.mYaxisLabelOffset = 0.007;
        opts.mYaxisTickLength = 0.015;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.0;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString(Form("FinalResult_InvMass_2Pi_DPtBin%d_DeltaPhiLessThan90Deg", dpt));
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
        
        opts.mInsert = true;
        opts.mX1Insert = 0.43;
        opts.mX2Insert = 0.95;
        opts.mY1Insert = 0.15;
        opts.mY2Insert = 0.56;
        opts.mXMinInstert = 1.5;
        opts.mXMaxInstert = 3.5;
        opts.mYMaxInstert = 0.149;
        
        opts.mNColumns = 2;
        opts.mLegendX = 0.43;
        opts.mLegendXwidth = 0.45;
        opts.mLegendY = 0.61;
        opts.mLegendYwidth = 0.14;
        opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        
        //       opts.mXpositionSystErrorBox.push_back(0.4);
        opts.mXpositionSystErrorBox.push_back(0.66);
        //       opts.mXpositionSystErrorBox.push_back(0.8);
        opts.mXpositionSystErrorBox.push_back(1.02);
        opts.mXpositionSystErrorBox.push_back(1.26);
        opts.mXpositionSystErrorBox.push_back(2.15);
        
        TLatex lReaction(.4, .91, "#kern[-0.29]{#font[72]{ATLAS} Internal     #it{p}+#it{p}#rightarrow#it{p'}+"+pairStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
    //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //     opts.mTextToDraw.push_back( lATLAS );
        
        TString deltaPhiStr( "#Delta#varphi < 90#circ");
        TLatex lDeltaPhi(.18,.64, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
        TString deltaPhiStr2( (dpt==0) ? "|#vec{#Delta p'}_{T}| < 0.2 GeV" : "|#vec{#Delta p'}_{T}| > 0.2 GeV" );
        TLatex lDeltaPhi2(.18,.69, deltaPhiStr2); lDeltaPhi2.SetNDC(); lDeltaPhi2.SetTextFont(42);  lDeltaPhi2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi2 );
        
        TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
        TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}^{+},p_{T}^{-}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
        TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
        TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
        TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
        TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
        TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
        TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
        TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
        TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        drawFinalResult( referenceHist, opts, Systematics_2PiInvMass );
        
    }
    
    
    
    
    TFile *fileForFit = new TFile("Fit4Pi.root", "RECREATE");
    
    for(int dphi=0; dphi<Util::nDeltaPhiRanges; ++dphi){
        TH1F *referenceHist = new TH1F( *oo4PiInvMass_DeltaPhiBins_FineBinning[dphi]->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("InvMass4PiForFit_%d", dphi) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo4PiInvMass_DeltaPhiBins_FineBinning[dphi]->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        TH1F *originalReferenceHist = new TH1F( *oo4PiInvMass_DeltaPhiBins_FineBinning[dphi]->hQ_Weighted[Util::CH_SUM_0] );
        originalReferenceHist->SetName( Form("InvMass4PiForFit_%d_original", dphi) );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo4PiInvMass_DeltaPhiBins_FineBinning[dphi]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo4PiInvMass_DeltaPhiBins_FineBinning[dphi]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo4PiInvMass_DeltaPhiBins_FineBinning[dphi]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_4PiInvMass = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsInvMassPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
    }
    
    fileForFit->Write();
    fileForFit->Close();
    
    
    
    
    
    for(int dphi=0; dphi<Util::nDeltaPhiRanges; ++dphi){
        TH1F *referenceHist = new TH1F( *oo4PiInvMass_DeltaPhiBins[dphi]->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo4PiInvMass_DeltaPhiBins[dphi]->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo4PiInvMass_DeltaPhiBins[dphi]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo4PiInvMass_DeltaPhiBins[dphi]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo4PiInvMass_DeltaPhiBins[dphi]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_4PiInvMass = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsInvMassPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
        }
        
        //drawing final result
        DrawingOptions opts;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.12;
        opts.mRightMargin = 0.035;
        opts.mTopMargin = 0.02;
        opts.mBottomMargin = 0.13;
        
        //x axis
        opts.mXmin = referenceHist->GetXaxis()->GetXmin();
        opts.mXmax = 5.0;
        opts.mXaxisTitle = TString("m("+fourPionStr+") [GeV]");
        opts.mXaxisTitleSize = 0.05;
        opts.mXaxisTitleOffset = 1.04;
        opts.mXaxisLabelSize = 0.05;
        opts.mXaxisLabelOffset = 0.007;
        opts.mXaxisTickLength = 0.015;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = /*1.5 * referenceHist->GetMaximum()*/0.440;
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/dm("+fourPionStr+") [nb/GeV]" : "d#sigma/dm("+fourPionStr+") [#mub/GeV]");
        opts.mYaxisTitleSize = 0.05;
        opts.mYaxisTitleOffset = 1.21;
        opts.mYaxisLabelSize = 0.05;
        opts.mYaxisLabelOffset = 0.007;
        opts.mYaxisTickLength = 0.015;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.2;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString(Form("FinalResult_InvMass_4Pi_DeltaPhiBin%d", dphi));
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
        opts.mSystErrorBoxWidthAdjustmentFactor = 0.7;
        
        
        opts.mInsert = true;
        opts.mYlogInsert = true;
        opts.mX1Insert = 0.53;
        opts.mX2Insert = 0.95;
        opts.mY1Insert = 0.25;
        opts.mY2Insert = 0.58;
        opts.mXMinInstert = 5.0;
        opts.mXMaxInstert = 15.0;
        opts.mYMinInstert = 0.01;
        
        opts.mNColumns = 1;
        opts.mLegendX = 0.52;
        opts.mLegendXwidth = 0.17;
        opts.mLegendY = 0.61;
        opts.mLegendYwidth = 0.21;
        opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        
        //       opts.mXpositionSystErrorBox.push_back(0.4);
        opts.mXpositionSystErrorBox.push_back(0.66);
        //       opts.mXpositionSystErrorBox.push_back(0.8);
        opts.mXpositionSystErrorBox.push_back(1.02);
        opts.mXpositionSystErrorBox.push_back(1.26);
        opts.mXpositionSystErrorBox.push_back(2.15);
        
        TLatex lReaction(.4, .91, "#kern[-0.29]{#font[72]{ATLAS} Internal     #it{p}+#it{p}#rightarrow#it{p'}+"+fourPionStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
    //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //     opts.mTextToDraw.push_back( lATLAS );
        
        TString deltaPhiStr( (dphi==Util::DELTAPHI_1) ? "#Delta#varphi < 90#circ" : "#Delta#varphi > 90#circ" );
        TLatex lDeltaPhi(.18,.91, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
        
        TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
        TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
        TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
        TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
        TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
        TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
        TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
        TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
        TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
        TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        drawFinalResult( referenceHist, opts, Systematics_4PiInvMass );
        
    }
    
    
    
    
    
  
    
    
    
    for(int etaBin=0; etaBin<Util::nCentralTracksEtaConf; ++etaBin){
        TH1F *referenceHist = new TH1F( *oo2PiInvMass_CentralTracksEtaBins[etaBin]->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo2PiInvMass_CentralTracksEtaBins[etaBin]->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo2PiInvMass_CentralTracksEtaBins[etaBin]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo2PiInvMass_CentralTracksEtaBins[etaBin]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo2PiInvMass_CentralTracksEtaBins[etaBin]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_2PiInvMass = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsInvMassPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
        }
        
        //drawing final result
        DrawingOptions opts;
        opts.mSystErrorBoxWidthAdjustmentFactor = 0.7;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.12;
        opts.mRightMargin = 0.035;
        opts.mTopMargin = 0.02;
        opts.mBottomMargin = 0.13;
        
        //x axis
        opts.mXmin = referenceHist->GetXaxis()->GetXmin();
        opts.mXmax = 3.5;
        opts.mXaxisTitle = TString("m("+pairStr+") [GeV]");
        opts.mXaxisTitleSize = 0.05;
        opts.mXaxisTitleOffset = 1.04;
        opts.mXaxisLabelSize = 0.05;
        opts.mXaxisLabelOffset = 0.007;
        opts.mXaxisTickLength = 0.015;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 1.5 * referenceHist->GetMaximum();
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/dm("+pairStr+") [nb/GeV]" : "d#sigma/dm("+pairStr+") [#mub/GeV]");
        opts.mYaxisTitleSize = 0.05;
        opts.mYaxisTitleOffset = 1.21;
        opts.mYaxisLabelSize = 0.05;
        opts.mYaxisLabelOffset = 0.007;
        opts.mYaxisTickLength = 0.015;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.0;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString(Form("FinalResult_InvMass_2Pi_CentralTracksEtaBin%d", etaBin));
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
        
        opts.mInsert = true;
        opts.mX1Insert = 0.43;
        opts.mX2Insert = 0.95;
        opts.mY1Insert = 0.15;
        opts.mY2Insert = 0.56;
        opts.mXMinInstert = 1.5;
        opts.mXMaxInstert = 3.5;
        
        opts.mNColumns = 2;
        opts.mLegendX = 0.43;
        opts.mLegendXwidth = 0.45;
        opts.mLegendY = 0.61;
        opts.mLegendYwidth = 0.14;
        opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        
        //       opts.mXpositionSystErrorBox.push_back(0.4);
        opts.mXpositionSystErrorBox.push_back(0.66);
        //       opts.mXpositionSystErrorBox.push_back(0.8);
        opts.mXpositionSystErrorBox.push_back(1.02);
        opts.mXpositionSystErrorBox.push_back(1.26);
        opts.mXpositionSystErrorBox.push_back(2.15);
        
        TLatex lReaction(.4, .91, "#kern[-0.29]{#font[72]{ATLAS} Internal     #it{p}+#it{p}#rightarrow#it{p'}+"+pairStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
    //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //     opts.mTextToDraw.push_back( lATLAS );
        
        TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
        TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}^{+},p_{T}^{-}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
        TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
        TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
        TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
        TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
        TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
        TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
        TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
        TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        drawFinalResult( referenceHist, opts, Systematics_2PiInvMass );
        
    }
    
    
    
    mSpecialLabelRatio = true;
    mEnableLegend = true;
    mSpecialLabelOffsetFactor = 1.0;
    {
        TH1F *referenceHist = new TH1F( *oo2PiRapidity->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo2PiRapidity->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo2PiRapidity->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo2PiRapidity->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo2PiRapidity->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_2PiRapidity = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsRapidityPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
        }
        
        //drawing final result
        DrawingOptions opts;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.13*1.5;
        opts.mRightMargin = 0.02*1.7;
        opts.mTopMargin = 0.03;
        opts.mBottomMargin = 0.13*1.7;
        
        //x axis
        opts.mXmin = -2.6;
        opts.mXmax = 2.6;
        opts.mXaxisTitle = TString("y("+pairStr+")");
        opts.mXaxisTitleSize = 0.05*1.7;
        opts.mXaxisTitleOffset = 1.0;
        opts.mXaxisLabelSize = 0.05*1.7;
        opts.mXaxisLabelOffset = 0.005;
        opts.mXaxisTickLength = 0.015;
//         opts.mSetDivisionsX = kTRUE;
//         opts.mXnDivisionA = 4;
//         opts.mXnDivisionB = 5;
//         opts.mXnDivisionC = 1;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 1.6 * referenceHist->GetMaximum();
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/dy("+pairStr+") [nb]" : "d#sigma/dy("+pairStr+") [#mub]");
        opts.mYaxisTitleSize = 0.05*1.7;
        opts.mYaxisTitleOffset = 1.18;
        opts.mYaxisLabelSize = 0.05*1.7;
        opts.mYaxisLabelOffset = 0.013;
        opts.mYaxisTickLength = 0.015;
        opts.mYnDivisionA = 5;
        opts.mYnDivisionB = 5;
        opts.mYnDivisionC = 1;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.6;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString("FinalResult_Rapidity_2Pi");
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
        
//         opts.mNColumns = 2;
//         opts.mLegendX = 0.43;
//         opts.mLegendXwidth = 0.45;
//         opts.mLegendY = 0.61;
//         opts.mLegendYwidth = 0.14;
//         opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        opts.mLegendX = 0.22;
        opts.mNColumns = 2;
        opts.mLegendXwidth = 0.35;
        opts.mLegendY = 0.74;
        opts.mLegendYwidth = 0.13;
        opts.mLegendTextSize = 0.55 * 0.7 * opts.mXaxisTitleSize;
        
        //       opts.mXpositionSystErrorBox.push_back(0.4);
        opts.mXpositionSystErrorBox.push_back(0.05);
        //       opts.mXpositionSystErrorBox.push_back(0.8);
//         opts.mXpositionSystErrorBox.push_back(1.02);
        opts.mXpositionSystErrorBox.push_back(1.86);
        opts.mXpositionSystErrorBox.push_back(2.25);
        
        TLatex lReaction(.3, .9, "#kern[-0.29]{#font[72]{ATLAS} Internal     #it{p}+#it{p}#rightarrow#it{p'}+"+pairStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.7 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
    //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //     opts.mTextToDraw.push_back( lATLAS );
        
//         TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
//         TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}^{+},p_{T}^{-}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
//         TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
//         TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
//         TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
//         TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
//         TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
//         TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
//         TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
//         TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        drawFinalResult( referenceHist, opts, Systematics_2PiRapidity );
        
    }
    mSpecialLabelRatio = false;
    mSpecialLabelOffsetFactor = 1.0;
    
    
    
    
    
    
    {
        TH1F *referenceHist = new TH1F( *oo4PiRapidity->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo4PiRapidity->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo4PiRapidity->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo4PiRapidity->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo4PiRapidity->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_4PiRapidity = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsRapidityPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
        }
        
        //drawing final result
        DrawingOptions opts;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.13*1.5;
        opts.mRightMargin = 0.02*1.7;
        opts.mTopMargin = 0.03;
        opts.mBottomMargin = 0.13*1.7;
        
        //x axis
        opts.mXmin = -2.6;
        opts.mXmax = 2.6;
        opts.mXaxisTitle = TString("y("+fourPionStr+")");
        opts.mXaxisTitleSize = 0.05*1.7;
        opts.mXaxisTitleOffset = 1.0;
        opts.mXaxisLabelSize = 0.05*1.7;
        opts.mXaxisLabelOffset = 0.005;
        opts.mXaxisTickLength = 0.015;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 1.6 * referenceHist->GetMaximum();
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/dy("+fourPionStr+") [nb]" : "d#sigma/dy("+fourPionStr+") [#mub]");
        opts.mYaxisTitleSize = 0.05*1.7;
        opts.mYaxisTitleOffset = 1.18;
        opts.mYaxisLabelSize = 0.05*1.7;
        opts.mYaxisLabelOffset = 0.013;
        opts.mYaxisTickLength = 0.015;
        
        opts.mSetDivisionsY = kTRUE;
        opts.mYnDivisionA = 5;
        opts.mYnDivisionB = 5;
        opts.mYnDivisionC = 1;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.6;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString("FinalResult_Rapidity_4Pi");
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
/*        
        opts.mNColumns = 2;
        opts.mLegendX = 0.43;
        opts.mLegendXwidth = 0.45;
        opts.mLegendY = 0.61;
        opts.mLegendYwidth = 0.14;
        opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;*/

        opts.mLegendX = 0.22;
        opts.mNColumns = 2;
        opts.mLegendXwidth = 0.45;
        opts.mLegendY = 0.74;
        opts.mLegendYwidth = 0.13;
        opts.mLegendTextSize = 0.55 * 0.7 * opts.mXaxisTitleSize;
        
        //       opts.mXpositionSystErrorBox.push_back(0.4);
        opts.mXpositionSystErrorBox.push_back(0.05);
        //       opts.mXpositionSystErrorBox.push_back(0.8);
//         opts.mXpositionSystErrorBox.push_back(1.02);
        opts.mXpositionSystErrorBox.push_back(1.86);
        opts.mXpositionSystErrorBox.push_back(2.25);
        
        TLatex lReaction(.26, .9, "#kern[-0.29]{#font[72]{ATLAS} Internal     #it{p}+#it{p}#rightarrow#it{p'}+"+fourPionStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.7 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
    //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //     opts.mTextToDraw.push_back( lATLAS );
        
//         TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
//         TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}^{+},p_{T}^{-}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
//         TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
//         TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
//         TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
//         TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
//         TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
//         TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
//         TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
//         TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        mEnableLegend = true;
        mSpecialLabelRatio = true;
        mSpecialLabelOffsetFactor = 1.0;
        drawFinalResult( referenceHist, opts, Systematics_4PiRapidity );
        mSpecialLabelRatio = false;
        mSpecialLabelOffsetFactor = 1.0;
        mEnableLegend = false;
        
    }
    

    {
        TH1F *referenceHist = new TH1F( *oo6PiRapidity->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo6PiRapidity->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo6PiRapidity->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo6PiRapidity->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo6PiRapidity->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_6PiRapidity = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsRapidityPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
            histMC[fileId]->Scale(pion6PythiaScaleFactor);
        }
        
        //drawing final result
        DrawingOptions opts;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.13*1.5;
        opts.mRightMargin = 0.02*1.7;
        opts.mTopMargin = 0.03;
        opts.mBottomMargin = 0.13*1.7;
        
        //x axis
        opts.mXmin = -2.5;
        opts.mXmax = 2.5;
        opts.mXaxisTitle = TString("y("+sixPionStr+")");
        opts.mXaxisTitleSize = 0.05*1.7;
        opts.mXaxisTitleOffset = 1.0;
        opts.mXaxisLabelSize = 0.05*1.7;
        opts.mXaxisLabelOffset = 0.005;
        opts.mXaxisTickLength = 0.015;
        
        opts.mSetDivisionsX = kTRUE;
        opts.mXnDivisionA = 5;
        opts.mXnDivisionB = 5;
        opts.mXnDivisionC = 1;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 1.8 * referenceHist->GetMaximum();
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/dy("+sixPionStr+") [nb]" : "d#sigma/dy("+sixPionStr+") [#mub]");
        opts.mYaxisTitleSize = 0.05*1.7;
        opts.mYaxisTitleOffset = 1.18;
        opts.mYaxisLabelSize = 0.05*1.7;
        opts.mYaxisLabelOffset = 0.013;
        opts.mYaxisTickLength = 0.015;
        
        opts.mSetDivisionsY = kTRUE;
        opts.mYnDivisionA = 5;
        opts.mYnDivisionB = 5;
        opts.mYnDivisionC = 1;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.6;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString("FinalResult_Rapidity_6Pi");
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
        
        opts.mLegendX = 0.22;
        opts.mNColumns = 2;
        opts.mLegendXwidth = 0.45;
        opts.mLegendY = 0.74;
        opts.mLegendYwidth = 0.13;
        opts.mLegendTextSize = 0.7 * 0.7 * opts.mXaxisTitleSize;
        
        //       opts.mXpositionSystErrorBox.push_back(0.4);
        opts.mXpositionSystErrorBox.push_back(0.05);
        //       opts.mXpositionSystErrorBox.push_back(0.8);
//         opts.mXpositionSystErrorBox.push_back(1.02);
        opts.mXpositionSystErrorBox.push_back(1.86);
        opts.mXpositionSystErrorBox.push_back(2.25);
        
        TLatex lReaction(.26, .9, "#it{p}+#it{p}#rightarrow#it{p'}+"+sixPionStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.7 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
    //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //     opts.mTextToDraw.push_back( lATLAS );
        
//         TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
//         TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}^{+},p_{T}^{-}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
//         TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
//         TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
//         TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
//         TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
//         TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
//         TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
//         TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
//         TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        mEnableLegend = true;
        mSpecialLabelRatio = true;
        mSpecialLabelOffsetFactor = 1.0;
        drawFinalResult( referenceHist, opts, Systematics_6PiRapidity );
        mSpecialLabelRatio = false;
        mSpecialLabelOffsetFactor = 1.0;
        mEnableLegend = false;
        
    }
    
    
    
    {
        TH1F *referenceHist = new TH1F( *oo8PiRapidity->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo8PiRapidity->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo8PiRapidity->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo8PiRapidity->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo8PiRapidity->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_8PiRapidity = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsRapidityPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
            histMC[fileId]->Scale(pion8PythiaScaleFactor);
        }
        
        //drawing final result
        DrawingOptions opts;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.13*1.5;
        opts.mRightMargin = 0.02*1.7;
        opts.mTopMargin = 0.03;
        opts.mBottomMargin = 0.13*1.7;
        
        //x axis
        opts.mXmin = -2.5;
        opts.mXmax = 2.5;
        opts.mXaxisTitle = TString("y("+eightPionStr+")");
        opts.mXaxisTitleSize = 0.05*1.7;
        opts.mXaxisTitleOffset = 1.0;
        opts.mXaxisLabelSize = 0.05*1.7;
        opts.mXaxisLabelOffset = 0.005;
        opts.mXaxisTickLength = 0.015;
        
        opts.mSetDivisionsX = kTRUE;
        opts.mXnDivisionA = 5;
        opts.mXnDivisionB = 5;
        opts.mXnDivisionC = 1;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 1.8 * referenceHist->GetMaximum();
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/dy("+eightPionStr+") [nb]" : "d#sigma/dy("+eightPionStr+") [#mub]");
        opts.mYaxisTitleSize = 0.05*1.7;
        opts.mYaxisTitleOffset = 1.18;
        opts.mYaxisLabelSize = 0.05*1.7;
        opts.mYaxisLabelOffset = 0.013;
        opts.mYaxisTickLength = 0.015;
        
        opts.mSetDivisionsY = kTRUE;
        opts.mYnDivisionA = 5;
        opts.mYnDivisionB = 5;
        opts.mYnDivisionC = 1;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.6;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString("FinalResult_Rapidity_8Pi");
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
        
        opts.mLegendX = 0.22;
        opts.mNColumns = 2;
        opts.mLegendXwidth = 0.45;
        opts.mLegendY = 0.74;
        opts.mLegendYwidth = 0.13;
        opts.mLegendTextSize = 0.7 * 0.7 * opts.mXaxisTitleSize;
        
        //       opts.mXpositionSystErrorBox.push_back(0.4);
        opts.mXpositionSystErrorBox.push_back(0.05);
        //       opts.mXpositionSystErrorBox.push_back(0.8);
//         opts.mXpositionSystErrorBox.push_back(1.02);
        opts.mXpositionSystErrorBox.push_back(1.86);
        opts.mXpositionSystErrorBox.push_back(2.25);
        
        TLatex lReaction(.26, .9, "#it{p}+#it{p}#rightarrow#it{p'}+"+eightPionStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.65 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
    //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //     opts.mTextToDraw.push_back( lATLAS );
        
//         TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
//         TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}^{+},p_{T}^{-}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
//         TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
//         TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
//         TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
//         TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
//         TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
//         TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
//         TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
//         TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        
        mEnableLegend = true;
        mSpecialLabelRatio = true;
        mSpecialLabelOffsetFactor = 1.0;
        drawFinalResult( referenceHist, opts, Systematics_8PiRapidity );
        mSpecialLabelRatio = false;
        mSpecialLabelOffsetFactor = 1.0;
        mEnableLegend = false;
        
        
    }
    
    
    
    for(int mr=0; mr<Util::nMassRanges; ++mr){
        TH1F *referenceHist = new TH1F( *oo2PiRapidity_MassBins[mr]->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo2PiRapidity_MassBins[mr]->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo2PiRapidity_MassBins[mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo2PiRapidity_MassBins[mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo2PiRapidity_MassBins[mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_2PiRapidity = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsRapidityPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
        }
        
        //drawing final result
        DrawingOptions opts;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.13*1.7;
        opts.mRightMargin = 0.035*1.5;
        opts.mTopMargin = 0.03;
        opts.mBottomMargin = 0.13*1.7;
        
        //x axis
        opts.mXmin = -2.6;
        opts.mXmax = 2.6;
        opts.mXaxisTitle = TString("y("+pairStr+")");
        opts.mXaxisTitleSize = 0.05*1.7;
        opts.mXaxisTitleOffset = 1.0;
        opts.mXaxisLabelSize = 0.05*1.7;
        opts.mXaxisLabelOffset = 0.005;
        opts.mXaxisTickLength = 0.015;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 1.45 * referenceHist->GetMaximum();
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/dy("+pairStr+") [nb]" : "d#sigma/dy("+pairStr+") [#mub]");
        opts.mYaxisTitleSize = 0.05*1.7;
        opts.mYaxisTitleOffset = 1.3;
        opts.mYaxisLabelSize = 0.05*1.7;
        opts.mYaxisLabelOffset = 0.005;
        opts.mYaxisTickLength = 0.015;
        
        opts.mSetDivisionsY = kTRUE;
        opts.mYnDivisionA = 5;
        opts.mYnDivisionB = 5;
        opts.mYnDivisionC = 1;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.6;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString(Form("FinalResult_Rapidity_2Pi_MassBin%d", mr));
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
        
        
//         opts.mNColumns = 2;
//         opts.mLegendXwidth = 0.35;
//         opts.mLegendY = 0.65;
//         opts.mLegendYwidth = 0.13;
//         opts.mLegendTextSize = 0.7/*55*/ * 0.7 * opts.mXaxisTitleSize;
//               opts.mLegendX = /*pid==Util::KAON ? 0.12 :*/ 0.24;
      
      opts.mNColumns = 2;
      opts.mLegendX = 0.37;
      opts.mLegendXwidth = 0.35;
      opts.mLegendY = 0.42;
      opts.mLegendYwidth =  0.13;
      opts.mLegendTextSize = 0.7/*55*/ * 0.7 * opts.mXaxisTitleSize;
        
        //       opts.mXpositionSystErrorBox.push_back(0.4);
        opts.mXpositionSystErrorBox.push_back(0.05);
        //       opts.mXpositionSystErrorBox.push_back(0.8);
//         opts.mXpositionSystErrorBox.push_back(1.02);
        opts.mXpositionSystErrorBox.push_back(1.86);
        opts.mXpositionSystErrorBox.push_back(2.25);
        
        TLatex lReaction(.29 , .90, "#kern[-0.29]{#font[72]{ATLAS} Internal     #it{p}+#it{p}#rightarrow#it{p'}+"+pairStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.65 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
    //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //     opts.mTextToDraw.push_back( lATLAS );
        
//         TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
//         TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}^{+},p_{T}^{-}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
//         TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
//         TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
//         TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
//         TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
//         TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
//         TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
//         TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
//         TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        TString massBinStr;
        TString massLimitsStr[2];
        massLimitsStr[Util::MIN].Form(mr==Util::MASS_2 ? "%.0f" : "%.1f", mr==Util::MASS_2 ? 1.0 : 1.5 );
        massLimitsStr[Util::MAX].Form(mr==Util::MASS_1 ? "%.0f" : "%.1f", mr==Util::MASS_1 ? 1.0 : 1.5 );
        if(mr==Util::MASS_1){
          massBinStr = "m("+pairStr+") < " + massLimitsStr[Util::MAX]+" GeV";
        } else
        if(mr==Util::MASS_2){
          massBinStr = massLimitsStr[Util::MIN]+" GeV < m("+pairStr+") < " + massLimitsStr[Util::MAX]+" GeV";
        } else
        if(mr==Util::MASS_3){
          massBinStr = "m("+pairStr+") > " + massLimitsStr[Util::MIN]+" GeV";
        }
        TLatex lCuts6(.26, .82 , massBinStr); lCuts6.SetNDC(); lCuts6.SetTextFont(42); lCuts6.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts6 );
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        if( mr==Util::MASS_2 )
            mEnableLegend = true;
        else 
            mEnableLegend = false;
        mSpecialLabelRatio = true;
        drawFinalResult( referenceHist, opts, Systematics_2PiRapidity );
        mEnableLegend = false;
        mSpecialLabelRatio = false;
        
    }
    
    

    
    
    for(int mr=0; mr<Util::nMassRanges; ++mr){
        TH1F *referenceHist = new TH1F( *oo4PiRapidity_MassBins[mr]->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo4PiRapidity_MassBins[mr]->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo4PiRapidity_MassBins[mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo4PiRapidity_MassBins[mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo4PiRapidity_MassBins[mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_4PiRapidity = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsRapidityPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
        }
        
        //drawing final result
        DrawingOptions opts;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.13*1.7;
        opts.mRightMargin = 0.035*1.5;
        opts.mTopMargin = 0.03;
        opts.mBottomMargin = 0.13*1.7;
        
        //x axis
        opts.mXmin = -2.6;
        opts.mXmax = 2.6;
        opts.mXaxisTitle = TString("y("+fourPionStr+")");
        opts.mXaxisTitleSize = 0.05*1.7;
        opts.mXaxisTitleOffset = 1.0;
        opts.mXaxisLabelSize = 0.05*1.7;
        opts.mXaxisLabelOffset = 0.005;
        opts.mXaxisTickLength = 0.015;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 1.45 * referenceHist->GetMaximum();
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/dy("+fourPionStr+") [nb]" : "d#sigma/dy("+fourPionStr+") [#mub]");
        opts.mYaxisTitleSize = 0.05*1.7;
        opts.mYaxisTitleOffset = 1.3;
        opts.mYaxisLabelSize = 0.05*1.7;
        opts.mYaxisLabelOffset = 0.005;
        opts.mYaxisTickLength = 0.015;
        
        
        opts.mSetDivisionsY = kTRUE;
        opts.mYnDivisionA = 5;
        opts.mYnDivisionB = 5;
        opts.mYnDivisionC = 1;
        
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.6;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString(Form("FinalResult_Rapidity_4Pi_MassBin%d", mr));
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
        
        opts.mLegendX = 0.38;
        opts.mLegendXwidth = 0.18;
        opts.mLegendY = 0.28;
        opts.mLegendYwidth = 0.24;
        opts.mLegendTextSize = 0.7 * 0.7 * opts.mXaxisTitleSize;
        
//         opts.mNColumns = 2;
//         opts.mLegendX = 0.37;
//         opts.mLegendXwidth = 0.35;
//         opts.mLegendY = 0.42;
//         opts.mLegendYwidth =  0.13;
//         opts.mLegendTextSize = 0.7/*55*/ * 0.7 * opts.mXaxisTitleSize;
        
        //       opts.mXpositionSystErrorBox.push_back(0.4);
        opts.mXpositionSystErrorBox.push_back(0.05);
        //       opts.mXpositionSystErrorBox.push_back(0.8);
//         opts.mXpositionSystErrorBox.push_back(1.02);
        opts.mXpositionSystErrorBox.push_back(1.86);
        opts.mXpositionSystErrorBox.push_back(2.25);
        
        TLatex lReaction(.26, .9, "#kern[-0.29]{#font[72]{ATLAS} Internal     #it{p}+#it{p}#rightarrow#it{p'}+"+fourPionStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.65 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
    //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //     opts.mTextToDraw.push_back( lATLAS );
        
//         TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
//         TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}^{+},p_{T}^{-}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
//         TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
//         TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
//         TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
//         TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
//         TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
//         TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
//         TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
//         TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        TString massBinStr;
        TString massLimitsStr[2];
        massLimitsStr[Util::MIN].Form("%.1f", mr==Util::MASS_2 ? 1.5 : 2.5 );
        massLimitsStr[Util::MAX].Form("%.1f", mr==Util::MASS_1 ? 1.5 : 2.5 );
        if(mr==Util::MASS_1){
          massBinStr = "m("+fourPionStr+") < " + massLimitsStr[Util::MAX]+" GeV";
        } else
        if(mr==Util::MASS_2){
          massBinStr = massLimitsStr[Util::MIN]+" GeV < m("+fourPionStr+") < " + massLimitsStr[Util::MAX]+" GeV";
        } else
        if(mr==Util::MASS_3){
          massBinStr = "m("+fourPionStr+") > " + massLimitsStr[Util::MIN]+" GeV";
        }
        TLatex lCuts6(.26, .82 , massBinStr); lCuts6.SetNDC(); lCuts6.SetTextFont(42); lCuts6.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts6 );
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        
        if( mr==Util::MASS_1 )
            mEnableLegend = true;
        else 
            mEnableLegend = false;
        mSpecialLabelRatio = true;
        drawFinalResult( referenceHist, opts, Systematics_4PiRapidity );
        mEnableLegend = false;
        mSpecialLabelRatio = false;
        
    }
    
    
    
    
    {
        TH1F *referenceHist = new TH1F( *oo2PiDeltaPhi->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo2PiDeltaPhi->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo2PiDeltaPhi->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo2PiDeltaPhi->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo2PiDeltaPhi->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_2PiDeltaPhi = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsDeltaPhiPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
        }
        
        //drawing final result
        DrawingOptions opts;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.13*1.7;
        opts.mRightMargin = 0.035*1.5;
        opts.mTopMargin = 0.03;
        opts.mBottomMargin = 0.13*1.7;
        
        //x axis
        opts.mXmin = referenceHist->GetXaxis()->GetXmin();
        opts.mXmax = referenceHist->GetXaxis()->GetXmax();
        opts.mXaxisTitle = TString("#Delta#varphi [deg]");
        opts.mXaxisTitleSize = 0.05*1.7;
        opts.mXaxisTitleOffset = 1.0;
        opts.mXaxisLabelSize = 0.05*1.7;
        opts.mXaxisLabelOffset = 0.005;
        opts.mXaxisTickLength = 0.015;
        
        opts.mSetDivisionsX = kTRUE;
        opts.mForceDivisionsX = kTRUE;
        opts.mXnDivisionA = 6;
        opts.mXnDivisionB = 3;
        opts.mXnDivisionC = 1;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 1.5 * referenceHist->GetMaximum();
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/d#Delta#varphi [nb/deg]" : "d#sigma/d#Delta#varphi [#mub/deg]");
        opts.mYaxisTitleSize = 0.05*1.7;
        opts.mYaxisTitleOffset = 1.18;
        opts.mYaxisLabelSize = 0.05*1.7;
        opts.mYaxisLabelOffset = 0.013;
        opts.mYaxisTickLength = 0.015;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.6;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString("FinalResult_DeltaPhi_2Pi");
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
        
//         opts.mNColumns = 2;
//         opts.mLegendX = 0.43;
//         opts.mLegendXwidth = 0.45;
//         opts.mLegendY = 0.61;
//         opts.mLegendYwidth = 0.14;
//         opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        opts.mLegendX = 0.4;
        opts.mLegendXwidth = 0.17;
        opts.mLegendY = pid==Util::PROTON ? 0.67 : 0.5;
        opts.mLegendYwidth = pid==Util::PROTON ? 0.16:0.23;
        opts.mLegendTextSize = 0.7*0.7 * opts.mXaxisTitleSize;
        
        opts.mXpositionSystErrorBox.push_back(5);
        opts.mXpositionSystErrorBox.push_back(95);
        opts.mXpositionSystErrorBox.push_back(175);
        
        TLatex lReaction(.3, .9, "#it{p}+#it{p}#rightarrow#it{p'}+"+pairStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.7 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
//         TLatex lReaction(.4, .91, "#kern[-0.29]{#font[72]{ATLAS} Internal     #it{p}+#it{p}#rightarrow#it{p'}+"+pairStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
//         opts.mTextToDraw.push_back( lReaction );
        TLatex lATLAS(0.6, 0.83, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(0.7 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lATLAS );
        
//         TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
//         TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}^{+},p_{T}^{-}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
//         TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
//         TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
//         TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
//         TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
//         TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
//         TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
//         TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
//         TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        mEnableLegend = true;
        mSpecialLabelRatio = true;
        mSpecialLabelOffsetFactor = 1.0;
        drawFinalResult( referenceHist, opts, Systematics_2PiDeltaPhi );
        mEnableLegend = kFALSE;
        mSpecialLabelRatio = false;
        mSpecialLabelOffsetFactor = 1.0;
    }
    
    
    
    
    
    
    
    
    {
        TH1F *referenceHist = new TH1F( *oo4PiDeltaPhi->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo4PiDeltaPhi->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo4PiDeltaPhi->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo4PiDeltaPhi->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo4PiDeltaPhi->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_4PiDeltaPhi = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsDeltaPhiPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
        }
        
        //drawing final result
        DrawingOptions opts;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.13*1.7;
        opts.mRightMargin = 0.035*1.5;
        opts.mTopMargin = 0.03;
        opts.mBottomMargin = 0.13*1.7;
        
        
        //x axis
        opts.mXmin = referenceHist->GetXaxis()->GetXmin();
        opts.mXmax = referenceHist->GetXaxis()->GetXmax();
        opts.mXaxisTitle = TString("#Delta#varphi [deg]");
        opts.mXaxisTitleSize = 0.05*1.7;
        opts.mXaxisTitleOffset = 1.0;
        opts.mXaxisLabelSize = 0.05*1.7;
        opts.mXaxisLabelOffset = 0.005;
        opts.mXaxisTickLength = 0.015;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 1.7 * referenceHist->GetMaximum();
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/d#Delta#varphi [nb/deg]" : "d#sigma/d#Delta#varphi [#mub/deg]");
        opts.mYaxisTitleSize = 0.05*1.7;
        opts.mYaxisTitleOffset = 1.18;
        opts.mYaxisLabelSize = 0.05*1.7;
        opts.mYaxisLabelOffset = 0.013;
        opts.mYaxisTickLength = 0.015;
        
    
        opts.mSetDivisionsX = kTRUE;
        opts.mForceDivisionsX = kTRUE;
        opts.mXnDivisionA = 6;
        opts.mXnDivisionB = 3;
        opts.mXnDivisionC = 1;
        
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.6;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString("FinalResult_DeltaPhi_4Pi");
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
        
//         opts.mNColumns = 2;
//         opts.mLegendX = 0.43;
//         opts.mLegendXwidth = 0.45;
//         opts.mLegendY = 0.61;
//         opts.mLegendYwidth = 0.14;
//         opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        
        opts.mLegendX = 0.35;
        opts.mLegendXwidth = 0.17;
        opts.mLegendY = 0.6;
        opts.mLegendYwidth = 0.28;
        opts.mLegendTextSize = 0.7*0.7 * opts.mXaxisTitleSize;
        
        opts.mXpositionSystErrorBox.push_back(5);
        opts.mXpositionSystErrorBox.push_back(95);
        opts.mXpositionSystErrorBox.push_back(175);
        
        TLatex lReaction(.26, .9, "#kern[-0.29]{#font[72]{ATLAS} Internal     #it{p}+#it{p}#rightarrow#it{p'}+"+fourPionStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.7 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
    //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //     opts.mTextToDraw.push_back( lATLAS );
        
//         TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
//         TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}^{+},p_{T}^{-}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
//         TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
//         TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
//         TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
//         TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
//         TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
//         TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
//         TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
//         TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        mEnableLegend = true;
        mSpecialLabelRatio = true;
        mSpecialLabelOffsetFactor = 1.0;
        drawFinalResult( referenceHist, opts, Systematics_4PiDeltaPhi );
        mEnableLegend = kFALSE;
        mSpecialLabelRatio = false;
        mSpecialLabelOffsetFactor = 1.0;
        
        
    }
    
    
    
    {
        TH1F *referenceHist = new TH1F( *oo6PiDeltaPhi->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo6PiDeltaPhi->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo6PiDeltaPhi->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo6PiDeltaPhi->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo6PiDeltaPhi->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_6PiDeltaPhi = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsDeltaPhiPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
            histMC[fileId]->Scale(pion6PythiaScaleFactor);
        }
        
        //drawing final result
        DrawingOptions opts;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.13*1.7;
        opts.mRightMargin = 0.035*1.5;
        opts.mTopMargin = 0.03;
        opts.mBottomMargin = 0.13*1.7;
        
        
        //x axis
        opts.mXmin = referenceHist->GetXaxis()->GetXmin();
        opts.mXmax = referenceHist->GetXaxis()->GetXmax();
        opts.mXaxisTitle = TString("#Delta#varphi [deg]");
        opts.mXaxisTitleSize = 0.05*1.7;
        opts.mXaxisTitleOffset = 1.0;
        opts.mXaxisLabelSize = 0.05*1.7;
        opts.mXaxisLabelOffset = 0.005;
        opts.mXaxisTickLength = 0.015;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 1.7 * referenceHist->GetMaximum();
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/d#Delta#varphi [nb/deg]" : "d#sigma/d#Delta#varphi [#mub/deg]");
        opts.mYaxisTitleSize = 0.05*1.7;
        opts.mYaxisTitleOffset = 1.18;
        opts.mYaxisLabelSize = 0.05*1.7;
        opts.mYaxisLabelOffset = 0.013;
        opts.mYaxisTickLength = 0.015;
        
    
        opts.mSetDivisionsX = kTRUE;
        opts.mForceDivisionsX = kTRUE;
        opts.mXnDivisionA = 6;
        opts.mXnDivisionB = 3;
        opts.mXnDivisionC = 1;
        
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.6;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString("FinalResult_DeltaPhi_6Pi");
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
        
//         opts.mNColumns = 2;
//         opts.mLegendX = 0.43;
//         opts.mLegendXwidth = 0.45;
//         opts.mLegendY = 0.61;
//         opts.mLegendYwidth = 0.14;
//         opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        
        opts.mLegendX = 0.45;
        opts.mLegendXwidth = 0.17;
        opts.mLegendY = 0.63;
        opts.mLegendYwidth = 0.23;
        opts.mLegendTextSize = 0.7*0.7 * opts.mXaxisTitleSize;
        
        opts.mXpositionSystErrorBox.push_back(5);
        opts.mXpositionSystErrorBox.push_back(95);
        opts.mXpositionSystErrorBox.push_back(175);
        
        TLatex lReaction(.26, .9, "#it{p}+#it{p}#rightarrow#it{p'}+"+sixPionStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.7 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
    //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //     opts.mTextToDraw.push_back( lATLAS );
        
//         TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
//         TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}^{+},p_{T}^{-}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
//         TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
//         TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
//         TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
//         TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
//         TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
//         TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
//         TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
//         TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        mEnableLegend = true;
        mSpecialLabelRatio = true;
        mSpecialLabelOffsetFactor = 1.0;
        drawFinalResult( referenceHist, opts, Systematics_6PiDeltaPhi );
        mEnableLegend = kFALSE;
        mSpecialLabelRatio = false;
        mSpecialLabelOffsetFactor = 1.0;
        
    }
    
    
    
    {
        TH1F *referenceHist = new TH1F( *oo8PiDeltaPhi->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo8PiDeltaPhi->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo8PiDeltaPhi->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo8PiDeltaPhi->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo8PiDeltaPhi->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_8PiDeltaPhi = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsDeltaPhiPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
            histMC[fileId]->Scale(pion8PythiaScaleFactor);
        }
        
        //drawing final result
        DrawingOptions opts;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.13*1.7;
        opts.mRightMargin = 0.035*1.5;
        opts.mTopMargin = 0.03;
        opts.mBottomMargin = 0.13*1.7;
        
        
        //x axis
        opts.mXmin = referenceHist->GetXaxis()->GetXmin();
        opts.mXmax = referenceHist->GetXaxis()->GetXmax();
        opts.mXaxisTitle = TString("#Delta#varphi [deg]");
        opts.mXaxisTitleSize = 0.05*1.7;
        opts.mXaxisTitleOffset = 1.0;
        opts.mXaxisLabelSize = 0.05*1.7;
        opts.mXaxisLabelOffset = 0.005;
        opts.mXaxisTickLength = 0.015;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 1.7 * referenceHist->GetMaximum();
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/d#Delta#varphi [nb/deg]" : "d#sigma/d#Delta#varphi [#mub/deg]");
        opts.mYaxisTitleSize = 0.05*1.7;
        opts.mYaxisTitleOffset = 1.18;
        opts.mYaxisLabelSize = 0.05*1.7;
        opts.mYaxisLabelOffset = 0.013;
        opts.mYaxisTickLength = 0.015;
        
    
        opts.mSetDivisionsX = kTRUE;
        opts.mForceDivisionsX = kTRUE;
        opts.mXnDivisionA = 6;
        opts.mXnDivisionB = 3;
        opts.mXnDivisionC = 1;
        
        opts.mSetDivisionsY = kTRUE;
        opts.mYnDivisionA = 3;
        opts.mYnDivisionB = 5;
        opts.mYnDivisionC = 1;
        
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.6;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString("FinalResult_DeltaPhi_8Pi");
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
        
//         opts.mNColumns = 2;
//         opts.mLegendX = 0.43;
//         opts.mLegendXwidth = 0.45;
//         opts.mLegendY = 0.61;
//         opts.mLegendYwidth = 0.14;
//         opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        
        opts.mLegendX = 0.35;
        opts.mLegendXwidth = 0.17;
        opts.mLegendY = 0.65;
        opts.mLegendYwidth = 0.23;
        opts.mLegendTextSize = 0.7*0.7 * opts.mXaxisTitleSize;
        
        opts.mXpositionSystErrorBox.push_back(5);
        opts.mXpositionSystErrorBox.push_back(95);
        opts.mXpositionSystErrorBox.push_back(175);
        
        TLatex lReaction(.26, .9, "#it{p}+#it{p}#rightarrow#it{p'}+"+eightPionStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.7 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
    //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //     opts.mTextToDraw.push_back( lATLAS );
        
//         TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
//         TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}^{+},p_{T}^{-}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
//         TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
//         TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
//         TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
//         TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
//         TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
//         TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
//         TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
//         TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        mEnableLegend = true;
        mSpecialLabelRatio = true;
        mSpecialLabelOffsetFactor = 1.0;
        drawFinalResult( referenceHist, opts, Systematics_8PiDeltaPhi );
        mEnableLegend = kFALSE;
        mSpecialLabelRatio = false;
        mSpecialLabelOffsetFactor = 1.0;
        
    }
    
    
    
    
    for(int mr=0; mr<Util::nMassRanges; ++mr){
        TH1F *referenceHist = new TH1F( *oo4PiDeltaPhi_MassBins[mr]->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo4PiDeltaPhi_MassBins[mr]->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo4PiDeltaPhi_MassBins[mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo4PiDeltaPhi_MassBins[mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo4PiDeltaPhi_MassBins[mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_4PiDeltaPhi = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsDeltaPhiPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
        }
        
        //drawing final result
        DrawingOptions opts;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.13*1.7;
        opts.mRightMargin = 0.035*1.5;
        opts.mTopMargin = 0.03;
        opts.mBottomMargin = 0.13*1.7;
        
        //x axis
        opts.mXmin = referenceHist->GetXaxis()->GetXmin();
        opts.mXmax = referenceHist->GetXaxis()->GetXmax();
        opts.mXaxisTitle = TString("#Delta#varphi [deg]");
        opts.mXaxisTitleSize = 0.05*1.7;
        opts.mXaxisTitleOffset = 1.0;
        opts.mXaxisLabelSize = 0.05*1.7;
        opts.mXaxisLabelOffset = 0.005;
        opts.mXaxisTickLength = 0.015;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = (mr==Util::MASS_1 ? 1.25 : 1.5) * referenceHist->GetMaximum();
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/d#Delta#varphi [nb/deg]" : "d#sigma/d#Delta#varphi [#mub/deg]");
        opts.mYaxisTitleSize = 0.05*1.7;
        opts.mYaxisTitleOffset = 1.18;
        opts.mYaxisLabelSize = 0.05*1.7;
        opts.mYaxisLabelOffset = 0.013;
        opts.mYaxisTickLength = 0.015;
        
        opts.mSetDivisionsX = kTRUE;
        opts.mForceDivisionsX = kTRUE;
        opts.mXnDivisionA = 6;
        opts.mXnDivisionB = 3;
        opts.mXnDivisionC = 1;
        
        opts.mSetDivisionsY = kTRUE;
        opts.mYnDivisionA = 5;
        opts.mYnDivisionB = 5;
        opts.mYnDivisionC = 1;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.6;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString(Form("FinalResult_DeltaPhi_4Pi_MassBin%d", mr));
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
        
//         opts.mNColumns = 2;
//         opts.mLegendX = 0.43;
//         opts.mLegendXwidth = 0.45;
//         opts.mLegendY = 0.61;
//         opts.mLegendYwidth = 0.14;
//         opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        
        opts.mLegendX = 0.4;
        opts.mLegendXwidth = 0.17;
        opts.mLegendY = 0.28;
        opts.mLegendYwidth = 0.26;
    //       opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        opts.mLegendTextSize = 0.7/*55*/ * 0.7 * opts.mXaxisTitleSize;
        
        opts.mXpositionSystErrorBox.push_back(5);
        opts.mXpositionSystErrorBox.push_back(95);
        opts.mXpositionSystErrorBox.push_back(175);
        
        TLatex lReaction(.26, .9, "#kern[-0.29]{#font[72]{ATLAS} Internal     #it{p}+#it{p}#rightarrow#it{p'}+"+fourPionStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.65 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
    //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //     opts.mTextToDraw.push_back( lATLAS );
        
//         TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
//         TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}^{+},p_{T}^{-}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
//         TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
//         TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
//         TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
//         TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
//         TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
//         TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
//         TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
//         TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        TString massBinStr;
        TString massLimitsStr[2];
        massLimitsStr[Util::MIN].Form("%.1f", mr==Util::MASS_2 ? 1.5 : 2.5 );
        massLimitsStr[Util::MAX].Form("%.1f", mr==Util::MASS_1 ? 1.5 : 2.5 );
        if(mr==Util::MASS_1){
          massBinStr = "m("+fourPionStr+") < " + massLimitsStr[Util::MAX]+" GeV";
        } else
        if(mr==Util::MASS_2){
          massBinStr = massLimitsStr[Util::MIN]+" GeV < m("+fourPionStr+") < " + massLimitsStr[Util::MAX]+" GeV";
        } else
        if(mr==Util::MASS_3){
          massBinStr = "m("+fourPionStr+") > " + massLimitsStr[Util::MIN]+" GeV";
        }
        TLatex lCuts6(.26, .82 , massBinStr); lCuts6.SetNDC(); lCuts6.SetTextFont(42); lCuts6.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts6 );
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        
        if( mr==Util::MASS_1 )
            mEnableLegend = true;
        else 
            mEnableLegend = false;
        mSpecialLabelRatio = true;
        drawFinalResult( referenceHist, opts, Systematics_4PiDeltaPhi );
        mEnableLegend = false;
        mSpecialLabelRatio = false;
        
    }
    
    
    
    
    
    
    
    
    
    
    for(int mr=0; mr<Util::nMassRanges; ++mr){
        TH1F *referenceHist = new TH1F( *oo2PiDeltaPhi_MassBins[mr]->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo2PiDeltaPhi_MassBins[mr]->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo2PiDeltaPhi_MassBins[mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo2PiDeltaPhi_MassBins[mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo2PiDeltaPhi_MassBins[mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_2PiDeltaPhi = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsDeltaPhi_MassBins[mr]PidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
        }
        
        //drawing final result
        DrawingOptions opts;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.13*1.7;
        opts.mRightMargin = 0.035*1.5;
        opts.mTopMargin = 0.03;
        opts.mBottomMargin = 0.13*1.7;
        
        //x axis
        opts.mXmin = referenceHist->GetXaxis()->GetXmin();
        opts.mXmax = referenceHist->GetXaxis()->GetXmax();
        opts.mXaxisTitle = TString("#Delta#varphi [deg]");
        opts.mXaxisTitleSize = 0.05*1.7;
        opts.mXaxisTitleOffset = 1.0;
        opts.mXaxisLabelSize = 0.05*1.7;
        opts.mXaxisLabelOffset = 0.005;
        opts.mXaxisTickLength = 0.015;
        
        opts.mSetDivisionsX = kTRUE;
        opts.mForceDivisionsX = kTRUE;
        opts.mXnDivisionA = 6;
        opts.mXnDivisionB = 3;
        opts.mXnDivisionC = 1;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 1.5 * referenceHist->GetMaximum();
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/d#Delta#varphi [nb/deg]" : "d#sigma/d#Delta#varphi [#mub/deg]");
        opts.mYaxisTitleSize = 0.05*1.7;
        opts.mYaxisTitleOffset = 1.18;
        opts.mYaxisLabelSize = 0.05*1.7;
        opts.mYaxisLabelOffset = 0.013;
        opts.mYaxisTickLength = 0.015;
        
        opts.mSetDivisionsY = kTRUE;
        opts.mYnDivisionA = 5;
        opts.mYnDivisionB = 5;
        opts.mYnDivisionC = 1;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.6;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString(Form("FinalResult_DeltaPhi_2Pi_MassBin%d", mr));
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
        
//         opts.mNColumns = 2;
//         opts.mLegendX = 0.3;
//         opts.mLegendXwidth = 0.45;
//         opts.mLegendY = 0.55;
//         opts.mLegendYwidth = 0.14;
//         opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        
        opts.mLegendX = 0.44;
        opts.mLegendXwidth = 0.17;
        opts.mLegendY = 0.47;
        opts.mLegendYwidth = 0.26;
    //       opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        opts.mLegendTextSize = 0.7/*55*/ * 0.7 * opts.mXaxisTitleSize;
        
        opts.mXpositionSystErrorBox.push_back(5);
        opts.mXpositionSystErrorBox.push_back(95);
        opts.mXpositionSystErrorBox.push_back(175);
        
        TLatex lReaction(.29, .9, "#kern[-0.29]{#font[72]{ATLAS} Internal     #it{p}+#it{p}#rightarrow#it{p'}+"+pairStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.65 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
    //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //     opts.mTextToDraw.push_back( lATLAS );
        
//         TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
//         TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}^{+},p_{T}^{-}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
//         TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
//         TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
//         TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
//         TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
//         TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
//         TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
//         TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
//         TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        TString massBinStr;
        TString massLimitsStr[2];
        massLimitsStr[Util::MIN].Form(mr==Util::MASS_2 ? "%.0f" : "%.1f", mr==Util::MASS_2 ? 1.0 : 1.5 );
        massLimitsStr[Util::MAX].Form(mr==Util::MASS_1 ? "%.0f" : "%.1f", mr==Util::MASS_1 ? 1.0 : 1.5 );
        if(mr==Util::MASS_1){
          massBinStr = "m("+pairStr+") < " + massLimitsStr[Util::MAX]+" GeV";
        } else
        if(mr==Util::MASS_2){
          massBinStr = massLimitsStr[Util::MIN]+" GeV < m("+pairStr+") < " + massLimitsStr[Util::MAX]+" GeV";
        } else
        if(mr==Util::MASS_3){
          massBinStr = "m("+pairStr+") > " + massLimitsStr[Util::MIN]+" GeV";
        }
        TLatex lCuts6(.26, .82 , massBinStr); lCuts6.SetNDC(); lCuts6.SetTextFont(42); lCuts6.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts6 );
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        
        if( mr==Util::MASS_1 )
            mEnableLegend = true;
        else 
            mEnableLegend = false;
        mSpecialLabelRatio = true;
        drawFinalResult( referenceHist, opts, Systematics_2PiDeltaPhi );
        mEnableLegend = false;
        mSpecialLabelRatio = false;
    }
    
    
    
    
    
    
    {
        TH1F *referenceHist = new TH1F( *oo2PiMandelstamTSum->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo2PiMandelstamTSum->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo2PiMandelstamTSum->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo2PiMandelstamTSum->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo2PiMandelstamTSum->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_2PiMandelstamTSum = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsMandelstamTSumPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
        }
        
        //drawing final result
        DrawingOptions opts;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.13*1.7;
        opts.mRightMargin = 0.035*1.5;
        opts.mTopMargin = 0.03;
        opts.mBottomMargin = 0.13*1.7;
        
        //x axis
        opts.mXmin = 0.05;
        opts.mXmax = referenceHist->GetXaxis()->GetXmax();
        opts.mXaxisTitle = TString("|t_{#kern[-0.2]{#lower[-0.38]{1}}}+t_{#lower[-0.15]{2}}| [GeV^{2}]");
        opts.mXaxisTitleSize = 0.05*1.7;
        opts.mXaxisTitleOffset = 1.0;
        opts.mXaxisLabelSize = 0.05*1.7;
        opts.mXaxisLabelOffset = 0.005;
        opts.mXaxisTickLength = 0.015;
        
        opts.mSetDivisionsX = kTRUE;
        opts.mXnDivisionA = 4;
        opts.mXnDivisionB = 5;
        opts.mXnDivisionC = 1;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 1.3 * referenceHist->GetMaximum();
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/d|t_{#kern[-0.2]{#lower[-0.38]{1}}}+t_{#lower[-0.15]{2}}| [nb/GeV^{2}]" : "d#sigma/d|t_{#kern[-0.2]{#lower[-0.38]{1}}}+t_{#lower[-0.15]{2}}| [#mub/GeV^{2}]");
        opts.mYaxisTitleSize = 0.05*1.7;
        opts.mYaxisTitleOffset = 1.18;
        opts.mYaxisLabelSize = 0.05*1.7;
        opts.mYaxisLabelOffset = 0.013;
        opts.mYaxisTickLength = 0.015;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.6;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString("FinalResult_MandelstamTSum_2Pi");
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
        
//         opts.mInsert = true;
//         opts.mX1Insert = 0.43;
//         opts.mX2Insert = 0.95;
//         opts.mY1Insert = 0.15;
//         opts.mY2Insert = 0.56;
//         opts.mXMinInstert = 0.8;
//         opts.mXMaxInstert = referenceHist->GetXaxis()->GetXmax();
        
//         opts.mNColumns = 2;
//         opts.mLegendX = 0.43;
//         opts.mLegendXwidth = 0.45;
//         opts.mLegendY = 0.61;
//         opts.mLegendYwidth = 0.14;
//         opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        opts.mLegendX = 0.55;
        opts.mLegendXwidth = 0.17;
        opts.mLegendY = 0.5;
        opts.mLegendYwidth = 0.23;
        opts.mLegendTextSize = 0.7*0.7 * opts.mXaxisTitleSize;
        
        
        opts.mXpositionSystErrorBox.push_back(0.105);
        opts.mXpositionSystErrorBox.push_back(0.225);
        opts.mXpositionSystErrorBox.push_back(0.51);
        opts.mXpositionSystErrorBox.push_back(1.0);
        
        TLatex lReaction(.3, .9, "#it{p}+#it{p}#rightarrow#it{p'}+"+pairStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.7 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
    //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //     opts.mTextToDraw.push_back( lATLAS );
        TLatex lATLAS(0.6, 0.83, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(0.7 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lATLAS );
        
//         TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
//         TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}^{+},p_{T}^{-}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
//         TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
//         TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
//         TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
//         TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
//         TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
//         TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
//         TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
//         TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        
        mEnableLegend = true;
        mSpecialLabelRatio = true;
        mSpecialLabelOffsetFactor = 1.0;
        drawFinalResult( referenceHist, opts, Systematics_2PiMandelstamTSum );
        mEnableLegend = kFALSE;
        mSpecialLabelRatio = false;
        mSpecialLabelOffsetFactor = 1.0;
        
    }
    
    
    
    
    
    {
        TH1F *referenceHist = new TH1F( *oo4PiMandelstamTSum->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo4PiMandelstamTSum->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo4PiMandelstamTSum->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo4PiMandelstamTSum->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo4PiMandelstamTSum->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_4PiMandelstamTSum = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsMandelstamTSumPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
        }
        
        //drawing final result
        DrawingOptions opts;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
         opts.mLeftMargin = 0.13*1.7;
        opts.mRightMargin = 0.035*1.5;
        opts.mTopMargin = 0.03;
        opts.mBottomMargin = 0.13*1.7;
        
        //x axis
        opts.mXmin = 0.05;
        opts.mXmax = referenceHist->GetXaxis()->GetXmax();
        opts.mXaxisTitle = TString("|t_{#kern[-0.2]{#lower[-0.38]{1}}}+t_{#lower[-0.15]{2}}| [GeV^{2}]");
        opts.mXaxisTitleSize = 0.05*1.7;
        opts.mXaxisTitleOffset = 1.0;
        opts.mXaxisLabelSize = 0.05*1.7;
        opts.mXaxisLabelOffset = 0.005;
        opts.mXaxisTickLength = 0.015;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 1.3 * referenceHist->GetMaximum();
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/d|t_{#kern[-0.2]{#lower[-0.38]{1}}}+t_{#lower[-0.15]{2}}| [nb/GeV^{2}]" : "d#sigma/d|t_{#kern[-0.2]{#lower[-0.38]{1}}}+t_{#lower[-0.15]{2}}| [#mub/GeV^{2}]");
        opts.mYaxisTitleSize = 0.05*1.7;
        opts.mYaxisTitleOffset = 1.18;
        opts.mYaxisLabelSize = 0.05*1.7;
        opts.mYaxisLabelOffset = 0.013;
        opts.mYaxisTickLength = 0.015;
        
        
        opts.mSetDivisionsX = kTRUE;
        opts.mXnDivisionA = 4;
        opts.mXnDivisionB = 5;
        opts.mXnDivisionC = 1;

        opts.mSetDivisionsY = kTRUE;
        opts.mYnDivisionA = 5;
        opts.mYnDivisionB = 5;
        opts.mYnDivisionC = 1;
        
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.6;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString("FinalResult_MandelstamTSum_4Pi");
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
        
//         opts.mInsert = true;
//         opts.mX1Insert = 0.43;
//         opts.mX2Insert = 0.95;
//         opts.mY1Insert = 0.15;
//         opts.mY2Insert = 0.56;
//         opts.mXMinInstert = 0.8;
//         opts.mXMaxInstert = referenceHist->GetXaxis()->GetXmax();
        
//         opts.mNColumns = 2;
//         opts.mLegendX = 0.43;
//         opts.mLegendXwidth = 0.45;
//         opts.mLegendY = 0.61;
//         opts.mLegendYwidth = 0.14;
//         opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        
        opts.mLegendX = 0.45;
        opts.mLegendXwidth = 0.17;
        opts.mLegendY = 0.46;
        opts.mLegendYwidth = 0.29;
        opts.mLegendTextSize = 0.7*0.7 * opts.mXaxisTitleSize;
        
        opts.mXpositionSystErrorBox.push_back(0.105);
        opts.mXpositionSystErrorBox.push_back(0.225);
        opts.mXpositionSystErrorBox.push_back(0.51);
        opts.mXpositionSystErrorBox.push_back(1.0);
        
        TLatex lReaction(.25, .9, "#kern[-0.29]{#font[72]{ATLAS} Internal     #it{p}+#it{p}#rightarrow#it{p'}+"+fourPionStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.7 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
    //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //     opts.mTextToDraw.push_back( lATLAS );
        
//         TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
//         TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}^{+},p_{T}^{-}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
//         TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
//         TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
//         TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
//         TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
//         TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
//         TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
//         TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
//         TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        
        mEnableLegend = true;
        mSpecialLabelRatio = true;
        mSpecialLabelOffsetFactor = 0.95;
        drawFinalResult( referenceHist, opts, Systematics_4PiMandelstamTSum );
        mEnableLegend = kFALSE;
        mSpecialLabelRatio = false;
        mSpecialLabelOffsetFactor = 1.0;
        
    }
    
    
    
    {
        TH1F *referenceHist = new TH1F( *oo6PiMandelstamTSum->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo6PiMandelstamTSum->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo6PiMandelstamTSum->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo6PiMandelstamTSum->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo6PiMandelstamTSum->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_6PiMandelstamTSum = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsMandelstamTSumPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
            histMC[fileId]->Scale(pion6PythiaScaleFactor);
        }
        
//drawing final result
        DrawingOptions opts;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
         opts.mLeftMargin = 0.13*1.7;
        opts.mRightMargin = 0.035*1.5;
        opts.mTopMargin = 0.03;
        opts.mBottomMargin = 0.13*1.7;
        
        //x axis
        opts.mXmin = 0.05;
        opts.mXmax = referenceHist->GetXaxis()->GetXmax();
        opts.mXaxisTitle = TString("|t_{#kern[-0.2]{#lower[-0.38]{1}}}+t_{#lower[-0.15]{2}}| [GeV^{2}]");
        opts.mXaxisTitleSize = 0.05*1.7;
        opts.mXaxisTitleOffset = 1.0;
        opts.mXaxisLabelSize = 0.05*1.7;
        opts.mXaxisLabelOffset = 0.005;
        opts.mXaxisTickLength = 0.015;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 1.55 * referenceHist->GetMaximum();
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/d|t_{#kern[-0.2]{#lower[-0.38]{1}}}+t_{#lower[-0.15]{2}}| [nb/GeV^{2}]" : "d#sigma/d|t_{#kern[-0.2]{#lower[-0.38]{1}}}+t_{#lower[-0.15]{2}}| [#mub/GeV^{2}]");
        opts.mYaxisTitleSize = 0.05*1.7;
        opts.mYaxisTitleOffset = 1.18;
        opts.mYaxisLabelSize = 0.05*1.7;
        opts.mYaxisLabelOffset = 0.013;
        opts.mYaxisTickLength = 0.015;
        
        
        opts.mSetDivisionsX = kTRUE;
        opts.mXnDivisionA = 4;
        opts.mXnDivisionB = 5;
        opts.mXnDivisionC = 1;

        opts.mSetDivisionsY = kTRUE;
        opts.mYnDivisionA = 5;
        opts.mYnDivisionB = 5;
        opts.mYnDivisionC = 1;
        
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.6;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString("FinalResult_MandelstamTSum_6Pi");
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
        
//         opts.mInsert = true;
//         opts.mX1Insert = 0.43;
//         opts.mX2Insert = 0.95;
//         opts.mY1Insert = 0.15;
//         opts.mY2Insert = 0.56;
//         opts.mXMinInstert = 0.8;
//         opts.mXMaxInstert = referenceHist->GetXaxis()->GetXmax();
        
//         opts.mNColumns = 2;
//         opts.mLegendX = 0.43;
//         opts.mLegendXwidth = 0.45;
//         opts.mLegendY = 0.61;
//         opts.mLegendYwidth = 0.14;
//         opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        
        opts.mLegendX = 0.57;
        opts.mLegendXwidth = 0.17;
        opts.mLegendY = 0.46;
        opts.mLegendYwidth = 0.29;
        opts.mLegendTextSize = 0.7*0.7 * opts.mXaxisTitleSize;
        
        opts.mXpositionSystErrorBox.push_back(0.105);
        opts.mXpositionSystErrorBox.push_back(0.225);
        opts.mXpositionSystErrorBox.push_back(0.51);
        opts.mXpositionSystErrorBox.push_back(1.0);
        
        TLatex lReaction(.25, .9, "#it{p}+#it{p}#rightarrow#it{p'}+"+sixPionStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.7 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
    //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //     opts.mTextToDraw.push_back( lATLAS );
        
//         TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
//         TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}^{+},p_{T}^{-}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
//         TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
//         TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
//         TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
//         TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
//         TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
//         TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
//         TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
//         TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        
        mEnableLegend = true;
        mSpecialLabelRatio = true;
        mSpecialLabelOffsetFactor = 0.95;
        drawFinalResult( referenceHist, opts, Systematics_6PiMandelstamTSum );
        mEnableLegend = kFALSE;
        mSpecialLabelRatio = false;
        mSpecialLabelOffsetFactor = 1.0;
        
    }
    
    
    
    {
        TH1F *referenceHist = new TH1F( *oo8PiMandelstamTSum->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo8PiMandelstamTSum->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo8PiMandelstamTSum->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo8PiMandelstamTSum->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo8PiMandelstamTSum->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_8PiMandelstamTSum = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsMandelstamTSumPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
            histMC[fileId]->Scale(pion8PythiaScaleFactor);
        }
        
        //drawing final result
        DrawingOptions opts;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
         opts.mLeftMargin = 0.13*1.7;
        opts.mRightMargin = 0.035*1.5;
        opts.mTopMargin = 0.03;
        opts.mBottomMargin = 0.13*1.7;
        
        //x axis
        opts.mXmin = 0.05;
        opts.mXmax = referenceHist->GetXaxis()->GetXmax();
        opts.mXaxisTitle = TString("|t_{#kern[-0.2]{#lower[-0.38]{1}}}+t_{#lower[-0.15]{2}}| [GeV^{2}]");
        opts.mXaxisTitleSize = 0.05*1.7;
        opts.mXaxisTitleOffset = 1.0;
        opts.mXaxisLabelSize = 0.05*1.7;
        opts.mXaxisLabelOffset = 0.005;
        opts.mXaxisTickLength = 0.015;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 1.5 * referenceHist->GetMaximum();
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/d|t_{#kern[-0.2]{#lower[-0.38]{1}}}+t_{#lower[-0.15]{2}}| [nb/GeV^{2}]" : "d#sigma/d|t_{#kern[-0.2]{#lower[-0.38]{1}}}+t_{#lower[-0.15]{2}}| [#mub/GeV^{2}]");
        opts.mYaxisTitleSize = 0.05*1.7;
        opts.mYaxisTitleOffset = 1.18;
        opts.mYaxisLabelSize = 0.05*1.7;
        opts.mYaxisLabelOffset = 0.013;
        opts.mYaxisTickLength = 0.015;
        
        
        opts.mSetDivisionsX = kTRUE;
        opts.mXnDivisionA = 4;
        opts.mXnDivisionB = 5;
        opts.mXnDivisionC = 1;

        opts.mSetDivisionsY = kTRUE;
        opts.mYnDivisionA = 5;
        opts.mYnDivisionB = 5;
        opts.mYnDivisionC = 1;
        
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.6;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString("FinalResult_MandelstamTSum_8Pi");
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
        
//         opts.mInsert = true;
//         opts.mX1Insert = 0.43;
//         opts.mX2Insert = 0.95;
//         opts.mY1Insert = 0.15;
//         opts.mY2Insert = 0.56;
//         opts.mXMinInstert = 0.8;
//         opts.mXMaxInstert = referenceHist->GetXaxis()->GetXmax();
        
//         opts.mNColumns = 2;
//         opts.mLegendX = 0.43;
//         opts.mLegendXwidth = 0.45;
//         opts.mLegendY = 0.61;
//         opts.mLegendYwidth = 0.14;
//         opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        
        opts.mLegendX = 0.5;
        opts.mLegendXwidth = 0.17;
        opts.mLegendY = 0.46;
        opts.mLegendYwidth = 0.29;
        opts.mLegendTextSize = 0.7*0.7 * opts.mXaxisTitleSize;
        
        opts.mXpositionSystErrorBox.push_back(0.105);
        opts.mXpositionSystErrorBox.push_back(0.225);
        opts.mXpositionSystErrorBox.push_back(0.51);
        opts.mXpositionSystErrorBox.push_back(1.0);
        
        TLatex lReaction(.25, .9, "#it{p}+#it{p}#rightarrow#it{p'}+"+eightPionStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.7 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
    //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //     opts.mTextToDraw.push_back( lATLAS );
        
//         TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
//         TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}^{+},p_{T}^{-}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
//         TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
//         TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
//         TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
//         TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
//         TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
//         TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
//         TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
//         TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        
        mEnableLegend = true;
        mSpecialLabelRatio = true;
        mSpecialLabelOffsetFactor = 0.95;
        drawFinalResult( referenceHist, opts, Systematics_8PiMandelstamTSum );
        mEnableLegend = kFALSE;
        mSpecialLabelRatio = false;
        mSpecialLabelOffsetFactor = 1.0;
        
    }
    
    
    
    
    
    
    
    
    for(int mr=0; mr<Util::nMassRanges; ++mr){
        TH1F *referenceHist = new TH1F( *oo4PiMandelstamTSum_MassBins[mr]->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo4PiMandelstamTSum_MassBins[mr]->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo4PiMandelstamTSum_MassBins[mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo4PiMandelstamTSum_MassBins[mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo4PiMandelstamTSum_MassBins[mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_4PiMandelstamTSum = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsMandelstamTSumPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
        }
        
        //drawing final result
        DrawingOptions opts;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.13*1.7;
        opts.mRightMargin = 0.035*1.5;
        opts.mTopMargin = 0.03;
        opts.mBottomMargin = 0.13*1.7;
        
        //x axis
        opts.mXmin = 0.05;
        opts.mXmax = referenceHist->GetXaxis()->GetXmax();
        opts.mXaxisTitle = TString("|t_{#kern[-0.2]{#lower[-0.38]{1}}}+t_{#lower[-0.15]{2}}| [GeV^{2}]");
        opts.mXaxisTitleSize = 0.05*1.7;
        opts.mXaxisTitleOffset = 1.0;
        opts.mXaxisLabelSize = 0.05*1.7;
        opts.mXaxisLabelOffset = 0.005;
        opts.mXaxisTickLength = 0.015;
        
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = (mr==Util::MASS_2 ? 1.6 : 1.3) * referenceHist->GetMaximum();
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/d|t_{#kern[-0.2]{#lower[-0.38]{1}}}+t_{#lower[-0.15]{2}}| [nb/GeV^{2}]" : "d#sigma/d|t_{#kern[-0.2]{#lower[-0.38]{1}}}+t_{#lower[-0.15]{2}}| [#mub/GeV^{2}]");
        opts.mYaxisTitleSize = 0.05*1.7;
        opts.mYaxisTitleOffset = 1.3;
        opts.mYaxisLabelSize = 0.05*1.7;
        opts.mYaxisLabelOffset = 0.005;
        opts.mYaxisTickLength = 0.015;

        opts.mSetDivisionsX = kTRUE;
        opts.mXnDivisionA = 4;
        opts.mXnDivisionB = 5;
        opts.mXnDivisionC = 1;
        
        opts.mSetDivisionsY = kTRUE;
        opts.mYnDivisionA = 5;
        opts.mYnDivisionB = 5;
        opts.mYnDivisionC = 1;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.6;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString(Form("FinalResult_MandelstamTSum_4Pi_MassBin%d", mr));
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
        
//         opts.mInsert = true;
//         opts.mX1Insert = 0.43;
//         opts.mX2Insert = 0.95;
//         opts.mY1Insert = 0.15;
//         opts.mY2Insert = 0.56;
//         opts.mXMinInstert = 0.8;
//         opts.mXMaxInstert = referenceHist->GetXaxis()->GetXmax();
        
        opts.mLegendX = 0.45;
        opts.mLegendXwidth = 0.17;
        opts.mLegendY = 0.46;
        opts.mLegendYwidth = 0.23;
        opts.mLegendTextSize = 0.7*0.7 * opts.mXaxisTitleSize;
        
        opts.mXpositionSystErrorBox.push_back(0.105);
        opts.mXpositionSystErrorBox.push_back(0.225);
        opts.mXpositionSystErrorBox.push_back(0.51);
        opts.mXpositionSystErrorBox.push_back(1.0);
        
        TLatex lReaction(.27, .9, "#kern[-0.29]{#font[72]{ATLAS} Internal     #it{p}+#it{p}#rightarrow#it{p'}+"+fourPionStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.65 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
    //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //     opts.mTextToDraw.push_back( lATLAS );
        
//         TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
//         TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}^{+},p_{T}^{-}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
//         TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
//         TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
//         TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
//         TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
//         TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
//         TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
//         TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
//         TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        TString massBinStr;
        TString massLimitsStr[2];
        massLimitsStr[Util::MIN].Form("%.1f", mr==Util::MASS_2 ? 1.5 : 2.5 );
        massLimitsStr[Util::MAX].Form("%.1f", mr==Util::MASS_1 ? 1.5 : 2.5 );
        if(mr==Util::MASS_1){
          massBinStr = "m("+fourPionStr+") < " + massLimitsStr[Util::MAX]+" GeV";
        } else
        if(mr==Util::MASS_2){
          massBinStr = massLimitsStr[Util::MIN]+" GeV < m("+fourPionStr+") < " + massLimitsStr[Util::MAX]+" GeV";
        } else
        if(mr==Util::MASS_3){
          massBinStr = "m("+fourPionStr+") > " + massLimitsStr[Util::MIN]+" GeV";
        }
        TLatex lCuts6(.36 + (mr!=Util::MASS_2 ? 0.03 : -0.1), .82 , massBinStr); lCuts6.SetNDC(); lCuts6.SetTextFont(42);  lCuts6.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts6 );
        
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        if(mr==Util::MASS_1)
            mEnableLegend = true;
        else 
            mEnableLegend = false;
        mSpecialLabelRatio = true;
        mSpecialLabelOffsetFactor = 0.95;
        drawFinalResult( referenceHist, opts, Systematics_4PiMandelstamTSum );
        mEnableLegend = false;
        mSpecialLabelOffsetFactor = 1.0;
        mSpecialLabelRatio = false;
        
    }
    
    
    
    
    
    
    
    mSpecialLabelOffsetFactor = 0.94;
    
    for(int mr=0; mr<Util::nMassRanges; ++mr){
        TH1F *referenceHist = new TH1F( *oo2PiMandelstamTSum_MassBins[mr]->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo2PiMandelstamTSum_MassBins[mr]->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo2PiMandelstamTSum_MassBins[mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo2PiMandelstamTSum_MassBins[mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo2PiMandelstamTSum_MassBins[mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_2PiMandelstamTSum = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsMandelstamTSumPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
        }
        
        //drawing final result
        DrawingOptions opts;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.13*1.7;
        opts.mRightMargin = 0.035*1.5;
        opts.mTopMargin = 0.03;
        opts.mBottomMargin = 0.13*1.7;
        
        //x axis
        opts.mXmin = 0.05;
        opts.mXmax = referenceHist->GetXaxis()->GetXmax();
        opts.mXaxisTitle = TString("|t_{#kern[-0.2]{#lower[-0.38]{1}}}+t_{#lower[-0.15]{2}}| [GeV^{2}]");
        opts.mXaxisTitleSize = 0.05*1.7;
        opts.mXaxisTitleOffset = 1.0;
        opts.mXaxisLabelSize = 0.05*1.7;
        opts.mXaxisLabelOffset = 0.005;
        opts.mXaxisTickLength = 0.015;
        
        opts.mSetDivisionsX = kTRUE;
        opts.mXnDivisionA = 4;
        opts.mXnDivisionB = 5;
        opts.mXnDivisionC = 1;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 1.3 * referenceHist->GetMaximum();
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/d|t_{#kern[-0.2]{#lower[-0.38]{1}}}+t_{#lower[-0.15]{2}}| [nb/GeV^{2}]" : "d#sigma/d|t_{#kern[-0.2]{#lower[-0.38]{1}}}+t_{#lower[-0.15]{2}}| [#mub/GeV^{2}]");
        opts.mYaxisTitleSize = 0.05*1.7;
        opts.mYaxisTitleOffset = 1.3;
        opts.mYaxisLabelSize = 0.05*1.7;
        opts.mYaxisLabelOffset = 0.005;
        opts.mYaxisTickLength = 0.015;
        
        opts.mSetDivisionsY = kTRUE;
        opts.mYnDivisionA = 5;
        opts.mYnDivisionB = 5;
        opts.mYnDivisionC = 1;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.6;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString(Form("FinalResult_MandelstamTSum_2Pi_MassBin%d", mr));
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
        
//         opts.mInsert = true;
//         opts.mX1Insert = 0.43;
//         opts.mX2Insert = 0.95;
//         opts.mY1Insert = 0.15;
//         opts.mY2Insert = 0.56;
//         opts.mXMinInstert = 0.8;
//         opts.mXMaxInstert = referenceHist->GetXaxis()->GetXmax();
        
//         opts.mNColumns = 2;
//         opts.mLegendX = 0.43;
//         opts.mLegendXwidth = 0.45;
//         opts.mLegendY = 0.61;
//         opts.mLegendYwidth = 0.14;
//         opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
//         
        opts.mLegendX = 0.55;
        opts.mLegendXwidth = 0.17;
        opts.mLegendY = 0.43;
        opts.mLegendYwidth = 0.23;
        opts.mLegendTextSize = 0.7*0.7 * opts.mXaxisTitleSize;
        
        opts.mXpositionSystErrorBox.push_back(0.105);
        opts.mXpositionSystErrorBox.push_back(0.225);
        opts.mXpositionSystErrorBox.push_back(0.51);
        opts.mXpositionSystErrorBox.push_back(1.0);
        
        TLatex lReaction(.31, .90, "#kern[-0.29]{#font[72]{ATLAS} Internal     #it{p}+#it{p}#rightarrow#it{p'}+"+pairStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.65 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
    //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //     opts.mTextToDraw.push_back( lATLAS );
        
//         TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
//         TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}^{+},p_{T}^{-}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
//         TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
//         TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
//         TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
//         TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
//         TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
//         TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
//         TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
//         TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        
        TString massBinStr;
        TString massLimitsStr[2];
        massLimitsStr[Util::MIN].Form(mr==Util::MASS_2 ? "%.0f" : "%.1f", mr==Util::MASS_2 ? 1.0 : 1.5 );
        massLimitsStr[Util::MAX].Form(mr==Util::MASS_1 ? "%.0f" : "%.1f", mr==Util::MASS_1 ? 1.0 : 1.5 );
        if(mr==Util::MASS_1){
          massBinStr = "m("+pairStr+") < " + massLimitsStr[Util::MAX]+" GeV";
        } else
        if(mr==Util::MASS_2){
          massBinStr = massLimitsStr[Util::MIN]+" GeV < m("+pairStr+") < " + massLimitsStr[Util::MAX]+" GeV";
        } else
        if(mr==Util::MASS_3){
          massBinStr = "m("+pairStr+") > " + massLimitsStr[Util::MIN]+" GeV";
        }
        TLatex lCuts6(.36 + (mr==Util::MASS_2 ? 0.0 : 0.1), .82 , massBinStr); lCuts6.SetNDC(); lCuts6.SetTextFont(42);  lCuts6.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts6 );
        
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        if(mr==Util::MASS_1)
            mEnableLegend = true;
        else 
            mEnableLegend = false;
        mSpecialLabelRatio = true;
        drawFinalResult( referenceHist, opts, Systematics_2PiMandelstamTSum );
        mEnableLegend = false;
        mSpecialLabelRatio = false;
    }
    
    mSpecialLabelOffsetFactor = 1.0;
    
    
    
    
    for(int rf=0; rf<Util::nReferenceFrames; ++rf){
        TH1F *referenceHist = new TH1F( *oo2PiCosTheta[rf]->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo2PiCosTheta[rf]->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo2PiCosTheta[rf]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo2PiCosTheta[rf]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo2PiCosTheta[rf]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_2PiCosTheta = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsCosThetaPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
        }
        
        //drawing final result
        DrawingOptions opts;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.12;
        opts.mRightMargin = 0.035;
        opts.mTopMargin = 0.02;
        opts.mBottomMargin = 0.13;
        
        //x axis
        opts.mXmin = -1.0;
        opts.mXmax = 1.0;
        opts.mXaxisTitle = TString("cos#theta^{"+mUtil->referenceFrameShortName(rf)+"}("+singleTrkStr[Util::PLUS]+")");
        opts.mXaxisTitleSize = 0.05;
        opts.mXaxisTitleOffset = 1.04;
        opts.mXaxisLabelSize = 0.05;
        opts.mXaxisLabelOffset = 0.007;
        opts.mXaxisTickLength = 0.015;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 2.0 * referenceHist->GetMaximum();
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/dcos#theta^{"+mUtil->referenceFrameShortName(rf)+"}("+singleTrkStr[Util::PLUS]+") [nb]" : "d#sigma/dcos#theta^{"+mUtil->referenceFrameShortName(rf)+"}("+singleTrkStr[Util::PLUS]+") [#mub]");
        opts.mYaxisTitleSize = 0.05;
        opts.mYaxisTitleOffset = 1.21;
        opts.mYaxisLabelSize = 0.05;
        opts.mYaxisLabelOffset = 0.007;
        opts.mYaxisTickLength = 0.015;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.2;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString("FinalResult_CosTheta_"+mUtil->referenceFrameShortName(rf)+"_2Pi");
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
        
        opts.mNColumns = 2;
        opts.mLegendX = 0.43;
        opts.mLegendXwidth = 0.45;
        opts.mLegendY = 0.61;
        opts.mLegendYwidth = 0.14;
        opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        
        opts.mXpositionSystErrorBox.push_back(0.05);
        opts.mXpositionSystErrorBox.push_back(0.65);
        
        TLatex lReaction(.4, .91, "#kern[-0.29]{#font[72]{ATLAS} Internal     #it{p}+#it{p}#rightarrow#it{p'}+"+pairStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
    //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //     opts.mTextToDraw.push_back( lATLAS );
        
        TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
        TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}^{+},p_{T}^{-}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
        TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
        TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
        TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
        TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
        TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
        TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
        TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
        TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        drawFinalResult( referenceHist, opts, Systematics_2PiCosTheta );
        
    }
    
    
    
    
    for(int mr=0; mr<Util::nMassRanges; ++mr){
        for(int rf=0; rf<Util::nReferenceFrames; ++rf){
            TH1F *referenceHist = new TH1F( *oo2PiCosTheta_MassBins[rf][mr]->hQ_Weighted[Util::CH_SUM_0] );
            TString oldName = referenceHist->GetName();
            referenceHist->SetName( Form("tmp%s", oldName.Data()) );
            TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
            for(int i=0; i<Util::nSystChecks; ++i)
            systCheckHists[i] = new TH1F( *oo2PiCosTheta_MassBins[rf][mr]->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
            
            TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo2PiCosTheta_MassBins[rf][mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
            TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo2PiCosTheta_MassBins[rf][mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo2PiCosTheta_MassBins[rf][mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
            TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
            bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

            
            SystematicsOutput Systematics_2PiCosTheta = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
            
        //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsCosThetaPidWeighted[OPPO][pid], pid );

            transformToCrossSection(referenceHist);
            transformToCrossSection(bkgdHistogram);
            mUtil->subtractBackground( referenceHist, bkgdHistogram );
            
        //     referenceHist->Divide( pidEff );
            
            
            std::vector< TH1F* > histMC;
            for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
                TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
                if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
                histMC.push_back( new TH1F( *tmpHistogram ) );
                histMC[fileId]->SetName(Form("tmp%d",fileId));
                if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
                histMC[fileId]->SetDirectory(nullptr);
                histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
                histMC[fileId]->SetLineWidth(2 );
            }
            for(unsigned int fileId=0; fileId<filesMC_angularStudies[pid].size(); ++fileId){
                TString extraStr(fileId==0 ? "_S0m" : (fileId==1 ? "_D0m" : "") );
                TString oldName_2 = oldName;
                oldName_2.ReplaceAll("_ChSum0_Weighted", extraStr+"_ChSum0_Weighted");
                TH1F* hist = dynamic_cast<TH1F*>( filesMC_angularStudies[pid][fileId]->Get( oldName_2 ) );
                if(!hist) continue;
                TH1F *histCopy = new TH1F( *hist );
                histCopy->Scale( referenceHist->Integral(0, -1, "width") / histCopy->Integral(0, -1, "width") );
                histMC.push_back( histCopy );
                histMC[filesMC[pid].size()+fileId]->SetName(Form("tmp%d", static_cast<int>(filesMC[pid].size()+fileId)));
                if(histMC[filesMC[pid].size()+fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
                histMC[filesMC[pid].size()+fileId]->SetDirectory(nullptr);
                histMC[filesMC[pid].size()+fileId]->SetLineColor(colorsMC_angularStudies[pid][fileId]);
                histMC[filesMC[pid].size()+fileId]->SetLineWidth(2);
            }
            
            //drawing final result
            DrawingOptions opts;
            
            opts.mCanvasWidth = 900;
            opts.mCanvasHeight = 800;
            
            opts.mLeftMargin = 0.13*1.7;
            opts.mRightMargin = 0.035*1.5;
            opts.mTopMargin = 0.03;
            opts.mBottomMargin = 0.13*1.7;
            
            //x axis
            opts.mXmin = -1.0;
            opts.mXmax = 1.0;
            opts.mXaxisTitle = TString("cos#theta^{"+mUtil->referenceFrameShortName(rf)+"}("+singleTrkStr[Util::PLUS]+")");
            opts.mXaxisTitleSize = 0.05*1.7;
            opts.mXaxisTitleOffset = 1.0;
            opts.mXaxisLabelSize = 0.05*1.7;
            opts.mXaxisLabelOffset = 0.005;
            opts.mXaxisTickLength = 0.015;
            
            //y axis
            opts.mYmin = 0;
            opts.mYmax = (mr==Util::MASS_2 ? 2.0 : 1.55) * referenceHist->GetMaximum();
            bool changeMubToNb = false;
            if( opts.mYmax < 1.0 )
            changeMubToNb = true;
            opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/dcos#theta^{"+mUtil->referenceFrameShortName(rf)+"}("+singleTrkStr[Util::PLUS]+") [nb]" : "d#sigma/dcos#theta^{"+mUtil->referenceFrameShortName(rf)+"}("+singleTrkStr[Util::PLUS]+") [#mub]");
            opts.mYaxisTitleSize = 0.05*1.7;
            opts.mYaxisTitleOffset = 1.3;
            opts.mYaxisLabelSize = 0.05*1.7;
            opts.mYaxisLabelOffset = 0.005;
            opts.mYaxisTickLength = 0.015;
            
            opts.mSetDivisionsX = kTRUE;
        //       opts.mForceDivisionsX = kTRUE;
            opts.mXnDivisionA = 4;
            opts.mXnDivisionB = 5;
            opts.mXnDivisionC = 1;
            
            opts.mSetDivisionsY = kTRUE;
            opts.mYnDivisionA = 5;
            opts.mYnDivisionB = 5;
            opts.mYnDivisionC = 1;
            
            
            opts.mMarkerStyle = 20;
            opts.mMarkerColor = kBlack;
            opts.mMarkerSize = 1.6;
            
            opts.mLineStyle = 1;
            opts.mLineColor = kBlack;
            opts.mLineWidth = 2;
            
            opts.mPdfName = TString("FinalResult_CosTheta_"+mUtil->referenceFrameShortName(rf)+Form("_2Pi_MassBin%d", mr));
            
            for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
                if(histMC[fileId]){
                    opts.mHistTH1F.push_back( histMC[fileId] );
                    opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                    
                    opts.mObjForLegend.push_back( histMC[fileId] );
                    opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                    opts.mDrawTypeForLegend.push_back( "l" );
                }
            }
            for(unsigned int fileId=0; fileId<filesMC_angularStudies[pid].size(); ++fileId){
                if(histMC[filesMC[pid].size()+fileId]){
                    opts.mHistTH1F.push_back( histMC[filesMC[pid].size()+fileId] );
                    opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                    
                    opts.mObjForLegend.push_back( histMC[filesMC[pid].size()+fileId] );
                    opts.mDesriptionForLegend.push_back(  legendStrMC_angularStudies[pid][fileId] );
                    opts.mDrawTypeForLegend.push_back( "l" );
                }
            }
            
            opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
            opts.mSystErrorBoxColor = 16;
            opts.mSystErrorTotalBoxColor = 14;
            opts.mSystErrorBoxOpacity = 0.6;
            
//             opts.mNColumns = 2;
//             opts.mLegendX = 0.43;
//             opts.mLegendXwidth = 0.45;
//             opts.mLegendY = 0.61;
//             opts.mLegendYwidth = 0.14;
//             opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
            
            opts.mNColumns = 2;
            opts.mLegendX = /*pid==Util::PROTON ?*/ 0.24 /*: 0.39*/;
            opts.mLegendXwidth = 0.55;
            opts.mLegendY = /*pid==Util::PROTON ?*/ 0.64 /*: 0.17*/;
            opts.mLegendYwidth = 0.13;
            opts.mLegendTextSize = 0.6 * 0.7 * opts.mXaxisTitleSize;
            
            opts.mXpositionSystErrorBox.push_back(0.05);
            opts.mXpositionSystErrorBox.push_back(0.65);
            
            TLatex lReaction(.32, .9, "#kern[-0.29]{#font[72]{ATLAS} Internal     #it{p}+#it{p}#rightarrow#it{p'}+"+pairStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.65 * opts.mXaxisTitleSize);
            opts.mTextToDraw.push_back( lReaction );
        //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
        //     opts.mTextToDraw.push_back( lATLAS );
            
//             TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
//             TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}^{+},p_{T}^{-}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
//             TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
//             TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
//             TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
//             TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
//             TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
//             TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
//             TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
//             TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
            
            TString massBinStr;
            TString massLimitsStr[2];
            massLimitsStr[Util::MIN].Form(mr==Util::MASS_2 ? "%.0f" : "%.1f", mr==Util::MASS_2 ? 1.0 : 1.5 );
            massLimitsStr[Util::MAX].Form(mr==Util::MASS_1 ? "%.0f" : "%.1f", mr==Util::MASS_1 ? 1.0 : 1.5 );
            if(mr==Util::MASS_1){
            massBinStr = "m("+pairStr+") < " + massLimitsStr[Util::MAX]+" GeV";
            } else
            if(mr==Util::MASS_2){
            massBinStr = massLimitsStr[Util::MIN]+" GeV < m("+pairStr+") < " + massLimitsStr[Util::MAX]+" GeV";
            } else
            if(mr==Util::MASS_3){
            massBinStr = "m("+pairStr+") > " + massLimitsStr[Util::MIN]+" GeV";
            }
            TLatex lCuts6(.33 + (mr==Util::MASS_2 ? 0.0 : 0.1), .82 , massBinStr); lCuts6.SetNDC(); lCuts6.SetTextFont(42);  lCuts6.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts6 );
            
            if(changeMubToNb){
                opts.mScalingFactor = 1e3;
                opts.mScale = kTRUE;
            }
            
            mSpecialLabelOffsetFactor = 0.95;
            mSpecialLabelRatio = true;
            double prevVal = mSpecialLegendMarginFactor;
            mSpecialLegendMarginFactor = 0.5;
            if(mr==Util::MASS_2)
                mEnableLegend = true;
            else 
                mEnableLegend = false;
            drawFinalResult( referenceHist, opts, Systematics_2PiCosTheta );
            mEnableLegend = false;
            mSpecialLegendMarginFactor = prevVal;
            mSpecialLabelRatio = false;
            mSpecialLabelOffsetFactor = 1.0;
            
        }
    }
    
    

    
    for(int rf=0; rf<Util::nReferenceFrames; ++rf){
        TH1F *referenceHist = new TH1F( *oo2PiPhi[rf]->hQ_Weighted[Util::CH_SUM_0] );
        TString oldName = referenceHist->GetName();
        referenceHist->SetName( Form("tmp%s", oldName.Data()) );
        TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
        for(int i=0; i<Util::nSystChecks; ++i)
        systCheckHists[i] = new TH1F( *oo2PiPhi[rf]->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
        
        TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo2PiPhi[rf]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
        TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo2PiPhi[rf]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo2PiPhi[rf]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
        TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
        bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

        
        SystematicsOutput Systematics_2PiPhi = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
        
    //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsPhiPidWeighted[OPPO][pid], pid );

        transformToCrossSection(referenceHist);
        transformToCrossSection(bkgdHistogram);
        mUtil->subtractBackground( referenceHist, bkgdHistogram );
        
    //     referenceHist->Divide( pidEff );
        
        
        std::vector< TH1F* > histMC;
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
            if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
            histMC.push_back( new TH1F( *tmpHistogram ) );
            histMC[fileId]->SetName(Form("tmp%d",fileId));
            if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
            histMC[fileId]->SetDirectory(nullptr);
            histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
            histMC[fileId]->SetLineWidth(2 );
        }
        
        //drawing final result
        DrawingOptions opts;
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.12;
        opts.mRightMargin = 0.035;
        opts.mTopMargin = 0.02;
        opts.mBottomMargin = 0.13;
        
        //x axis
        opts.mXmin = -180;
        opts.mXmax = 180;
        opts.mXaxisTitle = TString("#phi^{"+mUtil->referenceFrameShortName(rf)+"}("+singleTrkStr[Util::PLUS]+") [deg]");
        opts.mXaxisTitleSize = 0.05;
        opts.mXaxisTitleOffset = 1.04;
        opts.mXaxisLabelSize = 0.05;
        opts.mXaxisLabelOffset = 0.007;
        opts.mXaxisTickLength = 0.015;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 2.0 * referenceHist->GetMaximum();
        bool changeMubToNb = false;
        if( opts.mYmax < 1.0 )
        changeMubToNb = true;
        opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/d#phi^{"+mUtil->referenceFrameShortName(rf)+"}("+singleTrkStr[Util::PLUS]+") [nb/deg]" : "d#sigma/d#phi^{"+mUtil->referenceFrameShortName(rf)+"}("+singleTrkStr[Util::PLUS]+") [#mub/deg]");
        opts.mYaxisTitleSize = 0.05;
        opts.mYaxisTitleOffset = 1.21;
        opts.mYaxisLabelSize = 0.05;
        opts.mYaxisLabelOffset = 0.007;
        opts.mYaxisTickLength = 0.015;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.2;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString("FinalResult_Phi_"+mUtil->referenceFrameShortName(rf)+"_2Pi");
        
        for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
            if(histMC[fileId]){
                opts.mHistTH1F.push_back( histMC[fileId] );
                opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                
                opts.mObjForLegend.push_back( histMC[fileId] );
                opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                opts.mDrawTypeForLegend.push_back( "l" );
            }
        }

        
        opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
        opts.mSystErrorBoxColor = 16;
        opts.mSystErrorTotalBoxColor = 14;
        opts.mSystErrorBoxOpacity = 0.6;
        
        opts.mNColumns = 2;
        opts.mLegendX = 0.43;
        opts.mLegendXwidth = 0.45;
        opts.mLegendY = 0.61;
        opts.mLegendYwidth = 0.14;
        opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        
        opts.mXpositionSystErrorBox.push_back(0.05);
        opts.mXpositionSystErrorBox.push_back(0.65);
        
        TLatex lReaction(.4, .91, "#kern[-0.29]{#font[72]{ATLAS} Internal     #it{p}+#it{p}#rightarrow#it{p'}+"+pairStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
    //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
    //     opts.mTextToDraw.push_back( lATLAS );
        
        TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
        TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}^{+},p_{T}^{-}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
        TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
        TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
        TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
        TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
        TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
        TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
        TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
        TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        
        if(changeMubToNb){
            opts.mScalingFactor = 1e3;
            opts.mScale = kTRUE;
        }
        
        drawFinalResult( referenceHist, opts, Systematics_2PiPhi );
        
    }
    
    
    
    
    
    for(int mr=0; mr<Util::nMassRanges; ++mr){    
        for(int rf=0; rf<Util::nReferenceFrames; ++rf){
            TH1F *referenceHist = new TH1F( *oo2PiPhi_MassBins[rf][mr]->hQ_Weighted[Util::CH_SUM_0] );
            TString oldName = referenceHist->GetName();
            referenceHist->SetName( Form("tmp%s", oldName.Data()) );
            TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
            for(int i=0; i<Util::nSystChecks; ++i)
            systCheckHists[i] = new TH1F( *oo2PiPhi_MassBins[rf][mr]->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
            
            TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo2PiPhi_MassBins[rf][mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
            TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo2PiPhi_MassBins[rf][mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo2PiPhi_MassBins[rf][mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
            TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
            bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

            
            SystematicsOutput Systematics_2PiPhi = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
            
        //     TH1F* pidEff = getPtMissEfficiency( referenceHist, mExclusiveEventSelector->mhPminVsPmaxVsPhiPidWeighted[OPPO][pid], pid );

            transformToCrossSection(referenceHist);
            transformToCrossSection(bkgdHistogram);
            mUtil->subtractBackground( referenceHist, bkgdHistogram );
            
        //     referenceHist->Divide( pidEff );
            
            
            std::vector< TH1F* > histMC;
            for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
                TH1F *tmpHistogram = dynamic_cast<TH1F*>( filesMC[pid][fileId]->Get( oldName ) );
                if(!tmpHistogram){ histMC.push_back( nullptr ); continue; }
                histMC.push_back( new TH1F( *tmpHistogram ) );
                histMC[fileId]->SetName(Form("tmp%d",fileId));
                if(histMC[fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
                histMC[fileId]->SetDirectory(nullptr);
                histMC[fileId]->SetLineColor(colorsMC[pid][fileId]);
                histMC[fileId]->SetLineWidth(2 );
            }
            for(unsigned int fileId=0; fileId<filesMC_angularStudies[pid].size(); ++fileId){
                TString extraStr(fileId==0 ? "_S0m" : (fileId==1 ? "_D0m" : "") );
                TString oldName_2 = oldName;
                oldName_2.ReplaceAll("_ChSum0_Weighted", extraStr+"_ChSum0_Weighted");
                TH1F* hist = dynamic_cast<TH1F*>( filesMC_angularStudies[pid][fileId]->Get( oldName_2 ) );
                if(!hist) continue;
                TH1F *histCopy = new TH1F( *hist );
                histCopy->Scale( referenceHist->Integral(0, -1, "width") / histCopy->Integral(0, -1, "width") );
                histMC.push_back( histCopy );
                histMC[filesMC[pid].size()+fileId]->SetName(Form("tmp%d", static_cast<int>(filesMC[pid].size()+fileId)));
                if(histMC[filesMC[pid].size()+fileId]->GetEntries()==0){ histMC[fileId] = nullptr; continue; }
                histMC[filesMC[pid].size()+fileId]->SetDirectory(nullptr);
                histMC[filesMC[pid].size()+fileId]->SetLineColor(colorsMC_angularStudies[pid][fileId]);
                histMC[filesMC[pid].size()+fileId]->SetLineWidth(2);
            }
            
            //drawing final result
            DrawingOptions opts;
            
            opts.mCanvasWidth = 900;
            opts.mCanvasHeight = 800;
            
            opts.mLeftMargin = 0.13*1.7;
            opts.mRightMargin = 0.035*1.5;
            opts.mTopMargin = 0.03;
            opts.mBottomMargin = 0.13*1.7;
            
            //x axis
            opts.mXmin = -180;
            opts.mXmax = 180;
            opts.mXaxisTitle = TString("#phi^{"+mUtil->referenceFrameShortName(rf)+"}("+singleTrkStr[Util::PLUS]+") [deg]");
            opts.mXaxisTitleSize = 0.05*1.7;
            opts.mXaxisTitleOffset = 1.0;
            opts.mXaxisLabelSize = 0.05*1.7;
            opts.mXaxisLabelOffset = 0.005;
            opts.mXaxisTickLength = 0.015;
            
            //y axis
            opts.mYmin = 0;
            opts.mYmax = (mr==Util::MASS_2 ? 2.0 : 1.55)  * referenceHist->GetMaximum();
            bool changeMubToNb = false;
            if( opts.mYmax < 1.0 )
            changeMubToNb = true;
            opts.mYaxisTitle = TString( changeMubToNb ? "d#sigma/d#phi^{"+mUtil->referenceFrameShortName(rf)+"}("+singleTrkStr[Util::PLUS]+") [nb/deg]" : "d#sigma/d#phi^{"+mUtil->referenceFrameShortName(rf)+"}("+singleTrkStr[Util::PLUS]+") [#mub/deg]");
            opts.mYaxisTitleSize = 0.05*1.7;
            opts.mYaxisTitleOffset = 1.3;
            opts.mYaxisLabelSize = 0.05*1.7;
            opts.mYaxisLabelOffset = 0.005;
            opts.mYaxisTickLength = 0.015;
            
            
            opts.mSetDivisionsX = kTRUE;
            opts.mForceDivisionsX = kTRUE;
            opts.mXnDivisionA = 4;
            opts.mXnDivisionB = 2;
            opts.mXnDivisionC = 1;
            
            opts.mSetDivisionsY = kTRUE;
            opts.mYnDivisionA = 5;
            opts.mYnDivisionB = 5;
            opts.mYnDivisionC = 1;
            
            opts.mMarkerStyle = 20;
            opts.mMarkerColor = kBlack;
            opts.mMarkerSize = 1.6;
            
            opts.mLineStyle = 1;
            opts.mLineColor = kBlack;
            opts.mLineWidth = 2;
            
            opts.mPdfName = TString("FinalResult_Phi_"+mUtil->referenceFrameShortName(rf)+Form("_2Pi_MassBin%d", mr));
            
            for(unsigned int fileId=0; fileId<filesMC[pid].size(); ++fileId){
                if(histMC[fileId]){
                    opts.mHistTH1F.push_back( histMC[fileId] );
                    opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                    
                    opts.mObjForLegend.push_back( histMC[fileId] );
                    opts.mDesriptionForLegend.push_back(  legendStrMC[pid][fileId] );
                    opts.mDrawTypeForLegend.push_back( "l" );
                }
            }
            for(unsigned int fileId=0; fileId<filesMC_angularStudies[pid].size(); ++fileId){
                if(histMC[filesMC[pid].size()+fileId]){
                    opts.mHistTH1F.push_back( histMC[filesMC[pid].size()+fileId] );
                    opts.mHistTH1F_DrawingOpt.push_back( "SAME HIST ][" );
                    
                    opts.mObjForLegend.push_back( histMC[filesMC[pid].size()+fileId] );
                    opts.mDesriptionForLegend.push_back(  legendStrMC_angularStudies[pid][fileId] );
                    opts.mDrawTypeForLegend.push_back( "l" );
                }
            }

            
            opts.mXwidthSystErrorBox = referenceHist->GetBinWidth(2);
            opts.mSystErrorBoxColor = 16;
            opts.mSystErrorTotalBoxColor = 14;
            opts.mSystErrorBoxOpacity = 0.6;
            
//             opts.mNColumns = 2;
//             opts.mLegendX = 0.43;
//             opts.mLegendXwidth = 0.45;
//             opts.mLegendY = 0.61;
//             opts.mLegendYwidth = 0.14;
//             opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
            
            opts.mNColumns = 2;
            opts.mLegendX = /*pid==Util::PROTON ?*/ 0.24 /*: 0.39*/;
            opts.mLegendXwidth = 0.55;
            opts.mLegendY = /*pid==Util::PROTON ?*/ 0.64 /*: 0.17*/;
            opts.mLegendYwidth = 0.13;
            opts.mLegendTextSize = 0.6 * 0.7 * opts.mXaxisTitleSize;
            
            opts.mXpositionSystErrorBox.push_back(0.05);
            opts.mXpositionSystErrorBox.push_back(0.65);
            
            TLatex lReaction(.3, .9, "#kern[-0.29]{#font[72]{ATLAS} Internal     #it{p}+#it{p}#rightarrow#it{p'}+"+pairStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.65 * opts.mXaxisTitleSize);
            opts.mTextToDraw.push_back( lReaction );
        //     TLatex lATLAS(0.15, 0.9, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
        //     opts.mTextToDraw.push_back( lATLAS );
            
//             TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
//             TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}^{+},p_{T}^{-}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
//             TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
//             TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
//             TLatex lCutsA(.16,.84, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
//             TLatex lCutsB(.52,.84, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
//             TLatex lCuts1(.26,.84, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
//             TLatex lCuts12(.26,.795, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
//             TLatex lCuts11(.26,.75, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
//             TLatex lCuts3(.62,.84, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
            
            TString massBinStr;
            TString massLimitsStr[2];
            massLimitsStr[Util::MIN].Form(mr==Util::MASS_2 ? "%.0f" : "%.1f", mr==Util::MASS_2 ? 1.0 : 1.5 );
            massLimitsStr[Util::MAX].Form(mr==Util::MASS_1 ? "%.0f" : "%.1f", mr==Util::MASS_1 ? 1.0 : 1.5 );
            if(mr==Util::MASS_1){
            massBinStr = "m("+pairStr+") < " + massLimitsStr[Util::MAX]+" GeV";
            } else
            if(mr==Util::MASS_2){
            massBinStr = massLimitsStr[Util::MIN]+" GeV < m("+pairStr+") < " + massLimitsStr[Util::MAX]+" GeV";
            } else
            if(mr==Util::MASS_3){
            massBinStr = "m("+pairStr+") > " + massLimitsStr[Util::MIN]+" GeV";
            }
            TLatex lCuts6(.33 + (mr==Util::MASS_2 ? 0.0 : 0.1), .82 , massBinStr); lCuts6.SetNDC(); lCuts6.SetTextFont(42);  lCuts6.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts6 );
            
            if(changeMubToNb){
                opts.mScalingFactor = 1e3;
                opts.mScale = kTRUE;
            }
            
            mSpecialLabelOffsetFactor = 0.95;
            mSpecialLabelRatio = true;
            double prevVal = mSpecialLegendMarginFactor;
            mSpecialLegendMarginFactor = 0.5;
            if(mr==Util::MASS_2)
                mEnableLegend = true;
            else 
                mEnableLegend = false;
            drawFinalResult( referenceHist, opts, Systematics_2PiPhi );
            mEnableLegend = false;
            mSpecialLegendMarginFactor = prevVal;
            mSpecialLabelRatio = false;
            mSpecialLabelOffsetFactor = 1.0;
            
        }
    }
    
    
    
    
    
    
    
    } // end skipCrossSections
    
    
    
    
    
    
    
    //----------------------- B slope extraction -----------------------
    
    {
    
        enum EXPERIMENT {STAR, ATLAS, nExperiments};
        
        TH1F * hSlopeVsDeltaPhi[nExperiments][Util::nMassRanges];
        
        double slopeVal[nExperiments][4][Util::nMassRanges];
        double slopeErr[nExperiments][4][Util::nMassRanges];
        double slopeSystErrUp[nExperiments][4][Util::nMassRanges];
        double slopeSystErrDown[nExperiments][4][Util::nMassRanges];
        
        TH1F *hSlope_Val[2];
        TH1F *hSlope_SystUp[2];
        TH1F *hSlope_SystDown[2];

        TFile *fSlopes = new TFile("Slopes.root", "READ");
        for(int i=0; i<2/*Util::nDeltaPhiRanges*/; ++i){
            hSlope_Val[i] = dynamic_cast<TH1F*>( fSlopes->Get( Form("Slope_Val_%d", i) ) );
            hSlope_Val[i]->SetDirectory(0);
            hSlope_SystUp[i] = dynamic_cast<TH1F*>( fSlopes->Get( Form("Slope_SystUp_%d", i) ) );
            hSlope_SystUp[i]->SetDirectory(0);
            hSlope_SystDown[i] = dynamic_cast<TH1F*>( fSlopes->Get( Form("Slope_SystDown_%d", i) ) );
            hSlope_SystDown[i]->SetDirectory(0);
            
            for(int j=0; j<3; ++j){
                slopeVal[STAR][i==0 ? 0 : 3][j] = hSlope_Val[i]->GetBinContent(j+1);
                slopeErr[STAR][i==0 ? 0 : 3][j] = hSlope_Val[i]->GetBinError(j+1);
                slopeSystErrUp[STAR][i==0 ? 0 : 3][j] = hSlope_SystUp[i]->GetBinContent(j+1);
                slopeSystErrDown[STAR][i==0 ? 0 : 3][j] = fabs( hSlope_SystDown[i]->GetBinContent(j+1) );
            }
        }
        fSlopes->Close();

        
        vector<double> binningT_2D_Vec;
        binningT_2D_Vec.push_back(0.05);
        binningT_2D_Vec.push_back(0.08);
        binningT_2D_Vec.push_back(0.13);
        binningT_2D_Vec.push_back(0.18);
        binningT_2D_Vec.push_back(0.25);
        
        TCanvas *canv = new TCanvas("canv", "canv", 2*800, 800);
        canv->SetLeftMargin(0);
        canv->SetRightMargin(0);
        canv->SetTopMargin(0);
        canv->SetBottomMargin(0);
        canv->Divide(2,1);
        canv->Print("PDF/SlopeExtraction.pdf[");
        
        for(int mr=0; mr<Util::nMassRanges; ++mr){    
            for(int deltaPhiBin=0; deltaPhiBin<4; ++deltaPhiBin){
                
                
                fExponent2D = new TF2( "fExponent2D", "[0] * [1] * exp( -[1] * ( x + y ) )", 0.05, (mr==0 && deltaPhiBin==3) ? 0.13 : 0.25, 0.05, (mr==0 && deltaPhiBin==3) ? 0.13 : 0.25);
                TF1 *fExponent1D = new TF1("fExponent1D", exponent1D, 0.05, (mr==0 && deltaPhiBin==3) ? 0.13 : 0.25, 0);
                fExponent1D->SetLineColor(kGray);
                fExponent1D->SetLineStyle(2);
                maxMandT = (mr==0 && deltaPhiBin==3) ? 0.13 : 0.25;
                
                
                TH1F *referenceHist = new TH1F( *oo2PiMandelstamT_CVsA_ForSlopeExtraction_DeltaPhiBins_MassBins[0][deltaPhiBin][mr]->hQ_Weighted[Util::CH_SUM_0] );
                TString oldName = referenceHist->GetName();
                referenceHist->SetName( Form("tmp%s", oldName.Data()) );
                TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
                for(int i=0; i<Util::nSystChecks; ++i)
                systCheckHists[i] = new TH1F( *oo2PiMandelstamT_CVsA_ForSlopeExtraction_DeltaPhiBins_MassBins[0][deltaPhiBin][mr]->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
                
                TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo2PiMandelstamT_CVsA_ForSlopeExtraction_DeltaPhiBins_MassBins[0][deltaPhiBin][mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
                TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo2PiMandelstamT_CVsA_ForSlopeExtraction_DeltaPhiBins_MassBins[0][deltaPhiBin][mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo2PiMandelstamT_CVsA_ForSlopeExtraction_DeltaPhiBins_MassBins[0][deltaPhiBin][mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
                TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
                bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

                
                SystematicsOutput Systematics_2PiPhi = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
                
                transformToCrossSection(referenceHist);
                transformToCrossSection(bkgdHistogram);
                mUtil->subtractBackground( referenceHist, bkgdHistogram );
                
                //-----------------------
                
                canv->cd(1);
                gPad->SetTheta(20);
                gPad->SetPhi(240);
                
                fExponent2D->SetParameter(0, 100);
                fExponent2D->SetParError(0, 20);
                fExponent2D->SetParameter(1, 6);
                fExponent2D->SetParError(1, 0.5);
                fExponent2D->SetParLimits(1, 1, 40);
                fExponent2D->SetLineColor(kGray);
                
                TH2F *hT1T2 = getTH2F_from_TH1F_t1t2( referenceHist );
                
                hT1T2->SetMarkerStyle(20);
                hT1T2->SetMarkerColor(deltaPhiBin==0 ? kBlack : kRed);
                hT1T2->SetLineColor(deltaPhiBin==0 ? kBlack : kRed);
                hT1T2->SetLineWidth(2);
                hT1T2->SetMarkerSize(1.5);
                
                
                hT1T2->GetXaxis()->SetTitle("-t_{1} [GeV^{2}]");
                hT1T2->GetXaxis()->SetNdivisions(5,2,1);
                hT1T2->GetXaxis()->SetTitleOffset(1.7);
                hT1T2->GetYaxis()->SetTitle("-t_{2} [GeV^{2}]");
                hT1T2->GetYaxis()->SetNdivisions(5,2,1);
                hT1T2->Draw( "PE"  );
                hT1T2->GetZaxis()->SetRangeUser(0, 1.3*hT1T2->GetMaximum() );
                hT1T2->GetZaxis()->SetTitle("d^{2}#sigma/dt_{1}dt_{2} [#mub/GeV^{4}]");
                hT1T2->GetZaxis()->SetTitleOffset(1.62);
                
                TFitResultPtr res = hT1T2->Fit( fExponent2D, "IRSQ", ""/*, 0.05, 0.25*/ );
                slopeVal[ATLAS][deltaPhiBin][mr] = res->Parameter(1);
                slopeErr[ATLAS][deltaPhiBin][mr] = res->ParError(1);
                
                hT1T2->Draw( "PE SAME"  );
                
                TLatex l;
                l.SetNDC();
                l.SetTextSize(0.05);
                TString chi2Str; chi2Str.Form("%.1f/%d", res->Chi2(), res->Ndf() );
                l.DrawLatex(0.65, 0.93, "#chi^{2}/ndf = "+chi2Str);
                
                TString bStr; bStr.Form("#beta = %.2f #pm %.2f GeV^{-2}", res->Parameter(1), res->ParError(1) );
                l.DrawLatex(0.01, 0.93, bStr);
                
                TString label;
                if(deltaPhiBin==0)
                    label = TString("#Delta#varphi < 45^{#circ}"); else
                if(deltaPhiBin==1)
                    label = TString("45^{#circ} < #Delta#varphi < 90^{#circ}"); else
                if(deltaPhiBin==2)
                    label = TString("90^{#circ} < #Delta#varphi < 135^{#circ}"); else
                    label = TString("#Delta#varphi > 135^{#circ}");
                
                l.DrawLatex(0.03, 0.11, label);
                l.DrawLatex(0.03, 0.04, TString(mr==0?"0.6 GeV < m < 1 GeV": (mr==1 ? "1 GeV < m < 1.5 GeV" : "m > 1.5 GeV") ));
                
                if( deltaPhiBin==3 && mr==2 )
                    canv->cd(1)->Print("slopePhd.pdf", "EmbedFonts");
                
                canv->cd(2);
                
                
                
                TH1D *projY = new TH1D(Form("py%d%d", deltaPhiBin, mr), "tempname", binningT_2D_Vec.size()-1, &binningT_2D_Vec[0]);
                for(int biny=1; biny<=projY->GetNbinsX(); ++biny){
                    double integral, error;
                    if(mr==0 && deltaPhiBin==3)
                        integral = hT1T2->IntegralAndError( 0, hT1T2->GetYaxis()->FindBin(0.1299), biny, biny, error, "width" );
                    else
                        integral = hT1T2->IntegralAndError( 0, -1, biny, biny, error, "width" );
                    projY->SetBinContent(biny, integral);
                    projY->SetBinError(biny, error);
//                     cout << "Y :   " <<  biny << " " << integral << endl;
                }
                projY->Scale(1., "width");
                
                TH1D *projX = new TH1D(Form("px%d%d", deltaPhiBin, mr), "tempname", binningT_2D_Vec.size()-1, &binningT_2D_Vec[0]);
                for(int binx=1; binx<=projX->GetNbinsX(); ++binx){
                    double integral, error;
                    if(mr==0 && deltaPhiBin==3)
                        integral = hT1T2->IntegralAndError( binx, binx, 0, hT1T2->GetYaxis()->FindBin(0.1299), error, "width" );
                    else
                        integral = hT1T2->IntegralAndError( binx, binx, 0, -1, error, "width" );
                    projX->SetBinContent(binx, integral);
                    projX->SetBinError(binx, error);
//                     cout << "X :   " <<  binx << " " << integral << endl;
                }
                projX->Scale(1., "width");
                
                projX->GetXaxis()->SetTitle("-t [GeV^{2}]");
                projX->GetYaxis()->SetRangeUser(0, 1.25*max( projX->GetBinContent(projX->GetMaximumBin()), projY->GetBinContent(projY->GetMaximumBin()) ));
                projX->SetMarkerSize(2.0);
                projX->SetMarkerColor(deltaPhiBin==0 ? kBlack : kRed);
                projX->SetLineColor(deltaPhiBin==0 ? kBlack : kRed);
                projX->SetLineWidth(2);
                projX->Draw("PE");
                
                projY->SetMarkerColor(deltaPhiBin==0 ? kBlack : kRed);
                projY->SetLineColor(deltaPhiBin==0 ? kBlack : kRed);
                projY->SetLineWidth(2);
                projY->SetMarkerSize(2.0);
                projY->SetMarkerStyle(25);
                projY->Draw("PE SAME");
                fExponent1D->Draw("SAME");
                
                TLegend *leg = new TLegend(0.22, 0.25, 0.4, 0.4);
                leg->SetBorderSize(0);
                leg->SetFillStyle(0);
                leg->AddEntry( projX, "1", "pel" );
                leg->AddEntry( projY, "2", "pel" );
                leg->Draw();
                
                TLatex lSTAR(0.45, 0.85, "ATLAS #font[52]{Internal}"); lSTAR.SetNDC(); lSTAR.SetTextFont(72);  lSTAR.SetTextSize(.06); lSTAR.Draw();
                
                
                
                canv->Print("PDF/SlopeExtraction.pdf");
                
                // ------------ SYSTEMATCS --------------
                
                double deltaSqPlus = 0;
                double deltaSqMinus = 0;
                
                TCanvas *canv2 = new TCanvas("canv2", "canv2", 800, 800);
                canv2->SetTheta(20);
                canv2->SetPhi(240);
                
                canv2->Print(Form("PDF/SlopeExtraction_Systematics_Bin%d%d.pdf[", deltaPhiBin, mr));
                for(int syst=0; syst<(Util::nSystChecks+nFilesWithAcceptanceCorr-1); ++syst){
                    
                    TH2F *hT1T2Syst = new TH2F( *hT1T2 );
                    hT1T2Syst->SetName( Form("hT1T2Syst_%s", hT1T2->GetName()) );
                    
                    TH1F *hRatio1D;
                    
                    if(syst<Util::nSystChecks)
                        hRatio1D = new TH1F( *Systematics_2PiPhi.mSystCheckHistVec[syst] );
                    else
                        hRatio1D = new TH1F( *oo2PiMandelstamT_CVsA_ForSlopeExtraction_DeltaPhiBins_MassBins[syst - Util::nSystChecks + 1][deltaPhiBin][mr]->hQ_Weighted[Util::CH_SUM_0] );
                    
                    hRatio1D->Divide( Systematics_2PiPhi.mSystCheckHistVec[Util::nSystChecks] );
                    
                    TH2F *hRatio = getTH2F_from_TH1F_t1t2( hRatio1D, false );
                    
                    for(int bx=1; bx<=hT1T2->GetNbinsX(); ++bx){
                        for(int by=1; by<=hT1T2->GetNbinsY(); ++by){
                            hT1T2Syst->SetBinContent( bx, by, hT1T2->GetBinContent(bx, by) * hRatio->GetBinContent(bx, by) );
                            hT1T2Syst->SetBinError( bx, by, hT1T2->GetBinError(bx, by) * hRatio->GetBinContent(bx, by) );
                        }
                    }
                    
                    hT1T2Syst->SetMarkerStyle(20);
                    hT1T2Syst->SetMarkerColor(deltaPhiBin==0 ? kBlack : kRed);
                    hT1T2Syst->SetLineColor(deltaPhiBin==0 ? kBlack : kRed);
                    hT1T2Syst->SetMarkerSize(1.5);
                    hT1T2Syst->SetLineWidth(2);
                    
                    hT1T2Syst->GetXaxis()->SetTitle("t_{1} [GeV^{2}]");
                    hT1T2Syst->GetYaxis()->SetTitle("t_{2} [GeV^{2}]");
                    hT1T2Syst->Draw( "PE"  );
                    hT1T2Syst->GetZaxis()->SetRangeUser(0, 1.3*hT1T2Syst->GetMaximum() );
                    
                    TFitResultPtr resSyst = hT1T2Syst->Fit( fExponent2D, "IRSQ", ""/*, 0.05, 0.25*/ );
                    
                    double delta = resSyst->Parameter(1) - res->Parameter(1);
                    if( delta>0 )
                    deltaSqPlus += delta*delta;
                    else
                    deltaSqMinus += delta*delta;
                    
                    TString chi2StrSyst; chi2StrSyst.Form("%.1f/%d", resSyst->Chi2(), resSyst->Ndf() );
                    l.DrawLatex(0.03, 0.93, "#chi^{2}/ndf = "+chi2StrSyst);
                    
                    TString bStrSyst; bStrSyst.Form("#beta = %.2f #pm %.2f", resSyst->Parameter(1), resSyst->ParError(1) );
                    l.DrawLatex(0.65, 0.93, bStrSyst);
                    
                    l.DrawLatex(0.03, 0.11, label);
                    l.DrawLatex(0.03, 0.04, TString(mr==0?"0.6 GeV < m < 1 GeV": (mr==1 ? "1 GeV < m < 1.5 GeV" : "m > 1.5 GeV") ));
                    
                    l.DrawLatex(0.8, 0.05, Form("Syst. check #%d", syst+1) );
                    
                    
                    canv2->Print(Form("PDF/SlopeExtraction_Systematics_Bin%d%d.pdf", deltaPhiBin, mr));
                }
                canv2->Print(Form("PDF/SlopeExtraction_Systematics_Bin%d%d.pdf]", deltaPhiBin, mr));
                delete canv2;
                
                
                slopeSystErrUp[ATLAS][deltaPhiBin][mr] = sqrt( deltaSqPlus );
                slopeSystErrDown[ATLAS][deltaPhiBin][mr] = sqrt( deltaSqMinus );
                
            }
        }
        canv->Print("PDF/SlopeExtraction.pdf]");
        
        
        
        
        
        
        for(int i=0; i<nExperiments; ++i){
            for(int mr=0; mr<Util::nMassRanges; ++mr){
                hSlopeVsDeltaPhi[i][mr] = new TH1F( Form("hSlopeVsDeltaPhi_%d%d", i, mr), "", 4, 0, 180 );
                
                hSlopeVsDeltaPhi[i][mr]->SetLineColor( mr==0 ? 209 : (mr==1 ? 95 : 64) );
                hSlopeVsDeltaPhi[i][mr]->SetMarkerColor( mr==0 ? 209 : (mr==1 ? 95 : 64) );
                hSlopeVsDeltaPhi[i][mr]->SetMarkerSize( i==STAR ? 3.0 : 1.2 );
                hSlopeVsDeltaPhi[i][mr]->SetMarkerStyle( i==STAR ? 29 : 21 );
                hSlopeVsDeltaPhi[i][mr]->SetBarOffset( (i==STAR ? -0.05 : 0.05) + (mr-1)*0.2 );
                
                for(int j=0; j<4; ++j){
                    hSlopeVsDeltaPhi[i][mr]->SetBinContent( j+1, slopeVal[i][j][mr] );
                    hSlopeVsDeltaPhi[i][mr]->SetBinError( j+1, slopeErr[i][j][mr] );
                }
            }
        }
        
        

        gStyle->SetErrorX(0.0);
        gStyle->SetGridColor(kGray+1);
        
        TLatex lATLAS; lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.075);
        
        TCanvas *canvasSlopes = new TCanvas("canvasSlopes", "canvasSlopes", 800, 600);
        
        double leftMargin_canvasSlopes = 0.18;
        double rightMargin_canvasSlopes = 0.04;
        double topMargin_canvasSlopes = 0.03;
        double bottomMargin_canvasSlopes = 0.19;
        //   TGaxis::SetMaxDigits(3);
        canvasSlopes->SetFillStyle(4000);
        canvasSlopes->SetFillColor(0);
        canvasSlopes->SetFrameFillStyle(4000);
        canvasSlopes->SetLeftMargin(leftMargin_canvasSlopes);
        canvasSlopes->SetRightMargin(rightMargin_canvasSlopes);
        canvasSlopes->SetTopMargin(topMargin_canvasSlopes);
        canvasSlopes->SetBottomMargin(bottomMargin_canvasSlopes);
        canvasSlopes->SetGridx();
        
        TH1F *hist = nullptr;
        TString fileName;
        double axisMin;
        double axisMax;
        TString xAxisLabel;
        TString yAxisLabel;
        
        hist = hSlopeVsDeltaPhi[STAR][0];
        fileName = "SlopesFinal.pdf";
        axisMin = hist->GetXaxis()->GetXmin();
        axisMax = hist->GetXaxis()->GetXmax();
        yAxisLabel = TString("Exponential slope #beta [GeV^{-2}]     ");
        xAxisLabel = TString("#Delta#varphi [deg]");
        
        
        hist->GetXaxis()->SetLabelSize(1.5*hist->GetXaxis()->GetLabelSize());
        hist->GetXaxis()->SetTitleSize(1.7*hist->GetXaxis()->GetTitleSize());
        hist->GetYaxis()->SetLabelSize(1.5*hist->GetYaxis()->GetLabelSize());
        hist->GetYaxis()->SetTitleSize(1.7*hist->GetYaxis()->GetTitleSize());
        hist->GetZaxis()->SetLabelSize(1.5*hist->GetZaxis()->GetLabelSize());
        hist->GetZaxis()->SetTitleSize(1.7*hist->GetZaxis()->GetTitleSize());
        
        hist->GetYaxis()->SetTitle( yAxisLabel );
        hist->GetYaxis()->CenterTitle();
        hist->GetXaxis()->SetTitleOffset(1.03);
        hist->GetXaxis()->SetTitle( xAxisLabel );
        hist->GetXaxis()->CenterTitle();
        hist->GetYaxis()->SetTitleOffset(1.05);
        
        hist->GetXaxis()->SetRangeUser(axisMin, axisMax);
        hist->GetYaxis()->SetRangeUser(2, 15);
        hist->GetXaxis()->SetNdivisions(4,1,1, kFALSE);
    //     hist->GetYaxis()->SetNdivisions(4,1,1, kFALSE);
        
        hist->Draw("PE");
        
        
        for(int i=0; i<nExperiments; ++i){
            for(int mr=0; mr<Util::nMassRanges; ++mr){
                
                const double systErrorBoxWidth = 4.0;
                
                TBox *b;
                for(int d=1; d<=4; ++d){
                    
                    if( slopeSystErrUp[i][d-1][mr]>0 && slopeSystErrDown[i][d-1][mr]>0 && hSlopeVsDeltaPhi[i][mr]->GetBinContent(d)>0 ){
                        
                        b = new TBox( hSlopeVsDeltaPhi[i][mr]->GetBinCenter(d)-systErrorBoxWidth/2+hSlopeVsDeltaPhi[i][mr]->GetBinWidth(d)*((i==STAR ? -0.05 : 0.05) + (mr-1)*0.2), 
                                    hSlopeVsDeltaPhi[i][mr]->GetBinContent(d)-slopeSystErrDown[i][d-1][mr], 
                                    hSlopeVsDeltaPhi[i][mr]->GetBinCenter(d)+systErrorBoxWidth/2+hSlopeVsDeltaPhi[i][mr]->GetBinWidth(d)*((i==STAR ? -0.05 : 0.05) + (mr-1)*0.2), 
                                    hSlopeVsDeltaPhi[i][mr]->GetBinContent(d)+slopeSystErrUp[i][d-1][mr] );
                        b->SetFillColor( mr==0 ? 209 : (mr==1 ? 95 : 64) );
                        b->SetLineColor( mr==0 ? 209 : (mr==1 ? 95 : 64) );
                        b->SetFillStyle(3004);
                //         b->SetLineWidth(0);
                        b->Draw("l");
                        
                    }
                }
            }
        }
        
        
        for(int i=0; i<nExperiments; ++i){
            for(int mr=0; mr<Util::nMassRanges; ++mr){
                hSlopeVsDeltaPhi[i][mr]->Draw("PE SAME");
            }
        }
        
        lATLAS.DrawLatex(0.25, 0.87, "#scale[0.8]{#font[52]{p + p #rightarrow p' + #pi^{+}#pi^{-} + p'}}");
        lATLAS.DrawLatex(0.25, 0.77, "#scale[0.8]{ATLAS #font[52]{Internal}}");

        const double legVerticalOffset = 0.09;
        const int legendFillColor = kWhite/*kYellow-10*/;
        const double alpha = 0.6;
        
    //     TLegend *legend_markerStyle = new TLegend(0.27-0.05, 0.85, 0.64+0.05, 0.95);
        TLegend *legend_markerStyle = new TLegend(0.27-0.06, 0.85-0.64, 0.89+0.04, 0.95-0.64);
        legend_markerStyle->SetTextSize(0.03);
        legend_markerStyle->SetTextColor(kBlack);
    //     legend_markerStyle->SetFillStyle(3000);
        legend_markerStyle->SetFillColorAlpha(legendFillColor, alpha);
        legend_markerStyle->SetBorderSize(0);
        legend_markerStyle->SetMargin(0.2);
        legend_markerStyle->SetNColumns(3);
        
        TH1F *hSTAR = new TH1F();
        hSTAR->SetMarkerStyle(29);
        hSTAR->SetMarkerSize(2.0);
        TH1F *hATLAS = new TH1F();
        hATLAS->SetMarkerStyle(21);
        
        legend_markerStyle->AddEntry(hSTAR, "#sqrt{s} = 200 GeV", "p");
        legend_markerStyle->AddEntry(hATLAS, "#sqrt{s} = 13 TeV", "p");
        legend_markerStyle->AddEntry(new TH1F(), "", "");
        
        TH1F *hMass1 = new TH1F();
        hMass1->SetLineWidth(5);
        hMass1->SetLineColor(209);
        TH1F *hMass2 = new TH1F();
        hMass2->SetLineWidth(5);
        hMass2->SetLineColor(95);
        TH1F *hMass3 = new TH1F();
        hMass3->SetLineWidth(5);
        hMass3->SetLineColor(64);
        
        legend_markerStyle->AddEntry(hMass1, "m #in [0.6 GeV, 1.0 GeV]", "l");
        legend_markerStyle->AddEntry(hMass2, "m #in (1.0 GeV, 1.5 GeV]", "l");
        legend_markerStyle->AddEntry(hMass3, "m > 1.5 GeV", "l");
        
        legend_markerStyle->Draw();
        
        canvasSlopes->Print( "PDF/"+fileName, "EmbedFonts");
        
        gStyle->SetErrorX(0.5);
        
    
    }
    
    
    
    
    
    // four pions slope
    
    
    {
        
        enum EXPERIMENT {ATLAS, nExperiments};
        
        TH1F * hSlopeVsDeltaPhi[nExperiments][Util::nMassRanges];
        
        double slopeVal[nExperiments][Util::nDeltaPhiRanges][Util::nMassRanges];
        double slopeErr[nExperiments][Util::nDeltaPhiRanges][Util::nMassRanges];
        double slopeSystErrUp[nExperiments][Util::nDeltaPhiRanges][Util::nMassRanges];
        double slopeSystErrDown[nExperiments][Util::nDeltaPhiRanges][Util::nMassRanges];
        
        fExponent2D = new TF2( "fExponent2D", "[0] * [1] * exp( -[1] * ( x + y ) )", 0.05, 0.25, 0.05, 0.25);
        TF1 *fExponent1D = new TF1("fExponent1D", exponent1D, 0.05, 0.25, 0);
        fExponent1D->SetLineColor(kGray);
        fExponent1D->SetLineStyle(2);
        maxMandT = 0.25;
        
        vector<double> binningT_2D_Vec;
        binningT_2D_Vec.push_back(0.05);
        binningT_2D_Vec.push_back(0.08);
        binningT_2D_Vec.push_back(0.13);
        binningT_2D_Vec.push_back(0.18);
        binningT_2D_Vec.push_back(0.25);
        
        TCanvas *canv = new TCanvas("canv", "canv", 2*800, 800);
        canv->SetLeftMargin(0);
        canv->SetRightMargin(0);
        canv->SetTopMargin(0);
        canv->SetBottomMargin(0);
        canv->Divide(2,1);
        canv->Print("PDF/SlopeExtraction_4Pi.pdf[");
        
        for(int mr=0; mr<Util::nMassRanges; ++mr){    
            for(int deltaPhiBin=0; deltaPhiBin<Util::nDeltaPhiRanges; ++deltaPhiBin){
                
                TH1F *referenceHist = new TH1F( *oo4PiMandelstamT_CVsA_ForSlopeExtraction_DeltaPhiBins_MassBins[0][deltaPhiBin][mr]->hQ_Weighted[Util::CH_SUM_0] );
                TString oldName = referenceHist->GetName();
                referenceHist->SetName( Form("tmp%s", oldName.Data()) );
                TH1F **systCheckHists = new TH1F* [Util::nSystChecks];
                for(int i=0; i<Util::nSystChecks; ++i)
                systCheckHists[i] = new TH1F( *oo4PiMandelstamT_CVsA_ForSlopeExtraction_DeltaPhiBins_MassBins[0][deltaPhiBin][mr]->hQ_Weighted_Syst[Util::CH_SUM_0][i] );
                
                TH1F *bkgdHistogram = (TH1F*)mUtil->bkgdHistogram( oo4PiMandelstamT_CVsA_ForSlopeExtraction_DeltaPhiBins_MassBins[0][deltaPhiBin][mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut );
                TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( oo4PiMandelstamT_CVsA_ForSlopeExtraction_DeltaPhiBins_MassBins[0][deltaPhiBin][mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_0], nSigmaMomentumBalanceCut, oo4PiMandelstamT_CVsA_ForSlopeExtraction_DeltaPhiBins_MassBins[0][deltaPhiBin][mr]->hNSigmaPtMissVsQ_Weighted[Util::CH_SUM_NOT_0]->ProjectionY() );
                TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
                bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

                
                SystematicsOutput Systematics_4PiPhi = calculateSystematicErrors( referenceHist, systCheckHists, bkgdDifferenceHistogram);
                
                transformToCrossSection(referenceHist);
                transformToCrossSection(bkgdHistogram);
                mUtil->subtractBackground( referenceHist, bkgdHistogram );
                
                //-----------------------
                
                canv->cd(1);
                gPad->SetTheta(20);
                gPad->SetPhi(240);
                
                fExponent2D->SetParameter(0, 100);
                fExponent2D->SetParError(0, 20);
                fExponent2D->SetParameter(1, 6);
                fExponent2D->SetParError(1, 0.5);
                fExponent2D->SetParLimits(1, 1, 40);
                fExponent2D->SetLineColor(kGray);
                
                TH2F *hT1T2 = getTH2F_from_TH1F_t1t2( referenceHist );
                
                hT1T2->SetMarkerStyle(20);
                hT1T2->SetMarkerColor(deltaPhiBin==0 ? kBlack : kRed);
                hT1T2->SetLineColor(deltaPhiBin==0 ? kBlack : kRed);
                hT1T2->SetLineWidth(2);
                hT1T2->SetMarkerSize(1.5);
                
                
                hT1T2->GetXaxis()->SetTitle("-t_{1} [GeV^{2}]");
                hT1T2->GetXaxis()->SetNdivisions(5,2,1);
                hT1T2->GetXaxis()->SetTitleOffset(1.7);
                hT1T2->GetYaxis()->SetTitle("-t_{2} [GeV^{2}]");
                hT1T2->GetYaxis()->SetNdivisions(5,2,1);
                hT1T2->Draw( "PE"  );
                hT1T2->GetZaxis()->SetRangeUser(0, 1.3*hT1T2->GetMaximum() );
                hT1T2->GetZaxis()->SetTitle("d^{2}#sigma/dt_{1}dt_{2} [#mub/GeV^{4}]");
                hT1T2->GetZaxis()->SetTitleOffset(1.7);
                
                TFitResultPtr res = hT1T2->Fit( fExponent2D, "IRSQ", ""/*, 0.05, 0.25*/ );
                slopeVal[ATLAS][deltaPhiBin][mr] = res->Parameter(1);
                slopeErr[ATLAS][deltaPhiBin][mr] = res->ParError(1);
                
                hT1T2->Draw( "PE SAME"  );
                
                TLatex l;
                l.SetNDC();
                l.SetTextSize(0.05);
                TString chi2Str; chi2Str.Form("%.1f/%d", res->Chi2(), res->Ndf() );
                l.DrawLatex(0.65, 0.93, "#chi^{2}/ndf = "+chi2Str);
                
                TString bStr; bStr.Form("#beta = %.2f #pm %.2f GeV^{-2}", res->Parameter(1), res->ParError(1) );
                l.DrawLatex(0.01, 0.93, bStr);
                
                TString label;
                if(deltaPhiBin==0)
                    label = TString("#Delta#varphi < 90^{#circ}");
                else
                    label = TString("#Delta#varphi > 90^{#circ}");
                
                l.DrawLatex(0.03, 0.11, label);
                l.DrawLatex(0.03, 0.04, TString(mr==0?"0.6 GeV < m < 1 GeV": (mr==1 ? "1 GeV < m < 1.5 GeV" : "m > 1.5 GeV") ));
                
                canv->cd(2);
                
                
                
                TH1D *projY = new TH1D(Form("py%d%d", deltaPhiBin, mr), "tempname", binningT_2D_Vec.size()-1, &binningT_2D_Vec[0]);
                for(int biny=1; biny<=projY->GetNbinsY(); ++biny){
                    double integral, error;
                    integral = hT1T2->IntegralAndError( 0, -1, biny, biny, error, "width" );
                    projY->SetBinContent(biny, integral);
                    projY->SetBinError(biny, error);
                }
                projY->Scale(1., "width");
                
                TH1D *projX = new TH1D(Form("px%d%d", deltaPhiBin, mr), "tempname", binningT_2D_Vec.size()-1, &binningT_2D_Vec[0]);
                for(int binx=1; binx<=projX->GetNbinsX(); ++binx){
                    double integral, error;
                    integral = hT1T2->IntegralAndError( binx, binx, 0, -1, error, "width" );
                    projX->SetBinContent(binx, integral);
                    projX->SetBinError(binx, error);
                }
                projX->Scale(1., "width");
                
                projX->GetXaxis()->SetTitle("-t [GeV^{2}]");
                projX->GetYaxis()->SetRangeUser(0, 1.25*max( projX->GetBinContent(projX->GetMaximumBin()), projY->GetBinContent(projY->GetMaximumBin()) ));
                projX->SetMarkerSize(2.0);
                projX->SetMarkerColor(deltaPhiBin==0 ? kBlack : kRed);
                projX->SetLineColor(deltaPhiBin==0 ? kBlack : kRed);
                projX->SetLineWidth(2);
                projX->Draw("PE");
                
                projY->SetMarkerColor(deltaPhiBin==0 ? kBlack : kRed);
                projY->SetLineColor(deltaPhiBin==0 ? kBlack : kRed);
                projY->SetLineWidth(2);
                projY->SetMarkerSize(2.0);
                projY->SetMarkerStyle(25);
                projY->Draw("PE SAME");
                fExponent1D->Draw("SAME");
                
                TLegend *leg = new TLegend(0.22, 0.25, 0.4, 0.4);
                leg->SetBorderSize(0);
                leg->SetFillStyle(0);
                leg->AddEntry( projX, "1", "pel" );
                leg->AddEntry( projY, "2", "pel" );
                leg->Draw();
                /*
                TLatex lSTAR(0.7, 0.85, "STAR"); lSTAR.SetNDC(); lSTAR.SetTextFont(72);  lSTAR.SetTextSize(.06); lSTAR.Draw();
                */
                canv->Print("PDF/SlopeExtraction_4Pi.pdf");
                
                // ------------ SYSTEMATCS --------------
                
                double deltaSqPlus = 0;
                double deltaSqMinus = 0;
                
                TCanvas *canv2 = new TCanvas("canv2", "canv2", 800, 800);
                canv2->SetTheta(20);
                canv2->SetPhi(240);
                
                canv2->Print(Form("PDF/SlopeExtraction_4Pi_Systematics_Bin%d%d.pdf[", deltaPhiBin, mr));
                for(int syst=0; syst<Util::nSystChecks; ++syst){
                    
                    TH2F *hT1T2Syst = new TH2F( *hT1T2 );
                    hT1T2Syst->SetName( Form("hT1T2Syst_%s", hT1T2->GetName()) );
                    
                    TH1F *hRatio1D = new TH1F( *Systematics_4PiPhi.mSystCheckHistVec[syst] );
                    hRatio1D->Divide( Systematics_4PiPhi.mSystCheckHistVec[Util::nSystChecks] );
                    
                    TH2F *hRatio = getTH2F_from_TH1F_t1t2( hRatio1D, false );
                    
                    for(int bx=1; bx<=hT1T2->GetNbinsX(); ++bx){
                    for(int by=1; by<=hT1T2->GetNbinsY(); ++by){
                        hT1T2Syst->SetBinContent( bx, by, hT1T2->GetBinContent(bx, by) * hRatio->GetBinContent(bx, by) );
                        hT1T2Syst->SetBinError( bx, by, hT1T2->GetBinError(bx, by) * hRatio->GetBinContent(bx, by) );
                    }
                    }
                    
                    hT1T2Syst->SetMarkerStyle(20);
                    hT1T2Syst->SetMarkerColor(deltaPhiBin==0 ? kBlack : kRed);
                    hT1T2Syst->SetLineColor(deltaPhiBin==0 ? kBlack : kRed);
                    hT1T2Syst->SetMarkerSize(1.5);
                    hT1T2Syst->SetLineWidth(2);
                    
                    hT1T2Syst->GetXaxis()->SetTitle("t_{1} [GeV^{2}]");
                    hT1T2Syst->GetYaxis()->SetTitle("t_{2} [GeV^{2}]");
                    hT1T2Syst->Draw( "PE"  );
                    hT1T2Syst->GetZaxis()->SetRangeUser(0, 1.3*hT1T2Syst->GetMaximum() );
                    
                    TFitResultPtr resSyst = hT1T2Syst->Fit( fExponent2D, "IRSQ", "", 0.05, 0.25 );
                    
                    double delta = resSyst->Parameter(1) - res->Parameter(1);
                    if( delta>0 )
                    deltaSqPlus += delta*delta;
                    else
                    deltaSqMinus += delta*delta;
                    
                    TString chi2StrSyst; chi2StrSyst.Form("%.1f/%d", resSyst->Chi2(), resSyst->Ndf() );
                    l.DrawLatex(0.03, 0.93, "#chi^{2}/ndf = "+chi2StrSyst);
                    
                    TString bStrSyst; bStrSyst.Form("#beta = %.2f #pm %.2f", resSyst->Parameter(1), resSyst->ParError(1) );
                    l.DrawLatex(0.65, 0.93, bStrSyst);
                    
                    l.DrawLatex(0.03, 0.11, label);
                    l.DrawLatex(0.03, 0.04, TString(mr==0?"0.6 GeV < m < 1 GeV": (mr==1 ? "1 GeV < m < 1.5 GeV" : "m > 1.5 GeV") ));
                    
                    l.DrawLatex(0.8, 0.05, Form("Syst. check #%d", syst+1) );
                    
                    
                    canv2->Print(Form("PDF/SlopeExtraction_4Pi_Systematics_Bin%d%d.pdf", deltaPhiBin, mr));
                }
                canv2->Print(Form("PDF/SlopeExtraction_4Pi_Systematics_Bin%d%d.pdf]", deltaPhiBin, mr));
                delete canv2;
                
                
                slopeSystErrUp[ATLAS][deltaPhiBin][mr] = sqrt( deltaSqPlus );
                slopeSystErrDown[ATLAS][deltaPhiBin][mr] = sqrt( deltaSqMinus );
                
            }
        }
        canv->Print("PDF/SlopeExtraction_4Pi.pdf]");
        
        
        
        
        
        
        for(int i=0; i<nExperiments; ++i){
            for(int mr=0; mr<Util::nMassRanges; ++mr){
                hSlopeVsDeltaPhi[i][mr] = new TH1F( Form("hSlopeVsDeltaPhi_4Pi_%d%d", i, mr), "", Util::nDeltaPhiRanges, 0, 180 );
                
                hSlopeVsDeltaPhi[i][mr]->SetLineColor( mr==0 ? 209 : (mr==1 ? 95 : 64) );
                hSlopeVsDeltaPhi[i][mr]->SetMarkerColor( mr==0 ? 209 : (mr==1 ? 95 : 64) );
                hSlopeVsDeltaPhi[i][mr]->SetMarkerSize(  1.2 );
                hSlopeVsDeltaPhi[i][mr]->SetMarkerStyle(  21 );
                hSlopeVsDeltaPhi[i][mr]->SetBarOffset(  (mr-1)*0.1 );
                
                for(int j=0; j<Util::nDeltaPhiRanges; ++j){
                    hSlopeVsDeltaPhi[i][mr]->SetBinContent( j+1, slopeVal[i][j][mr] );
                    hSlopeVsDeltaPhi[i][mr]->SetBinError( j+1, slopeErr[i][j][mr] );
                }
            }
        }
        
        

        gStyle->SetErrorX(0.0);
        gStyle->SetGridColor(kGray+1);
        
        TLatex lATLAS; lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.075);
        
        TCanvas *canvasSlopes = new TCanvas("canvasSlopes", "canvasSlopes", 800, 600);
        
        double leftMargin_canvasSlopes = 0.18;
        double rightMargin_canvasSlopes = 0.04;
        double topMargin_canvasSlopes = 0.03;
        double bottomMargin_canvasSlopes = 0.19;
        //   TGaxis::SetMaxDigits(3);
        canvasSlopes->SetFillStyle(4000);
        canvasSlopes->SetFillColor(0);
        canvasSlopes->SetFrameFillStyle(4000);
        canvasSlopes->SetLeftMargin(leftMargin_canvasSlopes);
        canvasSlopes->SetRightMargin(rightMargin_canvasSlopes);
        canvasSlopes->SetTopMargin(topMargin_canvasSlopes);
        canvasSlopes->SetBottomMargin(bottomMargin_canvasSlopes);
        canvasSlopes->SetGridx();
        
        TH1F *hist = nullptr;
        TString fileName;
        double axisMin;
        double axisMax;
        TString xAxisLabel;
        TString yAxisLabel;
        
        hist = hSlopeVsDeltaPhi[ATLAS][0];
        fileName = "SlopesFinal_4Pi.pdf";
        axisMin = hist->GetXaxis()->GetXmin();
        axisMax = hist->GetXaxis()->GetXmax();
        yAxisLabel = TString("Exponential slope #beta [GeV^{-2}]     ");
        xAxisLabel = TString("#Delta#varphi [deg]");
        
        
        hist->GetXaxis()->SetLabelSize(1.5*hist->GetXaxis()->GetLabelSize());
        hist->GetXaxis()->SetTitleSize(1.7*hist->GetXaxis()->GetTitleSize());
        hist->GetYaxis()->SetLabelSize(1.5*hist->GetYaxis()->GetLabelSize());
        hist->GetYaxis()->SetTitleSize(1.7*hist->GetYaxis()->GetTitleSize());
        hist->GetZaxis()->SetLabelSize(1.5*hist->GetZaxis()->GetLabelSize());
        hist->GetZaxis()->SetTitleSize(1.7*hist->GetZaxis()->GetTitleSize());
        
        hist->GetYaxis()->SetTitle( yAxisLabel );
        hist->GetYaxis()->CenterTitle();
        hist->GetXaxis()->SetTitleOffset(1.03);
        hist->GetXaxis()->SetTitle( xAxisLabel );
        hist->GetXaxis()->CenterTitle();
        hist->GetYaxis()->SetTitleOffset(1.05);
        
        hist->GetXaxis()->SetRangeUser(axisMin, axisMax);
        hist->GetYaxis()->SetRangeUser(2, 15);
        hist->GetXaxis()->SetNdivisions(2,1,1, kFALSE);
    //     hist->GetYaxis()->SetNdivisions(4,1,1, kFALSE);
        
        hist->Draw("PE");
        
        
        for(int i=0; i<nExperiments; ++i){
            for(int mr=0; mr<Util::nMassRanges; ++mr){
                
                const double systErrorBoxWidth = 4.0;
                
                TBox *b;
                for(int d=1; d<=Util::nDeltaPhiRanges; ++d){
                    
                    if( slopeSystErrUp[i][d-1][mr]>0 && slopeSystErrDown[i][d-1][mr]>0 && hSlopeVsDeltaPhi[i][mr]->GetBinContent(d)>0 ){
                        
                        b = new TBox( hSlopeVsDeltaPhi[i][mr]->GetBinCenter(d)-systErrorBoxWidth/2+hSlopeVsDeltaPhi[i][mr]->GetBinWidth(d)*( (mr-1)*0.1), 
                                    hSlopeVsDeltaPhi[i][mr]->GetBinContent(d)-slopeSystErrDown[i][d-1][mr], 
                                    hSlopeVsDeltaPhi[i][mr]->GetBinCenter(d)+systErrorBoxWidth/2+hSlopeVsDeltaPhi[i][mr]->GetBinWidth(d)*( (mr-1)*0.1), 
                                    hSlopeVsDeltaPhi[i][mr]->GetBinContent(d)+slopeSystErrUp[i][d-1][mr] );
                        b->SetFillColor( mr==0 ? 209 : (mr==1 ? 95 : 64) );
                        b->SetLineColor( mr==0 ? 209 : (mr==1 ? 95 : 64) );
                        b->SetFillStyle(3004);
                //         b->SetLineWidth(0);
                        b->Draw("l");
                        
                    }
                }
            }
        }
        
        
        for(int i=0; i<nExperiments; ++i){
            for(int mr=0; mr<Util::nMassRanges; ++mr){
                hSlopeVsDeltaPhi[i][mr]->Draw("PE SAME");
            }
        }
        
        lATLAS.DrawLatex(0.25, 0.87, "#scale[0.8]{#font[52]{p + p #rightarrow p' +}#font[42]{ 2#pi^{+}2#pi^{-} }#font[52]{+ p'}}");
        lATLAS.DrawLatex(0.25, 0.76, "#scale[0.8]{#font[42]{#sqrt{s} = 13 TeV}}");

        const double legVerticalOffset = 0.09;
        const int legendFillColor = kWhite/*kYellow-10*/;
        const double alpha = 0.6;
        
    //     TLegend *legend_markerStyle = new TLegend(0.27-0.05, 0.85, 0.64+0.05, 0.95);
        TLegend *legend_markerStyle = new TLegend(0.27-0.06, 0.85-0.64, 0.89+0.04, 0.95-0.64);
        legend_markerStyle->SetTextSize(0.03);
        legend_markerStyle->SetTextColor(kBlack);
    //     legend_markerStyle->SetFillStyle(3000);
        legend_markerStyle->SetFillColorAlpha(legendFillColor, alpha);
        legend_markerStyle->SetBorderSize(0);
        legend_markerStyle->SetMargin(0.2);
        legend_markerStyle->SetNColumns(3);
        
        TH1F *hMass1 = new TH1F();
        hMass1->SetLineWidth(5);
        hMass1->SetLineColor(209);
        TH1F *hMass2 = new TH1F();
        hMass2->SetLineWidth(5);
        hMass2->SetLineColor(95);
        TH1F *hMass3 = new TH1F();
        hMass3->SetLineWidth(5);
        hMass3->SetLineColor(64);
        
        legend_markerStyle->AddEntry(hMass1, "m #in [1.1 GeV, 1.5 GeV]", "l");
        legend_markerStyle->AddEntry(hMass2, "m #in (1.5 GeV, 2.0 GeV]", "l");
        legend_markerStyle->AddEntry(hMass3, "m > 2.0 GeV", "l");
        
        legend_markerStyle->Draw();
        
        canvasSlopes->Print( "PDF/"+fileName, "EmbedFonts" );
        
        gStyle->SetErrorX(0.5);
    }
}



TH2F* CrossSectionAnalysis::getTH2F_from_TH1F_t1t2( const TH1F* hIn, bool divideByBinWidth ){
    
    vector<double> binningT_2D_Vec;
    binningT_2D_Vec.push_back(0.05);
    binningT_2D_Vec.push_back(0.08);
    binningT_2D_Vec.push_back(0.13);
    binningT_2D_Vec.push_back(0.18);
    binningT_2D_Vec.push_back(0.25);
  
  TH2F* hOut = new TH2F( Form("TH2F_of_%s", hIn->GetName()), Form("TH2F_of_%s", hIn->GetName()), binningT_2D_Vec.size()-1, &binningT_2D_Vec[0], binningT_2D_Vec.size()-1, &binningT_2D_Vec[0] );
  
  for(int i=0; i<4; ++i){
    for(int j=0; j<4; ++j){
      double bin2dWidth = divideByBinWidth ? ((binningT_2D_Vec[i+1] - binningT_2D_Vec[i]) * (binningT_2D_Vec[j+1] - binningT_2D_Vec[j])) : 1.0;
      hOut->SetBinContent( i+1, j+1, hIn->GetBinContent(4*i+j+1)/bin2dWidth );
      hOut->SetBinError( i+1, j+1, hIn->GetBinError(4*i+j+1)/bin2dWidth );
    }
  }
  
  return hOut;
  
}




void CrossSectionAnalysis::drawNSigmaMissingPt(){
    
  double hatchesSpacing = gStyle->GetHatchesSpacing();
  double hatchesLineWidth = gStyle->GetHatchesLineWidth();
  gStyle->SetHatchesSpacing(1.5);
  gStyle->SetHatchesLineWidth(3);
  
  
//   TH1F *hMissingPtPid_signal[4];
//   TH1F *hInvMassPid_signal[4];
  TF1* funcMissingPt[4];
  
//   double ptmisseff[4] = {1., 1., 1., 1.};
//   double signalNormalization[4] = {1., 1., 1., 1.};
//   double integratedBkgdFrac[4] = {1., 1., 1., 1.};
  
  for(int cepChannel=0; cepChannel<4; ++cepChannel){
      
    TH1D *hNSigmaPtMiss_Oppo;
    TH1D *hNSigmaPtMiss_Same;
    ObservableObject *ooTmp;
    switch(cepChannel){
        case 0 : ooTmp = oo2PiRapidity; break;
        case 1 : ooTmp = oo4PiRapidity; break;
        case 2 : ooTmp = oo6PiRapidity; break;
        case 3 : ooTmp = oo8PiRapidity; break;
        default: ooTmp = nullptr; break;
    }
    hNSigmaPtMiss_Oppo = ooTmp->hNSigmaPtMissVsQ[Util::OPPO]->ProjectionY(Form("%drandName", static_cast<int>(1000000*gRandom->Uniform())));
    hNSigmaPtMiss_Same = ooTmp->hNSigmaPtMissVsQ[Util::SAME]->ProjectionY(Form("%drandName", static_cast<int>(1000000*gRandom->Uniform())));
    
    funcMissingPt[cepChannel] = new TF1();
    funcMissingPt[cepChannel]->SetName(Form("%dfunc", static_cast<int>(1000000*gRandom->Uniform())));
    /*integratedBkgdFrac[cepChannel] = */mUtil->bkgdFraction( hNSigmaPtMiss_Oppo, 3, 5, 10, funcMissingPt[cepChannel] );
    
//     if(fileSignal[cepChannel]){
//       hMissingPtPid_signal[cepChannel] = dynamic_cast<TH1F*>(  fileSignal[cepChannel]->Get( hNSigmaPtMiss_Oppo->GetName() ) );
//       hMissingPtPid_signal[cepChannel]->SetDirectory(0);
//       hMissingPtPid_signal[cepChannel]->SetName( Form("%s_signalFromCepMC", hNSigmaPtMiss_Oppo->GetName()) );
//       
//       hInvMassPid_signal[cepChannel] = dynamic_cast<TH1F*>(  fileSignal[cepChannel]->Get( mhInvMassPid[Util::OPPO][cepChannel]->GetName() ) );
//       hInvMassPid_signal[cepChannel]->SetDirectory(0);
//       hInvMassPid_signal[cepChannel]->SetName( Form("%s_signalFromCepMC", mhInvMassPid[Util::OPPO][cepChannel]->GetName()) );
//       
//       hInvMassPid_signal[cepChannel]->SetFillColor( cepChannel==0 ? (kYellow-7) : (cepChannel==1 ? (kTeal-8) : myBlueIndex) );
//       hInvMassPid_signal[cepChannel]->SetLineColor( kBlack );
//       hInvMassPid_signal[cepChannel]->SetLineWidth( 2 );
//       
//       ptmisseff[cepChannel] = hMissingPtPid_signal[cepChannel]->Integral( 0, hMissingPtPid_signal[cepChannel]->GetXaxis()->FindBin( mParams->maxMissingPt()-0.0001 ), "width" ) / hMissingPtPid_signal[cepChannel]->Integral( 0, -1, "width" );
//       signalNormalization[cepChannel] =  (1.-integratedBkgdFrac[cepChannel]) * hNSigmaPtMiss_Oppo->Integral( 0, hNSigmaPtMiss_Oppo->GetXaxis()->FindBin( mParams->maxMissingPt()-0.0001 ), "width" ) /
//       hMissingPtPid_signal[cepChannel]->Integral( 0, hMissingPtPid_signal[cepChannel]->GetXaxis()->FindBin( mParams->maxMissingPt()-0.0001 ), "width" );
//       
//       fileSignal[cepChannel]->Close();
//     }
  
    
    TString pairStr(cepChannel==0 ? "#pi^{+}#pi^{-}" : (cepChannel==1 ? "2#pi^{+}2#pi^{-}" : (cepChannel==2 ? "3#pi^{+}3#pi^{-}" : "4#pi^{+}4#pi^{-}")) );
    TString singleTrkPlusMinusStr;
    TString singleTrkStr[2];
    singleTrkStr[Util::MINUS] = TString(/*cepChannel==0 ? */"#pi^{-}" /*: (cepChannel==1 ? "K^{-}" : "#bar{#it{p}}") */);
    singleTrkStr[Util::PLUS] = TString(/*cepChannel==0 ? */"#pi^{+}"/* : (cepChannel==1 ? "K^{+}" : "#it{p}") */);
    singleTrkPlusMinusStr = TString(/*cepChannel==0 ? */"#pi^{#pm}:"/* : (cepChannel==1 ? "K^{#pm}:" : "#it{p},#bar{#it{p}}:")*/ );
    
    //---
    
    std::vector<double> binsVector;
    std::vector<double> binsVector2;
      
//     if( cepChannel==3 ){
//         binsVector.push_back(  0 );
//         binsVector.push_back(  1 );
//         binsVector.push_back(  2 );
//         binsVector.push_back(  3.0 );
//     } else{
        binsVector.push_back(  0 );
        binsVector.push_back(  0.25 );
        binsVector.push_back(  0.5 );
        binsVector.push_back(  1 );
        binsVector.push_back(  1.5 );
        binsVector.push_back(  2 );
        binsVector.push_back(  2.5 );
        binsVector.push_back(  3.0 );
//     }
        
        binsVector.push_back(  5.0 );
        binsVector.push_back(  7.5 );
        binsVector.push_back(  10.0 );
        binsVector.push_back(  12.0 );
        binsVector.push_back(  14.0 );
        binsVector.push_back(  16.0 );
        binsVector.push_back(  18.0 );
        binsVector.push_back(  20.0 );
        binsVector.push_back(  22.0 );
        binsVector.push_back(  26.0 );
        binsVector.push_back(  30.0 );
        
        binsVector2.push_back(  0 );
        binsVector2.push_back(  1 );
        binsVector2.push_back(  2 );
        binsVector2.push_back(  3.0 );
        binsVector2.push_back(  5.0 );
        binsVector2.push_back(  7.5 );
        binsVector2.push_back(  10.0 );
        binsVector2.push_back(  12.0 );
        binsVector2.push_back(  14.0 );
        binsVector2.push_back(  16.0 );
        binsVector2.push_back(  18.0 );
        binsVector2.push_back(  20.0 );
        binsVector2.push_back(  22.0 );
        binsVector2.push_back(  26.0 );
        binsVector2.push_back(  30.0 );
  
      
      
    TH1F* missingPtHist_DataOppo = new TH1F( Form("%s_copyayy", hNSigmaPtMiss_Oppo->GetName()), Form("%s_copyyy", hNSigmaPtMiss_Oppo->GetName()), binsVector.size()-1, &binsVector[0] );
    TH1F* missingPtHist_DataSame = new TH1F( Form("%s_copysyy", hNSigmaPtMiss_Same->GetName()), Form("%s_copyyy", hNSigmaPtMiss_Same->GetName()), binsVector2.size()-1, &binsVector2[0] );
//     TH1F* hMissingPtPid_signal_pointer = hMissingPtPid_signal[cepChannel];
//     if(hMissingPtPid_signal_pointer)
//       hMissingPtPid_signal[cepChannel] = new TH1F( Form("%s_copyyyd", hMissingPtPid_signal_pointer->GetName()), Form("%s_copyyy", hMissingPtPid_signal_pointer->GetName()), binsVector.size()-1, &binsVector[0] );
    TH1F* missingPtHist_nonExclBkgd = new TH1F( Form("%s_missingPtHist_nonExclBkgd", hNSigmaPtMiss_Oppo->GetName()), Form("%s_missingPtHist_nonExclBkgd", hNSigmaPtMiss_Oppo->GetName()), binsVector.size()-1, &binsVector[0] );
    TH1F* missingPtHist_nonExclBkgd_FitRegion = new TH1F( Form("%s_missingPtHist_nonExclBkgd_FitRegion", hNSigmaPtMiss_Oppo->GetName()), Form("%s_missingPtHist_nonExclBkgd_FitRegion", hNSigmaPtMiss_Oppo->GetName()), binsVector.size()-1, &binsVector[0] );
    TH1F* missingPtHist_nonExclBkgd_FullRange = new TH1F( Form("%s_missingPtHist_nonExclBkgd_FullRange", hNSigmaPtMiss_Oppo->GetName()), Form("%s_missingPtHist_nonExclBkgd_FullRange", hNSigmaPtMiss_Oppo->GetName()), binsVector.size()-1, &binsVector[0] );
    
    for(int bin=0; bin<=(hNSigmaPtMiss_Oppo->GetNbinsX()+1); ++bin){
      missingPtHist_DataOppo->Fill( hNSigmaPtMiss_Oppo->GetXaxis()->GetBinCenter(bin), hNSigmaPtMiss_Oppo->GetBinContent(bin) );
      missingPtHist_DataSame->Fill( hNSigmaPtMiss_Same->GetXaxis()->GetBinCenter(bin), hNSigmaPtMiss_Same->GetBinContent(bin) );
//       if(hMissingPtPid_signal_pointer)
//         hMissingPtPid_signal[cepChannel]->Fill( hMissingPtPid_signal_pointer->GetXaxis()->GetBinCenter(bin), hMissingPtPid_signal_pointer->GetBinContent(bin) );
    }
    
//     if(hMissingPtPid_signal_pointer){
//       for(int i=0; i<funcMissingPt[cepChannel]->GetNpar(); ++i)
//         funcMissingPt[cepChannel]->SetParameter( i, funcMissingPt[cepChannel]->GetParameter(i) * missingPtHist_DataOppo->GetBinWidth(1)/hMissingPtPid_signal_pointer->GetBinWidth(1) );
//     }
   
    for(int bin=1; bin<=missingPtHist_DataOppo->GetNbinsX(); ++bin){ // ommit last bin which is the overflow
      missingPtHist_DataOppo->SetBinError( bin, sqrt(missingPtHist_DataOppo->GetBinContent(bin)) );
      missingPtHist_DataOppo->SetBinContent( bin, missingPtHist_DataOppo->GetBinContent(bin) * missingPtHist_DataOppo->GetBinWidth(1)/missingPtHist_DataOppo->GetBinWidth(bin) );
      missingPtHist_DataOppo->SetBinError( bin, missingPtHist_DataOppo->GetBinError(bin) * missingPtHist_DataOppo->GetBinWidth(1)/missingPtHist_DataOppo->GetBinWidth(bin) );
      
      missingPtHist_DataSame->SetBinError( bin, sqrt(missingPtHist_DataSame->GetBinContent(bin)) );
      missingPtHist_DataSame->SetBinContent( bin, missingPtHist_DataSame->GetBinContent(bin) * missingPtHist_DataOppo->GetBinWidth(1)/missingPtHist_DataSame->GetBinWidth(bin) );
      missingPtHist_DataSame->SetBinError( bin, missingPtHist_DataSame->GetBinError(bin) * missingPtHist_DataOppo->GetBinWidth(1)/missingPtHist_DataSame->GetBinWidth(bin) );
      
//       if(hMissingPtPid_signal_pointer){
//         hMissingPtPid_signal[cepChannel]->SetBinError( bin, sqrt(hMissingPtPid_signal[cepChannel]->GetBinContent(bin)) );
//         hMissingPtPid_signal[cepChannel]->SetBinContent( bin, hMissingPtPid_signal[cepChannel]->GetBinContent(bin) * hMissingPtPid_signal[cepChannel]->GetBinWidth(1)/hMissingPtPid_signal[cepChannel]->GetBinWidth(bin) );
//         hMissingPtPid_signal[cepChannel]->SetBinError( bin, hMissingPtPid_signal[cepChannel]->GetBinError(bin) * hMissingPtPid_signal[cepChannel]->GetBinWidth(1)/hMissingPtPid_signal[cepChannel]->GetBinWidth(bin) );
//       }
    }
   
    for(int bin=1; bin<=missingPtHist_nonExclBkgd->GetNbinsX(); ++bin){
      
      const double integralOverBinWidth = funcMissingPt[cepChannel]->Integral( missingPtHist_nonExclBkgd->GetXaxis()->GetBinLowEdge(bin), missingPtHist_nonExclBkgd->GetXaxis()->GetBinUpEdge(bin) ) / missingPtHist_nonExclBkgd->GetBinWidth(bin);
      
      if( missingPtHist_nonExclBkgd->GetXaxis()->GetBinCenter(bin) < 3 ){
        missingPtHist_nonExclBkgd->SetBinContent( bin, integralOverBinWidth );
        missingPtHist_nonExclBkgd->SetBinError( bin, 0. );
      }
      if( missingPtHist_nonExclBkgd_FitRegion->GetXaxis()->GetBinCenter(bin) < 10 && missingPtHist_nonExclBkgd_FitRegion->GetXaxis()->GetBinCenter(bin) > 5){
        missingPtHist_nonExclBkgd_FitRegion->SetBinContent( bin, integralOverBinWidth );
        missingPtHist_nonExclBkgd_FitRegion->SetBinError( bin, 0. );
      }
      if( missingPtHist_nonExclBkgd_FitRegion->GetXaxis()->GetBinCenter(bin) < 10 ){
        missingPtHist_nonExclBkgd_FullRange->SetBinContent( bin, integralOverBinWidth );
        missingPtHist_nonExclBkgd_FullRange->SetBinError( bin, 0. );
      }
    }

    
    DrawingOptions opts;
    
    opts.mCanvasWidth = 900;
    opts.mCanvasHeight = 800;
    
    opts.mLeftMargin = 0.12;
    opts.mRightMargin = 0.035;
    opts.mTopMargin = 0.06;
    opts.mBottomMargin = 0.13;
    
    //x axis
    opts.mXmin = 0;
    opts.mXmax = 30;
    opts.mXaxisTitle = TString("n#sigma(p_{T}^{miss})");
    opts.mXaxisTitleSize = 0.05;
    opts.mXaxisTitleOffset = 1.04;
    opts.mXaxisLabelSize = 0.05;
    opts.mXaxisLabelOffset = 0.007;
    opts.mXaxisTickLength = 0.015;
    
    opts.mSetDivisionsX = kTRUE;
    //     opt.mForceDivisionsX = kTRUE;
    opts.mXnDivisionA = 5;
    opts.mXnDivisionB = 5;
    opts.mXnDivisionC = 1;
    
    opts.mSetDivisionsY = kTRUE;
    //     opt.mForceDivisionsY = kTRUE;
    opts.mYnDivisionA = 5;
    opts.mYnDivisionB = 5;
    opts.mYnDivisionC = 1;
    
    //y axis
    opts.mYmin = 0;
    opts.mYmax = (cepChannel==3 ? 1.6 : 1.15) * missingPtHist_DataOppo->GetMaximum();
    opts.mYaxisTitle = TString( Form("Events / %g", missingPtHist_DataOppo->GetXaxis()->GetBinWidth(1) ) );
    opts.mYaxisTitleSize = 0.05;
    opts.mYaxisTitleOffset = 1.25;
    opts.mYaxisLabelSize = 0.05;
    opts.mYaxisLabelOffset = 0.007;
    opts.mYaxisTickLength = 0.015;
    
    opts.mMarkerStyle = 20;
    opts.mMarkerColor = kBlack;
    opts.mMarkerSize = 1.6;
    
    opts.mMarkerStyle2 = 20;
    opts.mMarkerColor2 = kRed;
    opts.mMarkerSize2 = 1.6;
    
    opts.mLineStyle = 1;
    opts.mLineColor = kBlack;
    opts.mLineWidth = 2;
    
    opts.mLineStyle2 = 1;
    opts.mLineColor2 = kRed;
    opts.mLineWidth2 = 2;
    
    opts.mPdfName = TString(Form("NSigmaMissingPt_channel%d", cepChannel) );
    
    opts.mNColumns = 1;
    opts.mLegendX = cepChannel==0 ? 0.37 : (cepChannel==1 ? 0.45 : 0.5);
    opts.mLegendXwidth = 0.5;
    opts.mLegendY = cepChannel==0 ? 0.35 : (cepChannel==1 ? 0.4 : 0.45);
    opts.mLegendYwidth = 0.24;
    opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
    opts.mSetLegendMargin = kTRUE;
    opts.mLegendMargin = 0.11;
    if(cepChannel>1)
        opts.mFillLegendWithColor = kTRUE;
    
    opts.mDataLegendText_1 = TString(cepChannel==0 ? "" : "Data (total charge = 0)");
    opts.mDataLegendText_2 = TString(cepChannel==0 ? "" : "Data (total charge #neq 0)");
    
    
    double arrowHeight = cepChannel==0 ? 0.4 : (cepChannel==1 ? 0.6 : 0.9);
    
    TLine *cutLine = new TLine( 3, 0, 3, arrowHeight*missingPtHist_DataOppo->GetMaximum() );
    cutLine->SetLineWidth(4);
    cutLine->SetLineColor(kRed);
    cutLine->SetLineStyle(7);
    cutLine->Draw();
    opts.mObjToDraw.push_back(cutLine);
    opts.mObjToDrawOption.push_back("");
    
    
    TArrow *cutArrow = new TArrow( 3, arrowHeight*missingPtHist_DataOppo->GetMaximum(), 1.5, arrowHeight*missingPtHist_DataOppo->GetMaximum(), 0.035, "|>");
    cutArrow->SetAngle(30);
    cutArrow->SetLineWidth(4);
    cutArrow->SetLineColor( kRed );
    cutArrow->SetFillColor( kRed );
    opts.mObjToDraw.push_back(cutArrow);
    opts.mObjToDrawOption.push_back("");
    
    TLatex lReaction(.16, .88, "#font[72]{ATLAS} Internal    #it{p}+#it{p}#rightarrow#it{p}'+"+pairStr+"+#it{p}'  #sqrt{#it{s}} = 13 TeV"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
    opts.mTextToDraw.push_back( lReaction );
//     TLatex lSTAR(0.15, 0.85, "STAR"); lSTAR.SetNDC(); lSTAR.SetTextFont(72);  lSTAR.SetTextSize(.06);
//     opts.mTextToDraw.push_back( lSTAR );
    
    //---//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--
    
    missingPtHist_nonExclBkgd_FitRegion->SetFillColor( kMagenta );
    missingPtHist_nonExclBkgd_FitRegion->SetFillStyle( 3345 );
    missingPtHist_nonExclBkgd_FitRegion->SetLineColor( kMagenta+1 );
    missingPtHist_nonExclBkgd_FitRegion->SetLineWidth( 0 );
    
    TH1F *hCopyForDrawing = new TH1F(*missingPtHist_nonExclBkgd_FitRegion);
    hCopyForDrawing->SetName("hCopyForDrawing");
    hCopyForDrawing->SetLineWidth(2);
    
    opts.mHistTH1F.push_back( missingPtHist_nonExclBkgd_FitRegion );
    opts.mHistTH1F_DrawingOpt.push_back( "HIST ][ SAME" );
    opts.mObjForLegend.push_back( hCopyForDrawing );
    opts.mDesriptionForLegend.push_back(  "Non-excl. background (fit)" );
    opts.mDrawTypeForLegend.push_back( "f" );
    
    //--
    
    missingPtHist_nonExclBkgd->SetFillColor( kMagenta );
    missingPtHist_nonExclBkgd->SetLineColor( kMagenta+1 );
    missingPtHist_nonExclBkgd->SetLineWidth( 2 );
    
    opts.mHistTH1F.push_back( missingPtHist_nonExclBkgd );
    opts.mHistTH1F_DrawingOpt.push_back( "HIST ][ SAME" );
    opts.mObjForLegend.push_back( missingPtHist_nonExclBkgd );
    opts.mDesriptionForLegend.push_back(  "Non-excl. background (extr.)" );
    opts.mDrawTypeForLegend.push_back( "f" );
    
    //--
    
    missingPtHist_nonExclBkgd_FullRange->SetLineColor( kMagenta+1 );
    missingPtHist_nonExclBkgd_FullRange->SetLineWidth( 2 );
    opts.mHistTH1F.push_back( missingPtHist_nonExclBkgd_FullRange );
    opts.mHistTH1F_DrawingOpt.push_back( "HIST ][ SAME" );
    //--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--v
    
//     hMissingPtPid_signal[cepChannel]->Scale( signalNormalization[cepChannel] );
//     hMissingPtPid_signal[cepChannel]->SetFillColor( cepChannel==0 ? (kYellow-7) : (cepChannel==1 ? (kTeal-8) : myBlueIndex) );
//     hMissingPtPid_signal[cepChannel]->SetLineColor( kBlack );
//     hMissingPtPid_signal[cepChannel]->SetLineWidth( 2 );
//     
//     opts.mHistTH1_stack.push_back( hMissingPtPid_signal[cepChannel] );
//     opts.mHistTH1_stack_DrawingOpt.push_back( "HIST" );
//     opts.mObjForLegend.push_back( hMissingPtPid_signal[cepChannel] );
//     opts.mDesriptionForLegend.push_back(  TString("Exclusive ")+pairStr );
//     opts.mDrawTypeForLegend.push_back( "f" );
    
    /* //special shaded magenta for the background
    double ww = 0.2 - mParams->maxMissingPt();
    for(int bb=0; bb<100; ++bb){
      double xMinSystErrorBox = mParams->maxMissingPt() + bb*ww/100;
      double xMaxSystErrorBox = mParams->maxMissingPt() + (bb+1)*ww/100;
      int binNumber = missingPtHist_nonExclBkgd->GetXaxis()->FindBin( xMinSystErrorBox );
      double yMinSystErrorBox = 0;
      double yMaxSystErrorBox = missingPtHist_nonExclBkgd->GetBinContent(binNumber);
      TBox *b = new TBox( xMinSystErrorBox, yMinSystErrorBox, xMaxSystErrorBox, yMaxSystErrorBox );
      b->SetFillColor( magentaToWhite[bb] );
      b->SetLineWidth(0);
      b->Draw("l");
      opts.mObjToDraw.push_back( b );
      opts.mObjToDrawOption.push_back("SAME");
    }
    */
    //----
    
    
        TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
        TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
        TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
        TString rpKinematicRegion1("0.17 GeV < |p_{y}| < 0.5 GeV");
        TLatex lCutsA(.16+0.08,.84-0.02, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
        TLatex lCutsB(.52,.84-0.02, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
        TLatex lCuts1(.26+0.04,.84-0.02, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
        TLatex lCuts12(.26+0.04,.795-0.02, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
        TLatex lCuts11(.26+0.04,.75-0.02, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
        TLatex lCuts3(.62,.84-0.02, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
    
    drawDataMcComparison2( missingPtHist_DataOppo, missingPtHist_DataSame, opts );
  }
  
  gStyle->SetHatchesSpacing(hatchesSpacing);
  gStyle->SetHatchesLineWidth(hatchesLineWidth);
    
}





void CrossSectionAnalysis::drawDataMcComparison2(TH1F* hMainInput, TH1F* hMainInput2, DrawingOptions & opt){
  
  TH1F *hMain = new TH1F( *hMainInput );
  hMain->SetName("drawDataMcComparison2_"+TString(hMain->GetName()));
  TH1F *hMain2 = new TH1F( *hMainInput2 );
  hMain2->SetName("drawDataMcComparison2_"+TString(hMain2->GetName()));
  
  TGaxis::SetMaxDigits(3);
  
  TCanvas *c = new TCanvas("cccccc", "cccccc", opt.mCanvasWidth, opt.mCanvasHeight);
  //   c->SetFrameFillStyle(0);
  //   c->SetFrameLineWidth(2);
  //   c->SetFillColor(-1);
  c->SetLeftMargin(opt.mLeftMargin);
  c->SetRightMargin(opt.mRightMargin);
  c->SetTopMargin(opt.mTopMargin);
  c->SetBottomMargin(opt.mBottomMargin);
  
  c->SetLogy(opt.mYlog);

  
  if(opt.mScale){
    hMain->Scale( opt.mScalingFactor );
    hMain2->Scale( opt.mScalingFactor );
  }
  
  hMain->SetMarkerStyle(opt.mMarkerStyle);
  hMain->SetMarkerSize(opt.mMarkerSize);
  hMain->SetMarkerColor(opt.mMarkerColor);
  hMain->SetLineStyle(opt.mLineStyle);
  hMain->SetLineColor(opt.mLineColor);
  hMain->SetLineWidth(opt.mLineWidth);
  
  hMain2->SetMarkerStyle(opt.mMarkerStyle2);
  hMain2->SetMarkerSize(opt.mMarkerSize2);
  hMain2->SetMarkerColor(opt.mMarkerColor2);
  hMain2->SetLineStyle(opt.mLineStyle);
  hMain2->SetLineColor(opt.mLineColor2);
  hMain2->SetLineWidth(opt.mLineWidth2);
  
  hMain->GetXaxis()->SetTitle(opt.mXaxisTitle);
  hMain->GetXaxis()->SetTitleSize(opt.mXaxisTitleSize);
  hMain->GetXaxis()->SetTitleOffset(opt.mXaxisTitleOffset);
  hMain->GetXaxis()->SetLabelSize(opt.mXaxisLabelSize);
  hMain->GetXaxis()->SetLabelOffset(opt.mXaxisLabelOffset);
  hMain->GetXaxis()->SetTickLength(opt.mXaxisTickLength);
  hMain->GetXaxis()->SetRangeUser(opt.mXmin, opt.mXmax);
  
  if( opt.mSetDivisionsX )
    hMain->GetXaxis()->SetNdivisions( opt.mXnDivisionA, opt.mXnDivisionB, opt.mXnDivisionC, !opt.mForceDivisionsX );
  
  hMain->GetYaxis()->SetTitleSize(opt.mYaxisTitleSize);
  hMain->GetYaxis()->SetTitle(opt.mYaxisTitle);
  hMain->GetYaxis()->SetTitleOffset(opt.mYaxisTitleOffset);
  hMain->GetYaxis()->SetLabelSize(opt.mYaxisLabelSize);
  hMain->GetYaxis()->SetLabelOffset(opt.mYaxisLabelOffset);
  hMain->GetYaxis()->SetTickLength(opt.mYaxisTickLength);
  hMain->GetYaxis()->SetRangeUser(opt.mYmin, (opt.mScale ? opt.mScalingFactor : 1.0) * opt.mYmax);
  
  if( opt.mSetDivisionsY ){
    hMain->GetYaxis()->SetNdivisions( opt.mYnDivisionA, opt.mYnDivisionB, opt.mYnDivisionC, !opt.mForceDivisionsY );
    hMain2->GetYaxis()->SetNdivisions( opt.mYnDivisionA, opt.mYnDivisionB, opt.mYnDivisionC, !opt.mForceDivisionsY );
  }
  
  hMain->Draw("PE"/*"PEX0"*/);
  hMain2->Draw("PE SAME");
  
  for(unsigned int i=0; i<opt.mHistTH1F.size(); ++i){
    if(opt.mScale){
      opt.mHistTH1F[i]->Scale( opt.mScalingFactor );
    }
    opt.mHistTH1F[i]->GetXaxis()->SetRangeUser(opt.mXmin, opt.mXmax);
    opt.mHistTH1F[i]->Draw( opt.mHistTH1F_DrawingOpt[i] );
  }
  
  
//   if( opt.mHistTH1_stack.size() > 0 ){
//     THStack *histStack = new THStack("histStack", "");
//     for(unsigned int i=0; i<opt.mHistTH1_stack.size(); ++i){
//       opt.mHistTH1_stack[i]->GetXaxis()->SetRangeUser(opt.mXmin, opt.mXmax);
//       histStack->Add( opt.mHistTH1_stack[i], opt.mHistTH1_stack_DrawingOpt[i] );
//     }
//     histStack->Draw("SAME");
//   }
  
  for(unsigned int i=0; i<opt.mObjToDraw.size(); ++i){
    if( opt.mObjToDrawOption[i]=="" )
      opt.mObjToDraw[i]->Draw();
    else
      opt.mObjToDraw[i]->Draw( opt.mObjToDrawOption[i] );
  }
  
    
  if( opt.mTextToDraw.size() > 0 ){
    for(unsigned int i=0; i<opt.mTextToDraw.size(); ++i){
      opt.mTextToDraw[i].SetNDC(kTRUE);
      opt.mTextToDraw[i].Draw();
    }
  }
  
//   if( opt.mTextToDraw_NotNDC.size() > 0 ){
//     for(unsigned int i=0; i<opt.mTextToDraw_NotNDC.size(); ++i){
//       opt.mTextToDraw_NotNDC[i].SetNDC(kFALSE);
//       opt.mTextToDraw_NotNDC[i].Draw();
//     }
//   }
  
  
  hMain->DrawCopy("PE SAME"/*"PEX0 SAME"*/, "");
  hMain2->DrawCopy("PE SAME"/*"PEX0 SAME"*/, "");
  gPad->RedrawAxis();
  
  TLegend *legend = new TLegend( opt.mLegendX, opt.mLegendY, opt.mLegendX+opt.mLegendXwidth, opt.mLegendY+opt.mLegendYwidth );
  legend->SetNColumns(opt.mNColumns);
  legend->SetBorderSize(0);  
  legend->SetTextSize(opt.mLegendTextSize);
  legend->AddEntry(hMain, opt.mDataLegendText_1==TString("") ? "Data (opposite-sign)" : opt.mDataLegendText_1, "pel"/*"pe"*/);
  legend->AddEntry(hMain2, opt.mDataLegendText_2==TString("") ? "Data (same-sign)" : opt.mDataLegendText_2, "pel");
  for(unsigned int i=0; i<opt.mObjForLegend.size(); ++i)
    legend->AddEntry( opt.mObjForLegend[i], opt.mDesriptionForLegend[i], opt.mDrawTypeForLegend[i]);
  if( opt.mSetLegendMargin )
    legend->SetMargin( opt.mLegendMargin );
  else
    legend->SetMargin(0.3 * opt.mLegendXwidth * legend->GetNRows() / legend->GetNColumns() );
  if( opt.mFillLegendWithColor )
      legend->SetFillColorAlpha( kWhite, 0.8);
  else
      legend->SetFillStyle(0);
  legend->Draw();
  

  
  
//   if( opt.mInsert ){
//     TPad *insert = new TPad("insert", "insert", opt.mX1Insert, opt.mY1Insert, opt.mX2Insert, opt.mY2Insert);
//     insert->SetBottomMargin(0); // Upper and lower plot are joined
//     insert->SetFrameFillStyle(0);
//     insert->SetFrameLineWidth(2);
//     insert->SetFillColor(-1);
//     insert->Draw();             // Draw the upper pad: insert
//     insert->cd();               // insert becomes the current pad
//     insert->SetLeftMargin(opt.mLeftMargin);
//     insert->SetRightMargin(opt.mRightMargin);
//     insert->SetTopMargin(opt.mTopMargin);
//     insert->SetBottomMargin(opt.mBottomMargin);
//     
//     TH1F *hMainCopy = new TH1F( *hMain );
//     TH1F *hMainCopy2 = new TH1F( *hMain2 );
//     hMainCopy->GetXaxis()->SetTitle("");
//     hMainCopy->GetXaxis()->SetLabelSize( 0.9*hMainCopy->GetXaxis()->GetLabelSize() * ( 1. - opt.mLeftMargin - opt.mRightMargin) / (opt.mX2Insert - opt.mX1Insert) );
//     hMainCopy->GetYaxis()->SetLabelSize( 0.9*hMainCopy->GetYaxis()->GetLabelSize() * ( 1. - opt.mLeftMargin - opt.mRightMargin) / (opt.mX2Insert - opt.mX1Insert) );
//     hMainCopy->GetYaxis()->SetTitle("");
//     hMainCopy->GetXaxis()->SetRange( hMainCopy->FindBin(opt.mXMinInstert ), hMainCopy->FindBin(opt.mXMaxInstert ) );
//     int binMaxY;
//     binMaxY = hMainCopy->GetMaximumBin();
//     hMainCopy->GetYaxis()->SetRangeUser(0.01, opt.mYMaxInstert<0 ? (1.1*hMainCopy->GetBinContent( binMaxY )) : opt.mYMaxInstert);
//     
//     int n1 = hMainCopy->GetYaxis()->GetNdivisions()%100;
//     int n2 = hMainCopy->GetYaxis()->GetNdivisions()%10000 - n1;
//     int n3 = hMainCopy->GetYaxis()->GetNdivisions() - 100*n2 - n1;
//     
//     hMainCopy->GetYaxis()->SetNdivisions( n1/2, 2, 1 );
//     hMainCopy->GetXaxis()->SetRangeUser(opt.mXMinInstert, opt.mXMaxInstert);
//     hMainCopy->GetXaxis()->SetNdivisions(10,5,1);
//     
//     hMainCopy->GetXaxis()->SetTickLength(0.04);
//     hMainCopy->GetYaxis()->SetTickLength(0.02);
//     
//     hMainCopy->Draw("PEX0");
//     hMainCopy2->Draw("PEX0 SAME");
//     for(unsigned int i=0; i<opt.mHistTH1F.size(); ++i){
//       if(opt.mScale){
//         opt.mHistTH1F[i]->Scale( opt.mScalingFactor );
//       }
//       opt.mHistTH1F[i]->DrawCopy( opt.mHistTH1F_DrawingOpt[i], "" );
//     }
//     
//     hMainCopy->DrawCopy("PEX0 SAME", "");
//     hMainCopy2->DrawCopy("PEX0 SAME", "");
//     gPad->RedrawAxis();
//   }
  
  c->cd();
  c->Modified();
  c->Update();
  
  c->Print(opt.mPdfName+".pdf");
  
  delete c;
}





void CrossSectionAnalysis::drawResolutions(){
    
    TH2F *mhResponseMatrix_InvMass;
    TH2F *mhResponseMatrix_PairRapidity;
    TH2F *mhResponseMatrix_CosThetaCS;
    TH2F *mhResponseMatrix_PhiCS;
    TH2F *mhResponseMatrix_MandelstamTSum;
    TH2F *mhResponseMatrix_DeltaPhi;
    
    
    TFile *file = TFile::Open( "ROOT_files/CD.root" );
    mhResponseMatrix_InvMass = dynamic_cast<TH2F*>(file->Get("mhResponseMatrix_InvMass"));
    mhResponseMatrix_InvMass->SetDirectory(0);
    mhResponseMatrix_PairRapidity = dynamic_cast<TH2F*>(file->Get("mhResponseMatrix_PairRapidity"));
    mhResponseMatrix_PairRapidity->SetDirectory(0);
    mhResponseMatrix_CosThetaCS = dynamic_cast<TH2F*>(file->Get("mhResponseMatrix_CosThetaCS"));
    mhResponseMatrix_CosThetaCS->SetDirectory(0);
    mhResponseMatrix_PhiCS = dynamic_cast<TH2F*>(file->Get("mhResponseMatrix_PhiCS"));
    mhResponseMatrix_PhiCS->SetDirectory(0);
    file->Close();
    file = TFile::Open( "ROOT_files/Output_pion_DiMe_CepTree_pipi.root" );
    mhResponseMatrix_MandelstamTSum = dynamic_cast<TH2F*>(file->Get("mhResponseMatrix_MandelstamTSum"));
    mhResponseMatrix_MandelstamTSum->SetDirectory(0);
    mhResponseMatrix_DeltaPhi = dynamic_cast<TH2F*>(file->Get("mhResponseMatrix_DeltaPhi"));
    mhResponseMatrix_DeltaPhi->SetDirectory(0);
    file->Close();
    
    
    
    
    gStyle->SetPalette(62);
    TColor::InvertPalette();
    TLatex lATLAS; lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.075);
    
    for(int h=0; h<6; ++h){
        
        TCanvas *canvasRespMtrx = new TCanvas("canvasRespMtrx", "canvasRespMtrx", 800, 800);
        
        double leftMargin_canvasRespMtrx = 0.15;
        double rightMargin_canvasRespMtrx = 0.18;
        double topMargin_canvasRespMtrx = 0.06;
        double bottomMargin_canvasRespMtrx = 0.15;
        //   TGaxis::SetMaxDigits(3);
        canvasRespMtrx->SetFillStyle(4000);
        canvasRespMtrx->SetFillColor(0);
        canvasRespMtrx->SetFrameFillStyle(4000);
        canvasRespMtrx->SetLeftMargin(leftMargin_canvasRespMtrx);
        canvasRespMtrx->SetRightMargin(rightMargin_canvasRespMtrx);
        canvasRespMtrx->SetTopMargin(topMargin_canvasRespMtrx);
        canvasRespMtrx->SetBottomMargin(bottomMargin_canvasRespMtrx);
        
        TH2F *hist = nullptr;
        TString fileName;
        double axisMin;
        double axisMax;
        TString xAxisLabel;
        TString yAxisLabel;
        switch( h ){
            case 0: hist = mhResponseMatrix_InvMass;
            fileName = "ResponseMatrix_InvMass.pdf";
            axisMin = hist->GetXaxis()->GetXmin();
            axisMax = 3.0;
            yAxisLabel = TString("m_{true}(#pi^{+}#pi^{-}) [GeV]");
            xAxisLabel = TString("m_{reco}(#pi^{+}#pi^{-}) [GeV]");
            break;
            case 1: hist = mhResponseMatrix_PairRapidity;
            fileName = "ResponseMatrix_PairRapidity.pdf";
            axisMin = hist->GetXaxis()->GetXmin();
            axisMax = hist->GetXaxis()->GetXmax();
            yAxisLabel = TString("y_{true}(#pi^{+}#pi^{-})");
            xAxisLabel = TString("y_{reco}(#pi^{+}#pi^{-})");
            break;
            case 2: hist = mhResponseMatrix_MandelstamTSum;
            fileName = "ResponseMatrix_MandelstamTSum.pdf";
            axisMin = 0.05;
            axisMax = hist->GetXaxis()->GetXmax();
            yAxisLabel = TString("|t_{#kern[-0.2]{#lower[-0.38]{1}}}+t_{#lower[-0.15]{2}}|_{true} [GeV^{2}]");
            xAxisLabel = TString("|t_{#kern[-0.2]{#lower[-0.38]{1}}}+t_{#lower[-0.15]{2}}|_{reco} [GeV^{2}]");
            break;
            case 3: hist = mhResponseMatrix_DeltaPhi;
            fileName = "ResponseMatrix_DeltaPhi.pdf";
            axisMin = 0;
            axisMax = 180;
            yAxisLabel = TString("#Delta#varphi_{true} [deg]");
            xAxisLabel = TString("#Delta#varphi_{reco} [deg]");
            break;
            case 4: hist = mhResponseMatrix_CosThetaCS;
            fileName = "ResponseMatrix_CosThetaCS.pdf";
            axisMin = -1;
            axisMax = 1;
            yAxisLabel = TString("cos#theta^{CS}_{true}");
            xAxisLabel = TString("cos#theta^{CS}_{reco}");
            break;
            case 5: hist = mhResponseMatrix_PhiCS;
            fileName = "ResponseMatrix_PhiCS.pdf";
            axisMin = -180;
            axisMax = 180;
            yAxisLabel = TString("#phi^{CS}_{true} [deg]");
            xAxisLabel = TString("#phi^{CS}_{reco} [deg]");
            break;
        };
        
        hist->GetXaxis()->SetLabelSize(1.5*hist->GetXaxis()->GetLabelSize());
        hist->GetXaxis()->SetTitleSize(1.7*hist->GetXaxis()->GetTitleSize());
        hist->GetYaxis()->SetLabelSize(1.5*hist->GetYaxis()->GetLabelSize());
        hist->GetYaxis()->SetTitleSize(1.7*hist->GetYaxis()->GetTitleSize());
        hist->GetZaxis()->SetLabelSize(1.5*hist->GetZaxis()->GetLabelSize());
        hist->GetZaxis()->SetTitleSize(1.7*hist->GetZaxis()->GetTitleSize());
        
        hist->GetYaxis()->SetTitle( yAxisLabel );
        hist->GetYaxis()->CenterTitle();
        hist->GetXaxis()->SetTitleOffset(1.03);
        hist->GetXaxis()->SetTitle( xAxisLabel );
        hist->GetXaxis()->CenterTitle();
        hist->GetYaxis()->SetTitleOffset(1.15);
        
        hist->GetXaxis()->SetRangeUser(axisMin, axisMax);
        hist->GetYaxis()->SetRangeUser(axisMin, axisMax);
        hist->GetXaxis()->SetNdivisions(5,5,1);
        hist->GetYaxis()->SetNdivisions(5,5,1);
        if( h==3 || h==5){
            hist->GetXaxis()->SetNdivisions(6,3,1, kFALSE);
            hist->GetYaxis()->SetNdivisions(6,3,1, kFALSE);
        }
        
        hist->GetZaxis()->SetTitle( "Probability" );
        hist->GetZaxis()->SetTitleOffset(1.1);
        
        for(int j=1; j<=hist->GetNbinsY(); ++j){
            double sum = 0;
            for(int i=1; i<=hist->GetNbinsX(); ++i)
                sum += hist->GetBinContent( i, j );
            if( sum>0 )
                for(int i=1; i<=hist->GetNbinsX(); ++i)
                    hist->SetBinContent( i, j, hist->GetBinContent(i, j)/sum );
        }
        
        hist->Draw("colz");
        hist->GetZaxis()->SetRangeUser(0, 1);
        
        
        lATLAS.DrawLatex(0.203, 0.82, "#scale[0.6]{#font[42]{#sqrt{s} = 13 TeV}}");
        
        canvasRespMtrx->Print( "PDF/"+fileName );
        
        //     TH2D *resMtrx = new TH2D();
        //     hist->Copy( *resMtrx );
        //     resMtrx->SetName( fileName );
        //     resMtrx->SetDirectory( file );
    }
    
}



void CrossSectionAnalysis::pullAnalysis(){
    
    
    vector<TH2F*> input_unweighted;
    vector<TH2F*> input_weighted;
    vector<TString> input_name;
    
    input_unweighted.push_back(mhN2PiCepEventsVsLumiBlockVsRunNumber[Util::ELA]);
    input_weighted.push_back(mhN2PiCepEventsVsLumiBlockVsRunNumber_Weighted[Util::ELA]);
    input_name.push_back( TString("2Pi_ELA") );
    
    input_unweighted.push_back(mhN4PiCepEventsVsLumiBlockVsRunNumber[Util::ELA]);
    input_weighted.push_back(mhN4PiCepEventsVsLumiBlockVsRunNumber_Weighted[Util::ELA]);
    input_name.push_back( TString("4Pi_ELA") );
    
    input_unweighted.push_back(mhN2PiCepEventsVsLumiBlockVsRunNumber[Util::INE]);
    input_weighted.push_back(mhN2PiCepEventsVsLumiBlockVsRunNumber_Weighted[Util::INE]);
    input_name.push_back( TString("2Pi_INE") );
    
    input_unweighted.push_back(mhN4PiCepEventsVsLumiBlockVsRunNumber[Util::INE]);
    input_weighted.push_back(mhN4PiCepEventsVsLumiBlockVsRunNumber_Weighted[Util::INE]);
    input_name.push_back( TString("4Pi_INE") );
    
    
    for(unsigned int in=0; in<input_name.size(); ++in){
        
    //     TH1F* hNExclusiveEventsPerLumi = new TH1F( "NExclusiveEventsPerLumi", "Number of exclusive events per integrated luminosity", 125, 0, 25000 );
        TH1F* hNExclusiveEventsPerLumi_Pull = new TH1F( Form("NExclusiveEventsPerLumi_Pull_%d", in), "Pull of number of exclusive events per integrated luminosity", 200, -15, 15 );
    //     TH1F* hNExclusiveEventsWeightedPerLumi = new TH1F( "NExclusiveEventsWeightedPerLumi", "Number of exclusive events (weighted) per integrated luminosity", 500, 0, 10000000 );
        TH1F* hNExclusiveEventsWeightedPerLumi_Pull = new TH1F( Form("NExclusiveEventsWeightedPerLumi_Pull_%d", in), "Pull of number of exclusive events per integrated luminosity", 200, -15, 15 );
        
        
        TH2F *hIn_Unw = input_unweighted[in];
        TH2F *hIn_W = input_weighted[in];
        
        
        double totalLumi = 0;
        double totalNExclusiveEvents = 0;
        double totalNExclusiveEventsWeighted = 0;
        
        double averageNExclusiveEventsPerLumi = 0;
        double averageNExclusiveEventsPerLumiWeighted = 0;
        double sumOfWeightsPerLumi = 0;
        double sumOfWeightsPerLumiWeighted = 0;
        
        const double avrgBkgdFrac = 0.05;
        
        hIn_Unw->SetBinErrorOption(TH1::kPoisson);
        hIn_W->SetBinErrorOption(TH1::kPoisson);
        
        for(int i=1; i <= mhNEventsVsLumiBlockVsRunNumber->GetNbinsX(); ++i){
            for(int j=1; j <= mhNEventsVsLumiBlockVsRunNumber->GetNbinsY(); ++j){
                if( mhNEventsVsLumiBlockVsRunNumber->GetBinContent(i,j)>0 && hIn_Unw->GetBinContent(i,j)>0 ){
                    
                    int runNum = static_cast<int>( mhNEventsVsLumiBlockVsRunNumber->GetXaxis()->GetBinCenter(i) );
                    int lb = static_cast<int>( mhNEventsVsLumiBlockVsRunNumber->GetYaxis()->GetBinCenter(j) );
                    
                    const double integratedLumi = lumiPerLumiBlockVector_RunMap[runNum][lb] * lumiblockDurationVector_RunMap[runNum][lb] * 1.e-3;
                    double nExclPerLumi = hIn_Unw->GetBinContent(i,j) / integratedLumi;
                    double nExclWeightedPerLumi = hIn_W->GetBinContent(i,j) / integratedLumi;
                    double nExclPerLumiError = hIn_Unw->GetBinError(i,j) / integratedLumi;
                    double nExclWeightedPerLumiError = hIn_W->GetBinError(i,j) / integratedLumi;
                    
    //                 cout << hIn_Unw->GetBinContent(i,j) << endl;
                    
                    nExclPerLumi *= (1-avrgBkgdFrac);
                    nExclWeightedPerLumi *= (1-avrgBkgdFrac);
                    nExclPerLumiError *= (1+avrgBkgdFrac);
                    nExclWeightedPerLumiError *= (1+avrgBkgdFrac);
                    
                    totalLumi += integratedLumi;
                    totalNExclusiveEvents += hIn_Unw->GetBinContent(i,j);
                    totalNExclusiveEventsWeighted += hIn_W->GetBinContent(i,j);
                    
                    averageNExclusiveEventsPerLumi += nExclPerLumi/pow( nExclPerLumiError, 2);
                    sumOfWeightsPerLumi += 1./pow( nExclPerLumiError, 2);
                    averageNExclusiveEventsPerLumiWeighted += nExclWeightedPerLumi/pow( nExclWeightedPerLumiError, 2);
                    sumOfWeightsPerLumiWeighted += 1./pow( nExclWeightedPerLumiError, 2);
                    
    //                 hNExclusiveEventsPerLumi->Fill( nExclPerLumi );
    //                 hNExclusiveEventsWeightedPerLumi->Fill( nExclWeightedPerLumi );
                }
            }
        }
        
        averageNExclusiveEventsPerLumi /= sumOfWeightsPerLumi;
        averageNExclusiveEventsPerLumiWeighted /= sumOfWeightsPerLumiWeighted;
        
        cout << " Average N = " << averageNExclusiveEventsPerLumi << "   Average Nw = " << averageNExclusiveEventsPerLumiWeighted << endl;
        
        
        for(int i=1; i <= mhNEventsVsLumiBlockVsRunNumber->GetNbinsX(); ++i){
            for(int j=1; j <= mhNEventsVsLumiBlockVsRunNumber->GetNbinsY(); ++j){
                if( mhNEventsVsLumiBlockVsRunNumber->GetBinContent(i,j)>0 && hIn_Unw->GetBinContent(i,j)>0 ){
                    
                    int runNum = static_cast<int>( mhNEventsVsLumiBlockVsRunNumber->GetXaxis()->GetBinCenter(i) );
                    int lb = static_cast<int>( mhNEventsVsLumiBlockVsRunNumber->GetYaxis()->GetBinCenter(j) );
                    
                    const double integratedLumi = lumiPerLumiBlockVector_RunMap[runNum][lb] * lumiblockDurationVector_RunMap[runNum][lb]  * 1.e-3;
                    double nExclPerLumi = hIn_Unw->GetBinContent(i,j) / integratedLumi;
                    double nExclPerLumiError =  hIn_Unw->GetBinError(i,j) / integratedLumi;
                    double nExclPerLumiError_Up =  hIn_Unw->GetBinErrorUp(i,j) / integratedLumi;
                    double nExclPerLumiError_Down =  hIn_Unw->GetBinErrorLow(i,j) / integratedLumi;
                    double nExclWeightedPerLumi = hIn_W->GetBinContent(i,j) / integratedLumi;
                    double nExclWeightedPerLumiError =  hIn_W->GetBinError(i,j) / integratedLumi;
                    double nExclWeightedPerLumiError_Up =  hIn_W->GetBinErrorUp(i,j) / integratedLumi;
                    double nExclWeightedPerLumiError_Down =  hIn_W->GetBinErrorLow(i,j) / integratedLumi;
                    
                    nExclPerLumi *= (1-avrgBkgdFrac);
                    nExclPerLumiError *= (1+avrgBkgdFrac);
                    nExclPerLumiError_Up *= (1+avrgBkgdFrac);
                    nExclPerLumiError_Down *= (1+avrgBkgdFrac);
                    nExclWeightedPerLumi *= (1-avrgBkgdFrac);
                    nExclWeightedPerLumiError *= (1+avrgBkgdFrac);
                    nExclWeightedPerLumiError_Up *= (1+avrgBkgdFrac);
                    nExclWeightedPerLumiError_Down *= (1+avrgBkgdFrac);
                    
                    bool positive = (nExclWeightedPerLumi - averageNExclusiveEventsPerLumiWeighted) > 0;
                    bool positive_uncorrected = (nExclPerLumi - averageNExclusiveEventsPerLumi) > 0;
                    double pull = (nExclWeightedPerLumi - averageNExclusiveEventsPerLumiWeighted ) / (positive ? nExclWeightedPerLumiError_Down : nExclWeightedPerLumiError_Up );
                    double pull_uncorrected = (nExclPerLumi - averageNExclusiveEventsPerLumi ) / (positive_uncorrected ? nExclPerLumiError_Down : nExclPerLumiError_Up );
                    
                    if(pull < -15 )
                        cout << runNum << " LB: " << lb << "  duration: " << lumiblockDurationVector_RunMap[runNum][lb] << "   N = " << hIn_Unw->GetBinContent(i,j) <<  " +/- " << hIn_Unw->GetBinError(i,j) << "   Nw = " << hIn_W->GetBinContent(i,j) <<  " +/- " << hIn_W->GetBinError(i,j) << "    pull = " << pull_uncorrected << "   pull_corr = " << pull << endl;
                    
                    hNExclusiveEventsWeightedPerLumi_Pull->Fill( pull );
                    hNExclusiveEventsPerLumi_Pull->Fill( pull_uncorrected );
                }
            }
        }
        
        
        
        //-------------------------------------
        
        
        TCanvas *c = new TCanvas("c", "c", 800, 800);
        c->SetFrameFillStyle(0);
        TGaxis::SetMaxDigits(3);
        c->SetFillColor(-1);
        c->SetLeftMargin(0.13);
        c->SetRightMargin(0.025);
        c->SetTopMargin(0.02);
        c->SetBottomMargin(0.14);
        
        
        TF1 *gausPull = new TF1(Form("gausPull_%d", in), "gausn", -15, 15);
        gausPull->SetLineWidth(4);
        gausPull->SetLineColor(kRed);
        gausPull->SetNpx(1e3);
        
        TF1 *gausPull_uncorrected = new TF1(Form("gausPull_uncorrected_%d", in), "gausn", -15, 15);
        gausPull_uncorrected->SetLineWidth(4);
        gausPull_uncorrected->SetLineStyle(7);
        gausPull_uncorrected->SetLineColor(kBlue);
        gausPull_uncorrected->SetNpx(1e3);
        
        hNExclusiveEventsWeightedPerLumi_Pull->Rebin(5);
        hNExclusiveEventsWeightedPerLumi_Pull->GetXaxis()->SetRangeUser(-16, 16);
        hNExclusiveEventsWeightedPerLumi_Pull->GetYaxis()->SetRangeUser(0, 1.3*hNExclusiveEventsWeightedPerLumi_Pull->GetMaximum());
        hNExclusiveEventsWeightedPerLumi_Pull->GetXaxis()->SetTitleSize(0.06);
        hNExclusiveEventsWeightedPerLumi_Pull->GetXaxis()->SetLabelSize(0.05);
        hNExclusiveEventsWeightedPerLumi_Pull->GetYaxis()->SetTitleSize(0.06);
        hNExclusiveEventsWeightedPerLumi_Pull->GetYaxis()->SetLabelSize(0.05);
        hNExclusiveEventsWeightedPerLumi_Pull->GetXaxis()->SetNdivisions(6,6,1);
        hNExclusiveEventsWeightedPerLumi_Pull->GetYaxis()->SetNdivisions(5,5,1);
        hNExclusiveEventsWeightedPerLumi_Pull->GetYaxis()->SetTitleOffset(1.08);
        hNExclusiveEventsWeightedPerLumi_Pull->SetStats(kFALSE);
        hNExclusiveEventsWeightedPerLumi_Pull->SetLineWidth(2);
        hNExclusiveEventsWeightedPerLumi_Pull->Fit(gausPull, "QNI");
        hNExclusiveEventsWeightedPerLumi_Pull->SetMarkerSize(1.6);
        hNExclusiveEventsWeightedPerLumi_Pull->SetMarkerStyle(20);
        hNExclusiveEventsWeightedPerLumi_Pull->SetMarkerColor(kBlack);
        hNExclusiveEventsWeightedPerLumi_Pull->SetLineColor(kBlack);
        hNExclusiveEventsWeightedPerLumi_Pull->GetXaxis()->SetTitle("Pull");
        hNExclusiveEventsWeightedPerLumi_Pull->GetXaxis()->SetTitleOffset(0.8);
        hNExclusiveEventsWeightedPerLumi_Pull->GetYaxis()->SetTitle(Form("Luminosity blocks / %.1f", hNExclusiveEventsWeightedPerLumi_Pull->GetXaxis()->GetBinWidth(1)));
        hNExclusiveEventsWeightedPerLumi_Pull->Draw("PE");
        
    //     hNExclusiveEventsWeightedPerLumi_Pull->GetYaxis()->SetRangeUser(0, 2000);
        
        hNExclusiveEventsPerLumi_Pull->Rebin(5);
        hNExclusiveEventsPerLumi_Pull->GetXaxis()->SetRangeUser(-16, 16);
        hNExclusiveEventsPerLumi_Pull->SetMarkerSize(1.9);
        hNExclusiveEventsPerLumi_Pull->SetMarkerStyle(24);
        hNExclusiveEventsPerLumi_Pull->SetMarkerColor(kBlack);
        hNExclusiveEventsPerLumi_Pull->SetLineColor(kBlack);
        hNExclusiveEventsPerLumi_Pull->Fit(gausPull_uncorrected, "QNI");
        hNExclusiveEventsPerLumi_Pull->Draw("PE SAME");
        
        gausPull->Draw("SAME");
        gausPull_uncorrected->Draw("SAME");
        hNExclusiveEventsWeightedPerLumi_Pull->Draw("PE SAME");
        
        TLatex lSTAR; lSTAR.SetNDC(); lSTAR.SetTextFont(42);  lSTAR.SetTextSize(.05);
//         lSTAR.DrawLatex(0.22, 0.86, "ATLAS");
//         lSTAR.DrawLatex(0.233, 0.82, "#scale[0.6]{#font[42]{Internal}}");
        lSTAR.DrawLatex(0.16, 0.9, "#sqrt{s} = 13 TeV   #beta* = 90 m");
        lSTAR.DrawLatex(0.16, 0.84, "p+p #rightarrow p'+X+p'");
        
        TLatex l; l.SetNDC(); l.SetTextFont(42); l.SetTextSize(.05);
        l.SetTextColor(kRed);
        l.DrawLatex(0.63, 0.85, Form("#mu = %.2f #pm %.2f", gausPull->GetParameter(1), gausPull->GetParError(1)));
        l.DrawLatex(0.63, 0.80, Form("#sigma = %.2f #pm %.2f", gausPull->GetParameter(2), gausPull->GetParError(2)));
        l.SetTextColor(kBlue);
        l.DrawLatex(0.63, 0.72, Form("#mu = %.2f #pm %.2f", gausPull_uncorrected->GetParameter(1), gausPull_uncorrected->GetParError(1)));
        l.DrawLatex(0.63, 0.67, Form("#sigma = %.2f #pm %.2f", gausPull_uncorrected->GetParameter(2), gausPull_uncorrected->GetParError(2)));
        
        TLegend *legend = new TLegend(0.16, 0.5, 0.5, 0.75);
        legend->SetTextSize(0.04);
        legend->SetBorderSize(0);
        legend->SetFillColor(-1);
        legend->SetFillStyle(-1);
        legend->AddEntry( hNExclusiveEventsWeightedPerLumi_Pull, "Data (corr.)", "PEL" );
        legend->AddEntry( gausPull, "Fit (corr.)", "L" );
        legend->AddEntry( hNExclusiveEventsPerLumi_Pull, "Data (uncorr.)", "PEL" );
        legend->AddEntry( gausPull_uncorrected, "Fit (uncorr.)", "L" );
        legend->Draw();
        
        
        TLatex l2; l2.SetTextFont(42); l2.SetTextSize(.03); l2.SetTextAngle(90); //l2.SetTextColor(kGreen+2);
        l2.DrawLatex(hNExclusiveEventsPerLumi_Pull->GetXaxis()->GetBinUpEdge(0), 10+hNExclusiveEventsPerLumi_Pull->GetBinContent(0), "Underflow" );
        l2.DrawLatex(hNExclusiveEventsPerLumi_Pull->GetXaxis()->GetBinUpEdge(hNExclusiveEventsPerLumi_Pull->GetNbinsX()+1), 10+hNExclusiveEventsPerLumi_Pull->GetBinContent(hNExclusiveEventsPerLumi_Pull->GetNbinsX()+1), "Overflow" );
        
        
        gPad->RedrawAxis();
        
        c->Print( "Pull_PerLB_"+input_name[in]+".pdf");
        
    }
    
    
}


void CrossSectionAnalysis::transformToCrossSection(TH1* hist) const{
    hist->Scale(1./totalIntegratedLuminosity, "width");
}



SystematicsOutput CrossSectionAnalysis::calculateSystematicErrors( TH1F* const referenceHist, TH1F** const checkHist, TH1F* const bkgdErrorHistogram ) const{
  SystematicsOutput output;
  for(int i=0; i<Util::nSystChecks; ++i){
    output.mSystCheckHistVec.push_back( checkHist[i] );
    if( referenceHist->GetNbinsX() != checkHist[i]->GetNbinsX() ){
      std::cerr << "Error in CrossSectionAnalysis::calculateSystematicErrors()" << std::endl;
//       std::cerr << "Ommited systematic check:  " << mSystCheckName[i] << std::endl;
      continue;
    }
    for(int bin=1; bin<=referenceHist->GetNbinsX(); ++bin){
      if( i==Util::NONEXCL_BKGD_DOWN || i==Util::NONEXCL_BKGD_UP ){
        if( bkgdErrorHistogram ){
          if( !(referenceHist->GetBinContent(bin)>0) ){
            output.mError[i].push_back( 0.0 );
          } else{
            output.mError[i].push_back( (i==Util::NONEXCL_BKGD_DOWN ? 1 : -1) * fabs(bkgdErrorHistogram->GetBinContent(bin)) );
            checkHist[i]->SetBinContent(bin, referenceHist->GetBinContent(bin) + (i==Util::NONEXCL_BKGD_DOWN ? 1 : -1) * fabs(bkgdErrorHistogram->GetBinContent(bin)) );
          }
        } else{
          output.mError[i].push_back( 0.0 );
        }
      } else{
        output.mError[i].push_back( checkHist[i]->GetBinContent(bin) - referenceHist->GetBinContent(bin) );
      }
    }
  }
  TH1F *tmpHist = new TH1F( *referenceHist );
  tmpHist->SetName(Form("%s_forSyst", tmpHist->GetName()));
  output.mSystCheckHistVec.push_back( tmpHist );
  
  TH1F *tmpHistZero = new TH1F( *referenceHist );
  tmpHistZero->SetName("tmpHistZero");
  for(int bin=0; bin<=(tmpHistZero->GetNbinsX()+1); ++bin)
      tmpHistZero->SetBinContent(bin, 0.0);
  output.mhSystErrorUp = new TH1F( *referenceHist ); output.mhSystErrorUp->SetName( "SysteErrorUp_"+TString(referenceHist->GetName()) ); output.mhSystErrorUp->Reset("ICESM");
  output.mhSystErrorDown = new TH1F( *referenceHist ); output.mhSystErrorDown->SetName( "SysteErrorDown_"+TString(referenceHist->GetName()) ); output.mhSystErrorDown->Reset("ICESM");
//   output.mhSystErrorUp_TOF = new TH1F( *referenceHist ); output.mhSystErrorUp_TOF->SetName( "SysteErrorUp_TOF_"+TString(referenceHist->GetName()) ); output.mhSystErrorUp_TOF->Reset("ICESM");
//   output.mhSystErrorDown_TOF = new TH1F( *referenceHist ); output.mhSystErrorDown_TOF->SetName( "SysteErrorDown_TOF_"+TString(referenceHist->GetName()) ); output.mhSystErrorDown_TOF->Reset("ICESM");
//   output.mhSystErrorUp_TPC = new TH1F( *referenceHist ); output.mhSystErrorUp_TPC->SetName( "SysteErrorUp_TPC_"+TString(referenceHist->GetName()) ); output.mhSystErrorUp_TPC->Reset("ICESM");
//   output.mhSystErrorDown_TPC = new TH1F( *referenceHist ); output.mhSystErrorDown_TPC->SetName( "SysteErrorDown_TPC_"+TString(referenceHist->GetName()) ); output.mhSystErrorDown_TPC->Reset("ICESM");
//   output.mhSystErrorUp_RP = new TH1F( *referenceHist ); output.mhSystErrorUp_RP->SetName( "SysteErrorUp_RP_"+TString(referenceHist->GetName()) ); output.mhSystErrorUp_RP->Reset("ICESM");
//   output.mhSystErrorDown_RP = new TH1F( *referenceHist ); output.mhSystErrorDown_RP->SetName( "SysteErrorDown_RP_"+TString(referenceHist->GetName()) ); output.mhSystErrorDown_RP->Reset("ICESM");
  output.mhSystErrorUp_Other = new TH1F( *referenceHist ); output.mhSystErrorUp_Other->SetName( "SysteErrorUp_"+TString(referenceHist->GetName()) ); output.mhSystErrorUp_Other->Reset("ICESM");
  output.mhSystErrorDown_Other = new TH1F( *referenceHist ); output.mhSystErrorDown_Other->SetName( "SysteErrorDown_"+TString(referenceHist->GetName()) ); output.mhSystErrorDown_Other->Reset("ICESM");
  
  for(int bin=1; bin<=tmpHistZero->GetNbinsX(); ++bin){
      double errors[Util::nSigns] = {0,0};
//       double errors_Tof[Util::nSigns] = {0,0};
//       double errors_Tpc[Util::nSigns] = {0,0};
//       double errors_TofTpc[Util::nSigns] = {0,0};
//       double errors_Rp[Util::nSigns] = {0,0};
      double errors_Other[Util::nSigns] = {0,0};
      for(int i=0; i<Util::nSystChecks; ++i){
          double err = output.mError[i][bin-1];
          double errPrev = bin>1 ? output.mError[i][bin-2] : 0;
          double errNext = bin<tmpHistZero->GetNbinsX() ? output.mError[i][bin] : 0;
          errors[ err>0 ? Util::PLUS : Util::MINUS ] += err*err;
          
          switch(i){
//               case DELTAZ0CUT_EFF_UP:
//               case DELTAZ0CUT_EFF_DOWN:
//               case VERTEXING_EFF_UP:
//               case VERTEXING_EFF_DOWN:
//               case TPC_RECO_EFF_EMB_SAMPLE_REPRESENTATIVENESS_UP:
//               case TPC_RECO_EFF_EMB_SAMPLE_REPRESENTATIVENESS_DOWN:
//               case TPC_RECO_EFF_EMBEDDING_EFFECT_UP:
//               case TPC_RECO_EFF_EMBEDDING_EFFECT_DOWN:
//               case TPC_QUALITY_TIGHT:
//               case TPC_QUALITY_LOOSE:
//               case TPC_DEAD_MATERIAL_UP:
//               case TPC_DEAD_MATERIAL_DOWN: errors_Tpc[ err>0 ? Util::PLUS : Util::MINUS ] += err*err; break;
//               
//               case TOF_MATCH_EFF_UP:
//               case TOF_MATCH_EFF_DOWN: errors_Tof[ err>0 ? Util::PLUS : Util::MINUS ] += err*err; break;
//               
//               case RP_TRIGG_EFF_UP:
//               case RP_TRIGG_EFF_DOWN:
//               case RP_DEADMAT_UP:
//               case RP_DEADMAT_DOWN:
//               case RP_EFF_UP:
//               case RP_EFF_DOWN: errors_Rp[ err>0 ? Util::PLUS : Util::MINUS ] += err*err; break;
//               
//               case PILEUP_CORRECTION_UP:
//               case PILEUP_CORRECTION_DOWN:
//               case ZVTX_MEAN_UP:
//               case ZVTX_MEAN_DOWN:
//               case ZVTX_SIGMA_UP:
//               case ZVTX_SIGMA_DOWN:
//               case NTOFCLUSTERSCUT_EFF_UP:
//               case NTOFCLUSTERSCUT_EFF_DOWN:
//               case NONEXCL_BKGD_UP:
//               case NONEXCL_BKGD_DOWN: errors_Other[ err>0 ? Util::PLUS : Util::MINUS ] += err*err; break;
              default: errors_Other[ err>0 ? Util::PLUS : Util::MINUS ] += err*err; break;
          };
          
      }
      double yVal = referenceHist->GetBinContent(bin);
      output.mhSystErrorUp->SetBinContent( bin, sqrt( errors[Util::PLUS]/(yVal*yVal)  ) );
      output.mhSystErrorDown->SetBinContent( bin, sqrt( errors[Util::MINUS]/(yVal*yVal)  ) );
//       output.mhSystErrorUp_TOF->SetBinContent( bin, sqrt( errors_Tof[Util::PLUS]/(yVal*yVal)  ) );
//       output.mhSystErrorDown_TOF->SetBinContent( bin, sqrt( errors_Tof[Util::MINUS]/(yVal*yVal)  ) );
//       output.mhSystErrorUp_TPC->SetBinContent( bin, sqrt( errors_Tpc[Util::PLUS]/(yVal*yVal)  ) );
//       output.mhSystErrorDown_TPC->SetBinContent( bin, sqrt( errors_Tpc[Util::MINUS]/(yVal*yVal)  ) );
//       output.mhSystErrorUp_RP->SetBinContent( bin, sqrt( errors_Rp[Util::PLUS]/(yVal*yVal)  ) );
//       output.mhSystErrorDown_RP->SetBinContent( bin, sqrt( errors_Rp[Util::MINUS]/(yVal*yVal)  ) );
      output.mhSystErrorUp_Other->SetBinContent( bin, sqrt( errors_Other[Util::PLUS]/(yVal*yVal)  ) );
      output.mhSystErrorDown_Other->SetBinContent( bin, sqrt( errors_Other[Util::MINUS]/(yVal*yVal)  ) );
  }
  
  return output;
}







void CrossSectionAnalysis::drawFinalResult(TH1F* hMainInput, DrawingOptions & opt, SystematicsOutput & syst) const{
  
  TH1F *hMain = new TH1F( *hMainInput );
  hMain->SetName("FinalResult_"+TString(hMain->GetName()));
  
  TGaxis::SetMaxDigits(3);
  
  TCanvas *c = new TCanvas("cccccc", "cccccc", opt.mCanvasWidth, opt.mCanvasHeight);
//   c->SetFrameFillStyle(0);
//   c->SetFrameLineWidth(2);
//   c->SetFillColor(-1);
  c->SetLeftMargin(opt.mLeftMargin);
  c->SetRightMargin(opt.mRightMargin);
  c->SetTopMargin(opt.mTopMargin);
  c->SetBottomMargin(opt.mBottomMargin);
  
  c->SetLogy(opt.mYlog);
    
  //   systematicErrorsGraph->SetFillColor( 18 );
  //   //systematicErrorsGraph->SetFillStyle(/*3013*//*3345*/3005);
  //   systematicErrorsGraph->SetLineWidth(0);
  //   systematicErrorsGraph->SetLineColor(kWhite);
  
  if(opt.mScale){
    hMain->Scale( opt.mScalingFactor );
  }
  
  hMain->SetMarkerStyle(opt.mMarkerStyle);
  hMain->SetMarkerSize(opt.mMarkerSize);
  hMain->SetMarkerColor(opt.mMarkerColor);
  hMain->SetLineStyle(opt.mLineStyle);
  hMain->SetLineColor(opt.mLineColor);
  hMain->SetLineWidth(opt.mLineWidth);
  
  hMain->GetXaxis()->SetTitle(opt.mXaxisTitle);
  hMain->GetXaxis()->SetTitleSize(opt.mXaxisTitleSize);
  hMain->GetXaxis()->SetTitleOffset(opt.mXaxisTitleOffset);
  hMain->GetXaxis()->SetLabelSize(opt.mXaxisLabelSize);
  hMain->GetXaxis()->SetLabelOffset(opt.mXaxisLabelOffset);
  hMain->GetXaxis()->SetTickLength(opt.mXaxisTickLength);
  hMain->GetXaxis()->SetRangeUser(opt.mXmin, opt.mXmax);
  
  if( opt.mSetDivisionsX )
    hMain->GetXaxis()->SetNdivisions( opt.mXnDivisionA, opt.mXnDivisionB, opt.mXnDivisionC, !opt.mForceDivisionsX );
  
  hMain->GetYaxis()->SetTitleSize(opt.mYaxisTitleSize);
  hMain->GetYaxis()->SetTitle(opt.mYaxisTitle);
  hMain->GetYaxis()->SetTitleOffset(opt.mYaxisTitleOffset);
  hMain->GetYaxis()->SetLabelSize(opt.mYaxisLabelSize);
  hMain->GetYaxis()->SetLabelOffset(opt.mYaxisLabelOffset);
  hMain->GetYaxis()->SetTickLength(opt.mYaxisTickLength);
  hMain->GetYaxis()->SetRangeUser(opt.mYmin, (opt.mScale ? opt.mScalingFactor : 1.0) * opt.mYmax);
  
  if( opt.mSetDivisionsY )
    hMain->GetYaxis()->SetNdivisions( opt.mYnDivisionA, opt.mYnDivisionB, opt.mYnDivisionC, !opt.mForceDivisionsY );
  
  hMain->Draw("PEX0");
  
  double lumiError = Util::luminosityUncertainty;
  const double systErrorBoxWidth = (opt.mMarkerSize * 8.0 * opt.mSystErrorBoxWidthAdjustmentFactor / opt.mCanvasWidth*( 1.0 - opt.mLeftMargin - opt.mRightMargin )) * (hMain->GetXaxis()->GetXmax() - hMain->GetXaxis()->GetXmin());
  
    
  TBox *typicalBox = nullptr;
  if( opt.mXpositionSystErrorBox.size() > 0 ){
    for(unsigned int i=0; i<opt.mXpositionSystErrorBox.size(); ++i){
      int binNumber = hMain->GetXaxis()->FindBin( opt.mXpositionSystErrorBox[i] );
      if( !(hMain->GetBinContent(binNumber)>0) )
        continue;
      
      double xMinSystErrorBox = hMain->GetXaxis()->GetBinCenter(binNumber) - systErrorBoxWidth/2.;
      double xMaxSystErrorBox = hMain->GetXaxis()->GetBinCenter(binNumber) + systErrorBoxWidth/2.;
      double yMinSystErrorBox = hMain->GetBinContent(binNumber) * (1.0 - sqrt(pow(syst.mhSystErrorDown->GetBinContent(binNumber),2) + pow(lumiError/(1.0+lumiError),2)));
      double yMaxSystErrorBox = hMain->GetBinContent(binNumber) * (1.0 + sqrt(pow(syst.mhSystErrorUp->GetBinContent(binNumber),2) + pow(lumiError/(1.0-lumiError),2)));
      TBox *b = new TBox( xMinSystErrorBox, yMinSystErrorBox, xMaxSystErrorBox, yMaxSystErrorBox );
      b->SetFillColor(opt.mSystErrorTotalBoxColor);
      b->SetLineWidth(0);
      b->Draw("l");
      
      yMinSystErrorBox = hMain->GetBinContent(binNumber) * (1.0 - syst.mhSystErrorDown->GetBinContent(binNumber));
      yMaxSystErrorBox = hMain->GetBinContent(binNumber) * (1.0 + syst.mhSystErrorUp->GetBinContent(binNumber));
      b = new TBox( xMinSystErrorBox, yMinSystErrorBox, xMaxSystErrorBox, yMaxSystErrorBox );
      b->SetFillColor(opt.mSystErrorBoxColor);
      b->SetLineWidth(0);
      b->Draw("l");
      
      if( !typicalBox ){
        typicalBox = new TBox( xMinSystErrorBox, yMinSystErrorBox, xMaxSystErrorBox, yMaxSystErrorBox );
        typicalBox->SetFillColor(opt.mSystErrorTotalBoxColor);
        typicalBox->SetLineColor(opt.mSystErrorBoxColor);
        typicalBox->SetLineWidth(8);
      }
    }
  }
  
  for(unsigned int i=0; i<opt.mHistTH1F.size(); ++i){
    if(opt.mScale){
      opt.mHistTH1F[i]->Scale( opt.mScalingFactor );
    }
    opt.mHistTH1F[i]->Draw( opt.mHistTH1F_DrawingOpt[i] );
  }
  
  
  TLegend *legend = new TLegend( opt.mLegendX, opt.mLegendY, opt.mLegendX+opt.mLegendXwidth, opt.mLegendY+opt.mLegendYwidth );
  legend->SetNColumns(opt.mNColumns);
  legend->SetBorderSize(0);
  legend->SetFillStyle(0);
  legend->SetTextSize(opt.mLegendTextSize);
  legend->SetTextFont(42);
  legend->AddEntry(hMain, "Data", "pe");
//   legend->AddEntry(hMain, "Stat. uncertainty", "el");
  if(typicalBox)
    legend->AddEntry(typicalBox, "Syst. uncertainty", "fl");
  for(unsigned int i=0; i<opt.mObjForLegend.size(); ++i)
    legend->AddEntry( opt.mObjForLegend[i], opt.mDesriptionForLegend[i], opt.mDrawTypeForLegend[i]);
  legend->SetMargin(mSpecialLegendMarginFactor * 0.3 * opt.mLegendXwidth * legend->GetNRows() / legend->GetNColumns() );
  if( !mSpecialLabelRatio || mEnableLegend )
    legend->Draw();
  
  
  if( opt.mTextToDraw.size() > 0 ){
    for(unsigned int i=0; i<opt.mTextToDraw.size(); ++i){
      opt.mTextToDraw[i].SetNDC(kTRUE);
      opt.mTextToDraw[i].Draw();
    }
  }
  
  //   TASImage *img = new TASImage("miscellaneous/STAR_logo.pdf");
  //   //   TASImage *img = new TASImage("miscellaneous/STAR_logo.eps");
  //   img->SetImageQuality(TAttImage::kImgBest);
  
  
  hMain->DrawCopy("PEX0 SAME", "");
  gPad->RedrawAxis();
  
  
  if( opt.mInsert ){
    TPad *insert = new TPad("insert", "insert", opt.mX1Insert, opt.mY1Insert, opt.mX2Insert, opt.mY2Insert);
    insert->SetBottomMargin(0); // Upper and lower plot are joined
    insert->SetFrameFillStyle(0);
    insert->SetFrameLineWidth(2);
    insert->SetFillColor(-1);
    insert->Draw();             // Draw the upper pad: insert
    insert->cd();               // insert becomes the current pad
    insert->SetLeftMargin(opt.mLeftMargin);
    insert->SetRightMargin(opt.mRightMargin);
    insert->SetTopMargin(opt.mTopMargin);
    insert->SetBottomMargin(opt.mBottomMargin);
    if( opt.mYlogInsert )
        insert->SetLogy();
    
    TH1F *hMainCopy = new TH1F( *hMain );
    hMainCopy->GetXaxis()->SetTitle("");
    hMainCopy->GetXaxis()->SetLabelSize( 0.9*hMainCopy->GetXaxis()->GetLabelSize() * ( 1. - opt.mLeftMargin - opt.mRightMargin) / (opt.mX2Insert - opt.mX1Insert) );
    hMainCopy->GetYaxis()->SetLabelSize( 0.9*hMainCopy->GetYaxis()->GetLabelSize() * ( 1. - opt.mLeftMargin - opt.mRightMargin) / (opt.mX2Insert - opt.mX1Insert) );
    hMainCopy->GetYaxis()->SetTitle("");
    hMainCopy->GetXaxis()->SetRange( hMainCopy->FindBin(opt.mXMinInstert ), hMainCopy->FindBin(opt.mXMaxInstert ) );
    int binMaxY;
    binMaxY = hMainCopy->GetMaximumBin();
    hMainCopy->GetYaxis()->SetRangeUser(opt.mYMinInstert, opt.mYMaxInstert<0 ? (1.1*hMainCopy->GetBinContent( binMaxY )) : opt.mYMaxInstert);
    
    int n1 = hMainCopy->GetYaxis()->GetNdivisions()%100;
    int n2 = hMainCopy->GetYaxis()->GetNdivisions()%10000 - n1;
    int n3 = hMainCopy->GetYaxis()->GetNdivisions() - 100*n2 - n1;
    
    hMainCopy->GetYaxis()->SetNdivisions( n1/2, 2, 1 );
    hMainCopy->GetXaxis()->SetRangeUser(opt.mXMinInstert, opt.mXMaxInstert);
    hMainCopy->GetXaxis()->SetNdivisions(10,5,1);
    
    hMainCopy->GetXaxis()->SetTickLength(0.04);
    hMainCopy->GetYaxis()->SetTickLength(0.02);
    
    hMainCopy->Draw("PEX0");
    for(unsigned int i=0; i<opt.mHistTH1F.size(); ++i){
//       if(opt.mScale){
//         opt.mHistTH1F[i]->Scale( opt.mScalingFactor );
//       }
      opt.mHistTH1F[i]->DrawCopy( opt.mHistTH1F_DrawingOpt[i], "" );
    }
    
    if( opt.mXpositionSystErrorBox.size() > 0 ){
      for(unsigned int i=0; i<opt.mXpositionSystErrorBox.size(); ++i){
        int binNumber = hMainCopy->GetXaxis()->FindBin( opt.mXpositionSystErrorBox[i] );
        if( !(hMainCopy->GetBinContent(binNumber)>0) || opt.mXpositionSystErrorBox[i]>opt.mXMaxInstert || opt.mXpositionSystErrorBox[i]<opt.mXMinInstert )
          continue;
        
        double xMinSystErrorBox = hMainCopy->GetXaxis()->GetBinCenter(binNumber) - systErrorBoxWidth/2.;
        double xMaxSystErrorBox = hMainCopy->GetXaxis()->GetBinCenter(binNumber) + systErrorBoxWidth/2.;
        double yMinSystErrorBox = hMainCopy->GetBinContent(binNumber) * (1.0 - sqrt(pow(syst.mhSystErrorDown->GetBinContent(binNumber),2) + pow(lumiError/(1.0+lumiError),2)));
        double yMaxSystErrorBox = hMainCopy->GetBinContent(binNumber) * (1.0 + sqrt(pow(syst.mhSystErrorUp->GetBinContent(binNumber),2) + pow(lumiError/(1.0-lumiError),2)));
        TBox *b = new TBox( xMinSystErrorBox, yMinSystErrorBox, xMaxSystErrorBox, yMaxSystErrorBox );
        b->SetFillColor(opt.mSystErrorTotalBoxColor);
        b->SetLineWidth(0);
        b->Draw("l");
        
        yMinSystErrorBox = hMainCopy->GetBinContent(binNumber) * (1.0 - syst.mhSystErrorDown->GetBinContent(binNumber));
        yMaxSystErrorBox = hMainCopy->GetBinContent(binNumber) * (1.0 + syst.mhSystErrorUp->GetBinContent(binNumber));
        b = new TBox( xMinSystErrorBox, yMinSystErrorBox, xMaxSystErrorBox, yMaxSystErrorBox );
        b->SetFillColor(opt.mSystErrorBoxColor);
        b->SetLineWidth(0);
        b->Draw("l");
      }
    }
    
    hMainCopy->DrawCopy("PEX0 SAME", "");
    gPad->RedrawAxis();
  }
  
  c->cd();
  c->Modified();
  c->Update();
  
  c->Print( TString("PDF/")+opt.mPdfName+".pdf", "EmbedFonts");
//   c->Print( TString("PDF/")+opt.mPdfName+".png");
  
  
  const double ratioFactor = 0.25;
  TCanvas *c2 = new TCanvas("cccccc2", "cccccc2", opt.mCanvasWidth, (ratioFactor + 1.0)*opt.mCanvasHeight);
  //   c->SetFrameFillStyle(0);
  //   c->SetFrameLineWidth(2);
  //   c->SetFillColor(-1);
  c2->SetLeftMargin(0);
  c2->SetRightMargin(0);
  c2->SetTopMargin(0);
  c2->SetBottomMargin(0);
  
  // Upper plot will be in pad1
  TPad *pad1 = new TPad("pad1", "pad1", 0, ratioFactor, 1.0, 1.0);
  pad1->SetBottomMargin(0); // Upper and lower plot are joined
  pad1->SetFrameFillStyle(0);
  pad1->SetFillColor(-1);
  pad1->Draw();             // Draw the upper pad: pad1
  pad1->cd();               // pad1 becomes the current pad
  gPad->SetLeftMargin(0);
  gPad->SetRightMargin(0);
  gPad->SetTopMargin(0);
  gPad->SetBottomMargin(0);
  
  TH1F *ratio_Data_to_Data = new TH1F( *hMain );
  hMain->GetXaxis()->SetLabelSize( 0 );
  hMain->GetXaxis()->SetTitle("");
  c->Modified();
  c->Update();
  
  c->DrawClonePad();

  const double modifiedRatioFactor = ratioFactor + 0.54*opt.mBottomMargin;
  
  c2->cd();          // Go back to the main canvas before defining pad2
  TPad *pad2 = new TPad("pad2", "pad2", 0, 0, 1.0, ratioFactor + 0.65*opt.mBottomMargin);
  pad2->SetFrameFillStyle(0);
  pad2->SetFillColor(-1);
//   gStyle->SetGridColor(kGray);
  pad2->SetGridy(kTRUE);
  pad2->Draw();
  pad2->cd();       // pad2 becomes the current pad
  gPad->SetLeftMargin(opt.mLeftMargin);
  gPad->SetRightMargin(opt.mRightMargin);
  gPad->SetTopMargin(0.005);
  gPad->SetBottomMargin(opt.mBottomMargin * (1.0-modifiedRatioFactor)/modifiedRatioFactor);
  
  
  std::vector< TH1F* > histMC_ratio;
  for(unsigned int fileId=0; fileId<opt.mHistTH1F.size(); ++fileId) 
      histMC_ratio.push_back( new TH1F(*opt.mHistTH1F[fileId]) );
  
  for(int i=1; i<=hMain->GetNbinsX(); ++i){
    if( ratio_Data_to_Data->GetBinError(i)>0 ){
      ratio_Data_to_Data->SetBinError( i, hMain->GetBinError(i)/hMain->GetBinContent(i) );
      ratio_Data_to_Data->SetBinContent( i, 1.0 );
    }
  }
  
  for(unsigned int j=0; j<histMC_ratio.size(); ++j){
    histMC_ratio[j]->Scale( hMain->Integral(0, -1, "width") / histMC_ratio[j]->Integral(0, -1, "width") );
    histMC_ratio[j]->Divide( hMain );
  }
  
  ratio_Data_to_Data->GetYaxis()->SetRangeUser(0.2, 1.8);
  ratio_Data_to_Data->GetYaxis()->SetTitle(mSpecialLabelRatio ? "MC(norm.)/Data       " : "MC(norm.)/Data");
  ratio_Data_to_Data->GetYaxis()->SetTitleOffset( mSpecialLabelRatio ? (mSpecialLabelOffsetFactor * ratio_Data_to_Data->GetYaxis()->GetTitleOffset()*modifiedRatioFactor/(1.0-modifiedRatioFactor)) : 0.6  );
  ratio_Data_to_Data->GetYaxis()->CenterTitle();
  ratio_Data_to_Data->GetYaxis()->SetNdivisions(4,4,1);
  ratio_Data_to_Data->GetYaxis()->SetTitleSize( /*1.6**/ratio_Data_to_Data->GetYaxis()->GetTitleSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
  ratio_Data_to_Data->GetYaxis()->SetLabelSize( /*1.2**/ratio_Data_to_Data->GetYaxis()->GetLabelSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
  ratio_Data_to_Data->GetXaxis()->SetLabelOffset( 0.01 );
  ratio_Data_to_Data->GetYaxis()->SetTickLength( ratio_Data_to_Data->GetYaxis()->GetTickLength() );
  ratio_Data_to_Data->GetXaxis()->SetTitleSize( /*1.4**/ratio_Data_to_Data->GetXaxis()->GetTitleSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
  ratio_Data_to_Data->GetXaxis()->SetLabelSize( /*1.4**/ratio_Data_to_Data->GetXaxis()->GetLabelSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
  ratio_Data_to_Data->GetXaxis()->SetTickLength( (1.0-modifiedRatioFactor)/modifiedRatioFactor* 1.2 *ratio_Data_to_Data->GetXaxis()->GetTickLength() );
  
  ratio_Data_to_Data->Draw("AXIS");
  ratio_Data_to_Data->Draw("AXIG SAME");
  
  
  TLine *line = new TLine(opt.mXmin, 1.0, opt.mXmax, 1.0);
  line->SetLineWidth(2);
  line->SetLineStyle(7);
  line->SetLineColor(kBlack);
  line->Draw();
  
  for(unsigned int i=0; i<histMC_ratio.size(); ++i){
    histMC_ratio[i]->SetMarkerColor( histMC_ratio[i]->GetLineColor() );
    histMC_ratio[i]->SetMarkerStyle( opt.mMarkerStyle );
    histMC_ratio[i]->SetMarkerSize( opt.mMarkerSize );
    histMC_ratio[i]->Draw( /*opt.mHistTH1F_DrawingOpt[i]*/"PE0 ][ SAME" );
  }
  
//   ratio_Data_to_Data->DrawCopy("PEX0 SAME", "");
  gPad->RedrawAxis();
  
  c2->Print( TString("PDF/")+"Ratio_"+opt.mPdfName+".pdf", "EmbedFonts");
//   c2->Print( TString("PDF/")+"Ratio_"+opt.mPdfName+".png");
  
  
  //--
  
  
  c->Clear();
  
  c->SetLeftMargin(opt.mLeftMargin);
  c->SetRightMargin(opt.mRightMargin);
  c->SetTopMargin(opt.mTopMargin);
  c->SetBottomMargin(0.4);
  
  legend = new TLegend(0.0, 0.02, 1.0, 0.3);
  legend->SetBorderSize(0);
  legend->SetFillStyle(0);
  legend->SetMargin(0.18);
  legend->SetNColumns(3);
       
  TBox *b, *b2;
  TH1F *mainRatio;
  
  double sumUp = 0;
  double sumDown = 0;
  int nBinsHMain = 0;
  for(int i=1; i<=hMain->GetNbinsX(); ++i){
    if( hMain->GetBinContent(i) == 0 || hMain->GetXaxis()->GetBinCenter(i)<opt.mXmin || hMain->GetXaxis()->GetBinCenter(i)>opt.mXmax ) continue;
    ++nBinsHMain;
    sumDown += /*1./*/(1.0 - sqrt(pow(syst.mhSystErrorDown->GetBinContent(i),2) + pow(lumiError/(1.0+lumiError),2)));
    sumUp += /*1./*/(1.0 + sqrt(pow(syst.mhSystErrorUp->GetBinContent(i),2) + pow(lumiError/(1.0-lumiError),2)));
  }
  
  double averageSystUp = sumUp / nBinsHMain;
  double averageSystDown = sumDown / nBinsHMain;
  
  
//   const double minY = averageSystDown > 0.9 ? 0.95 : (0.95 - ((0.95-averageSystDown)/0.05+1)*0.05 );
//   const double maxY = averageSystUp < 1.11 ? 1.11 : (((averageSystUp-1.05)/0.05+1)*0.1 + 1.11);
  
  const double minY = averageSystDown > 0.9 ? 0.92 : (0.95 - ((0.95-averageSystDown)/0.05+1)*0.05 );
  const double maxY = averageSystUp < 1.11 ? 1.16 : (((averageSystUp-1.05)/0.05+1)*0.1 + 1.11);
  
  int nColumn = 0;
  for(int i=0; i<Util::nSystChecks; ++i){
    TH1F* ratio = new TH1F( *hMain );
    if(i==0) mainRatio = ratio;
    ratio->SetName("SystCheckRatio_"+TString(hMain->GetName())+"_"+mSystCheckName[i]);
    ratio->Divide( syst.mSystCheckHistVec[i], syst.mSystCheckHistVec[Util::nSystChecks] );
    
    
    for(int bin=1; bin<=ratio->GetNbinsX(); ++bin ){
      if( !(ratio->GetBinContent(bin) > 0) )
        ratio->SetBinContent(bin, 1.0); // for better drawing
    }
    
    int lineColor = i/2+2;
    if( lineColor > 10 ){
      switch(lineColor){
        case 11: lineColor = kOrange-7; break;
        case 12: lineColor = 37; break;
        case 13: lineColor = 30; break;
        case 14: lineColor = 49; break;
        case 15: lineColor = 46; break;
        case 16: lineColor = kSpring+10; break;
      }
    }
    
    ratio->SetLineWidth(4/*opt.mLineWidth*/);
    ratio->SetLineColor( lineColor );
//     ratio->SetLineStyle( i%2==0 ? 1 : 7 ); // ALERT this was used to present separately up and down syst. check effect
    
    if(i==0){
      ratio->GetXaxis()->SetTitle(opt.mXaxisTitle);
      ratio->GetXaxis()->SetTitleSize(opt.mXaxisTitleSize);
      ratio->GetXaxis()->SetTitleOffset(opt.mXaxisTitleOffset);
      ratio->GetXaxis()->SetLabelSize(opt.mXaxisLabelSize);
      ratio->GetXaxis()->SetLabelOffset(opt.mXaxisLabelOffset);
      ratio->GetXaxis()->SetTickLength(opt.mXaxisTickLength);
      ratio->GetXaxis()->SetRangeUser(opt.mXmin, opt.mXmax);
      
      if( opt.mSetDivisionsX )
        ratio->GetXaxis()->SetNdivisions( opt.mXnDivisionA, opt.mXnDivisionB, opt.mXnDivisionC, !opt.mForceDivisionsX );
      
      ratio->GetYaxis()->SetTitleSize(opt.mYaxisTitleSize);
      ratio->GetYaxis()->SetTitle(opt.mYaxisTitle);
      ratio->GetYaxis()->SetTitleOffset(opt.mYaxisTitleOffset+0.06);
      ratio->GetYaxis()->SetLabelSize(opt.mYaxisLabelSize);
      ratio->GetYaxis()->SetLabelOffset(opt.mYaxisLabelOffset);
      ratio->GetYaxis()->SetTickLength(opt.mYaxisTickLength);
      
      if( opt.mSetDivisionsY )
        ratio->GetYaxis()->SetNdivisions( opt.mYnDivisionA, opt.mYnDivisionB, opt.mYnDivisionC, !opt.mForceDivisionsY );
      
      ratio->Draw("AXIS");
      
      for(int j=1; j<=hMain->GetNbinsX(); ++j){
        if( hMain->GetBinContent(j) == 0 || hMain->GetXaxis()->GetBinCenter(j)<opt.mXmin || hMain->GetXaxis()->GetBinCenter(j)>opt.mXmax ) continue;
        
        double yMinSystErrorBox = /*1./*/(1.0 - sqrt(pow(syst.mhSystErrorDown->GetBinContent(j),2) + pow(lumiError/(1.0+lumiError),2)));
        double yMaxSystErrorBox = /*1./*/(1.0 + sqrt(pow(syst.mhSystErrorUp->GetBinContent(j),2) + pow(lumiError/(1.0-lumiError),2)));
        double xMinSystErrorBox = hMain->GetXaxis()->GetBinLowEdge(j);
        double xMaxSystErrorBox = hMain->GetXaxis()->GetBinUpEdge(j);
        if( yMinSystErrorBox<minY ) yMinSystErrorBox = minY;
        if( yMaxSystErrorBox>maxY ) yMaxSystErrorBox = maxY;
        b = new TBox( xMinSystErrorBox, yMinSystErrorBox, xMaxSystErrorBox, yMaxSystErrorBox );
        b->SetFillColor(opt.mSystErrorTotalBoxColor);
        b->SetLineWidth(0);
        b->Draw("l");
        
        yMinSystErrorBox = /*1./*/(1.0 - syst.mhSystErrorDown->GetBinContent(j));
        yMaxSystErrorBox = /*1./*/(1.0 + syst.mhSystErrorUp->GetBinContent(j));
        xMinSystErrorBox = hMain->GetXaxis()->GetBinLowEdge(j);
        xMaxSystErrorBox = hMain->GetXaxis()->GetBinUpEdge(j);
        if( yMinSystErrorBox<minY ) yMinSystErrorBox = minY;
        if( yMaxSystErrorBox>maxY ) yMaxSystErrorBox = maxY;
        b2 = new TBox( xMinSystErrorBox, yMinSystErrorBox, xMaxSystErrorBox, yMaxSystErrorBox );
        b2->SetFillColor(opt.mSystErrorBoxColor);
        b2->SetLineWidth(0);
        b2->Draw("l");
      }
      
    }
    
    
    ratio->Draw("HIST ][ SAME");
    ratio->GetYaxis()->SetRangeUser(minY, maxY);
    ratio->GetYaxis()->SetTitle("Ratio to nominal");
    ratio->GetYaxis()->CenterTitle();
    
    
    
    //     legend->AddEntry(ratio, mSystCheckShortName[i], "l"); //ALERT this was used to present separately up and down syst. check effect
    
//     if( i%2==1 ){
//       if( i<(nSystChecks-2) ){
//         TObject *emptyPtr = nullptr;
//         legend->AddEntry(emptyPtr, "", "");
//       } else
//         if( i==(nSystChecks-1) ){
//           legend->AddEntry(b2, "Total (w/o lumi.)", "f");
//         } 
//     }
    
    TBox* boxForLegend = new TBox();
    boxForLegend->SetFillColor( opt.mSystErrorBoxColor );
    boxForLegend->SetLineColor( lineColor );
    boxForLegend->SetLineWidth( 5 );
    
    
    if( nColumn%3==2 ){
      if( i==(Util::nSystChecks-3) ){
        legend->AddEntry(b2, "Total (w/o lumi.)", "f");
        ++nColumn;
      } else{
        TObject *emptyPtr = nullptr;
        legend->AddEntry(emptyPtr, "", "");
        ++nColumn;
      }
    } else{
      if( i%2==0 ){
        legend->AddEntry(boxForLegend, mSystCheckShortName[i], "fl");
        ++nColumn;
      }
    }
    
    
    
  }
  
  
  TLine *lumiDownLine;
  lumiDownLine = new TLine(mainRatio->GetXaxis()->GetXmin(), 1./(1.0+lumiError), mainRatio->GetXaxis()->GetXmax(), 1./(1.0+lumiError));
  lumiDownLine->SetLineWidth(4/*opt.mLineWidth*/);
  lumiDownLine->SetLineStyle(7);
  lumiDownLine->SetLineColor(kTeal-9);
  lumiDownLine->Draw();
  
//   for( int i=1; i<=mainRatio->GetNbinsX(); ++i ){
//     if( mainRatio->GetBinError(i) > 0 && mainRatio->GetXaxis()->GetBinLowEdge(i) >= opt.mXmin && mainRatio->GetXaxis()->GetBinUpEdge(i) <= opt.mXmax ){
//       lumiDownLine = new TLine(mainRatio->GetXaxis()->GetBinLowEdge(i), 1./(1.0+lumiError), mainRatio->GetXaxis()->GetBinUpEdge(i), 1./(1.0+lumiError));
//       lumiDownLine->SetLineWidth(4/*opt.mLineWidth*/);
//       lumiDownLine->SetLineStyle(2);
//       lumiDownLine->SetLineColor(kTeal-9);
//       lumiDownLine->Draw();
//     }
//   }
  TBox* boxForLegend = new TBox();
  boxForLegend->SetFillColor( opt.mSystErrorBoxColor );
  boxForLegend->SetLineColor( kTeal-9 );
  boxForLegend->SetLineStyle( 7 );
  boxForLegend->SetLineWidth( 5 );
  legend->AddEntry(boxForLegend, "#DeltaLuminosity", "fl");
  ++nColumn;
  
  
  TLine *lumiUpLine;
  lumiUpLine = new TLine(mainRatio->GetXaxis()->GetXmin(), 1./(1.0-lumiError), mainRatio->GetXaxis()->GetXmax(), 1./(1.0-lumiError));
  lumiUpLine->SetLineWidth(4/*opt.mLineWidth*/);
  lumiUpLine->SetLineStyle(7);
  lumiUpLine->SetLineColor(kTeal-9);
  lumiUpLine->Draw();
  
//   for( int i=1; i<=mainRatio->GetNbinsX(); ++i ){
//     if( mainRatio->GetBinError(i) > 0 && mainRatio->GetXaxis()->GetBinLowEdge(i) >= opt.mXmin && mainRatio->GetXaxis()->GetBinUpEdge(i) <= opt.mXmax ){
//       lumiUpLine = new TLine(mainRatio->GetXaxis()->GetBinLowEdge(i), 1./(1.0-lumiError), mainRatio->GetXaxis()->GetBinUpEdge(i), 1./(1.0-lumiError));
//       lumiUpLine->SetLineWidth(4/*opt.mLineWidth*/);
//       lumiUpLine->SetLineStyle(2);
//       lumiUpLine->SetLineColor(kTeal-9);
//       lumiUpLine->Draw();
//     }
//   }
//   legend->AddEntry(lumiUpLine, "Luminosity#downarrow", "l");
  
  while( nColumn%3!=2 ){
    TObject *emptyPtr = nullptr;
    legend->AddEntry(emptyPtr, "", "");
    ++nColumn;
  }
  legend->AddEntry(b, "Total (w/ lumi.)", "f"); 
  
  legend->SetTextSize(opt.mLegendTextSize);
  legend->Draw();
  
  if( opt.mTextToDraw.size() > 0 ){
    for(unsigned int i=0; i<opt.mTextToDraw.size(); ++i){
      opt.mTextToDraw[i].SetNDC(kTRUE);
      opt.mTextToDraw[i].Draw();
    }
  }
  
  line = new TLine(opt.mXmin, 1.0, opt.mXmax, 1.0);
  line->SetLineWidth(3);
  line->SetLineStyle(1);
  line->SetLineColor(kBlack);
  line->Draw();
  gPad->RedrawAxis();
  
  c->Print( TString("PDF/")+opt.mPdfName+"_Systematics.pdf", "EmbedFonts");
  
  
  mainRatio->GetXaxis()->SetTitleSize( mainRatio->GetXaxis()->GetTitleSize() * opt.mCanvasWidth );
  mainRatio->GetYaxis()->SetTitleSize( mainRatio->GetYaxis()->GetTitleSize() * opt.mCanvasWidth );
  mainRatio->GetXaxis()->SetLabelSize( mainRatio->GetXaxis()->GetLabelSize() * opt.mCanvasWidth );
  mainRatio->GetYaxis()->SetLabelSize( mainRatio->GetYaxis()->GetLabelSize() * opt.mCanvasWidth );
  
  mainRatio->GetXaxis()->SetTitleFont( 10*(mainRatio->GetXaxis()->GetTitleFont()/10)+3 );
  mainRatio->GetYaxis()->SetTitleFont( 10*(mainRatio->GetYaxis()->GetTitleFont()/10)+3 );
  mainRatio->GetXaxis()->SetLabelFont( 10*(mainRatio->GetXaxis()->GetLabelFont()/10)+3 );
  mainRatio->GetYaxis()->SetLabelFont( 10*(mainRatio->GetYaxis()->GetLabelFont()/10)+3 );
  
  mainRatio->GetYaxis()->SetTitleOffset( (1.0-0.4+opt.mBottomMargin + 0.05)* opt.mYaxisTitleOffset);
  
  if( opt.mTextToDraw.size() > 0 ){
    for(unsigned int i=0; i<opt.mTextToDraw.size(); ++i){
      opt.mTextToDraw[i].SetX( 0.98*opt.mTextToDraw[i].GetX() );
      opt.mTextToDraw[i].SetY( opt.mTextToDraw[i].GetY() - 0.3*(1.0-opt.mTextToDraw[i].GetY()) );
      opt.mTextToDraw[i].SetTextSize( opt.mTextToDraw[i].GetTextSize() * opt.mCanvasWidth  );
      opt.mTextToDraw[i].SetTextFont( 10*(opt.mTextToDraw[i].GetTextFont()/10)+3 );
    }
  }
  
  c->SetFixedAspectRatio(kFALSE);
  c->SetCanvasSize( opt.mCanvasWidth, (1.0-0.4+opt.mBottomMargin + 0.05)*opt.mCanvasHeight );
  c->GetListOfPrimitives()->Remove(legend);
  c->SetBottomMargin( opt.mBottomMargin );
  c->Modified();
  c->Update();
  c->Print( TString("PDF/")+opt.mPdfName+"_Systematics2.pdf", "EmbedFonts");
  
  
  delete c;
  delete c2;
}




Double_t exponent1D(Double_t *x, Double_t *par){
  TF12 f12("f1", fExponent2D, x[0], "X");
  return f12.Integral(0.05, maxMandT, 1e-5);
}




void CrossSectionAnalysis::getListOfAnalysedRuns(){
    for(int i=1; i<=mhNEventsVsLumiBlockVsRunNumber->GetNbinsX(); ++i){
        TH1D *dNdLumiBlock = mhNEventsVsLumiBlockVsRunNumber->ProjectionY("py", i, i);
        int runNum = static_cast<int>( mhNEventsVsLumiBlockVsRunNumber->GetXaxis()->GetBinCenter(i) );
        if( dNdLumiBlock->GetEntries() > 0 )
            runNumber.push_back( runNum );
        delete dNdLumiBlock;
        
        for(int j=1; j<=mhNEventsVsLumiBlockVsRunNumber->GetNbinsY(); ++j){
            if( mhNEventsVsLumiBlockVsRunNumber->GetBinContent(i,j) > 0 )
                analysedLumiBlockVector_RunMap[runNum].push_back( static_cast<int>( mhNEventsVsLumiBlockVsRunNumber->GetYaxis()->GetBinCenter(j) ) );
        }
    }
}

void CrossSectionAnalysis::getTotalIntegratedLuminosity(){
    double integral = 0.0;
    map<int, vector<int> >::iterator it;
    for( it = analysedLumiBlockVector_RunMap.begin(); it != analysedLumiBlockVector_RunMap.end(); it++ ){
        int runNum = it->first;
        for(unsigned int i=0; i<it->second.size(); ++i){
            integral += lumiPerLumiBlockVector_RunMap[runNum][it->second[i]-1] * lumiblockDurationVector_RunMap[runNum][it->second[i]-1]; // mub^-1
            if( !isPhysicsLumiBlockVector_RunMap[runNum][it->second[i]-1] )
                cout << Form("ERROR in CrossSectionAnalysis::getTotalIntegratedLuminosity(): LumiBlock %d in run %d was not marked as \"PHYSICS\", but was analysed and contributes to cross sections!", runNum, it->second[i]) << endl;
        }
    }
    totalIntegratedLuminosity = integral;
}

void CrossSectionAnalysis::readLumiFiles(){
    for(unsigned int i=0; i<runNumber.size(); ++i){
        string line;
        ifstream lumiFile( Form("offlineLumiInfo/%d.dat", runNumber[i]) );
        if (lumiFile.is_open()){
            while ( getline(lumiFile, line) ){
                string val2, val5;
                int val1;
                double val3, val4;
                stringstream ss(line);
                ss >> val1 >> val2 >> val3 >> val4 >> val5;
                lumiblockDurationVector_RunMap[runNumber[i]].push_back( val3 );
                lumiPerLumiBlockVector_RunMap[runNumber[i]].push_back( val4 );
                isPhysicsLumiBlockVector_RunMap[runNumber[i]].push_back( val5 == "T" );
//                 cout << val1 << " " << val2 << " " << val3 << " " << val4 << " " << val5 << endl;
            }
            lumiFile.close();
        } else{
            cerr << "\nERROR: Problems with opening a file: " << Form("offlineLumiInfo/%d.dat", runNumber[i]) << endl;
        }
    }
}


void CrossSectionAnalysis::calculateDeltaZ0Eff(){
    
    gStyle->SetPadTickX(1);
    gStyle->SetPadTickY(1);
    
    double leftMargin = 0.17;
    double rightMargin = 0.03;
    double topMargin = 0.03;
    double bottomMargin = 0.19;
    double divisionY = 0.4;
    
    //------------------ ------------------ ------------------ ------------------ ------------------ ------------------ 
    TCanvas *c = new TCanvas("c12314r2", "c", 900, 600);
    TGaxis::SetMaxDigits(3);
    c->SetFillStyle(4000);
    c->SetFillColor(0);
    c->SetFrameFillStyle(4000);
    c->SetLeftMargin(0);
    c->SetRightMargin(0);
    c->SetTopMargin(0);
    c->SetBottomMargin(0);
    
    // Upper plot will be in pad1
    TPad *pad1 = new TPad("pad1", "pad1", 0, divisionY, 1, 1.0);
    pad1->SetBottomMargin(0); // Upper and lower plot are joined
    pad1->SetFillStyle(4000);
    pad1->SetFillColor(0);
    pad1->SetFrameFillStyle(4000);
    pad1->Draw();             // Draw the upper pad: pad1
    pad1->cd();               // pad1 becomes the current pad
    gPad->SetLeftMargin(leftMargin);
    gPad->SetRightMargin(rightMargin);
    gPad->SetTopMargin(topMargin/(1.0-divisionY));
    gPad->SetBottomMargin(0);
    c->cd();          // Go back to the main canvas before defining pad2
    TPad *pad2 = new TPad("pad2", "pad2", 0, 0, 1, divisionY);
    pad2->SetFillStyle(4000);
    pad2->SetFillColor(0);
    pad2->SetFrameFillStyle(4000);
    pad2->Draw();
    pad2->cd();       // pad2 becomes the current pad
    gPad->SetLeftMargin(leftMargin);
    gPad->SetRightMargin(rightMargin);
    gPad->SetTopMargin(0.0);
    gPad->SetBottomMargin(bottomMargin/divisionY);
    gStyle->SetGridColor(kGray);
    gPad->SetGridy();
    
    
    pad1->cd();
    
    
    TGraphAsymmErrors *gDeltaZ0Eff_noBkgdSub[4];
    TGraphAsymmErrors *gDeltaZ0Eff_bkgdSub[4];
    
    for(int i=0; i<3; ++i){
        
        
        TH3F *h3 = nullptr;
        switch(i){
            case 0: h3 = mhNSigmaMissingPtVsDeltaZ0VsMinPt_2Pi; break;
            case 1: h3 = mhNSigmaMissingPtVsDeltaZ0VsMinPt_4Pi; break;
            case 2: h3 = mhNSigmaMissingPtVsDeltaZ0VsMinPt_6Pi; break;
            case 3: h3 = mhNSigmaMissingPtVsDeltaZ0VsMinPt_8Pi; break;
            default: break;
        }
        
        TH2D *mhNSigmaMissingPtVsMinPt_2Pi_All = dynamic_cast<TH2D*>( h3->Project3D("zx") );
        mhNSigmaMissingPtVsMinPt_2Pi_All->SetName("mhNSigmaMissingPtVsMinPt_2Pi_All");
        h3->GetYaxis()->SetRange(0, h3->GetYaxis()->FindBin(19.99) );
        TH2D *mhNSigmaMissingPtVsMinPt_2Pi_DeltaZ0LessThan20 = dynamic_cast<TH2D*>( h3->Project3D("zx") );
        mhNSigmaMissingPtVsMinPt_2Pi_DeltaZ0LessThan20->SetName("mhNSigmaMissingPtVsMinPt_2Pi_DeltaZ0LessThan20");
    
        //-------------------------------------
        // no bkgd subtraction
        TH1D *all = mhNSigmaMissingPtVsMinPt_2Pi_All->ProjectionX("lowerPt_All", 0, mhNSigmaMissingPtVsMinPt_2Pi_All->GetYaxis()->FindBin( 2.99 ) );
        TH1D *vertexed = mhNSigmaMissingPtVsMinPt_2Pi_DeltaZ0LessThan20->ProjectionX("lowerPt_DeltaZ0LessThan2", 0, mhNSigmaMissingPtVsMinPt_2Pi_DeltaZ0LessThan20->GetYaxis()->FindBin( 2.99 ) );
        TEfficiency *deltaZ0Eff_noBkgdSub = new TEfficiency( *vertexed, *all );
        gDeltaZ0Eff_noBkgdSub[i] = deltaZ0Eff_noBkgdSub->CreateGraph();
        //-------------------------------------
        
        //-------------------------------------
        // with bkgd subtraction
        TH1D *bkgdHistogram_all = new TH1D();
        TH1D *bkgdHistogram_vertexed = new TH1D();
        mUtil->bkgdHistogram(mhNSigmaMissingPtVsMinPt_2Pi_All, 3 )->Copy( *bkgdHistogram_all );
        bkgdHistogram_all->SetName("bkgdHistogram_all");
        mUtil->bkgdHistogram(mhNSigmaMissingPtVsMinPt_2Pi_DeltaZ0LessThan20, 3 )->Copy( *bkgdHistogram_vertexed );
        bkgdHistogram_vertexed->SetName("bkgdHistogram_vertexed");
        
        //     TH1D *bkgdToAll = new TH1D(*bkgdHistogram_all);
        //     bkgdToAll->SetName("bkgdToAll");
        
        TH1D *allBkgdSubtracted = new TH1D( *all );
        allBkgdSubtracted->Add( bkgdHistogram_all, -1 );
        TH1D *vertexedBkgdSubtracted = new TH1D( *vertexed );
        vertexedBkgdSubtracted->Add( bkgdHistogram_vertexed, -1 );
        
        
        for(int i=0; i<=allBkgdSubtracted->GetNbinsX()+1; ++i){
            if( allBkgdSubtracted->GetBinContent(i) < vertexedBkgdSubtracted->GetBinContent(i) ){
                allBkgdSubtracted->SetBinContent(i, vertexedBkgdSubtracted->GetBinContent(i));
            }
        }
        
        TEfficiency *deltaZ0Eff_bkgdSub = new TEfficiency( *vertexedBkgdSubtracted, *allBkgdSubtracted );
        gDeltaZ0Eff_bkgdSub[i] = deltaZ0Eff_bkgdSub->CreateGraph();
        //-------------------------------------
    }
        
//     TEfficiency *mcDeltaZ0Eff = dynamic_cast<TEfficiency*>( TFile::Open("vertexingEff/DeltaZ0EfficiencyMC.root")->Get("effDeltaZ0_vs_lowerPt") );
//     TGraphAsymmErrors *gMcDeltaZ0Eff = mcDeltaZ0Eff->CreateGraph();
    
    TMultiGraph *mg = new TMultiGraph();
    
    for(int i=0; i<3; ++i){
        gDeltaZ0Eff_noBkgdSub[i]->SetMarkerSize(1.6);
        gDeltaZ0Eff_noBkgdSub[i]->SetMarkerStyle(20+i);
        gDeltaZ0Eff_noBkgdSub[i]->SetMarkerColor(i==0 ? (kGreen+2) : (i==1 ? kBlue : kRed));
        gDeltaZ0Eff_noBkgdSub[i]->SetLineColor(i==0 ? (kGreen+2) : (i==1 ? kBlue : kRed));
        
        gDeltaZ0Eff_bkgdSub[i]->SetMarkerSize(1.8);
        gDeltaZ0Eff_bkgdSub[i]->SetMarkerStyle(24+i);
        gDeltaZ0Eff_bkgdSub[i]->SetMarkerColor(kBlack);
        gDeltaZ0Eff_bkgdSub[i]->SetLineColor(kBlack);
    //     
    //     gMcDeltaZ0Eff->SetMarkerSize(1.6);
    //     gMcDeltaZ0Eff->SetMarkerStyle(21);
    //     gMcDeltaZ0Eff->SetMarkerColor(kRed);
    //     gMcDeltaZ0Eff->SetLineColor(kRed);
    //     
    //     mg->Add(gMcDeltaZ0Eff, "PE");
        mg->Add(gDeltaZ0Eff_noBkgdSub[i], "PE");
        mg->Add(gDeltaZ0Eff_bkgdSub[i], "PE");
    }
//     
    mg->SetTitle("; min(p_{T}) [GeV];Eff. of |#Deltaz_{0}| < 20 mm");
    mg->Draw("A");
    
    
    TLine* eff1Line = new TLine( 0.1, 1, 1, 1 );
    eff1Line->SetLineWidth(2);
    eff1Line->SetLineColor(kGray+2);
    eff1Line->SetLineStyle(7);
    eff1Line->Draw();
    
    
    mg->GetYaxis()->SetTitleOffset(0.85);
    mg->GetXaxis()->SetLimits(0.1, 1);
    mg->GetXaxis()->SetTickLength( 0.8*mg->GetXaxis()->GetTickLength() );
    mg->GetYaxis()->SetTickLength( 0.5*mg->GetXaxis()->GetTickLength() );
    mg->GetYaxis()->SetRangeUser(0.984, 1.001);
    mg->GetYaxis()->SetTitleSize( 2*mg->GetYaxis()->GetTitleSize() );
    mg->GetYaxis()->SetLabelSize( 2*mg->GetYaxis()->GetLabelSize() );
    mg->GetXaxis()->SetTitleSize( 2*mg->GetXaxis()->GetTitleSize() );
    mg->GetXaxis()->SetLabelSize( 2*mg->GetXaxis()->GetLabelSize() );
//   
    mg->GetYaxis()->SetNdivisions(5, 5, 1);
    mg->GetXaxis()->SetNdivisions(5, 2, 1);
//     lSTAR.DrawLatex(0.8, 0.86, "#scale[1.2]{STAR}");
//     lSTAR.DrawLatex(0.8, 0.8, "#font[42]{Internal}");
//     
//     
    TLegend *legend = new TLegend(0.53, 0.235, 0.9, 0.59);
    legend->SetFillColorAlpha(kWhite, 0.8);
    legend->SetNColumns(2);
    legend->SetBorderSize(-1);
    for(int i=0; i<3; ++i){
        TString multStr(i==0 ? "2#pi" : (i==1 ? "4#pi" : "6#pi"));
        legend->AddEntry(gDeltaZ0Eff_noBkgdSub[i], multStr, "pel");
        legend->AddEntry(gDeltaZ0Eff_bkgdSub[i], multStr+" (bkgd sub.)", "pel");
    }
//         legend->AddEntry(gMcDeltaZ0Eff, "GenEx #oplus ZeroBias", "pel");
    legend->SetTextSize( 0.8*mg->GetYaxis()->GetTitleSize() );
    legend->Draw();
//     
//     
//     l.SetTextSize( 0.8*mg->GetYaxis()->GetTitleSize() );
//     TString cutsStr = Form("|z_{vtx}|<%d cm,   p_{T}>%.1f GeV", static_cast<int>(mParams->maxZVertex()), mParams->minTpcTrackPt(Util::PION) );
//     l.DrawLatex( 0.32, 0.88, cutsStr );
//     cutsStr = Form("|#eta|<%.1f",  mParams->maxEta() );
//     l.DrawLatex( 0.539, 0.8, cutsStr );
//     l.SetTextSize(.04);
//     
//     
//     
        pad2->cd();
        
        TMultiGraph *mg2 = new TMultiGraph();
        
        for(int j=0; j<4; ++j){
        
            TGraphAsymmErrors *gData_NoBkgdSub_minus_BkgdSub = new TGraphAsymmErrors( *gDeltaZ0Eff_noBkgdSub[j] );
            
            for(int i=0; i<gData_NoBkgdSub_minus_BkgdSub->GetN(); ++i){
            double x, y;
            double xMC, yMC;
            double errX, errY;
            gDeltaZ0Eff_bkgdSub[j]->GetPoint(i, xMC, yMC);
            
            gData_NoBkgdSub_minus_BkgdSub->GetPoint(i, x, y);
            y -= yMC;
            gData_NoBkgdSub_minus_BkgdSub->SetPoint(i, x, y);
            errX = gData_NoBkgdSub_minus_BkgdSub->GetErrorX(i);
            errY = 0.0;///*sqrt(pow(*/gData_NoBkgdSub_minus_BkgdSub->GetErrorY(i)/*,2) + pow(gMcDeltaZ0Eff->GetErrorY(i),2))*/;
            gData_NoBkgdSub_minus_BkgdSub->SetPointError(i, errX, errX, errY, errY);
            
            
            }
            
            mg2->Add(gData_NoBkgdSub_minus_BkgdSub, "PE");
            
        }
        
        mg2->SetTitle("; min(p_{T}) [GeV]; Diff.");
        mg2->Draw("A");
        
        TGaxis::SetMaxDigits(5);
        mg2->GetYaxis()->SetNdivisions(5, 2, 1);
        mg2->GetXaxis()->SetNdivisions(5, 2, 1);
        mg2->GetYaxis()->CenterTitle();
        mg2->GetYaxis()->SetTitleOffset( 1.05*mg->GetYaxis()->GetTitleOffset() / ((1.0-divisionY)/divisionY) );
        mg2->GetXaxis()->SetLimits(0.1,1);
        mg2->GetYaxis()->SetRangeUser(-0.0035, 0.0035);
        mg2->GetXaxis()->SetTickLength( mg->GetXaxis()->GetTickLength()*(1.0-divisionY)/divisionY );
        mg2->GetYaxis()->SetTitleSize( 2*mg2->GetYaxis()->GetTitleSize()*(1.0-divisionY)/divisionY );
        mg2->GetYaxis()->SetLabelSize( 2*mg2->GetYaxis()->GetLabelSize()*(1.0-divisionY)/divisionY );
        mg2->GetXaxis()->SetTitleSize( 2*mg2->GetXaxis()->GetTitleSize()*(1.0-divisionY)/divisionY );
        mg2->GetXaxis()->SetLabelSize( 2*mg2->GetXaxis()->GetLabelSize()*(1.0-divisionY)/divisionY );
        
        TLine* lineRatioEqual1 = new TLine( 0.1, 0, 1, 0 );
        lineRatioEqual1->SetLineWidth(2);
        lineRatioEqual1->SetLineStyle(7);
        lineRatioEqual1->SetLineColor(kGray+2);
        lineRatioEqual1->Draw();
//     
    c->Print("DeltaZ0Efficiency_2.pdf", "EmbedFonts");
//     
//     
//     TFile *file = new TFile( mAnManager->outputDir()+"/DeltaZ0Eff.root", "RECREATE" );
//     deltaZ0Eff_bkgdSub->SetName("deltaZ0Eff_bkgdSub");
//     deltaZ0Eff_bkgdSub->SetDirectory(file);
//     deltaZ0Eff_noBkgdSub->SetName("deltaZ0Eff_noBkgdSub");
//     deltaZ0Eff_noBkgdSub->SetDirectory(file);
//     file->Write();
//     file->Close();
  
  
  
}
