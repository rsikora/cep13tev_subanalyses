#ifndef CrossSectionAnalysis_hh
#define CrossSectionAnalysis_hh

#include <TROOT.h>
#include <TFile.h>
#include <TString.h>
#include "Util.hh"
#include "ObservableObject.hh"
// #include "Parameters.hh"
// #include "Efficiencies.hh"
#include "DrawingOptions.hh"
#include "SystematicsOutput.hh"

class TH2;
class TF2;
class TH1F;
class TH2F;


TF2 *fExponent2D;
Double_t exponent1D(Double_t *x, Double_t *par);
double maxMandT;
    

class CrossSectionAnalysis{
public:
    
    CrossSectionAnalysis(TString opt="");
    ~CrossSectionAnalysis();
    
    void analyze();
    
private:
    
    Util *mUtil;
    //   Parameters *mParams;
    //   Efficiencies *mEff;
    
    const double nSigmaMomentumBalanceCut = 3.0;
    static constexpr int nFilesWithAcceptanceCorr = 3;
    
    void getListOfAnalysedRuns();
    void readLumiFiles();
    void getTotalIntegratedLuminosity();
    
    void pullAnalysis();
    
    void transformToCrossSection(TH1*) const;
    SystematicsOutput calculateSystematicErrors( TH1F* const, TH1F** const, TH1F* const) const;
    void drawFinalResult(TH1F*, DrawingOptions &, SystematicsOutput &) const;
    void drawDataMcComparison2(TH1F*, TH1F*, DrawingOptions & opt);
    TH2F* getTH2F_from_TH1F_t1t2( const TH1F*, bool = true );
    
    void drawResolutions();
    
    void drawNSigmaMissingPt();
    
    void calculateDeltaZ0Eff();
    
    Double_t mSpecialLegendMarginFactor;
    Bool_t mSpecialLabelRatio;
    Double_t mSpecialLabelOffsetFactor;
    Bool_t mEnableLegend;
    
    TString* mSystCheckName;
    TString* mSystCheckShortName;
    
    double totalIntegratedLuminosity; // mub^-1
    vector<int> runNumber;
    vector<int> colors;
    
    // all LBs are numbered starting from 1 and there are no gaps between LBs
    map<int, vector<double> > lumiPerLumiBlockVector_RunMap;
    map<int, vector<double> > lumiblockDurationVector_RunMap;
    map<int, vector<bool> > isPhysicsLumiBlockVector_RunMap;
    
    map<int, vector<int> > analysedLumiBlockVector_RunMap;
    
    //-- 2pi
    
    ObservableObject *oo2PiInvMass; //!
    ObservableObject *oo2PiRapidity; //!
    ObservableObject *oo2PiDeltaPhi; //!
    ObservableObject *oo2PiMandelstamTSum; //!
    ObservableObject *oo2PiCosTheta[Util::nReferenceFrames]; //!
    ObservableObject *oo2PiPhi[Util::nReferenceFrames]; //!
    
    ObservableObject *oo2PiInvMass_DeltaPhiBins[Util::nDeltaPhiRanges]; //!
    ObservableObject *oo2PiInvMass_DPtBins_DeltaPhiLessThan90Deg[2]; //!
    
    ObservableObject *oo2PiInvMass_CentralTracksEtaBins[Util::nCentralTracksEtaConf]; //!
  
    ObservableObject *oo2PiRapidity_MassBins[Util::nMassRanges]; //!
    ObservableObject *oo2PiDeltaPhi_MassBins[Util::nMassRanges]; //!
    ObservableObject *oo2PiMandelstamTSum_MassBins[Util::nMassRanges]; //!
    ObservableObject *oo2PiCosTheta_MassBins[Util::nReferenceFrames][Util::nMassRanges]; //!
    ObservableObject *oo2PiPhi_MassBins[Util::nReferenceFrames][Util::nMassRanges]; //!
    
    ObservableObject *oo2PiMandelstamT_CVsA_ForSlopeExtraction_DeltaPhiBins_MassBins[nFilesWithAcceptanceCorr][4/*deltaPhiBins*/][Util::nMassRanges]; //!
    
    //-- 4pi
    
    ObservableObject *oo4PiInvMass; //!
    ObservableObject *oo4PiInvMass_MinMassOf2Pi; //!
    ObservableObject *oo4PiInvMass_MaxMassOf2Pi; //!
    ObservableObject *oo4PiRapidity; //!
    ObservableObject *oo4PiDeltaPhi; //!
    ObservableObject *oo4PiMandelstamTSum; //!
    
    ObservableObject *oo4PiInvMass_DeltaPhiBins[Util::nDeltaPhiRanges]; //!
    ObservableObject *oo4PiInvMass_MinMassOf2Pi_DeltaPhiBins[Util::nDeltaPhiRanges]; //!
    ObservableObject *oo4PiInvMass_MaxMassOf2Pi_DeltaPhiBins[Util::nDeltaPhiRanges]; //!
    
    ObservableObject *oo4PiInvMass_DeltaPhiBins_FineBinning[Util::nDeltaPhiRanges]; //!
  
    ObservableObject *oo4PiRapidity_MassBins[Util::nMassRanges]; //!
    ObservableObject *oo4PiDeltaPhi_MassBins[Util::nMassRanges]; //!
    ObservableObject *oo4PiMandelstamTSum_MassBins[Util::nMassRanges]; //!
    
    ObservableObject *oo4PiMandelstamT_CVsA_ForSlopeExtraction_DeltaPhiBins_MassBins[nFilesWithAcceptanceCorr][Util::nDeltaPhiRanges][Util::nMassRanges]; //!
    
    
    //-- 6pi
    
    ObservableObject *oo6PiInvMass; //!
    ObservableObject *oo6PiRapidity; //!
    ObservableObject *oo6PiDeltaPhi; //!
    ObservableObject *oo6PiMandelstamTSum; //!
    
    //-- 8pi
    
    ObservableObject *oo8PiInvMass; //!
    ObservableObject *oo8PiRapidity; //!
    ObservableObject *oo8PiDeltaPhi; //!
    ObservableObject *oo8PiMandelstamTSum; //!
    
    
    
    
    
    
    ObservableObject *oo2PiIntegratedXSec; //!
    ObservableObject *oo4PiIntegratedXSec; //!
    ObservableObject *oo6PiIntegratedXSec; //!
    ObservableObject *oo8PiIntegratedXSec; //!
    
    
    
    //----------------------------------------------------------------------------------------
    
    TH2F *mhNEventsVsLumiBlockVsRunNumber;
    TH2F *mhN2PiCepEventsVsLumiBlockVsRunNumber[Util::nProtonsConfigurations];
    TH2F *mhN2PiCepEventsVsLumiBlockVsRunNumber_Weighted[Util::nProtonsConfigurations];
    TH2F *mhN4PiCepEventsVsLumiBlockVsRunNumber[Util::nProtonsConfigurations];
    TH2F *mhN4PiCepEventsVsLumiBlockVsRunNumber_Weighted[Util::nProtonsConfigurations];
    
    TH3F *mhNSigmaMissingPtVsDeltaZ0VsMinPt_2Pi;
    TH3F *mhNSigmaMissingPtVsDeltaZ0VsMinPt_4Pi;
    TH3F *mhNSigmaMissingPtVsDeltaZ0VsMinPt_6Pi;
    TH3F *mhNSigmaMissingPtVsDeltaZ0VsMinPt_8Pi;
    
    ClassDef(CrossSectionAnalysis,0);
};

#endif
