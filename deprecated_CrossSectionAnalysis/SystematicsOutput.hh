#ifndef SystematicsOutput_hh
#define SystematicsOutput_hh

#include <TROOT.h>
#include <TStyle.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TProfile.h>
#include <TFile.h>
#include <TString.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TLatex.h>
#include <cmath>
#include <TLegend.h>
#include <TPaveText.h>
#include <TEllipse.h>
#include <algorithm>
#include <vector>
#include <map>
#include <fstream>
#include <sstream>
#include <string>
#include <TGaxis.h>
#include <TObjArray.h>
#include <TObjString.h>
#include <TEfficiency.h>
#include <TMultiGraph.h>
#include <TGraphAsymmErrors.h>
#include <TFitResult.h>
#include <TArrow.h>
#include "Util.hh"

struct SystematicsOutput{
    SystematicsOutput(): mhSystErrorUp(nullptr), mhSystErrorDown(nullptr), mhSystErrorUp_Other(nullptr), mhSystErrorDown_Other(nullptr){}
    std::vector<Double_t> mError[Util::nSystChecks];
    std::vector<TH1F*> mSystCheckHistVec;
    TH1F* mhSystErrorUp;
    TH1F* mhSystErrorDown;
//     TH1F* mhSystErrorUp_TOF;
//     TH1F* mhSystErrorDown_TOF;
//     TH1F* mhSystErrorUp_TPC;
//     TH1F* mhSystErrorDown_TPC;
//     TH1F* mhSystErrorUp_RP;
//     TH1F* mhSystErrorDown_RP;
    TH1F* mhSystErrorUp_Other;
    TH1F* mhSystErrorDown_Other;
};

#endif
