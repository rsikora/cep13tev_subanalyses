#define Util_cxx

#include <limits>
#include <algorithm>
#include "Util.hh"
#include "TString.h"
#include "TF1.h"
#include "TVectorD.h"
#include "TMatrixD.h"
#include "TSpline.h"
#include "TRandom3.h"
#include "TFitResult.h"

Util::Util(TString codePath): mSpeedOfLight(29.9792), mBeamMomentum(6500.0){

  mSideName = new TString[nSides];
  mSideName[A] = TString("A");
  mSideName[C] = TString("C");
  
  mCoordinateName = new TString[nCoordinates];
  mCoordinateName[X] = TString("x");
  mCoordinateName[Y] = TString("y");
  
  mTriggerName = new TString[nTriggers];
  mTriggerName[SD] = TString("SD");
  mTriggerName[CPT2] = TString("CPT2");
  mTriggerName[SDT] = TString("SDT");
  mTriggerName[RPZMU] = TString("RPZMU");
  mTriggerName[RP2MU] = TString("RP2MU");
  mTriggerName[ET] = TString("ET");
  mTriggerName[CP] = TString("CP");
  mTriggerName[CPT] = TString("CPT");
  mTriggerName[RP2E] = TString("RP2E");
  mTriggerName[Zerobias] = TString("Zerobias");
  mTriggerName[CPX] = TString("CPX");
  mTriggerName[SDZ] = TString("SDZ");
  mTriggerName[CPEI] = TString("CPEI");
  mTriggerName[ZE] = TString("ZE");
  
  mTriggerBitLabel = new TString[nTriggerBits];
  mTriggerBitLabel[BIT_ET] = TString("ET");
  mTriggerBitLabel[BIT_IT] = TString("IT");
  mTriggerBitLabel[BIT_EOR] = TString("EOR");
  mTriggerBitLabel[BIT_WOR] = TString("WOR");
  mTriggerBitLabel[BIT_EU] = TString("EU");
  mTriggerBitLabel[BIT_ED] = TString("ED");
  mTriggerBitLabel[BIT_WU] = TString("WU");
  mTriggerBitLabel[BIT_WD] = TString("WD");
  
  mArmName = new TString[nArms];
  mArmName[EU_WD] = TString("EU-WD");
  mArmName[ED_WU] = TString("ED-WU");
  
  mBranchName = new TString[nBranches];
  mBranchName[EU] = TString("EU");
  mBranchName[ED] = TString("ED");
  mBranchName[WU] = TString("WU");
  mBranchName[WD] = TString("WD");
  
  mBranchesConfigurationName = new TString[nBranchesConfigurations];
  mBranchesConfigurationName[CONF_EU_WU] = TString("EU-WU");
  mBranchesConfigurationName[CONF_ED_WD] = TString("ED-WD");
  mBranchesConfigurationName[CONF_EU_WD] = TString("EU-WD");
  mBranchesConfigurationName[CONF_ED_WU] = TString("ED-WU");
    
  mRpName = new TString[nRomanPots];
  mRpName[E1U] = TString("E1U");
  mRpName[E1D] = TString("E1D");
  mRpName[E2U] = TString("E2U");
  mRpName[E2D] = TString("E2D");
  mRpName[W1U] = TString("W1U");
  mRpName[W1D] = TString("W1D");
  mRpName[W2U] = TString("W2U");
  mRpName[W2D] = TString("W2D");
  
//   mPlaneName = new TString[nPlanes];
//   mPlaneName[A] = TString("A");
//   mPlaneName[B] = TString("B");
//   mPlaneName[C] = TString("C");
//   mPlaneName[D] = TString("D");
  
  mStationName = new TString[nStations];
  mStationName[E1] = TString("E1");
  mStationName[E2] = TString("E2");
  mStationName[W1] = TString("W1");
  mStationName[W2] = TString("W2");
  
  mProtonsConfiguration = new TString[nProtonsConfigurations];
  mProtonsConfiguration[ALL] = TString("");
  mProtonsConfiguration[ELA] = TString("ET");
  mProtonsConfiguration[INE] = TString("IT");
  
  mParticleName = new TString[nDefinedParticles];
//   mParticleName[ELECTRON] = TString("electron");
//   mParticleName[MUON] = TString("muon");
  mParticleName[PION] = TString("pion");
  mParticleName[KAON] = TString("kaon");
  mParticleName[PROTON] = TString("proton");
    
  mParticleNameExtended = new TString[nDefinedParticlesExtended];
  mParticleNameExtended[PIOn] = mParticleName[PION];
  mParticleNameExtended[KAOn] = mParticleName[KAON];
  mParticleNameExtended[PROTOn] = mParticleName[PROTON];
  mParticleNameExtended[ELECTROn] = TString("electron");
  mParticleNameExtended[DEUTEROn] = TString("deuteron");
  
  mTpcTrackTypeName = new TString[nTpcTrkTypes];
  mTpcTrackTypeName[GLO] = TString("global");
  mTpcTrackTypeName[PRI] = TString("primary");
  mTpcTrackTypeName[TOF] = TString("TofMatched");
  mTpcTrackTypeName[QUA] = TString("goodQuality");

  mBunchCrossingTypeName = new TString[nBnchXngsTypes];
  mBunchCrossingTypeName[CB] = TString("collidingBunches");
  mBunchCrossingTypeName[AG] = TString("abortGaps");
  
  mChargeSum2TrksName = new TString[nCharges2Trks];
  mChargeSum2TrksName[OPPO] = TString("oppositeSign");
  mChargeSum2TrksName[SAME] = TString("sameSign");
  
  mChargeSum4TrksName = new TString[nCharges4Trks];
  mChargeSum4TrksName[QSUM_ZERO] = TString("QSum0");
  mChargeSum4TrksName[QSUM_NON0] = TString("QSumNon0");

  mSignName = new TString[nSigns];
  mSignName[PLUS] = TString("Plus");
  mSignName[MINUS] = TString("Minus");
  
  mRpTrackCombinationName = new TString[nRpTrackTypeCombinations];
  mRpTrackCombinationName[LL] = TString("LL");
  mRpTrackCombinationName[LG] = TString("LG");
  mRpTrackCombinationName[GG] = TString("GG");
  
  mEfficiencyName = new TString[nEffCorrections];
  mEfficiencyName[RPACC] = TString("RpAcc");
  mEfficiencyName[TPCRECOEFF] = TString("TpcRecoEff");
  mEfficiencyName[TOFMATCHEFF] = TString("TofMatchEff");
  
  mCutName = new TString[nAnalysisCuts];
  mCutName[TRIG] = TString("Triggers");
  mCutName[TWORPTRKS] = TString("2 RP trks");
  mCutName[ONETOFVX] = TString("1 TOF vrtx");
  mCutName[ZVERTEX] = TString("|z_{vx}| < 100 cm");
  mCutName[TWOTOFTRKS] = TString("2 TOF-mtchd trks");
  mCutName[TWOQUATRKS] = TString("TPC trks quality");
  mCutName[OPPOSITE] = TString("Opposite-sign");
  mCutName[TPCRPVX_MATCHED] = TString("TPC-RP vrtx match");
  mCutName[BBCCLEAN] = TString("BBC-L clean");
  mCutName[TOFHITSRECO] = TString("2 N_{TOF}^{clusters}");
  mCutName[EXCLUSIVE] = TString("Exclusive (p_{T}^{miss} cut)");
  mCutName[PIPI] = TString("#pi^{+}#pi^{-}");
  mCutName[KK] = TString("K^{+}K^{-}");
  mCutName[PPBAR] = TString("p#bar{p}");

  mCutShortName = new TString[nAnalysisCuts];
  mCutShortName[TRIG] = TString("TRIG");
  mCutShortName[TWORPTRKS] = TString("TWORPTRKS");
  mCutShortName[ONETOFVX] = TString("ONETOFVX");
  mCutShortName[ZVERTEX] = TString("ZVERTEX");
  mCutShortName[TWOTOFTRKS] = TString("TWOTOFTRKS");
  mCutShortName[TWOQUATRKS] = TString("TWOQUATRKS");
  mCutShortName[OPPOSITE] = TString("OPPOSITE");
  mCutShortName[TPCRPVX_MATCHED] = TString("TPCRPVX_MATCHED");
  mCutShortName[BBCCLEAN] = TString("BBCCLEAN");
  mCutShortName[TOFHITSRECO] = TString("TOFHITSRECO");
  mCutShortName[EXCLUSIVE] = TString("EXCLUSIVE");
  mCutShortName[PIPI] = TString("PIPI");
  mCutShortName[KK] = TString("KK");
  mCutShortName[PPBAR] = TString("PPBAR");
  
  mReferenceFrameName = new TString[nReferenceFrames];
  mReferenceFrameName[GOTTFRIED_JACKSON] = TString("Gottfried-Jackson");
  mReferenceFrameName[COLLINS_SOPER] = TString("Collins-Soper");
  mReferenceFrameName[HELICITY] = TString("Helicity");
  
  mReferenceFrameShortName = new TString[nReferenceFrames];
  mReferenceFrameShortName[GOTTFRIED_JACKSON] = TString("GJ");
  mReferenceFrameShortName[COLLINS_SOPER] = TString("CS");
  mReferenceFrameShortName[HELICITY] = TString("H");
  
  mTrueLevelCentralStateTopologyName = new TString[nCentralStateTopologies];
  mTrueLevelCentralStateTopologyName[kNoChargedOnlyNeutrals] = TString("NoChargedOnlyNeutrals");
  mTrueLevelCentralStateTopologyName[k1ChargedPairNoNeutrals] = TString("1ChargedPairNoNeutrals");
  mTrueLevelCentralStateTopologyName[k1ChargedPairAndNeutrals] = TString("1ChargedPairAndNeutrals");
  mTrueLevelCentralStateTopologyName[k2ChargedPairsNoNeutrals] = TString("2ChargedPairsNoNeutrals");
  mTrueLevelCentralStateTopologyName[k2ChargedPairsAndNeutrals] = TString("2ChargedPairsAndNeutrals");
  mTrueLevelCentralStateTopologyName[k3OrMoreChargedPairsNoNeutrals] = TString("3OrMoreChargedPairsNoNeutrals");
  mTrueLevelCentralStateTopologyName[k3OrMoreChargedPairsAndNeutrals] = TString("3OrMoreChargedPairsAndNeutrals");
  mTrueLevelCentralStateTopologyName[kUnqualified] = TString("Unqualified");
  
//   mMassRangeLimits[MASS_1][MIN] = 0.0;
//   mMassRangeLimits[MASS_1][MAX] = 1.0;
//   mMassRangeLimits[MASS_2][MIN] = 1.0;
//   mMassRangeLimits[MASS_2][MAX] = 1.5;
//   mMassRangeLimits[MASS_3][MIN] = 1.5;
//   mMassRangeLimits[MASS_3][MAX] = 9e9;
//   
// //   mDeltaPhiLimits[DELTAPHI_1][MIN] = 0.0;
// //   mDeltaPhiLimits[DELTAPHI_1][MAX] = 60.0;
// //   mDeltaPhiLimits[DELTAPHI_2][MIN] = 60.0;
// //   mDeltaPhiLimits[DELTAPHI_2][MAX] = 120.0;
// //   mDeltaPhiLimits[DELTAPHI_3][MIN] = 120.0;
// //   mDeltaPhiLimits[DELTAPHI_3][MAX] = 180.0;
// 
//   mDeltaPhiLimits[DELTAPHI_1][MIN] = 0.0;
//   mDeltaPhiLimits[DELTAPHI_1][MAX] = 90.0;
//   mDeltaPhiLimits[DELTAPHI_2][MIN] = 90.0;
//   mDeltaPhiLimits[DELTAPHI_2][MAX] = 180.0;
//   
//   mPairPtRangeLimits[PAIRPT_1][MIN] = 0.0;
//   mPairPtRangeLimits[PAIRPT_1][MAX] = 0.25;
//   mPairPtRangeLimits[PAIRPT_2][MIN] = 0.25;
//   mPairPtRangeLimits[PAIRPT_2][MAX] = 0.45;
//   mPairPtRangeLimits[PAIRPT_3][MIN] = 0.45;
//   mPairPtRangeLimits[PAIRPT_3][MAX] = 9e99;
//   
//   mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_1][MIN] = 0.0;
//   mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_1][MAX] = 0.08;
//   mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_2][MIN] = 0.08;
//   mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_2][MAX] =  9e99;
//   
//   mTRangeLimits[TRANGE_1][MIN] = 0.05;
//   mTRangeLimits[TRANGE_1][MAX] = 0.08;
//   mTRangeLimits[TRANGE_2][MIN] = 0.08;
//   mTRangeLimits[TRANGE_2][MAX] = 0.11;
//   mTRangeLimits[TRANGE_3][MIN] = 0.11;
//   mTRangeLimits[TRANGE_3][MAX] = 0.16;
  
//   mParticleMass[ELECTRON] = 0.000511;
//   mParticleMass[MUON] = 0.10565837;
  mParticleMass[PION] = 0.13957018;
  mParticleMass[KAON] = 0.493667;
  mParticleMass[PROTON] = 0.93827208;
  
  mParticleMassExtended[PIOn] = 0.13957018;
  mParticleMassExtended[KAOn] = 0.493667;
  mParticleMassExtended[PROTOn] = 0.93827208;
  mParticleMassExtended[ELECTROn] = 0.000511;
  mParticleMassExtended[DEUTEROn] = 1.875612;
  
  mBranchPerRp[E1U] = EU;
  mBranchPerRp[E2U] = EU;
  mBranchPerRp[E1D] = ED;
  mBranchPerRp[E2D] = ED;
  mBranchPerRp[W1U] = WU;
  mBranchPerRp[W2U] = WU;
  mBranchPerRp[W1D] = WD;
  mBranchPerRp[W2D] = WD;
  
  mOppositeBranch[EU] = WD;
  mOppositeBranch[ED] = WU;
  mOppositeBranch[WU] = ED;
  mOppositeBranch[WD] = EU;
  
//   mSidePerRp[E1U] = E;
//   mSidePerRp[E2U] = E;
//   mSidePerRp[E1D] = E;
//   mSidePerRp[E2D] = E;
//   mSidePerRp[W1U] = W;
//   mSidePerRp[W2U] = W;
//   mSidePerRp[W1D] = W;
//   mSidePerRp[W2D] = W;
  
  mStationOrderPerRp[E1U] = RP1;
  mStationOrderPerRp[E2U] = RP2;
  mStationOrderPerRp[E1D] = RP1;
  mStationOrderPerRp[E2D] = RP2;
  mStationOrderPerRp[W1U] = RP1;
  mStationOrderPerRp[W2U] = RP2;
  mStationOrderPerRp[W1D] = RP1;
  mStationOrderPerRp[W2D] = RP2;
  
  mRpPerBranchStationOrder[EU][RP1] = E1U;
  mRpPerBranchStationOrder[EU][RP2] = E2U;
  mRpPerBranchStationOrder[ED][RP1] = E1D;
  mRpPerBranchStationOrder[ED][RP2] = E2D;
  mRpPerBranchStationOrder[WU][RP1] = W1U;
  mRpPerBranchStationOrder[WU][RP2] = W2U;
  mRpPerBranchStationOrder[WD][RP1] = W1D;
  mRpPerBranchStationOrder[WD][RP2] = W2D;
  
  mGenerator = new TRandom3(1); // if seed set to 0, then randomization will be different each time program is run

}

Util::~Util(){
  if(mSideName) delete [] mSideName;
  if(mTriggerName) delete [] mTriggerName;
  if(mTriggerBitLabel) delete [] mTriggerBitLabel;
  if(mArmName) delete [] mArmName;
  if(mBranchName) delete [] mBranchName;
  if(mBranchesConfigurationName) delete [] mBranchesConfigurationName;
  if(mRpName) delete [] mRpName;
  if(mPlaneName) delete [] mPlaneName;
  if(mStationName) delete [] mStationName;
  if(mProtonsConfiguration) delete [] mProtonsConfiguration;
  if(mParticleName) delete [] mParticleName;
  if(mParticleNameExtended) delete [] mParticleNameExtended;
  if(mTpcTrackTypeName) delete [] mTpcTrackTypeName;
  if(mBunchCrossingTypeName) delete [] mBunchCrossingTypeName;
  if(mChargeSum2TrksName) delete [] mChargeSum2TrksName;
  if(mChargeSum4TrksName) delete [] mChargeSum4TrksName;
  if(mSignName) delete [] mSignName;
  if(mRpTrackCombinationName) delete [] mRpTrackCombinationName;
  if(mEfficiencyName) delete [] mEfficiencyName;
  if(mCutName) delete [] mCutName;
  if(mCutShortName) delete [] mCutShortName;
  if(mReferenceFrameName) delete [] mReferenceFrameName;
  if(mReferenceFrameShortName) delete [] mReferenceFrameShortName;
  if(mTrueLevelCentralStateTopologyName) delete [] mTrueLevelCentralStateTopologyName;
}

Util* Util::instance(TString codePath){
  if(!mInst) mInst = new Util(codePath);
  return mInst;
}

// Util::MASS_RANGE Util::massRange(Double_t mass) const{
//   for(int i=0; i<nMassRanges; ++i)
//     if(mass>=mMassRangeLimits[i][MIN] && mass<mMassRangeLimits[i][MAX])
//       return static_cast<MASS_RANGE>(i);
//   std::cerr << "ERROR in Util::massRange(Double_t): argument not within expected limits!" << std::endl;
//   return nMassRanges;
// }
// 
// Util::DELTAPHI_RANGE Util::deltaPhiRange(Double_t deltaPhi) const{
//   for(int i=0; i<nDeltaPhiRanges; ++i)
//     if(deltaPhi>=mDeltaPhiLimits[i][MIN] && deltaPhi<mDeltaPhiLimits[i][MAX])
//       return static_cast<DELTAPHI_RANGE>(i);
//   std::cerr << "ERROR in Util::deltaPhiRange(Double_t): argument not within expected limits!" << std::endl;
//   return nDeltaPhiRanges;
// }
// Util::PAIRPT_RANGE Util::pairPtRange(Double_t pairPt) const{
//   for(int i=0; i<nPairPtRanges; ++i)
//     if(pairPt>=mPairPtRangeLimits[i][MIN] && pairPt<mPairPtRangeLimits[i][MAX])
//       return static_cast<PAIRPT_RANGE>(i);
//   std::cerr << "ERROR in Util::pairPtRange(Double_t): argument not within expected limits!" << std::endl;
//   return nPairPtRanges;
// }
// 
// Util::MANDELSTAMTSINGLESIDE_RANGE Util::mandelstamTSingleSideRange(Double_t t) const{
//   for(int i=0; i<nMandelstamTSingleSideRanges; ++i)
//     if(t>=mMandelstamTRangeLimits[i][MIN] && t<mMandelstamTRangeLimits[i][MAX])
//       return static_cast<MANDELSTAMTSINGLESIDE_RANGE>(i);
//   std::cerr << "ERROR in Util::mandelstamTSingleSideRange(Double_t): argument not within expected limits!" << std::endl;
//   return nMandelstamTSingleSideRanges;
// }
// 
// Util::MANDELSTAMTBOTHSIDES_RANGE Util::mandelstamTBothSidesRange(Double_t t1, Double_t t2) const{
//   if(t1>=mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_1][MIN] && t1<mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_1][MAX] && t2>=mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_1][MIN] && t2<mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_1][MAX])
//     return MANDELSTAMTBOTHSIDES_1; else
//   if((t1<t2?t1:t2)>=mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_1][MIN] && (t1<t2?t1:t2)<mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_1][MAX] && (t1<t2?t2:t1)>=mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_2][MIN] && (t1<t2?t2:t1)<mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_2][MAX])
//     return MANDELSTAMTBOTHSIDES_2; else
//   if(t1>=mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_2][MIN] && t1<mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_2][MAX] && t2>=mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_2][MIN] && t2<mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_2][MAX])
//     return MANDELSTAMTBOTHSIDES_3; else{
//       std::cerr << "ERROR in Util::mandelstamTBothSidesRange(Double_t, Double_t): at least one argument not within expected limits!" << std::endl;
//       return nMandelstamTBothSidesRanges;
//   }
// }
// 
// Util::MANDELSTAMTRANGE Util::mandelstamTRange(Double_t t) const{
//   if(t>=mTRangeLimits[TRANGE_1][MIN] && t<mTRangeLimits[TRANGE_1][MAX])
//     return TRANGE_1; else
//   if(t>=mTRangeLimits[TRANGE_2][MIN] && t<mTRangeLimits[TRANGE_2][MAX])
//     return TRANGE_2; else
//   if(t>=mTRangeLimits[TRANGE_3][MIN] && t<mTRangeLimits[TRANGE_3][MAX])
//     return TRANGE_3; else{
//     return nTRanges;
//   }
// }


Double_t Util::bkgdFraction(const TH1* hNSigmaPtMiss, Double_t nSigmaPtMissCut, Double_t fitLimitMin, Double_t fitLimitMax, TF1 *funcReturn) const{
  static int funcId; ++funcId;
  TString idStr; idStr.Form("func%d", funcId);
  TF1* func = new TF1(idStr, "[0]*x+[1]*x*x", 0, fitLimitMax);
  func->SetNpx(1e3);
  func->SetParameter(0, 1);
  func->SetParameter(1, -1);
  double bkgdFrac = 1e9;
  if(hNSigmaPtMiss){
    const int firstBinInFitRange = hNSigmaPtMiss->GetXaxis()->FindBin(fitLimitMin);
    const int lastBinInFitRange = hNSigmaPtMiss->GetXaxis()->FindBin(fitLimitMax)-1;
    const int nBins = lastBinInFitRange - firstBinInFitRange + 1;
    TH1F* hTemp = new TH1F();
    hNSigmaPtMiss->Copy( *hTemp );
    hTemp->SetName("VeryTemp");
    const int nRebin = static_cast<int>( nBins/4 ); // so that there are 4 bins to fit
    if( nRebin>1 )
      hTemp->Rebin(nRebin);
    hTemp->Fit( func, "MQNI", "", fitLimitMin, fitLimitMax);
    if( fabs(func->GetParameter(0)-1)<0.001 && fabs(func->GetParameter(1)+1)<0.001 ){
      bkgdFrac = 0;
    } else{
      double integralOfFunc = func->Integral(0, nSigmaPtMissCut);
      if( integralOfFunc<0 ){
        func->FixParameter(1, 0);
        hTemp->Fit( func, "MQNI", "", fitLimitMin, fitLimitMax);
        integralOfFunc = func->Integral(0, nSigmaPtMissCut);
      }
      double nBkgdEvents = integralOfFunc / hNSigmaPtMiss->GetBinWidth(1);
      double nAllEvents = integratePtMiss(hNSigmaPtMiss, nSigmaPtMissCut);
      bkgdFrac = nBkgdEvents/nAllEvents;
    }
    if( nRebin>1 ){
      func->SetParameter(0, func->GetParameter(0)/nRebin );
      func->SetParameter(1, func->GetParameter(1)/nRebin );
    }
    delete hTemp;
  } else std::cerr << "ERROR in getBkgdFraction()" << std::endl;
  
  if(funcReturn) *funcReturn = *func;
  else delete func;
  return bkgdFrac;
}


Double_t Util::integratePtMiss(const TH1* hNSigmaPtMiss, Double_t nSigmaPtMissCut) const{
  int binNum = 1; 
  for(int i=1; i<=hNSigmaPtMiss->GetNbinsX(); ++i) if(hNSigmaPtMiss->GetXaxis()->GetBinUpEdge(i) >= nSigmaPtMissCut ){ binNum = i; break; }
  double integral = hNSigmaPtMiss->Integral(1, binNum-1);
  integral += hNSigmaPtMiss->GetBinContent(binNum) * ( nSigmaPtMissCut - hNSigmaPtMiss->GetXaxis()->GetBinLowEdge(binNum) ) / hNSigmaPtMiss->GetXaxis()->GetBinWidth(binNum);
  return integral;
}


std::vector<float> Util::getBinsVectorF(const TAxis *axis) const{
  std::vector<float> binsVector;
  for( int i=1; i<=axis->GetNbins(); ++i)
    binsVector.push_back( axis->GetBinLowEdge( i ) );
  binsVector.push_back( axis->GetBinUpEdge( axis->GetNbins() ) );
  return binsVector;
}


std::vector<double> Util::getBinsVectorD(const TAxis *axis) const{
  std::vector<double> binsVector;
  for( int i=1; i<=axis->GetNbins(); ++i)
    binsVector.push_back( axis->GetBinLowEdge( i ) );
  binsVector.push_back( axis->GetBinUpEdge( axis->GetNbins() ) );
  return binsVector;
}


TH1F* Util::bkgdHistogram(const TH2* hNSigmaPtMissVsX, Double_t nSigmaPtMissCut, Double_t fitLimitMin, Double_t fitLimitMax, Int_t mode, vector<TF1*> * funcVec) const{
  std::vector<float> binsVector = getBinsVectorF( hNSigmaPtMissVsX->GetXaxis() );
  TH1F *hBkgd = new TH1F( TString("Background_")+hNSigmaPtMissVsX->GetName(), TString("Background_")+hNSigmaPtMissVsX->GetTitle(), binsVector.size()-1, &(binsVector[0]) );
  //BEGIN             
  TH1D *missingPtHist;
  //previous version:
  missingPtHist = hNSigmaPtMissVsX->ProjectionY("tmpNameProjectionY");
  //current version (added)
  double backgroundIntegral_FullPtMiss = missingPtHist->Integral( missingPtHist->FindBin( fitLimitMin ), missingPtHist->FindBin( fitLimitMax ) );
  missingPtHist->Reset("ICESM");
  for(int i=0; i<=(hNSigmaPtMissVsX->GetNbinsX()+1); ++i){
    TH1D* singleBinProjection = hNSigmaPtMissVsX->ProjectionY("tmpNameProjectionY_singleBin", i, i);
    double singnalRegionIntegral = integratePtMiss( singleBinProjection, nSigmaPtMissCut );
    if( singnalRegionIntegral>0 )
      missingPtHist->Add( singleBinProjection );
  }
  double backgroundIntegral_SlicedPtMiss = missingPtHist->Integral( missingPtHist->FindBin( fitLimitMin ), missingPtHist->FindBin( fitLimitMax ) );
  //END               
  const double integralLimit_MIN = missingPtHist->GetXaxis()->GetBinLowEdge( missingPtHist->GetXaxis()->FindBin( fitLimitMin ) );
  const double integralLimit_MAX = missingPtHist->GetXaxis()->GetBinUpEdge( missingPtHist->GetXaxis()->FindBin( fitLimitMax ) );
  if( mode==0 ){ // default mode
    TF1 *pTMissExtrapolationFunc = new TF1();
    double bkgdFrac = bkgdFraction( missingPtHist, nSigmaPtMissCut, fitLimitMin, fitLimitMax, pTMissExtrapolationFunc);
    const double integralRatio_signalRegion_to_bkgdFreeRegion = pTMissExtrapolationFunc->Integral(0, nSigmaPtMissCut) / pTMissExtrapolationFunc->Integral(integralLimit_MIN, integralLimit_MAX);
    //     double nBkgdEvents = bkgdFrac*integratePtMiss( missingPtHist, nSigmaPtMissCut );
    for(int i=0; i<=(hBkgd->GetNbinsX()+1); ++i){
      TH1D* hNSigmaPtMissProj = hNSigmaPtMissVsX->ProjectionY("tmpNameProjectionY_singleBin", i, i);
      double nBkgdInPtMissBin = (backgroundIntegral_FullPtMiss / backgroundIntegral_SlicedPtMiss) * integralRatio_signalRegion_to_bkgdFreeRegion * hNSigmaPtMissProj->Integral( hNSigmaPtMissProj->FindBin( fitLimitMin ), hNSigmaPtMissProj->FindBin( fitLimitMax ) );
      double singnalRegionIntegral = integratePtMiss( hNSigmaPtMissProj, nSigmaPtMissCut );
      hBkgd->SetBinContent(i, nBkgdInPtMissBin > singnalRegionIntegral ? singnalRegionIntegral : nBkgdInPtMissBin );
      hBkgd->SetBinError(i, 0.0001 * hBkgd->GetBinContent(i) );
//       hBkgd->SetBinError(i, sqrt(hBkgd->GetBinContent(i)) ); //ALERT
    }
    if(funcVec) funcVec->push_back( pTMissExtrapolationFunc );
  } else{
    for(int i=0; i<=(hBkgd->GetNbinsX()+1); ++i){
      TF1 *pTMissExtrapolationFunc = new TF1();
      TH1D* hNSigmaPtMissProj = hNSigmaPtMissVsX->ProjectionY("tmpNameProjectionY_singleBin", i, i);
      double bkgdFrac = bkgdFraction( hNSigmaPtMissProj, nSigmaPtMissCut, fitLimitMin, fitLimitMax, pTMissExtrapolationFunc);
      double nBkgdEvents = bkgdFrac*integratePtMiss( hNSigmaPtMissProj, nSigmaPtMissCut );
      hBkgd->SetBinContent(i, nBkgdEvents);
      hBkgd->SetBinError(i, 0);
      if(funcVec) funcVec->push_back( pTMissExtrapolationFunc );
    }
  }
  return hBkgd;
}



TH1F* Util::backgroundFromSameSignTemplate( TH2F* const hNSigmaPtMissVsX, Double_t nSigmaPtMissCut, TH1* const hNSigmaPtMissSameSign) const{
  std::vector<float> binsVector = getBinsVectorF( hNSigmaPtMissVsX->GetXaxis() );
  TH1F *hBkgd = new TH1F( TString("BackgroundFromSameSignTemplate_")+hNSigmaPtMissVsX->GetName(), TString("BackgroundFromSameSignTemplate_")+hNSigmaPtMissVsX->GetTitle(), binsVector.size()-1, &(binsVector[0]) );
  const double minFitLimit = 5;
  const double maxFitLimit = 10;
  const double integralRatio_signalRegion_to_bkgdFreeRegion = integratePtMiss( hNSigmaPtMissSameSign, nSigmaPtMissCut ) / hNSigmaPtMissSameSign->Integral(hNSigmaPtMissSameSign->FindBin(minFitLimit), hNSigmaPtMissSameSign->FindBin(maxFitLimit) );
  for(int i=0; i<=(hBkgd->GetNbinsX()+1); ++i){
    TH1D* hNSigmaPtMissProj = hNSigmaPtMissVsX->ProjectionY("tmpTmpNameProjectionY_singleBin", i, i);
    hBkgd->SetBinContent(i, integralRatio_signalRegion_to_bkgdFreeRegion * hNSigmaPtMissProj->Integral(hNSigmaPtMissProj->FindBin(minFitLimit), hNSigmaPtMissProj->FindBin(maxFitLimit) ));
    hBkgd->SetBinError(i, 0.0001 * hBkgd->GetBinContent(i) );
  }
  return hBkgd;
}


Double_t Util::binomialCoeff(UInt_t n, UInt_t k) const{
  if(k>n){ cerr << "ERROR in Util::binomialCoeff(UInt_t n, UInt_t k):  k>n !!!!" << endl; return 1; }
  double result = 1.0;
  for(unsigned int i=1; i<=k; ++i) result *= static_cast<double>(n-i+1)/static_cast<double>(i);
  return result;
}




void Util::subtractBackground(TH1* hSignalPlusBkgd, const TH1* hBkgd) const{
  if( hSignalPlusBkgd->GetNbinsX() != hBkgd->GetNbinsX() ){
    std::cerr << "ERROR in subtractBackground(): number of bins of signal and background histogram is different. Return" << std::endl;
    return;
  }
  for(int i=0; i<=(hSignalPlusBkgd->GetNbinsX()+1); ++i){
    const double signalPlusBkgdContent = hSignalPlusBkgd->GetBinContent(i);
    if( signalPlusBkgdContent<1e-9 ) continue;
    const double signalPlusBkgdError = hSignalPlusBkgd->GetBinError(i);
    const double backgroundContent = hBkgd->GetBinContent(i);
    const double bkgdFraction = backgroundContent / signalPlusBkgdContent;
    const double bkgdFractionError = sqrt(bkgdFraction*(1.-bkgdFraction) / signalPlusBkgdContent ); //ALERT binomial error approximation
    
    const double pureSignalContent = signalPlusBkgdContent - backgroundContent;
    if( pureSignalContent<0 ) std::cerr << "WARNING in subtractBackground(): Background larger than signal" << std::endl;
    hSignalPlusBkgd->SetBinContent(i, pureSignalContent>0 ? pureSignalContent : 1e-9 ); //ALERT
    

//     hSignalPlusBkgd->SetBinError(i, pureSignalContent>0 ? (signalPlusBkgdError*pureSignalContent/signalPlusBkgdContent) : signalPlusBkgdError ); // ALERT    
    
    double signalError = sqrt( (1.+bkgdFraction)*(1.+bkgdFraction)*signalPlusBkgdError*signalPlusBkgdError   /*+   signalPlusBkgdContent*signalPlusBkgdContent*bkgdFractionError*bkgdFractionError*/ );
    hSignalPlusBkgd->SetBinError(i, signalError );
  }
}




TH2F* Util::swapHistogramAxes(const TH2F *h1, TString name) const{
  Int_t nbinx = h1->GetNbinsX();
  Int_t nbiny = h1->GetNbinsY();
  
  std::vector<float> binsXVec = getBinsVectorF( h1->GetXaxis() );
  std::vector<float> binsYVec = getBinsVectorF( h1->GetYaxis() );
  
  TH2F *h2 = new TH2F(name, name, nbiny, &(binsYVec[0]),nbinx, &(binsXVec[0]));
  
  Double_t c;
  for(int i=0; i<=nbinx+1; i++){
    for(int j=0; j<=nbiny+1; j++){
      h2->SetBinContent(j,i, h1->GetBinContent(i,j) );
      h2->SetBinError(j,i, h1->GetBinError(i,j) );
    }
  }
  return h2;
}
