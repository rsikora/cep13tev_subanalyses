#define ObservableObject_cxx

#include "ObservableObject.hh"
#include <TH2.h> 
#include <TF1.h> 
#include <TF2.h> 
#include <THStack.h> 
#include <TStyle.h> 
#include <TMultiGraph.h> 
#include <TGraph.h> 
#include <TGraph2D.h> 
#include <TGraphAsymmErrors.h> 
#include <TEfficiency.h> 
#include <TFractionFitter.h>
#include <TObjArray.h>
#include <iostream>
#include <utility>
#include <sstream> 
#include <algorithm> 
#include <stdio.h> 
#include <stdlib.h> 
#include <TCanvas.h> 
#include <TNamed.h>
#include <TLegend.h> 
#include <TBox.h>
#include <TGaxis.h> 
#include <vector> 
#include <fstream> 
#include <TString.h> 
#include <TColor.h> 
#include <TLine.h> 
#include <cmath> 
#include <TExec.h>
#include <TEllipse.h>
#include <TFitResultPtr.h> 
#include <TFitResult.h> 
#include <TLatex.h> 
#include <TExec.h>
#include <TMath.h>
#include <TArrow.h>
#include <TPaletteAxis.h>

ClassImp(ObservableObject)


ObservableObject::ObservableObject(TFile *file, TString inputName){
    name = inputName;
    
    for(int ch=0; ch<Util::nChargeSum; ++ch){
        TString chargeSumStr(ch==Util::CH_SUM_0 ? "_ChSum0" : "_ChSumNot0" );
        
        hQ[ch] = dynamic_cast<TH1F*>(file->Get("h_"+name+chargeSumStr));
        if(hQ[ch])
            hQ[ch]->SetDirectory(0);
        hQ_Weighted[ch] = dynamic_cast<TH1F*>(file->Get("h_"+name+chargeSumStr+"_Weighted"));
        if(hQ_Weighted[ch])
            hQ_Weighted[ch]->SetDirectory(0);
        for(int i=0; i<Util::nSystChecks; ++i){
            hQ_Weighted_Syst[ch][i] = dynamic_cast<TH1F*>(file->Get("h_"+name+chargeSumStr+Form("_Weighted_Systematics_%d", i)));
            if(hQ_Weighted_Syst[ch][i])
                hQ_Weighted_Syst[ch][i]->SetDirectory(0);
        }
        
        hNSigmaPtMissVsQ[ch] = dynamic_cast<TH2F*>(file->Get("hNSigmaPtMiss_vs_"+name+chargeSumStr));
        if(hNSigmaPtMissVsQ[ch])
            hNSigmaPtMissVsQ[ch]->SetDirectory(0);
        hNSigmaPtMissVsQ_Weighted[ch] = dynamic_cast<TH2F*>(file->Get("hNSigmaPtMiss_vs_"+name+chargeSumStr+"_Weighted"));
        if(hNSigmaPtMissVsQ_Weighted[ch])
            hNSigmaPtMissVsQ_Weighted[ch]->SetDirectory(0);
    }
}


ObservableObject::~ObservableObject(){
}
