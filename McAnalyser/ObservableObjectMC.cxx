#define ObservableObjectMC_cxx

#include "ObservableObjectMC.hh"
#include <TH2.h> 
#include <TF1.h> 
#include <TF2.h> 
#include <THStack.h> 
#include <TStyle.h> 
#include <TMultiGraph.h> 
#include <TGraph.h> 
#include <TGraph2D.h> 
#include <TGraphAsymmErrors.h> 
#include <TEfficiency.h> 
#include <TFractionFitter.h>
#include <TObjArray.h>
#include <iostream>
#include <utility>
#include <sstream> 
#include <algorithm> 
#include <stdio.h> 
#include <stdlib.h> 
#include <TCanvas.h> 
#include <TNamed.h>
#include <TLegend.h> 
#include <TBox.h>
#include <TGaxis.h> 
#include <vector> 
#include <fstream> 
#include <TString.h> 
#include <TColor.h> 
#include <TLine.h> 
#include <cmath> 
#include <TExec.h>
#include <TEllipse.h>
#include <TFitResultPtr.h> 
#include <TFitResult.h> 
#include <TLatex.h> 
#include <TExec.h>
#include <TMath.h>
#include <TArrow.h>
#include <TPaletteAxis.h>

ClassImp(ObservableObjectMC)


ObservableObjectMC::ObservableObjectMC(void *file, TString inputName, std::vector<double> & binning){
    name = inputName;
    binsVec = binning;
    TString chargeSumStr("_ChSum0");
    
    hQ_Weighted = new TH1F("h_"+name+chargeSumStr+"_Weighted", "h_"+name+chargeSumStr+"_Weighted", nBins(), &binning[0]);
}


ObservableObjectMC::~ObservableObjectMC(){
}
