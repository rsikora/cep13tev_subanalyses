// #define ONLY_RHO

#include <TH1.h>
#include <TH2.h>
#include <TF2.h>
#include <TFile.h>
#include <TTree.h>
#include "TMath.h"
#include <TLorentzVector.h>
#include <TVector2.h>
#include <cmath>
#include <complex>
#include <iostream>
#include <fstream>
#include <TRandom3.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include "Util.hh"
#include <TObjString.h>
#include <TObjArray.h>
#include <TLorentzRotation.h>
#include <TEfficiency.h>
#include <TFitResult.h>
#include <TCanvas.h>
#include "Math/DistFunc.h"
#include "Math/SpecFunc.h"

using namespace std;

struct CosTheta_Phi{
    CosTheta_Phi(): mCosTheta(-1000), mPhi(-1000), mPhiDegrees(-1000){}
    Double_t mCosTheta;
    Double_t mPhi;
    Double_t mPhiDegrees;
};

Util *mUtil;

double minPt[Util::nDefinedParticles];
double oneTrackMaxPt[Util::nDefinedParticles];
const double maxEta = 0.7;
const double initialMomentum = 100.2;

int mParticlesId = Util::nDefinedParticles;
long totalNEvents = 0;
double totalLumi = 0.0;
double specialWeightFactor = 0.01;

TH1F *mhInvMassPidWeighted[Util::nCharges2Trks][Util::nDefinedParticles];
TH1F *mhPairRapidityPidWeighted[Util::nCharges2Trks][Util::nDefinedParticles];
TH1F *mhDeltaPhiDegreesPidWeighted[Util::nCharges2Trks][Util::nDefinedParticles];
TH1F *mhMandelstamTSumPidWeighted[Util::nCharges2Trks][Util::nDefinedParticles];
TH1F *mhDeltaRapidityPidWeighted[Util::nCharges2Trks][Util::nDefinedParticles];
TH1F *mhCosThetaPidWeighted[Util::nCharges2Trks][Util::nDefinedParticles][Util::nReferenceFrames];
TH1F *mhPhiDegreesPidWeighted[Util::nCharges2Trks][Util::nDefinedParticles][Util::nReferenceFrames];

TH1F *mhInvMassPid_DeltaPhiBinsWeighted[Util::nCharges2Trks][Util::nDefinedParticles][Util::nDeltaPhiRanges];

TH1F *mhInvMassPidWeighted_Pion_FineBinning_RapidityCut_NarrowerDeltaPhiBins[Util::nDeltaPhiRanges];

TH1F *mhInvMassPidWeighted_Pion_HighPairMass;
TH1F *mhInvMassPidWeighted_Pion_HighPairMass_HighTrackPt;
TH1F *mhInvMassPidWeighted_Pion_HighPairMass_VHighTrackPt;
TH1F *mhInvMassPidWeighted_Pion_HighPairMass_VVHighTrackPt;

TH1F *mhPairRapidityPid_MassBinsWeighted[Util::nCharges2Trks][Util::nDefinedParticles][Util::nMassRanges];
TH1F *mhDeltaPhiDegreesPid_MassBinsWeighted[Util::nCharges2Trks][Util::nDefinedParticles][Util::nMassRanges];
TH1F *mhMandelstamTSumPid_MassBinsWeighted[Util::nCharges2Trks][Util::nDefinedParticles][Util::nMassRanges];
TH1F *mhDeltaRapidityPid_MassBinsWeighted[Util::nCharges2Trks][Util::nDefinedParticles][Util::nMassRanges];
TH1F *mhCosThetaPid_MassBinsWeighted[Util::nCharges2Trks][Util::nDefinedParticles][Util::nReferenceFrames][Util::nMassRanges];
TH1F *mhPhiDegreesPid_MassBinsWeighted[Util::nCharges2Trks][Util::nDefinedParticles][Util::nReferenceFrames][Util::nMassRanges];

TH1F *mhInvMassPid_KaonBinsWeighted[Util::nCharges2Trks][Util::nDefinedParticles];
TH1F *mhInvMassPid_ProtonBinsWeighted[Util::nCharges2Trks][Util::nDefinedParticles];

TH1F *mhInvMass_DeltaPtBins_DeltaPhiBinsWeighted[Util::nCharges2Trks][Util::nDefinedParticles][Util::nDptDeltaPhiRanges];
TH1F *mhInvMass_DeltaPtBins_DeltaPhiBins_MandelstamTSumBinsWeighted[Util::nCharges2Trks][Util::nDefinedParticles][Util::nDptDeltaPhiRanges][Util::nMandelstamTSumRanges];
TH1F *mhInvMass_DeltaPtBins_DeltaPhiBins_PairRapidityBinsWeighted[Util::nCharges2Trks][Util::nDefinedParticles][Util::nDptDeltaPhiRanges][Util::nPairRapidityRanges];

TH2F *mht1t2[Util::nDeltaPhiRanges][Util::nMassRanges];
TH1F *mhInvMassNoCuts[Util::nDeltaPhiRanges];

void divideByBinWidth(TH1F*);
void createBinnings();
void createHistograms();
void convertToCrossSection();
CosTheta_Phi transformCentralTrks(TLorentzVector trackPlus, TLorentzVector trackMinus, TLorentzVector protonA, TLorentzVector protonB, UInt_t rf);

//enum MC_TYPES {GENEX_PIPI, GENEX_KK, DIME_PIPI, DIME_KK, PYTHIA_PIPI, PYTHIA_KK, PYTHIA_PPBAR, nMcTypes };


enum WAVES_ENUM { S0m_MAG, P0m_MAG, P0m_PH, P1m_MAG, P1m_PH, D0m_MAG, D0m_PH, D1m_MAG, D1m_PH,
                  P1p_MAG, D1p_MAG, D1p_PH,      nWaveParameters };
Double_t intensityFromWaves(const Double_t *x,const  Double_t *par);
void transformCentralTrksToWave(TLorentzVector &, TLorentzVector &, TLorentzVector, TLorentzVector, TF2* const);

TEfficiency *mMassAcceptance_LimitedMandelstamT_S0m[Util::nDeltaPhiRanges];
TEfficiency *mMassAcceptance_LimitedMandelstamT_D0m[Util::nDeltaPhiRanges];
TEfficiency *mMassAcceptance_LimitedMandelstamT_DiMe[Util::nDeltaPhiRanges];

TEfficiency *mAcceptanceForDeltaPhiCuts_Vs_t1t2[Util::nDeltaPhiRanges];
TH2F *mAcceptanceForDeltaPhiCuts_Vs_t1t2_PASSED[Util::nDeltaPhiRanges];
TH2F *mAcceptanceForDeltaPhiCuts_Vs_t1t2_TOTAL[Util::nDeltaPhiRanges];

//------------------------------------------------------------------------

vector<double> pionMassBinsVec;
vector<double> kaonMassBinsVec;
vector<double> protonMassBinsVec;
vector<double> pionMassBinsFineBinningVec;
vector<double> pionHighMassTailBinsVec;  
std::vector<double> tSumBinsVec[Util::nDefinedParticles];
std::vector<double> deltaPhiBinsVec[Util::nDefinedParticles];
std::vector<double> pairRapidityBinsVec[Util::nDefinedParticles];
vector<double> binningT_2D_Vec;



int main(int argc, char *argv[]){
  
    minPt[Util::PION] = 0.2;
    minPt[Util::KAON] = 0.3;
    minPt[Util::PROTON] = 0.4;
    
    oneTrackMaxPt[Util::PION] = 999;
    oneTrackMaxPt[Util::KAON] = 0.7;
    oneTrackMaxPt[Util::PROTON] = 1.1;
  
    TH1::SetDefaultSumw2();
    mUtil = Util::instance();
    vector<TString> fileNameVec[Util::nDefinedParticles];
    vector<double> weightsVec[Util::nDefinedParticles];
    
    createBinnings();
  
  
//     fileNameVec[Util::PION].push_back( "../DiMe/RCF/DiMe_CepTree_pipi_model1_exp.root" ); weightsVec[Util::PION].push_back( 1.0 );
//     fileNameVec[Util::PION].push_back( "../DiMe/RCF/DiMe_CepTree_pipi_model1_orexp.root" ); weightsVec[Util::PION].push_back( 1.0 );
//     fileNameVec[Util::PION].push_back( "../DiMe/RCF/DiMe_CepTree_pipi_model1_power.root" ); weightsVec[Util::PION].push_back( 1.0 );
//     fileNameVec[Util::PION].push_back( "../DiMe/RCF/DiMe_CepTree_pipi_model2_exp.root" ); weightsVec[Util::PION].push_back( 1.0 );
//     fileNameVec[Util::PION].push_back( "../DiMe/RCF/DiMe_CepTree_pipi_model2_orexp.root" ); weightsVec[Util::PION].push_back( 1.0 );
//     fileNameVec[Util::PION].push_back( "../DiMe/RCF/DiMe_CepTree_pipi_model2_power.root" ); weightsVec[Util::PION].push_back( 1.0 );
//     fileNameVec[Util::PION].push_back( "../DiMe/RCF/DiMe_CepTree_pipi_model3_exp.root" ); weightsVec[Util::PION].push_back( 1.0 );
//     fileNameVec[Util::PION].push_back( "../DiMe/RCF/DiMe_CepTree_pipi_model3_orexp.root" ); weightsVec[Util::PION].push_back( 1.0 );
//     fileNameVec[Util::PION].push_back( "../DiMe/RCF/DiMe_CepTree_pipi_model3_power.root" ); weightsVec[Util::PION].push_back( 1.0 );
//     fileNameVec[Util::PION].push_back( "../DiMe/RCF/DiMe_CepTree_pipi_model4_exp.root" ); weightsVec[Util::PION].push_back( 1.0 );
//     fileNameVec[Util::PION].push_back( "../DiMe/RCF/DiMe_CepTree_pipi_model4_orexp.root" ); weightsVec[Util::PION].push_back( 1.0 );
//     fileNameVec[Util::PION].push_back( "../DiMe/RCF/DiMe_CepTree_pipi_model4_power.root" ); weightsVec[Util::PION].push_back( 1.0 );
//     
//     fileNameVec[Util::PION].push_back( "../DiMe/DiMe_CepTree_pipi_Exp_Model1.root" ); weightsVec[Util::PION].push_back( 1.0 );
    
//     //---
//     fileNameVec[Util::PION].push_back( "../pythia8243/STAR_generation/Pythia8p243_CepTree_PomFlux5_DefaultMassThreshold.root" ); weightsVec[Util::PION].push_back( 1.0  );
    fileNameVec[Util::PION].push_back( "../pythia8243/STAR_generation/Pythia8p244_CepTree_PomFlux5.root" ); weightsVec[Util::PION].push_back( 1.0 );
//     fileNameVec[Util::PION].push_back( "../Pythia/Pythia8_CepTree.root" ); weightsVec[Util::PION].push_back( 1.0 );
//     fileNameVec[Util::PION].push_back( "../Pythia/Pythia8_CepTree_minM2eq1p5.root" ); weightsVec[Util::PION].push_back( 1.0 );
    
//     fileNameVec[Util::PION].push_back( "../pythia8243/STAR_generation/Pythia8p244_CepTree_PomFlux5.root" ); weightsVec[Util::PION].push_back( 0.25 );
//     fileNameVec[Util::PION].push_back( "../Pythia/Pythia8_CepTree.root" ); weightsVec[Util::PION].push_back( 0.2831 ); // weight to correct for xsection modification
// //     fileNameVec[Util::PION].push_back( "../DiMe/DiMe_CepTree_pipi_model2_exp_b1.root" ); weightsVec[Util::PION].push_back( 1.0 );
// //     fileNameVec[Util::PION].push_back( "../DiMe/DiMe_CepTree_pipi_noPtEtaCut.root" ); weightsVec[Util::PION].push_back( 1.0 );
//     fileNameVec[Util::PION].push_back( "../GenEx/GenEx_Lambda1p0_pipi.root" ); weightsVec[Util::PION].push_back( 0.25 ); // absorption factor // version before 14 Oct 2019
//     fileNameVec[Util::PION].push_back( "../GenEx/GenEx_BestSample_LambdaSq_1p0_pipi.root" ); weightsVec[Util::PION].push_back( 0.25 ); // absorption factor // version since 14 Oct 2019
// //     fileNameVec[Util::PION].push_back( "../SuperChic3/txt2root/SuperChic3_CepTree_pass1_pass1.root" ); weightsVec[Util::PION].push_back( 1.0 );
// //     fileNameVec[Util::PION].push_back( "../SuperChic3/txt2root/SuperChic3_CepTree_chic0.root" ); weightsVec[Util::PION].push_back( 1.0 );
// //     fileNameVec[Util::PION].push_back( "../SuperChic3/txt2root/SuperChic3_CepTree_chic0_2.root" ); weightsVec[Util::PION].push_back( 1.0 );
// //     fileNameVec[Util::PION].push_back( "../SuperChic3/txt2root/SuperChic3_CepTree_chic0_3.root" ); weightsVec[Util::PION].push_back( 1.0 );
// //     
// //     fileNameVec[Util::PION].push_back( "../DiMe/DiMe_CepTree_pipi_exp_fsiTrue.root" ); weightsVec[Util::PION].push_back( 1.0 );
// //     fileNameVec[Util::PION].push_back( "../DiMe/DiMe_CepTree_pipi_exp_fsiFalse.root" ); weightsVec[Util::PION].push_back( 1.0 );
// //     fileNameVec[Util::PION].push_back( "../DiMe/DiMe_CepTree_pipi_orexp_fsiTrue.root" ); weightsVec[Util::PION].push_back( 1.0 );
// //     fileNameVec[Util::PION].push_back( "../DiMe/DiMe_CepTree_pipi_orexp_fsiFalse.root" ); weightsVec[Util::PION].push_back( 1.0 );
// //     fileNameVec[Util::PION].push_back( "../DiMe/DiMe_CepTree_pipi_power_fsiTrue.root" ); weightsVec[Util::PION].push_back( 1.0 );
// //     fileNameVec[Util::PION].push_back( "../DiMe/DiMe_CepTree_pipi_power_fsiFalse.root" ); weightsVec[Util::PION].push_back( 1.0 );
// //     
//     fileNameVec[Util::PION].push_back( "../DiMe/DiMe_CepTree_pipi_CutPairRapidityMandelstamT.root" ); weightsVec[Util::PION].push_back( 1.0 );
    
//     fileNameVec[Util::KAON].push_back( "../pythia8243/STAR_generation/Pythia8p243_CepTree_PomFlux5_DefaultMassThreshold.root" ); weightsVec[Util::KAON].push_back( 1.0  );
    fileNameVec[Util::KAON].push_back( "../pythia8243/STAR_generation/Pythia8p244_CepTree_PomFlux5.root" ); weightsVec[Util::KAON].push_back( 1.0 );
//     fileNameVec[Util::KAON].push_back( "../Pythia/Pythia8_CepTree.root" ); weightsVec[Util::KAON].push_back( 1.0 );
//     fileNameVec[Util::KAON].push_back( "../Pythia/Pythia8_CepTree_minM2eq1p5.root" ); weightsVec[Util::KAON].push_back( 1.0 );
    
//     fileNameVec[Util::KAON].push_back( "../pythia8243/STAR_generation/Pythia8p244_CepTree_PomFlux5.root" ); weightsVec[Util::KAON].push_back( 0.25 );
//     fileNameVec[Util::KAON].push_back( "../Pythia/Pythia8_CepTree.root" ); weightsVec[Util::KAON].push_back( 0.2831 ); // weight to correct for xsection modification
//     fileNameVec[Util::KAON].push_back( "../DiMe/DiMe_CepTree_kk_model2_exp_b1.root" ); weightsVec[Util::KAON].push_back( 1.0 ); //DiMe bug corr
//       fileNameVec[Util::KAON].push_back( "../DiMe/DiMe_CepTree_kk_Exp_Model1.root" ); weightsVec[Util::KAON].push_back( 1.0 );
//     fileNameVec[Util::KAON].push_back( "../GenEx/GenEx_Lambda1p0_kk.root" ); weightsVec[Util::KAON].push_back( 0.45 ); // absorption factor
//     
//     fileNameVec[Util::PROTON].push_back( "../pythia8243/STAR_generation/Pythia8p243_CepTree_PomFlux5_DefaultMassThreshold.root" ); weightsVec[Util::PROTON].push_back( 1.0  );
    fileNameVec[Util::PROTON].push_back( "../pythia8243/STAR_generation/Pythia8p244_CepTree_PomFlux5.root" ); weightsVec[Util::PROTON].push_back( 1.0 );
//     fileNameVec[Util::PROTON].push_back( "../Pythia/Pythia8_CepTree.root" ); weightsVec[Util::PROTON].push_back( 1.0 );
//     fileNameVec[Util::PROTON].push_back( "../Pythia/Pythia8_CepTree_minM2eq1p5.root" ); weightsVec[Util::PROTON].push_back( 1.0 );
    
//     fileNameVec[Util::PROTON].push_back( "../pythia8243/STAR_generation/Pythia8p244_CepTree_PomFlux5.root" ); weightsVec[Util::PROTON].push_back( 0.25 );
//     fileNameVec[Util::PROTON].push_back( "../Pythia/Pythia8_CepTree.root" ); weightsVec[Util::PROTON].push_back( 0.2831 ); // weight to correct for xsection modification
    
    
    #ifdef ONLY_RHO
    fileNameVec[Util::PION].push_back( "../SuperChic3/txt2root/SuperChic3_CepTree_rho0_3_rho0_3.root" ); weightsVec[Util::PION].push_back( 1.0 );
    #endif
    
    
    
    // ------- angular studies -------
    TF2 *angularDist_S0m = new TF2("angularDist_S0m", intensityFromWaves, -TMath::Pi(), TMath::Pi(), 0, TMath::Pi(), 12);
    TF2 *angularDist_D0m = new TF2("angularDist_D0m", intensityFromWaves, -TMath::Pi(), TMath::Pi(), 0, TMath::Pi(), 12);
    angularDist_S0m->SetNpx(5e2); angularDist_S0m->SetNpy(5e2);
    angularDist_D0m->SetNpx(5e2); angularDist_D0m->SetNpy(5e2);
    for(int i=0; i<nWaveParameters; ++i){
      angularDist_S0m->FixParameter(i, 0.0);
      angularDist_D0m->FixParameter(i, 0.0);
    }
    //S-wave
    angularDist_S0m->FixParameter(S0m_MAG, 1.0);
    //D-wave
    angularDist_D0m->FixParameter(D0m_MAG, 1.0);
    // ------- ------- ------- -------
    
    
    for(int particle=0; particle<Util::nDefinedParticles; ++particle){
        mParticlesId = particle;
        
        double xSecInMassRange[2];
        
        for(unsigned int file=0; file<fileNameVec[particle].size(); ++file){
            
          xSecInMassRange[0] = 0.0;
          xSecInMassRange[1] = 0.0;
          
            const double weightPid = weightsVec[particle][file];
            totalNEvents = 0;
            totalLumi = 0;
            
            TFile *fInput = new TFile(fileNameVec[particle][file], "READ");
            
            TTreeReader cepTreeReader("CepTree", fInput);
            TTreeReaderValue<int> pid(cepTreeReader, "pid");
            TTreeReaderValue<float> luminosity(cepTreeReader, "luminosity");
            TTreeReaderValue<float> pxPlus(cepTreeReader, "pxPlus");
            TTreeReaderValue<float> pxMinus(cepTreeReader, "pxMinus");
            TTreeReaderValue<float> pyPlus(cepTreeReader, "pyPlus");
            TTreeReaderValue<float> pyMinus(cepTreeReader, "pyMinus");
            TTreeReaderValue<float> pzPlus(cepTreeReader, "pzPlus");
            TTreeReaderValue<float> pzMinus(cepTreeReader, "pzMinus");
            TTreeReaderValue<float> pxProton1(cepTreeReader, "pxProton1");
            TTreeReaderValue<float> pxProton2(cepTreeReader, "pxProton2");
            TTreeReaderValue<float> pyProton1(cepTreeReader, "pyProton1");
            TTreeReaderValue<float> pyProton2(cepTreeReader, "pyProton2");
            TTreeReaderValue<float> pzProton1(cepTreeReader, "pzProton1");
            TTreeReaderValue<float> pzProton2(cepTreeReader, "pzProton2");
            
            TObjArray *tokenizedName = fileNameVec[particle][file].Tokenize("/");
            TString nameStr = ((TObjString *)(tokenizedName->At(tokenizedName->GetLast())))->String();
            TFile *fHistFile = new TFile("Output_"+mUtil->particleName(particle)+"_"+nameStr, "RECREATE");
            createHistograms();
            
            std::cout << "Now running " << mUtil->particleName(particle) << ", file " << (file+1) << "/" << fileNameVec[particle].size() << ":  " << nameStr << std::endl;
            
            while( cepTreeReader.Next() ){
                ++totalNEvents;
                
                if( *pid == particle ){
                    
                    TLorentzVector proton4Vec[Util::nSides];
                    TLorentzVector centralParticle4Vec[Util::nSigns];
                    TLorentzVector centralParticle4Vec_S0m[Util::nSigns];
                    TLorentzVector centralParticle4Vec_D0m[Util::nSigns];
                    
                    proton4Vec[Util::E] = TLorentzVector( *pxProton2, *pyProton2, *pzProton2, sqrt( pow(*pxProton2,2)+pow(*pyProton2,2)+pow(*pzProton2,2) + pow(mUtil->mass(Util::PROTON),2) ) );
                    proton4Vec[Util::W] = TLorentzVector( *pxProton1, *pyProton1, *pzProton1, sqrt( pow(*pxProton1,2)+pow(*pyProton1,2)+pow(*pzProton1,2) + pow(mUtil->mass(Util::PROTON),2) ) );

                    centralParticle4Vec[Util::PLUS] = TLorentzVector( *pxPlus, *pyPlus, *pzPlus, sqrt( pow(*pxPlus,2)+pow(*pyPlus,2)+pow(*pzPlus,2) + pow(mUtil->mass(static_cast<Util::PARTICLE_NAME>(mParticlesId)),2) ) );
                    centralParticle4Vec[Util::MINUS] = TLorentzVector( *pxMinus, *pyMinus, *pzMinus, sqrt( pow(*pxMinus,2)+pow(*pyMinus,2)+pow(*pzMinus,2) + pow(mUtil->mass(static_cast<Util::PARTICLE_NAME>(mParticlesId)),2) ) );
                    centralParticle4Vec_S0m[Util::PLUS] = centralParticle4Vec[Util::PLUS];
                    centralParticle4Vec_S0m[Util::MINUS] = centralParticle4Vec[Util::MINUS];
                    centralParticle4Vec_D0m[Util::PLUS] = centralParticle4Vec[Util::PLUS];
                    centralParticle4Vec_D0m[Util::MINUS] = centralParticle4Vec[Util::MINUS];
                    
//                     transformCentralTrksToWave( centralParticle4Vec_S0m[Util::PLUS], centralParticle4Vec_S0m[Util::MINUS], proton4Vec[Util::W], proton4Vec[Util::E], angularDist_S0m );
//                     transformCentralTrksToWave( centralParticle4Vec_D0m[Util::PLUS], centralParticle4Vec_D0m[Util::MINUS], proton4Vec[Util::W], proton4Vec[Util::E], angularDist_D0m );
                    
                    //--------- --------- --------- --------- --------- --------- --------- --------- ---------
                    
                    const double mMass = (centralParticle4Vec[0] + centralParticle4Vec[1]).M();
                    const int mMassRange = mUtil->massRange( mMass );
                    const double mPairRapidity = (centralParticle4Vec[0] + centralParticle4Vec[1]).Rapidity();
                    const double mdPt = (proton4Vec[Util::W]-proton4Vec[Util::E]).Pt();
                    
                    TVector2 v1(proton4Vec[0].Px(), proton4Vec[0].Py());
                    TVector2 v2(proton4Vec[1].Px(), proton4Vec[1].Py());
                    double mDeltaPhiDegrees = acos(v1.Unit()*v2.Unit()) * 180. / Util::PI;
                    const int mDeltaPhiRange = mUtil->deltaPhiRange( mDeltaPhiDegrees );
                    const int mNarrowerDeltaPhiRange = (mDeltaPhiDegrees<45. ? Util::DELTAPHI_1 : (mDeltaPhiDegrees>135. ? Util::DELTAPHI_2 : Util::nDeltaPhiRanges));
                    
                    double t[Util::nSides];
                    for(int side=0; side<Util::nSides; ++side)
                      t[side] = -(proton4Vec[side] - TLorentzVector(0,0, (side==0 ? (-1) : 1)*initialMomentum, sqrt(pow(initialMomentum,2) + pow(mUtil->mass(Util::PROTON),2)) )).Mag2();
                    
                    int mDptDeltaPhiRange = Util::nDptDeltaPhiRanges;
                    if( mDeltaPhiRange==Util::DELTAPHI_1)
                      mDptDeltaPhiRange = (mdPt<0.12) ? Util::SMALLDPT_DELTAPHI_1 : Util::LARGEDPT_DELTAPHI_1;
                    else
                      mDptDeltaPhiRange = (mdPt<0.56) ? Util::SMALLDPT_DELTAPHI_2 : Util::LARGEDPT_DELTAPHI_2;
                    
                    const int mMandelstamTSumRange = (fabs(t[Util::E] + t[Util::W]) < 0.16) ? Util::MANDELSTAMTSUM_1 : Util::MANDELSTAMTSUM_2;
                    const int mPairRapidityRange = (fabs(mPairRapidity) < 0.2 ) ? Util::PAIRRAPIDITY_1 : Util::PAIRRAPIDITY_2;
                    //--------- --------- --------- --------- --------- --------- --------- --------- ---------
                    
                    
                    const bool centralTracksPassCuts = centralParticle4Vec[0].Pt() > minPt[mParticlesId] && centralParticle4Vec[1].Pt() > minPt[mParticlesId] &&
                                                        fabs(centralParticle4Vec[0].Eta()) < maxEta && fabs(centralParticle4Vec[1].Eta()) < maxEta;
                    const bool centralTracksPassCuts_S0m = centralParticle4Vec_S0m[0].Pt() > minPt[mParticlesId] && centralParticle4Vec_S0m[1].Pt() > minPt[mParticlesId] &&
                                                            fabs(centralParticle4Vec_S0m[0].Eta()) < maxEta && fabs(centralParticle4Vec_S0m[1].Eta()) < maxEta;
                    const bool centralTracksPassCuts_D0m = centralParticle4Vec_D0m[0].Pt() > minPt[mParticlesId] && centralParticle4Vec_D0m[1].Pt() > minPt[mParticlesId] &&
                                                            fabs(centralParticle4Vec_D0m[0].Eta()) < maxEta && fabs(centralParticle4Vec_D0m[1].Eta()) < maxEta;

                    const bool forwardProtonsPassCuts = proton4Vec[0].Px() > -0.2 && fabs(proton4Vec[0].Py()) > 0.2 && fabs(proton4Vec[0].Py()) < 0.4 && (pow( proton4Vec[0].Px() + 0.3, 2) + pow( proton4Vec[0].Py(), 2 )) < 0.25 &&
                                                        proton4Vec[1].Px() > -0.2 && fabs(proton4Vec[1].Py()) > 0.2 && fabs(proton4Vec[1].Py()) < 0.4 && (pow( proton4Vec[1].Px() + 0.3, 2) + pow( proton4Vec[1].Py(), 2 )) < 0.25;
                    
                                                        
                    if( t[Util::E]>0.05 && t[Util::W]>0.05 && t[Util::E]<0.16 && t[Util::W]<0.16 && (mDeltaPhiDegrees < 45. || mDeltaPhiDegrees > 135.) && fabs(mPairRapidity) < 0.4 ){
                      mMassAcceptance_LimitedMandelstamT_S0m[mDeltaPhiRange]->Fill( centralTracksPassCuts_S0m, mMass );
                      mMassAcceptance_LimitedMandelstamT_D0m[mDeltaPhiRange]->Fill( centralTracksPassCuts_D0m, mMass );
                      mMassAcceptance_LimitedMandelstamT_DiMe[mDeltaPhiRange]->Fill( centralTracksPassCuts, mMass );
                      if( mMass > 0.6 && mMass < 1.7 )
                        xSecInMassRange[mDeltaPhiRange] += 1.0;
                      
                      mht1t2[mDeltaPhiRange][mMassRange]->Fill( t[Util::W], t[Util::E] );
                      mhInvMassNoCuts[mDeltaPhiRange]->Fill( mMass );
                    }
                    
                    
                    if( (mDeltaPhiDegrees < 45. || mDeltaPhiDegrees > 135.) && fabs(mPairRapidity) < 0.4 ){
                      mAcceptanceForDeltaPhiCuts_Vs_t1t2_TOTAL[mDeltaPhiRange]->Fill( t[Util::E], t[Util::W] );
                      if( forwardProtonsPassCuts )
                        mAcceptanceForDeltaPhiCuts_Vs_t1t2_PASSED[mDeltaPhiRange]->Fill( t[Util::E], t[Util::W] );
                      
                      mAcceptanceForDeltaPhiCuts_Vs_t1t2[mDeltaPhiRange]->Fill( forwardProtonsPassCuts, t[Util::E], t[Util::W] );
                    }
                                                        
                                                        
                    
                    if( centralTracksPassCuts && forwardProtonsPassCuts ){
                        
                        bool extraPtCutPassed = true;
                        if( min(centralParticle4Vec[0].Pt(), centralParticle4Vec[1].Pt()) > oneTrackMaxPt[mParticlesId] )
                            extraPtCutPassed = false;
                        
                        if( extraPtCutPassed ){
                            
                            CosTheta_Phi mAngles[Util::nReferenceFrames];
                            for(unsigned int rf=0; rf<Util::nReferenceFrames; ++rf)
                                mAngles[rf] = transformCentralTrks( centralParticle4Vec[Util::PLUS], centralParticle4Vec[Util::MINUS], proton4Vec[Util::W], proton4Vec[Util::E], rf );
                            
                            //BEGIN -------------------------------------------- filling histograms --------------------------------------------
                            mhInvMassPidWeighted[Util::OPPO][mParticlesId]->Fill( mMass, weightPid*specialWeightFactor );
                            #ifndef ONLY_RHO
                            mhPairRapidityPidWeighted[Util::OPPO][mParticlesId]->Fill( mPairRapidity, weightPid*specialWeightFactor );
                            mhDeltaPhiDegreesPidWeighted[Util::OPPO][mParticlesId]->Fill( mDeltaPhiDegrees, weightPid*specialWeightFactor );
                            mhMandelstamTSumPidWeighted[Util::OPPO][mParticlesId]->Fill( t[Util::E]+t[Util::W], weightPid*specialWeightFactor );
                            mhDeltaRapidityPidWeighted[Util::OPPO][mParticlesId]->Fill( centralParticle4Vec[Util::PLUS].Rapidity() - centralParticle4Vec[Util::MINUS].Rapidity(), weightPid*specialWeightFactor );
                            
                            for(unsigned int rf=0; rf<Util::nReferenceFrames; ++rf){
                                mhCosThetaPidWeighted[Util::OPPO][mParticlesId][rf]->Fill( mAngles[rf].mCosTheta, weightPid*specialWeightFactor );
                                mhPhiDegreesPidWeighted[Util::OPPO][mParticlesId][rf]->Fill( mAngles[rf].mPhiDegrees, weightPid*specialWeightFactor );
                            }
                            
                            mhInvMassPid_DeltaPhiBinsWeighted[Util::OPPO][mParticlesId][mDeltaPhiRange]->Fill( mMass, weightPid*specialWeightFactor );
                            
                            mhPairRapidityPid_MassBinsWeighted[Util::OPPO][mParticlesId][mMassRange]->Fill( mPairRapidity, weightPid*specialWeightFactor );
                            mhDeltaPhiDegreesPid_MassBinsWeighted[Util::OPPO][mParticlesId][mMassRange]->Fill( mDeltaPhiDegrees, weightPid*specialWeightFactor );
                            mhMandelstamTSumPid_MassBinsWeighted[Util::OPPO][mParticlesId][mMassRange]->Fill( t[Util::E]+t[Util::W], weightPid*specialWeightFactor );
                            mhDeltaRapidityPid_MassBinsWeighted[Util::OPPO][mParticlesId][mMassRange]->Fill( centralParticle4Vec[Util::PLUS].Rapidity() - centralParticle4Vec[Util::MINUS].Rapidity(), weightPid*specialWeightFactor );
                            for(unsigned int rf=0; rf<Util::nReferenceFrames; ++rf){
                                mhCosThetaPid_MassBinsWeighted[Util::OPPO][mParticlesId][rf][mMassRange]->Fill( mAngles[rf].mCosTheta, weightPid*specialWeightFactor );
                                mhPhiDegreesPid_MassBinsWeighted[Util::OPPO][mParticlesId][rf][mMassRange]->Fill( mAngles[rf].mPhiDegrees, weightPid*specialWeightFactor );
                            }
                            
                            mhInvMassPid_KaonBinsWeighted[Util::OPPO][mParticlesId]->Fill( mMass, weightPid*specialWeightFactor );
                            mhInvMassPid_ProtonBinsWeighted[Util::OPPO][mParticlesId]->Fill( mMass, weightPid*specialWeightFactor );
                            
                            mhInvMass_DeltaPtBins_DeltaPhiBinsWeighted[Util::OPPO][mParticlesId][mDptDeltaPhiRange]->Fill( mMass, weightPid*specialWeightFactor );
                            mhInvMass_DeltaPtBins_DeltaPhiBins_MandelstamTSumBinsWeighted[Util::OPPO][mParticlesId][mDptDeltaPhiRange][mMandelstamTSumRange]->Fill( mMass, weightPid*specialWeightFactor );
                            mhInvMass_DeltaPtBins_DeltaPhiBins_PairRapidityBinsWeighted[Util::OPPO][mParticlesId][mDptDeltaPhiRange][mPairRapidityRange]->Fill( mMass, weightPid*specialWeightFactor );
                            
                            mhInvMassPidWeighted_Pion_HighPairMass->Fill( mMass, weightPid*specialWeightFactor );
                            if( centralParticle4Vec[0].Pt() > 1.0 && centralParticle4Vec[1].Pt() > 1.0 ){
                              mhInvMassPidWeighted_Pion_HighPairMass_HighTrackPt->Fill( mMass, weightPid*specialWeightFactor );
                            }
                            if( centralParticle4Vec[0].Pt() > 1.2 && centralParticle4Vec[1].Pt() > 1.2 ){
                              mhInvMassPidWeighted_Pion_HighPairMass_VHighTrackPt->Fill( mMass, weightPid*specialWeightFactor );
                            }
                            if( centralParticle4Vec[0].Pt() > 1.4 && centralParticle4Vec[1].Pt() > 1.4 ){
                              mhInvMassPidWeighted_Pion_HighPairMass_VVHighTrackPt->Fill( mMass, weightPid*specialWeightFactor );
                            }
                            #endif
                            //END -------------------------------------------- -------------------------------------------- ---------------------------
                        }
                    }
                    
                    #ifndef ONLY_RHO
                    if( forwardProtonsPassCuts ){
                      if( fabs(mPairRapidity) < 0.4 && mNarrowerDeltaPhiRange < Util::nDeltaPhiRanges)
                        mhInvMassPidWeighted_Pion_FineBinning_RapidityCut_NarrowerDeltaPhiBins[mNarrowerDeltaPhiRange]->Fill( mMass, weightPid*specialWeightFactor );
                    }
                    #endif
                    
                } else if( *pid == Util::nDefinedParticles && *luminosity > 0 ){
                    totalLumi += *luminosity;
                }
            }
            
            fInput->Close();
            
            convertToCrossSection();
            
            for(int i=0; i<Util::nDeltaPhiRanges; ++i){
              TH2F *ratio = new TH2F( *mAcceptanceForDeltaPhiCuts_Vs_t1t2_PASSED[i] );
              ratio->SetName( mAcceptanceForDeltaPhiCuts_Vs_t1t2[i]->GetName() + TString("_TH2F") );
              ratio->Divide( mAcceptanceForDeltaPhiCuts_Vs_t1t2_TOTAL[i] );
            }
            
            fHistFile->Write();
            fHistFile->Close();
            
            cout << "File name: " << fileNameVec[particle][file] << "  xSec deltaPhi<45* = " << xSecInMassRange[Util::DELTAPHI_1]/totalLumi << "\txSec deltaPhi>135* = " << xSecInMassRange[Util::DELTAPHI_2]/totalLumi << "\tRatio = " << xSecInMassRange[Util::DELTAPHI_1]/xSecInMassRange[Util::DELTAPHI_2] << endl;
        }
    }

}


void createHistograms(){


//     int nMassBins[Util::nDefinedParticles];
//     nMassBins[Util::PION] = 250;
//     nMassBins[Util::KAON] = 100;
//     nMassBins[Util::PROTON] = 25;
    
    int nCosThetaBins[Util::nDefinedParticles];
    nCosThetaBins[Util::PION] = 25;
    nCosThetaBins[Util::KAON] = 20;
    nCosThetaBins[Util::PROTON] = 10;
    
    int nPhiBins[Util::nDefinedParticles];
    nPhiBins[Util::PION] = 18;
    nPhiBins[Util::KAON] = 18;
    nPhiBins[Util::PROTON] = 9;
    
    
    for(unsigned int r=0; r<Util::nDeltaPhiRanges; ++r){
      for(unsigned int mr=0; mr<Util::nMassRanges; ++mr){
        mht1t2[r][mr] = new TH2F( Form("t1t2_%d%d", r, mr), Form("t1t2_%d%d", r, mr), binningT_2D_Vec.size()-1, &binningT_2D_Vec[0], binningT_2D_Vec.size()-1, &binningT_2D_Vec[0] );
      }
      mhInvMassNoCuts[r] = new TH1F(Form("InvMassNoCuts_%d", r), Form("InvMassNoCuts_%d", r), 100, 0, 2.0);
    }
    
    
    for(int j=0; j<Util::nDefinedParticles; ++j){
        
        if( j!= mParticlesId ) continue;

        for(int i=0; i<Util::SAME; ++i){
                if( j==Util::KAON )
                mhInvMassPidWeighted[i][j] = new TH1F("InvMassPidWeighted_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(j), "Inv. mass, weighted, "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j), kaonMassBinsVec.size()-1, &(kaonMassBinsVec[0]));
                else if( j==Util::PROTON )
                mhInvMassPidWeighted[i][j] = new TH1F("InvMassPidWeighted_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(j), "Inv. mass, weighted, "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j), protonMassBinsVec.size()-1, &(protonMassBinsVec[0]));
                else if( j==Util::PION )
                mhInvMassPidWeighted[i][j] = new TH1F("InvMassPidWeighted_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(j), "Inv. mass, weighted, "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j), pionMassBinsVec.size()-1, &(pionMassBinsVec[0]));
        }
//         #ifndef ONLY_RHO
        for(int i=0; i<Util::SAME; ++i)
          mhPairRapidityPidWeighted[i][j] = new TH1F("PairRapidityPidWeighted_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(j), "Pair rapidity, weighted, "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j), pairRapidityBinsVec[j].size()-1, &(pairRapidityBinsVec[j][0]));
        
        for(int i=0; i<Util::SAME; ++i)
          mhDeltaPhiDegreesPidWeighted[i][j] = new TH1F("DeltaPhiDegreesPidWeighted_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(j), "#Delta#varphi_{pp} [#circ], weighted, "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j), deltaPhiBinsVec[j].size()-1, &deltaPhiBinsVec[j][0]);
        
        for(int i=0; i<Util::SAME; ++i)
          mhMandelstamTSumPidWeighted[i][j] = new TH1F("MandelstamTSumPidWeighted_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(j), "|t|^{E}+|t|^{W} [GeV^{2}/c^{2}], weighted, "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j), tSumBinsVec[j].size()-1, &tSumBinsVec[j][0]);
        
        for(int i=0; i<Util::SAME; ++i)
          mhDeltaRapidityPidWeighted[i][j] = new TH1F("DeltaRapidityWeighted_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(j), "y^{+}-y^{-}, weighted, "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j), j==Util::PION ? 60 : (j==Util::KAON ? 24 : 10), -1.2, 1.2);
        
        for(int i=0; i<Util::SAME; ++i)
                for(int rf=0; rf<Util::nReferenceFrames; ++rf)
                    mhCosThetaPidWeighted[i][j][rf] = new TH1F("CosThetaPidWeighted_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(j)+"_"+mUtil->referenceFrameName(rf), "cos#theta^{"+mUtil->referenceFrameShortName(rf)+"}, weighted, "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j), nCosThetaBins[j], -1, 1);
        
        for(int i=0; i<Util::SAME; ++i)
                for(int rf=0; rf<Util::nReferenceFrames; ++rf)
                    mhPhiDegreesPidWeighted[i][j][rf] = new TH1F("PhiDegreesPidWeighted_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(j)+"_"+mUtil->referenceFrameName(rf), "#varphi^{"+mUtil->referenceFrameShortName(rf)+"}, weighted, "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j), nPhiBins[j], -180, 180);
        
        for(int i=0; i<Util::SAME; ++i)
            for(unsigned int r=0; r<Util::nDeltaPhiRanges; ++r){
                TString deltaPhiRangeShortStr, deltaPhiLimitsStr[2];
                deltaPhiRangeShortStr.Form("deltaPhi_Bin%d", r+1);
                deltaPhiLimitsStr[Util::MIN].Form("%.0f", mUtil->deltaPhiRangeLimit(r, Util::MIN) );
                deltaPhiLimitsStr[Util::MAX].Form("%.0f", mUtil->deltaPhiRangeLimit(r, Util::MAX) );
                if( j==Util::KAON )
                mhInvMassPid_DeltaPhiBinsWeighted[i][j][r] = new TH1F("InvMassPid_DeltaPhiBinsWeighted_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(j)+"_"+deltaPhiRangeShortStr, "Inv. mass [GeV/c^{2}], "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j)+", #Delta#varphi_{pp}#in["+deltaPhiLimitsStr[Util::MIN]+"#circ, "+deltaPhiLimitsStr[Util::MAX]+"#circ], weighted", kaonMassBinsVec.size()-1, &(kaonMassBinsVec[0]));
                else if( j==Util::PROTON )
                mhInvMassPid_DeltaPhiBinsWeighted[i][j][r] = new TH1F("InvMassPid_DeltaPhiBinsWeighted_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(j)+"_"+deltaPhiRangeShortStr, "Inv. mass [GeV/c^{2}], "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j)+", #Delta#varphi_{pp}#in["+deltaPhiLimitsStr[Util::MIN]+"#circ, "+deltaPhiLimitsStr[Util::MAX]+"#circ], weighted", protonMassBinsVec.size()-1, &(protonMassBinsVec[0]));
                else if( j==Util::PION )
                mhInvMassPid_DeltaPhiBinsWeighted[i][j][r] = new TH1F("InvMassPid_DeltaPhiBinsWeighted_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(j)+"_"+deltaPhiRangeShortStr, "Inv. mass [GeV/c^{2}], "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j)+", #Delta#varphi_{pp}#in["+deltaPhiLimitsStr[Util::MIN]+"#circ, "+deltaPhiLimitsStr[Util::MAX]+"#circ], weighted", pionMassBinsVec.size()-1, &(pionMassBinsVec[0]));
            }
        
        for(int i=0; i<Util::SAME; ++i)
            for(unsigned int r=0; r<Util::nMassRanges; ++r){
                TString massRangeShortStr, massLimitsStr[2];
                massRangeShortStr.Form("mass_Bin%d", r+1);
                massLimitsStr[Util::MIN].Form("%.1f", mUtil->massRangeLimit(r, Util::MIN) );
                massLimitsStr[Util::MAX].Form("%.1f", mUtil->massRangeLimit(r, Util::MAX) );
                if( r==(Util::nMassRanges-1) )
                    massLimitsStr[Util::MAX] = TString("#infty");
                mhMandelstamTSumPid_MassBinsWeighted[i][j][r] = new TH1F("MandelstamTSumPid_MassBinsWeighted_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(j)+"_"+massRangeShortStr, "|t_{1}|+|t_{2}|, "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j)+", m#in["+massLimitsStr[Util::MIN]+", "+massLimitsStr[Util::MAX]+"] GeV/c^{2}, weighted", tSumBinsVec[j].size()-1, &tSumBinsVec[j][0]);
            }
        
        for(int i=0; i<Util::SAME; ++i)
            for(unsigned int r=0; r<Util::nMassRanges; ++r){
                TString massRangeShortStr, massLimitsStr[2];
                massRangeShortStr.Form("mass_Bin%d", r+1);
                massLimitsStr[Util::MIN].Form("%.1f", mUtil->massRangeLimit(r, Util::MIN) );
                massLimitsStr[Util::MAX].Form("%.1f", mUtil->massRangeLimit(r, Util::MAX) );
                if( r==(Util::nMassRanges-1) )
                    massLimitsStr[Util::MAX] = TString("#infty");
                mhDeltaRapidityPid_MassBinsWeighted[i][j][r] = new TH1F("DeltaRapidityPid_MassBinsWeighted_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(j)+"_"+massRangeShortStr, "y^{+}-y^{-}, "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j)+", m#in["+massLimitsStr[Util::MIN]+", "+massLimitsStr[Util::MAX]+"] GeV/c^{2}, weighted", j==Util::PION ? 60 : (j==Util::KAON ? 24 : 10), -1.2, 1.2);
            }
        
        for(int i=0; i<Util::SAME; ++i)
            for(unsigned int r=0; r<Util::nMassRanges; ++r){
                TString massRangeShortStr, massLimitsStr[2];
                massRangeShortStr.Form("mass_Bin%d", r+1);
                massLimitsStr[Util::MIN].Form("%.1f", mUtil->massRangeLimit(r, Util::MIN) );
                massLimitsStr[Util::MAX].Form("%.1f", mUtil->massRangeLimit(r, Util::MAX) );
                if( r==(Util::nMassRanges-1) )
                massLimitsStr[Util::MAX] = TString("#infty");
                mhPairRapidityPid_MassBinsWeighted[i][j][r] = new TH1F("PairRapidityPid_MassBinsWeighted_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(j)+"_"+massRangeShortStr, "y^{pair}, "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j)+", m#in["+massLimitsStr[Util::MIN]+", "+massLimitsStr[Util::MAX]+"] GeV/c^{2}, weighted", pairRapidityBinsVec[j].size()-1, &(pairRapidityBinsVec[j][0]));
            }
        
        for(int i=0; i<Util::SAME; ++i)
            for(unsigned int r=0; r<Util::nMassRanges; ++r){
                TString massRangeShortStr, massLimitsStr[2];
                massRangeShortStr.Form("mass_Bin%d", r+1);
                massLimitsStr[Util::MIN].Form("%.1f", mUtil->massRangeLimit(r, Util::MIN) );
                massLimitsStr[Util::MAX].Form("%.1f", mUtil->massRangeLimit(r, Util::MAX) );
                if( r==(Util::nMassRanges-1) )
                massLimitsStr[Util::MAX] = TString("#infty");
                mhDeltaPhiDegreesPid_MassBinsWeighted[i][j][r] = new TH1F("DeltaPhiDegreesPid_MassBinsWeighted_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(j)+"_"+massRangeShortStr, "y^{pair}, "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j)+", m#in["+massLimitsStr[Util::MIN]+", "+massLimitsStr[Util::MAX]+"] GeV/c^{2}, weighted", deltaPhiBinsVec[j].size()-1, &deltaPhiBinsVec[j][0]);
            }
        
        for(int i=0; i<Util::SAME; ++i)
            for(int rf=0; rf<Util::nReferenceFrames; ++rf)
                for(unsigned int r=0; r<Util::nMassRanges; ++r){
                    TString massRangeShortStr, massLimitsStr[2];
                    massRangeShortStr.Form("mass_Bin%d", r+1);
                    massLimitsStr[Util::MIN].Form("%.1f", mUtil->massRangeLimit(r, Util::MIN) );
                    massLimitsStr[Util::MAX].Form("%.1f", mUtil->massRangeLimit(r, Util::MAX) );
                    if( r==(Util::nMassRanges-1) )
                    massLimitsStr[Util::MAX] = TString("#infty");
                    mhCosThetaPid_MassBinsWeighted[i][j][rf][r] = new TH1F("CosThetaPid_MassBinsWeighted_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(j)+"_"+mUtil->referenceFrameName(rf)+"_"+massRangeShortStr, "y^{pair}, "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j)+", "+mUtil->referenceFrameName(rf)+", m#in["+massLimitsStr[Util::MIN]+", "+massLimitsStr[Util::MAX]+"] GeV/c^{2}, weighted", nCosThetaBins[j], -1, 1);
                }

        for(int i=0; i<Util::SAME; ++i)
            for(int rf=0; rf<Util::nReferenceFrames; ++rf)
                for(unsigned int r=0; r<Util::nMassRanges; ++r){
                    TString massRangeShortStr, massLimitsStr[2];
                    massRangeShortStr.Form("mass_Bin%d", r+1);
                    massLimitsStr[Util::MIN].Form("%.1f", mUtil->massRangeLimit(r, Util::MIN) );
                    massLimitsStr[Util::MAX].Form("%.1f", mUtil->massRangeLimit(r, Util::MAX) );
                    if( r==(Util::nMassRanges-1) )
                    massLimitsStr[Util::MAX] = TString("#infty");
                    mhPhiDegreesPid_MassBinsWeighted[i][j][rf][r] = new TH1F("PhiDegreesPid_MassBinsWeighted_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(j)+"_"+mUtil->referenceFrameName(rf)+"_"+massRangeShortStr, "y^{pair}, "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j)+", "+mUtil->referenceFrameName(rf)+", m#in["+massLimitsStr[Util::MIN]+", "+massLimitsStr[Util::MAX]+"] GeV/c^{2}, weighted", nPhiBins[j], -180, 180);
                }
    
        for(int i=0; i<Util::SAME; ++i)
            mhInvMassPid_KaonBinsWeighted[i][j] = new TH1F("InvMassPid_KaonBinsWeighted_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(j), "Inv. mass, weighted, "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j), kaonMassBinsVec.size()-1, &(kaonMassBinsVec[0]));

        for(int i=0; i<Util::SAME; ++i)
            mhInvMassPid_ProtonBinsWeighted[i][j] = new TH1F("InvMassPid_ProtonBinsWeighted_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(j), "Inv. mass, weighted, "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j), protonMassBinsVec.size()-1, &(protonMassBinsVec[0]));
        
        
        for(int i=0; i<Util::SAME; ++i)
                for(unsigned int r=0; r<Util::nDptDeltaPhiRanges; ++r){
                    TString dptDeltaPhiRangeShortStr, dptDeltaPhiStr;
                    dptDeltaPhiRangeShortStr.Form("dptDeltaPhiBin_%d", r+1);
                    switch(r){
                        case Util::SMALLDPT_DELTAPHI_1: dptDeltaPhiStr = TString("small dp_{T}^{PomPom}, #Delta#varphi < 90^{#circ}"); break;
                        case Util::LARGEDPT_DELTAPHI_1: dptDeltaPhiStr = TString("large dp_{T}^{PomPom}, #Delta#varphi < 90^{#circ}"); break;
                        case Util::SMALLDPT_DELTAPHI_2: dptDeltaPhiStr = TString("small dp_{T}^{PomPom}, #Delta#varphi > 90^{#circ}"); break;
                        case Util::LARGEDPT_DELTAPHI_2: dptDeltaPhiStr = TString("large dp_{T}^{PomPom}, #Delta#varphi > 90^{#circ}"); break;
                        default: break;
                    }
                    if( j==Util::KAON )
                        mhInvMass_DeltaPtBins_DeltaPhiBinsWeighted[i][j][r] = new TH1F("InvMass_DeltaPtBins_DeltaPhiBinsWeighted_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(j)+"_"+dptDeltaPhiRangeShortStr, "Inv. mass [GeV/c^{2}], weighted, "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j)+", "+dptDeltaPhiStr, kaonMassBinsVec.size()-1, &(kaonMassBinsVec[0]));
                    else if( j==Util::PROTON )
                        mhInvMass_DeltaPtBins_DeltaPhiBinsWeighted[i][j][r] = new TH1F("InvMass_DeltaPtBins_DeltaPhiBinsWeighted_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(j)+"_"+dptDeltaPhiRangeShortStr, "Inv. mass [GeV/c^{2}], weighted, "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j)+", "+dptDeltaPhiStr, protonMassBinsVec.size()-1, &(protonMassBinsVec[0]));
                    else if( j==Util::PION )
                        mhInvMass_DeltaPtBins_DeltaPhiBinsWeighted[i][j][r] = new TH1F("InvMass_DeltaPtBins_DeltaPhiBinsWeighted_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(j)+"_"+dptDeltaPhiRangeShortStr, "Inv. mass [GeV/c^{2}], weighted, "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j)+", "+dptDeltaPhiStr, pionMassBinsVec.size()-1, &(pionMassBinsVec[0]));
                }
        

        
        for(int i=0; i<Util::SAME; ++i)
                for(unsigned int r=0; r<Util::nDptDeltaPhiRanges; ++r){
                    TString dptDeltaPhiRangeShortStr, dptDeltaPhiStr;
                    dptDeltaPhiRangeShortStr.Form("dptDeltaPhiBin_%d", r+1);
                    switch(r){
                        case Util::SMALLDPT_DELTAPHI_1: dptDeltaPhiStr = TString("small dp_{T}^{PomPom}, #Delta#varphi < 90^{#circ}"); break;
                        case Util::LARGEDPT_DELTAPHI_1: dptDeltaPhiStr = TString("large dp_{T}^{PomPom}, #Delta#varphi < 90^{#circ}"); break;
                        case Util::SMALLDPT_DELTAPHI_2: dptDeltaPhiStr = TString("small dp_{T}^{PomPom}, #Delta#varphi > 90^{#circ}"); break;
                        case Util::LARGEDPT_DELTAPHI_2: dptDeltaPhiStr = TString("large dp_{T}^{PomPom}, #Delta#varphi > 90^{#circ}"); break;
                        default: break;
                    }
                    for(unsigned int s=0; s<Util::nMandelstamTSumRanges; ++s){
                        TString mandelstamTSumBinStr = Form("mandelstamTSumBin_%d", s+1);
                        if( j==Util::KAON )
                            mhInvMass_DeltaPtBins_DeltaPhiBins_MandelstamTSumBinsWeighted[i][j][r][s] = new TH1F("InvMass_DeltaPtBins_DeltaPhiBins_MandelstamTSumBinsWeighted_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(j)+"_"+dptDeltaPhiRangeShortStr+"_"+mandelstamTSumBinStr, "Inv. mass [GeV/c^{2}], "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j)+", "+dptDeltaPhiStr+", "+mandelstamTSumBinStr, kaonMassBinsVec.size()-1, &(kaonMassBinsVec[0]));
                        else if( j==Util::PROTON )
                            mhInvMass_DeltaPtBins_DeltaPhiBins_MandelstamTSumBinsWeighted[i][j][r][s] = new TH1F("InvMass_DeltaPtBins_DeltaPhiBins_MandelstamTSumBinsWeighted_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(j)+"_"+dptDeltaPhiRangeShortStr+"_"+mandelstamTSumBinStr, "Inv. mass [GeV/c^{2}], "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j)+", "+dptDeltaPhiStr+", "+mandelstamTSumBinStr, protonMassBinsVec.size()-1, &(protonMassBinsVec[0]));
                        else if( j==Util::PION )
                            mhInvMass_DeltaPtBins_DeltaPhiBins_MandelstamTSumBinsWeighted[i][j][r][s] = new TH1F("InvMass_DeltaPtBins_DeltaPhiBins_MandelstamTSumBinsWeighted_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(j)+"_"+dptDeltaPhiRangeShortStr+"_"+mandelstamTSumBinStr, "Inv. mass [GeV/c^{2}], "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j)+", "+dptDeltaPhiStr+", "+mandelstamTSumBinStr, pionMassBinsVec.size()-1, &(pionMassBinsVec[0]));
                    }
                }
        
        
        for(int i=0; i<Util::SAME; ++i)
                for(unsigned int r=0; r<Util::nDptDeltaPhiRanges; ++r){
                    TString dptDeltaPhiRangeShortStr, dptDeltaPhiStr;
                    dptDeltaPhiRangeShortStr.Form("dptDeltaPhiBin_%d", r+1);
                    switch(r){
                        case Util::SMALLDPT_DELTAPHI_1: dptDeltaPhiStr = TString("small dp_{T}^{PomPom}, #Delta#varphi < 90^{#circ}"); break;
                        case Util::LARGEDPT_DELTAPHI_1: dptDeltaPhiStr = TString("large dp_{T}^{PomPom}, #Delta#varphi < 90^{#circ}"); break;
                        case Util::SMALLDPT_DELTAPHI_2: dptDeltaPhiStr = TString("small dp_{T}^{PomPom}, #Delta#varphi > 90^{#circ}"); break;
                        case Util::LARGEDPT_DELTAPHI_2: dptDeltaPhiStr = TString("large dp_{T}^{PomPom}, #Delta#varphi > 90^{#circ}"); break;
                        default: break;
                    }
                    for(unsigned int s=0; s<Util::nPairRapidityRanges; ++s){
                        TString mandelstamTSumBinStr = Form("pairRapidityBin_%d", s+1);
                        if( j==Util::KAON )
                            mhInvMass_DeltaPtBins_DeltaPhiBins_PairRapidityBinsWeighted[i][j][r][s] = new TH1F("InvMass_DeltaPtBins_DeltaPhiBins_PairRapidityBinsWeighted_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(j)+"_"+dptDeltaPhiRangeShortStr+"_"+mandelstamTSumBinStr, "Inv. mass [GeV/c^{2}], "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j)+", "+dptDeltaPhiStr+", "+mandelstamTSumBinStr, kaonMassBinsVec.size()-1, &(kaonMassBinsVec[0]));
                        else if( j==Util::PROTON )
                            mhInvMass_DeltaPtBins_DeltaPhiBins_PairRapidityBinsWeighted[i][j][r][s] = new TH1F("InvMass_DeltaPtBins_DeltaPhiBins_PairRapidityBinsWeighted_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(j)+"_"+dptDeltaPhiRangeShortStr+"_"+mandelstamTSumBinStr, "Inv. mass [GeV/c^{2}], "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j)+", "+dptDeltaPhiStr+", "+mandelstamTSumBinStr, protonMassBinsVec.size()-1, &(protonMassBinsVec[0]));
                        else if( j==Util::PION )
                            mhInvMass_DeltaPtBins_DeltaPhiBins_PairRapidityBinsWeighted[i][j][r][s] = new TH1F("InvMass_DeltaPtBins_DeltaPhiBins_PairRapidityBinsWeighted_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(j)+"_"+dptDeltaPhiRangeShortStr+"_"+mandelstamTSumBinStr, "Inv. mass [GeV/c^{2}], "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j)+", "+dptDeltaPhiStr+", "+mandelstamTSumBinStr, pionMassBinsVec.size()-1, &(pionMassBinsVec[0]));
                    }
                }
        
        for(int i=0; i<Util::SAME; ++i)
          for(unsigned int r=0; r<Util::nDeltaPhiRanges; ++r){
            TString deltaPhiRangeShortStr, deltaPhiLimitsStr[2];
            deltaPhiRangeShortStr.Form("deltaPhi_NarrowerBin%d", r+1);
            deltaPhiLimitsStr[Util::MIN] = TString(r==0? "0" : "135");
            deltaPhiLimitsStr[Util::MAX] = TString(r==0? "45": "180" );
            mhInvMassPidWeighted_Pion_FineBinning_RapidityCut_NarrowerDeltaPhiBins[r] = new TH1F("InvMassPidWeighted_Pion_FineBinning_RapidityCut_NarrowerDeltaPhiBins_"+deltaPhiRangeShortStr, "Inv. mass, weighted, "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(j)+", #Delta#varphi_{pp}#in["+deltaPhiLimitsStr[Util::MIN]+"#circ, "+deltaPhiLimitsStr[Util::MAX]+"#circ]", pionMassBinsFineBinningVec.size()-1, &(pionMassBinsFineBinningVec[0]));  
          }
//     #endif
    }
//     #ifndef ONLY_RHO
    mhInvMassPidWeighted_Pion_HighPairMass = new TH1F("InvMassPidWeighted_Pion_HighPairMass", "Inv. mass, weighted, high pair mass", pionHighMassTailBinsVec.size()-1, &(pionHighMassTailBinsVec[0]));
    mhInvMassPidWeighted_Pion_HighPairMass_HighTrackPt = new TH1F("InvMassPidWeighted_Pion_HighPairMass_HighTrackPt", "Inv. mass, weighted, high pair mass and high track p_{T}", pionHighMassTailBinsVec.size()-1, &(pionHighMassTailBinsVec[0]));
    mhInvMassPidWeighted_Pion_HighPairMass_VHighTrackPt = new TH1F("InvMassPidWeighted_Pion_HighPairMass_VHighTrackPt", "Inv. mass, weighted, high pair mass and vhigh track p_{T}", pionHighMassTailBinsVec.size()-1, &(pionHighMassTailBinsVec[0]));
    mhInvMassPidWeighted_Pion_HighPairMass_VVHighTrackPt = new TH1F("InvMassPidWeighted_Pion_HighPairMass_VVHighTrackPt", "Inv. mass, weighted, high pair mass and vhigh track p_{T}", pionHighMassTailBinsVec.size()-1, &(pionHighMassTailBinsVec[0]));
//     #endif
    
    
    
    for(int i=0; i<Util::nDeltaPhiRanges; ++i){
      mMassAcceptance_LimitedMandelstamT_S0m[i] = new TEfficiency(Form("mMassAcceptance_LimitedMandelstamT_DeltaPhiNarrowerBin%d_S0m", i+1), Form("mMassAcceptance_LimitedMandelstamT_DeltaPhiNarrowerBin%d_S0m", i+1), 200, 0, 2);
      mMassAcceptance_LimitedMandelstamT_D0m[i] = new TEfficiency(Form("mMassAcceptance_LimitedMandelstamT_DeltaPhiNarrowerBin%d_D0m", i+1), Form("mMassAcceptance_LimitedMandelstamT_DeltaPhiNarrowerBin%d_D0m", i+1), 200, 0, 2);
      mMassAcceptance_LimitedMandelstamT_DiMe[i] = new TEfficiency(Form("mMassAcceptance_LimitedMandelstamT_DeltaPhiNarrowerBin%d_DiMe", i+1), Form("mMassAcceptance_LimitedMandelstamT_DeltaPhiNarrowerBin%d_DiMe", i+1), 200, 0, 2);
    }
    
    for(int i=0; i<Util::nDeltaPhiRanges; ++i){
      mAcceptanceForDeltaPhiCuts_Vs_t1t2[i] = new TEfficiency("mAcceptanceForDeltaPhiCuts_Vs_t1t2"+TString(Form("_deltaPhiBin_%d", i+1)), "mAcceptanceForDeltaPhiCuts_Vs_t1t2"+TString(Form("_deltaPhiBin_%d", i+1)), 50, 0, 0.5, 50, 0, 0.5);
      mAcceptanceForDeltaPhiCuts_Vs_t1t2_PASSED[i] = new TH2F("mAcceptanceForDeltaPhiCuts_Vs_t1t2_PASSED"+TString(Form("_deltaPhiBin_%d", i+1)), "mAcceptanceForDeltaPhiCuts_Vs_t1t2_PASSED"+TString(Form("_deltaPhiBin_%d", i+1)), 50, 0, 0.5, 50, 0, 0.5);
      mAcceptanceForDeltaPhiCuts_Vs_t1t2_TOTAL[i] = new TH2F("mAcceptanceForDeltaPhiCuts_Vs_t1t2_TOTAL"+TString(Form("_deltaPhiBin_%d", i+1)), "mAcceptanceForDeltaPhiCuts_Vs_t1t2_TOTAL"+TString(Form("_deltaPhiBin_%d", i+1)), 50, 0, 0.5, 50, 0, 0.5);
    }
}


void convertToCrossSection(){
    mhInvMassPidWeighted[Util::OPPO][mParticlesId]->Scale(1./(totalLumi*specialWeightFactor), "width");
    #ifndef ONLY_RHO
    mhPairRapidityPidWeighted[Util::OPPO][mParticlesId]->Scale(1./(totalLumi*specialWeightFactor), "width");
    mhDeltaPhiDegreesPidWeighted[Util::OPPO][mParticlesId]->Scale(1./(totalLumi*specialWeightFactor), "width");
    mhMandelstamTSumPidWeighted[Util::OPPO][mParticlesId]->Scale(1./(totalLumi*specialWeightFactor), "width");
    
    for(int rf=0; rf<Util::nReferenceFrames; ++rf){
        mhCosThetaPidWeighted[Util::OPPO][mParticlesId][rf]->Scale(1./(totalLumi*specialWeightFactor), "width");
        mhPhiDegreesPidWeighted[Util::OPPO][mParticlesId][rf]->Scale(1./(totalLumi*specialWeightFactor), "width");
    }
    
    for(unsigned int r=0; r<Util::nDeltaPhiRanges; ++r){
        mhInvMassPid_DeltaPhiBinsWeighted[Util::OPPO][mParticlesId][r]->Scale(1./(totalLumi*specialWeightFactor), "width");
    }
    
    for(unsigned int r=0; r<Util::nMassRanges; ++r){
        mhMandelstamTSumPid_MassBinsWeighted[Util::OPPO][mParticlesId][r]->Scale(1./(totalLumi*specialWeightFactor), "width");
        mhPairRapidityPid_MassBinsWeighted[Util::OPPO][mParticlesId][r]->Scale(1./(totalLumi*specialWeightFactor), "width");
        mhDeltaPhiDegreesPid_MassBinsWeighted[Util::OPPO][mParticlesId][r]->Scale(1./(totalLumi*specialWeightFactor), "width");
        for(int rf=0; rf<Util::nReferenceFrames; ++rf){
            mhCosThetaPid_MassBinsWeighted[Util::OPPO][mParticlesId][rf][r]->Scale(1./(totalLumi*specialWeightFactor), "width");
            mhPhiDegreesPid_MassBinsWeighted[Util::OPPO][mParticlesId][rf][r]->Scale(1./(totalLumi*specialWeightFactor), "width");
        }
    }
    
    mhInvMassPid_KaonBinsWeighted[Util::OPPO][mParticlesId]->Scale(1./(totalLumi*specialWeightFactor), "width");
    mhInvMassPid_ProtonBinsWeighted[Util::OPPO][mParticlesId]->Scale(1./(totalLumi*specialWeightFactor), "width");
    

    for(unsigned int r=0; r<Util::nDptDeltaPhiRanges; ++r)
        mhInvMass_DeltaPtBins_DeltaPhiBinsWeighted[Util::OPPO][mParticlesId][r]->Scale(1./(totalLumi*specialWeightFactor), "width");

    for(unsigned int r=0; r<Util::nDptDeltaPhiRanges; ++r)
        for(unsigned int s=0; s<Util::nMandelstamTSumRanges; ++s)
            mhInvMass_DeltaPtBins_DeltaPhiBins_MandelstamTSumBinsWeighted[Util::OPPO][mParticlesId][r][s]->Scale(1./(totalLumi*specialWeightFactor), "width");
    
    for(unsigned int r=0; r<Util::nDptDeltaPhiRanges; ++r)
        for(unsigned int s=0; s<Util::nPairRapidityRanges; ++s)
            mhInvMass_DeltaPtBins_DeltaPhiBins_PairRapidityBinsWeighted[Util::OPPO][mParticlesId][r][s]->Scale(1./(totalLumi*specialWeightFactor), "width");

    mhInvMassPidWeighted_Pion_HighPairMass->Scale(1./(totalLumi*specialWeightFactor), "width");
    mhInvMassPidWeighted_Pion_HighPairMass_HighTrackPt->Scale(1./(totalLumi*specialWeightFactor), "width");
    mhInvMassPidWeighted_Pion_HighPairMass_VHighTrackPt->Scale(1./(totalLumi*specialWeightFactor), "width");
    mhInvMassPidWeighted_Pion_HighPairMass_VVHighTrackPt->Scale(1./(totalLumi*specialWeightFactor), "width");
    
    for(unsigned int r=0; r<Util::nDeltaPhiRanges; ++r){
      mhInvMassPidWeighted_Pion_FineBinning_RapidityCut_NarrowerDeltaPhiBins[r]->Scale(1./(totalLumi*specialWeightFactor), "width");
      mhInvMassNoCuts[r]->Scale(1./totalLumi, "width");
    }
    #endif
    
//     TF2* fExponent2D = new TF2( "fExponent2D", "[0] * [1] * exp( -[1] * ( x + y ) )", 0.05, 0.16, 0.05, 0.16);
//     for(int i=0; i<2; ++i){
//       cout << TString(i==0?"deltaPhi < 45*":"deltaPhi > 135*") <<  endl;
//       for(int j=0; j<3; ++j){
//         cout << Form("Mass bin %d", j+1) <<  endl;
//         
//         fExponent2D->SetParameter(0, 1e4);
//         fExponent2D->SetParameter(1, 6);
//         fExponent2D->SetParLimits(1, 1, 40);
//         
//         mht1t2[i][j]->Scale(1., "width");
//         TFitResultPtr res = mht1t2[i][j]->Fit( fExponent2D, "IRSQ", "", 0.05, 0.16 );
//         cout << Form("#chi^{2}/ndf = %.1f/%d", res->Chi2(), res->Ndf()) << endl;
//         cout << Form("B = %.2f #pm %.2f", res->Parameter(1), res->ParError(1) ) << endl;
//       }
//     }
//     
//     
//     
//   TCanvas *ccc = new TCanvas("ccc", "ccc", 800, 600);
//   ccc->Print("Cont.pdf[");
//   for(int i=0; i<2; ++i){
//     TF1 *f1 = new TF1(Form("f1_%d", i), "[0]*pow(0.5 * sqrt( x*x - 4*0.13957018*0.13957018 )/x, fabs([1])) * exp( (-fabs([2]) * 0.5 * sqrt( x*x - 4*0.13957018*0.13957018 ) ))", 0.6, 1.7);
//     f1->SetParameters(1e5, 0.5, 6.0);
//     mhInvMassNoCuts[i]->Fit(f1, "RI");
//     mhInvMassNoCuts[i]->Draw();
//     ccc->Print("Cont.pdf");
//   }
//   ccc->Print("Cont.pdf]");
}


void transformCentralTrksToWave(TLorentzVector &trackPlus, TLorentzVector &trackMinus, TLorentzVector protonA, TLorentzVector protonB, TF2 * const intensityFunc){
  const double cmsEnergySq = (trackPlus+trackMinus+protonA+protonB).Mag2();
  const double p0 = sqrt( cmsEnergySq/4. - mUtil->mass(Util::PROTON)*mUtil->mass(Util::PROTON) );
  TLorentzVector beamProton[Util::nSides];
  for(int side=0; side<Util::nSides; ++side)
    beamProton[side] = TLorentzVector(0, 0, (side==Util::E?-1:1)*p0/*mUtil->p0()*/, sqrt(/*mUtil->p0()*mUtil->p0()*/p0*p0 + mUtil->mass(Util::PROTON)*mUtil->mass(Util::PROTON)));
  TLorentzVector trk[Util::nSigns];
  trk[Util::PLUS] = trackPlus;
  trk[Util::MINUS] = trackMinus;
  TLorentzVector trkPair = trk[Util::PLUS] + trk[Util::MINUS];
  TVector3 trkPairBoost = trkPair.BoostVector();
  TLorentzRotation l2(trkPairBoost);
  TLorentzRotation l;
  TVector3 newX, newY, newZ;
  
  TLorentzVector pomeron[Util::nSides];
  for(int side=0; side<Util::nSides; ++side)
    pomeron[side] = beamProton[side] - (side==Util::E ? protonB : protonA);
  
  newY = pomeron[Util::W].Vect().Cross( /*pomeron[Util::E].Vect()*/trkPairBoost ).Unit();
  pomeron[Util::W].Transform(l2.Inverse());
  pomeron[Util::E].Transform(l2.Inverse());
  newZ = pomeron[Util::W].Vect().Unit();
  newX = newY.Cross( newZ ).Unit();
  
  //----
  
  TRotation r;
  r.SetZAxis(newZ, newX);
  TLorentzRotation l1(r);
  l=l2*l1;
  
  trk[Util::PLUS].Transform(l.Inverse());
  trk[Util::MINUS].Transform(l.Inverse());
  
  //-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
  TRotation r_zeroOrientation;
  r_zeroOrientation.RotateZ( trk[Util::PLUS].Phi() );
  TVector3 rotationAxis = TVector3(0,0,1).Cross( trk[Util::PLUS].Vect() ).Unit();
  double theta = trk[Util::PLUS].Theta();
  r_zeroOrientation.Rotate( theta, rotationAxis );
  trk[Util::PLUS].Transform(r_zeroOrientation.Inverse());
  trk[Util::MINUS].Transform(r_zeroOrientation.Inverse());
  //-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
  
  double randomPhi;
  double randomTheta;
  intensityFunc->GetRandom2(randomPhi, randomTheta);
  
  TRotation r_Wave;
  r_Wave.RotateZ( randomPhi );
  r_Wave.RotateX( randomTheta );
  
  trk[Util::PLUS].Transform(r_Wave.Inverse());
  trk[Util::MINUS].Transform(r_Wave.Inverse());

  // back to LAB frame
  trk[Util::PLUS].Transform(l);
  trk[Util::MINUS].Transform(l);
  
  trackPlus = trk[Util::PLUS];
  trackMinus = trk[Util::MINUS];
}


CosTheta_Phi transformCentralTrks(TLorentzVector trackPlus, TLorentzVector trackMinus, TLorentzVector protonA, TLorentzVector protonB, UInt_t referenceFrame){
    CosTheta_Phi result;
    if( !(referenceFrame<Util::nReferenceFrames) ){
        std::cerr << "ERROR in CosTheta_Phi transformCentralTrks(...) - wrong reference frame ID provided." << std::endl;
        return result;
    }
    
    const double cmsEnergySq = (trackPlus+trackMinus+protonA+protonB).Mag2();
    const double p0 = sqrt( cmsEnergySq/4. - mUtil->mass(Util::PROTON)*mUtil->mass(Util::PROTON) );
    TLorentzVector beamProton[Util::nSides];
    for(int side=0; side<Util::nSides; ++side)
        beamProton[side] = TLorentzVector(0, 0, (side==Util::E?-1:1)*p0/*mUtil->p0()*/, sqrt(/*mUtil->p0()*mUtil->p0()*/p0*p0 + mUtil->mass(Util::PROTON)*mUtil->mass(Util::PROTON)));
    TLorentzVector trk[Util::nSigns];
    trk[Util::PLUS] = trackPlus;
    trk[Util::MINUS] = trackMinus;
    TLorentzVector trkPair = trk[Util::PLUS] + trk[Util::MINUS];
    TVector3 trkPairBoost = trkPair.BoostVector();
    TLorentzRotation l2(trkPairBoost);
    TLorentzRotation l;
    TVector3 newX, newY, newZ;
    
    //--------------------------------------------------
    if( referenceFrame == Util::GOTTFRIED_JACKSON || referenceFrame == Util::HELICITY ){
        
        TLorentzVector pomeron[Util::nSides];
        for(int side=0; side<Util::nSides; ++side)
            pomeron[side] = beamProton[side] - (side==Util::E ? protonB : protonA);
        
        newY = pomeron[Util::W].Vect().Cross( /*pomeron[Util::E].Vect()*/trkPairBoost ).Unit();
        pomeron[Util::W].Transform(l2.Inverse());
        pomeron[Util::E].Transform(l2.Inverse());
        /*
        if( referenceFrame == Util::GOTTFRIED_JACKSON && totalNEvents<10 ){
            TLorentzVector total4Momentum = trkPair + protonA + protonB;
            cout << "------------" << endl;
            cout << "Total 4-momentum in the LAB:" << endl;
            total4Momentum.Print();
            cout << "Pomeron WEST in the CMS of pi+pi-:" << endl;
            pomeron[Util::W].Print();
            cout << "Pomeron EAST in the CMS of pi+pi-:" << endl;
            pomeron[Util::E].Print();
        }
        */
        switch(referenceFrame){
            case Util::GOTTFRIED_JACKSON:{ newZ = pomeron[Util::W].Vect().Unit(); break;}
            case Util::HELICITY:{ newZ = trkPair.Vect().Unit(); break;}
            default:{ break;}
        }
        
        newX = newY.Cross( newZ ).Unit();
    }
    
    else
    
    if( referenceFrame == Util::COLLINS_SOPER ){
        for(int side=0; side<Util::nSides; ++side)
            beamProton[side].Transform(l2.Inverse());
        
        newY = beamProton[Util::W].Vect().Unit().Cross( beamProton[Util::E].Vect().Unit() );
        newZ = (beamProton[Util::W].Vect().Unit() - beamProton[Util::E].Vect().Unit()).Unit();
        newX = newY.Cross( newZ ).Unit();
        
        //     //cross-check
        //     TVector3 newX_2 = (beamProton[Util::W].Vect().Unit() + beamProton[Util::E].Vect().Unit()).Unit();
        //     cout << "-------" << endl;
        //     newX.Print();
        //     newX_2.Print();
    }
    //--------------------------------------------------
    
    TRotation r;
    r.SetZAxis(newZ, newX);
    TLorentzRotation l1(r);
    l=l2*l1;
    
    trk[Util::PLUS].Transform(l.Inverse());
    trk[Util::MINUS].Transform(l.Inverse());
    
    result.mCosTheta = cos( trk[Util::PLUS].Theta() );
    result.mPhi = trk[Util::PLUS].Phi();
    result.mPhiDegrees = result.mPhi*180./Util::PI;
    return result;
}


Double_t intensityFromWaves(const Double_t *x,const  Double_t *par){
  const double theta = x[1];
  const double phi = x[0];
  complex<double> S0m, P0m, D0m, P1m, D1m, P1p, D1p;
  S0m = polar( par[0], 0.0 ) * ROOT::Math::sph_legendre(0,0, theta);
  P0m = polar( par[1], par[2] ) * ROOT::Math::sph_legendre(1,0, theta);
  D0m = polar( par[3], par[4] ) * ROOT::Math::sph_legendre(2,0, theta);
  P1m = polar( par[5], par[6] ) * ROOT::Math::sph_legendre(1,1, theta) * complex<double>(cos(phi), 0.0) * polar( 2.0/sqrt(2.0), 0.);
  D1m = polar( par[7], par[8] ) * ROOT::Math::sph_legendre(2,1, theta) * complex<double>(cos(phi), 0.0) * polar( 2.0/sqrt(2.0), 0.);
  P1p = polar( par[9], 0.0 ) * ROOT::Math::sph_legendre(1,1, theta) * complex<double>(0.0, sin(phi)) * polar( 2.0/sqrt(2.0), 0.);
  D1p = polar( par[10], par[11] ) * ROOT::Math::sph_legendre(2,1, theta) * complex<double>(0.0, sin(phi)) * polar( 2.0/sqrt(2.0), 0.);
  double intensity = norm(S0m + P0m + D0m + P1m + D1m) + norm(P1p + D1p);
  return intensity * sin(theta);
}


void createBinnings(){
  
  //   special binning
    //KK
    double lowMassThreshold = 2*mUtil->mass(Util::KAON);
    double massBinWidth = 5.0 / 100;
    kaonMassBinsVec.push_back( lowMassThreshold );
    int index = 0;
    double massBinBoundary = 0;
    while( massBinBoundary < 2 ){
        ++index;
        massBinBoundary = 1.0 + massBinWidth*index;
        kaonMassBinsVec.push_back( massBinBoundary );
    }
    index = 0;
    massBinWidth = 0.1;
    while( massBinBoundary < 5 ){
        ++index;
        massBinBoundary = 2.0 + massBinWidth*index;
        kaonMassBinsVec.push_back( massBinBoundary );
    }
    
    // ppbar
    lowMassThreshold = 2*mUtil->mass(Util::PROTON);
    protonMassBinsVec.push_back( lowMassThreshold );
    protonMassBinsVec.push_back( 2.0 );
    protonMassBinsVec.push_back( 2.2 );
    protonMassBinsVec.push_back( 2.4 );
    protonMassBinsVec.push_back( 2.6 );
    protonMassBinsVec.push_back( 2.8 );
    protonMassBinsVec.push_back( 3.0 );
    protonMassBinsVec.push_back( 3.2 );
    protonMassBinsVec.push_back( 3.4 );
    protonMassBinsVec.push_back( 3.6 );
    protonMassBinsVec.push_back( 3.8 );
    
    
    // pipi
    
    lowMassThreshold = 2*mUtil->mass(Util::PION);
    pionMassBinsVec.push_back( lowMassThreshold );
    massBinBoundary = 0;
    index = 0;
    massBinWidth = 0.02;
    while( massBinBoundary < 0.7 ){
      ++index;
      massBinBoundary = 0.3 + massBinWidth*index;
      pionMassBinsVec.push_back( massBinBoundary );
    }
    index = 0;
    massBinWidth = 0.03;
    while( massBinBoundary < 1.2999 ){
      ++index;
      massBinBoundary = 0.7 + massBinWidth*index;
      pionMassBinsVec.push_back( massBinBoundary );
    }
    index = 0;
    massBinWidth = 0.05;
    while( massBinBoundary < 2.9999 ){
      ++index;
      massBinBoundary = 1.3 + massBinWidth*index;
      pionMassBinsVec.push_back( massBinBoundary );
    }
    index = 0;
    massBinWidth = 0.10;
    while( massBinBoundary < 5.0 ){
      ++index;
      massBinBoundary = 3.0 + massBinWidth*index;
      pionMassBinsVec.push_back( massBinBoundary );
    }
    
    
    lowMassThreshold = 2*mUtil->mass(Util::PION);
    pionMassBinsFineBinningVec.push_back( lowMassThreshold );
    massBinBoundary = 0;
    index = 0;
    massBinWidth = 0.01;
    while( massBinBoundary < 2.0 ){
      ++index;
      massBinBoundary = 0.29 + massBinWidth*index;
      pionMassBinsFineBinningVec.push_back( massBinBoundary );
    }

    
    
    pionHighMassTailBinsVec.push_back( 2.6 );
    pionHighMassTailBinsVec.push_back( 2.7 );
    pionHighMassTailBinsVec.push_back( 2.8 );
    pionHighMassTailBinsVec.push_back( 2.9 );
    pionHighMassTailBinsVec.push_back( 3.0 );
    pionHighMassTailBinsVec.push_back( 3.1 );
    pionHighMassTailBinsVec.push_back( 3.2 );
    pionHighMassTailBinsVec.push_back( 3.4 );
    pionHighMassTailBinsVec.push_back( 3.6 );
    pionHighMassTailBinsVec.push_back( 4.0 );
    pionHighMassTailBinsVec.push_back( 4.5 );
    pionHighMassTailBinsVec.push_back( 5.0 );

    tSumBinsVec[Util::PION].push_back(0.08);
    tSumBinsVec[Util::PION].push_back(0.12);
    tSumBinsVec[Util::PION].push_back(0.16);
    tSumBinsVec[Util::PION].push_back(0.2);
    tSumBinsVec[Util::PION].push_back(0.24);
    tSumBinsVec[Util::PION].push_back(0.3);
    tSumBinsVec[Util::PION].push_back(0.38);
    
    tSumBinsVec[Util::KAON].push_back(0.08);
    tSumBinsVec[Util::KAON].push_back(0.12);
    tSumBinsVec[Util::KAON].push_back(0.16);
    tSumBinsVec[Util::KAON].push_back(0.2);
    tSumBinsVec[Util::KAON].push_back(0.24);
    tSumBinsVec[Util::KAON].push_back(0.3);
    tSumBinsVec[Util::KAON].push_back(0.38);
    
    tSumBinsVec[Util::PROTON].push_back(0.08);
    tSumBinsVec[Util::PROTON].push_back(0.1);
    tSumBinsVec[Util::PROTON].push_back(0.15);
    tSumBinsVec[Util::PROTON].push_back(0.2);
    tSumBinsVec[Util::PROTON].push_back(0.25);
    tSumBinsVec[Util::PROTON].push_back(0.3);
    tSumBinsVec[Util::PROTON].push_back(0.38);
    
    
    deltaPhiBinsVec[Util::PION].push_back(0);
    deltaPhiBinsVec[Util::PION].push_back(20);
    deltaPhiBinsVec[Util::PION].push_back(40);
    deltaPhiBinsVec[Util::PION].push_back(60);
    deltaPhiBinsVec[Util::PION].push_back(90);
    deltaPhiBinsVec[Util::PION].push_back(120);
    deltaPhiBinsVec[Util::PION].push_back(140);
    deltaPhiBinsVec[Util::PION].push_back(160);
    deltaPhiBinsVec[Util::PION].push_back(180);
    
    deltaPhiBinsVec[Util::KAON].push_back(0);
    deltaPhiBinsVec[Util::KAON].push_back(20);
    deltaPhiBinsVec[Util::KAON].push_back(40);
    deltaPhiBinsVec[Util::KAON].push_back(60);
    deltaPhiBinsVec[Util::KAON].push_back(90);
    deltaPhiBinsVec[Util::KAON].push_back(120);
    deltaPhiBinsVec[Util::KAON].push_back(140);
    deltaPhiBinsVec[Util::KAON].push_back(160);
    deltaPhiBinsVec[Util::KAON].push_back(180);
    
    deltaPhiBinsVec[Util::PROTON].push_back(0);
    deltaPhiBinsVec[Util::PROTON].push_back(20);
    deltaPhiBinsVec[Util::PROTON].push_back(40);
    deltaPhiBinsVec[Util::PROTON].push_back(90);
    deltaPhiBinsVec[Util::PROTON].push_back(140);
    deltaPhiBinsVec[Util::PROTON].push_back(160);
    deltaPhiBinsVec[Util::PROTON].push_back(180);
    
    pairRapidityBinsVec[Util::PROTON].push_back(-0.7);
    pairRapidityBinsVec[Util::PROTON].push_back(-0.6);
    pairRapidityBinsVec[Util::PROTON].push_back(-0.4);
    pairRapidityBinsVec[Util::PROTON].push_back(-0.2);
    pairRapidityBinsVec[Util::PROTON].push_back(0.0);
    pairRapidityBinsVec[Util::PROTON].push_back(0.2);
    pairRapidityBinsVec[Util::PROTON].push_back(0.4);
    pairRapidityBinsVec[Util::PROTON].push_back(0.6);
    pairRapidityBinsVec[Util::PROTON].push_back(0.7);
    
    pairRapidityBinsVec[Util::KAON].push_back(-0.7);
    pairRapidityBinsVec[Util::KAON].push_back(-0.6);
    pairRapidityBinsVec[Util::KAON].push_back(-0.45);
    pairRapidityBinsVec[Util::KAON].push_back(-0.3);
    pairRapidityBinsVec[Util::KAON].push_back(-0.15);
    pairRapidityBinsVec[Util::KAON].push_back(0.0);
    pairRapidityBinsVec[Util::KAON].push_back(0.15);
    pairRapidityBinsVec[Util::KAON].push_back(0.3);
    pairRapidityBinsVec[Util::KAON].push_back(0.45);
    pairRapidityBinsVec[Util::KAON].push_back(0.6);
    pairRapidityBinsVec[Util::KAON].push_back(0.7);
    
    pairRapidityBinsVec[Util::PION].push_back(-0.7);
    pairRapidityBinsVec[Util::PION].push_back(-0.6);
    pairRapidityBinsVec[Util::PION].push_back(-0.5);
    pairRapidityBinsVec[Util::PION].push_back(-0.4);
    pairRapidityBinsVec[Util::PION].push_back(-0.3);
    pairRapidityBinsVec[Util::PION].push_back(-0.2);
    pairRapidityBinsVec[Util::PION].push_back(-0.1);
    pairRapidityBinsVec[Util::PION].push_back(0.0);
    pairRapidityBinsVec[Util::PION].push_back(0.1);
    pairRapidityBinsVec[Util::PION].push_back(0.2);
    pairRapidityBinsVec[Util::PION].push_back(0.3);
    pairRapidityBinsVec[Util::PION].push_back(0.4);
    pairRapidityBinsVec[Util::PION].push_back(0.5);
    pairRapidityBinsVec[Util::PION].push_back(0.6);
    pairRapidityBinsVec[Util::PION].push_back(0.7);
    
    binningT_2D_Vec.push_back(0.05);
    binningT_2D_Vec.push_back(0.06);
    binningT_2D_Vec.push_back(0.07);
    binningT_2D_Vec.push_back(0.08);
    binningT_2D_Vec.push_back(0.10);
    binningT_2D_Vec.push_back(0.13);
    binningT_2D_Vec.push_back(0.16);
  
}

void divideByBinWidth(TH1F* hIn){
  for(int i=0; i<=(hIn->GetNbinsX()+1); ++i){
    hIn->SetBinContent( i, hIn->GetBinContent(i)/hIn->GetBinWidth(i) );
    hIn->SetBinError( i, hIn->GetBinError(i)/hIn->GetBinWidth(i) );
  }
}
