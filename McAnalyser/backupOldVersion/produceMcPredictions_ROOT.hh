#ifndef produceMcPredictions_ROOT_hh
#define produceMcPredictions_ROOT_hh

#include <vector>
#include <TROOT.h>
#include <TFile.h>
#include <TString.h>
#include "/home/rafal/STAR_analysis/CEP-STAR/mainProgramCode/Util.hh"

class TH2;
class TH1F;
class TH2F;
class TGraphAsymmErrors;
class TLine;
class TVector3;
class TF2;
class TLorentzVector;
class TRandom3;
class TEfficiency;

class produceMcPredictions_ROOT{
public:

  produceMcPredictions_ROOT(TString opt="");
  ~produceMcPredictions_ROOT();

  void run();
  
  Util *mUtil;
  
  struct CosTheta_Phi{
    CosTheta_Phi(): mCosTheta(-1000), mPhi(-1000), mPhiDegrees(-1000){}
    Double_t mCosTheta;
    Double_t mPhi;
    Double_t mPhiDegrees;
  };


  double minPt[Util::nDefinedParticles];
  double oneTrackMaxPt[Util::nDefinedParticles];
  const double maxEta = 0.7;
  const double initialMomentum = 100.2;

  int mParticlesId = Util::nDefinedParticles;
  int totalNEvents = 0;
  double totalLumi = 0.0;
  double specialWeightFactor = 0.01;

  TH1F *mhInvMassPidWeighted[Util::nCharges2Trks][Util::nDefinedParticles];
  TH1F *mhPairRapidityPidWeighted[Util::nCharges2Trks][Util::nDefinedParticles];
  TH1F *mhDeltaPhiDegreesPidWeighted[Util::nCharges2Trks][Util::nDefinedParticles];
  TH1F *mhMandelstamTSumPidWeighted[Util::nCharges2Trks][Util::nDefinedParticles];
  TH1F *mhDeltaRapidityPidWeighted[Util::nCharges2Trks][Util::nDefinedParticles];
  TH1F *mhCosThetaPidWeighted[Util::nCharges2Trks][Util::nDefinedParticles][Util::nReferenceFrames];
  TH1F *mhPhiDegreesPidWeighted[Util::nCharges2Trks][Util::nDefinedParticles][Util::nReferenceFrames];

  TH1F *mhInvMassPid_DeltaPhiBinsWeighted[Util::nCharges2Trks][Util::nDefinedParticles][Util::nDeltaPhiRanges];

  TH1F *mhInvMassPidWeighted_Pion_FineBinning_RapidityCut_NarrowerDeltaPhiBins[Util::nDeltaPhiRanges];

  TH1F *mhInvMassPidWeighted_Pion_HighPairMass;
  TH1F *mhInvMassPidWeighted_Pion_HighPairMass_HighTrackPt;
  TH1F *mhInvMassPidWeighted_Pion_HighPairMass_VHighTrackPt;
  TH1F *mhInvMassPidWeighted_Pion_HighPairMass_VVHighTrackPt;

  TH1F *mhPairRapidityPid_MassBinsWeighted[Util::nCharges2Trks][Util::nDefinedParticles][Util::nMassRanges];
  TH1F *mhDeltaPhiDegreesPid_MassBinsWeighted[Util::nCharges2Trks][Util::nDefinedParticles][Util::nMassRanges];
  TH1F *mhMandelstamTSumPid_MassBinsWeighted[Util::nCharges2Trks][Util::nDefinedParticles][Util::nMassRanges];
  TH1F *mhDeltaRapidityPid_MassBinsWeighted[Util::nCharges2Trks][Util::nDefinedParticles][Util::nMassRanges];
  TH1F *mhCosThetaPid_MassBinsWeighted[Util::nCharges2Trks][Util::nDefinedParticles][Util::nReferenceFrames][Util::nMassRanges];
  TH1F *mhPhiDegreesPid_MassBinsWeighted[Util::nCharges2Trks][Util::nDefinedParticles][Util::nReferenceFrames][Util::nMassRanges];

  TH1F *mhInvMassPid_KaonBinsWeighted[Util::nCharges2Trks][Util::nDefinedParticles];
  TH1F *mhInvMassPid_ProtonBinsWeighted[Util::nCharges2Trks][Util::nDefinedParticles];

  TH1F *mhInvMass_DeltaPtBins_DeltaPhiBinsWeighted[Util::nCharges2Trks][Util::nDefinedParticles][Util::nDptDeltaPhiRanges];
  TH1F *mhInvMass_DeltaPtBins_DeltaPhiBins_MandelstamTSumBinsWeighted[Util::nCharges2Trks][Util::nDefinedParticles][Util::nDptDeltaPhiRanges][Util::nMandelstamTSumRanges];
  TH1F *mhInvMass_DeltaPtBins_DeltaPhiBins_PairRapidityBinsWeighted[Util::nCharges2Trks][Util::nDefinedParticles][Util::nDptDeltaPhiRanges][Util::nPairRapidityRanges];

  TH2F *mht1t2[Util::nDeltaPhiRanges][Util::nMassRanges];
  TH1F *mhInvMassNoCuts[Util::nDeltaPhiRanges];

  void divideByBinWidth(TH1F*);
  void createBinnings();
  void createHistograms();
  void convertToCrossSection();
  CosTheta_Phi transformCentralTrks(TLorentzVector trackPlus, TLorentzVector trackMinus, TLorentzVector protonA, TLorentzVector protonB, UInt_t rf);

  //enum MC_TYPES {GENEX_PIPI, GENEX_KK, DIME_PIPI, DIME_KK, PYTHIA_PIPI, PYTHIA_KK, PYTHIA_PPBAR, nMcTypes };


  enum WAVES_ENUM { S0m_MAG, P0m_MAG, P0m_PH, P1m_MAG, P1m_PH, D0m_MAG, D0m_PH, D1m_MAG, D1m_PH,
                    P1p_MAG, D1p_MAG, D1p_PH,      nWaveParameters };
  Double_t intensityFromWaves(const Double_t *x,const  Double_t *par);
  void transformCentralTrksToWave(TLorentzVector &, TLorentzVector &, TLorentzVector, TLorentzVector, TF2* const);

  TEfficiency *mMassAcceptance_LimitedMandelstamT_S0m[Util::nDeltaPhiRanges];
  TEfficiency *mMassAcceptance_LimitedMandelstamT_D0m[Util::nDeltaPhiRanges];
  TEfficiency *mMassAcceptance_LimitedMandelstamT_DiMe[Util::nDeltaPhiRanges];

  TEfficiency *mAcceptanceForDeltaPhiCuts_Vs_t1t2[Util::nDeltaPhiRanges];
  TH2F *mAcceptanceForDeltaPhiCuts_Vs_t1t2_PASSED[Util::nDeltaPhiRanges];
  TH2F *mAcceptanceForDeltaPhiCuts_Vs_t1t2_TOTAL[Util::nDeltaPhiRanges];

  //------------------------------------------------------------------------

  vector<double> pionMassBinsVec;
  vector<double> kaonMassBinsVec;
  vector<double> protonMassBinsVec;
  vector<double> pionMassBinsFineBinningVec;
  vector<double> pionHighMassTailBinsVec;  
  std::vector<double> tSumBinsVec[Util::nDefinedParticles];
  std::vector<double> deltaPhiBinsVec[Util::nDefinedParticles];
  std::vector<double> pairRapidityBinsVec[Util::nDefinedParticles];
  vector<double> binningT_2D_Vec;

   
  ClassDef(produceMcPredictions_ROOT,0);
};

#endif
