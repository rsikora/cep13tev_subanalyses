#define Util_cxx

#include <limits>
#include <algorithm>
#include "Util.hh"
#include "TString.h"
#include "TF1.h"
#include "TVectorD.h"
#include "TMatrixD.h"
#include "TSpline.h"
#include "TRandom3.h"
#include "TFitResult.h"

Util* Util::mInst = nullptr;
const Double_t Util::PI = 3.14159265359;
const Double_t Util::kEpsilon = 1e-7;


Util::Util(TString codePath, Bool_t isEmb): mSpeedOfLight(29.9792), mBeamMomentum(100.14), mTpcBFieldStrength(0.498318), mIsEmbeddedMC(isEmb){

  mSideName = new TString[nSides];
  mSideName[E] = TString("East");
  mSideName[W] = TString("West");
  
  mCoordinateName = new TString[nCoordinates];
  mCoordinateName[X] = TString("x");
  mCoordinateName[Y] = TString("y");
  
  mTriggerName = new TString[nTriggers];
  mTriggerName[SD] = TString("SD");
  mTriggerName[CPT2] = TString("CPT2");
  mTriggerName[SDT] = TString("SDT");
  mTriggerName[RPZMU] = TString("RPZMU");
  mTriggerName[RP2MU] = TString("RP2MU");
  mTriggerName[ET] = TString("ET");
  mTriggerName[CP] = TString("CP");
  mTriggerName[CPT] = TString("CPT");
  mTriggerName[RP2E] = TString("RP2E");
  mTriggerName[Zerobias] = TString("Zerobias");
  mTriggerName[CPX] = TString("CPX");
  mTriggerName[SDZ] = TString("SDZ");
  mTriggerName[CPEI] = TString("CPEI");
  mTriggerName[ZE] = TString("ZE");
  
  mTriggerBitLabel = new TString[nTriggerBits];
  mTriggerBitLabel[BIT_ET] = TString("ET");
  mTriggerBitLabel[BIT_IT] = TString("IT");
  mTriggerBitLabel[BIT_EOR] = TString("EOR");
  mTriggerBitLabel[BIT_WOR] = TString("WOR");
  mTriggerBitLabel[BIT_EU] = TString("EU");
  mTriggerBitLabel[BIT_ED] = TString("ED");
  mTriggerBitLabel[BIT_WU] = TString("WU");
  mTriggerBitLabel[BIT_WD] = TString("WD");
  
  mArmName = new TString[nArms];
  mArmName[EU_WD] = TString("EU-WD");
  mArmName[ED_WU] = TString("ED-WU");
  
  mBranchName = new TString[nBranches];
  mBranchName[EU] = TString("EU");
  mBranchName[ED] = TString("ED");
  mBranchName[WU] = TString("WU");
  mBranchName[WD] = TString("WD");
  
  mBranchesConfigurationName = new TString[nBranchesConfigurations];
  mBranchesConfigurationName[CONF_EU_WU] = TString("EU-WU");
  mBranchesConfigurationName[CONF_ED_WD] = TString("ED-WD");
  mBranchesConfigurationName[CONF_EU_WD] = TString("EU-WD");
  mBranchesConfigurationName[CONF_ED_WU] = TString("ED-WU");
    
  mRpName = new TString[nRomanPots];
  mRpName[E1U] = TString("E1U");
  mRpName[E1D] = TString("E1D");
  mRpName[E2U] = TString("E2U");
  mRpName[E2D] = TString("E2D");
  mRpName[W1U] = TString("W1U");
  mRpName[W1D] = TString("W1D");
  mRpName[W2U] = TString("W2U");
  mRpName[W2D] = TString("W2D");
  
  mPlaneName = new TString[nPlanes];
  mPlaneName[A] = TString("A");
  mPlaneName[B] = TString("B");
  mPlaneName[C] = TString("C");
  mPlaneName[D] = TString("D");
  
  mStationName = new TString[nStations];
  mStationName[E1] = TString("E1");
  mStationName[E2] = TString("E2");
  mStationName[W1] = TString("W1");
  mStationName[W2] = TString("W2");
  
  mProtonsConfiguration = new TString[nProtonsConfigurations];
  mProtonsConfiguration[ALL] = TString("");
  mProtonsConfiguration[ELA] = TString("ET");
  mProtonsConfiguration[INE] = TString("IT");
  
  mParticleName = new TString[nDefinedParticles];
//   mParticleName[ELECTRON] = TString("electron");
//   mParticleName[MUON] = TString("muon");
  mParticleName[PION] = TString("pion");
  mParticleName[KAON] = TString("kaon");
  mParticleName[PROTON] = TString("proton");
    
  mParticleNameExtended = new TString[nDefinedParticlesExtended];
  mParticleNameExtended[PIOn] = mParticleName[PION];
  mParticleNameExtended[KAOn] = mParticleName[KAON];
  mParticleNameExtended[PROTOn] = mParticleName[PROTON];
  mParticleNameExtended[ELECTROn] = TString("electron");
  mParticleNameExtended[DEUTEROn] = TString("deuteron");
  
  mTpcTrackTypeName = new TString[nTpcTrkTypes];
  mTpcTrackTypeName[GLO] = TString("global");
  mTpcTrackTypeName[PRI] = TString("primary");
  mTpcTrackTypeName[TOF] = TString("TofMatched");
  mTpcTrackTypeName[QUA] = TString("goodQuality");

  mBunchCrossingTypeName = new TString[nBnchXngsTypes];
  mBunchCrossingTypeName[CB] = TString("collidingBunches");
  mBunchCrossingTypeName[AG] = TString("abortGaps");
  
  mChargeSum2TrksName = new TString[nCharges2Trks];
  mChargeSum2TrksName[OPPO] = TString("oppositeSign");
  mChargeSum2TrksName[SAME] = TString("sameSign");
  
  mChargeSum4TrksName = new TString[nCharges4Trks];
  mChargeSum4TrksName[QSUM_ZERO] = TString("QSum0");
  mChargeSum4TrksName[QSUM_NON0] = TString("QSumNon0");

  mSignName = new TString[nSigns];
  mSignName[PLUS] = TString("Plus");
  mSignName[MINUS] = TString("Minus");
  
  mRpTrackCombinationName = new TString[nRpTrackTypeCombinations];
  mRpTrackCombinationName[LL] = TString("LL");
  mRpTrackCombinationName[LG] = TString("LG");
  mRpTrackCombinationName[GG] = TString("GG");
  
  mEfficiencyName = new TString[nEffCorrections];
  mEfficiencyName[RPACC] = TString("RpAcc");
  mEfficiencyName[TPCRECOEFF] = TString("TpcRecoEff");
  mEfficiencyName[TOFMATCHEFF] = TString("TofMatchEff");
  
  mCutName = new TString[nAnalysisCuts];
  mCutName[TRIG] = TString("Triggers");
  mCutName[TWORPTRKS] = TString("2 RP trks");
  mCutName[ONETOFVX] = TString("1 TOF vrtx");
  mCutName[ZVERTEX] = TString("|z_{vx}| < 100 cm");
  mCutName[TWOTOFTRKS] = TString("2 TOF-mtchd trks");
  mCutName[TWOQUATRKS] = TString("TPC trks quality");
  mCutName[OPPOSITE] = TString("Opposite-sign");
  mCutName[TPCRPVX_MATCHED] = TString("TPC-RP vrtx match");
  mCutName[BBCCLEAN] = TString("BBC-L clean");
  mCutName[TOFHITSRECO] = TString("2 N_{TOF}^{clusters}");
  mCutName[EXCLUSIVE] = TString("Exclusive (p_{T}^{miss} cut)");
  mCutName[PIPI] = TString("#pi^{+}#pi^{-}");
  mCutName[KK] = TString("K^{+}K^{-}");
  mCutName[PPBAR] = TString("p#bar{p}");

  mCutShortName = new TString[nAnalysisCuts];
  mCutShortName[TRIG] = TString("TRIG");
  mCutShortName[TWORPTRKS] = TString("TWORPTRKS");
  mCutShortName[ONETOFVX] = TString("ONETOFVX");
  mCutShortName[ZVERTEX] = TString("ZVERTEX");
  mCutShortName[TWOTOFTRKS] = TString("TWOTOFTRKS");
  mCutShortName[TWOQUATRKS] = TString("TWOQUATRKS");
  mCutShortName[OPPOSITE] = TString("OPPOSITE");
  mCutShortName[TPCRPVX_MATCHED] = TString("TPCRPVX_MATCHED");
  mCutShortName[BBCCLEAN] = TString("BBCCLEAN");
  mCutShortName[TOFHITSRECO] = TString("TOFHITSRECO");
  mCutShortName[EXCLUSIVE] = TString("EXCLUSIVE");
  mCutShortName[PIPI] = TString("PIPI");
  mCutShortName[KK] = TString("KK");
  mCutShortName[PPBAR] = TString("PPBAR");
  
  mReferenceFrameName = new TString[nReferenceFrames];
  mReferenceFrameName[GOTTFRIED_JACKSON] = TString("Gottfried-Jackson");
  mReferenceFrameName[COLLINS_SOPER] = TString("Collins-Soper");
  mReferenceFrameName[HELICITY] = TString("Helicity");
  
  mReferenceFrameShortName = new TString[nReferenceFrames];
  mReferenceFrameShortName[GOTTFRIED_JACKSON] = TString("GJ");
  mReferenceFrameShortName[COLLINS_SOPER] = TString("CS");
  mReferenceFrameShortName[HELICITY] = TString("H");
  
  mTrueLevelCentralStateTopologyName = new TString[nCentralStateTopologies];
  mTrueLevelCentralStateTopologyName[kNoChargedOnlyNeutrals] = TString("NoChargedOnlyNeutrals");
  mTrueLevelCentralStateTopologyName[k1ChargedPairNoNeutrals] = TString("1ChargedPairNoNeutrals");
  mTrueLevelCentralStateTopologyName[k1ChargedPairAndNeutrals] = TString("1ChargedPairAndNeutrals");
  mTrueLevelCentralStateTopologyName[k2ChargedPairsNoNeutrals] = TString("2ChargedPairsNoNeutrals");
  mTrueLevelCentralStateTopologyName[k2ChargedPairsAndNeutrals] = TString("2ChargedPairsAndNeutrals");
  mTrueLevelCentralStateTopologyName[k3OrMoreChargedPairsNoNeutrals] = TString("3OrMoreChargedPairsNoNeutrals");
  mTrueLevelCentralStateTopologyName[k3OrMoreChargedPairsAndNeutrals] = TString("3OrMoreChargedPairsAndNeutrals");
  mTrueLevelCentralStateTopologyName[kUnqualified] = TString("Unqualified");
  
  mMassRangeLimits[MASS_1][MIN] = 0.0;
  mMassRangeLimits[MASS_1][MAX] = 1.0;
  mMassRangeLimits[MASS_2][MIN] = 1.0;
  mMassRangeLimits[MASS_2][MAX] = 1.5;
  mMassRangeLimits[MASS_3][MIN] = 1.5;
  mMassRangeLimits[MASS_3][MAX] = 9e9;
  
//   mDeltaPhiLimits[DELTAPHI_1][MIN] = 0.0;
//   mDeltaPhiLimits[DELTAPHI_1][MAX] = 60.0;
//   mDeltaPhiLimits[DELTAPHI_2][MIN] = 60.0;
//   mDeltaPhiLimits[DELTAPHI_2][MAX] = 120.0;
//   mDeltaPhiLimits[DELTAPHI_3][MIN] = 120.0;
//   mDeltaPhiLimits[DELTAPHI_3][MAX] = 180.0;

  mDeltaPhiLimits[DELTAPHI_1][MIN] = 0.0;
  mDeltaPhiLimits[DELTAPHI_1][MAX] = 90.0;
  mDeltaPhiLimits[DELTAPHI_2][MIN] = 90.0;
  mDeltaPhiLimits[DELTAPHI_2][MAX] = 180.0;
  
  mPairPtRangeLimits[PAIRPT_1][MIN] = 0.0;
  mPairPtRangeLimits[PAIRPT_1][MAX] = 0.25;
  mPairPtRangeLimits[PAIRPT_2][MIN] = 0.25;
  mPairPtRangeLimits[PAIRPT_2][MAX] = 0.45;
  mPairPtRangeLimits[PAIRPT_3][MIN] = 0.45;
  mPairPtRangeLimits[PAIRPT_3][MAX] = 9e99;
  
  mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_1][MIN] = 0.0;
  mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_1][MAX] = 0.08;
  mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_2][MIN] = 0.08;
  mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_2][MAX] =  9e99;
 
//   mParticleMass[ELECTRON] = 0.000511;
//   mParticleMass[MUON] = 0.10565837;
  mParticleMass[PION] = 0.13957018;
  mParticleMass[KAON] = 0.493667;
  mParticleMass[PROTON] = 0.93827208;
  
  mParticleMassExtended[PIOn] = 0.13957018;
  mParticleMassExtended[KAOn] = 0.493667;
  mParticleMassExtended[PROTOn] = 0.93827208;
  mParticleMassExtended[ELECTROn] = 0.000511;
  mParticleMassExtended[DEUTEROn] = 1.875612;
  
  mBranchPerRp[E1U] = EU;
  mBranchPerRp[E2U] = EU;
  mBranchPerRp[E1D] = ED;
  mBranchPerRp[E2D] = ED;
  mBranchPerRp[W1U] = WU;
  mBranchPerRp[W2U] = WU;
  mBranchPerRp[W1D] = WD;
  mBranchPerRp[W2D] = WD;
  
  mOppositeBranch[EU] = WD;
  mOppositeBranch[ED] = WU;
  mOppositeBranch[WU] = ED;
  mOppositeBranch[WD] = EU;
  
  mSidePerRp[E1U] = E;
  mSidePerRp[E2U] = E;
  mSidePerRp[E1D] = E;
  mSidePerRp[E2D] = E;
  mSidePerRp[W1U] = W;
  mSidePerRp[W2U] = W;
  mSidePerRp[W1D] = W;
  mSidePerRp[W2D] = W;
  
  mStationOrderPerRp[E1U] = RP1;
  mStationOrderPerRp[E2U] = RP2;
  mStationOrderPerRp[E1D] = RP1;
  mStationOrderPerRp[E2D] = RP2;
  mStationOrderPerRp[W1U] = RP1;
  mStationOrderPerRp[W2U] = RP2;
  mStationOrderPerRp[W1D] = RP1;
  mStationOrderPerRp[W2D] = RP2;
  
  mRpPerBranchStationOrder[EU][RP1] = E1U;
  mRpPerBranchStationOrder[EU][RP2] = E2U;
  mRpPerBranchStationOrder[ED][RP1] = E1D;
  mRpPerBranchStationOrder[ED][RP2] = E2D;
  mRpPerBranchStationOrder[WU][RP1] = W1U;
  mRpPerBranchStationOrder[WU][RP2] = W2U;
  mRpPerBranchStationOrder[WD][RP1] = W1D;
  mRpPerBranchStationOrder[WD][RP2] = W2D;
  
  // based on http://www.star.bnl.gov/public/comp/simu/gstar/Manual/particle_id.html
  for(int i=0; i<mMaxStarGeantIdRecognized; ++i)
    mParticlePerStarGeantId[i] = Util::nDefinedParticlesExtended;
  mParticlePerStarGeantId[7] = Util::PIOn;
  mParticlePerStarGeantId[8] = Util::PIOn;
  mParticlePerStarGeantId[10] = Util::KAOn;
  mParticlePerStarGeantId[11] = Util::KAOn;
  mParticlePerStarGeantId[13] = Util::PROTOn;
  mParticlePerStarGeantId[14] = Util::PROTOn;
  mParticlePerStarGeantId[1] = Util::ELECTROn;
  mParticlePerStarGeantId[2] = Util::ELECTROn;
  mParticlePerStarGeantId[44] = Util::DEUTEROn;
  
  mParticleNamePerStarGeantId = new TString[mNGeantIds];
  mParticleNamePerStarGeantId[0] = TString("Undefined");
  mParticleNamePerStarGeantId[1] = TString("GAMMA");
  mParticleNamePerStarGeantId[2] = TString("POSITRON");
  mParticleNamePerStarGeantId[3] = TString("ELECTRON");
  mParticleNamePerStarGeantId[4] = TString("NEUTRINO");
  mParticleNamePerStarGeantId[5] = TString("MUON+");
  mParticleNamePerStarGeantId[6] = TString("MUON-");
  mParticleNamePerStarGeantId[7] = TString("PION0");
  mParticleNamePerStarGeantId[8] = TString("PION+");
  mParticleNamePerStarGeantId[9] = TString("PION-");
  mParticleNamePerStarGeantId[10] = TString("KAON0 LONG");
  mParticleNamePerStarGeantId[11] = TString("KAON+");
  mParticleNamePerStarGeantId[12] = TString("KAON-");
  mParticleNamePerStarGeantId[13] = TString("NEUTRON");
  mParticleNamePerStarGeantId[14] = TString("PROTON");
  mParticleNamePerStarGeantId[15] = TString("ANTIPROTON");
  mParticleNamePerStarGeantId[16] = TString("KAON0 SHORT");
  mParticleNamePerStarGeantId[17] = TString("ETA");
  mParticleNamePerStarGeantId[18] = TString("LAMBDA");
  mParticleNamePerStarGeantId[19] = TString("SIGMA+");
  mParticleNamePerStarGeantId[20] = TString("SIGMA0");
  mParticleNamePerStarGeantId[21] = TString("SIGMA-");
  mParticleNamePerStarGeantId[22] = TString("XI0");
  mParticleNamePerStarGeantId[23] = TString("XI-");
  mParticleNamePerStarGeantId[24] = TString("OMEGA-");
  mParticleNamePerStarGeantId[25] = TString("ANTINEUTRON");
  mParticleNamePerStarGeantId[26] = TString("ANTILAMBDA");
  mParticleNamePerStarGeantId[27] = TString("ANTISIGMA-");
  mParticleNamePerStarGeantId[28] = TString("ANTISIGMA0");
  mParticleNamePerStarGeantId[29] = TString("ANTISIGMA+");
  mParticleNamePerStarGeantId[30] = TString("ANTIXI0");
  mParticleNamePerStarGeantId[31] = TString("ANTIXI+");
  mParticleNamePerStarGeantId[32] = TString("ANTIOMEGA+");
  mParticleNamePerStarGeantId[45] = TString("DEUTERON");
  mParticleNamePerStarGeantId[46] = TString("TRITON");
  mParticleNamePerStarGeantId[47] = TString("ALPHA");
  mParticleNamePerStarGeantId[48] = TString("GEANTINO");
  mParticleNamePerStarGeantId[49] = TString("HE3");
  mParticleNamePerStarGeantId[50] = TString("Cerenkov");

  
  mGenerator = new TRandom3(1); // if seed set to 0, then randomization will be different each time program is run
  
  
  
}

Util::~Util(){
  if(mSideName) delete [] mSideName;
  if(mTriggerName) delete [] mTriggerName;
  if(mTriggerBitLabel) delete [] mTriggerBitLabel;
  if(mArmName) delete [] mArmName;
  if(mBranchName) delete [] mBranchName;
  if(mBranchesConfigurationName) delete [] mBranchesConfigurationName;
  if(mRpName) delete [] mRpName;
  if(mPlaneName) delete [] mPlaneName;
  if(mStationName) delete [] mStationName;
  if(mProtonsConfiguration) delete [] mProtonsConfiguration;
  if(mParticleName) delete [] mParticleName;
  if(mParticleNameExtended) delete [] mParticleNameExtended;
  if(mTpcTrackTypeName) delete [] mTpcTrackTypeName;
  if(mBunchCrossingTypeName) delete [] mBunchCrossingTypeName;
  if(mChargeSum2TrksName) delete [] mChargeSum2TrksName;
  if(mChargeSum4TrksName) delete [] mChargeSum4TrksName;
  if(mSignName) delete [] mSignName;
  if(mRpTrackCombinationName) delete [] mRpTrackCombinationName;
  if(mEfficiencyName) delete [] mEfficiencyName;
  if(mCutName) delete [] mCutName;
  if(mCutShortName) delete [] mCutShortName;
  if(mReferenceFrameName) delete [] mReferenceFrameName;
  if(mReferenceFrameShortName) delete [] mReferenceFrameShortName;
  if(mParticleNamePerStarGeantId) delete [] mParticleNamePerStarGeantId;
  if(mTrueLevelCentralStateTopologyName) delete [] mTrueLevelCentralStateTopologyName;
}

Util* Util::instance(TString codePath, Bool_t isEmb){
  if(!mInst) mInst = new Util(codePath, isEmb);
  return mInst;
}

Util::MASS_RANGE Util::massRange(Double_t mass) const{
  for(int i=0; i<nMassRanges; ++i)
    if(mass>=mMassRangeLimits[i][MIN] && mass<mMassRangeLimits[i][MAX])
      return static_cast<MASS_RANGE>(i);
  std::cerr << "ERROR in Util::massRange(Double_t): argument not within expected limits!" << std::endl;
  return nMassRanges;
}

Util::DELTAPHI_RANGE Util::deltaPhiRange(Double_t deltaPhi) const{
  for(int i=0; i<nDeltaPhiRanges; ++i)
    if(deltaPhi>=mDeltaPhiLimits[i][MIN] && deltaPhi<mDeltaPhiLimits[i][MAX])
      return static_cast<DELTAPHI_RANGE>(i);
  std::cerr << "ERROR in Util::deltaPhiRange(Double_t): argument not within expected limits!" << std::endl;
  return nDeltaPhiRanges;
}
Util::PAIRPT_RANGE Util::pairPtRange(Double_t pairPt) const{
  for(int i=0; i<nPairPtRanges; ++i)
    if(pairPt>=mPairPtRangeLimits[i][MIN] && pairPt<mPairPtRangeLimits[i][MAX])
      return static_cast<PAIRPT_RANGE>(i);
  std::cerr << "ERROR in Util::pairPtRange(Double_t): argument not within expected limits!" << std::endl;
  return nPairPtRanges;
}

Util::MANDELSTAMTSINGLESIDE_RANGE Util::mandelstamTSingleSideRange(Double_t t) const{
  for(int i=0; i<nMandelstamTSingleSideRanges; ++i)
    if(t>=mMandelstamTRangeLimits[i][MIN] && t<mMandelstamTRangeLimits[i][MAX])
      return static_cast<MANDELSTAMTSINGLESIDE_RANGE>(i);
  std::cerr << "ERROR in Util::mandelstamTSingleSideRange(Double_t): argument not within expected limits!" << std::endl;
  return nMandelstamTSingleSideRanges;
}

Util::MANDELSTAMTBOTHSIDES_RANGE Util::mandelstamTBothSidesRange(Double_t t1, Double_t t2) const{
  if(t1>=mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_1][MIN] && t1<mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_1][MAX] && t2>=mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_1][MIN] && t2<mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_1][MAX])
    return MANDELSTAMTBOTHSIDES_1; else
  if((t1<t2?t1:t2)>=mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_1][MIN] && (t1<t2?t1:t2)<mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_1][MAX] && (t1<t2?t2:t1)>=mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_2][MIN] && (t1<t2?t2:t1)<mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_2][MAX])
    return MANDELSTAMTBOTHSIDES_2; else
  if(t1>=mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_2][MIN] && t1<mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_2][MAX] && t2>=mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_2][MIN] && t2<mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_2][MAX])
    return MANDELSTAMTBOTHSIDES_3; else{
      std::cerr << "ERROR in Util::mandelstamTBothSidesRange(Double_t, Double_t): at least one argument not within expected limits!" << std::endl;
      return nMandelstamTBothSidesRanges;
  }
}

Double_t Util::bkgdFraction(const TH1* hPtMiss, Double_t ptMissCut, Double_t fitLimitMin, Double_t fitLimitMax, TF1 *funcReturn) const{
  static int funcId; ++funcId;
  TString idStr; idStr.Form("func%d", funcId);
  TF1* func = new TF1(idStr, "[0]*x+[1]*x*x", 0, fitLimitMax);
  func->SetNpx(1e3);
  func->SetParameter(0, 1);
  func->SetParameter(1, -1);
  double bkgdFrac = 1e9;
  if(hPtMiss){
    const_cast<TH1*>(hPtMiss)->Fit( func, "MQNI", "", fitLimitMin, fitLimitMax);
    if( fabs(func->GetParameter(0)-1)<0.001 && fabs(func->GetParameter(1)+1)<0.001 ){
      bkgdFrac = 0;
    } else{
      double nBkgdEvents = func->Integral(0, ptMissCut) / hPtMiss->GetBinWidth(1);
      double nAllEvents = integratePtMiss(hPtMiss, ptMissCut);
      bkgdFrac = nBkgdEvents/nAllEvents;
    }
  } else std::cerr << "ERROR in getBkgdFraction()" << std::endl;
  
  if(funcReturn) *funcReturn = *func;
  return bkgdFrac;
}


Double_t Util::integratePtMiss(const TH1* hPtMiss, Double_t ptMissCut) const{
  int binNum = 1; 
  for(int i=1; i<=hPtMiss->GetNbinsX(); ++i) if(hPtMiss->GetXaxis()->GetBinUpEdge(i) >= ptMissCut ){ binNum = i; break; }
  double integral = hPtMiss->Integral(1, binNum-1);
  integral += hPtMiss->GetBinContent(binNum) * ( ptMissCut - hPtMiss->GetXaxis()->GetBinLowEdge(binNum) ) / hPtMiss->GetXaxis()->GetBinWidth(binNum);
  return integral;
}


std::vector<float> Util::getBinsVectorF(const TAxis *axis) const{
  std::vector<float> binsVector;
  for( int i=1; i<=axis->GetNbins(); ++i)
    binsVector.push_back( axis->GetBinLowEdge( i ) );
  binsVector.push_back( axis->GetBinUpEdge( axis->GetNbins() ) );
  return binsVector;
}


std::vector<double> Util::getBinsVectorD(const TAxis *axis) const{
  std::vector<double> binsVector;
  for( int i=1; i<=axis->GetNbins(); ++i)
    binsVector.push_back( axis->GetBinLowEdge( i ) );
  binsVector.push_back( axis->GetBinUpEdge( axis->GetNbins() ) );
  return binsVector;
}

TH1F* Util::bkgdHistogram(const TH2* hMissPtVsX, Double_t ptMissCut, Double_t fitLimitMin, Double_t fitLimitMax, Int_t mode, vector<TF1*> * funcVec) const{
  std::vector<float> binsVector = getBinsVectorF( hMissPtVsX->GetXaxis() );
  TH1F *hBkgd = new TH1F( TString("Background_")+hMissPtVsX->GetName(), TString("Background_")+hMissPtVsX->GetTitle(), binsVector.size()-1, &(binsVector[0]) );
  TH1D *missingPtHist = hMissPtVsX->ProjectionY("tmpNameProjectionY");
  const double integralLimit_MIN = missingPtHist->GetXaxis()->GetBinLowEdge( missingPtHist->GetXaxis()->FindBin( fitLimitMin ) );
  const double integralLimit_MAX = missingPtHist->GetXaxis()->GetBinUpEdge( missingPtHist->GetXaxis()->FindBin( fitLimitMax ) );
  if( mode==0 ){ // default mode
    TF1 *pTMissExtrapolationFunc = new TF1();
    double bkgdFrac = bkgdFraction( missingPtHist, ptMissCut, fitLimitMin, fitLimitMax, pTMissExtrapolationFunc);
    const double integralRatio_signalRegion_to_bkgdFreeRegion = pTMissExtrapolationFunc->Integral(0, ptMissCut) / pTMissExtrapolationFunc->Integral(integralLimit_MIN, integralLimit_MAX);
    double nBkgdEvents = bkgdFrac*integratePtMiss( missingPtHist, ptMissCut );
    for(int i=0; i<=(hBkgd->GetNbinsX()+1); ++i){
      TH1D* hMissPtProj = hMissPtVsX->ProjectionY("tmpName", i, i);
      double nBkgdInPtMissBin = integralRatio_signalRegion_to_bkgdFreeRegion * hMissPtProj->Integral( hMissPtProj->FindBin( fitLimitMin ), hMissPtProj->FindBin( fitLimitMax ) );
      hBkgd->SetBinContent(i, nBkgdInPtMissBin);
      hBkgd->SetBinError(i, 0);
    }
    if(funcVec) funcVec->push_back( pTMissExtrapolationFunc );
  } else{
    for(int i=0; i<=(hBkgd->GetNbinsX()+1); ++i){
      TF1 *pTMissExtrapolationFunc = new TF1();
      TH1D* hMissPtProj = hMissPtVsX->ProjectionY("tmpName", i, i);
      double bkgdFrac = bkgdFraction( hMissPtProj, ptMissCut, fitLimitMin, fitLimitMax, pTMissExtrapolationFunc);
      double nBkgdEvents = bkgdFrac*integratePtMiss( hMissPtProj, ptMissCut );
      hBkgd->SetBinContent(i, nBkgdEvents);
      hBkgd->SetBinError(i, 0);
      if(funcVec) funcVec->push_back( pTMissExtrapolationFunc );
    }
  }
  return hBkgd;
}


Double_t Util::binomialCoeff(UInt_t n, UInt_t k) const{
  if(k>n){ cerr << "ERROR in Util::binomialCoeff(UInt_t n, UInt_t k):  k>n !!!!" << endl; return 1; }
  double result = 1.0;
  for(unsigned int i=1; i<=k; ++i) result *= static_cast<double>(n-i+1)/static_cast<double>(i);
  return result;
}

// TH1F** Util::pidLeakFreeHistogram(TH3F* const * hQuantityVsPPlusVsPMinus, UInt_t pid, TH2F* const * hPidTransferFunctionFromXToY) const{
//   TH1F **hOut = new TH1F*[Util::nDefinedParticles];
//   for(int i=0; i<Util::nDefinedParticles; ++i)
//     hOut[i] = new TH1F( TString("pidLeakFreeHistogram_")+hQuantityVsPPlusVsPMinus[i]->GetName(), TString("pidLeakFreeHistogram_")+hQuantityVsPPlusVsPMinus[i]->GetTitle(), hQuantityVsPPlusVsPMinus[i]->GetNbinsZ(), hQuantityVsPPlusVsPMinus[i]->GetZaxis()->GetXmin(), hQuantityVsPPlusVsPMinus[i]->GetZaxis()->GetXmax() );
//   for(int bin=0; bin<=(hQuantityVsPPlusVsPMinus[pid]->GetNbinsZ()+1); ++bin){
//     TH2D* hPPlusVsPMinus[Util::nDefinedParticles];
//     for(int i=0; i<Util::nDefinedParticles; ++i){
//       hQuantityVsPPlusVsPMinus[i]->GetZaxis()->SetRange( bin, bin );
//       hPPlusVsPMinus[i] = dynamic_cast<TH2D*>( hQuantityVsPPlusVsPMinus[i]->Project3D("yx") );
//     }
//     
//     TVectorD Nevts_total(/*Util::nDefinedParticles*/2);
//     double NleakedToPions = 0;
//     
//     if( hPPlusVsPMinus[pid]->GetEntries() > 0 ){
//     
//       for(int i=0; i<=(hPPlusVsPMinus[pid]->GetNbinsX()+1); ++i)
// 	for(int j=0; j<=(hPPlusVsPMinus[pid]->GetNbinsY()+1); ++j){
// 	  
// 	  TVectorD Nevts(/*Util::nDefinedParticles*/2);
// 	  for(int p=0; p</*Util::nDefinedParticles*/2; ++p)
// 	    Nevts[p] = hPPlusVsPMinus[p]->GetBinContent(i,j);
// 	    
// 	  if( Nevts[Util::PION] > 0 && Nevts[Util::KAON] > 0 ){
// 	    TMatrixD transferMatrix(2/*Util::nDefinedParticles*/, 2/*Util::nDefinedParticles*/);
// 	    bool isNonZeroRow[Util::nDefinedParticles] = {false, false, false};
// 	    bool isNonZeroColumn[Util::nDefinedParticles] = {false, false, false};
// 	    for(int pidX=0; pidX<2/*Util::nDefinedParticles*/; ++pidX)
// 	      for(int pidY=0; pidY<2/*Util::nDefinedParticles*/; ++pidY){
// 		transferMatrix[pidY][pidX] = hPidTransferFunctionFromXToY[Util::nDefinedParticles * pidX + pidY]->GetBinContent(i, j);
// 		if(transferMatrix[pidY][pidX]>0){
// 		  isNonZeroRow[pidY] = true;
// 		  isNonZeroColumn[pidX] = true;
// 		}
// 	      }
// 	    
// 	    bool hasZeroRow = false;
// 	    bool hasZeroColumn = false;
// 	    for(int p=0; p<2/*Util::nDefinedParticles*/; ++p){
// 	      if(!isNonZeroRow[p]) hasZeroRow = true;
// 	      if(!isNonZeroColumn[p]) hasZeroColumn = true;
// 	    }
// 	    
// 	    
// 	    if(!hasZeroRow && !hasZeroColumn){
// 	      transferMatrix.Print();
// 	      transferMatrix.Invert();
// 	      
// 	      Nevts = transferMatrix * Nevts;
// 	      for(int p=0; p<Util::nDefinedParticles; ++p)
// 		if(Nevts[p]<0) Nevts[p] = 0;
// // 	      cout << Nevts[Util::KAON] << "  " << transferMatrix[Util::KAON][Util::PION] << endl;
// 	      if(Nevts[Util::KAON]>0)
// 		NleakedToPions += Nevts[Util::KAON] * /*transferMatrix[Util::KAON][Util::PION]*/hPidTransferFunctionFromXToY[Util::nDefinedParticles * 1 + 0]->GetBinContent(i, j);
// 	    }/*else{
// 	      
// 	    }*/
// 	  }
// 	  
// 	  Nevts_total += Nevts;
// 	  
// 	}
//     }
//     
//     for(int p=0; p<2/*Util::nDefinedParticles*/; ++p){
//       hOut[p]->SetBinContent(bin, /*Nevts_total[p]*/NleakedToPions );
// //       if(hOut[p]->GetXaxis()->GetBinCenter(bin)  < 1.7)
// //       cout << "M = " << hOut[p]->GetXaxis()->GetBinCenter(bin) <<  "    Nevts = " <<  NleakedToPions << endl;
//     }
//   }
//   return hOut;
// }





// TH1F** Util::pidLeakFreeHistogram(TH1F* const * hQuantity, UInt_t pid, TH1F* const * hPidTransferFunctionFromXToY) const{
//   TH1F **hOut = new TH1F*[Util::nDefinedParticles];
//   TSpline3 splineTransferFunctionFromXToY[Util::nDefinedParticles * Util::nDefinedParticles];
//   for(int pidX=0; pidX< Util::nDefinedParticles; ++pidX)
//     for(int pidY=0; pidY<Util::nDefinedParticles; ++pidY){
//       splineTransferFunctionFromXToY[Util::nDefinedParticles * pidX + pidY] = TSpline3(hPidTransferFunctionFromXToY[Util::nDefinedParticles * pidX + pidY]);
//     }
//   
//   double offset = 0/*0.3*/; // in units of hQuantity
//   int nBinsMassOffset = offset / hQuantity[pid]->GetBinWidth(1); //ALERT (assuming equal binning)
//   
//   for(int i=0; i<Util::nDefinedParticles; ++i)
//     hOut[i] = new TH1F( TString("pidLeakFreeHistogram_")+hQuantity[i]->GetName(), TString("pidLeakFreeHistogram_")+hQuantity[i]->GetTitle(), hQuantity[i]->GetNbinsX(), hQuantity[i]->GetXaxis()->GetXmin(), hQuantity[i]->GetXaxis()->GetXmax() );
//   for(int bin=0; bin<=(hQuantity[pid]->GetNbinsX()+1-nBinsMassOffset); ++bin){
// 
//     TVectorD Nevts_total(/*Util::nDefinedParticles*/2);
//     double NleakedToPions = 0;
//     double NleakedToPionsErr = 0;
//     
//     if( hQuantity[pid]->GetBinContent(bin) > 0 ){
// 
//       TVectorD Nevts(/*Util::nDefinedParticles*/2);
//       for(int p=0; p</*Util::nDefinedParticles*/2; ++p)
// 	Nevts[p] = hQuantity[p]->GetBinContent(bin);
//       
//       if( Nevts[Util::PION] > 0 && Nevts[Util::KAON] > 0 ){
// 	TMatrixD transferMatrix(2/*Util::nDefinedParticles*/, 2/*Util::nDefinedParticles*/);
// 	bool isNonZeroRow[Util::nDefinedParticles] = {false, false, false};
// 	bool isNonZeroColumn[Util::nDefinedParticles] = {false, false, false};
// 	bool isLeakageEqual1 = false;
// 	bool isEfficiencyEqual0 = false;
// 	for(int pidX=0; pidX<2/*Util::nDefinedParticles*/; ++pidX)
// 	  for(int pidY=0; pidY<2/*Util::nDefinedParticles*/; ++pidY){
// // 	    int binNumInTransferFunction = hPidTransferFunctionFromXToY[Util::nDefinedParticles * pidX + pidY]->GetXaxis()->FindBin( hQuantity[pid]->GetXaxis()->GetBinCenter(bin) );
// // 	    transferMatrix[pidY][pidX] = hPidTransferFunctionFromXToY[Util::nDefinedParticles * pidX + pidY]->GetBinContent( binNumInTransferFunction );
// 	    transferMatrix[pidY][pidX] = splineTransferFunctionFromXToY[Util::nDefinedParticles * pidX + pidY].Eval( hQuantity[pid]->GetXaxis()->GetBinCenter(bin+nBinsMassOffset) );
// 	    if(transferMatrix[pidY][pidX]>0){
// 	      isNonZeroRow[pidY] = true;
// 	      isNonZeroColumn[pidX] = true;
// 	    }
// 	    if(pidY!=pidX && hPidTransferFunctionFromXToY[Util::nDefinedParticles * pidX + pidY]->GetBinContent( hPidTransferFunctionFromXToY[Util::nDefinedParticles * pidX + pidY]->GetXaxis()->FindBin( hQuantity[pid]->GetXaxis()->GetBinCenter(bin+nBinsMassOffset) ) )>0.99)
// 	      isLeakageEqual1 = true;
// 	    if(pidY==pidX && hPidTransferFunctionFromXToY[Util::nDefinedParticles * pidX + pidY]->GetBinContent( hPidTransferFunctionFromXToY[Util::nDefinedParticles * pidX + pidY]->GetXaxis()->FindBin( hQuantity[pid]->GetXaxis()->GetBinCenter(bin+nBinsMassOffset) ) )<0.01)
// 	      isEfficiencyEqual0 = true;
// 	  }
// 	  
// 	bool hasZeroRow = false;
// 	bool hasZeroColumn = false;
// 	for(int p=0; p<2/*Util::nDefinedParticles*/; ++p){
// 	  if(!isNonZeroRow[p]) hasZeroRow = true;
// 	  if(!isNonZeroColumn[p]) hasZeroColumn = true;
// 	}
// 	
// 	
// 	if(!hasZeroRow && !hasZeroColumn && !isLeakageEqual1 && !isEfficiencyEqual0){
// // 	  if(hQuantity[pid]->GetBinCenter(bin) >1.3 && hQuantity[pid]->GetBinCenter(bin) < 1.7){
// // 	    cout << "M = " << hOut[pid]->GetXaxis()->GetBinCenter(bin) << endl;
// // 	    transferMatrix.Print();
// // 	  }
// 	  transferMatrix.Invert();
// 	  
// 	  Nevts = transferMatrix * Nevts;
// 	  for(int p=0; p<2/*Util::nDefinedParticles*/; ++p)
// // 	    if(Nevts[p]<0) Nevts[p] = 0;
// // 	    	      cout << Nevts[Util::KAON] /*<< "  " << transferMatrix[Util::KAON][Util::PION] */<< endl;
// 	    if(Nevts[Util::KAON]>0){
// 	      NleakedToPions = Nevts[Util::KAON] * splineTransferFunctionFromXToY[Util::nDefinedParticles * 1 + 0].Eval( hQuantity[pid]->GetXaxis()->GetBinCenter(bin+nBinsMassOffset) );
// 	      NleakedToPionsErr = sqrt(Nevts[Util::KAON]) * splineTransferFunctionFromXToY[Util::nDefinedParticles * 1 + 0].Eval( hQuantity[pid]->GetXaxis()->GetBinCenter(bin+nBinsMassOffset) );
// 	    }
// 	}/*else{
// 	
//       }*/
//       }
//       
//       Nevts_total += Nevts;
//       
//     }
//     
//     for(int p=0; p<2/*Util::nDefinedParticles*/; ++p){
//       if(NleakedToPions>0){
// 	hOut[p]->SetBinContent(bin, /*Nevts_total[p]*/NleakedToPions );
// 	hOut[p]->SetBinError(bin, /*Nevts_total[p]*/NleakedToPionsErr );
// // 	if( p==Util::KAON && hOut[p]->GetXaxis()->GetBinCenter(bin)  < 1.7)
// //             cout << "M = " << hOut[p]->GetXaxis()->GetBinCenter(bin) <<  "    Nevts = " <<  Nevts_total[Util::KAON] << endl;
//       }
// 
//     }
//   }
//   return hOut;

// }




TH1F** Util::pidLeakFreeHistogram(TH1F* const * hQuantity, UInt_t pid, TH1F* const * hPidTransferFunctionFromXToY) const{
  TH1F **hOut = new TH1F*[Util::nDefinedParticles];
  TSpline3 splineTransferFunctionFromXToY[Util::nDefinedParticles * Util::nDefinedParticles];
  for(int pidX=0; pidX< Util::nDefinedParticles; ++pidX)
    for(int pidY=0; pidY<Util::nDefinedParticles; ++pidY)
      splineTransferFunctionFromXToY[Util::nDefinedParticles * pidX + pidY] = TSpline3(hPidTransferFunctionFromXToY[Util::nDefinedParticles * pidX + pidY]);
  
  double offset = 0/*0.3*/; // in units of hQuantity
  int nBinsMassOffset = offset / hQuantity[pid]->GetBinWidth(1); //ALERT (assuming equal binning)
  
  for(int i=0; i<Util::nDefinedParticles; ++i)
    hOut[i] = new TH1F( TString("pidLeakFreeHistogram_")+hQuantity[i]->GetName(), TString("pidLeakFreeHistogram_")+hQuantity[i]->GetTitle(), hQuantity[i]->GetNbinsX(), hQuantity[i]->GetXaxis()->GetXmin(), hQuantity[i]->GetXaxis()->GetXmax() );
  for(int bin=0; bin<=(hQuantity[pid]->GetNbinsX()+1-nBinsMassOffset); ++bin){

    double nEventsTotal[Util::nDefinedParticles] = {0, 0, 0};
    
    if( hQuantity[pid]->GetBinContent(bin) > 0 ){
      
      TMatrixD transferMatrix_initial(Util::nDefinedParticles, Util::nDefinedParticles);
      bool isNonZeroRow[Util::nDefinedParticles] = {false, false, false};
      bool isNonZeroColumn[Util::nDefinedParticles] = {false, false, false};
      bool isLeakageEqual1 = false;
      bool isEfficiencyEqual0 = false;
      for(int pidX=0; pidX<Util::nDefinedParticles; ++pidX)
	for(int pidY=0; pidY<Util::nDefinedParticles; ++pidY){
	  transferMatrix_initial[pidY][pidX] = splineTransferFunctionFromXToY[Util::nDefinedParticles * pidX + pidY].Eval( hQuantity[pid]->GetXaxis()->GetBinCenter(bin+nBinsMassOffset) );
	  if( transferMatrix_initial[pidY][pidX] > 0){
	    isNonZeroRow[pidY] = true;
	    isNonZeroColumn[pidX] = true;
	  }
	  if( pidY!=pidX && transferMatrix_initial[pidY][pidX] > 0.99 )
	    isLeakageEqual1 = true;
	  if( pidY==pidX && transferMatrix_initial[pidY][pidX] < 0.01 )
	    isEfficiencyEqual0 = true;
	}

      unsigned int nParticleTypesAccounted = Util::nDefinedParticles;
      bool removeParticleType[Util::nDefinedParticles] = {false, false, false};
      for(int p=0; p<Util::nDefinedParticles; ++p)
	if(!isNonZeroColumn[p] || !isNonZeroRow[p]){
	  --nParticleTypesAccounted;
	  removeParticleType[p] = true;
	}

      TMatrixD transferMatrix(nParticleTypesAccounted, nParticleTypesAccounted);
      if(nParticleTypesAccounted == Util::nDefinedParticles)
	transferMatrix = transferMatrix_initial;
      else{
	transferMatrix = TMatrixD(nParticleTypesAccounted, nParticleTypesAccounted);
	for(int pidX=0, nSkipsX=0; pidX<Util::nDefinedParticles; ++pidX){
	  if( removeParticleType[pidX] ){
	    ++nSkipsX;
	    continue;
	  }
	  for(int pidY=0, nSkipsY=0; pidY<Util::nDefinedParticles; ++pidY){
	    if( removeParticleType[pidY] ){
	      ++nSkipsY;
	      continue;
	    }
	    transferMatrix[pidY-nSkipsY][pidX-nSkipsX] = transferMatrix_initial[pidY][pidX];
	  }
	}
      }

      int nSuccessfullLoops = 0;
      const int nLoops = 1e3;
      
      for(int it=0; it<nLoops; ++it){

	TVectorD Nevts(nParticleTypesAccounted);
	for(int p=0, nSkips=0; p<Util::nDefinedParticles; ++p){
	  if( !removeParticleType[p] )
	    Nevts[p-nSkips] = mGenerator->PoissonD( hQuantity[p]->GetBinContent(bin) );
	  else ++nSkips;
	}
	
// 	if( Nevts[Util::PION] > 0 && Nevts[Util::KAON] > 0 ){
	  if(!isLeakageEqual1 && !isEfficiencyEqual0){
  // 	  if(hQuantity[pid]->GetBinCenter(bin) >1.3 && hQuantity[pid]->GetBinCenter(bin) < 1.7){
  // 	    cout << "M = " << hOut[pid]->GetXaxis()->GetBinCenter(bin) << endl;
  // 	    transferMatrix.Print();
  // 	  }
	    transferMatrix.Invert();
	    Nevts = transferMatrix * Nevts;
	    
	    bool isLoopSuccessfull = true;
	    for(unsigned int p=0; p<nParticleTypesAccounted; ++p)
	      if( Nevts[p]<0 ) isLoopSuccessfull = false;
	    
	    if( isLoopSuccessfull ){
	      for(int p=0, nSkips=0; p<Util::nDefinedParticles; ++p){
		if( !removeParticleType[p] )
		  nEventsTotal[p] += Nevts[p-nSkips];
		else ++nSkips;
	      }
	      
	      ++nSuccessfullLoops;
	    }

	  }/*else{
	  
	}*/
// 	}
      }
      
      for(int p=0; p<Util::nDefinedParticles; ++p)
	nEventsTotal[p] /= nSuccessfullLoops;
      
      // finally store calculated Nevts in desired histograms
      for(unsigned int p=0; p<Util::nDefinedParticles; ++p){
	if( nEventsTotal[p] > 0 ){
	  hOut[p]->SetBinContent(bin, p==pid ? nEventsTotal[p] : (nEventsTotal[p] * transferMatrix_initial[pid][p]));
	  hOut[p]->SetBinError(bin, p==pid ? sqrt(nEventsTotal[p]) : sqrt(nEventsTotal[p] * transferMatrix_initial[pid][p]) );
	}
      }
      
    }
    
  }
  
  return hOut;
}






void Util::subtractBackground(TH1* hSignal, const TH1* hBkgd) const{ //ALERT!! has to look closer at the under/overflow bins which are so far not accounted for!
  if( hSignal->GetNbinsX() != hBkgd->GetNbinsX() ){
    std::cerr << "ERROR in subtractBackground(): number of bins of signal and background histogram is different. Return" << std::endl;
    return;
  }
  for(int i=0; i<=(hSignal->GetNbinsX()+1); ++i){
    double signalContent = hSignal->GetBinContent(i);
    if( signalContent<1e-9 ) continue;
    double signalError = hSignal->GetBinError(i);
    double backgroundContent = hBkgd->GetBinContent(i);
    double newSignalContent = signalContent - backgroundContent;
    if( newSignalContent<0 ) std::cerr << "WARNING in subtractBackground(): Background larger than signal" << std::endl;
    hSignal->SetBinContent(i, newSignalContent>0 ? newSignalContent : 0.001 ); //ALERT
    hSignal->SetBinError(i, newSignalContent>0 ? (signalError*newSignalContent/signalContent) : signalError ); // ALERT
  }
}


