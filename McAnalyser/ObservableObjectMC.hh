#ifndef ObservableObjectMC_hh
#define ObservableObjectMC_hh

#include <TROOT.h>
#include <TFile.h>
#include <TString.h>
#include "Util.hh"

class TH1F;

class ObservableObjectMC{
    
public: 
    ObservableObjectMC(void *, TString, std::vector<double> &);
    ~ObservableObjectMC();
    
    inline int nBins() const{ return static_cast<int>(binsVec.size())-1; };

    TH1F *hQ_Weighted;
    
    std::vector<double> binsVec;
    TString name;
  
  ClassDef(ObservableObjectMC,0);
};

#endif
