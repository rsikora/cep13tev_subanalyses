#!/bin/bash

#DIR=/home/rafal/ATLAS_analysis/MonteCarlo/DiMe_ALFA_Fiducial
# DIR=/home/rafal/ATLAS_analysis/MonteCarlo/DiMe_ALFA_Fiducial_PtCut_65MeV
DIR=/home/rafal/ATLAS_analysis/MonteCarlo/DiMe_ALFA_fullPhi
NDIRS=`ls $DIR | wc -l`

for i in `seq $NDIRS`:
do
    if [ -f "$DIR/$i/exrec.dat" ] ; then
        if [ -f "$DIR/$i/log.txt" ] ; then
            echo "Running conversion for file #$i"
            ./read_hep_ALFA.x pipi $DIR/$i $i
        fi
    fi    
done
    
# N_threads=7
# N_passes=21
# 
# for i in `seq $N_threads`;
#   do
#     for j in `seq $N_passes`;
#       do
# 	./read_hep.x pipi Generator_PiPi/largeGeneration/$i/ $i\_$j exrec_$j.dat log_$j.txt
#       done
#   done
