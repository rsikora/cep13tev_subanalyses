#define CepAnalysis_cxx

#include "CepAnalysis.hh"
#include <TH2.h> 
#include <TF1.h> 
#include <TF2.h> 
#include <THStack.h> 
#include <TStyle.h> 
#include <TMultiGraph.h> 
#include <TGraph.h> 
#include <TGraph2D.h> 
#include <TGraphAsymmErrors.h> 
#include <TEfficiency.h> 
#include <TFractionFitter.h>
#include <TObjArray.h>
#include <iostream>
#include <utility>
#include <sstream> 
#include <algorithm> 
#include <stdio.h> 
#include <stdlib.h> 
#include <TCanvas.h> 
#include <TNamed.h>
#include <TLegend.h> 
#include <TBox.h>
#include <TGaxis.h> 
#include <vector> 
#include <fstream> 
#include <TString.h> 
#include <TColor.h> 
#include <TLine.h> 
#include <cmath> 
#include <TExec.h>
#include <TEllipse.h>
#include <TFitResultPtr.h> 
#include <TFitResult.h> 
#include <TLatex.h> 
#include <TExec.h>
#include <TMath.h>
#include <TArrow.h>
#include <TF12.h>
#include <TPaletteAxis.h>
#include <TRandom3.h>
#include <TKey.h>
#include <sys/stat.h>
#include "code/CepHistograms_ALFA.h"
#include "code/CepHistograms_ID.h"
#include "code/CepHistograms_MBTS.h"
#include "code/CepHistograms_ClosureTests.h"
#include "code/CepHistograms_EventFlow.h"
#include "code/CepHistograms_MomentumBalance.h"
#include "code/CepPhysicsContainer.h"
#include "code/CepObservableObject.h"
#include "code/DataVsMC.hh"
#include "code/CrossSectionAnalysis.hh"
#include "code/produceMcPredictions_ROOT.hh"

ClassImp(CepAnalysis)


CepAnalysis::CepAnalysis(TString option){
    mCepUtil = CepUtil::instance(/*option*/);
    mParams = CepParams::instance(/*option*/);
    //   mEff = Efficiencies::instance(mParams, option);
    
//     mSpecialLabelRatio = false;
//     mSpecialLabelOffsetFactor = 1.0;
//     mEnableLegend = false;
//     mSpecialLegendMarginFactor = 1.0;
//     
}

CepAnalysis::~CepAnalysis(){}






void CepAnalysis::analyze(){
    
    for(int i=0; i<=CepUtil::/*PIPI*/FOURPI; ++i){
        DataVsMC object_DataVsMC( static_cast<CepUtil::CEP_CHANNELS>(i)/*CepUtil::PPBAR*//*KK*//*PIPI*//*FOURPI*/ );
//         object_DataVsMC.analyze(); //ALERT
    }
    
    CrossSectionAnalysis object_CrossSectionAnalysis;
//     object_CrossSectionAnalysis.analyze(); //ALERT
    
//     return;
    
    produceMcPredictions_ROOT object_produceMcPredictions;
//     object_produceMcPredictions.run();
    
    setupDrawingStyle();

    
    TFile *fData = TFile::Open( "ROOT_files/master/data.root" );
    TFile *file_cd = TFile::Open( "ROOT_files/master/pythia_cd.root" );
    TFile *file_signal_pipi = TFile::Open( "ROOT_files/master/dime_pipi.root" );
    TFile *file_signal_kk = TFile::Open( "ROOT_files/master/dime_kk.root" );
    TFile *file_signal_ppbar = TFile::Open( "ROOT_files/master/graniitti_ppbar.root" );
    
    
    
    getListOfAnalysedRuns(fData);
    readLumiFiles();
    getTotalIntegratedLuminosity();
    cout << "Lumi = " << totalIntegratedLuminosity << " mub^-1" <<  endl;

    
    drawInDetPlots(fData);
    
    pullAnalysis(fData);
    
    drawDEdx(fData, file_cd, file_signal_pipi, file_signal_kk, file_signal_ppbar);
    
    drawMomentumBalancePxPy(fData, file_signal_pipi);
    
    drawNSigmaMissingPt(fData);
    
    drawRecoMethodsComparison(fData, file_signal_pipi, file_cd);
    
    drawMbtsEff(file_signal_pipi, file_cd);
    
    closureTests(file_signal_pipi, file_signal_kk, file_signal_ppbar, file_cd);
    
//     drawPtCorrections(file_signal_pipi);

//     drawInDetEff();
    
}








Double_t gammaBetaNotLog(Double_t *x, Double_t *par){
  double arg = fabs(x[0]/par[5]);
  return par[0]/TMath::Power(arg*arg/(1+arg*arg), par[2])*TMath::Log(1+TMath::Power((par[1]*arg),par[4]))-par[3];
}




int fileId = -1;
std::vector<TH1F*> vecTH1[2];
int histId = 0;

void readdir(TDirectory *dir) {
   TDirectory *dirsav = gDirectory;
   TIter next(dir->GetListOfKeys());
   TKey *key;
   while ((key = (TKey*)next())) {
       
      if (key->IsFolder()
          && TString(key->GetName()) != TString("EventLoop_OutputStream_myTree")
          && TString(key->GetName()) != TString("EventLoop_FileExecuted") ) {
         dir->cd(key->GetName());
         TDirectory *subdir = gDirectory;
         readdir(subdir);
         dirsav->cd();
         continue;
      }
      TH1 *h = dynamic_cast<TH1*>( key->ReadObj() );
      TH2 *h2 = dynamic_cast<TH2*>( key->ReadObj() );
      if(h && !h2){
        ++histId;
        TH1F *hh = (TH1F*)h->Clone();
        hh->SetDirectory(0);
        hh->SetName(Form("%s%d", h->GetName(), histId));
        hh->SetTitle(h->GetName());
        vecTH1[fileId].push_back(hh);
      }
//       TMacro *macro = (TMacro*)key->ReadObj();
//       nfiles++;
//       nlines += macro->GetListOfLines()->GetEntries();
//       if (strstr(key->GetName(),".h"))   nh++;
//       if (strstr(key->GetName(),".c"))   nc++;
//       if (strstr(key->GetName(),".C"))   nC++;
//       if (strstr(key->GetName(),".py"))  npy++;
//       delete macro;
   }
}



void CepAnalysis::drawInDetPlots(TFile * file){
    
    const TString dirName("PDF_InDetPlots/");
    mkdir(dirName.Data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    
    CepHistograms_ID *hID = new CepHistograms_ID("ID", false, file);
    
    {
              
        TLatex lSTAR; lSTAR.SetNDC(); lSTAR.SetTextFont(72);  lSTAR.SetTextSize(.1);
      
      gStyle->SetErrorX(0.5);
      
      TCanvas *c = new TCanvas("c", "c", 800, 800);
      c->SetFrameFillStyle(0);
      TGaxis::SetMaxDigits(5);
      c->SetFillColor(-1);
      
      const double labelTextSize = 0.045;
      const double axisTitleTextSize = 0.045;
      const double leftMargin = 0.12;
      const double rightMargin = 0.01;
      const double topMargin = 0.05;
      const double bottomMargin = 0.11;
      
      gPad->SetLeftMargin(leftMargin);
      gPad->SetRightMargin(rightMargin);
      gPad->SetTopMargin(topMargin);
      gPad->SetBottomMargin(bottomMargin);
      
      TH1F * hNPrimVertexMult = hID->hNPrimVertexMult;
      
      hNPrimVertexMult->GetYaxis()->SetTitleOffset(1.4);
      hNPrimVertexMult->GetXaxis()->SetRangeUser(-0.5, 5.5);
      hNPrimVertexMult->GetXaxis()->SetTitle("Number of primary vertices");
      hNPrimVertexMult->GetXaxis()->SetTitleSize(axisTitleTextSize);
      hNPrimVertexMult->GetYaxis()->SetTitle("Number of events");
      hNPrimVertexMult->GetYaxis()->SetTitleSize(axisTitleTextSize);
      hNPrimVertexMult->GetXaxis()->SetTitleOffset(1.15);
      hNPrimVertexMult->GetXaxis()->SetNdivisions(010);
      hNPrimVertexMult->GetXaxis()->SetTickLength(0.015);
      hNPrimVertexMult->GetXaxis()->SetLabelSize(labelTextSize);
      hNPrimVertexMult->GetYaxis()->SetLabelSize(labelTextSize);
      hNPrimVertexMult->GetXaxis()->SetLabelOffset(0.01);
      hNPrimVertexMult->SetLineWidth(4);
      hNPrimVertexMult->SetMarkerColor( kBlack );
      hNPrimVertexMult->SetMarkerStyle( 20 );
      hNPrimVertexMult->SetMarkerSize( 1.8 );
      hNPrimVertexMult->SetLineColor( kBlack );
      hNPrimVertexMult->SetMaximum(1.2*hNPrimVertexMult->GetMaximum());
      hNPrimVertexMult->Draw("PE");
      
      
      
      /*TFile *file2 = new TFile( "../Backgrounds/ROOT_files/analysisOutput.pipiEmbedding_tightFiltering_PileUp_ClosureTest_July2019.CPT2.root", "READ" );
      TH1F *genexVtxMult = dynamic_cast<TH1F*>( file2->Get("TpcTrackSelector/NumberOfVertices_TofMatched") );
      genexVtxMult->SetDirectory(0);
      
      
      genexVtxMult->SetLineColor(kBlack);
      genexVtxMult->SetFillColor(kYellow-7);
      genexVtxMult->SetLineWidth(2);
      genexVtxMult->Scale( hNPrimVertexMult->GetBinContent(hNPrimVertexMult->FindBin(1)) / genexVtxMult->GetBinContent(genexVtxMult->FindBin(1)) );
      genexVtxMult->Draw("HIST SAME");
      
      hNPrimVertexMult->Draw("PE SAME");*/
      
      gPad->RedrawAxis();
      
//       TArrow* cutArrow = new TArrow( hNPrimVertexMult->GetBinCenter(2), 1.03*hNPrimVertexMult->GetBinContent(2), hNPrimVertexMult->GetBinCenter(2), 1.17*hNPrimVertexMult->GetBinContent(2), 0.05, "<|");
//       cutArrow->SetAngle(30);
//       cutArrow->SetLineWidth(6);
//       cutArrow->SetLineColor( kRed );
//       cutArrow->SetFillColor( kRed );
//       cutArrow->Draw();
      
          TLine *cutLine = new TLine( 1.5, 0, 1.5, 0.95*hNPrimVertexMult->GetMaximum() );
        cutLine->SetLineWidth(6);
        cutLine->SetLineColor(kRed);
        cutLine->SetLineStyle(7);
        cutLine->Draw();
        
        TArrow *cutArrow = new TArrow( 1.5, 0.95*hNPrimVertexMult->GetMaximum(), 1.5-0.5, 0.95*hNPrimVertexMult->GetMaximum(), 0.035, "|>");
        cutArrow->SetAngle(30);
        cutArrow->SetLineWidth(6);
        cutArrow->SetLineColor( kRed );
        cutArrow->SetFillColor( kRed );
        cutArrow->Draw();
        
//       lSTAR.DrawLatex(0.68, 0.85, "STAR");
      lSTAR.DrawLatex(0.603, 0.81-0.3, "#scale[0.6]{#font[42]{#sqrt{s} = 13 TeV}}");
      lSTAR.DrawLatex(0.613, 0.73-0.3, "#scale[0.6]{#font[42]{#beta* = 90 m}}");
      
      TLatex l; l.SetNDC();
        l.SetTextColor(kBlack);
        l.SetTextFont(72);
        l.SetTextSize(.06);
        l.DrawLatex(.53,.81,Form("ATLAS #font[42]{Internal}"));
      
//       legend = new TLegend(0.55, 0.5, 0.81, 0.64);
//       legend->SetTextSize(0.04);
//       legend->SetFillColor(-1);
//       legend->SetBorderSize(0);
//       legend->AddEntry(hNPrimVertexMult, "Data", "pel");
//       legend->AddEntry(genexVtxMult, "Exclusive #pi^{+}#pi^{-}", "f");
//       legend->Draw();
      
      c->Print(dirName+"NumberOfPrimaryVertices.pdf");
      
      
      

      
      c = new TCanvas("c", "c", 800, 800);
      gPad->SetLeftMargin(0.13);
      gPad->SetRightMargin(0.05);
      gPad->SetTopMargin(0.05);
      gPad->SetBottomMargin(0.13);
      TGaxis::SetMaxDigits(3);
      
      TH1F * hZVertex = hID->hZVertex;
      
      hZVertex->Rebin(10);
      TString binWidthZ; binWidthZ.Form("%d", static_cast<int>(hZVertex->GetXaxis()->GetBinWidth(1)));
      hZVertex->GetYaxis()->SetTitle("Number of events / "+binWidthZ+" mm");
      //   hZVertex->GetYaxis()->SetTickLength(-0.012);
      hZVertex->GetYaxis()->SetTitleOffset(1.24);
      hZVertex->GetYaxis()->SetRangeUser(0, 1.05*hZVertex->GetMaximum());
      hZVertex->GetXaxis()->SetRangeUser(-200, 200);
      hZVertex->GetXaxis()->SetTitle("z_{vtx} [mm]");
      hZVertex->GetXaxis()->SetTitleOffset(1.2);
      
      hZVertex->GetXaxis()->SetTitleSize(0.05);
      hZVertex->GetYaxis()->SetTitleSize(0.05);
      hZVertex->GetXaxis()->SetLabelSize(0.05);
      hZVertex->GetYaxis()->SetLabelSize(0.05);
      
      hZVertex->SetLineWidth(1);
      hZVertex->SetMarkerColor( kBlack );
      hZVertex->SetMarkerStyle( 20 );
      hZVertex->SetMarkerSize(1.8);
      hZVertex->SetLineColor( kBlack );
      hZVertex->Draw("PE");
      
      lSTAR.DrawLatex(0.15, 0.8, "#scale[0.6]{#font[42]{#sqrt{s} = 13 TeV}}");
      lSTAR.DrawLatex(0.16, 0.72, "#scale[0.6]{#font[42]{#beta* = 90 m}}");
      l.DrawLatex(.7,.8,Form("ATLAS"));
      l.DrawLatex(.7,.72,Form("#font[42]{Internal}"));

      c->Print(dirName+"ZVertex.pdf");
      
      
      
      gStyle->SetPalette(69);
      TColor::InvertPalette();
      
      TH2F * hXYVertex = hID->hXYVertex;
      
//       hXYVertex->Rebin(10);
      TString binWidthX; binWidthX.Form("%d", static_cast<int>(hXYVertex->GetXaxis()->GetBinWidth(1)));
      TString binWidthY; binWidthY.Form("%d", static_cast<int>(hXYVertex->GetYaxis()->GetBinWidth(1)));
      
      hXYVertex->GetZaxis()->SetTitle("Number of events / ("+binWidthX+" mm #times "+binWidthY+" mm)");
      //   hXYVertex->GetYaxis()->SetTickLength(-0.012);
      
      hXYVertex->GetYaxis()->SetTitle("y_{vtx} [mm]");
      hXYVertex->GetYaxis()->SetTitleOffset(1.24);

      
//       hXYVertex->GetXaxis()->SetRangeUser(-200, 200);
      hXYVertex->GetXaxis()->SetTitle("x_{vtx} [mm]");
      hXYVertex->GetXaxis()->SetTitleOffset(1.2);
      
      hXYVertex->GetXaxis()->SetTitleSize(0.05);
      hXYVertex->GetYaxis()->SetTitleSize(0.05);
      hXYVertex->GetXaxis()->SetLabelSize(0.05);
      hXYVertex->GetYaxis()->SetLabelSize(0.05);
      hXYVertex->GetZaxis()->SetLabelSize(0.05);
      hXYVertex->GetZaxis()->SetLabelSize(0.05);
      
      hXYVertex->Draw("col");
      
      lSTAR.DrawLatex(0.2, 0.8, "#scale[0.6]{#font[42]{#sqrt{s} = 13 TeV}}");
      lSTAR.DrawLatex(0.21, 0.72, "#scale[0.6]{#font[42]{#beta* = 90 m}}");
      l.DrawLatex(.7,.8,Form("ATLAS"));
      l.DrawLatex(.7,.72,Form("#font[42]{Internal}"));

      c->Print(dirName+"XYVertex.pdf");
      
      gStyle->SetPalette(55);
    }
    
}



void CepAnalysis::drawDEdx(TFile * file, TFile * file_CD, TFile * file_PIPI, TFile * file_KK, TFile * file_PPBAR ){
        
    const TString dirName("PDF_dEdx/");
    mkdir(dirName.Data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    
    
    CepHistograms_ID *hID = new CepHistograms_ID("ID", false, file);
    CepHistograms_ID *hID_CD = new CepHistograms_ID("ID", false, file_CD);
    CepHistograms_ID *hID_perPID[CepUtil::nDefinedParticles];
    hID_perPID[CepUtil::PION] = new CepHistograms_ID("ID", false, file_PIPI);
    hID_perPID[CepUtil::KAON] = new CepHistograms_ID("ID", false, file_KK);
    hID_perPID[CepUtil::PROTON] = new CepHistograms_ID("ID", false, file_PPBAR);
    
    {
        TCanvas *c = new TCanvas("c", "c", 800, 400);
        c->SetFillColor(-1);
        c->Print(dirName+"dEdx_vs_qp.pdf[");
        
        for(int ff=0; ff<2; ++ff){
            TH2F * hdEdx_vs_qp = ff==0 ? hID->hdEdx_vs_qp : hID->hdEdx_vs_qp_exclusiveCandidates;
            
            
            double lm = 0.08;
            double rm = 0.12;
            double tm = 0.03;
            double bm = 0.16;
            
            gPad->SetLeftMargin(lm);
            gPad->SetRightMargin(rm);
            gPad->SetTopMargin(tm);
            gPad->SetBottomMargin(bm);
            c->SetLogz();
            gStyle->SetPalette(55);
            
            hdEdx_vs_qp->Draw("colz");
        //     hdEdx_vs_qp->Rebin2D();
            
            hdEdx_vs_qp->GetXaxis()->SetTitleSize(0.06);
            hdEdx_vs_qp->GetXaxis()->SetLabelSize(0.06);
            hdEdx_vs_qp->GetXaxis()->SetTickLength(0.005);
            hdEdx_vs_qp->GetXaxis()->SetTitleOffset(1.12);
            
            hdEdx_vs_qp->GetYaxis()->SetTitleSize(0.06);
            hdEdx_vs_qp->GetYaxis()->SetLabelSize(0.06);
            hdEdx_vs_qp->GetYaxis()->SetTickLength(0.005);
            hdEdx_vs_qp->GetYaxis()->SetTitleOffset(0.57);
            
            hdEdx_vs_qp->GetZaxis()->SetTitleSize(0.06);
            hdEdx_vs_qp->GetZaxis()->SetLabelSize(0.06);
            hdEdx_vs_qp->GetZaxis()->SetTickLength(0.005);
            hdEdx_vs_qp->GetZaxis()->SetTitleOffset(0.85);
            
            hdEdx_vs_qp->GetXaxis()->SetTitle("#frac{q}{e} #times p [GeV]");
            hdEdx_vs_qp->GetYaxis()->SetTitle("#frac{dE}{dx} [MeV cm^{2}/g]");
            hdEdx_vs_qp->GetYaxis()->SetRangeUser(0, 6);
            hdEdx_vs_qp->GetXaxis()->SetRangeUser(-2, 2);
            hdEdx_vs_qp->GetZaxis()->SetTitle(Form("Tracks / (%d MeV #times %d keV cm^{2}/g)", static_cast<int>(1e3*hdEdx_vs_qp->GetXaxis()->GetBinWidth(1)), static_cast<int>(1e3*hdEdx_vs_qp->GetYaxis()->GetBinWidth(1))));
            
            c->Update();
            TPaletteAxis* palette = (TPaletteAxis*)hdEdx_vs_qp->GetListOfFunctions()->FindObject("palette");
            palette->SetX1NDC(1-rm);
            palette->SetX2NDC(1-rm+0.02);
            palette->SetY1NDC(bm);
            palette->SetY2NDC(1-tm);
            
            TLatex l; l.SetNDC();
            l.SetTextColor(kBlack);
            l.SetTextFont(72);
            l.SetTextSize(.06);
            l.DrawLatex(.1,.02,Form("ATLAS #font[42]{Internal}"));
        //     l.DrawLatex(.12,.9,Form("#font[42]{p+p #rightarrow p'+X+p'}"));
            l.SetTextSize(.05);
        //     l.DrawLatex(.1,.75,Form("#font[42]{#sqrt[]{s} = 13 TeV,  #beta*=90 m}" ));
        //     l.DrawLatex(.244,.84,Form("#font[42]{X #rightarrow 4 tracks}" ));
        //     l.DrawLatex(.61,.9, Form("#font[42]{p_{T} > 0.1 GeV,   |#eta| < 2.5}" ));
        //     l.DrawLatex(.61,.82, Form("#font[42]{max(p_{T}) > 0.2 GeV}" ));
        //     l.DrawLatex(.61,.74, Form("#font[42]{d_{0} > 1.5 mm}" ));
            //   l.DrawLatex(.72,.84,Form("#font[42]{#it{(logarithmic color scale)}}" ));
            
            
            
            TLegend *legend = new TLegend(0.75-0.05, 0.67, 1-rm-0.02-0.05, 0.85);
            legend->SetBorderSize(0);
            legend->SetFillColorAlpha(kWhite, 0.8);
            
            TF1 *fdEdx[CepUtil::nDefinedParticles];
            TF1 *fdEdx2[CepUtil::nDefinedParticles];
            Double_t params[5] = {2.21261, 2.64564, 0.707229, 0.905925, 0.118289};
            for(int pid=0; pid<CepUtil::nDefinedParticles; ++pid){
                fdEdx[pid] = new TF1(TString("fdEdx")+Form("_PID%d",pid), gammaBetaNotLog, 0.05, 20.0, 6);
                fdEdx[pid]->SetNpx(1e4);
                fdEdx2[pid] = new TF1(TString("fdEdx2")+Form("_PID%d",pid), gammaBetaNotLog, -20, -0.05, 6);
                fdEdx2[pid]->SetNpx(1e4);
                for(int i=0; i<5; ++i){
                    fdEdx[pid]->FixParameter(i, params[i]);
                    fdEdx2[pid]->FixParameter(i, params[i]);
                }
                fdEdx[pid]->SetParameter(5, pid==CepUtil::PION?0.13957:(pid==CepUtil::KAON?0.49368:0.938));
                fdEdx2[pid]->SetParameter(5, pid==CepUtil::PION?0.13957:(pid==CepUtil::KAON?0.49368:0.938));
                fdEdx[pid]->SetLineColor(pid+1);
                fdEdx2[pid]->SetLineColor(pid+1);
                fdEdx[pid]->SetLineWidth(2);
                fdEdx2[pid]->SetLineWidth(2);
                fdEdx[pid]->Draw("SAME");
                fdEdx2[pid]->Draw("SAME");
                legend->AddEntry(fdEdx[pid], pid==CepUtil::PION?"#pi^{-}/#pi^{+}":(pid==CepUtil::KAON?"K^{-}/K^{+}":"#bar{p}/p"), "l");
            }
            
            legend->Draw();
            //   {
            //     TLine *line = new TLine(-2, 2.6, 2, 2.6);
            //     line->SetLineColor(kMagenta);
            //     line->SetLineWidth(18);
            //     line->SetLineStyle(2);
            //     line->Draw();
            //     
            //     TArrow *arrow = new TArrow(0,  2.6, 0.0, 1.5, 0.02,"|>");
            //     arrow->SetLineWidth(16);
            //     //       arrow->SetLineStyle(7);
            //     arrow->SetLineColor(kMagenta);
            //     arrow->SetFillColor(kMagenta);
            //     arrow->Draw();
            //     
            //       
            //   }
            c->Print(dirName+"dEdx_vs_qp.pdf");
        }
        c->Print(dirName+"dEdx_vs_qp.pdf]");
    }
    
    
    gStyle->SetPalette(69);
    TColor::InvertPalette();
    
    
    for(int ff=0; ff<4; ++ff){
        TCanvas *c2 = new TCanvas("c2", "c", 800, 400);
        TString filenameStr(ff==0?"MC_dEdx_vs_p_singlePID":(ff==1?"MC_dEdx_vs_p_perExclusiveChannel":(ff==2?"DATA_dEdx_vs_p_singlePID":"DATA_dEdx_vs_p_perExclusiveChannel")));
        
        c2->Print(dirName+filenameStr+".pdf[");
        for(int pid=0; pid<CepUtil::nDefinedParticles; ++pid){
            TH2F * hdEdx_vs_p;
            switch(ff){
                case 0: hdEdx_vs_p = hID_CD->hdEdx_vs_p_TruePID[pid]; break;
                case 1: hdEdx_vs_p = hID_perPID[pid]->hdEdx_vs_p_exclusiveCandidates_perPID[pid]; break;
                case 2: hdEdx_vs_p = hID->hdEdx_vs_p_exclusiveCandidates; break;
                case 3: hdEdx_vs_p = hID->hdEdx_vs_p_exclusiveCandidates_perPID[pid]; break;
            }
            
            c2->SetFillColor(-1);
            
            double lm = 0.08;
            double rm = 0.12;
            double tm = 0.06;
            double bm = 0.16;
            
            gPad->SetLeftMargin(lm);
            gPad->SetRightMargin(rm);
            gPad->SetTopMargin(tm);
            gPad->SetBottomMargin(bm);
    //         c2->SetLogz();
    //         gStyle->SetPalette(55);
            
            hdEdx_vs_p->Draw("colz");
        //     hdEdx_vs_p->Rebin2D();
            
            hdEdx_vs_p->GetXaxis()->SetTitleSize(0.06);
            hdEdx_vs_p->GetXaxis()->SetLabelSize(0.06);
            hdEdx_vs_p->GetXaxis()->SetTickLength(0.005);
            hdEdx_vs_p->GetXaxis()->SetTitleOffset(1.12);
            
            hdEdx_vs_p->GetYaxis()->SetTitleSize(0.06);
            hdEdx_vs_p->GetYaxis()->SetLabelSize(0.06);
            hdEdx_vs_p->GetYaxis()->SetTickLength(0.005);
            hdEdx_vs_p->GetYaxis()->SetTitleOffset(0.57);
            
            hdEdx_vs_p->GetZaxis()->SetTitleSize(0.06);
            hdEdx_vs_p->GetZaxis()->SetLabelSize(0.06);
            hdEdx_vs_p->GetZaxis()->SetTickLength(0.005);
            hdEdx_vs_p->GetZaxis()->SetTitleOffset(0.85);
            
            hdEdx_vs_p->GetXaxis()->SetTitle("p [GeV]");
            hdEdx_vs_p->GetYaxis()->SetTitle("#frac{dE}{dx} [MeV cm^{2}/g]");
            hdEdx_vs_p->GetYaxis()->SetRangeUser(0, 6);
            hdEdx_vs_p->GetXaxis()->SetRangeUser(0, 2);
            if(ff==2 && pid!=CepUtil::PION){
                hdEdx_vs_p->GetZaxis()->SetRangeUser(0, 1.5*hdEdx_vs_p->GetBinContent( hdEdx_vs_p->GetXaxis()->FindBin(pid==CepUtil::KAON?0.4:0.7), hdEdx_vs_p->GetYaxis()->FindBin( mParams->fdEdx[pid]->Eval(pid==CepUtil::KAON?0.4:0.7) ) ) );
            }
            hdEdx_vs_p->GetZaxis()->SetTitle(Form("Tracks / (%d MeV #times %d keV cm^{2}/g)", static_cast<int>(1e3*hdEdx_vs_p->GetXaxis()->GetBinWidth(1)), static_cast<int>(1e3*hdEdx_vs_p->GetYaxis()->GetBinWidth(1))));
            
            c2->Update();
            TPaletteAxis* palette = (TPaletteAxis*)hdEdx_vs_p->GetListOfFunctions()->FindObject("palette");
            palette->SetX1NDC(1-rm);
            palette->SetX2NDC(1-rm+0.02);
            palette->SetY1NDC(bm);
            palette->SetY2NDC(1-tm);
            
             TLegend *legend = new TLegend(0.65, 0.6, 1-rm-0.02-0.05, 0.81);
            legend->SetBorderSize(0);
            legend->SetLineWidth(3);
            legend->SetFillColorAlpha(kWhite, 0.9);
            
            TLatex l; l.SetNDC();
            l.SetTextColor(kBlack);
            l.SetTextFont(72);
            l.SetTextSize(.06);
            if(ff==2||ff==3)
                l.DrawLatex(.55,.84,Form("ATLAS #font[42]{Internal}"));
            else
                l.DrawLatex(.5,.84,Form("ATLAS #font[42]{Simulation Internal}"));
        //     l.DrawLatex(.12,.9,Form("#font[42]{p+p #rightarrow p'+X+p'}"));
            l.SetTextSize(.05);
        //     l.DrawLatex(.1,.75,Form("#font[42]{#sqrt[]{s} = 13 TeV,  #beta*=90 m}" ));
        //     l.DrawLatex(.244,.84,Form("#font[42]{X #rightarrow 4 tracks}" ));
        //     l.DrawLatex(.61,.9, Form("#font[42]{p_{T} > 0.1 GeV,   |#eta| < 2.5}" ));
        //     l.DrawLatex(.61,.82, Form("#font[42]{max(p_{T}) > 0.2 GeV}" ));
        //     l.DrawLatex(.61,.74, Form("#font[42]{d_{0} > 1.5 mm}" ));
            //   l.DrawLatex(.72,.84,Form("#font[42]{#it{(logarithmic color scale)}}" ));
            
            
            
            
            if((ff==0||ff==2) && pid!=CepUtil::PION){
                const int N = 1e2;
                double xStart = mParams->minP[pid];
                double xStop  = pid==CepUtil::KAON ? 0.4 : 0.7;
                double xStep  = (xStop-xStart)/N;
                double x[N], ytop[N], ybottom[N];
                
                for (int j=0; j<N; ++j){
                    x[j] = xStart + j*xStep;
                    ytop[j] = mParams->/*fdEdx_cuts*/fdEdx_avg[pid][CepUtil::MAX]->Eval(x[j]);
                    ybottom[j] = std::max( mParams->/*fdEdx_cuts*/fdEdx_avg[pid][CepUtil::MIN]->Eval(x[j]), pid==CepUtil::KAON ? 2.2 : 2.6);
                }
                
                TGraph *grshade = new TGraph(2*N);
                TGraph *grshade2 = new TGraph(2*N);
                for (int j=0; j<N; ++j){
                    grshade->SetPoint(j,x[j],ytop[j]);
                    grshade->SetPoint(N+j,x[N-j-1],ybottom[N-j-1]);
                    grshade2->SetPoint(j,x[N-j-1],ybottom[N-j-1]);
                    grshade2->SetPoint(N+j,x[j],ytop[j]);
                }
                grshade->SetFillColorAlpha(kBlack, 0.2);
                grshade2->SetFillColorAlpha(kBlack, 0.2);
//                 grshade->SetFillStyle(3013);
                grshade->Draw("lf SAME");
                grshade2->Draw("lf SAME");
            }
           
            
            
            mParams->fdEdx[pid]->Draw("SAME");
            if(pid==CepUtil::PION){
                mParams->fdEdx_shifted[pid][CepUtil::MAX]->Draw("SAME");
            } else{
                mParams->fdEdx_avg[pid][CepUtil::MIN]->Draw("SAME");
                mParams->fdEdx_avg[pid][CepUtil::MAX]->Draw("SAME");
                mParams->fdEdx_shifted[pid][CepUtil::MIN]->Draw("SAME");
                mParams->fdEdx_shifted[pid][CepUtil::MAX]->Draw("SAME");
                mParams->fdEdx_cuts[pid][CepUtil::MIN]->Draw("SAME");
                mParams->fdEdx_cuts[pid][CepUtil::MAX]->Draw("SAME");
            }
            
            legend->AddEntry(mParams->fdEdx[pid], pid==CepUtil::PION?"#scale[1.2]{#pi^{-}/#pi^{+}}":(pid==CepUtil::KAON?"#scale[1.2]{K^{-}/K^{+}}":"#scale[0.8]{#bar{p}/p}"), "l");
            
            legend->Draw();
            //   {
            //     TLine *line = new TLine(-2, 2.6, 2, 2.6);
            //     line->SetLineColor(kMagenta);
            //     line->SetLineWidth(18);
            //     line->SetLineStyle(2);
            //     line->Draw();
            //     
            //     TArrow *arrow = new TArrow(0,  2.6, 0.0, 1.5, 0.02,"|>");
            //     arrow->SetLineWidth(16);
            //     //       arrow->SetLineStyle(7);
            //     arrow->SetLineColor(kMagenta);
            //     arrow->SetFillColor(kMagenta);
            //     arrow->Draw();
            //     
            //       
            //   }
            c2->Print(dirName+filenameStr+".pdf");
        }
        c2->Print(dirName+filenameStr+".pdf]");
        
    }
    
    
    
    
    gStyle->SetPalette(55);
    
    
    
    TCanvas *c = new TCanvas("c", "c", 800, 400);
    c->SetFillColor(-1);
    
    TH3F * hdEdx_vs_qp_vs_eta = hID->hdEdx_vs_qp_vs_eta;
    
    c->Print(dirName+"dEdx_vs_qp_etaBins.pdf[");
    for(int j=1; j<=hdEdx_vs_qp_vs_eta->GetNbinsX(); ++j){
        
        hdEdx_vs_qp_vs_eta->GetXaxis()->SetRange(j,j);
        TH2D * hdEdx_vs_qp = dynamic_cast<TH2D*>( hdEdx_vs_qp_vs_eta->Project3D("zy") );
        
        double lm = 0.08;
        double rm = 0.12;
        double tm = 0.03;
        double bm = 0.16;
        
        gPad->SetLeftMargin(lm);
        gPad->SetRightMargin(rm);
        gPad->SetTopMargin(tm);
        gPad->SetBottomMargin(bm);
        c->SetLogz();
        gStyle->SetPalette(55);
        
        hdEdx_vs_qp->Draw("colz");
    //     hdEdx_vs_qp->Rebin2D();
        
        hdEdx_vs_qp->GetXaxis()->SetTitleSize(0.06);
        hdEdx_vs_qp->GetXaxis()->SetLabelSize(0.06);
        hdEdx_vs_qp->GetXaxis()->SetTickLength(0.005);
        hdEdx_vs_qp->GetXaxis()->SetTitleOffset(1.12);
        
        hdEdx_vs_qp->GetYaxis()->SetTitleSize(0.06);
        hdEdx_vs_qp->GetYaxis()->SetLabelSize(0.06);
        hdEdx_vs_qp->GetYaxis()->SetTickLength(0.005);
        hdEdx_vs_qp->GetYaxis()->SetTitleOffset(0.57);
        
        hdEdx_vs_qp->GetZaxis()->SetTitleSize(0.06);
        hdEdx_vs_qp->GetZaxis()->SetLabelSize(0.06);
        hdEdx_vs_qp->GetZaxis()->SetTickLength(0.005);
        hdEdx_vs_qp->GetZaxis()->SetTitleOffset(0.85);
        
        hdEdx_vs_qp->GetXaxis()->SetTitle("#frac{q}{e} #times p [GeV]");
        hdEdx_vs_qp->GetYaxis()->SetTitle("#frac{dE}{dx} [MeV cm^{2}/g]");
        hdEdx_vs_qp->GetYaxis()->SetRangeUser(0, 6);
        hdEdx_vs_qp->GetZaxis()->SetTitle(Form("Tracks / (%d MeV #times %d keV cm^{2}/g)", static_cast<int>(1e3*hdEdx_vs_qp->GetXaxis()->GetBinWidth(1)), static_cast<int>(1e3*hdEdx_vs_qp->GetYaxis()->GetBinWidth(1))));
        
        c->Update();
        TPaletteAxis* palette = (TPaletteAxis*)hdEdx_vs_qp->GetListOfFunctions()->FindObject("palette");
        palette->SetX1NDC(1-rm);
        palette->SetX2NDC(1-rm+0.02);
        palette->SetY1NDC(bm);
        palette->SetY2NDC(1-tm);
        
        TLatex l; l.SetNDC();
        l.SetTextColor(kBlack);
        l.SetTextFont(72);
        l.SetTextSize(.06);
        l.DrawLatex(.1,.74-0.3,Form("ATLAS #font[42]{Internal}"));
        l.DrawLatex(.1,.02, Form("#font[42]{%g < #eta < %g}", hdEdx_vs_qp_vs_eta->GetXaxis()->GetBinLowEdge(j), hdEdx_vs_qp_vs_eta->GetXaxis()->GetBinUpEdge(j)));
    //     l.DrawLatex(.12,.9,Form("#font[42]{p+p #rightarrow p'+X+p'}"));
        l.SetTextSize(.05);
    //     l.DrawLatex(.1,.75,Form("#font[42]{#sqrt[]{s} = 13 TeV,  #beta*=90 m}" ));
    //     l.DrawLatex(.244,.84,Form("#font[42]{X #rightarrow 4 tracks}" ));
    //     l.DrawLatex(.61,.9, Form("#font[42]{p_{T} > 0.1 GeV,   |#eta| < 2.5}" ));
    //     l.DrawLatex(.61,.82, Form("#font[42]{max(p_{T}) > 0.2 GeV}" ));
    //     l.DrawLatex(.61,.74, Form("#font[42]{d_{0} > 1.5 mm}" ));
        //   l.DrawLatex(.72,.84,Form("#font[42]{#it{(logarithmic color scale)}}" ));
        
        
        
        TLegend *legend = new TLegend(0.75-0.05, 0.47, 1-rm-0.02-0.05, 0.65);
        legend->SetBorderSize(0);
        legend->SetFillColorAlpha(kWhite, 0.8);
        
        TF1 *fdEdx[CepUtil::nDefinedParticles];
        TF1 *fdEdx2[CepUtil::nDefinedParticles];
        Double_t params[5] = {2.21261, 2.64564, 0.707229, 0.905925, 0.118289};
        for(int pid=0; pid<CepUtil::nDefinedParticles; ++pid){
            fdEdx[pid] = new TF1(TString("fdEdx")+Form("_PID%d",pid), gammaBetaNotLog, 0.05, 20.0, 6);
            fdEdx[pid]->SetNpx(1e4);
            fdEdx2[pid] = new TF1(TString("fdEdx2")+Form("_PID%d",pid), gammaBetaNotLog, -20, -0.05, 6);
            fdEdx2[pid]->SetNpx(1e4);
            for(int i=0; i<5; ++i){
                fdEdx[pid]->FixParameter(i, params[i]);
                fdEdx2[pid]->FixParameter(i, params[i]);
            }
            fdEdx[pid]->SetParameter(5, pid==CepUtil::PION?0.13957:(pid==CepUtil::KAON?0.49368:0.938));
            fdEdx2[pid]->SetParameter(5, pid==CepUtil::PION?0.13957:(pid==CepUtil::KAON?0.49368:0.938));
            fdEdx[pid]->SetLineColor(pid+1);
            fdEdx2[pid]->SetLineColor(pid+1);
            fdEdx[pid]->SetLineWidth(2);
            fdEdx2[pid]->SetLineWidth(2);
            fdEdx[pid]->Draw("SAME");
            fdEdx2[pid]->Draw("SAME");
            legend->AddEntry(fdEdx[pid], pid==CepUtil::PION?"#pi^{-}/#pi^{+}":(pid==CepUtil::KAON?"K^{-}/K^{+}":"#bar{p}/p"), "l");
        }
        
        legend->Draw();
        //   {
        //     TLine *line = new TLine(-2, 2.6, 2, 2.6);
        //     line->SetLineColor(kMagenta);
        //     line->SetLineWidth(18);
        //     line->SetLineStyle(2);
        //     line->Draw();
        //     
        //     TArrow *arrow = new TArrow(0,  2.6, 0.0, 1.5, 0.02,"|>");
        //     arrow->SetLineWidth(16);
        //     //       arrow->SetLineStyle(7);
        //     arrow->SetLineColor(kMagenta);
        //     arrow->SetFillColor(kMagenta);
        //     arrow->Draw();
        //     
        //       
        //   }
        c->Print(dirName+"dEdx_vs_qp_etaBins.pdf");
    }
    c->Print(dirName+"dEdx_vs_qp_etaBins.pdf]");
    
    
    
    
    
    delete hID;
}


void CepAnalysis::drawRecoMethodsComparison(TFile * file, TFile * file_signal, TFile * file_cd){
    
    const TString dirName("PDF_PxPyDifferentMethods/");
    mkdir(dirName.Data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    
    CepHistograms_MomentumBalance *hMomBal_data = new CepHistograms_MomentumBalance("MomentumBalance", false, file);
    CepHistograms_MomentumBalance *hMomBal_signal = new CepHistograms_MomentumBalance("MomentumBalance", false, file_signal);
    CepHistograms_MomentumBalance *hMomBal_cd = new CepHistograms_MomentumBalance("MomentumBalance", false, file_cd);
    
    std::vector<TString> listOfBranchComb;
    listOfBranchComb.push_back(TString("AU-CU"));
    listOfBranchComb.push_back(TString("AU-CD"));
    listOfBranchComb.push_back(TString("AD-CU"));
    listOfBranchComb.push_back(TString("AD-CD"));
    
    
    TCanvas *c = new TCanvas("c", "c", 800, /*2**/600);
    c->SetFillColor(-1);
    c->SetFrameLineWidth(2);
    gPad->SetLeftMargin(0.14);
    gPad->SetRightMargin(0.015);
    gPad->SetTopMargin(0.06);
    gPad->SetBottomMargin(0.13);
    TGaxis::SetMaxDigits(3);
    
    TLegend *legend;
    TLatex l; l.SetNDC(); l.SetTextFont(72); l.SetTextSize(.05);
    
//     c->Divide(1,2);
    
    c->Print(dirName+"MomentumBalance_PerBranchesConfigurations.pdf[");
    
    for(int br=0; br<CepUtil::nBranchesConfigurations; ++br){
        for(int j=0; j<CepUtil::nCoordinates; ++j){
            
//             c->cd(j+1);
//             gPad->SetGridx();
            
            TH1F *hBalance_DATA[3];
            TH1F *hBalance_MC[3];
            for(int m=0; m<3; ++m){
                hBalance_DATA[m] = hMomBal_data->mhTotalMomentum_PerProtonsConfigurations_DifferentMethods[CepUtil::PIPI][j][br][m];
                hBalance_MC[m] = hMomBal_cd->mhTotalMomentum_PerProtonsConfigurations_DifferentMethods[CepUtil::PIPI][j][br][m];
                
                hBalance_DATA[m]->Rebin(j==CepUtil::X ? 10 : 4);
                hBalance_MC[m]->Rebin(j==CepUtil::X ? 10 : 4);
            }
            
            hBalance_DATA[0]->GetXaxis()->SetTitle(j==0 ? "p_{x}^{miss} [GeV]" : "p_{y}^{miss} [GeV]");
            
            for(int m=0; m<3; ++m){
                hBalance_DATA[m]->SetMarkerColor( m==0 ? kBlack : (m==1 ? kRed : kBlue) );
                hBalance_DATA[m]->SetMarkerStyle( m==0 ? 20 : (m==1 ? 21 : 22) );
                hBalance_DATA[m]->SetMarkerSize( 1 );
                hBalance_DATA[m]->SetLineColor( m==0 ? kBlack : (m==1 ? kRed : kBlue) );
                hBalance_DATA[m]->SetLineWidth( 2 );
                
                hBalance_MC[m]->SetMarkerColor( m==0 ? kBlack : (m==1 ? kRed : kBlue) );
                hBalance_MC[m]->SetMarkerStyle( m==0 ? 20 : (m==1 ? 21 : 22) );
                hBalance_MC[m]->SetMarkerSize( 1 );
                hBalance_MC[m]->SetLineColor( m==0 ? kBlack : (m==1 ? kRed : kBlue) );
                hBalance_MC[m]->SetFillColorAlpha( m==0 ? kBlack : (m==1 ? kRed : kBlue), 0.1 );
                hBalance_MC[m]->SetLineWidth( 2 );
                
                hBalance_DATA[m]->Scale(hBalance_DATA[0]->GetMaximum()/hBalance_DATA[0]->GetMaximum());
                hBalance_MC[m]->Scale(hBalance_DATA[m]->GetMaximum()/hBalance_MC[m]->GetMaximum());
            }
            
            
            
            hBalance_DATA[0]->GetXaxis()->SetNdivisions(8,5,1);
      
            hBalance_DATA[0]->GetXaxis()->SetTitleSize( 0.05 );
            hBalance_DATA[0]->GetXaxis()->SetTitleOffset( 1.04 );
            hBalance_DATA[0]->GetXaxis()->SetLabelSize( 0.05 );
            hBalance_DATA[0]->GetXaxis()->SetLabelOffset( 0.007 );
            
            hBalance_DATA[0]->GetYaxis()->SetTitleSize( 0.05 );
            hBalance_DATA[0]->GetYaxis()->SetTitleOffset( 1.35 );
            hBalance_DATA[0]->GetYaxis()->SetLabelSize( 0.05 );
            hBalance_DATA[0]->GetYaxis()->SetLabelOffset( 0.007 );
            
            if(j==1)
                hBalance_DATA[0]->GetXaxis()->SetRangeUser(-0.4,0.4);
            hBalance_DATA[0]->GetYaxis()->SetTitle( TString( Form("Events / %d MeV", static_cast<int>(1e3*hBalance_DATA[0]->GetXaxis()->GetBinWidth( hBalance_DATA[0]->GetNbinsX()/2 )) ) ) );
            hBalance_DATA[0]->GetYaxis()->SetRangeUser(0, 1.15 * hBalance_DATA[j==0 ? 2 : 0]->GetMaximum());
            
            hBalance_DATA[0]->Draw("PE");
            hBalance_MC[0]->Draw("HIST SAME");
            hBalance_DATA[1]->Draw("PE SAME");
            hBalance_MC[1]->Draw("HIST SAME");
            hBalance_DATA[2]->Draw("PE SAME");
            hBalance_MC[2]->Draw("HIST SAME");
            
            
            legend = new TLegend(0.2, 0.46, 0.4,0.74);
            legend->SetTextSize(0.035);
            legend->SetBorderSize(0);
            legend->SetFillColor(-1);
            legend->SetFillStyle(0);
            legend->AddEntry(hBalance_DATA[0], "1 (data)", "pel");
            legend->AddEntry(hBalance_MC[0], "1 (MC)", "f");
            legend->AddEntry(hBalance_DATA[1], "2 (data)", "pel");
            legend->AddEntry(hBalance_MC[1], "2 (MC)", "f");
            legend->AddEntry(hBalance_DATA[2], "3 (data)", "pel");
            legend->AddEntry(hBalance_MC[2], "3 (MC)", "f");
            legend->Draw();
            
            
            l.SetNDC(); l.SetTextFont(72);
            l.SetTextSize(.05);
            l.DrawLatex(.18,.87,Form("ATLAS #font[42]{Internal}"));
            l.DrawLatex(.635,.87,Form("#font[62]{Branches %s}", listOfBranchComb[br].Data()));
            
            l.DrawLatex(.7, .65, Form("#font[42]{#sqrt{s} = 13 TeV}") );
            l.DrawLatex(.7, .58, Form("#font[42]{#beta* = 90 m}") );
            l.DrawLatex(.7, .51, Form("#font[42]{p+p #rightarrow p+#pi^{+}#pi^{-}+p}") );
            
            gPad->RedrawAxis();
            
            c->Print(dirName+"MomentumBalance_PerBranchesConfigurations.pdf");
        }
        
    }
    
    c->Print(dirName+"MomentumBalance_PerBranchesConfigurations.pdf]");
    
}




void CepAnalysis::drawMomentumBalancePxPy(TFile * file, TFile * file_mc ){
    
    TH1F * hMean[CepUtil::nCoordinates][2];
    TH1F * hWidth[CepUtil::nCoordinates][2];
    TH1F * hMean_perChannel[CepUtil::nCoordinates][2][CepUtil::nCepChannels];
    TH1F * hWidth_perChannel[CepUtil::nCoordinates][2][CepUtil::nCepChannels];
    
    for(int coor=0; coor<CepUtil::nCoordinates; ++coor){
        for(int f=0; f<2; ++f){
            hMean[coor][f] = new TH1F( Form("hMean%d%d", coor, f), "", CepUtil::nBranchesConfigurations, 0, CepUtil::nBranchesConfigurations );
            hWidth[coor][f] = new TH1F( Form("hWidth%d%d", coor, f), "", CepUtil::nBranchesConfigurations, 0, CepUtil::nBranchesConfigurations );
            for(int ch=0; ch<CepUtil::nCepChannels; ++ch){
                hMean_perChannel[coor][f][ch] = new TH1F( Form("hMean_perChannel%d%d%d", coor, f, ch), "", CepUtil::nBranchesConfigurations, 0, CepUtil::nBranchesConfigurations );
                hWidth_perChannel[coor][f][ch] = new TH1F( Form("hWidth_perChannel%d%d%d", coor, f, ch), "", CepUtil::nBranchesConfigurations, 0, CepUtil::nBranchesConfigurations );
            }
        }
    }
    
    
    for(int f=0; f<2; ++f){
        
        const TString dirName( f==0 ? "PDF_MomentumBalance/" : "PDF_MomentumBalance_MC/");
        mkdir(dirName.Data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
        
        CepHistograms_MomentumBalance *hMomBal = new CepHistograms_MomentumBalance("MomentumBalance", false, f==0 ? file : file_mc);
        
        
        TCanvas* c = new TCanvas("c", "c", 800, 670);
        //         gStyle->SetLineScalePS();
        double leftMargin = 0.16;
        double rightMargin = 0.18;
        double topMargin = 0.06;
        double bottomMargin = 0.16;
        TGaxis::SetMaxDigits(3);
        
        gStyle->SetPalette(69);
        TColor::InvertPalette();
        
        gStyle->SetNumberContours(99);
        
        
        for(int tt=0; tt<2; ++tt){

            TString extraStr(tt==0 ? "" : "_BalancedInTheOtherCoordinate");
            
            c->Print(dirName+"PxCentralVsPxForward"+extraStr+".pdf[");
            c->Print(dirName+"PyCentralVsPyForward"+extraStr+".pdf[");

        //     TH2F * hPyVsPxProtons[2];
        //     TH1D* mandelstamT_perSide[2];

            
            for(int sp=0; sp<CepUtil::nCepChannels; ++sp){
                
                c->Clear();
                
                c->SetLogz(kFALSE);
                c->SetFillStyle(4000);
                c->SetFillColor(0);
                c->SetFrameFillStyle(4000);
                c->SetLeftMargin(leftMargin);
                c->SetRightMargin(rightMargin);
                c->SetTopMargin(topMargin);
                c->SetBottomMargin(bottomMargin);
                
                for(int coor=0; coor<2; ++coor){
                    
                    TH2F *hPlot = (tt==0 ? hMomBal->mhTotalMomentum_CentralVsForward : hMomBal->mhTotalMomentum_CentralVsForward_BalancedInTheOtherCoordinate)[sp][coor][CepUtil::OPPO];
                    TString coorStr(coor==0?"x":"y");
                    
                    if(hPlot->GetEntries()==0) continue;
                    
                    int nrebin = sp==CepUtil::PPBAR ? 2 : (sp>CepUtil::SIXPI ? 4 : 1);
                    hPlot->Rebin2D(nrebin, nrebin);
                    
                    hPlot->GetXaxis()->SetLabelSize(1.5*hPlot->GetXaxis()->GetLabelSize());
                    hPlot->GetXaxis()->SetTitleSize(1.7*hPlot->GetXaxis()->GetTitleSize());
                    hPlot->GetYaxis()->SetLabelSize(1.5*hPlot->GetYaxis()->GetLabelSize());
                    hPlot->GetYaxis()->SetTitleSize(1.7*hPlot->GetYaxis()->GetTitleSize());
                    hPlot->GetZaxis()->SetLabelSize(1.5*hPlot->GetZaxis()->GetLabelSize());
                    hPlot->GetZaxis()->SetTitleSize(1.7*hPlot->GetZaxis()->GetTitleSize());
                    
                    hPlot->GetXaxis()->SetTitle("[#vec{p}_{p'}^{A}+#vec{p}_{p'}^{C}]_{"+coorStr+"} [GeV]");
                    hPlot->GetXaxis()->SetTitleOffset(1.13);
                    
                    hPlot->GetYaxis()->SetTitle("[#vec{p}_{"+mCepUtil->mCepChannelLatexName[sp]+"}]_{"+coorStr+"} [GeV]");
                    
                    hPlot->GetYaxis()->SetTitleOffset(1.18);
                    hPlot->GetXaxis()->SetRangeUser(-1.9, 1.9);
                    hPlot->GetXaxis()->SetNdivisions(5,5,1);
                    hPlot->GetYaxis()->SetRangeUser(-1.9, 1.9);
                    hPlot->GetYaxis()->SetNdivisions(5,5,1);
                    
                    hPlot->GetZaxis()->SetTitle(Form("Events / ( %d MeV #times  %d MeV )", static_cast<int>(nearbyint(1e3*hPlot->GetXaxis()->GetBinWidth(1))),  static_cast<int>(nearbyint(1e3*hPlot->GetYaxis()->GetBinWidth(1)))));
                    hPlot->GetZaxis()->SetTitleOffset(1.1);
                    hPlot->Draw("colz");
                    
                    
                    TLine *exclusiveLine[2];
                    exclusiveLine[0] = new TLine(-1.8, 1.8 - 3*mParams->sigmaMissingMomentum[CepUtil::DATA][coor][sp]/1e3, 1.8 - 3*mParams->sigmaMissingMomentum[CepUtil::DATA][coor][sp]/1e3, -1.8);
                    exclusiveLine[1] = new TLine(-1.8 + 3*mParams->sigmaMissingMomentum[CepUtil::DATA][coor][sp]/1e3, 1.8, 1.8, -1.8 + 3*mParams->sigmaMissingMomentum[CepUtil::DATA][coor][sp]/1e3);
                    
                    for(int l=0; l<2; ++l){
                        exclusiveLine[l]->SetNDC(kFALSE);
                        exclusiveLine[l]->SetLineWidth(3);
                        exclusiveLine[l]->SetLineStyle(7);
                        exclusiveLine[l]->SetLineColor(kGray+2);
                        exclusiveLine[l]->Draw();
                    }
                    
                    TLatex lReaction; 
                    lReaction.SetNDC(); 
                    lReaction.SetTextFont(42);  
                    lReaction.SetTextSize(0.8 * hPlot->GetYaxis()->GetTitleSize());
                    lReaction.DrawLatex(.25, .87, "#it{p}+#it{p}#rightarrow#it{p'}+"+mCepUtil->mCepChannelLatexName[sp]+"+#it{p'}  #sqrt{#it{s}} = 13 TeV"); 
                    TLatex l; l.SetNDC(); l.SetTextFont(72); l.SetTextSize(0.8 * hPlot->GetYaxis()->GetTitleSize());
                    l.DrawLatex(.2, .2, "ATLAS #font[42]{Internal}");
                    
                    
                    TLatex lCut( 0.58, 0.75, Form("n#sigma(p_{%s}^{miss}) < 3", coor==CepUtil::Y ? "x" : "y" ) );
                    lCut.SetNDC(); lCut.SetTextFont(42); lCut.SetTextSize(.05);
                    if(tt==1){
                        lCut.Draw();
                    }
                    
                    c->Print(dirName + TString(coor == 0 ? "PxCentralVsPxForward"+extraStr+".pdf" : "PyCentralVsPyForward"+extraStr+".pdf"));
                }
                
            }
            
            c->Print(dirName+"PxCentralVsPxForward"+extraStr+".pdf]");
            c->Print(dirName+"PyCentralVsPyForward"+extraStr+".pdf]");
        }
        
        
        
        
        
        
        c = new TCanvas("c", "c", 900, 800);
        c->SetFillColor(-1);
        c->SetFrameLineWidth(2);
        gPad->SetLeftMargin(0.14);
        gPad->SetRightMargin(0.015);
        gPad->SetTopMargin(0.06);
        gPad->SetBottomMargin(0.13);
        TGaxis::SetMaxDigits(3);
        
        TLegend *legend;
        TLatex l; l.SetNDC(); l.SetTextFont(72); l.SetTextSize(.05);
        
        for(int tt=0; tt<2; ++tt){
            
            TString extraStr(tt==0?"":"_PerBranchConf");
            
            c->Print(dirName+"PxBalance"+extraStr+".pdf[");
            c->Print(dirName+"PyBalance"+extraStr+".pdf[");
            
            for(int sp=0; sp<CepUtil::nCepChannels; ++sp){
                    
                    TString singleTrkPlusMinusStr;
            //     TString singleTrkStr[2];
            //     singleTrkStr[CepUtil::MINUS] = TString((sp!=CepUtil::KK && sp!=CepUtil::PPBAR) ? "#pi^{-}" : (sp==CepUtil::KK ? "K^{-}" : "#bar{#it{p}}") );
            //     singleTrkStr[CepUtil::PLUS] = TString((sp!=CepUtil::KK && sp!=CepUtil::PPBAR) ? "#pi^{+}" : (sp==CepUtil::KK ? "K^{+}" : "#it{p}") );
                singleTrkPlusMinusStr = TString( (sp!=CepUtil::KK && sp!=CepUtil::PPBAR) ? "#pi^{#pm}" : (sp==CepUtil::KK ? "K^{#pm}" : "#it{p},#bar{#it{p}}") );
                    
                for(int j=0; j<2; ++j){
                    
                    for(int brConf=0; brConf<(tt==0 ? 1 : CepUtil::nBranchesConfigurations ); ++brConf){
                        
                        TH2F *plot = tt==0 ? hMomBal->mhTotalMomentum_PyVsPx_BalancedInTheOtherCoordinate[sp][j][CepUtil::OPPO] : hMomBal->mhTotalMomentum_PyVsPx_BalancedInTheOtherCoordinate_PerBranchConf[sp][j][CepUtil::OPPO][brConf];
                        if(plot->GetEntries()==0) continue;
                        
                        
                        TH1D *hBalance = j==0 ?
                        plot->ProjectionX(Form("px%d%d%d%d",sp,j,tt,brConf)) : plot->ProjectionY(Form("py%d%d%d%d",sp,j,tt,brConf));
                        
                        hBalance->Rebin(sp>CepUtil::FOURPI ? (sp==CepUtil::TENPI ? (j==CepUtil::X ? 4 : 2) : 2) : (sp==CepUtil::PPBAR ? 2 : 1));
                        
                        hBalance->GetXaxis()->SetTitle(j==0 ? "p_{x}^{miss} [GeV]" : "p_{y}^{miss} [GeV]");
                        hBalance->SetMarkerColor( kBlack );
                        hBalance->SetMarkerStyle( 20 );
                        hBalance->SetMarkerSize( 1.6 );
                        hBalance->SetLineColor( kBlack );
                        hBalance->SetLineWidth( 2 );
                        hBalance->GetXaxis()->SetNdivisions(8,5,1);
                
                        hBalance->GetXaxis()->SetTitleSize( 0.05 );
                        hBalance->GetXaxis()->SetTitleOffset( 1.04 );
                        hBalance->GetXaxis()->SetLabelSize( 0.05 );
                        hBalance->GetXaxis()->SetLabelOffset( 0.007 );
                        
                        hBalance->GetYaxis()->SetTitleSize( 0.05 );
                        hBalance->GetYaxis()->SetTitleOffset( 1.35 );
                        hBalance->GetYaxis()->SetLabelSize( 0.05 );
                        hBalance->GetYaxis()->SetLabelOffset( 0.007 );
                        
                        hBalance->GetXaxis()->SetRangeUser(-1,1);
                        hBalance->GetYaxis()->SetTitle( TString( Form("Events / %d MeV", static_cast<int>(1e3*hBalance->GetXaxis()->GetBinWidth( hBalance->GetNbinsX()/2 )) ) ) );
                        hBalance->GetYaxis()->SetRangeUser(0, 1.8 * hBalance->GetMaximum());
                        hBalance->Draw("PE");
                        
                        
                        legend = new TLegend(0.2, ((sp>1) ? 0.05 : 0.0) + 0.36, 0.48, ((sp>1) ? 0.05 : 0.0) + 0.64);
                        legend->SetTextSize(0.035);
                        legend->SetBorderSize(0);
                        legend->SetFillColor(-1);
                        legend->SetFillStyle(0);
                        
                        TF1 *doubleGaus = new TF1(Form("doubleGaus_%d", static_cast<int>(1e6*gRandom->Uniform())), "gausn(0) + gausn(3)", -0.3, 0.3);
                        doubleGaus->SetParameters(hBalance->GetMaximum(), 0.0, j==0 ? 0.07 : 0.03, 0.05*hBalance->GetMaximum(), 0.0, 0.4);
                        doubleGaus->SetParLimits( 2, j==0 ? 0.02 : 0.005, j==0 ? 0.15 : 0.06 );
                        doubleGaus->SetParLimits( 5, 0.08, 1.0 );
                        doubleGaus->SetParError(5, 0.002);
                        doubleGaus->SetNpx(1e3);
                        doubleGaus->SetLineColor(kRed);
                        
                        double fitRange = sp==CepUtil::TENPI ? 0.9 : 0.5;
//                         if(tt>0)
//                             fitRange += 0.2;
                        double fitRangeOffset = 0;
                        if(tt>0)
                            fitRangeOffset = j==CepUtil::X ? 0.0 : (brConf==0 ? 0.2 : (brConf==3 ? -0.2 : 0.0));
                        
                        TFitResultPtr res = nullptr;
                        do{
                            res = hBalance->Fit(doubleGaus, "SIQ", "", -fitRange+fitRangeOffset, fitRange+fitRangeOffset);
                            fitRange += 0.05;
                        } while( !res->IsValid() /*&& j==CepUtil::Y*/ );
                        doubleGaus->Draw("same");
                //       opts.mObjToDraw.push_back( doubleGaus );
                //       opts.mObjToDrawOption.push_back("SAME");
                        
                        TF1* singleGaus[2];
                        singleGaus[0] = new TF1(Form("singleGaus_signal_%d", static_cast<int>(1e6*gRandom->Uniform())), "gausn", -fitRange+fitRangeOffset, fitRange+fitRangeOffset);
                        singleGaus[0]->SetNpx(1e3);
                        singleGaus[1] = new TF1(Form("singleGaus_background_%d", static_cast<int>(1e6*gRandom->Uniform())), "gausn", -fitRange+fitRangeOffset, fitRange+fitRangeOffset);
                        singleGaus[1]->SetNpx(1e3);
                        
                        singleGaus[0]->SetParameters( doubleGaus->GetParameter(0), doubleGaus->GetParameter(1), doubleGaus->GetParameter(2) );
                        singleGaus[1]->SetParameters( doubleGaus->GetParameter(3), doubleGaus->GetParameter(4), doubleGaus->GetParameter(5) );
                        
            //             hBalanceOffset[file][j]->SetBinContent( i+1, 1e3*doubleGaus->GetParameter(1) );
            //             hBalanceOffset[file][j]->SetBinError( i+1, 1e3*doubleGaus->GetParError(1) );
            //             hBalanceWidth[file][j]->SetBinContent( i+1, 1e3*doubleGaus->GetParameter(2) );
            //             hBalanceWidth[file][j]->SetBinError( i+1, 1e3*doubleGaus->GetParError(2) );
            //             hBalanceOffset_Bkgd[file][j]->SetBinContent( i+1, 1e3*doubleGaus->GetParameter(4) );
            //             hBalanceOffset_Bkgd[file][j]->SetBinError( i+1, 1e3*doubleGaus->GetParError(4) );
            //             hBalanceWidth_Bkgd[file][j]->SetBinContent( i+1, 1e3*doubleGaus->GetParameter(5) );
            //             hBalanceWidth_Bkgd[file][j]->SetBinError( i+1, 1e3*doubleGaus->GetParError(5) );
                        
                        singleGaus[0]->SetLineColor(kOrange-3);
                        singleGaus[0]->SetFillColorAlpha(kOrange-3, 0.3);
                        singleGaus[0]->SetFillStyle(1001);
                        singleGaus[0]->SetLineWidth(4.0);
                        singleGaus[0]->SetLineStyle(7);
                        singleGaus[1]->SetLineColor(kViolet);
                        singleGaus[1]->SetLineStyle(7);
                        
                        singleGaus[0]->Draw("same");
                        singleGaus[1]->Draw("same");
                        
                        if(tt>0){
                            if(sp==0){
                                hMean[j][f]->SetBinContent( brConf+1, 1e3*doubleGaus->GetParameter(1) );
                                hMean[j][f]->SetBinError( brConf+1, 1e3*doubleGaus->GetParError(1) );
                                hWidth[j][f]->SetBinContent( brConf+1, 1e3*doubleGaus->GetParameter(2) );
                                hWidth[j][f]->SetBinError( brConf+1, 1e3*doubleGaus->GetParError(2) );
                            }
                            hMean_perChannel[j][f][sp]->SetBinContent( brConf+1, 1e3*doubleGaus->GetParameter(1) );
                            hMean_perChannel[j][f][sp]->SetBinError( brConf+1, 1e3*doubleGaus->GetParError(1) );
                            hWidth_perChannel[j][f][sp]->SetBinContent( brConf+1, 1e3*doubleGaus->GetParameter(2) );
                            hWidth_perChannel[j][f][sp]->SetBinError( brConf+1, 1e3*doubleGaus->GetParError(2) );
                            
                        }
                        
                        TLatex lFitRes1(0.64, 0.55+0.05+0.1, Form("#mu = %.1f #pm %.1f MeV", 1e3*doubleGaus->GetParameter(1),  1e3*doubleGaus->GetParError(1)));
                        lFitRes1.SetNDC(); lFitRes1.SetTextFont(42); lFitRes1.SetTextSize(.04);
                        lFitRes1.SetTextColor(kOrange+5);
                        lFitRes1.Draw();
                        
                        TLatex lFitRes2(0.64, 0.49+0.05+0.1, Form("#sigma = %.1f #pm %.1f MeV",  1e3*doubleGaus->GetParameter(2),  1e3*doubleGaus->GetParError(2)));
                        lFitRes2.SetNDC(); lFitRes2.SetTextFont(42); lFitRes2.SetTextSize(.04);
                        lFitRes2.SetTextColor(kOrange+5);
                        lFitRes2.Draw();
                        
                        TLatex lFitRes3(0.64, 0.43+0.05+0.1, Form("#chi^{2}/ndf = %.1f/%d", res->Chi2(), res->Ndf() ) );
                        lFitRes3.SetNDC(); lFitRes3.SetTextFont(42); lFitRes3.SetTextSize(.04);
                        lFitRes3.SetTextColor(kOrange+5);
                        lFitRes3.Draw();
                        

                        legend->AddEntry(hBalance, "Data", "pel");
                        legend->AddEntry(doubleGaus, "Fit (total)", "l");
                        legend->AddEntry(singleGaus[0], "Signal (fit)", "f");
                        legend->AddEntry(singleGaus[1], "Bkgd (fit)", "l");
                        legend->Draw();
                        
                    
                        l.SetNDC(); l.SetTextFont(72);
                        l.SetTextSize(.05);
                        l.DrawLatex(.18,.87,Form("ATLAS #font[42]{Internal}"));
            // //             l.DrawLatex(.635,.35,Form("#font[62]{Run 282420}"));
                        if(tt>0)
                            l.DrawLatex(.7,.95,Form("#font[62]{Branches %d-%d}", mCepUtil->mBranchIdPerProtonConfiguration[brConf][CepUtil::A], mCepUtil->mBranchIdPerProtonConfiguration[brConf][CepUtil::C]));
            //             l.SetTextSize(.038);
            //             l.DrawLatex(.56,.917,Form("#font[42]{#sqrt[]{s} = 13 TeV,  #beta* = 90 m}" ));
            //             l.DrawLatex(.56,.877,Form("#font[42]{p+p #rightarrow p'+#pi^{+}#pi^{-}+p'}"));
            //             l.DrawLatex(.18,.807,Form("#font[42]{0.17 GeV < |p_{y}(p')| < 0.5 GeV    p_{T}(#pi) > 100 MeV}" ));
            //             l.DrawLatex(.655,.75,Form("#font[42]{|#eta(#pi)| < 2.5}" ));
                        
                        std::vector<TLatex> mTextToDraw;
                        const double mXaxisTitleSize = hBalance->GetXaxis()->GetTitleSize();
                        
                        
                        TLatex lCut(sp>CepUtil::EIGHTPI ? 0.76 : 0.7, 0.45, Form("n#sigma(p_{%s}^{miss}) < 3", j==CepUtil::Y ? "x" : "y" ) );
                        lCut.SetNDC(); lCut.SetTextFont(42); lCut.SetTextSize(.04);
                        lCut.Draw();
                        
                        TLatex lReaction(.49, .91-0.04, "#it{p}+#it{p}#rightarrow#it{p'}+"+mCepUtil->mCepChannelLatexName[sp]+"+#it{p'}  #sqrt{#it{s}} = 13 TeV"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * mXaxisTitleSize);
                        mTextToDraw.push_back( lReaction );
                        TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
                        TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}) > %.1f GeV", 0.2 );
                        TString tpcEta; tpcEta.Form("|#eta| < %.1f", 2.5 );
                        TString rpKinematicRegion1("0.18 GeV < |p_{y}| < 0.46 GeV");
                        TLatex lCutsA(.19,.84-0.04, singleTrkPlusMinusStr+":"); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * mXaxisTitleSize); mTextToDraw.push_back( lCutsA );
                        TLatex lCutsB(.52,.84-0.04, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * mXaxisTitleSize); mTextToDraw.push_back( lCutsB );
                        TLatex lCuts1(.26,.84-0.04, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * mXaxisTitleSize); mTextToDraw.push_back( lCuts1 );
                        TLatex lCuts12(.26,.795-0.04, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * mXaxisTitleSize); mTextToDraw.push_back( lCuts12 );
                        TLatex lCuts11(.26,.75-0.04, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * mXaxisTitleSize); mTextToDraw.push_back( lCuts11 );
                        TLatex lCuts3(.62,.84-0.04, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * mXaxisTitleSize); mTextToDraw.push_back( lCuts3 ); 
                        
                        for(unsigned int i=0; i<mTextToDraw.size(); ++i){
                            mTextToDraw[i].SetNDC(kTRUE);
                            mTextToDraw[i].Draw();
                        }
                
                        gPad->RedrawAxis();
                        
                        c->Print(dirName+ (j==0 ? "PxBalance"+extraStr+".pdf" : "PyBalance"+extraStr+".pdf"));
                    }
                }
                
            }
            
            c->Print(dirName+"PxBalance"+extraStr+".pdf]");
            c->Print(dirName+"PyBalance"+extraStr+".pdf]");
        }
        
        setupDrawingStyle(); // reset drawing style
        
        
        delete hMomBal;
        
    }
    
    
    
    {
        TCanvas *c = new TCanvas("c", "c", 900, 800);
        c->SetFillColor(-1);
        c->SetFrameLineWidth(2);
        gPad->SetLeftMargin(0.14);
        gPad->SetRightMargin(0.015);
        gPad->SetTopMargin(0.06);
        gPad->SetBottomMargin(0.13);
        TGaxis::SetMaxDigits(3);
        
        
        TLegend *legend;
        TLatex l; l.SetNDC(); l.SetTextFont(72); l.SetTextSize(.05);
        
        gStyle->SetGridColor(kGray);
        
        c->Print("MeanMomentumBalance.pdf["/*, "EmbedFonts"*/); 
        c->Print("SigmaMomentumBalance.pdf["/*, "EmbedFonts"*/); 
        
        
        for(int coor=0; coor<CepUtil::nCoordinates; ++coor){
            gPad->SetGridy();
            gPad->SetGridx();
            
            hMean[coor][CepUtil::DATA]->GetXaxis()->SetBinLabel(CepUtil::CONF_AU_CD+1, "AU-CD");
            hMean[coor][CepUtil::DATA]->GetXaxis()->SetBinLabel(CepUtil::CONF_AD_CU+1, "AD-CU");
            hMean[coor][CepUtil::DATA]->GetXaxis()->SetBinLabel(CepUtil::CONF_AU_CU+1, "AU-CU");
            hMean[coor][CepUtil::DATA]->GetXaxis()->SetBinLabel(CepUtil::CONF_AD_CD+1, "AD-CD");
            
            hMean[coor][CepUtil::DATA]->GetXaxis()->SetTitle("ALFA branches");
            hMean[coor][CepUtil::DATA]->SetMarkerColor( kBlack );
            hMean[coor][CepUtil::DATA]->SetMarkerStyle( 20 );
            hMean[coor][CepUtil::DATA]->SetMarkerSize( 1.6 );
            hMean[coor][CepUtil::DATA]->SetLineColor( kBlack );
            hMean[coor][CepUtil::DATA]->SetLineWidth( 2 );
        //     hMean[coor][CepUtil::DATA]->GetXaxis()->SetNdivisions(8,5,1);
            
            hMean[coor][CepUtil::DATA]->GetXaxis()->SetTitleSize( 0.05 );
            hMean[coor][CepUtil::DATA]->GetXaxis()->SetTitleOffset( 1.04 );
            hMean[coor][CepUtil::DATA]->GetXaxis()->SetLabelSize( 0.05 );
            hMean[coor][CepUtil::DATA]->GetXaxis()->SetLabelOffset( 0.007 );
            
            hMean[coor][CepUtil::DATA]->GetYaxis()->SetTitleSize( 0.05 );
            hMean[coor][CepUtil::DATA]->GetYaxis()->SetTitleOffset( 1.35 );
            hMean[coor][CepUtil::DATA]->GetYaxis()->SetLabelSize( 0.05 );
            hMean[coor][CepUtil::DATA]->GetYaxis()->SetLabelOffset( 0.007 );
            
        //     hMean[coor][CepUtil::DATA]->GetXaxis()->SetRangeUser(-0.5,0.5);
            hMean[coor][CepUtil::DATA]->GetYaxis()->SetTitle( Form("#LTp_{%s}^{miss}#GT [MeV]", coor==CepUtil::X ? "x" : "y") );
            hMean[coor][CepUtil::DATA]->GetYaxis()->SetRangeUser(-30, 30);
            hMean[coor][CepUtil::DATA]->Draw("PE");
            
            //--
            hMean_perChannel[coor][CepUtil::DATA][CepUtil::FOURPI]->SetMarkerColor( kBlue );
            hMean_perChannel[coor][CepUtil::DATA][CepUtil::FOURPI]->SetMarkerStyle( 20 );
            hMean_perChannel[coor][CepUtil::DATA][CepUtil::FOURPI]->SetMarkerSize( 1.6 );
            hMean_perChannel[coor][CepUtil::DATA][CepUtil::FOURPI]->SetLineColor( kBlue );
            hMean_perChannel[coor][CepUtil::DATA][CepUtil::FOURPI]->SetLineWidth( 2 );
            hMean_perChannel[coor][CepUtil::DATA][CepUtil::FOURPI]->Draw("PE SAME");
            
            hMean_perChannel[coor][CepUtil::DATA][CepUtil::SIXPI]->SetMarkerColor( kGray+1 );
            hMean_perChannel[coor][CepUtil::DATA][CepUtil::SIXPI]->SetMarkerStyle( 20 );
            hMean_perChannel[coor][CepUtil::DATA][CepUtil::SIXPI]->SetMarkerSize( 1.6 );
            hMean_perChannel[coor][CepUtil::DATA][CepUtil::SIXPI]->SetLineColor( kGray+1 );
            hMean_perChannel[coor][CepUtil::DATA][CepUtil::SIXPI]->SetLineWidth( 2 );
            hMean_perChannel[coor][CepUtil::DATA][CepUtil::SIXPI]->Draw("PE SAME");
            //--
            
            
            hMean[coor][CepUtil::MC]->SetMarkerColor( kRed );
            hMean[coor][CepUtil::MC]->SetMarkerStyle( 21 );
            hMean[coor][CepUtil::MC]->SetMarkerSize( 1.6 );
            hMean[coor][CepUtil::MC]->SetLineColor( kRed );
            hMean[coor][CepUtil::MC]->SetLineWidth( 2 );
            
            hMean[coor][CepUtil::MC]->Draw("PE SAME");
            
            
            legend = new TLegend(0.2+0.55, 0.15, 0.48+0.55, 0.34);
            legend->SetTextSize(0.035);
            legend->SetBorderSize(0);
            legend->SetFillColor(-1);
            legend->SetFillStyle(0);
            
            
            legend->AddEntry(hMean[coor][CepUtil::DATA], "Data (2#pi)", "pe");
            legend->AddEntry(hMean_perChannel[coor][CepUtil::DATA][CepUtil::FOURPI], "Data (4#pi)", "pe");
            legend->AddEntry(hMean_perChannel[coor][CepUtil::DATA][CepUtil::SIXPI], "Data (6#pi)", "pe");
            legend->AddEntry(hMean[coor][CepUtil::MC], "MC (2#pi)", "pe");
            legend->Draw();
            
            l.SetNDC(); l.SetTextFont(72);
            l.SetTextSize(.05);
            l.DrawLatex(.18,.87,Form("ATLAS #font[42]{Internal}"));
            
            
            gPad->RedrawAxis();
            
            c->Print("MeanMomentumBalance.pdf"/*, "EmbedFonts"*/); 

            
            
            
            
            
            
            hWidth[coor][CepUtil::DATA]->GetXaxis()->SetBinLabel(CepUtil::CONF_AU_CD+1, "AU-CD");
            hWidth[coor][CepUtil::DATA]->GetXaxis()->SetBinLabel(CepUtil::CONF_AD_CU+1, "AD-CU");
            hWidth[coor][CepUtil::DATA]->GetXaxis()->SetBinLabel(CepUtil::CONF_AU_CU+1, "AU-CU");
            hWidth[coor][CepUtil::DATA]->GetXaxis()->SetBinLabel(CepUtil::CONF_AD_CD+1, "AD-CD");
            
            hWidth[coor][CepUtil::DATA]->GetXaxis()->SetTitle("ALFA branches");
            hWidth[coor][CepUtil::DATA]->SetMarkerColor( kBlack );
            hWidth[coor][CepUtil::DATA]->SetMarkerStyle( 20 );
            hWidth[coor][CepUtil::DATA]->SetMarkerSize( 1.6 );
            hWidth[coor][CepUtil::DATA]->SetLineColor( kBlack );
            hWidth[coor][CepUtil::DATA]->SetLineWidth( 2 );
            
            hWidth[coor][CepUtil::DATA]->GetXaxis()->SetTitleSize( 0.05 );
            hWidth[coor][CepUtil::DATA]->GetXaxis()->SetTitleOffset( 1.04 );
            hWidth[coor][CepUtil::DATA]->GetXaxis()->SetLabelSize( 0.05 );
            hWidth[coor][CepUtil::DATA]->GetXaxis()->SetLabelOffset( 0.007 );
            
            hWidth[coor][CepUtil::DATA]->GetYaxis()->SetTitleSize( 0.05 );
            hWidth[coor][CepUtil::DATA]->GetYaxis()->SetTitleOffset( 1.35 );
            hWidth[coor][CepUtil::DATA]->GetYaxis()->SetLabelSize( 0.05 );
            hWidth[coor][CepUtil::DATA]->GetYaxis()->SetLabelOffset( 0.007 );
            
        //     hWidth[coor][CepUtil::DATA]->GetXaxis()->SetRangeUser(-0.5,0.5);
            hWidth[coor][CepUtil::DATA]->GetYaxis()->SetTitle( Form("#sigma(p_{%s}^{miss}) [MeV]", coor==CepUtil::X ? "x" : "y") );
            hWidth[coor][CepUtil::DATA]->GetYaxis()->SetRangeUser(coor==CepUtil::X ? 40 : 0, coor==CepUtil::X ? 90 : 50);
            hWidth[coor][CepUtil::DATA]->GetYaxis()->SetNdivisions(5,5,1);
            hWidth[coor][CepUtil::DATA]->Draw("PE");
            
            //--
            hWidth_perChannel[coor][CepUtil::DATA][CepUtil::FOURPI]->SetMarkerColor( kBlue );
            hWidth_perChannel[coor][CepUtil::DATA][CepUtil::FOURPI]->SetMarkerStyle( 20 );
            hWidth_perChannel[coor][CepUtil::DATA][CepUtil::FOURPI]->SetMarkerSize( 1.6 );
            hWidth_perChannel[coor][CepUtil::DATA][CepUtil::FOURPI]->SetLineColor( kBlue );
            hWidth_perChannel[coor][CepUtil::DATA][CepUtil::FOURPI]->SetLineWidth( 2 );
            hWidth_perChannel[coor][CepUtil::DATA][CepUtil::FOURPI]->Draw("PE SAME");
            
            hWidth_perChannel[coor][CepUtil::DATA][CepUtil::SIXPI]->SetMarkerColor( kGray+1 );
            hWidth_perChannel[coor][CepUtil::DATA][CepUtil::SIXPI]->SetMarkerStyle( 20 );
            hWidth_perChannel[coor][CepUtil::DATA][CepUtil::SIXPI]->SetMarkerSize( 1.6 );
            hWidth_perChannel[coor][CepUtil::DATA][CepUtil::SIXPI]->SetLineColor( kGray+1 );
            hWidth_perChannel[coor][CepUtil::DATA][CepUtil::SIXPI]->SetLineWidth( 2 );
            hWidth_perChannel[coor][CepUtil::DATA][CepUtil::SIXPI]->Draw("PE SAME");
            //--
            
            hWidth[coor][CepUtil::MC]->SetMarkerColor( kRed );
            hWidth[coor][CepUtil::MC]->SetMarkerStyle( 21 );
            hWidth[coor][CepUtil::MC]->SetMarkerSize( 1.6 );
            hWidth[coor][CepUtil::MC]->SetLineColor( kRed );
            hWidth[coor][CepUtil::MC]->SetLineWidth( 2 );
            
            hWidth[coor][CepUtil::MC]->Draw("PE SAME");
            
            legend = new TLegend(0.2+0.55, 0.15, 0.48+0.55, 0.34);
            legend->SetTextSize(0.035);
            legend->SetBorderSize(0);
            legend->SetFillColor(-1);
            legend->SetFillStyle(0);
            
            legend->AddEntry(hWidth[coor][CepUtil::DATA], "Data (2#pi)", "pe");
            legend->AddEntry(hWidth_perChannel[coor][CepUtil::DATA][CepUtil::FOURPI], "Data (4#pi)", "pe");
            legend->AddEntry(hWidth_perChannel[coor][CepUtil::DATA][CepUtil::SIXPI], "Data (6#pi)", "pe");
            legend->AddEntry(hWidth[coor][CepUtil::MC], "MC (2#pi)", "pe");
            legend->Draw();
            

            legend->Draw();
            
            l.SetNDC(); l.SetTextFont(72);
            l.SetTextSize(.05);
            l.DrawLatex(.18,.87,Form("ATLAS #font[42]{Internal}"));
            
            
            gPad->RedrawAxis();
            
            c->Print("SigmaMomentumBalance.pdf"/*, "EmbedFonts"*/); 
        }
        
        c->Print("MeanMomentumBalance.pdf]"/*, "EmbedFonts"*/); 
        c->Print("SigmaMomentumBalance.pdf]"/*, "EmbedFonts"*/); 
    }
    
    
}




void CepAnalysis::drawMbtsEff(TFile * file_signal, TFile * file_cd){
    
    const TString dirName("PDF_MBTSEff/");
    mkdir(dirName.Data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    
    
    CepHistograms_MBTS *hMBTS = nullptr;
    if(file_cd)
        hMBTS = new CepHistograms_MBTS("MBTS", false, file_cd);
    
    if(hMBTS){
        TCanvas *c;
        TGaxis::SetMaxDigits(3);
        //   TLegend *legend;
        TLatex l; l.SetNDC(); l.SetTextFont(72); l.SetTextSize(.05);
        
        c = new TCanvas("c", "c", 800, 600);
        TGaxis::SetMaxDigits(3);
        
        //         c->SetLeftMargin(0.11);
        c->SetRightMargin(0.1);
        //         c->SetTopMargin(0.02);
        //         c->SetBottomMargin(0.11);
        
        TGraphAsymmErrors *graph = hMBTS->effOnlineAndOfflineMBTSVeto_vs_Multiplicity->CreateGraph();
        graph->SetName( Form("Graph_%s", hMBTS->effOnlineAndOfflineMBTSVeto_vs_Multiplicity->GetName() ) );
        graph->Draw("APE");
    
        
        graph->GetXaxis()->SetTitle("Central system multipliticty");
        
        graph->SetMarkerSize(1.4);
        graph->SetMarkerStyle(20);
        graph->SetMarkerColor(kBlack);
        graph->SetLineColor(kBlack);
        
        //             graph->GetXaxis()->SetTitle("Lumiblock");
        graph->GetYaxis()->SetTitle("Offline inner MBTS veto efficiency");
        
        graph->GetXaxis()->SetTitleSize( 1.3*graph->GetXaxis()->GetTitleSize() );
        graph->GetYaxis()->SetTitleSize( 1.3*graph->GetYaxis()->GetTitleSize() );
        graph->GetXaxis()->SetLabelSize( 1.3*graph->GetXaxis()->GetLabelSize() );
        graph->GetYaxis()->SetLabelSize( 1.3*graph->GetYaxis()->GetLabelSize() );
        
        
        graph->GetXaxis()->SetTitleOffset(1.15);
        graph->GetYaxis()->SetTitleOffset(1.15);
        
        graph->GetYaxis()->SetRangeUser(0.97, 1);
//         graph->GetXaxis()->SetLimits(0, 1);
        graph->GetYaxis()->SetNdivisions(4,5,1);
        graph->GetXaxis()->SetNdivisions(5,1,1);
        
        TLatex lATLAS(.18, .2, "#font[72]{ATLAS} Simulation Internal"); lATLAS.SetNDC(); lATLAS.SetTextFont(42);  lATLAS.SetTextSize(0.05); lATLAS.Draw();
        
//         l.DrawLatex(.4, .75, "#font[42]{p+p   #sqrt{s} = 13 TeV   #beta* = 90 m}");
//         l.DrawLatex(.45, .6, "#font[42]{Exactly 1 ID track}");
//         l.DrawLatex(.45, .55, "#font[42]{|d_{0}| < 1.5 mm}");
//         l.DrawLatex(.45, .5, "#font[42]{|z_{0}| < 150 mm}");
//         
        TLegend *leg = new TLegend(0.22, 0.35, 0.55, 0.5);
        leg->SetBorderSize(0);
//         leg->SetFillStyle(0);
        leg->SetFillColorAlpha(kYellow, 0.2);
        leg->AddEntry( graph, "Pythia CD (N_{neutral} = 0)", "pel" );
        leg->Draw();
        
        
        c->Print(dirName+"MBTSVetoEffVsMult.pdf");
    
    }
    delete hMBTS;
    
    
    if(file_signal)
        hMBTS = new CepHistograms_MBTS("MBTS", false, file_signal);
    
    if(hMBTS){
        TCanvas *c;
        TGaxis::SetMaxDigits(3);
        //   TLegend *legend;
        TLatex l; l.SetNDC(); l.SetTextFont(72); l.SetTextSize(.05);
        
        c = new TCanvas("c", "c", 800, 600);
        TGaxis::SetMaxDigits(3);
        
        //         c->SetLeftMargin(0.11);
        c->SetRightMargin(0.1);
        //         c->SetTopMargin(0.02);
        //         c->SetBottomMargin(0.11);
        
        TGraphAsymmErrors *graph = hMBTS->effOnlineAndOfflineMBTSVeto_vs_MaxEta->CreateGraph();
        graph->SetName( Form("Graph_%s", hMBTS->effOnlineAndOfflineMBTSVeto_vs_MaxEta->GetName() ) );
        
        TGraphAsymmErrors *graph2 = hMBTS->effOnlineAndOfflineMBTSVeto_0SignalsAllowed_vs_MaxEta->CreateGraph();
        graph2->SetName( Form("Graph_%s", hMBTS->effOnlineAndOfflineMBTSVeto_0SignalsAllowed_vs_MaxEta->GetName() ) );
        
        graph->SetMarkerSize(1.2);
        graph->SetMarkerStyle(20);
        graph->SetMarkerColor(kBlack);
        graph->SetLineColor(kBlack);
        
        graph2->SetMarkerSize(1.2);
        graph2->SetMarkerStyle(20);
        graph2->SetMarkerColor(kBlue);
        graph2->SetLineColor(kBlue);
        
        TMultiGraph *mg = new TMultiGraph();
    
        mg->Add(graph, "PE");
        mg->Add(graph2, "PE");
        mg->Draw("A");
        
        mg->GetXaxis()->SetTitle("Maximum tracks' #eta");
        
        
        //             mg->GetXaxis()->SetTitle("Lumiblock");
        mg->GetYaxis()->SetTitle("Offline inner MBTS veto efficiency");
        
        mg->GetXaxis()->SetTitleSize( 1.3*mg->GetXaxis()->GetTitleSize() );
        mg->GetYaxis()->SetTitleSize( 1.3*mg->GetYaxis()->GetTitleSize() );
        mg->GetXaxis()->SetLabelSize( 1.3*mg->GetXaxis()->GetLabelSize() );
        mg->GetYaxis()->SetLabelSize( 1.3*mg->GetYaxis()->GetLabelSize() );
        
        
        mg->GetXaxis()->SetTitleOffset(1.15);
        mg->GetYaxis()->SetTitleOffset(1.15);
        
        mg->GetYaxis()->SetRangeUser(0.9, 1);
        mg->GetXaxis()->SetLimits(0, 2.5);
        mg->GetYaxis()->SetNdivisions(5,5,1);
        mg->GetXaxis()->SetNdivisions(5,5,1);
        
        TLatex lATLAS(.18, .2, "#font[72]{ATLAS} Simulation Internal"); lATLAS.SetNDC(); lATLAS.SetTextFont(42);  lATLAS.SetTextSize(0.05); lATLAS.Draw();
        
//         l.DrawLatex(.4, .75, "#font[42]{p+p   #sqrt{s} = 13 TeV   #beta* = 90 m}");
//         l.DrawLatex(.45, .6, "#font[42]{Exactly 1 ID track}");
//         l.DrawLatex(.45, .55, "#font[42]{|d_{0}| < 1.5 mm}");
//         l.DrawLatex(.45, .5, "#font[42]{|z_{0}| < 150 mm}");
//         
        TLegend *leg = new TLegend(0.22+0.35, 0.25+0.4, 0.55+0.35, 0.4+0.4, "CEP #pi^{+}#pi^{-}");
        leg->SetBorderSize(0);
        leg->SetFillStyle(0);
//         leg->SetFillColorAlpha(kYellow, 0.2);
        leg->AddEntry( graph, "veto >1 iMBTS", "pel" );
        leg->AddEntry( graph2, "veto >0 iMBTS", "pel" );
        leg->Draw();
        
        
        c->Print(dirName+"MBTSVetoEffVsMaxEta.pdf");
    
    }
}



void CepAnalysis::closureTests(TFile *file_signal_pipi, TFile *file_signal_kk, TFile *file_signal_ppbar, TFile *file_cd){
    
    const TString dirName("PDF_ClosureTests/");
    mkdir(dirName.Data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    
    
    CepHistograms_ClosureTests *kClosure_pipi = new CepHistograms_ClosureTests("ClosureTests", false, file_signal_pipi);
    CepHistograms_ClosureTests *kClosure_kk = new CepHistograms_ClosureTests("ClosureTests", false, file_signal_kk);
    CepHistograms_ClosureTests *kClosure_ppbar = new CepHistograms_ClosureTests("ClosureTests", false, file_signal_ppbar);
    CepHistograms_ClosureTests *kClosure_cd = new CepHistograms_ClosureTests("ClosureTests", false, file_cd);
    
    CepHistograms_ClosureTests *kClosure[CepUtil::nCepChannels];
    kClosure[CepUtil::PIPI] = kClosure_pipi;
    kClosure[CepUtil::KK] = kClosure_kk;
    kClosure[CepUtil::PPBAR] = kClosure_ppbar;
    kClosure[CepUtil::FOURPI] = kClosure_cd;
    kClosure[CepUtil::SIXPI] = kClosure_cd;
    kClosure[CepUtil::EIGHTPI] = kClosure_cd;
    kClosure[CepUtil::TENPI] = kClosure_cd;
    
    vector<TString> closureTestNameVec;
    vector<TString> closureTestDescriptionVec;
    closureTestNameVec.push_back( "IDTracking" ); closureTestDescriptionVec.push_back( "ID trk recon. & selection" );
    closureTestNameVec.push_back( "IDTrackingAndVertexing" ); closureTestDescriptionVec.push_back( "ID trk recon. & selection + vertexing" );
    
    
    //-----------------
    
    mLegendWidth = 0.3;
    mLegendHeight = 0.36;
    mPrint = true;
    
    for(int channel=0; channel<CepUtil::nCepChannels; ++channel){
            
        if(channel==CepUtil::KK || channel==CepUtil::PPBAR) continue; //skip unfilled plots
        if(channel > CepUtil::SIXPI) continue; //skip unfilled plots
        
        int nRebin = 1;
        switch(channel){
            case CepUtil::FOURPI: nRebin = 4; break;
            case CepUtil::SIXPI: nRebin = 2; break;
            default: nRebin = 1; break;
        }
            
        for(unsigned int closureId=0; closureId<closureTestNameVec.size(); ++closureId){
            
            double maxXaxis = 2.0;
            switch(channel){
                case CepUtil::FOURPI: maxXaxis = 8.0; break;
                case CepUtil::SIXPI: maxXaxis = 12.0; break;
                default: maxXaxis = 2.0; break;
            }
            
            auto hClosureArray = (closureId == 0 ? kClosure[channel]->hInvMass_Tracking : kClosure[channel]->hInvMass_TrackingAndVertexing );
            
            
            for(int j=0; j<CepUtil::nClosureTestValueTypes; ++j){
                mPdfNameSuffix = j==0 ? "(" : (j==(CepUtil::nClosureTestValueTypes-1) ? ")" : "");
                TString xAxisLabel;
                if( j==CepUtil::FILL_TRUE_CORR_USING_RECO || j==CepUtil::FILL_TRUE_CORR_USING_TRUE )
                    xAxisLabel = Form("m_{true}(%s) [GeV]", mCepUtil->mCepChannelLatexName[channel].Data());
                else
                    xAxisLabel = Form("G: m_{true}(%s),  R: m_{reco}(%s) [GeV]", mCepUtil->mCepChannelLatexName[channel].Data(), mCepUtil->mCepChannelLatexName[channel].Data());
                
                TString fillValCorrValStr;
                fillValCorrValStr.Form("#splitline{Corrections calculated}{using %s values}", (j==CepUtil::FILL_TRUE_CORR_USING_TRUE || j==CepUtil::FILL_RECO_CORR_USING_TRUE) ? "true" : "recon.");
                
                
                mPdfName = Form(dirName+"Closure_InvMass%dPi_%s", mCepUtil->mCepChannelMult[channel], closureTestNameVec[closureId].Data());
                TH1F *hClosure[CepUtil::nClosureTestCases];
                for(int i=0; i<CepUtil::nClosureTestCases; ++i){
                    hClosure[i] = hClosureArray[channel][j][i];
                    if( nRebin>1 )
                        hClosure[i]->Rebin(nRebin);
                }

                if( hClosure[CepUtil::GEN]->GetEntries() == 0 )
                    break;
                
                hClosure[CepUtil::GEN]->GetXaxis()->SetRangeUser(hClosure[CepUtil::GEN]->GetXaxis()->GetXmin(), maxXaxis);
                hClosure[CepUtil::GEN]->Scale(hClosure[CepUtil::GEN]->GetBinWidth(3), "width");
                hClosure[CepUtil::RECO_UNCORR]->Scale(hClosure[CepUtil::GEN]->GetBinWidth(3), "width");
                hClosure[CepUtil::RECO_CORR]->Scale(hClosure[CepUtil::GEN]->GetBinWidth(3), "width");
                
                correctClosureTestErrors(hClosure[CepUtil::GEN], hClosure[CepUtil::RECO_CORR], hClosure[CepUtil::RECO_UNCORR]);
                drawComparisonAndRatio(hClosure[CepUtil::GEN], hClosure[CepUtil::RECO_CORR], hClosure[CepUtil::RECO_UNCORR], nullptr,
                                    xAxisLabel, Form("Number of events / %g GeV", hClosure[CepUtil::GEN]->GetBinWidth(3)),
                                    TString("Generated (G)"), TString("#splitline{Reconstructed}{and weighted (R)}"), TString("Reconstructed"), TString(""),
                                    TString("(R-G)/G"), TString(""),
                                    0.66, 0.56, -0.15, 0.15, nullptr, nullptr, nullptr,
                                    closureTestDescriptionVec[closureId], fillValCorrValStr );
            }
        }
    }
    
    
    
    
    
    //BEGIN             single particle              
    
    
    closureTestNameVec.clear();
    closureTestDescriptionVec.clear();
    closureTestNameVec.push_back( "IDTracking" ); closureTestDescriptionVec.push_back( "ID trk recon. & selection" );
    closureTestNameVec.push_back( "IDTrackingIncludingZ0SinTheta" ); closureTestDescriptionVec.push_back( "ID trk recon. & selection (incl. |z_{0}sin#theta| < 1.5 mm)" );
    
    std::vector<TString> quantityName;
    std::vector<TString> quantitySymbol;
    quantitySymbol.push_back( "p_{T}" ); quantityName.push_back( "TrackPt" );
    quantitySymbol.push_back( "#eta" ); quantityName.push_back( "TrackEta" );
    
    //-----------------
    
    mLegendWidth = 0.3;
    mLegendHeight = 0.36;
    mPrint = true;
    
    for(int pid=0; pid<CepUtil::nDefinedParticles; ++pid){
        
        int nRebin = 1;
        switch(pid){
            default: nRebin = 1; break;
        }
        
        for(unsigned int q=0; q<quantityName.size(); ++q){
            
            double minXaxis = 0.0;
            double maxXaxis = 2.0;
            switch(q){
                case 0: minXaxis = 0.0; maxXaxis = 2.0; break;
                case 1: minXaxis = -2.5; maxXaxis = 2.5; break;
                default: break;
            }
            for(unsigned int closureId=0; closureId<closureTestNameVec.size(); ++closureId){
                
                
                 //-------------------------------------------------------------------
                auto hClosureArray = kClosure[pid]->hTrackPt_Tracking;
                if( q==0 ){
                    if( closureId==0 ){ hClosureArray = kClosure[pid]->hTrackPt_Tracking; } else
                    if( closureId==1 ){ hClosureArray = kClosure[pid]->hTrackPt_TrackingAndVertexing; }
                } else
                if( q==1 ){
                    if( closureId==0 ){ hClosureArray = kClosure[pid]->hTrackEta_Tracking; } else
                    if( closureId==1 ){ hClosureArray = kClosure[pid]->hTrackEta_TrackingAndVertexing;  }
                }//-------------------------------------------------------------------
                
                for(int j=0; j<CepUtil::nClosureTestValueTypes; ++j){
                    mPdfNameSuffix = (j==0&&q==0&&closureId==0) ? "(" : (((j==(CepUtil::nClosureTestValueTypes-1))&&(q==(quantityName.size()-1))&&(closureId==(closureTestNameVec.size()-1))) ? ")" : "");
                    TString xAxisLabel;
                    if( j==CepUtil::FILL_TRUE_CORR_USING_RECO || j==CepUtil::FILL_TRUE_CORR_USING_TRUE )
                        xAxisLabel = Form("%s_{true}(%s) [GeV]", quantitySymbol[q].Data(), mCepUtil->mParticleSymbol[pid].Data());
                    else
                        xAxisLabel = Form("G: %s_{true}(%s),  R: %s_{reco}(%s) [GeV]", quantitySymbol[q].Data(), mCepUtil->mParticleSymbol[pid].Data(), quantitySymbol[q].Data(), mCepUtil->mParticleSymbol[pid].Data());
                    
                    TString fillValCorrValStr;
                    fillValCorrValStr.Form("#splitline{Corrections calculated}{using %s values}", (j==CepUtil::FILL_TRUE_CORR_USING_TRUE || j==CepUtil::FILL_RECO_CORR_USING_TRUE) ? "true" : "recon.");
                    
                    
                    mPdfName = Form(dirName+"SingleTrackClosure_%s"/*"Output_Closure/Closure_%s_%s_%s"*/,/* quantityName[q].Data(),*/ mCepUtil->mParticleName[pid].Data()/*, closureTestNameVec[closureId].Data()*/);
                    TH1F *hClosure[CepUtil::nClosureTestCases];
                    for(int i=0; i<CepUtil::nClosureTestCases; ++i){
                        hClosure[i] = hClosureArray[pid][j][i];
                        if( nRebin>1 )
                            hClosure[i]->Rebin(nRebin);
                    }

                    if( hClosure[CepUtil::GEN]->GetEntries() == 0 )
                        break;
                    
                    hClosure[CepUtil::GEN]->GetXaxis()->SetRangeUser(minXaxis, maxXaxis);
                    hClosure[CepUtil::GEN]->Scale(hClosure[CepUtil::GEN]->GetBinWidth(3), "width");
                    hClosure[CepUtil::RECO_UNCORR]->Scale(hClosure[CepUtil::GEN]->GetBinWidth(3), "width");
                    hClosure[CepUtil::RECO_CORR]->Scale(hClosure[CepUtil::GEN]->GetBinWidth(3), "width");
                    
                    
                    correctClosureTestErrors(hClosure[CepUtil::GEN], hClosure[CepUtil::RECO_CORR], hClosure[CepUtil::RECO_UNCORR]);
                    drawComparisonAndRatio(hClosure[CepUtil::GEN], hClosure[CepUtil::RECO_CORR], hClosure[CepUtil::RECO_UNCORR], nullptr,
                                        xAxisLabel, Form("Number of events / %g GeV", hClosure[CepUtil::GEN]->GetBinWidth(3)),
                                        TString("Generated (G)"), TString("#splitline{Reconstructed}{and weighted (R)}"), TString("Reconstructed"), TString(""),
                                        TString("(R-G)/G"), TString(""),
                                        0.66, 0.56, -0.15, 0.15, nullptr, nullptr, nullptr,
                                        closureTestDescriptionVec[closureId], fillValCorrValStr );
                }
            }
            
        }
    
    }
    
}



void CepAnalysis::correctClosureTestErrors(TH1F* gen, TH1F* w, TH1F* uw) const{
    if(gen && w && uw){
        if(gen->GetNbinsX() == w->GetNbinsX() && gen->GetNbinsX() == uw->GetNbinsX()){
            const int nBins = gen->GetNbinsX();
            for(int i = 0; i<nBins; ++i){
                double genErr = gen->GetBinError(i);
                double wErr = w->GetBinError(i);
                double uwErr = uw->GetBinError(i);
                
                if( (wErr*wErr - genErr*genErr) > 0)
                    w->SetBinError(i, sqrt(wErr*wErr - genErr*genErr));
                if( (uwErr*uwErr - genErr*genErr) > 0)
                    uw->SetBinError(i, sqrt(uwErr*uwErr - genErr*genErr));
            }
        }
    }
}






void CepAnalysis::drawInDetEff(){
    
    const TString dirName("PDF_InDetEff/");
    mkdir(dirName.Data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    
    
    std::vector<TFile*> fileVec;
    std::vector<TString> outPdfNameVec;
    
    fileVec.push_back( TFile::Open( "../../InclusiveDiffractionAnalysis/InclusiveDiffractionAnalysis/share/InDetEff_SelfCalculated/pythia_nd_ID_Eff.root" ) ); outPdfNameVec.push_back( "InDetTrackRecoEff_EtaVsPt_ND" );
    fileVec.push_back( TFile::Open( "../../InclusiveDiffractionAnalysis/InclusiveDiffractionAnalysis/share/InDetEff_SelfCalculated/InDetEffCEP.root" ) );  outPdfNameVec.push_back( "InDetTrackRecoEff_EtaVsPt_CD" );
    
    std::vector<TH2*> effTH2FVec_WithZ0SinThetaCut[CepUtil::nDefinedParticles][CepUtil::nSigns];
    std::vector<TH2*> effTH2FVec_NoZ0SinThetaCut[CepUtil::nDefinedParticles][CepUtil::nSigns];
    std::vector<TEfficiency*> effTEfficiencyVec_WithZ0SinThetaCut[CepUtil::nDefinedParticles][CepUtil::nSigns];
    std::vector<TEfficiency*> effTEfficiencyVec_NoZ0SinThetaCut[CepUtil::nDefinedParticles][CepUtil::nSigns];
    
    TCanvas *c = new TCanvas("c", "c", 900, 800);
    
    TGaxis::SetMaxDigits(3);
    gPad->SetLeftMargin(0.09);
    gPad->SetRightMargin(0.17);
    gPad->SetTopMargin(0.05);
    gPad->SetBottomMargin(0.15);
    
    gStyle->SetPalette(62);
    TColor::InvertPalette();
    gStyle->SetNumberContours(99);
    
    
    for(unsigned int fileId=0; fileId<fileVec.size(); ++fileId){
        
        TFile *file = fileVec[fileId];
        
        for(int i=0; i<CepUtil::nDefinedParticles; ++i){
            for(int j=0; j<CepUtil::nSigns; ++j){
                TString name;
                TEfficiency *eff;
                
                name.Form(fileId == 0 ? "%s_mass%d_WithZ0SinThetaCut" : "MCEfficiencies/%s_mass%d_WithZ0SinThetaCut_WithPtRecoEtaRecoCuts", j==CepUtil::PLUS ? "Pos" : "Neg", i);
                eff = dynamic_cast<TEfficiency*>( file->Get( name ) );
                if( eff ) eff->SetDirectory(0);
                effTEfficiencyVec_WithZ0SinThetaCut[i][j].push_back( eff );
                
                name.Form(fileId == 0 ? "%s_mass%d_NoZ0SinThetaCut" : "MCEfficiencies/%s_mass%d_NoZ0SinThetaCut_WithPtRecoEtaRecoCuts", j==CepUtil::PLUS ? "Pos" : "Neg", i);
                eff = dynamic_cast<TEfficiency*>( file->Get( name ) );
                if( eff ) eff->SetDirectory(0);
                effTEfficiencyVec_NoZ0SinThetaCut[i][j].push_back( eff );
                
            }
        }
        

        
        TF1 *fEtaVsPt[2];
        fEtaVsPt[0] = new TF1("fEtaVsPt_0", etaVsPt, 0, 1, 2);
        fEtaVsPt[1] = new TF1("fEtaVsPt_1", etaVsPt, 0, -1, 2);
                
                
        c->Print(dirName+outPdfNameVec[fileId]+".pdf["/*, "EmbedFonts"*/);
        for(int i=0; i<CepUtil::nDefinedParticles; ++i){
            for(int j=0; j<CepUtil::nSigns; ++j){
        
                TH2 * hInDetTrackRecoEff_EtaVsPt = effTEfficiencyVec_NoZ0SinThetaCut[i][j][fileId]->CreateHistogram()
;
                hInDetTrackRecoEff_EtaVsPt->SetDirectory(0);
                effTH2FVec_NoZ0SinThetaCut[i][j].push_back( hInDetTrackRecoEff_EtaVsPt );
                
                hInDetTrackRecoEff_EtaVsPt->GetXaxis()->SetLabelOffset(1.5*hInDetTrackRecoEff_EtaVsPt->GetXaxis()->GetLabelOffset());
                hInDetTrackRecoEff_EtaVsPt->GetXaxis()->SetTitle("p_{T,true} [GeV]");
                hInDetTrackRecoEff_EtaVsPt->GetXaxis()->SetRangeUser(0.1, 1);
                hInDetTrackRecoEff_EtaVsPt->GetXaxis()->SetNdivisions(5,5,1);
                hInDetTrackRecoEff_EtaVsPt->GetYaxis()->SetTitleOffset(1.2);
                
                hInDetTrackRecoEff_EtaVsPt->GetYaxis()->SetLabelOffset(1.5*hInDetTrackRecoEff_EtaVsPt->GetXaxis()->GetLabelOffset());
                hInDetTrackRecoEff_EtaVsPt->GetYaxis()->SetTitle("#eta_{true}");
                hInDetTrackRecoEff_EtaVsPt->GetYaxis()->SetTitleOffset(0.8);
                hInDetTrackRecoEff_EtaVsPt->GetYaxis()->SetNdivisions(5,5,1);
                
                hInDetTrackRecoEff_EtaVsPt->GetZaxis()->SetLabelOffset(1.5*hInDetTrackRecoEff_EtaVsPt->GetXaxis()->GetLabelOffset());
                hInDetTrackRecoEff_EtaVsPt->GetZaxis()->SetTitle("Track reconstruction efficiency");
                hInDetTrackRecoEff_EtaVsPt->GetZaxis()->SetTitleOffset(1.2);
                hInDetTrackRecoEff_EtaVsPt->GetZaxis()->SetRangeUser(0, 1);
                
                hInDetTrackRecoEff_EtaVsPt->Draw("colz");
                
                gPad->Update();
                TPaletteAxis *palette = (TPaletteAxis*)hInDetTrackRecoEff_EtaVsPt->GetListOfFunctions()->FindObject("palette");
                if(palette){
                    palette->SetX1NDC(0.835);
                    palette->SetX2NDC(0.865);
                }
                gPad->Modified();
                
                
                
                //lines and arrows
                TLine etaCutLine[2];
                TLine ptCutLine( mParams->minPt[i], -mParams->maxEta[i], mParams->minPt[i], mParams->maxEta[i] );
                TLine ptCutLines[2];
                etaCutLine[0] = TLine( mParams->minPt[i], -mParams->maxEta[i], 1.0, -mParams->maxEta[i] );
                etaCutLine[1] = TLine( mParams->minPt[i],  mParams->maxEta[i], 1.0, mParams->maxEta[i] );
                ptCutLines[0] = TLine( mParams->minPt[i], -mParams->maxEta[i], mParams->minPt[i], -acosh(mParams->minP[i]/mParams->minPt[i]) );
                ptCutLines[1] = TLine( mParams->minPt[i], acosh(mParams->minP[i]/mParams->minPt[i]), mParams->minPt[i], mParams->maxEta[i] );
                
                TArrow etaCutArrow[2];
                TArrow ptCutArrow( mParams->minP[i], 0., mParams->minP[i]+0.1, 0.0, 0.03, "|>");
                etaCutArrow[0] = TArrow( mParams->minPt[i]+(1.0-mParams->minPt[i])/2, -mParams->maxEta[i], mParams->minPt[i]+(1.0-mParams->minPt[i])/2, -mParams->maxEta[i] + 0.5, 0.03, "|>");
                etaCutArrow[1] = TArrow( mParams->minPt[i]+(1.0-mParams->minPt[i])/2,  mParams->maxEta[i], mParams->minPt[i]+(1.0-mParams->minPt[i])/2, mParams->maxEta[i] - 0.5, 0.03, "|>");
                
                ptCutLine.SetLineWidth(8);
                ptCutLine.SetLineColor(kGreen+2);
                ptCutLine.SetLineStyle(9);
                
                for(int fff=0; fff<2; ++fff){
                    fEtaVsPt[fff]->SetLineWidth(8);
                    fEtaVsPt[fff]->SetLineColor(kGreen+2);
                    fEtaVsPt[fff]->SetLineStyle(9);
                    fEtaVsPt[fff]->SetRange( mParams->minPt[i], mParams->minP[i]);
                    fEtaVsPt[fff]->SetParameters(fff==0 ? 1 : -1, mParams->minP[i]);
                    
                    ptCutLines[fff].SetLineWidth(8);
                    ptCutLines[fff].SetLineColor(kGreen+2);
                    ptCutLines[fff].SetLineStyle(9);
                    
                }
                
                ptCutArrow.SetAngle(30);
                ptCutArrow.SetLineWidth(8);
                ptCutArrow.SetLineColor( kGreen+2 );
                ptCutArrow.SetFillColor( kGreen+2 );
                
                for(int lId=0; lId<2; ++lId){
                    etaCutLine[lId].SetLineWidth(8);
                    etaCutLine[lId].SetLineColor(kGreen+2);
                    etaCutLine[lId].SetLineStyle(9);
                    
                    etaCutArrow[lId].SetAngle(30);
                    etaCutArrow[lId].SetLineWidth(8);
                    etaCutArrow[lId].SetLineColor( kGreen+2 );
                    etaCutArrow[lId].SetFillColor( kGreen+2 );
                }
                
                //                 for(int lId=0; lId<2; ++lId){
                //                     etaCutLine[lId].Draw();
                //                     etaCutArrow[lId].Draw();
                //                 }
                    
                if(i>CepUtil::PION){
                    for(int fff=0; fff<2; ++fff){
                        fEtaVsPt[fff]->Draw("SAME");
                        ptCutLines[fff].Draw();
                    }
                    
//                     ptCutLine.Draw();
                    ptCutArrow.Draw();
                }
                
                TLatex l; l.SetNDC(); l.SetTextFont(72); l.SetTextSize(.05); l.SetTextColor(kWhite);
                l.DrawLatex(.3, .85, "ATLAS #font[42]{Simulation Internal}");
                l.DrawLatex(.45, .78, "#font[42]{#sqrt{s} = 13 TeV}");
                l.DrawLatex(.45, .71, mCepUtil->mParticleSymbolCh[i][j]);
                
                c->Print(dirName+outPdfNameVec[fileId]+".pdf"/*, "EmbedFonts"*/);   
            }
        }
        c->Print(dirName+outPdfNameVec[fileId]+".pdf]"/*, "EmbedFonts"*/);
        
    }
    
    
    setBlueWhiteRedColorScale();
    
    c->Print(dirName+"EffRatio_CD_over_ND.pdf["/*, "EmbedFonts"*/);
    for(int i=0; i<CepUtil::nDefinedParticles; ++i){
        for(int j=0; j<CepUtil::nSigns; ++j){
            
            TH2 *hRatio = new TH2F( *dynamic_cast<TH2F*>(effTH2FVec_NoZ0SinThetaCut[i][j][1]) );
            hRatio->Divide( effTH2FVec_NoZ0SinThetaCut[i][j][0] );
            
            hRatio->GetXaxis()->SetLabelOffset(1.5*hRatio->GetXaxis()->GetLabelOffset());
            hRatio->GetXaxis()->SetTitle("p_{T,true} [GeV]");
            hRatio->GetXaxis()->SetRangeUser(0.1, 1);
            hRatio->GetXaxis()->SetNdivisions(5,5,1);
            hRatio->GetYaxis()->SetTitleOffset(1.2);
            
            hRatio->GetYaxis()->SetLabelOffset(1.5*hRatio->GetXaxis()->GetLabelOffset());
            hRatio->GetYaxis()->SetTitle("#eta_{true}");
            hRatio->GetYaxis()->SetTitleOffset(0.8);
            hRatio->GetYaxis()->SetNdivisions(5,5,1);
            
            hRatio->GetZaxis()->SetLabelOffset(1.5*hRatio->GetXaxis()->GetLabelOffset());
            hRatio->GetZaxis()->SetTitle("Efficiency ratio: CD/ND");
            hRatio->GetZaxis()->SetTitleOffset(1.2);
            hRatio->GetZaxis()->SetRangeUser(0.95, 1.05);
            
            hRatio->Draw("colz");
            
            gPad->Update();
            TPaletteAxis *palette = (TPaletteAxis*)hRatio->GetListOfFunctions()->FindObject("palette");
            if(palette){
                palette->SetX1NDC(0.835);
                palette->SetX2NDC(0.865);
            }
            gPad->Modified();
            
            TLatex l; l.SetNDC(); l.SetTextFont(72); l.SetTextSize(.05); l.SetTextColor(kWhite);
            l.DrawLatex(.3, .85, "ATLAS #font[42]{Simulation Internal}");
            l.DrawLatex(.45, .78, "#font[42]{#sqrt{s} = 13 TeV}");
            l.DrawLatex(.45, .71, mCepUtil->mParticleSymbolCh[i][j]);
            
            c->Print(dirName+"EffRatio_CD_over_ND.pdf"/*, "EmbedFonts"*/);   
        }
    }
    c->Print(dirName+"EffRatio_CD_over_ND.pdf]"/*, "EmbedFonts"*/);
    
    
    
    
    
    //---- drawing eff with systematics for the note
    
    fileVec.clear(); outPdfNameVec.clear();
    fileVec.clear();  outPdfNameVec.clear();
    
    for(int i=0; i<CepUtil::nDefinedParticles; ++i){
        for(int j=0; j<CepUtil::nSigns; ++j){
            effTH2FVec_WithZ0SinThetaCut[i][j].clear();
            effTH2FVec_NoZ0SinThetaCut[i][j].clear();
            effTEfficiencyVec_WithZ0SinThetaCut[i][j].clear();
            effTEfficiencyVec_NoZ0SinThetaCut[i][j].clear();
        }
    }
    
    enum EFF_SYST { NOMINAL, SYST_1, SYST_2, SYST_3, nEffs };
    enum EFF_PASSED_TOTAL { PASSED, TOTAL, nPassedTotal };
    
    fileVec.push_back( TFile::Open( "../../InclusiveDiffractionAnalysis/InclusiveDiffractionAnalysis/share/InDetEff_SelfCalculated/pythia_nd_ID_Eff.root" ) ); outPdfNameVec.push_back( "InDetTrackRecoEff_EtaVsPt_ND" );
    fileVec.push_back( TFile::Open( "../../InclusiveDiffractionAnalysis/InclusiveDiffractionAnalysis/share/InDetEff_SelfCalculated/pythia_nd_ID_Eff_sys1.root" ) );  outPdfNameVec.push_back( "InDetTrackRecoEff_EtaVsPt_ND_sys1" );
    fileVec.push_back( TFile::Open( "../../InclusiveDiffractionAnalysis/InclusiveDiffractionAnalysis/share/InDetEff_SelfCalculated/pythia_nd_ID_Eff_sys2.root" ) );  outPdfNameVec.push_back( "InDetTrackRecoEff_EtaVsPt_ND_sys2" );
    fileVec.push_back( TFile::Open( "../../InclusiveDiffractionAnalysis/InclusiveDiffractionAnalysis/share/InDetEff_SelfCalculated/pythia_nd_ID_Eff_sys3.root" ) );  outPdfNameVec.push_back( "InDetTrackRecoEff_EtaVsPt_ND_sys3" );
    
    assert( nEffs == static_cast<int>( fileVec.size() ) );
    
    
    
    TH2* effTH2_NoZ0SinThetaCut[CepUtil::nDefinedParticles][CepUtil::nSigns][nEffs][nPassedTotal];
    TH2* effTH2_WithZ0SinThetaCut[CepUtil::nDefinedParticles][CepUtil::nSigns][nEffs][nPassedTotal];
    
    TH2F* effTH2F_NoZ0SinThetaCut_SystErr[CepUtil::nDefinedParticles][CepUtil::nSigns];
    TH2F* effTH2F_WithZ0SinThetaCut_SystErr[CepUtil::nDefinedParticles][CepUtil::nSigns];
    
    
    for(unsigned int fileId=0; fileId<fileVec.size(); ++fileId){
        
        TFile *file = fileVec[fileId];
        
        for(int i=0; i<CepUtil::nDefinedParticles; ++i){
            for(int j=0; j<CepUtil::nSigns; ++j){
                TString name;
                TEfficiency *eff;
                
                name.Form( "%s_mass%d_WithZ0SinThetaCut", j==CepUtil::PLUS ? "Pos" : "Neg", i);
                eff = dynamic_cast<TEfficiency*>( file->Get( name ) );
                if( eff ) eff->SetDirectory(0);
                effTEfficiencyVec_WithZ0SinThetaCut[i][j].push_back( eff );
                
                TH2* copy_passed = dynamic_cast<TH2*>( eff->GetCopyPassedHisto() );
                copy_passed->SetDirectory(0);
                TH2* copy_total = dynamic_cast<TH2*>( eff->GetCopyTotalHisto() );
                copy_total->SetDirectory(0);
                
                effTH2_WithZ0SinThetaCut[i][j][fileId][PASSED] = copy_passed;
                effTH2_WithZ0SinThetaCut[i][j][fileId][TOTAL] = copy_total;
                
                effTH2F_WithZ0SinThetaCut_SystErr[i][j] = new TH2F();
                effTH2_WithZ0SinThetaCut[i][j][fileId][PASSED]->Copy( *effTH2F_WithZ0SinThetaCut_SystErr[i][j] );
                effTH2F_WithZ0SinThetaCut_SystErr[i][j]->Reset("ICESM");
                effTH2F_WithZ0SinThetaCut_SystErr[i][j]->SetName(name+"_SystError");
                effTH2F_WithZ0SinThetaCut_SystErr[i][j]->SetTitle(name+"_SystError");
                
                //----
                
                name.Form( "%s_mass%d_NoZ0SinThetaCut", j==CepUtil::PLUS ? "Pos" : "Neg", i);
                eff = dynamic_cast<TEfficiency*>( file->Get( name ) );
                if( eff ) eff->SetDirectory(0);
                effTEfficiencyVec_NoZ0SinThetaCut[i][j].push_back( eff );
                
                copy_passed = dynamic_cast<TH2*>( eff->GetCopyPassedHisto() );
                copy_passed->SetDirectory(0);
                copy_total = dynamic_cast<TH2*>( eff->GetCopyTotalHisto() );
                copy_total->SetDirectory(0);
                
                effTH2_NoZ0SinThetaCut[i][j][fileId][PASSED] = copy_passed;
                effTH2_NoZ0SinThetaCut[i][j][fileId][TOTAL] = copy_total;
                
                effTH2F_NoZ0SinThetaCut_SystErr[i][j] = new TH2F();
                effTH2_NoZ0SinThetaCut[i][j][fileId][PASSED]->Copy( *effTH2F_NoZ0SinThetaCut_SystErr[i][j] );
                effTH2F_NoZ0SinThetaCut_SystErr[i][j]->Reset("ICESM");
                effTH2F_NoZ0SinThetaCut_SystErr[i][j]->SetName(name+"_SystError");
                effTH2F_NoZ0SinThetaCut_SystErr[i][j]->SetTitle(name+"_SystError");
            }
        }
    }

    
    
    c = new TCanvas("c", "c", 800, 600);
    
    TGaxis::SetMaxDigits(3);
    gPad->SetLeftMargin(0.13);
    gPad->SetRightMargin(0.05);
    gPad->SetTopMargin(0.05);
    gPad->SetBottomMargin(0.14);
    
    
    c->Print(dirName+"InDetEff_SelfCalculated.pdf[");
    c->Print(dirName+"InDetEff_SelfCalculated_pT_binsOf_eta.pdf[");
    c->Print(dirName+"InDetEff_SelfCalculated_eta_binsOf_pT.pdf[");
    for(int i=0; i<CepUtil::nDefinedParticles; ++i){
        for(int j=0; j<CepUtil::nSigns; ++j){
            
//             TEfficiency *eff1D_TEfficiency[nEffs][CepUtil::nCoordinates];
            TH1D *eff1D_TH1_NoZ0SinThetaCut[nEffs][CepUtil::nCoordinates];
            vector<TH1D*> eff1D_TH1_NoZ0SinThetaCut_Slices[nEffs][CepUtil::nCoordinates];
            TH1D *eff1D_TH1_WithZ0SinThetaCut[nEffs][CepUtil::nCoordinates];
            vector<TH1D*> eff1D_TH1_WithZ0SinThetaCut_Slices[nEffs][CepUtil::nCoordinates];
            
            for(int coor=0; coor<CepUtil::nCoordinates; ++coor){
                for(int k=0; k<nEffs; ++k){
                    TH1D * nTracks[nPassedTotal];
                    
                    nTracks[PASSED] = (coor==CepUtil::X ? effTH2_NoZ0SinThetaCut[i][j][k][PASSED]->ProjectionX() : effTH2_NoZ0SinThetaCut[i][j][k][PASSED]->ProjectionY());
                    nTracks[PASSED]->SetDirectory(0);
                    nTracks[TOTAL] = (coor==CepUtil::X ? effTH2_NoZ0SinThetaCut[i][j][k][TOTAL]->ProjectionX() : effTH2_NoZ0SinThetaCut[i][j][k][TOTAL]->ProjectionY());
                    nTracks[TOTAL]->SetDirectory(0);
                    
                    eff1D_TH1_NoZ0SinThetaCut[k][coor] = new TH1D( *(nTracks[PASSED]) );
                    eff1D_TH1_NoZ0SinThetaCut[k][coor]->SetName( Form("%d%d%d%d_eff1D_TEfficiency_NoZ0SinThetaCut", i, j, k, coor) );
                    eff1D_TH1_NoZ0SinThetaCut[k][coor]->Divide( nTracks[TOTAL] );
                    
                    for(int bin=1; bin<=(coor==CepUtil::X ? effTH2_NoZ0SinThetaCut[i][j][k][PASSED]->GetNbinsY() : effTH2_NoZ0SinThetaCut[i][j][k][PASSED]->GetNbinsX() ); ++bin ){
                        nTracks[PASSED] = (coor==CepUtil::X ? effTH2_NoZ0SinThetaCut[i][j][k][PASSED]->ProjectionX("px", bin, bin) : effTH2_NoZ0SinThetaCut[i][j][k][PASSED]->ProjectionY("py", bin, bin));
                        nTracks[PASSED]->SetDirectory(0);
                        nTracks[TOTAL] = (coor==CepUtil::X ? effTH2_NoZ0SinThetaCut[i][j][k][TOTAL]->ProjectionX("px2", bin, bin) : effTH2_NoZ0SinThetaCut[i][j][k][TOTAL]->ProjectionY("py2", bin, bin));
                        nTracks[TOTAL]->SetDirectory(0);
                        
                        TH1D *hist = new TH1D( *(nTracks[PASSED]) );
                        hist->SetName( Form("%d%d%d%d_eff1D_TEfficiency_NoZ0SinThetaCut_Slice%d", i, j, k, coor, bin) );
                        hist->Divide( nTracks[TOTAL] );
                        
                        eff1D_TH1_NoZ0SinThetaCut_Slices[k][coor].push_back( hist );
                    }
                
//                     eff1D_TEfficiency[k][coor] = new TEfficiency(*(nTracks[PASSED]), *(nTracks[TOTAL]) );
                    // ---------------------- ---------------------- ---------------------- ---------------------- ----------------------
                    
                    nTracks[PASSED] = (coor==CepUtil::X ? effTH2_WithZ0SinThetaCut[i][j][k][PASSED]->ProjectionX() : effTH2_WithZ0SinThetaCut[i][j][k][PASSED]->ProjectionY());
                    nTracks[PASSED]->SetDirectory(0);
                    nTracks[TOTAL] = (coor==CepUtil::X ? effTH2_WithZ0SinThetaCut[i][j][k][TOTAL]->ProjectionX() : effTH2_WithZ0SinThetaCut[i][j][k][TOTAL]->ProjectionY());
                    nTracks[TOTAL]->SetDirectory(0);
                    
                    eff1D_TH1_WithZ0SinThetaCut[k][coor] = new TH1D( *(nTracks[PASSED]) );
                    eff1D_TH1_WithZ0SinThetaCut[k][coor]->SetName( Form("%d%d%d%d_eff1D_TEfficiency_WithZ0SinThetaCut", i, j, k, coor) );
                    eff1D_TH1_WithZ0SinThetaCut[k][coor]->Divide( nTracks[TOTAL] );
                    
                    for(int bin=1; bin<=(coor==CepUtil::X ? effTH2_WithZ0SinThetaCut[i][j][k][PASSED]->GetNbinsY() : effTH2_WithZ0SinThetaCut[i][j][k][PASSED]->GetNbinsX() ); ++bin ){
                        nTracks[PASSED] = (coor==CepUtil::X ? effTH2_WithZ0SinThetaCut[i][j][k][PASSED]->ProjectionX("px", bin, bin) : effTH2_WithZ0SinThetaCut[i][j][k][PASSED]->ProjectionY("py", bin, bin));
                        nTracks[PASSED]->SetDirectory(0);
                        nTracks[TOTAL] = (coor==CepUtil::X ? effTH2_WithZ0SinThetaCut[i][j][k][TOTAL]->ProjectionX("px2", bin, bin) : effTH2_WithZ0SinThetaCut[i][j][k][TOTAL]->ProjectionY("py2", bin, bin));
                        nTracks[TOTAL]->SetDirectory(0);
                        
                        TH1D *hist = new TH1D( *(nTracks[PASSED]) );
                        hist->SetName( Form("%d%d%d%d_eff1D_TEfficiency_WithZ0SinThetaCut_Slice%d", i, j, k, coor, bin) );
                        hist->Divide( nTracks[TOTAL] );
                        
                        eff1D_TH1_WithZ0SinThetaCut_Slices[k][coor].push_back( hist );
                    }
                }
                
                for(int bin=1; bin<=eff1D_TH1_NoZ0SinThetaCut[NOMINAL][coor]->GetNbinsX(); ++bin){
                    double err1 = fabs( eff1D_TH1_NoZ0SinThetaCut[NOMINAL][coor]->GetBinContent(bin) - eff1D_TH1_NoZ0SinThetaCut[SYST_1][coor]->GetBinContent(bin) );
                    double err2 = fabs( eff1D_TH1_NoZ0SinThetaCut[NOMINAL][coor]->GetBinContent(bin) - eff1D_TH1_NoZ0SinThetaCut[SYST_2][coor]->GetBinContent(bin) );
                    double err3 = fabs( eff1D_TH1_NoZ0SinThetaCut[NOMINAL][coor]->GetBinContent(bin) - eff1D_TH1_NoZ0SinThetaCut[SYST_3][coor]->GetBinContent(bin) );
                    double totalErr = err1+err2+err3;
                    
                    eff1D_TH1_NoZ0SinThetaCut[NOMINAL][coor]->SetBinError(bin, totalErr);
                    
                    err1 = fabs( eff1D_TH1_WithZ0SinThetaCut[NOMINAL][coor]->GetBinContent(bin) - eff1D_TH1_WithZ0SinThetaCut[SYST_1][coor]->GetBinContent(bin) );
                    err2 = fabs( eff1D_TH1_WithZ0SinThetaCut[NOMINAL][coor]->GetBinContent(bin) - eff1D_TH1_WithZ0SinThetaCut[SYST_2][coor]->GetBinContent(bin) );
                    err3 = fabs( eff1D_TH1_WithZ0SinThetaCut[NOMINAL][coor]->GetBinContent(bin) - eff1D_TH1_WithZ0SinThetaCut[SYST_3][coor]->GetBinContent(bin) );
                    totalErr = err1+err2+err3;
                    
                    eff1D_TH1_WithZ0SinThetaCut[NOMINAL][coor]->SetBinError(bin, totalErr);
                    
                    assert( eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor].size() == eff1D_TH1_WithZ0SinThetaCut_Slices[NOMINAL][coor].size() );
                    
                    for(unsigned int slice=0; slice<eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor].size(); ++slice){
                        err1 = fabs( eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor][slice]->GetBinContent(bin) - eff1D_TH1_NoZ0SinThetaCut_Slices[SYST_1][coor][slice]->GetBinContent(bin) );
                        err2 = fabs( eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor][slice]->GetBinContent(bin) - eff1D_TH1_NoZ0SinThetaCut_Slices[SYST_2][coor][slice]->GetBinContent(bin) );
                        err3 = fabs( eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor][slice]->GetBinContent(bin) - eff1D_TH1_NoZ0SinThetaCut_Slices[SYST_3][coor][slice]->GetBinContent(bin) );
                        totalErr = err1+err2+err3;
                        
                        eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor][slice]->SetBinError(bin, totalErr);
                        
                        if(coor == CepUtil::X){
                            effTH2F_NoZ0SinThetaCut_SystErr[i][j]->SetBinContent(bin, slice+1, totalErr);
                            effTH2F_NoZ0SinThetaCut_SystErr[i][j]->SetBinError(bin, slice+1, 0);
                        }
                    
                        err1 = fabs( eff1D_TH1_WithZ0SinThetaCut_Slices[NOMINAL][coor][slice]->GetBinContent(bin) - eff1D_TH1_WithZ0SinThetaCut_Slices[SYST_1][coor][slice]->GetBinContent(bin) );
                        err2 = fabs( eff1D_TH1_WithZ0SinThetaCut_Slices[NOMINAL][coor][slice]->GetBinContent(bin) - eff1D_TH1_WithZ0SinThetaCut_Slices[SYST_2][coor][slice]->GetBinContent(bin) );
                        err3 = fabs( eff1D_TH1_WithZ0SinThetaCut_Slices[NOMINAL][coor][slice]->GetBinContent(bin) - eff1D_TH1_WithZ0SinThetaCut_Slices[SYST_3][coor][slice]->GetBinContent(bin) );
                        totalErr = err1+err2+err3;
                        
                        eff1D_TH1_WithZ0SinThetaCut_Slices[NOMINAL][coor][slice]->SetBinError(bin, totalErr);
                        
                        if(coor == CepUtil::X){
                            effTH2F_WithZ0SinThetaCut_SystErr[i][j]->SetBinContent(bin, slice+1, totalErr);
                            effTH2F_WithZ0SinThetaCut_SystErr[i][j]->SetBinError(bin, slice+1, 0);
                        }
                    }
                }
                
                
                eff1D_TH1_NoZ0SinThetaCut[NOMINAL][coor]->GetXaxis()->SetLabelOffset(1.5*eff1D_TH1_NoZ0SinThetaCut[NOMINAL][coor]->GetXaxis()->GetLabelOffset());
                eff1D_TH1_NoZ0SinThetaCut[NOMINAL][coor]->GetXaxis()->SetTitle(coor==CepUtil::X ? "p_{T,true} [GeV]" : "#eta_{true}");
                if(coor==CepUtil::X)
                    eff1D_TH1_NoZ0SinThetaCut[NOMINAL][coor]->GetXaxis()->SetRangeUser(0.1, 1);
                eff1D_TH1_NoZ0SinThetaCut[NOMINAL][coor]->GetXaxis()->SetNdivisions(5,5,1);
                eff1D_TH1_NoZ0SinThetaCut[NOMINAL][coor]->GetXaxis()->SetTitleOffset(1.2);
                
                eff1D_TH1_NoZ0SinThetaCut[NOMINAL][coor]->GetYaxis()->SetLabelOffset(1.5*eff1D_TH1_NoZ0SinThetaCut[NOMINAL][coor]->GetXaxis()->GetLabelOffset());
                eff1D_TH1_NoZ0SinThetaCut[NOMINAL][coor]->GetYaxis()->SetTitle("Track reconstruction efficiency");
                eff1D_TH1_NoZ0SinThetaCut[NOMINAL][coor]->GetYaxis()->SetTitleOffset(1.0);
                eff1D_TH1_NoZ0SinThetaCut[NOMINAL][coor]->GetYaxis()->SetNdivisions(5,5,1);
                eff1D_TH1_NoZ0SinThetaCut[NOMINAL][coor]->GetYaxis()->SetRangeUser(0, 1);
                
                eff1D_TH1_NoZ0SinThetaCut[NOMINAL][coor]->GetXaxis()->SetTitleSize( eff1D_TH1_NoZ0SinThetaCut[NOMINAL][coor]->GetYaxis()->GetTitleSize() );
                eff1D_TH1_NoZ0SinThetaCut[NOMINAL][coor]->GetXaxis()->SetLabelSize( eff1D_TH1_NoZ0SinThetaCut[NOMINAL][coor]->GetYaxis()->GetLabelSize() );
                
                eff1D_TH1_NoZ0SinThetaCut[NOMINAL][coor]->SetMarkerStyle(20);
                eff1D_TH1_NoZ0SinThetaCut[NOMINAL][coor]->SetMarkerColor(kBlack);
                eff1D_TH1_NoZ0SinThetaCut[NOMINAL][coor]->SetMarkerSize(1.2);
                eff1D_TH1_NoZ0SinThetaCut[NOMINAL][coor]->SetLineColor(kBlack);
                eff1D_TH1_NoZ0SinThetaCut[NOMINAL][coor]->SetLineWidth(2);
                eff1D_TH1_NoZ0SinThetaCut[NOMINAL][coor]->SetFillColorAlpha(kAzure+2, 0.5);
                
                eff1D_TH1_WithZ0SinThetaCut[NOMINAL][coor]->SetMarkerStyle(25);
                eff1D_TH1_WithZ0SinThetaCut[NOMINAL][coor]->SetMarkerColor(kRed);
                eff1D_TH1_WithZ0SinThetaCut[NOMINAL][coor]->SetMarkerSize(1.2);
                eff1D_TH1_WithZ0SinThetaCut[NOMINAL][coor]->SetLineColor(kRed);
                eff1D_TH1_WithZ0SinThetaCut[NOMINAL][coor]->SetLineWidth(2);
                eff1D_TH1_WithZ0SinThetaCut[NOMINAL][coor]->SetFillStyle(0);
                
                eff1D_TH1_NoZ0SinThetaCut[NOMINAL][coor]->Draw("PE2");
                eff1D_TH1_WithZ0SinThetaCut[NOMINAL][coor]->Draw("PE2 SAME");
                
                
                TLatex l; l.SetNDC(); l.SetTextFont(72); l.SetTextSize(.05); l.SetTextColor(kBlack);
                l.DrawLatex(.3+0.15+0.06, .78-0.57, "ATLAS #font[42]{Simulation Internal}");
                l.DrawLatex(.45+0.2+0.06, .85-0.57, "#font[42]{#sqrt{s} = 13 TeV}");
                l.SetTextSize(.07);
                l.DrawLatex(.45+0.13+0.06, .85-0.57, mCepUtil->mParticleSymbolCh[i][j]);
                
                if(i==0 && j==0){
                    TLegend *leg = new TLegend(0.39, 0.43, 0.52, 0.57, "Pythia 8 (ND)");
                    leg->SetTextSize(0.05);
                    leg->SetTextFont(42);
                    leg->SetBorderSize(0);
                    leg->SetFillStyle(0);
    //                 leg->SetFillColorAlpha(kYellow, 0.2);
                    leg->AddEntry( eff1D_TH1_NoZ0SinThetaCut[NOMINAL][coor], "w/o |z_{0}sin#theta| < 1.5 mm", "pef" );
                    leg->AddEntry( eff1D_TH1_WithZ0SinThetaCut[NOMINAL][coor], "w/   |z_{0}sin#theta| < 1.5 mm", "pef" );
                    leg->Draw();
                }
                
                c->Print(dirName+"InDetEff_SelfCalculated.pdf");
                
                
                
                for(unsigned int slice=0; slice<eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor].size(); ++slice){
                    
                    if( slice==0 && coor==CepUtil::Y ) continue;
                    
                    eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor][slice]->GetXaxis()->SetLabelOffset(1.5*eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor][slice]->GetXaxis()->GetLabelOffset());
                    eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor][slice]->GetXaxis()->SetTitle(coor==CepUtil::X ? "p_{T,true} [GeV]" : "#eta_{true}");
                    if(coor==CepUtil::X)
                        eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor][slice]->GetXaxis()->SetRangeUser(0.1, 1);
                    eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor][slice]->GetXaxis()->SetNdivisions(5,5,1);
                    eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor][slice]->GetXaxis()->SetTitleOffset(1.2);
                    
                    eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor][slice]->GetYaxis()->SetLabelOffset(1.5*eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor][slice]->GetXaxis()->GetLabelOffset());
                    eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor][slice]->GetYaxis()->SetTitle("Track reconstruction efficiency");
                    eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor][slice]->GetYaxis()->SetTitleOffset(1.0);
                    eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor][slice]->GetYaxis()->SetNdivisions(5,5,1);
                    eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor][slice]->GetYaxis()->SetRangeUser(0, 1);
                    
                    eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor][slice]->GetXaxis()->SetTitleSize( eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor][slice]->GetYaxis()->GetTitleSize() );
                    eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor][slice]->GetXaxis()->SetLabelSize( eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor][slice]->GetYaxis()->GetLabelSize() );
                    
                    eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor][slice]->SetMarkerStyle(20);
                    eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor][slice]->SetMarkerColor(kBlack);
                    eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor][slice]->SetMarkerSize(1.2);
                    eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor][slice]->SetLineColor(kBlack);
                    eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor][slice]->SetLineWidth(2);
                    eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor][slice]->SetFillColorAlpha(kAzure+2, 0.5);
                    
                    eff1D_TH1_WithZ0SinThetaCut_Slices[NOMINAL][coor][slice]->SetMarkerStyle(25);
                    eff1D_TH1_WithZ0SinThetaCut_Slices[NOMINAL][coor][slice]->SetMarkerColor(kRed);
                    eff1D_TH1_WithZ0SinThetaCut_Slices[NOMINAL][coor][slice]->SetMarkerSize(1.2);
                    eff1D_TH1_WithZ0SinThetaCut_Slices[NOMINAL][coor][slice]->SetLineColor(kRed);
                    eff1D_TH1_WithZ0SinThetaCut_Slices[NOMINAL][coor][slice]->SetLineWidth(2);
                    eff1D_TH1_WithZ0SinThetaCut_Slices[NOMINAL][coor][slice]->SetFillStyle(0);
                    
                    eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor][slice]->Draw("PE2");
                    eff1D_TH1_WithZ0SinThetaCut_Slices[NOMINAL][coor][slice]->Draw("PE2 SAME");
                    
                    
                    TLatex l; l.SetNDC(); l.SetTextFont(72); l.SetTextSize(.05); l.SetTextColor(kBlack);
                    l.DrawLatex(.3+0.15+0.06, .78-0.57, "ATLAS #font[42]{Simulation Internal}");
                    l.DrawLatex(.45+0.2+0.06, .85-0.57, "#font[42]{#sqrt{s} = 13 TeV}");
                    l.SetTextSize(.07);
                    l.DrawLatex(.45+0.13+0.06, .85-0.57, mCepUtil->mParticleSymbolCh[i][j]);
                    
                    if(slice==(coor==CepUtil::X?0:1) && j==0){
                        TLegend *leg = new TLegend(0.39, 0.43, 0.52, 0.57, "Pythia 8 (ND)");
                        leg->SetTextSize(0.05);
                        leg->SetTextFont(42);
                        leg->SetBorderSize(0);
                        leg->SetFillStyle(0);
        //                 leg->SetFillColorAlpha(kYellow, 0.2);
                        leg->AddEntry( eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor][slice], "w/o |z_{0}sin#theta| < 1.5 mm", "pef" );
                        leg->AddEntry( eff1D_TH1_WithZ0SinThetaCut_Slices[NOMINAL][coor][slice], "w/   |z_{0}sin#theta| < 1.5 mm", "pef" );
                        leg->Draw();
                    }
                    
                    
                    l.SetTextFont(42); l.SetTextSize(.05);
                    l.DrawLatex(.18, .85, coor==CepUtil::X ? Form("%g < #eta < %g", effTH2_NoZ0SinThetaCut[i][j][NOMINAL][PASSED]->GetYaxis()->GetBinLowEdge(slice+1), effTH2_NoZ0SinThetaCut[i][j][NOMINAL][PASSED]->GetYaxis()->GetBinUpEdge(slice+1)) : Form("%g < p_{T}/GeV < %g", effTH2_NoZ0SinThetaCut[i][j][NOMINAL][PASSED]->GetXaxis()->GetBinLowEdge(slice+1), effTH2_NoZ0SinThetaCut[i][j][NOMINAL][PASSED]->GetXaxis()->GetBinUpEdge(slice+1)) );
                    
                    
                    c->Print(dirName+ (coor==CepUtil::X ? "InDetEff_SelfCalculated_pT_binsOf_eta.pdf" : "InDetEff_SelfCalculated_eta_binsOf_pT.pdf"));
                    if(slice==(eff1D_TH1_NoZ0SinThetaCut_Slices[NOMINAL][coor].size()-1)){ // extra dummy pages to have the same number of pages in both pdfs (help in LaTeX)
                        for(int v=0; v<5; ++v){
                            c->Clear();
                            c->Print(dirName+ (coor==CepUtil::X ? "InDetEff_SelfCalculated_pT_binsOf_eta.pdf" : "InDetEff_SelfCalculated_eta_binsOf_pT.pdf"));
                        }
                    }
                }
                
                
            }
        }
    }
    c->Print(dirName+"InDetEff_SelfCalculated.pdf]");
    c->Print(dirName+"InDetEff_SelfCalculated_pT_binsOf_eta.pdf]");
    c->Print(dirName+"InDetEff_SelfCalculated_eta_binsOf_pT.pdf]");
    
    
    
    //-------- syst err drawing 
    
    
    
    c = new TCanvas("c", "c", 900, 800);
    
    TGaxis::SetMaxDigits(3);
    gPad->SetLeftMargin(0.09);
    gPad->SetRightMargin(0.17);
    gPad->SetTopMargin(0.05);
    gPad->SetBottomMargin(0.15);
    
    gStyle->SetPalette(62);
    TColor::InvertPalette();
    gStyle->SetNumberContours(99);

        
    for(int k=0; k<2; ++k){
        
        TString pdfName(k==0 ? "InDetEff_SystErr_NoZ0SinThetaCut" : "InDetEff_SystErr_WithZ0SinThetaCut");
        auto hInDetTrackRecoEff_EtaVsPt_Array = k==0 ? effTH2F_NoZ0SinThetaCut_SystErr : effTH2F_WithZ0SinThetaCut_SystErr;
        
        c->Print(dirName+pdfName+".pdf["/*, "EmbedFonts"*/);
        for(int i=0; i<CepUtil::nDefinedParticles; ++i){
            for(int j=0; j<CepUtil::nSigns; ++j){
                
                TH2F * hInDetTrackRecoEff_EtaVsPt = hInDetTrackRecoEff_EtaVsPt_Array[i][j];
                
                hInDetTrackRecoEff_EtaVsPt->GetXaxis()->SetLabelOffset(1.5*hInDetTrackRecoEff_EtaVsPt->GetXaxis()->GetLabelOffset());
                hInDetTrackRecoEff_EtaVsPt->GetXaxis()->SetTitle("p_{T,true} [GeV]");
                hInDetTrackRecoEff_EtaVsPt->GetXaxis()->SetRangeUser(0.1, 1);
                hInDetTrackRecoEff_EtaVsPt->GetXaxis()->SetNdivisions(5,5,1);
                
                hInDetTrackRecoEff_EtaVsPt->GetYaxis()->SetLabelOffset(1.5*hInDetTrackRecoEff_EtaVsPt->GetXaxis()->GetLabelOffset());
                hInDetTrackRecoEff_EtaVsPt->GetYaxis()->SetTitle("#eta_{true}");
                hInDetTrackRecoEff_EtaVsPt->GetYaxis()->SetNdivisions(5,5,1);
                
                hInDetTrackRecoEff_EtaVsPt->GetZaxis()->SetLabelOffset(1.5*hInDetTrackRecoEff_EtaVsPt->GetXaxis()->GetLabelOffset());
                hInDetTrackRecoEff_EtaVsPt->GetZaxis()->SetTitle("Systematic uncertainty of the recon. eff.");
                hInDetTrackRecoEff_EtaVsPt->GetZaxis()->SetRangeUser(0, 0.10);
                
                hInDetTrackRecoEff_EtaVsPt->GetXaxis()->SetTitleOffset(effTH2FVec_NoZ0SinThetaCut[i][j][0]->GetXaxis()->GetTitleOffset());
                hInDetTrackRecoEff_EtaVsPt->GetXaxis()->SetTitleSize(effTH2FVec_NoZ0SinThetaCut[i][j][0]->GetXaxis()->GetTitleSize());
                hInDetTrackRecoEff_EtaVsPt->GetXaxis()->SetLabelOffset(effTH2FVec_NoZ0SinThetaCut[i][j][0]->GetXaxis()->GetLabelOffset());
                hInDetTrackRecoEff_EtaVsPt->GetXaxis()->SetLabelSize(effTH2FVec_NoZ0SinThetaCut[i][j][0]->GetXaxis()->GetLabelSize());
                
                hInDetTrackRecoEff_EtaVsPt->GetYaxis()->SetTitleOffset(effTH2FVec_NoZ0SinThetaCut[i][j][0]->GetYaxis()->GetTitleOffset());
                hInDetTrackRecoEff_EtaVsPt->GetYaxis()->SetTitleSize(effTH2FVec_NoZ0SinThetaCut[i][j][0]->GetYaxis()->GetTitleSize());
                hInDetTrackRecoEff_EtaVsPt->GetYaxis()->SetLabelOffset(effTH2FVec_NoZ0SinThetaCut[i][j][0]->GetYaxis()->GetLabelOffset());
                hInDetTrackRecoEff_EtaVsPt->GetYaxis()->SetLabelSize(effTH2FVec_NoZ0SinThetaCut[i][j][0]->GetYaxis()->GetLabelSize());
                
                hInDetTrackRecoEff_EtaVsPt->GetZaxis()->SetTitleOffset(/*effTH2FVec_NoZ0SinThetaCut[i][j][0]->GetZaxis()->GetTitleOffset()*/1.4);
                hInDetTrackRecoEff_EtaVsPt->GetZaxis()->SetTitleSize(effTH2FVec_NoZ0SinThetaCut[i][j][0]->GetZaxis()->GetTitleSize());
                hInDetTrackRecoEff_EtaVsPt->GetZaxis()->SetLabelOffset(effTH2FVec_NoZ0SinThetaCut[i][j][0]->GetZaxis()->GetLabelOffset());
                hInDetTrackRecoEff_EtaVsPt->GetZaxis()->SetLabelSize(effTH2FVec_NoZ0SinThetaCut[i][j][0]->GetZaxis()->GetLabelSize());
                hInDetTrackRecoEff_EtaVsPt->GetZaxis()->SetNdivisions(5,2,1);
                
                hInDetTrackRecoEff_EtaVsPt->Draw("colz");
                
                gPad->Update();
                TPaletteAxis *palette = (TPaletteAxis*)hInDetTrackRecoEff_EtaVsPt->GetListOfFunctions()->FindObject("palette");
                if(palette){
                    palette->SetX1NDC(0.835);
                    palette->SetX2NDC(0.865);
                }
                gPad->Modified();
                
                TLatex l; l.SetNDC(); l.SetTextFont(72); l.SetTextSize(.05); l.SetTextColor(kWhite);
                l.DrawLatex(.3, .85, "ATLAS #font[42]{Simulation Internal}");
                l.DrawLatex(.45, .78, "#font[42]{#sqrt{s} = 13 TeV}");
                l.DrawLatex(.45, .71, mCepUtil->mParticleSymbolCh[i][j]);
                
                c->Print(dirName+pdfName+".pdf"/*, "EmbedFonts"*/);   
            }
        }
        c->Print(dirName+pdfName+".pdf]"/*, "EmbedFonts"*/);
    
    }
    
    
    
    
    
    TFile *outFile = new TFile(dirName+"InDetEff_SystErr.root", "RECREATE");
    
    for(int i=0; i<CepUtil::nDefinedParticles; ++i){
        for(int j=0; j<CepUtil::nSigns; ++j){
            effTH2F_NoZ0SinThetaCut_SystErr[i][j]->SetDirectory(outFile);
            effTH2F_WithZ0SinThetaCut_SystErr[i][j]->SetDirectory(outFile);
        }
    }
    
    outFile->Write();
    outFile->Close();
    
}




TH2F* CepAnalysis::getTH2F_from_TH1F_t1t2( const TH1F* hIn, bool divideByBinWidth ){
    
    vector<double> binningT_2D_Vec;
    binningT_2D_Vec.push_back(0.05);
    binningT_2D_Vec.push_back(0.08);
    binningT_2D_Vec.push_back(0.13);
    binningT_2D_Vec.push_back(0.18);
    binningT_2D_Vec.push_back(0.25);
  
  TH2F* hOut = new TH2F( Form("TH2F_of_%s", hIn->GetName()), Form("TH2F_of_%s", hIn->GetName()), binningT_2D_Vec.size()-1, &binningT_2D_Vec[0], binningT_2D_Vec.size()-1, &binningT_2D_Vec[0] );
  
  for(int i=0; i<4; ++i){
    for(int j=0; j<4; ++j){
      double bin2dWidth = divideByBinWidth ? ((binningT_2D_Vec[i+1] - binningT_2D_Vec[i]) * (binningT_2D_Vec[j+1] - binningT_2D_Vec[j])) : 1.0;
      hOut->SetBinContent( i+1, j+1, hIn->GetBinContent(4*i+j+1)/bin2dWidth );
      hOut->SetBinError( i+1, j+1, hIn->GetBinError(4*i+j+1)/bin2dWidth );
    }
  }
  
  return hOut;
  
}




void CepAnalysis::drawNSigmaMissingPt(TFile *file){
    
    const TString dirName("PDF_NSigmaMissingPt/");
    mkdir(dirName.Data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    
  CepPhysicsContainer* mCepPhys = new CepPhysicsContainer( "Physics", file );
    
    
  double hatchesSpacing = gStyle->GetHatchesSpacing();
  double hatchesLineWidth = gStyle->GetHatchesLineWidth();
  gStyle->SetHatchesSpacing(1.5);
  gStyle->SetHatchesLineWidth(3);
  
  
//   TH1F *hMissingPtPid_signal[4];
//   TH1F *hInvMassPid_signal[4];
  TF1* funcMissingPt[CepUtil::nCepChannels];
  
//   double ptmisseff[4] = {1., 1., 1., 1.};
//   double signalNormalization[4] = {1., 1., 1., 1.};
//   double integratedBkgdFrac[4] = {1., 1., 1., 1.};
  
  for(int cepChannel=0; cepChannel<CepUtil::nCepChannels; ++cepChannel){
      
    TH1D *hNSigmaPtMiss_Oppo;
    TH1D *hNSigmaPtMiss_Same;
    hNSigmaPtMiss_Oppo = mCepPhys->ooInvMass[cepChannel]->hNSigmaPtMissVsQ[CepUtil::CH_SUM_0]->ProjectionY(Form("%drandName", static_cast<int>(1000000*gRandom->Uniform())));
    hNSigmaPtMiss_Same = mCepPhys->ooInvMass[cepChannel]->hNSigmaPtMissVsQ[CepUtil::CH_SUM_NOT_0]->ProjectionY(Form("%drandName", static_cast<int>(1000000*gRandom->Uniform())));
    
    funcMissingPt[cepChannel] = new TF1();
    funcMissingPt[cepChannel]->SetName(Form("%dfunc", static_cast<int>(1000000*gRandom->Uniform())));
    /*integratedBkgdFrac[cepChannel] = */mCepUtil->bkgdFraction( hNSigmaPtMiss_Oppo, 3, 5, 10, funcMissingPt[cepChannel] );
    
//     if(fileSignal[cepChannel]){
//       hMissingPtPid_signal[cepChannel] = dynamic_cast<TH1F*>(  fileSignal[cepChannel]->Get( hNSigmaPtMiss_Oppo->GetName() ) );
//       hMissingPtPid_signal[cepChannel]->SetDirectory(0);
//       hMissingPtPid_signal[cepChannel]->SetName( Form("%s_signalFromCepMC", hNSigmaPtMiss_Oppo->GetName()) );
//       
//       hInvMassPid_signal[cepChannel] = dynamic_cast<TH1F*>(  fileSignal[cepChannel]->Get( mhInvMassPid[CepUtil::OPPO][cepChannel]->GetName() ) );
//       hInvMassPid_signal[cepChannel]->SetDirectory(0);
//       hInvMassPid_signal[cepChannel]->SetName( Form("%s_signalFromCepMC", mhInvMassPid[CepUtil::OPPO][cepChannel]->GetName()) );
//       
//       hInvMassPid_signal[cepChannel]->SetFillColor( cepChannel==0 ? (kYellow-7) : (cepChannel==1 ? (kTeal-8) : myBlueIndex) );
//       hInvMassPid_signal[cepChannel]->SetLineColor( kBlack );
//       hInvMassPid_signal[cepChannel]->SetLineWidth( 2 );
//       
//       ptmisseff[cepChannel] = hMissingPtPid_signal[cepChannel]->Integral( 0, hMissingPtPid_signal[cepChannel]->GetXaxis()->FindBin( mParams->maxMissingPt()-0.0001 ), "width" ) / hMissingPtPid_signal[cepChannel]->Integral( 0, -1, "width" );
//       signalNormalization[cepChannel] =  (1.-integratedBkgdFrac[cepChannel]) * hNSigmaPtMiss_Oppo->Integral( 0, hNSigmaPtMiss_Oppo->GetXaxis()->FindBin( mParams->maxMissingPt()-0.0001 ), "width" ) /
//       hMissingPtPid_signal[cepChannel]->Integral( 0, hMissingPtPid_signal[cepChannel]->GetXaxis()->FindBin( mParams->maxMissingPt()-0.0001 ), "width" );
//       
//       fileSignal[cepChannel]->Close();
//     }
  
    
    TString pairStr = mCepUtil->mCepChannelLatexName[cepChannel];
    TString singleTrkPlusMinusStr;
    TString singleTrkStr[2];
    singleTrkStr[CepUtil::MINUS] = TString((cepChannel!=CepUtil::KK && cepChannel!=CepUtil::PPBAR) ? "#pi^{-}" : (cepChannel==CepUtil::KK ? "K^{-}" : "#bar{#it{p}}") );
    singleTrkStr[CepUtil::PLUS] = TString((cepChannel!=CepUtil::KK && cepChannel!=CepUtil::PPBAR) ? "#pi^{+}" : (cepChannel==CepUtil::KK ? "K^{+}" : "#it{p}") );
    singleTrkPlusMinusStr = TString( (cepChannel!=CepUtil::KK && cepChannel!=CepUtil::PPBAR) ? "#pi^{#pm}:" : (cepChannel==CepUtil::KK ? "K^{#pm}:" : "#it{p},#bar{#it{p}}:") );
    
    //---
    
    std::vector<double> binsVector;
    std::vector<double> binsVector2;
      
//     if( cepChannel==3 ){
//         binsVector.push_back(  0 );
//         binsVector.push_back(  1 );
//         binsVector.push_back(  2 );
//         binsVector.push_back(  3.0 );
//     } else{
        binsVector.push_back(  0 );
        binsVector.push_back(  0.25 );
        binsVector.push_back(  0.5 );
        binsVector.push_back(  1 );
        binsVector.push_back(  1.5 );
        binsVector.push_back(  2 );
        binsVector.push_back(  2.5 );
        binsVector.push_back(  3.0 );
//     }
        
        binsVector.push_back(  5.0 );
        binsVector.push_back(  7.5 );
        binsVector.push_back(  10.0 );
        binsVector.push_back(  12.0 );
        binsVector.push_back(  14.0 );
        binsVector.push_back(  16.0 );
        binsVector.push_back(  18.0 );
        binsVector.push_back(  20.0 );
        binsVector.push_back(  22.0 );
        binsVector.push_back(  26.0 );
        binsVector.push_back(  30.0 );
        
        binsVector2.push_back(  0 );
        binsVector2.push_back(  1 );
        binsVector2.push_back(  2 );
        binsVector2.push_back(  3.0 );
        binsVector2.push_back(  5.0 );
        binsVector2.push_back(  7.5 );
        binsVector2.push_back(  10.0 );
        binsVector2.push_back(  12.0 );
        binsVector2.push_back(  14.0 );
        binsVector2.push_back(  16.0 );
        binsVector2.push_back(  18.0 );
        binsVector2.push_back(  20.0 );
        binsVector2.push_back(  22.0 );
        binsVector2.push_back(  26.0 );
        binsVector2.push_back(  30.0 );
  
      
      
    TH1F* missingPtHist_DataOppo = new TH1F( Form("%s_copyayy", hNSigmaPtMiss_Oppo->GetName()), Form("%s_copyyy", hNSigmaPtMiss_Oppo->GetName()), binsVector.size()-1, &binsVector[0] );
    TH1F* missingPtHist_DataSame = new TH1F( Form("%s_copysyy", hNSigmaPtMiss_Same->GetName()), Form("%s_copyyy", hNSigmaPtMiss_Same->GetName()), binsVector2.size()-1, &binsVector2[0] );
//     TH1F* hMissingPtPid_signal_pointer = hMissingPtPid_signal[cepChannel];
//     if(hMissingPtPid_signal_pointer)
//       hMissingPtPid_signal[cepChannel] = new TH1F( Form("%s_copyyyd", hMissingPtPid_signal_pointer->GetName()), Form("%s_copyyy", hMissingPtPid_signal_pointer->GetName()), binsVector.size()-1, &binsVector[0] );
    TH1F* missingPtHist_nonExclBkgd = new TH1F( Form("%s_missingPtHist_nonExclBkgd", hNSigmaPtMiss_Oppo->GetName()), Form("%s_missingPtHist_nonExclBkgd", hNSigmaPtMiss_Oppo->GetName()), binsVector.size()-1, &binsVector[0] );
    TH1F* missingPtHist_nonExclBkgd_FitRegion = new TH1F( Form("%s_missingPtHist_nonExclBkgd_FitRegion", hNSigmaPtMiss_Oppo->GetName()), Form("%s_missingPtHist_nonExclBkgd_FitRegion", hNSigmaPtMiss_Oppo->GetName()), binsVector.size()-1, &binsVector[0] );
    TH1F* missingPtHist_nonExclBkgd_FullRange = new TH1F( Form("%s_missingPtHist_nonExclBkgd_FullRange", hNSigmaPtMiss_Oppo->GetName()), Form("%s_missingPtHist_nonExclBkgd_FullRange", hNSigmaPtMiss_Oppo->GetName()), binsVector.size()-1, &binsVector[0] );
    
    for(int bin=0; bin<=(hNSigmaPtMiss_Oppo->GetNbinsX()+1); ++bin){
      missingPtHist_DataOppo->Fill( hNSigmaPtMiss_Oppo->GetXaxis()->GetBinCenter(bin), hNSigmaPtMiss_Oppo->GetBinContent(bin) );
      missingPtHist_DataSame->Fill( hNSigmaPtMiss_Same->GetXaxis()->GetBinCenter(bin), hNSigmaPtMiss_Same->GetBinContent(bin) );
//       if(hMissingPtPid_signal_pointer)
//         hMissingPtPid_signal[cepChannel]->Fill( hMissingPtPid_signal_pointer->GetXaxis()->GetBinCenter(bin), hMissingPtPid_signal_pointer->GetBinContent(bin) );
    }
    
//     if(hMissingPtPid_signal_pointer){
//       for(int i=0; i<funcMissingPt[cepChannel]->GetNpar(); ++i)
//         funcMissingPt[cepChannel]->SetParameter( i, funcMissingPt[cepChannel]->GetParameter(i) * missingPtHist_DataOppo->GetBinWidth(1)/hMissingPtPid_signal_pointer->GetBinWidth(1) );
//     }
   
    for(int bin=1; bin<=missingPtHist_DataOppo->GetNbinsX(); ++bin){ // ommit last bin which is the overflow
      missingPtHist_DataOppo->SetBinError( bin, sqrt(missingPtHist_DataOppo->GetBinContent(bin)) );
      missingPtHist_DataOppo->SetBinContent( bin, missingPtHist_DataOppo->GetBinContent(bin) * missingPtHist_DataOppo->GetBinWidth(1)/missingPtHist_DataOppo->GetBinWidth(bin) );
      missingPtHist_DataOppo->SetBinError( bin, missingPtHist_DataOppo->GetBinError(bin) * missingPtHist_DataOppo->GetBinWidth(1)/missingPtHist_DataOppo->GetBinWidth(bin) );
      
      missingPtHist_DataSame->SetBinError( bin, sqrt(missingPtHist_DataSame->GetBinContent(bin)) );
      missingPtHist_DataSame->SetBinContent( bin, missingPtHist_DataSame->GetBinContent(bin) * missingPtHist_DataOppo->GetBinWidth(1)/missingPtHist_DataSame->GetBinWidth(bin) );
      missingPtHist_DataSame->SetBinError( bin, missingPtHist_DataSame->GetBinError(bin) * missingPtHist_DataOppo->GetBinWidth(1)/missingPtHist_DataSame->GetBinWidth(bin) );
      
//       if(hMissingPtPid_signal_pointer){
//         hMissingPtPid_signal[cepChannel]->SetBinError( bin, sqrt(hMissingPtPid_signal[cepChannel]->GetBinContent(bin)) );
//         hMissingPtPid_signal[cepChannel]->SetBinContent( bin, hMissingPtPid_signal[cepChannel]->GetBinContent(bin) * hMissingPtPid_signal[cepChannel]->GetBinWidth(1)/hMissingPtPid_signal[cepChannel]->GetBinWidth(bin) );
//         hMissingPtPid_signal[cepChannel]->SetBinError( bin, hMissingPtPid_signal[cepChannel]->GetBinError(bin) * hMissingPtPid_signal[cepChannel]->GetBinWidth(1)/hMissingPtPid_signal[cepChannel]->GetBinWidth(bin) );
//       }
    }
   
    for(int bin=1; bin<=missingPtHist_nonExclBkgd->GetNbinsX(); ++bin){
      
      const double integralOverBinWidth = funcMissingPt[cepChannel]->Integral( missingPtHist_nonExclBkgd->GetXaxis()->GetBinLowEdge(bin), missingPtHist_nonExclBkgd->GetXaxis()->GetBinUpEdge(bin) ) / missingPtHist_nonExclBkgd->GetBinWidth(bin);
      
      if( missingPtHist_nonExclBkgd->GetXaxis()->GetBinCenter(bin) < 3 ){
        missingPtHist_nonExclBkgd->SetBinContent( bin, integralOverBinWidth );
        missingPtHist_nonExclBkgd->SetBinError( bin, 0. );
      }
      if( missingPtHist_nonExclBkgd_FitRegion->GetXaxis()->GetBinCenter(bin) < 10 && missingPtHist_nonExclBkgd_FitRegion->GetXaxis()->GetBinCenter(bin) > 5){
        missingPtHist_nonExclBkgd_FitRegion->SetBinContent( bin, integralOverBinWidth );
        missingPtHist_nonExclBkgd_FitRegion->SetBinError( bin, 0. );
      }
      if( missingPtHist_nonExclBkgd_FitRegion->GetXaxis()->GetBinCenter(bin) < 10 ){
        missingPtHist_nonExclBkgd_FullRange->SetBinContent( bin, integralOverBinWidth );
        missingPtHist_nonExclBkgd_FullRange->SetBinError( bin, 0. );
      }
    }

    
    DrawingOptions opts;
    
    opts.mCanvasWidth = 900;
    opts.mCanvasHeight = 800;
    
    opts.mLeftMargin = 0.12;
    opts.mRightMargin = 0.035;
    opts.mTopMargin = 0.06;
    opts.mBottomMargin = 0.13;
    
    //x axis
    opts.mXmin = 0;
    opts.mXmax = 30;
    opts.mXaxisTitle = TString("n#sigma(p_{T}^{miss})");
    opts.mXaxisTitleSize = 0.05;
    opts.mXaxisTitleOffset = 1.04;
    opts.mXaxisLabelSize = 0.05;
    opts.mXaxisLabelOffset = 0.007;
    opts.mXaxisTickLength = 0.015;
    
    opts.mSetDivisionsX = kTRUE;
    //     opt.mForceDivisionsX = kTRUE;
    opts.mXnDivisionA = 5;
    opts.mXnDivisionB = 5;
    opts.mXnDivisionC = 1;
    
    opts.mSetDivisionsY = kTRUE;
    //     opt.mForceDivisionsY = kTRUE;
    opts.mYnDivisionA = 5;
    opts.mYnDivisionB = 5;
    opts.mYnDivisionC = 1;
    
    //y axis
    opts.mYmin = 0;
    opts.mYmax = (cepChannel > CepUtil::FOURPI ? 1.6 : 1.15) * missingPtHist_DataOppo->GetMaximum();
    opts.mYaxisTitle = TString( Form("Events / %g", missingPtHist_DataOppo->GetXaxis()->GetBinWidth(1) ) );
    opts.mYaxisTitleSize = 0.05;
    opts.mYaxisTitleOffset = 1.25;
    opts.mYaxisLabelSize = 0.05;
    opts.mYaxisLabelOffset = 0.007;
    opts.mYaxisTickLength = 0.015;
    
    opts.mMarkerStyle = 20;
    opts.mMarkerColor = kBlack;
    opts.mMarkerSize = 1.6;
    
    opts.mMarkerStyle2 = 20;
    opts.mMarkerColor2 = kRed;
    opts.mMarkerSize2 = 1.6;
    
    opts.mLineStyle = 1;
    opts.mLineColor = kBlack;
    opts.mLineWidth = 2;
    
    opts.mLineStyle2 = 1;
    opts.mLineColor2 = kRed;
    opts.mLineWidth2 = 2;
    
    opts.mPdfName = Form("%sNSigmaMissingPt_channel%d", dirName.Data(), cepChannel);
    
    opts.mNColumns = 1;
    opts.mLegendX = cepChannel==0 ? 0.37 : (cepChannel==1 ? 0.45 : 0.5);
    opts.mLegendXwidth = 0.4;
    opts.mLegendY = cepChannel==0 ? 0.35 : (cepChannel==1 ? 0.4 : 0.45);
    opts.mLegendYwidth = 0.24;
    opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
    opts.mSetLegendMargin = kTRUE;
    opts.mLegendMargin = 0.11;
    if(cepChannel>1)
        opts.mFillLegendWithColor = kTRUE;
    
    opts.mDataLegendText_1 = TString(cepChannel==0 ? "" : "Data (total charge = 0)");
    opts.mDataLegendText_2 = TString(cepChannel==0 ? "" : "Data (total charge #neq 0)");
    
    
    double arrowHeight = cepChannel<CepUtil::FOURPI ? 0.4 : (cepChannel==CepUtil::FOURPI ? 0.6 : 0.9);
    
    TLine *cutLine = new TLine( 3, 0, 3, arrowHeight*missingPtHist_DataOppo->GetMaximum() );
    cutLine->SetLineWidth(4);
    cutLine->SetLineColor(kRed);
    cutLine->SetLineStyle(7);
    cutLine->Draw();
    opts.mObjToDraw.push_back(cutLine);
    opts.mObjToDrawOption.push_back("");
    
    
    TArrow *cutArrow = new TArrow( 3, arrowHeight*missingPtHist_DataOppo->GetMaximum(), 1.5, arrowHeight*missingPtHist_DataOppo->GetMaximum(), 0.035, "|>");
    cutArrow->SetAngle(30);
    cutArrow->SetLineWidth(4);
    cutArrow->SetLineColor( kRed );
    cutArrow->SetFillColor( kRed );
    opts.mObjToDraw.push_back(cutArrow);
    opts.mObjToDrawOption.push_back("");
    
    TLatex lReaction(.16, .88, "#font[72]{ATLAS} Internal    #it{p}+#it{p}#rightarrow#it{p}'+"+pairStr+"+#it{p}'  #sqrt{#it{s}} = 13 TeV"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
    opts.mTextToDraw.push_back( lReaction );
//     TLatex lATLAS(0.15, 0.85, "ATLAS"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
//     opts.mTextToDraw.push_back( lATLAS );
    
    //---//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--
    
    missingPtHist_nonExclBkgd_FitRegion->SetFillColor( kMagenta );
    missingPtHist_nonExclBkgd_FitRegion->SetFillStyle( 3345 );
    missingPtHist_nonExclBkgd_FitRegion->SetLineColor( kMagenta+1 );
    missingPtHist_nonExclBkgd_FitRegion->SetLineWidth( 0 );
    
    TH1F *hCopyForDrawing = new TH1F(*missingPtHist_nonExclBkgd_FitRegion);
    hCopyForDrawing->SetName("hCopyForDrawing");
    hCopyForDrawing->SetLineWidth(2);
    
    opts.mHistTH1F.push_back( missingPtHist_nonExclBkgd_FitRegion );
    opts.mHistTH1F_DrawingOpt.push_back( "HIST ][ SAME" );
    opts.mObjForLegend.push_back( hCopyForDrawing );
    opts.mDesriptionForLegend.push_back(  "Non-excl. background (fit)" );
    opts.mDrawTypeForLegend.push_back( "f" );
    
    //--
    
    missingPtHist_nonExclBkgd->SetFillColor( kMagenta );
    missingPtHist_nonExclBkgd->SetLineColor( kMagenta+1 );
    missingPtHist_nonExclBkgd->SetLineWidth( 2 );
    
    opts.mHistTH1F.push_back( missingPtHist_nonExclBkgd );
    opts.mHistTH1F_DrawingOpt.push_back( "HIST ][ SAME" );
    opts.mObjForLegend.push_back( missingPtHist_nonExclBkgd );
    opts.mDesriptionForLegend.push_back(  "Non-excl. background (extr.)" );
    opts.mDrawTypeForLegend.push_back( "f" );
    
    //--
    
    missingPtHist_nonExclBkgd_FullRange->SetLineColor( kMagenta+1 );
    missingPtHist_nonExclBkgd_FullRange->SetLineWidth( 2 );
    opts.mHistTH1F.push_back( missingPtHist_nonExclBkgd_FullRange );
    opts.mHistTH1F_DrawingOpt.push_back( "HIST ][ SAME" );
    //--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--v
    
//     hMissingPtPid_signal[cepChannel]->Scale( signalNormalization[cepChannel] );
//     hMissingPtPid_signal[cepChannel]->SetFillColor( cepChannel==0 ? (kYellow-7) : (cepChannel==1 ? (kTeal-8) : myBlueIndex) );
//     hMissingPtPid_signal[cepChannel]->SetLineColor( kBlack );
//     hMissingPtPid_signal[cepChannel]->SetLineWidth( 2 );
//     
//     opts.mHistTH1_stack.push_back( hMissingPtPid_signal[cepChannel] );
//     opts.mHistTH1_stack_DrawingOpt.push_back( "HIST" );
//     opts.mObjForLegend.push_back( hMissingPtPid_signal[cepChannel] );
//     opts.mDesriptionForLegend.push_back(  TString("Exclusive ")+pairStr );
//     opts.mDrawTypeForLegend.push_back( "f" );
    
    /* //special shaded magenta for the background
    double ww = 0.2 - mParams->maxMissingPt();
    for(int bb=0; bb<100; ++bb){
      double xMinSystErrorBox = mParams->maxMissingPt() + bb*ww/100;
      double xMaxSystErrorBox = mParams->maxMissingPt() + (bb+1)*ww/100;
      int binNumber = missingPtHist_nonExclBkgd->GetXaxis()->FindBin( xMinSystErrorBox );
      double yMinSystErrorBox = 0;
      double yMaxSystErrorBox = missingPtHist_nonExclBkgd->GetBinContent(binNumber);
      TBox *b = new TBox( xMinSystErrorBox, yMinSystErrorBox, xMaxSystErrorBox, yMaxSystErrorBox );
      b->SetFillColor( magentaToWhite[bb] );
      b->SetLineWidth(0);
      b->Draw("l");
      opts.mObjToDraw.push_back( b );
      opts.mObjToDrawOption.push_back("SAME");
    }
    */
    //----
    
    
        TString tpcMinPt; tpcMinPt.Form("p_{T} > %.1f GeV", 0.1 );
        TString tpcMinPt2; tpcMinPt2.Form("max(p_{T}) > %.1f GeV", /*mParams->minTpcTrackPt(pid)*/0.2 );
        TString tpcEta; tpcEta.Form("|#eta| < %.1f", /*mParams->maxEta()*/2.5 );
        TString rpKinematicRegion1("0.18 GeV < |p_{y}| < 0.46 GeV");
        TLatex lCutsA(.16+0.08,.84-0.02, singleTrkPlusMinusStr); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
        TLatex lCutsB(.52,.84-0.02, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
        TLatex lCuts1(.26+0.04,.84-0.02, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
        TLatex lCuts12(.26+0.04,.795-0.02, tpcMinPt2); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
        TLatex lCuts11(.26+0.04,.75-0.02, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
        TLatex lCuts3(.62,.84-0.02, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
    
    drawDataMcComparison2( missingPtHist_DataOppo, missingPtHist_DataSame, opts );
  }
  
  gStyle->SetHatchesSpacing(hatchesSpacing);
  gStyle->SetHatchesLineWidth(hatchesLineWidth);
    
  
  delete mCepPhys;
}


void CepAnalysis::drawPtCorrections(TFile *file){
        
    const TString dirName("PDF_pTCorr/");
    mkdir(dirName.Data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    
    
    CepHistograms_ID *hID = new CepHistograms_ID("ID", false, file);
    auto hPtRecoMinusPtTrue_Vs_PtTrue = hID->hPtRecoMinusPtTrue_Vs_PtTrue;
    
    gStyle->SetPalette(62);
    TColor::InvertPalette();
    
    {
        TCanvas *c;
        TGaxis::SetMaxDigits(3);
        //   TLegend *legend;
        TLatex l; l.SetNDC(); l.SetTextFont(72); l.SetTextSize(.05);
        
        c = new TCanvas("c", "c", 800, 600);
        TGaxis::SetMaxDigits(3);
        
        c->SetLeftMargin(0.13);
        c->SetRightMargin(0.16);
        c->SetTopMargin(0.06);
        c->SetBottomMargin(0.13);
         
        c->Print(dirName+"pTCorr.pdf[");
        for(int i=0; i<CepUtil::nDefinedParticles; ++i){
            
            TH1D *hProj[2];
            
            for(int j=0; j<2; ++j){
                c->Clear();
                
                hPtRecoMinusPtTrue_Vs_PtTrue[i][j]->SetMarkerSize(1.4);
                hPtRecoMinusPtTrue_Vs_PtTrue[i][j]->SetMarkerStyle(20);
                hPtRecoMinusPtTrue_Vs_PtTrue[i][j]->SetMarkerColor(kBlack);
                hPtRecoMinusPtTrue_Vs_PtTrue[i][j]->SetLineColor(kBlack);
                
                hPtRecoMinusPtTrue_Vs_PtTrue[i][j]->GetXaxis()->SetTitle("p_{T,true} [GeV]");
                hPtRecoMinusPtTrue_Vs_PtTrue[i][j]->GetYaxis()->SetTitle("p_{T,reco} - p_{T,true} [GeV]");
                hPtRecoMinusPtTrue_Vs_PtTrue[i][j]->GetZaxis()->SetTitle( Form("Number of tracks / %g MeV #times %g MeV", 1e3*hPtRecoMinusPtTrue_Vs_PtTrue[i][j]->GetXaxis()->GetBinWidth(1), 1e3*hPtRecoMinusPtTrue_Vs_PtTrue[i][j]->GetYaxis()->GetBinWidth(1)));
                
                hPtRecoMinusPtTrue_Vs_PtTrue[i][j]->GetXaxis()->SetTitleSize( 1.3*hPtRecoMinusPtTrue_Vs_PtTrue[i][j]->GetXaxis()->GetTitleSize() );
                hPtRecoMinusPtTrue_Vs_PtTrue[i][j]->GetYaxis()->SetTitleSize( 1.3*hPtRecoMinusPtTrue_Vs_PtTrue[i][j]->GetYaxis()->GetTitleSize() );
                hPtRecoMinusPtTrue_Vs_PtTrue[i][j]->GetZaxis()->SetTitleSize( 1.3*hPtRecoMinusPtTrue_Vs_PtTrue[i][j]->GetZaxis()->GetTitleSize() );
                hPtRecoMinusPtTrue_Vs_PtTrue[i][j]->GetXaxis()->SetLabelSize( 1.3*hPtRecoMinusPtTrue_Vs_PtTrue[i][j]->GetXaxis()->GetLabelSize() );
                hPtRecoMinusPtTrue_Vs_PtTrue[i][j]->GetYaxis()->SetLabelSize( 1.3*hPtRecoMinusPtTrue_Vs_PtTrue[i][j]->GetYaxis()->GetLabelSize() );
                hPtRecoMinusPtTrue_Vs_PtTrue[i][j]->GetZaxis()->SetLabelSize( 1.3*hPtRecoMinusPtTrue_Vs_PtTrue[i][j]->GetZaxis()->GetLabelSize() );
                
                hPtRecoMinusPtTrue_Vs_PtTrue[i][j]->GetXaxis()->SetTitleOffset(1.15);
                hPtRecoMinusPtTrue_Vs_PtTrue[i][j]->GetYaxis()->SetTitleOffset(1.15);
                
                hPtRecoMinusPtTrue_Vs_PtTrue[i][j]->GetYaxis()->SetRangeUser(-0.15, 0.15);
                hPtRecoMinusPtTrue_Vs_PtTrue[i][j]->GetYaxis()->SetNdivisions(4,5,1);
                hPtRecoMinusPtTrue_Vs_PtTrue[i][j]->GetXaxis()->SetNdivisions(5,1,1);
                
                hPtRecoMinusPtTrue_Vs_PtTrue[i][j]->Draw("colz");
                
                TLatex lATLAS(.16, .86, "#font[72]{ATLAS} Internal Simulation"); lATLAS.SetNDC(); lATLAS.SetTextFont(42);  lATLAS.SetTextSize(0.05); lATLAS.Draw();
                
                l.DrawLatex(.22, .2, Form("#font[42]{%s, %s from Pythia CD    #sqrt{s} = 13 TeV}", mCepUtil->mParticleSymbolCh[i][CepUtil::MINUS].Data(), mCepUtil->mParticleSymbolCh[i][CepUtil::PLUS].Data()));
                l.DrawLatex(.22, .76, Form("#font[42]{%s}", j==0?"Before corr.":"After corr."));
        //         l.DrawLatex(.45, .5, "#font[42]{|z_{0}| < 150 mm}");
                
                c->Print(dirName+"pTCorr.pdf");
            }
            
            c->Clear();
            
            for(int j=1; j>=0; --j){
                hProj[j] = hPtRecoMinusPtTrue_Vs_PtTrue[i][j]->ProjectionY(Form("hDeltaPt_%d%d",i,j));
                
                hProj[j]->SetLineColor(j==0 ? kRed : kBlack );
                hProj[j]->GetYaxis()->SetRangeUser(0, 1.3 * hProj[j]->GetMaximum());
                hProj[j]->GetXaxis()->SetNdivisions(5,5,1);
                hProj[j]->GetYaxis()->SetNdivisions(5,5,1);
                hProj[j]->GetYaxis()->SetTitle( Form("Number of tracks / %g MeV", 1e3*hProj[j]->GetXaxis()->GetBinWidth(1)));
                hProj[j]->GetYaxis()->SetTitleOffset(0.9);
                
                hProj[j]->Draw(j==1 ? "HIST" : "HIST SAME");
                
                gPad->RedrawAxis();
            }
            
            TLegend *leg = new TLegend(0.22+0.33, 0.25+0.3, 0.55+0.33, 0.4+0.3);
            leg->SetBorderSize(0);
            leg->SetFillStyle(0);
            leg->AddEntry( hProj[0], "Before corr.", "f" );
            leg->AddEntry( hProj[1], "After corr.", "f" );
            leg->Draw();
            
            TLatex lATLAS(.16, .86, "#font[72]{ATLAS} Internal Simulation"); lATLAS.SetNDC(); lATLAS.SetTextFont(42);  lATLAS.SetTextSize(0.05); lATLAS.Draw();
            l.DrawLatex(.16, .78, Form("#font[42]{%s, %s from Pythia CD    #sqrt{s} = 13 TeV}", mCepUtil->mParticleSymbolCh[i][CepUtil::MINUS].Data(), mCepUtil->mParticleSymbolCh[i][CepUtil::PLUS].Data()));
            
            c->Print(dirName+"pTCorr.pdf");
            
        }
        c->Print(dirName+"pTCorr.pdf]");
    
    }
    
    
    //------------------------------------------------------------------------------------------------------------------------------------
    
    TColor::InvertPalette();
    
    
    // now the energy loss correction
    TH2D* mIDTrackingEnergyLoss_WithZ0SinThetaCut[CepUtil::nDefinedParticles][CepUtil::nSigns];
    TH2D* mIDTrackingEnergyLoss_NoZ0SinThetaCut[CepUtil::nDefinedParticles][CepUtil::nSigns];
    
    TFile *f = TFile::Open( "../../InclusiveDiffractionAnalysis/InclusiveDiffractionAnalysis/share/CepCorrections/MomentumCorr.root" );    
    for(int i=0; i<CepUtil::nDefinedParticles; ++i){
        for(int j=0; j<CepUtil::nSigns; ++j){            
            TString name;
            name.Form("%s_mass%d_PtBias_WithZ0SinThetaCut_1", j==CepUtil::PLUS ? "Pos" : "Neg", i);
            mIDTrackingEnergyLoss_WithZ0SinThetaCut[i][j] = dynamic_cast<TH2D*>( f->Get( name ) );
            if( mIDTrackingEnergyLoss_WithZ0SinThetaCut[i][j] )
                mIDTrackingEnergyLoss_WithZ0SinThetaCut[i][j]->SetDirectory(0);
            name.Form("%s_mass%d_PtBias_NoZ0SinThetaCut_1", j==CepUtil::PLUS ? "Pos" : "Neg", i);
            mIDTrackingEnergyLoss_NoZ0SinThetaCut[i][j] = dynamic_cast<TH2D*>( f->Get( name ) );
            if( mIDTrackingEnergyLoss_NoZ0SinThetaCut[i][j] ){
                mIDTrackingEnergyLoss_NoZ0SinThetaCut[i][j]->SetDirectory(0);
            }
            
        }
    }
    f->Close();
    
    
    TCanvas *c = new TCanvas("c", "c", 900, 800);
    c->cd();
    
    TGaxis::SetMaxDigits(3);
    gPad->SetLeftMargin(0.09);
    gPad->SetRightMargin(0.17);
    gPad->SetTopMargin(0.05);
    gPad->SetBottomMargin(0.15);

        
    for(int k=0; k<2; ++k){
        
        TString pdfName(k==0 ? "IDTrackingEnergyLoss_NoZ0SinThetaCut" : "IDTrackingEnergyLoss_WithZ0SinThetaCut");
        auto InDetEnergyLossCorr_EtaVsPt_Array = k==0 ? mIDTrackingEnergyLoss_NoZ0SinThetaCut : mIDTrackingEnergyLoss_WithZ0SinThetaCut;
        
        c->Print(dirName+pdfName+".pdf["/*, "EmbedFonts"*/);
        for(int i=0; i<CepUtil::nDefinedParticles; ++i){
            for(int j=0; j<CepUtil::nSigns; ++j){
                
                TH2D * InDetEnergyLossCorr_EtaVsPt = new TH2D(*InDetEnergyLossCorr_EtaVsPt_Array[i][j]);
                InDetEnergyLossCorr_EtaVsPt->Scale(1e3); // GeV->MeV
                
                InDetEnergyLossCorr_EtaVsPt->GetXaxis()->SetLabelOffset(1.5*InDetEnergyLossCorr_EtaVsPt->GetXaxis()->GetLabelOffset());
                InDetEnergyLossCorr_EtaVsPt->GetXaxis()->SetTitle("p_{T,reco} [GeV]");
                InDetEnergyLossCorr_EtaVsPt->GetXaxis()->SetRangeUser(0.1, 2);
                InDetEnergyLossCorr_EtaVsPt->GetXaxis()->SetNdivisions(5,5,1);
                
                InDetEnergyLossCorr_EtaVsPt->GetYaxis()->SetLabelOffset(1.5*InDetEnergyLossCorr_EtaVsPt->GetXaxis()->GetLabelOffset());
                InDetEnergyLossCorr_EtaVsPt->GetYaxis()->SetTitle("#eta_{reco}");
                InDetEnergyLossCorr_EtaVsPt->GetYaxis()->SetNdivisions(5,5,1);
                
                InDetEnergyLossCorr_EtaVsPt->GetZaxis()->SetLabelOffset(1.5*InDetEnergyLossCorr_EtaVsPt->GetXaxis()->GetLabelOffset());
                InDetEnergyLossCorr_EtaVsPt->GetZaxis()->SetTitle("#LTp_{T,reco}-p_{T,true}#GT [MeV]");
                InDetEnergyLossCorr_EtaVsPt->GetZaxis()->SetRangeUser(-99, 0);
                
                InDetEnergyLossCorr_EtaVsPt->GetXaxis()->SetTitleOffset(1.2);
//                 InDetEnergyLossCorr_EtaVsPt->GetXaxis()->SetTitleSize(InDetEnergyLossCorr_EtaVsPt->GetXaxis()->GetTitleSize());
                InDetEnergyLossCorr_EtaVsPt->GetXaxis()->SetLabelOffset(InDetEnergyLossCorr_EtaVsPt->GetXaxis()->GetLabelOffset());
//                 InDetEnergyLossCorr_EtaVsPt->GetXaxis()->SetLabelSize(InDetEnergyLossCorr_EtaVsPt->GetXaxis()->GetLabelSize());
                
                InDetEnergyLossCorr_EtaVsPt->GetYaxis()->SetTitleOffset(0.85);
//                 InDetEnergyLossCorr_EtaVsPt->GetYaxis()->SetTitleSize(InDetEnergyLossCorr_EtaVsPt->GetYaxis()->GetTitleSize());
                InDetEnergyLossCorr_EtaVsPt->GetYaxis()->SetLabelOffset(InDetEnergyLossCorr_EtaVsPt->GetYaxis()->GetLabelOffset());
//                 InDetEnergyLossCorr_EtaVsPt->GetYaxis()->SetLabelSize(InDetEnergyLossCorr_EtaVsPt->GetYaxis()->GetLabelSize());
                
                InDetEnergyLossCorr_EtaVsPt->GetZaxis()->SetTitleOffset(1.3);
//                 InDetEnergyLossCorr_EtaVsPt->GetZaxis()->SetTitleSize(InDetEnergyLossCorr_EtaVsPt->GetZaxis()->GetTitleSize());
                InDetEnergyLossCorr_EtaVsPt->GetZaxis()->SetLabelOffset(0.3*InDetEnergyLossCorr_EtaVsPt->GetZaxis()->GetLabelOffset());
//                 InDetEnergyLossCorr_EtaVsPt->GetZaxis()->SetLabelSize(InDetEnergyLossCorr_EtaVsPt->GetZaxis()->GetLabelSize());
                InDetEnergyLossCorr_EtaVsPt->GetZaxis()->SetNdivisions(5,2,1);
                
                InDetEnergyLossCorr_EtaVsPt->Draw("colz");
                
                gPad->Update();
                TPaletteAxis *palette = (TPaletteAxis*)InDetEnergyLossCorr_EtaVsPt->GetListOfFunctions()->FindObject("palette");
                if(palette){
                    palette->SetX1NDC(0.835);
                    palette->SetX2NDC(0.865);
                    palette->SetY1NDC(0.15);
                    palette->SetY2NDC(0.95);
                }
                gPad->Modified();
                
                TLatex l; l.SetNDC(); l.SetTextFont(72); l.SetTextSize(.05); l.SetTextColor(kBlack);
                l.DrawLatex(.3, .85, "ATLAS #font[42]{Simulation Internal}");
                l.DrawLatex(.45, .78, "#font[42]{#sqrt{s} = 13 TeV}");
                l.DrawLatex(.45, .71, mCepUtil->mParticleSymbolCh[i][j]);
                
                c->Print(dirName+pdfName+".pdf"/*, "EmbedFonts"*/);   
            }
        }
        c->Print(dirName+pdfName+".pdf]"/*, "EmbedFonts"*/);
    
    }
    
    
    
    
    
    setupDrawingStyle();
}



void CepAnalysis::drawDataMcComparison2(TH1F* hMainInput, TH1F* hMainInput2, DrawingOptions & opt, bool drawRatio){
  
  TH1F *hMain = new TH1F( *hMainInput );
  hMain->SetName("drawDataMcComparison2_"+TString(hMain->GetName()));
  TH1F *hMain2 = new TH1F( *hMainInput2 );
  hMain2->SetName("drawDataMcComparison2_"+TString(hMain2->GetName()));
  
  TGaxis::SetMaxDigits(3);
  
    double mRatioFactor = 0.25;
  double mRatioYmin = 0.81;
  double mRatioYmax = 1.19;
  double mRatioYTitleOffsetFactor = 1.0;
  TString mRatioYTitle = TString("Rel. 21 / Rel. 20");
  
  TCanvas *c = 0;
  TCanvas *c2 = 0;
  TPad *pad1 = 0;
  TPad *pad2 = 0;
  

      c = new TCanvas("cccccc", "cccccc", opt.mCanvasWidth, opt.mCanvasHeight);
      //   c->SetFrameFillStyle(0);
      //   c->SetFrameLineWidth(2);
      //   c->SetFillColor(-1);
      c->SetLeftMargin(opt.mLeftMargin);
      c->SetRightMargin(opt.mRightMargin);
      c->SetTopMargin(opt.mTopMargin);
      c->SetBottomMargin(opt.mBottomMargin);
      
      c->SetLogy(opt.mYlog);
  
  
  if(opt.mScale){
    hMain->Scale( opt.mScalingFactor );
    hMain2->Scale( opt.mScalingFactor );
  }
  
  hMain->SetMarkerStyle(opt.mMarkerStyle);
  hMain->SetMarkerSize(opt.mMarkerSize);
  hMain->SetMarkerColor(opt.mMarkerColor);
  hMain->SetLineStyle(opt.mLineStyle);
  hMain->SetLineColor(opt.mLineColor);
  hMain->SetLineWidth(opt.mLineWidth);
  
  hMain2->SetMarkerStyle(opt.mMarkerStyle2);
  hMain2->SetMarkerSize(opt.mMarkerSize2);
  hMain2->SetMarkerColor(opt.mMarkerColor2);
  hMain2->SetLineStyle(opt.mLineStyle);
  hMain2->SetLineColor(opt.mLineColor2);
  hMain2->SetLineWidth(opt.mLineWidth2);
  
  hMain->GetXaxis()->SetTitle(opt.mXaxisTitle);
  hMain->GetXaxis()->SetTitle(opt.mXaxisTitle);
  hMain->GetXaxis()->SetTitleSize(opt.mXaxisTitleSize);
  hMain->GetXaxis()->SetTitleOffset(opt.mXaxisTitleOffset);
  hMain->GetXaxis()->SetLabelSize(opt.mXaxisLabelSize);
  hMain->GetXaxis()->SetLabelOffset(opt.mXaxisLabelOffset);
  hMain->GetXaxis()->SetTickLength(opt.mXaxisTickLength);
  hMain->GetXaxis()->SetRangeUser(opt.mXmin, opt.mXmax);
  
  hMain2->GetXaxis()->SetTitle(opt.mXaxisTitle);
  hMain2->GetXaxis()->SetTitle(opt.mXaxisTitle);
  hMain2->GetXaxis()->SetTitleSize(opt.mXaxisTitleSize);
  hMain2->GetXaxis()->SetTitleOffset(opt.mXaxisTitleOffset);
  hMain2->GetXaxis()->SetLabelSize(opt.mXaxisLabelSize);
  hMain2->GetXaxis()->SetLabelOffset(opt.mXaxisLabelOffset);
  hMain2->GetXaxis()->SetTickLength(opt.mXaxisTickLength);
  hMain2->GetXaxis()->SetRangeUser(opt.mXmin, opt.mXmax);
  
  if( opt.mSetDivisionsX )
    hMain->GetXaxis()->SetNdivisions( opt.mXnDivisionA, opt.mXnDivisionB, opt.mXnDivisionC, !opt.mForceDivisionsX );
  
  hMain->GetYaxis()->SetTitleSize(opt.mYaxisTitleSize);
  hMain->GetYaxis()->SetTitle(opt.mYaxisTitle);
  hMain->GetYaxis()->SetTitleOffset(opt.mYaxisTitleOffset);
  hMain->GetYaxis()->SetLabelSize(opt.mYaxisLabelSize);
  hMain->GetYaxis()->SetLabelOffset(opt.mYaxisLabelOffset);
  hMain->GetYaxis()->SetTickLength(opt.mYaxisTickLength);
  hMain->GetYaxis()->SetRangeUser(opt.mYmin, (opt.mScale ? opt.mScalingFactor : 1.0) * opt.mYmax);
  
  hMain2->GetYaxis()->SetTitleSize(opt.mYaxisTitleSize);
  hMain2->GetYaxis()->SetTitle(opt.mYaxisTitle);
  hMain2->GetYaxis()->SetTitleOffset(opt.mYaxisTitleOffset);
  hMain2->GetYaxis()->SetLabelSize(opt.mYaxisLabelSize);
  hMain2->GetYaxis()->SetLabelOffset(opt.mYaxisLabelOffset);
  hMain2->GetYaxis()->SetTickLength(opt.mYaxisTickLength);
  hMain2->GetYaxis()->SetRangeUser(opt.mYmin, (opt.mScale ? opt.mScalingFactor : 1.0) * opt.mYmax);
  
  if( opt.mSetDivisionsY ){
    hMain->GetYaxis()->SetNdivisions( opt.mYnDivisionA, opt.mYnDivisionB, opt.mYnDivisionC, !opt.mForceDivisionsY );
    hMain2->GetYaxis()->SetNdivisions( opt.mYnDivisionA, opt.mYnDivisionB, opt.mYnDivisionC, !opt.mForceDivisionsY );
  }
  
  hMain->Draw("PE"/*"PEX0"*/);
  hMain2->Draw("PE SAME");
  
  for(unsigned int i=0; i<opt.mHistTH1F.size(); ++i){
    if(opt.mScale){
      opt.mHistTH1F[i]->Scale( opt.mScalingFactor );
    }
    opt.mHistTH1F[i]->GetXaxis()->SetRangeUser(opt.mXmin, opt.mXmax);
    opt.mHistTH1F[i]->Draw( opt.mHistTH1F_DrawingOpt[i] );
  }
  
  
//   if( opt.mHistTH1_stack.size() > 0 ){
//     THStack *histStack = new THStack("histStack", "");
//     for(unsigned int i=0; i<opt.mHistTH1_stack.size(); ++i){
//       opt.mHistTH1_stack[i]->GetXaxis()->SetRangeUser(opt.mXmin, opt.mXmax);
//       histStack->Add( opt.mHistTH1_stack[i], opt.mHistTH1_stack_DrawingOpt[i] );
//     }
//     histStack->Draw("SAME");
//   }
  
  for(unsigned int i=0; i<opt.mObjToDraw.size(); ++i){
    if( opt.mObjToDrawOption[i]=="" )
      opt.mObjToDraw[i]->Draw();
    else
      opt.mObjToDraw[i]->Draw( opt.mObjToDrawOption[i] );
  }
  
    
  if( opt.mTextToDraw.size() > 0 ){
    for(unsigned int i=0; i<opt.mTextToDraw.size(); ++i){
      opt.mTextToDraw[i].SetNDC(kTRUE);
      opt.mTextToDraw[i].Draw();
    }
  }
  
//   if( opt.mTextToDraw_NotNDC.size() > 0 ){
//     for(unsigned int i=0; i<opt.mTextToDraw_NotNDC.size(); ++i){
//       opt.mTextToDraw_NotNDC[i].SetNDC(kFALSE);
//       opt.mTextToDraw_NotNDC[i].Draw();
//     }
//   }
  
  
  hMain->DrawCopy("PE SAME"/*"PEX0 SAME"*/, "");
  hMain2->DrawCopy("PE SAME"/*"PEX0 SAME"*/, "");
  gPad->RedrawAxis();
  
  TLegend *legend = new TLegend( opt.mLegendX, opt.mLegendY, opt.mLegendX+opt.mLegendXwidth, opt.mLegendY+opt.mLegendYwidth );
  legend->SetNColumns(opt.mNColumns);
  legend->SetBorderSize(0);  
  legend->SetTextSize(opt.mLegendTextSize);
  legend->AddEntry(hMain, opt.mDataLegendText_1==TString("") ? "Data (opposite-sign)" : opt.mDataLegendText_1, "pel"/*"pe"*/);
  legend->AddEntry(hMain2, opt.mDataLegendText_2==TString("") ? "Data (same-sign)" : opt.mDataLegendText_2, "pel");
  for(unsigned int i=0; i<opt.mObjForLegend.size(); ++i)
    legend->AddEntry( opt.mObjForLegend[i], opt.mDesriptionForLegend[i], opt.mDrawTypeForLegend[i]);
  if( opt.mSetLegendMargin )
    legend->SetMargin( opt.mLegendMargin );
  else
    legend->SetMargin(0.3 * opt.mLegendXwidth * legend->GetNRows() / legend->GetNColumns() );
  if( opt.mFillLegendWithColor )
      legend->SetFillColorAlpha( kWhite, 0.8);
  else
      legend->SetFillStyle(0);
  legend->Draw();
  

  
  
//   if( opt.mInsert ){
//     TPad *insert = new TPad("insert", "insert", opt.mX1Insert, opt.mY1Insert, opt.mX2Insert, opt.mY2Insert);
//     insert->SetBottomMargin(0); // Upper and lower plot are joined
//     insert->SetFrameFillStyle(0);
//     insert->SetFrameLineWidth(2);
//     insert->SetFillColor(-1);
//     insert->Draw();             // Draw the upper pad: insert
//     insert->cd();               // insert becomes the current pad
//     insert->SetLeftMargin(opt.mLeftMargin);
//     insert->SetRightMargin(opt.mRightMargin);
//     insert->SetTopMargin(opt.mTopMargin);
//     insert->SetBottomMargin(opt.mBottomMargin);
//     
//     TH1F *hMainCopy = new TH1F( *hMain );
//     TH1F *hMainCopy2 = new TH1F( *hMain2 );
//     hMainCopy->GetXaxis()->SetTitle("");
//     hMainCopy->GetXaxis()->SetLabelSize( 0.9*hMainCopy->GetXaxis()->GetLabelSize() * ( 1. - opt.mLeftMargin - opt.mRightMargin) / (opt.mX2Insert - opt.mX1Insert) );
//     hMainCopy->GetYaxis()->SetLabelSize( 0.9*hMainCopy->GetYaxis()->GetLabelSize() * ( 1. - opt.mLeftMargin - opt.mRightMargin) / (opt.mX2Insert - opt.mX1Insert) );
//     hMainCopy->GetYaxis()->SetTitle("");
//     hMainCopy->GetXaxis()->SetRange( hMainCopy->FindBin(opt.mXMinInstert ), hMainCopy->FindBin(opt.mXMaxInstert ) );
//     int binMaxY;
//     binMaxY = hMainCopy->GetMaximumBin();
//     hMainCopy->GetYaxis()->SetRangeUser(0.01, opt.mYMaxInstert<0 ? (1.1*hMainCopy->GetBinContent( binMaxY )) : opt.mYMaxInstert);
//     
//     int n1 = hMainCopy->GetYaxis()->GetNdivisions()%100;
//     int n2 = hMainCopy->GetYaxis()->GetNdivisions()%10000 - n1;
//     int n3 = hMainCopy->GetYaxis()->GetNdivisions() - 100*n2 - n1;
//     
//     hMainCopy->GetYaxis()->SetNdivisions( n1/2, 2, 1 );
//     hMainCopy->GetXaxis()->SetRangeUser(opt.mXMinInstert, opt.mXMaxInstert);
//     hMainCopy->GetXaxis()->SetNdivisions(10,5,1);
//     
//     hMainCopy->GetXaxis()->SetTickLength(0.04);
//     hMainCopy->GetYaxis()->SetTickLength(0.02);
//     
//     hMainCopy->Draw("PEX0");
//     hMainCopy2->Draw("PEX0 SAME");
//     for(unsigned int i=0; i<opt.mHistTH1F.size(); ++i){
//       if(opt.mScale){
//         opt.mHistTH1F[i]->Scale( opt.mScalingFactor );
//       }
//       opt.mHistTH1F[i]->DrawCopy( opt.mHistTH1F_DrawingOpt[i], "" );
//     }
//     
//     hMainCopy->DrawCopy("PEX0 SAME", "");
//     hMainCopy2->DrawCopy("PEX0 SAME", "");
//     gPad->RedrawAxis();
//   }
  
  c->cd();
  c->Modified();
  c->Update();
  
  if(!drawRatio){
    c->Print(opt.mPdfName+".pdf");
  } else{
      c2 = new TCanvas("cccccc2", "cccccc2", opt.mCanvasWidth, (mRatioFactor + 1.0)*opt.mCanvasHeight);
      //   c->SetFrameFillStyle(0);
      //   c->SetFrameLineWidth(2);
      //   c->SetFillColor(-1);
      c2->SetLeftMargin(0);
      c2->SetRightMargin(0);
      c2->SetTopMargin(0);
      c2->SetBottomMargin(0);
      
      // Upper plot will be in pad1
      pad1 = new TPad("pad1", "pad1", 0, mRatioFactor, 1.0, 1.0);
      pad1->SetBottomMargin(0); // Upper and lower plot are joined
      pad1->SetFrameFillStyle(0);
      pad1->SetFillColor(-1);
      pad1->Draw();             // Draw the upper pad: pad1
      pad1->cd();               // pad1 becomes the current pad
      gPad->SetLeftMargin(0);
      gPad->SetRightMargin(0);
      gPad->SetTopMargin(0);
      gPad->SetBottomMargin(0);
      
      TH1F *ratio_Data_to_Data = new TH1F( *hMain2 );
      hMain->GetXaxis()->SetLabelSize( 0 );
      hMain->GetXaxis()->SetTitle("");
      ratio_Data_to_Data->Divide(hMain);
      c->Modified();
      c->Update();
      
      c->DrawClonePad();
      
      const double modifiedRatioFactor = mRatioFactor + 0.54*opt.mBottomMargin;
      
      c2->cd();          // Go back to the main canvas before defining pad2
      pad2 = new TPad("pad2", "pad2", 0, 0, 1.0, mRatioFactor + 0.65*opt.mBottomMargin);
      pad2->SetFrameFillStyle(0);
      pad2->SetFillColor(-1);
      //   gStyle->SetGridColor(kGray);
      pad2->SetGridy(kTRUE);
      pad2->Draw();
      pad2->cd();       // pad2 becomes the current pad
      gPad->SetLeftMargin(opt.mLeftMargin);
      gPad->SetRightMargin(opt.mRightMargin);
      gPad->SetTopMargin(0.005);
      gPad->SetBottomMargin(opt.mBottomMargin * (1.0-modifiedRatioFactor)/modifiedRatioFactor);
      
      ratio_Data_to_Data->GetYaxis()->SetRangeUser(mRatioYmin, mRatioYmax);
    ratio_Data_to_Data->GetYaxis()->SetTitle(mRatioYTitle);
    ratio_Data_to_Data->GetYaxis()->SetTitleOffset( 0.56 * mRatioYTitleOffsetFactor );
    ratio_Data_to_Data->GetYaxis()->CenterTitle();
    ratio_Data_to_Data->GetYaxis()->SetNdivisions(4,4,1);
    ratio_Data_to_Data->GetYaxis()->SetTitleSize( /*1.6**/ratio_Data_to_Data->GetYaxis()->GetTitleSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
    ratio_Data_to_Data->GetYaxis()->SetLabelSize( /*1.2**/ratio_Data_to_Data->GetYaxis()->GetLabelSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
    ratio_Data_to_Data->GetXaxis()->SetLabelOffset( 0.01 );
    ratio_Data_to_Data->GetYaxis()->SetTickLength( ratio_Data_to_Data->GetYaxis()->GetTickLength() );
    ratio_Data_to_Data->GetXaxis()->SetTitleSize( /*1.4**/ratio_Data_to_Data->GetXaxis()->GetTitleSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
    ratio_Data_to_Data->GetXaxis()->SetLabelSize( /*1.4**/ratio_Data_to_Data->GetXaxis()->GetLabelSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
    ratio_Data_to_Data->GetXaxis()->SetTickLength( (1.0-modifiedRatioFactor)/modifiedRatioFactor* 1.2 *ratio_Data_to_Data->GetXaxis()->GetTickLength() );
    
      ratio_Data_to_Data->Draw("PE0");
      
      TLine *line = new TLine(opt.mXmin, 1.0, opt.mXmax, 1.0);
    line->SetLineWidth(2);
    line->SetLineStyle(7);
    line->SetLineColor(kBlack);
    line->Draw();
    
    ratio_Data_to_Data->DrawCopy("PE SAME", "");
    gPad->RedrawAxis();
    
    c2->Print(opt.mPdfName+".pdf");
  }
  
  
  
  if(c)
    delete c;
  if(c2)
    delete c2;
}





void CepAnalysis::drawResolutions(){
    
    TH2F *mhResponseMatrix_InvMass;
    TH2F *mhResponseMatrix_PairRapidity;
    TH2F *mhResponseMatrix_CosThetaCS;
    TH2F *mhResponseMatrix_PhiCS;
    TH2F *mhResponseMatrix_MandelstamTSum;
    TH2F *mhResponseMatrix_DeltaPhi;
    
    
    TFile *file = TFile::Open( "ROOT_files/CD.root" );
    mhResponseMatrix_InvMass = dynamic_cast<TH2F*>(file->Get("mhResponseMatrix_InvMass"));
    mhResponseMatrix_InvMass->SetDirectory(0);
    mhResponseMatrix_PairRapidity = dynamic_cast<TH2F*>(file->Get("mhResponseMatrix_PairRapidity"));
    mhResponseMatrix_PairRapidity->SetDirectory(0);
    mhResponseMatrix_CosThetaCS = dynamic_cast<TH2F*>(file->Get("mhResponseMatrix_CosThetaCS"));
    mhResponseMatrix_CosThetaCS->SetDirectory(0);
    mhResponseMatrix_PhiCS = dynamic_cast<TH2F*>(file->Get("mhResponseMatrix_PhiCS"));
    mhResponseMatrix_PhiCS->SetDirectory(0);
    file->Close();
    file = TFile::Open( "ROOT_files/Output_pion_DiMe_CepTree_pipi.root" );
    mhResponseMatrix_MandelstamTSum = dynamic_cast<TH2F*>(file->Get("mhResponseMatrix_MandelstamTSum"));
    mhResponseMatrix_MandelstamTSum->SetDirectory(0);
    mhResponseMatrix_DeltaPhi = dynamic_cast<TH2F*>(file->Get("mhResponseMatrix_DeltaPhi"));
    mhResponseMatrix_DeltaPhi->SetDirectory(0);
    file->Close();
    
    
    
    
    gStyle->SetPalette(62);
    TColor::InvertPalette();
    TLatex lATLAS; lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.075);
    
    for(int h=0; h<6; ++h){
        
        TCanvas *canvasRespMtrx = new TCanvas("canvasRespMtrx", "canvasRespMtrx", 800, 800);
        
        double leftMargin_canvasRespMtrx = 0.15;
        double rightMargin_canvasRespMtrx = 0.18;
        double topMargin_canvasRespMtrx = 0.06;
        double bottomMargin_canvasRespMtrx = 0.15;
        //   TGaxis::SetMaxDigits(3);
        canvasRespMtrx->SetFillStyle(4000);
        canvasRespMtrx->SetFillColor(0);
        canvasRespMtrx->SetFrameFillStyle(4000);
        canvasRespMtrx->SetLeftMargin(leftMargin_canvasRespMtrx);
        canvasRespMtrx->SetRightMargin(rightMargin_canvasRespMtrx);
        canvasRespMtrx->SetTopMargin(topMargin_canvasRespMtrx);
        canvasRespMtrx->SetBottomMargin(bottomMargin_canvasRespMtrx);
        
        TH2F *hist = nullptr;
        TString fileName;
        double axisMin;
        double axisMax;
        TString xAxisLabel;
        TString yAxisLabel;
        switch( h ){
            case 0: hist = mhResponseMatrix_InvMass;
            fileName = "ResponseMatrix_InvMass.pdf";
            axisMin = hist->GetXaxis()->GetXmin();
            axisMax = 3.0;
            yAxisLabel = TString("m_{true}(#pi^{+}#pi^{-}) [GeV]");
            xAxisLabel = TString("m_{reco}(#pi^{+}#pi^{-}) [GeV]");
            break;
            case 1: hist = mhResponseMatrix_PairRapidity;
            fileName = "ResponseMatrix_PairRapidity.pdf";
            axisMin = hist->GetXaxis()->GetXmin();
            axisMax = hist->GetXaxis()->GetXmax();
            yAxisLabel = TString("y_{true}(#pi^{+}#pi^{-})");
            xAxisLabel = TString("y_{reco}(#pi^{+}#pi^{-})");
            break;
            case 2: hist = mhResponseMatrix_MandelstamTSum;
            fileName = "ResponseMatrix_MandelstamTSum.pdf";
            axisMin = 0.05;
            axisMax = hist->GetXaxis()->GetXmax();
            yAxisLabel = TString("|t_{#kern[-0.2]{#lower[-0.38]{1}}}+t_{#lower[-0.15]{2}}|_{true} [GeV^{2}]");
            xAxisLabel = TString("|t_{#kern[-0.2]{#lower[-0.38]{1}}}+t_{#lower[-0.15]{2}}|_{reco} [GeV^{2}]");
            break;
            case 3: hist = mhResponseMatrix_DeltaPhi;
            fileName = "ResponseMatrix_DeltaPhi.pdf";
            axisMin = 0;
            axisMax = 180;
            yAxisLabel = TString("#Delta#varphi_{true} [deg]");
            xAxisLabel = TString("#Delta#varphi_{reco} [deg]");
            break;
            case 4: hist = mhResponseMatrix_CosThetaCS;
            fileName = "ResponseMatrix_CosThetaCS.pdf";
            axisMin = -1;
            axisMax = 1;
            yAxisLabel = TString("cos#theta^{CS}_{true}");
            xAxisLabel = TString("cos#theta^{CS}_{reco}");
            break;
            case 5: hist = mhResponseMatrix_PhiCS;
            fileName = "ResponseMatrix_PhiCS.pdf";
            axisMin = -180;
            axisMax = 180;
            yAxisLabel = TString("#phi^{CS}_{true} [deg]");
            xAxisLabel = TString("#phi^{CS}_{reco} [deg]");
            break;
        };
        
        hist->GetXaxis()->SetLabelSize(1.5*hist->GetXaxis()->GetLabelSize());
        hist->GetXaxis()->SetTitleSize(1.7*hist->GetXaxis()->GetTitleSize());
        hist->GetYaxis()->SetLabelSize(1.5*hist->GetYaxis()->GetLabelSize());
        hist->GetYaxis()->SetTitleSize(1.7*hist->GetYaxis()->GetTitleSize());
        hist->GetZaxis()->SetLabelSize(1.5*hist->GetZaxis()->GetLabelSize());
        hist->GetZaxis()->SetTitleSize(1.7*hist->GetZaxis()->GetTitleSize());
        
        hist->GetYaxis()->SetTitle( yAxisLabel );
        hist->GetYaxis()->CenterTitle();
        hist->GetXaxis()->SetTitleOffset(1.03);
        hist->GetXaxis()->SetTitle( xAxisLabel );
        hist->GetXaxis()->CenterTitle();
        hist->GetYaxis()->SetTitleOffset(1.15);
        
        hist->GetXaxis()->SetRangeUser(axisMin, axisMax);
        hist->GetYaxis()->SetRangeUser(axisMin, axisMax);
        hist->GetXaxis()->SetNdivisions(5,5,1);
        hist->GetYaxis()->SetNdivisions(5,5,1);
        if( h==3 || h==5){
            hist->GetXaxis()->SetNdivisions(6,3,1, kFALSE);
            hist->GetYaxis()->SetNdivisions(6,3,1, kFALSE);
        }
        
        hist->GetZaxis()->SetTitle( "Probability" );
        hist->GetZaxis()->SetTitleOffset(1.1);
        
        for(int j=1; j<=hist->GetNbinsY(); ++j){
            double sum = 0;
            for(int i=1; i<=hist->GetNbinsX(); ++i)
                sum += hist->GetBinContent( i, j );
            if( sum>0 )
                for(int i=1; i<=hist->GetNbinsX(); ++i)
                    hist->SetBinContent( i, j, hist->GetBinContent(i, j)/sum );
        }
        
        hist->Draw("colz");
        hist->GetZaxis()->SetRangeUser(0, 1);
        
        
        lATLAS.DrawLatex(0.203, 0.82, "#scale[0.6]{#font[42]{#sqrt{s} = 13 TeV}}");
        
        canvasRespMtrx->Print( "PDF/"+fileName );
        
        //     TH2D *resMtrx = new TH2D();
        //     hist->Copy( *resMtrx );
        //     resMtrx->SetName( fileName );
        //     resMtrx->SetDirectory( file );
    }
    
}




void CepAnalysis::drawRel21VsRel20(){
    //------------
    TFile *filesForComparison[2];
//     filesForComparison[0] = TFile::Open( "ROOT_files/forComparisons/hist-user.ladamczy.data15_13TeV.00282248.physics_MinBias.recon.AOD.r9014.r3_EXT5.root" );
//     filesForComparison[1] = TFile::Open( "ROOT_files/forComparisons/hist-user.ladamczy.data15_13TeV.00282248.physics_MinBias.recon.DAOD.r10171_V4_EXT7.root" );
    filesForComparison[0] = TFile::Open( "ROOT_files/forComparisons_fraction/hist-user.ladamczy.data15_13TeV.00282248.physics_MinBias.recon.AOD.r9014.r3_EXT5.root" );
    filesForComparison[1] = TFile::Open( "ROOT_files/forComparisons_fraction/hist-user.ladamczy.data15_13TeV.00282248.physics_MinBias.recon.DAOD.r10171_V4_EXT7.root" );
    //------------
    fileId = 0;
    readdir(filesForComparison[0]);
    fileId = 1;
    readdir(filesForComparison[1]);
    
    std::cout << vecTH1[0].size() << " " << vecTH1[1].size() << std::endl;
    assert(vecTH1[0].size()==vecTH1[1].size());
    assert(vecTH1[0].size()>1);
    const double scalingRatio = vecTH1[1][vecTH1[1].size()-2]->GetBinContent(1) / vecTH1[0][vecTH1[0].size()-2]->GetBinContent(1);
    
    std::cout << scalingRatio << std::endl;
    const unsigned int nloops = vecTH1[1].size();
    
    
    for(unsigned int k=0; k<nloops; ++k){
        
        if( vecTH1[0][k]->GetEntries()==0 )
            continue;
        
        vecTH1[1][k]->Scale(1./scalingRatio);
        
        DrawingOptions opts;
        
        opts.mCanvasWidth = 800;
        opts.mCanvasHeight = 500;
        
        opts.mLeftMargin = 0.12;
        opts.mRightMargin = 0.035;
        opts.mTopMargin = 0.06;
        opts.mBottomMargin = 0.13;
        
        //x axis
        opts.mXmin = vecTH1[0][k]->GetXaxis()->GetXmin();
        opts.mXmax = vecTH1[0][k]->GetXaxis()->GetXmax();
        opts.mXaxisTitle = TString(vecTH1[0][k]->GetTitle());
        opts.mXaxisTitleSize = 0.05;
        opts.mXaxisTitleOffset = 1.04;
        opts.mXaxisLabelSize = 0.05;
        opts.mXaxisLabelOffset = 0.007;
        opts.mXaxisTickLength = 0.015;
        
        opts.mSetDivisionsX = kTRUE;
        //     opt.mForceDivisionsX = kTRUE;
        opts.mXnDivisionA = 5;
        opts.mXnDivisionB = 5;
        opts.mXnDivisionC = 1;
        
        opts.mSetDivisionsY = kTRUE;
        //     opt.mForceDivisionsY = kTRUE;
        opts.mYnDivisionA = 5;
        opts.mYnDivisionB = 5;
        opts.mYnDivisionC = 1;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 1.15*max(vecTH1[0][k]->GetMaximum(), vecTH1[1][k]->GetMaximum());
        opts.mYaxisTitle = TString( Form("Events / %g", vecTH1[0][k]->GetXaxis()->GetBinWidth(1) ) );
        opts.mYaxisTitleSize = 0.05;
        opts.mYaxisTitleOffset = 1.25;
        opts.mYaxisLabelSize = 0.05;
        opts.mYaxisLabelOffset = 0.007;
        opts.mYaxisTickLength = 0.015;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.6;
        
        opts.mMarkerStyle2 = 25;
        opts.mMarkerColor2 = kRed;
        opts.mMarkerSize2 = 1.6;
        
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mLineStyle2 = 1;
        opts.mLineColor2 = kRed;
        opts.mLineWidth2 = 2;
        
        opts.mPdfName = TString("pdf/")+TString(vecTH1[0][k]->GetName());
        
        opts.mDataLegendText_1 = TString("Rel. 20");
        opts.mDataLegendText_2 = TString("Rel. 21");
        
        opts.mNColumns = 1;
        opts.mLegendX = 0.45;
        opts.mLegendXwidth = 0.5;
        opts.mLegendY = 0.8;
        opts.mLegendYwidth = 0.14;
        opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        opts.mSetLegendMargin = kTRUE;
        opts.mLegendMargin = 0.11;
//         if(cepChannel>1)
//             opts.mFillLegendWithColor = kTRUE;
        
        TLatex lATLAS(.76, .88, "#font[72]{ATLAS} Internal"); lATLAS.SetNDC(); lATLAS.SetTextFont(42);  lATLAS.SetTextSize(0.8 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lATLAS );
        
        drawDataMcComparison2( vecTH1[0][k], vecTH1[1][k], opts, true );
    }
}



void CepAnalysis::pullAnalysis(TFile *file){
    
        
    const TString dirName("PDF_Pull/");
    mkdir(dirName.Data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    
    
    CepHistograms_EventFlow * hEF = new CepHistograms_EventFlow("EventFlow", false, file);
    
    vector<TH2F*> input_unweighted;
    vector<TH2F*> input_weighted;
    vector<TString> input_name;
    vector<TString> latex_name;
    

    for(int i=0; i<CepUtil::nCepChannels; ++i){
        if( i!=CepUtil::PIPI && i!=CepUtil::FOURPI) continue; //ALERT
        
        input_unweighted.push_back( new TH2F( *hEF->mhNCepEventsVsLumiBlockVsRunNumber[i][CepUtil::ELA] ) );
        input_weighted.push_back( new TH2F( *hEF->mhNCepEventsVsLumiBlockVsRunNumber_Weighted[i][CepUtil::ELA] ) );
        input_name.push_back( mCepUtil->mCepChannelName[i]+"_ELA" );
        latex_name.push_back( mCepUtil->mCepChannelLatexName[i] );
        
        input_unweighted.push_back( new TH2F( *hEF->mhNCepEventsVsLumiBlockVsRunNumber[i][CepUtil::INE] ) );
        input_weighted.push_back( new TH2F( *hEF->mhNCepEventsVsLumiBlockVsRunNumber_Weighted[i][CepUtil::INE] ) );
        input_name.push_back( mCepUtil->mCepChannelName[i]+"_INE" );
        latex_name.push_back( mCepUtil->mCepChannelLatexName[i] );
        
        input_unweighted.push_back( new TH2F( *hEF->mhNCepEventsVsLumiBlockVsRunNumber[i][CepUtil::ALL] ) );
        input_weighted.push_back( new TH2F( *hEF->mhNCepEventsVsLumiBlockVsRunNumber_Weighted[i][CepUtil::ALL] ) );
        input_name.push_back( mCepUtil->mCepChannelName[i]+"_ALL" );
        latex_name.push_back( mCepUtil->mCepChannelLatexName[i] );
    }
    
    
    
    //----- drawing event flow per LB ---
    
    std::vector<std::pair<int,int>> badLBsVec;
//     {
//         TCanvas *c = new TCanvas("c44", "c44", 800, 600);
//         c->SetFrameFillStyle(0);
//         TGaxis::SetMaxDigits(3);
//         c->SetFillColor(-1);
//         c->SetLeftMargin(0.13);
//         c->SetRightMargin(0.025);
//         c->SetTopMargin(0.02);
//         c->SetBottomMargin(0.14);
//         
//         for(int i=1; i<=hEF->mhCountsVsLumiBlockVsRunNumber[CepUtil::ALL]->GetNbinsX(); ++i){
//             for(int j=1; j<=hEF->mhCountsVsLumiBlockVsRunNumber[CepUtil::ALL]->GetNbinsY(); ++j){
//                 
//                 if( hEF->mhCountsVsLumiBlockVsRunNumber[CepUtil::ALL]->GetBinContent(i,j,1)==0 ) continue;
//                 
//                 TH1D *counts = hEF->mhCountsVsLumiBlockVsRunNumber[CepUtil::ALL]->ProjectionZ(Form("pz_%d%d",i,j), i,i, j,j);
//                 
//                 
//                 counts->Scale(1./counts->GetBinContent(1.));
//                 
//                 counts->SetLineColor( ((i+j)%9)+1 );
//                 counts->GetXaxis()->SetRangeUser(0, CepUtil::kNumCuts-1);
//                 counts->GetYaxis()->SetRangeUser(0.4, 0.7);
//                 counts->Draw("HIST SAME");
//                 
//                 if( counts->GetBinContent(2)<0.55 ){ // potential ALFA problems
//                     int run = static_cast<int>(hEF->mhCountsVsLumiBlockVsRunNumber[CepUtil::ALL]->GetXaxis()->GetBinCenter(i));
//                     int lb = static_cast<int>(hEF->mhCountsVsLumiBlockVsRunNumber[CepUtil::ALL]->GetYaxis()->GetBinCenter(j));
//                     std::cout << run << " " << lb << std::endl;
//                     badLBsVec.push_back( std::pair<int,int>(run,lb) );
//                 }
//                 
//             }
//         }
//         
// //         c->SetLogy();
// //         c->Print("TEST.pdf");
//     }
    
    delete hEF;
    
    /////////////////////////////
    
    for(unsigned int in=0; in<input_name.size(); ++in){
        
        
        vector<double> runFinish;
        vector<int> runNumVec;
        
    //     TH1F* hNExclusiveEventsPerLumi = new TH1F( "NExclusiveEventsPerLumi", "Number of exclusive events per integrated luminosity", 125, 0, 25000 );
        TH1F* hNExclusiveEventsPerLumi_Pull = new TH1F( Form("NExclusiveEventsPerLumi_Pull_%d", in), "Pull of number of exclusive events per integrated luminosity", 200, -15, 15 );
    //     TH1F* hNExclusiveEventsWeightedPerLumi = new TH1F( "NExclusiveEventsWeightedPerLumi", "Number of exclusive events (weighted) per integrated luminosity", 500, 0, 10000000 );
        TH1F* hNExclusiveEventsWeightedPerLumi_Pull = new TH1F( Form("NExclusiveEventsWeightedPerLumi_Pull_%d", in), "Pull of number of exclusive events per integrated luminosity", 200, -15, 15 );
        
        TH1F* pullVsLB_unweighted;
        TH1F* pullVsLB_weighted;
        int nLumiBlocks = 0;
        
        TH2F *hIn_Unw = input_unweighted[in];
        TH2F *hIn_W = input_weighted[in];
        
        
        double totalLumi = 0;
        double totalNExclusiveEvents = 0;
        double totalNExclusiveEventsWeighted = 0;
        
        double averageNExclusiveEventsPerLumi = 0;
        double averageNExclusiveEventsPerLumiWeighted = 0;
        double sumOfWeightsPerLumi = 0;
        double sumOfWeightsPerLumiWeighted = 0;
        
        const double avrgBkgdFrac = 0.05; //ALERT
        
        hIn_Unw->SetBinErrorOption(TH1::kPoisson);
        hIn_W->SetBinErrorOption(TH1::kPoisson);
        
        for(int i=1; i <= mhNEventsVsLumiBlockVsRunNumber->GetNbinsX(); ++i){
            for(int j=1; j <= mhNEventsVsLumiBlockVsRunNumber->GetNbinsY(); ++j){
                if( mhNEventsVsLumiBlockVsRunNumber->GetBinContent(i,j)>0 && hIn_Unw->GetBinContent(i,j)>0 ){
                
                    int runNum = static_cast<int>( mhNEventsVsLumiBlockVsRunNumber->GetXaxis()->GetBinCenter(i) );
                    int lb = static_cast<int>( mhNEventsVsLumiBlockVsRunNumber->GetYaxis()->GetBinCenter(j) );
                    
                    //BEGIN checking if bad run&lb
                    bool stop = false;
                    for(unsigned int k=0; k<badLBsVec.size(); ++k){
                        if( badLBsVec[k].first == runNum && badLBsVec[k].second == lb ){
                            stop = true;/* std::cout << "ASSSAA" << std::endl;*/
                            break;
                        }
                    }
                    if(stop) continue;
                    //END END END END END END END END
                    
                    ++nLumiBlocks;
                    
                    const double integratedLumi = lumiPerLumiBlockVector_RunMap[runNum][lb] * lumiblockDurationVector_RunMap[runNum][lb] * 1.e-3;
                    double nExclPerLumi = hIn_Unw->GetBinContent(i,j) / integratedLumi;
                    double nExclWeightedPerLumi = hIn_W->GetBinContent(i,j) / integratedLumi;
                    double nExclPerLumiError = hIn_Unw->GetBinError(i,j) / integratedLumi;
                    double nExclWeightedPerLumiError = hIn_W->GetBinError(i,j) / integratedLumi;
                    
    //                 cout << hIn_Unw->GetBinContent(i,j) << endl;
                    
                    nExclPerLumi *= (1-avrgBkgdFrac);
                    nExclWeightedPerLumi *= (1-avrgBkgdFrac);
                    nExclPerLumiError *= (1+avrgBkgdFrac);
                    nExclWeightedPerLumiError *= (1+avrgBkgdFrac);
                    
                    totalLumi += integratedLumi;
                    totalNExclusiveEvents += hIn_Unw->GetBinContent(i,j);
                    totalNExclusiveEventsWeighted += hIn_W->GetBinContent(i,j);
                    
                    averageNExclusiveEventsPerLumi += nExclPerLumi/pow( nExclPerLumiError, 2);
                    sumOfWeightsPerLumi += 1./pow( nExclPerLumiError, 2);
                    averageNExclusiveEventsPerLumiWeighted += nExclWeightedPerLumi/pow( nExclWeightedPerLumiError, 2);
                    sumOfWeightsPerLumiWeighted += 1./pow( nExclWeightedPerLumiError, 2);
                    
    //                 hNExclusiveEventsPerLumi->Fill( nExclPerLumi );
    //                 hNExclusiveEventsWeightedPerLumi->Fill( nExclWeightedPerLumi );
                }
            }
        }
        
        averageNExclusiveEventsPerLumi /= sumOfWeightsPerLumi;
        averageNExclusiveEventsPerLumiWeighted /= sumOfWeightsPerLumiWeighted;
        
//         cout << " Average N = " << averageNExclusiveEventsPerLumi << "   Average Nw = " << averageNExclusiveEventsPerLumiWeighted << endl;
        
        
        
        pullVsLB_weighted = new TH1F( Form("pullVsLB_weighted_%d", in), "Pull of number of exclusive events per integrated luminosity vs. LB", nLumiBlocks, 0, nLumiBlocks );
        pullVsLB_unweighted = new TH1F( Form("pullVsLB_unweighted_%d", in), "Pull of number of exclusive events per integrated luminosity vs. LB", nLumiBlocks, 0, nLumiBlocks );
        
        
        for(int i=1, counter = 0; i <= mhNEventsVsLumiBlockVsRunNumber->GetNbinsX(); ++i){
            bool anyEvent = false;
            int runNum = static_cast<int>( mhNEventsVsLumiBlockVsRunNumber->GetXaxis()->GetBinCenter(i) );
            for(int j=1; j <= mhNEventsVsLumiBlockVsRunNumber->GetNbinsY(); ++j){
                if( mhNEventsVsLumiBlockVsRunNumber->GetBinContent(i,j)>0 && hIn_Unw->GetBinContent(i,j)>0 ){
                    
                    int lb = static_cast<int>( mhNEventsVsLumiBlockVsRunNumber->GetYaxis()->GetBinCenter(j) );
                    
                    //BEGIN checking if bad run&lb
                    bool stop = false;
                    for(unsigned int k=0; k<badLBsVec.size(); ++k){
                        if( badLBsVec[k].first == runNum && badLBsVec[k].second == lb ){
                            stop = true;
                            break;
                        }
                    }
                    if(stop) continue;
                    //END END END END END END END END
                    
                    anyEvent = true;
                    
                    const double integratedLumi = lumiPerLumiBlockVector_RunMap[runNum][lb] * lumiblockDurationVector_RunMap[runNum][lb]  * 1.e-3;
                    double nExclPerLumi = hIn_Unw->GetBinContent(i,j) / integratedLumi;
                    double nExclPerLumiError =  hIn_Unw->GetBinError(i,j) / integratedLumi;
                    double nExclPerLumiError_Up =  hIn_Unw->GetBinErrorUp(i,j) / integratedLumi;
                    double nExclPerLumiError_Down =  hIn_Unw->GetBinErrorLow(i,j) / integratedLumi;
                    double nExclWeightedPerLumi = hIn_W->GetBinContent(i,j) / integratedLumi;
                    double nExclWeightedPerLumiError =  hIn_W->GetBinError(i,j) / integratedLumi;
                    double nExclWeightedPerLumiError_Up =  hIn_W->GetBinErrorUp(i,j) / integratedLumi;
                    double nExclWeightedPerLumiError_Down =  hIn_W->GetBinErrorLow(i,j) / integratedLumi;
                    
                    nExclPerLumi *= (1-avrgBkgdFrac);
                    nExclPerLumiError *= (1+avrgBkgdFrac);
                    nExclPerLumiError_Up *= (1+avrgBkgdFrac);
                    nExclPerLumiError_Down *= (1+avrgBkgdFrac);
                    nExclWeightedPerLumi *= (1-avrgBkgdFrac);
                    nExclWeightedPerLumiError *= (1+avrgBkgdFrac);
                    nExclWeightedPerLumiError_Up *= (1+avrgBkgdFrac);
                    nExclWeightedPerLumiError_Down *= (1+avrgBkgdFrac);
                    
                    bool positive = (nExclWeightedPerLumi - averageNExclusiveEventsPerLumiWeighted) > 0;
                    bool positive_uncorrected = (nExclPerLumi - averageNExclusiveEventsPerLumi) > 0;
                    double pull = (nExclWeightedPerLumi - averageNExclusiveEventsPerLumiWeighted ) / (positive ? nExclWeightedPerLumiError_Down : nExclWeightedPerLumiError_Up );
                    double pull_uncorrected = (nExclPerLumi - averageNExclusiveEventsPerLumi ) / (positive_uncorrected ? nExclPerLumiError_Down : nExclPerLumiError_Up );
                    
//                     if(pull < -5 )
//                         cout << runNum << " LB: " << lb << "  duration: " << lumiblockDurationVector_RunMap[runNum][lb] << "   N = " << hIn_Unw->GetBinContent(i,j) <<  " +/- " << hIn_Unw->GetBinError(i,j) << "   Nw = " << hIn_W->GetBinContent(i,j) <<  " +/- " << hIn_W->GetBinError(i,j) << "    pull = " << pull_uncorrected << "   pull_corr = " << pull << endl;
                    
                    hNExclusiveEventsWeightedPerLumi_Pull->Fill( pull );
                    hNExclusiveEventsPerLumi_Pull->Fill( pull_uncorrected );
                    
                    pullVsLB_weighted->SetBinContent(++counter, pull);
                    pullVsLB_weighted->SetBinError(counter, 0);
                    pullVsLB_unweighted->SetBinContent(counter, pull_uncorrected);
                    pullVsLB_unweighted->SetBinError(counter, 0);
                    
                }
            }
            if(anyEvent){
                runFinish.push_back( counter );
                runNumVec.push_back( runNum );
            }
        }
        
        
        
        //-------------------------------------
        
        {
            TCanvas *c = new TCanvas("c", "c", 800, 800);
            c->SetFrameFillStyle(0);
            TGaxis::SetMaxDigits(3);
            c->SetFillColor(-1);
            c->SetLeftMargin(0.13);
            c->SetRightMargin(0.025);
            c->SetTopMargin(0.02);
            c->SetBottomMargin(0.14);
            
            
            TF1 *gausPull = new TF1(Form("gausPull_%d", in), "gausn", -15, 15);
            gausPull->SetLineWidth(4);
            gausPull->SetLineColor(kBlue);
            gausPull->SetNpx(1e3);
            
            TF1 *gausPull_uncorrected = new TF1(Form("gausPull_uncorrected_%d", in), "gausn", -15, 15);
            gausPull_uncorrected->SetLineWidth(4);
            gausPull_uncorrected->SetLineStyle(7);
            gausPull_uncorrected->SetLineColor(kRed);
            gausPull_uncorrected->SetNpx(1e3);
            
            hNExclusiveEventsWeightedPerLumi_Pull->Rebin(5);
            hNExclusiveEventsWeightedPerLumi_Pull->GetXaxis()->SetRangeUser(-16, 16);
            hNExclusiveEventsWeightedPerLumi_Pull->GetYaxis()->SetRangeUser(0, 1.3*hNExclusiveEventsWeightedPerLumi_Pull->GetMaximum());
            hNExclusiveEventsWeightedPerLumi_Pull->GetXaxis()->SetTitleSize(0.06);
            hNExclusiveEventsWeightedPerLumi_Pull->GetXaxis()->SetLabelSize(0.05);
            hNExclusiveEventsWeightedPerLumi_Pull->GetYaxis()->SetTitleSize(0.06);
            hNExclusiveEventsWeightedPerLumi_Pull->GetYaxis()->SetLabelSize(0.05);
            hNExclusiveEventsWeightedPerLumi_Pull->GetXaxis()->SetNdivisions(6,6,1);
            hNExclusiveEventsWeightedPerLumi_Pull->GetYaxis()->SetNdivisions(5,5,1);
            hNExclusiveEventsWeightedPerLumi_Pull->GetYaxis()->SetTitleOffset(1.08);
            hNExclusiveEventsWeightedPerLumi_Pull->SetStats(kFALSE);
            hNExclusiveEventsWeightedPerLumi_Pull->SetLineWidth(2);
            hNExclusiveEventsWeightedPerLumi_Pull->Fit(gausPull, "QNI");
            hNExclusiveEventsWeightedPerLumi_Pull->SetMarkerSize(1.6);
            hNExclusiveEventsWeightedPerLumi_Pull->SetMarkerStyle(20);
            hNExclusiveEventsWeightedPerLumi_Pull->SetMarkerColor(kBlack);
            hNExclusiveEventsWeightedPerLumi_Pull->SetLineColor(kBlack);
            hNExclusiveEventsWeightedPerLumi_Pull->GetXaxis()->SetTitle("Pull");
            hNExclusiveEventsWeightedPerLumi_Pull->GetXaxis()->SetTitleOffset(0.8);
            hNExclusiveEventsWeightedPerLumi_Pull->GetYaxis()->SetTitle(Form("Luminosity blocks / %.1f", hNExclusiveEventsWeightedPerLumi_Pull->GetXaxis()->GetBinWidth(1)));
            hNExclusiveEventsWeightedPerLumi_Pull->Draw("PE");
            
        //     hNExclusiveEventsWeightedPerLumi_Pull->GetYaxis()->SetRangeUser(0, 2000);
            
            hNExclusiveEventsPerLumi_Pull->Rebin(5);
            hNExclusiveEventsPerLumi_Pull->GetXaxis()->SetRangeUser(-16, 16);
            hNExclusiveEventsPerLumi_Pull->SetMarkerSize(1.9);
            hNExclusiveEventsPerLumi_Pull->SetMarkerStyle(24);
            hNExclusiveEventsPerLumi_Pull->SetMarkerColor(kBlack);
            hNExclusiveEventsPerLumi_Pull->SetLineColor(kBlack);
            hNExclusiveEventsPerLumi_Pull->Fit(gausPull_uncorrected, "QNI");
            hNExclusiveEventsPerLumi_Pull->Draw("PE SAME");
            
            gausPull->Draw("SAME");
            gausPull_uncorrected->Draw("SAME");
            hNExclusiveEventsWeightedPerLumi_Pull->Draw("PE SAME");
            
            TLatex lATLAS; lATLAS.SetNDC(); lATLAS.SetTextFont(42);  lATLAS.SetTextSize(.05);
            lATLAS.DrawLatex(0.16, 0.9, "#font[72]{ATLAS} Internal  #sqrt{s} = 13 TeV #beta* = 90 m");
            lATLAS.DrawLatex(0.16, 0.84, Form("p+p #rightarrow p'+%s+p'", latex_name[in].Data() ) );
            
            TLatex l; l.SetNDC(); l.SetTextFont(42); l.SetTextSize(.05);
            l.SetTextColor(kBlue);
            l.DrawLatex(0.63, 0.85, Form("#mu = %.2f #pm %.2f", gausPull->GetParameter(1), gausPull->GetParError(1)));
            l.DrawLatex(0.63, 0.80, Form("#sigma = %.2f #pm %.2f", gausPull->GetParameter(2), gausPull->GetParError(2)));
            l.SetTextColor(kRed);
            l.DrawLatex(0.63, 0.72, Form("#mu = %.2f #pm %.2f", gausPull_uncorrected->GetParameter(1), gausPull_uncorrected->GetParError(1)));
            l.DrawLatex(0.63, 0.67, Form("#sigma = %.2f #pm %.2f", gausPull_uncorrected->GetParameter(2), gausPull_uncorrected->GetParError(2)));
            
            TLegend *legend = new TLegend(0.16, 0.5, 0.5, 0.75);
            legend->SetTextSize(0.04);
            legend->SetBorderSize(0);
            legend->SetFillColor(-1);
            legend->SetFillStyle(-1);
            legend->AddEntry( hNExclusiveEventsWeightedPerLumi_Pull, "Data (corr.)", "PEL" );
            legend->AddEntry( gausPull, "Fit (corr.)", "L" );
            legend->AddEntry( hNExclusiveEventsPerLumi_Pull, "Data (uncorr.)", "PEL" );
            legend->AddEntry( gausPull_uncorrected, "Fit (uncorr.)", "L" );
            legend->Draw();
            
            
            TLatex l2; l2.SetTextFont(42); l2.SetTextSize(.03); l2.SetTextAngle(90); //l2.SetTextColor(kGreen+2);
            l2.DrawLatex(hNExclusiveEventsPerLumi_Pull->GetXaxis()->GetBinUpEdge(0), 10+hNExclusiveEventsPerLumi_Pull->GetBinContent(0), "Underflow" );
            l2.DrawLatex(hNExclusiveEventsPerLumi_Pull->GetXaxis()->GetBinUpEdge(hNExclusiveEventsPerLumi_Pull->GetNbinsX()+1), 10+hNExclusiveEventsPerLumi_Pull->GetBinContent(hNExclusiveEventsPerLumi_Pull->GetNbinsX()+1), "Overflow" );
            
            
            gPad->RedrawAxis();
            
            c->Print( dirName+"Pull_PerLB_"+input_name[in]+".pdf");
        }
        
        
        
        
        {
            TCanvas *c = new TCanvas("c", "c", 1600, 900);
            c->SetFrameFillStyle(0);
            TGaxis::SetMaxDigits(3);
            c->SetFillColor(-1);
            c->SetLeftMargin(0.11);
            c->SetRightMargin(0.08);
            c->SetTopMargin(0.02);
            c->SetBottomMargin(0.14);
            
            double pull_limit = 16;
            
//             pullVsLB_weighted->GetXaxis()->SetRangeUser(-16, 16);
            pullVsLB_weighted->GetYaxis()->SetRangeUser(-pull_limit, pull_limit);
            pullVsLB_weighted->GetXaxis()->SetTitleSize(0.06);
            pullVsLB_weighted->GetXaxis()->SetLabelSize(0.05);
            pullVsLB_weighted->GetYaxis()->SetTitleSize(0.06);
            pullVsLB_weighted->GetYaxis()->SetLabelSize(0.05);
            pullVsLB_weighted->GetXaxis()->SetNdivisions(6,6,1);
            pullVsLB_weighted->GetYaxis()->SetNdivisions(5,5,1);
            pullVsLB_weighted->GetYaxis()->SetTitleOffset(0.8);
            pullVsLB_weighted->SetStats(kFALSE);
            pullVsLB_weighted->SetLineWidth(2);
            pullVsLB_weighted->SetMarkerSize(0.7);
            pullVsLB_weighted->SetMarkerStyle(20);
            pullVsLB_weighted->SetMarkerColor(kBlue);
            pullVsLB_weighted->SetLineColor(kBlue);
            pullVsLB_weighted->GetXaxis()->SetTitle("LB id");
            pullVsLB_weighted->GetXaxis()->SetTitleOffset(0.7);
            pullVsLB_weighted->GetYaxis()->SetTitle("Pull");
            pullVsLB_weighted->Draw("PE");

            
//             pullVsLB_unweighted->GetXaxis()->SetRangeUser(-16, 16);
            pullVsLB_unweighted->SetMarkerSize(0.7);
            pullVsLB_unweighted->SetMarkerStyle(20);
            pullVsLB_unweighted->SetMarkerColor(kRed);
            pullVsLB_unweighted->SetLineColor(kRed);
            pullVsLB_unweighted->Draw("PE SAME");
            
            pullVsLB_weighted->Draw("PE SAME");
            
            for(unsigned int j=0; j<runFinish.size()/*-1*/; ++j){
                TLine *line = new TLine(runFinish[j], -pull_limit, runFinish[j], pull_limit);
                line->SetLineColor(kGray+1);
                line->SetLineStyle(7);
                line->SetLineWidth(2);
                line->Draw();
                
                TLatex l; l.SetTextFont(42); l.SetTextSize(.03); l.SetTextColor(kGray+1); l.SetTextAngle(90);
                l.DrawLatex(runFinish[j]-5, -12, Form("%d", runNumVec[j]));
//                 std::cout << runNumVec[j] << std::endl;
            }
            
            TLatex lATLAS; lATLAS.SetNDC(); lATLAS.SetTextFont(42);  lATLAS.SetTextSize(.05);
            lATLAS.DrawLatex(0.16, 0.9, "#font[72]{ATLAS} Internal  #sqrt{s} = 13 TeV #beta* = 90 m");
            lATLAS.DrawLatex(0.16, 0.84, Form("p+p #rightarrow p'+%s+p'", latex_name[in].Data() ) );
            
            TLatex l; l.SetNDC(); l.SetTextFont(42); l.SetTextSize(.05);
            l.SetTextColor(kRed);
            l.DrawLatex(0.45, 0.35, "Data uncorrected");
            l.SetTextColor(kBlue);
            l.DrawLatex(0.45, 0.28, "Data corrected");
            
            
            gPad->RedrawAxis();
            
            c->Print( dirName+"PullVsLB_"+input_name[in]+".pdf");
        }
        
        
    }
    
    
}


void CepAnalysis::transformToCrossSection(TH1* hist) const{
    hist->Scale(1./totalIntegratedLuminosity, "width");
}



SystematicsOutput CepAnalysis::calculateSystematicErrors( TH1F* const referenceHist, TH1F** const checkHist, TH1F* const bkgdErrorHistogram ) const{
  SystematicsOutput output;
  for(int i=0; i<CepUtil::nSystChecks; ++i){
    output.mSystCheckHistVec.push_back( checkHist[i] );
    if( referenceHist->GetNbinsX() != checkHist[i]->GetNbinsX() ){
      std::cerr << "Error in CepAnalysis::calculateSystematicErrors()" << std::endl;
//       std::cerr << "Ommited systematic check:  " << mSystCheckName[i] << std::endl;
      continue;
    }
    for(int bin=1; bin<=referenceHist->GetNbinsX(); ++bin){
      if( i==CepUtil::NONEXCL_BKGD_DOWN || i==CepUtil::NONEXCL_BKGD_UP ){
        if( bkgdErrorHistogram ){
          if( !(referenceHist->GetBinContent(bin)>0) ){
            output.mError[i].push_back( 0.0 );
          } else{
            output.mError[i].push_back( (i==CepUtil::NONEXCL_BKGD_DOWN ? 1 : -1) * fabs(bkgdErrorHistogram->GetBinContent(bin)) );
            checkHist[i]->SetBinContent(bin, referenceHist->GetBinContent(bin) + (i==CepUtil::NONEXCL_BKGD_DOWN ? 1 : -1) * fabs(bkgdErrorHistogram->GetBinContent(bin)) );
          }
        } else{
          output.mError[i].push_back( 0.0 );
        }
      } else{
        output.mError[i].push_back( checkHist[i]->GetBinContent(bin) - referenceHist->GetBinContent(bin) );
      }
    }
  }
  TH1F *tmpHist = new TH1F( *referenceHist );
  tmpHist->SetName(Form("%s_forSyst", tmpHist->GetName()));
  output.mSystCheckHistVec.push_back( tmpHist );
  
  TH1F *tmpHistZero = new TH1F( *referenceHist );
  tmpHistZero->SetName("tmpHistZero");
  for(int bin=0; bin<=(tmpHistZero->GetNbinsX()+1); ++bin)
      tmpHistZero->SetBinContent(bin, 0.0);
  output.mhSystErrorUp = new TH1F( *referenceHist ); output.mhSystErrorUp->SetName( "SysteErrorUp_"+TString(referenceHist->GetName()) ); output.mhSystErrorUp->Reset("ICESM");
  output.mhSystErrorDown = new TH1F( *referenceHist ); output.mhSystErrorDown->SetName( "SysteErrorDown_"+TString(referenceHist->GetName()) ); output.mhSystErrorDown->Reset("ICESM");
//   output.mhSystErrorUp_TOF = new TH1F( *referenceHist ); output.mhSystErrorUp_TOF->SetName( "SysteErrorUp_TOF_"+TString(referenceHist->GetName()) ); output.mhSystErrorUp_TOF->Reset("ICESM");
//   output.mhSystErrorDown_TOF = new TH1F( *referenceHist ); output.mhSystErrorDown_TOF->SetName( "SysteErrorDown_TOF_"+TString(referenceHist->GetName()) ); output.mhSystErrorDown_TOF->Reset("ICESM");
//   output.mhSystErrorUp_TPC = new TH1F( *referenceHist ); output.mhSystErrorUp_TPC->SetName( "SysteErrorUp_TPC_"+TString(referenceHist->GetName()) ); output.mhSystErrorUp_TPC->Reset("ICESM");
//   output.mhSystErrorDown_TPC = new TH1F( *referenceHist ); output.mhSystErrorDown_TPC->SetName( "SysteErrorDown_TPC_"+TString(referenceHist->GetName()) ); output.mhSystErrorDown_TPC->Reset("ICESM");
//   output.mhSystErrorUp_RP = new TH1F( *referenceHist ); output.mhSystErrorUp_RP->SetName( "SysteErrorUp_RP_"+TString(referenceHist->GetName()) ); output.mhSystErrorUp_RP->Reset("ICESM");
//   output.mhSystErrorDown_RP = new TH1F( *referenceHist ); output.mhSystErrorDown_RP->SetName( "SysteErrorDown_RP_"+TString(referenceHist->GetName()) ); output.mhSystErrorDown_RP->Reset("ICESM");
  output.mhSystErrorUp_Other = new TH1F( *referenceHist ); output.mhSystErrorUp_Other->SetName( "SysteErrorUp_"+TString(referenceHist->GetName()) ); output.mhSystErrorUp_Other->Reset("ICESM");
  output.mhSystErrorDown_Other = new TH1F( *referenceHist ); output.mhSystErrorDown_Other->SetName( "SysteErrorDown_"+TString(referenceHist->GetName()) ); output.mhSystErrorDown_Other->Reset("ICESM");
  
  for(int bin=1; bin<=tmpHistZero->GetNbinsX(); ++bin){
      double errors[CepUtil::nSigns] = {0,0};
//       double errors_Tof[CepUtil::nSigns] = {0,0};
//       double errors_Tpc[CepUtil::nSigns] = {0,0};
//       double errors_TofTpc[CepUtil::nSigns] = {0,0};
//       double errors_Rp[CepUtil::nSigns] = {0,0};
      double errors_Other[CepUtil::nSigns] = {0,0};
      for(int i=0; i<CepUtil::nSystChecks; ++i){
          double err = output.mError[i][bin-1];
          double errPrev = bin>1 ? output.mError[i][bin-2] : 0;
          double errNext = bin<tmpHistZero->GetNbinsX() ? output.mError[i][bin] : 0;
          errors[ err>0 ? CepUtil::PLUS : CepUtil::MINUS ] += err*err;
          
          switch(i){
//               case DELTAZ0CUT_EFF_UP:
//               case DELTAZ0CUT_EFF_DOWN:
//               case VERTEXING_EFF_UP:
//               case VERTEXING_EFF_DOWN:
//               case TPC_RECO_EFF_EMB_SAMPLE_REPRESENTATIVENESS_UP:
//               case TPC_RECO_EFF_EMB_SAMPLE_REPRESENTATIVENESS_DOWN:
//               case TPC_RECO_EFF_EMBEDDING_EFFECT_UP:
//               case TPC_RECO_EFF_EMBEDDING_EFFECT_DOWN:
//               case TPC_QUALITY_TIGHT:
//               case TPC_QUALITY_LOOSE:
//               case TPC_DEAD_MATERIAL_UP:
//               case TPC_DEAD_MATERIAL_DOWN: errors_Tpc[ err>0 ? CepUtil::PLUS : CepUtil::MINUS ] += err*err; break;
//               
//               case TOF_MATCH_EFF_UP:
//               case TOF_MATCH_EFF_DOWN: errors_Tof[ err>0 ? CepUtil::PLUS : CepUtil::MINUS ] += err*err; break;
//               
//               case RP_TRIGG_EFF_UP:
//               case RP_TRIGG_EFF_DOWN:
//               case RP_DEADMAT_UP:
//               case RP_DEADMAT_DOWN:
//               case RP_EFF_UP:
//               case RP_EFF_DOWN: errors_Rp[ err>0 ? CepUtil::PLUS : CepUtil::MINUS ] += err*err; break;
//               
//               case PILEUP_CORRECTION_UP:
//               case PILEUP_CORRECTION_DOWN:
//               case ZVTX_MEAN_UP:
//               case ZVTX_MEAN_DOWN:
//               case ZVTX_SIGMA_UP:
//               case ZVTX_SIGMA_DOWN:
//               case NTOFCLUSTERSCUT_EFF_UP:
//               case NTOFCLUSTERSCUT_EFF_DOWN:
//               case NONEXCL_BKGD_UP:
//               case NONEXCL_BKGD_DOWN: errors_Other[ err>0 ? CepUtil::PLUS : CepUtil::MINUS ] += err*err; break;
              default: errors_Other[ err>0 ? CepUtil::PLUS : CepUtil::MINUS ] += err*err; break;
          };
          
      }
      double yVal = referenceHist->GetBinContent(bin);
      output.mhSystErrorUp->SetBinContent( bin, sqrt( errors[CepUtil::PLUS]/(yVal*yVal)  ) );
      output.mhSystErrorDown->SetBinContent( bin, sqrt( errors[CepUtil::MINUS]/(yVal*yVal)  ) );
//       output.mhSystErrorUp_TOF->SetBinContent( bin, sqrt( errors_Tof[CepUtil::PLUS]/(yVal*yVal)  ) );
//       output.mhSystErrorDown_TOF->SetBinContent( bin, sqrt( errors_Tof[CepUtil::MINUS]/(yVal*yVal)  ) );
//       output.mhSystErrorUp_TPC->SetBinContent( bin, sqrt( errors_Tpc[CepUtil::PLUS]/(yVal*yVal)  ) );
//       output.mhSystErrorDown_TPC->SetBinContent( bin, sqrt( errors_Tpc[CepUtil::MINUS]/(yVal*yVal)  ) );
//       output.mhSystErrorUp_RP->SetBinContent( bin, sqrt( errors_Rp[CepUtil::PLUS]/(yVal*yVal)  ) );
//       output.mhSystErrorDown_RP->SetBinContent( bin, sqrt( errors_Rp[CepUtil::MINUS]/(yVal*yVal)  ) );
      output.mhSystErrorUp_Other->SetBinContent( bin, sqrt( errors_Other[CepUtil::PLUS]/(yVal*yVal)  ) );
      output.mhSystErrorDown_Other->SetBinContent( bin, sqrt( errors_Other[CepUtil::MINUS]/(yVal*yVal)  ) );
  }
  
  return output;
}







void CepAnalysis::drawFinalResult(TH1F* hMainInput, DrawingOptions & opt, SystematicsOutput & syst) const{
  
  TH1F *hMain = new TH1F( *hMainInput );
  hMain->SetName("FinalResult_"+TString(hMain->GetName()));
  
  TGaxis::SetMaxDigits(3);
  
  TCanvas *c = new TCanvas("cccccc", "cccccc", opt.mCanvasWidth, opt.mCanvasHeight);
//   c->SetFrameFillStyle(0);
//   c->SetFrameLineWidth(2);
//   c->SetFillColor(-1);
  c->SetLeftMargin(opt.mLeftMargin);
  c->SetRightMargin(opt.mRightMargin);
  c->SetTopMargin(opt.mTopMargin);
  c->SetBottomMargin(opt.mBottomMargin);
  
  c->SetLogy(opt.mYlog);
    
  //   systematicErrorsGraph->SetFillColor( 18 );
  //   //systematicErrorsGraph->SetFillStyle(/*3013*//*3345*/3005);
  //   systematicErrorsGraph->SetLineWidth(0);
  //   systematicErrorsGraph->SetLineColor(kWhite);
  
  if(opt.mScale){
    hMain->Scale( opt.mScalingFactor );
  }
  
  hMain->SetMarkerStyle(opt.mMarkerStyle);
  hMain->SetMarkerSize(opt.mMarkerSize);
  hMain->SetMarkerColor(opt.mMarkerColor);
  hMain->SetLineStyle(opt.mLineStyle);
  hMain->SetLineColor(opt.mLineColor);
  hMain->SetLineWidth(opt.mLineWidth);
  
  hMain->GetXaxis()->SetTitle(opt.mXaxisTitle);
  hMain->GetXaxis()->SetTitleSize(opt.mXaxisTitleSize);
  hMain->GetXaxis()->SetTitleOffset(opt.mXaxisTitleOffset);
  hMain->GetXaxis()->SetLabelSize(opt.mXaxisLabelSize);
  hMain->GetXaxis()->SetLabelOffset(opt.mXaxisLabelOffset);
  hMain->GetXaxis()->SetTickLength(opt.mXaxisTickLength);
  hMain->GetXaxis()->SetRangeUser(opt.mXmin, opt.mXmax);
  
  if( opt.mSetDivisionsX )
    hMain->GetXaxis()->SetNdivisions( opt.mXnDivisionA, opt.mXnDivisionB, opt.mXnDivisionC, !opt.mForceDivisionsX );
  
  hMain->GetYaxis()->SetTitleSize(opt.mYaxisTitleSize);
  hMain->GetYaxis()->SetTitle(opt.mYaxisTitle);
  hMain->GetYaxis()->SetTitleOffset(opt.mYaxisTitleOffset);
  hMain->GetYaxis()->SetLabelSize(opt.mYaxisLabelSize);
  hMain->GetYaxis()->SetLabelOffset(opt.mYaxisLabelOffset);
  hMain->GetYaxis()->SetTickLength(opt.mYaxisTickLength);
  hMain->GetYaxis()->SetRangeUser(opt.mYmin, (opt.mScale ? opt.mScalingFactor : 1.0) * opt.mYmax);
  
  if( opt.mSetDivisionsY )
    hMain->GetYaxis()->SetNdivisions( opt.mYnDivisionA, opt.mYnDivisionB, opt.mYnDivisionC, !opt.mForceDivisionsY );
  
  hMain->Draw("PEX0");
  
  double lumiError = 0.04; //ALERT //CepUtil::luminosityUncertainty;
  const double systErrorBoxWidth = (opt.mMarkerSize * 8.0 * opt.mSystErrorBoxWidthAdjustmentFactor / opt.mCanvasWidth*( 1.0 - opt.mLeftMargin - opt.mRightMargin )) * (hMain->GetXaxis()->GetXmax() - hMain->GetXaxis()->GetXmin());
  
    
  TBox *typicalBox = nullptr;
  if( opt.mXpositionSystErrorBox.size() > 0 ){
    for(unsigned int i=0; i<opt.mXpositionSystErrorBox.size(); ++i){
      int binNumber = hMain->GetXaxis()->FindBin( opt.mXpositionSystErrorBox[i] );
      if( !(hMain->GetBinContent(binNumber)>0) )
        continue;
      
      double xMinSystErrorBox = hMain->GetXaxis()->GetBinCenter(binNumber) - systErrorBoxWidth/2.;
      double xMaxSystErrorBox = hMain->GetXaxis()->GetBinCenter(binNumber) + systErrorBoxWidth/2.;
      double yMinSystErrorBox = hMain->GetBinContent(binNumber) * (1.0 - sqrt(pow(syst.mhSystErrorDown->GetBinContent(binNumber),2) + pow(lumiError/(1.0+lumiError),2)));
      double yMaxSystErrorBox = hMain->GetBinContent(binNumber) * (1.0 + sqrt(pow(syst.mhSystErrorUp->GetBinContent(binNumber),2) + pow(lumiError/(1.0-lumiError),2)));
      TBox *b = new TBox( xMinSystErrorBox, yMinSystErrorBox, xMaxSystErrorBox, yMaxSystErrorBox );
      b->SetFillColor(opt.mSystErrorTotalBoxColor);
      b->SetLineWidth(0);
      b->Draw("l");
      
      yMinSystErrorBox = hMain->GetBinContent(binNumber) * (1.0 - syst.mhSystErrorDown->GetBinContent(binNumber));
      yMaxSystErrorBox = hMain->GetBinContent(binNumber) * (1.0 + syst.mhSystErrorUp->GetBinContent(binNumber));
      b = new TBox( xMinSystErrorBox, yMinSystErrorBox, xMaxSystErrorBox, yMaxSystErrorBox );
      b->SetFillColor(opt.mSystErrorBoxColor);
      b->SetLineWidth(0);
      b->Draw("l");
      
      if( !typicalBox ){
        typicalBox = new TBox( xMinSystErrorBox, yMinSystErrorBox, xMaxSystErrorBox, yMaxSystErrorBox );
        typicalBox->SetFillColor(opt.mSystErrorTotalBoxColor);
        typicalBox->SetLineColor(opt.mSystErrorBoxColor);
        typicalBox->SetLineWidth(8);
      }
    }
  }
  
  for(unsigned int i=0; i<opt.mHistTH1F.size(); ++i){
    if(opt.mScale){
      opt.mHistTH1F[i]->Scale( opt.mScalingFactor );
    }
    opt.mHistTH1F[i]->Draw( opt.mHistTH1F_DrawingOpt[i] );
  }
  
  
  TLegend *legend = new TLegend( opt.mLegendX, opt.mLegendY, opt.mLegendX+opt.mLegendXwidth, opt.mLegendY+opt.mLegendYwidth );
  legend->SetNColumns(opt.mNColumns);
  legend->SetBorderSize(0);
  legend->SetFillStyle(0);
  legend->SetTextSize(opt.mLegendTextSize);
  legend->SetTextFont(42);
  legend->AddEntry(hMain, "Data", "pe");
//   legend->AddEntry(hMain, "Stat. uncertainty", "el");
  if(typicalBox)
    legend->AddEntry(typicalBox, "Syst. uncertainty", "fl");
  for(unsigned int i=0; i<opt.mObjForLegend.size(); ++i)
    legend->AddEntry( opt.mObjForLegend[i], opt.mDesriptionForLegend[i], opt.mDrawTypeForLegend[i]);
  legend->SetMargin(mSpecialLegendMarginFactor * 0.3 * opt.mLegendXwidth * legend->GetNRows() / legend->GetNColumns() );
  if( !mSpecialLabelRatio || mEnableLegend )
    legend->Draw();
  
  
  if( opt.mTextToDraw.size() > 0 ){
    for(unsigned int i=0; i<opt.mTextToDraw.size(); ++i){
      opt.mTextToDraw[i].SetNDC(kTRUE);
      opt.mTextToDraw[i].Draw();
    }
  }
  
  //   TASImage *img = new TASImage("miscellaneous/ATLAS_logo.pdf");
  //   //   TASImage *img = new TASImage("miscellaneous/ATLAS_logo.eps");
  //   img->SetImageQuality(TAttImage::kImgBest);
  
  
  hMain->DrawCopy("PEX0 SAME", "");
  gPad->RedrawAxis();
  
  
  if( opt.mInsert ){
    TPad *insert = new TPad("insert", "insert", opt.mX1Insert, opt.mY1Insert, opt.mX2Insert, opt.mY2Insert);
    insert->SetBottomMargin(0); // Upper and lower plot are joined
    insert->SetFrameFillStyle(0);
    insert->SetFrameLineWidth(2);
    insert->SetFillColor(-1);
    insert->Draw();             // Draw the upper pad: insert
    insert->cd();               // insert becomes the current pad
    insert->SetLeftMargin(opt.mLeftMargin);
    insert->SetRightMargin(opt.mRightMargin);
    insert->SetTopMargin(opt.mTopMargin);
    insert->SetBottomMargin(opt.mBottomMargin);
    if( opt.mYlogInsert )
        insert->SetLogy();
    
    TH1F *hMainCopy = new TH1F( *hMain );
    hMainCopy->GetXaxis()->SetTitle("");
    hMainCopy->GetXaxis()->SetLabelSize( 0.9*hMainCopy->GetXaxis()->GetLabelSize() * ( 1. - opt.mLeftMargin - opt.mRightMargin) / (opt.mX2Insert - opt.mX1Insert) );
    hMainCopy->GetYaxis()->SetLabelSize( 0.9*hMainCopy->GetYaxis()->GetLabelSize() * ( 1. - opt.mLeftMargin - opt.mRightMargin) / (opt.mX2Insert - opt.mX1Insert) );
    hMainCopy->GetYaxis()->SetTitle("");
    hMainCopy->GetXaxis()->SetRange( hMainCopy->FindBin(opt.mXMinInstert ), hMainCopy->FindBin(opt.mXMaxInstert ) );
    int binMaxY;
    binMaxY = hMainCopy->GetMaximumBin();
    hMainCopy->GetYaxis()->SetRangeUser(opt.mYMinInstert, opt.mYMaxInstert<0 ? (1.1*hMainCopy->GetBinContent( binMaxY )) : opt.mYMaxInstert);
    
    int n1 = hMainCopy->GetYaxis()->GetNdivisions()%100;
    int n2 = hMainCopy->GetYaxis()->GetNdivisions()%10000 - n1;
    int n3 = hMainCopy->GetYaxis()->GetNdivisions() - 100*n2 - n1;
    
    hMainCopy->GetYaxis()->SetNdivisions( n1/2, 2, 1 );
    hMainCopy->GetXaxis()->SetRangeUser(opt.mXMinInstert, opt.mXMaxInstert);
    hMainCopy->GetXaxis()->SetNdivisions(10,5,1);
    
    hMainCopy->GetXaxis()->SetTickLength(0.04);
    hMainCopy->GetYaxis()->SetTickLength(0.02);
    
    hMainCopy->Draw("PEX0");
    for(unsigned int i=0; i<opt.mHistTH1F.size(); ++i){
//       if(opt.mScale){
//         opt.mHistTH1F[i]->Scale( opt.mScalingFactor );
//       }
      opt.mHistTH1F[i]->DrawCopy( opt.mHistTH1F_DrawingOpt[i], "" );
    }
    
    if( opt.mXpositionSystErrorBox.size() > 0 ){
      for(unsigned int i=0; i<opt.mXpositionSystErrorBox.size(); ++i){
        int binNumber = hMainCopy->GetXaxis()->FindBin( opt.mXpositionSystErrorBox[i] );
        if( !(hMainCopy->GetBinContent(binNumber)>0) || opt.mXpositionSystErrorBox[i]>opt.mXMaxInstert || opt.mXpositionSystErrorBox[i]<opt.mXMinInstert )
          continue;
        
        double xMinSystErrorBox = hMainCopy->GetXaxis()->GetBinCenter(binNumber) - systErrorBoxWidth/2.;
        double xMaxSystErrorBox = hMainCopy->GetXaxis()->GetBinCenter(binNumber) + systErrorBoxWidth/2.;
        double yMinSystErrorBox = hMainCopy->GetBinContent(binNumber) * (1.0 - sqrt(pow(syst.mhSystErrorDown->GetBinContent(binNumber),2) + pow(lumiError/(1.0+lumiError),2)));
        double yMaxSystErrorBox = hMainCopy->GetBinContent(binNumber) * (1.0 + sqrt(pow(syst.mhSystErrorUp->GetBinContent(binNumber),2) + pow(lumiError/(1.0-lumiError),2)));
        TBox *b = new TBox( xMinSystErrorBox, yMinSystErrorBox, xMaxSystErrorBox, yMaxSystErrorBox );
        b->SetFillColor(opt.mSystErrorTotalBoxColor);
        b->SetLineWidth(0);
        b->Draw("l");
        
        yMinSystErrorBox = hMainCopy->GetBinContent(binNumber) * (1.0 - syst.mhSystErrorDown->GetBinContent(binNumber));
        yMaxSystErrorBox = hMainCopy->GetBinContent(binNumber) * (1.0 + syst.mhSystErrorUp->GetBinContent(binNumber));
        b = new TBox( xMinSystErrorBox, yMinSystErrorBox, xMaxSystErrorBox, yMaxSystErrorBox );
        b->SetFillColor(opt.mSystErrorBoxColor);
        b->SetLineWidth(0);
        b->Draw("l");
      }
    }
    
    hMainCopy->DrawCopy("PEX0 SAME", "");
    gPad->RedrawAxis();
  }
  
  c->cd();
  c->Modified();
  c->Update();
  
  c->Print( TString("PDF/")+opt.mPdfName+".pdf", "EmbedFonts");
//   c->Print( TString("PDF/")+opt.mPdfName+".png");
  
  
  const double ratioFactor = 0.25;
  TCanvas *c2 = new TCanvas("cccccc2", "cccccc2", opt.mCanvasWidth, (ratioFactor + 1.0)*opt.mCanvasHeight);
  //   c->SetFrameFillStyle(0);
  //   c->SetFrameLineWidth(2);
  //   c->SetFillColor(-1);
  c2->SetLeftMargin(0);
  c2->SetRightMargin(0);
  c2->SetTopMargin(0);
  c2->SetBottomMargin(0);
  
  // Upper plot will be in pad1
  TPad *pad1 = new TPad("pad1", "pad1", 0, ratioFactor, 1.0, 1.0);
  pad1->SetBottomMargin(0); // Upper and lower plot are joined
  pad1->SetFrameFillStyle(0);
  pad1->SetFillColor(-1);
  pad1->Draw();             // Draw the upper pad: pad1
  pad1->cd();               // pad1 becomes the current pad
  gPad->SetLeftMargin(0);
  gPad->SetRightMargin(0);
  gPad->SetTopMargin(0);
  gPad->SetBottomMargin(0);
  
  TH1F *ratio_Data_to_Data = new TH1F( *hMain );
  hMain->GetXaxis()->SetLabelSize( 0 );
  hMain->GetXaxis()->SetTitle("");
  c->Modified();
  c->Update();
  
  c->DrawClonePad();

  const double modifiedRatioFactor = ratioFactor + 0.54*opt.mBottomMargin;
  
  c2->cd();          // Go back to the main canvas before defining pad2
  TPad *pad2 = new TPad("pad2", "pad2", 0, 0, 1.0, ratioFactor + 0.65*opt.mBottomMargin);
  pad2->SetFrameFillStyle(0);
  pad2->SetFillColor(-1);
//   gStyle->SetGridColor(kGray);
  pad2->SetGridy(kTRUE);
  pad2->Draw();
  pad2->cd();       // pad2 becomes the current pad
  gPad->SetLeftMargin(opt.mLeftMargin);
  gPad->SetRightMargin(opt.mRightMargin);
  gPad->SetTopMargin(0.005);
  gPad->SetBottomMargin(opt.mBottomMargin * (1.0-modifiedRatioFactor)/modifiedRatioFactor);
  
  
  std::vector< TH1F* > histMC_ratio;
  for(unsigned int fileId=0; fileId<opt.mHistTH1F.size(); ++fileId) 
      histMC_ratio.push_back( new TH1F(*opt.mHistTH1F[fileId]) );
  
  for(int i=1; i<=hMain->GetNbinsX(); ++i){
    if( ratio_Data_to_Data->GetBinError(i)>0 ){
      ratio_Data_to_Data->SetBinError( i, hMain->GetBinError(i)/hMain->GetBinContent(i) );
      ratio_Data_to_Data->SetBinContent( i, 1.0 );
    }
  }
  
  for(unsigned int j=0; j<histMC_ratio.size(); ++j){
    histMC_ratio[j]->Scale( hMain->Integral(0, -1, "width") / histMC_ratio[j]->Integral(0, -1, "width") );
    histMC_ratio[j]->Divide( hMain );
  }
  
  ratio_Data_to_Data->GetYaxis()->SetRangeUser(0.2, 1.8);
  ratio_Data_to_Data->GetYaxis()->SetTitle(mSpecialLabelRatio ? "MC(norm.)/Data       " : "MC(norm.)/Data");
  ratio_Data_to_Data->GetYaxis()->SetTitleOffset( mSpecialLabelRatio ? (mSpecialLabelOffsetFactor * ratio_Data_to_Data->GetYaxis()->GetTitleOffset()*modifiedRatioFactor/(1.0-modifiedRatioFactor)) : 0.6  );
  ratio_Data_to_Data->GetYaxis()->CenterTitle();
  ratio_Data_to_Data->GetYaxis()->SetNdivisions(4,4,1);
  ratio_Data_to_Data->GetYaxis()->SetTitleSize( /*1.6**/ratio_Data_to_Data->GetYaxis()->GetTitleSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
  ratio_Data_to_Data->GetYaxis()->SetLabelSize( /*1.2**/ratio_Data_to_Data->GetYaxis()->GetLabelSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
  ratio_Data_to_Data->GetXaxis()->SetLabelOffset( 0.01 );
  ratio_Data_to_Data->GetYaxis()->SetTickLength( ratio_Data_to_Data->GetYaxis()->GetTickLength() );
  ratio_Data_to_Data->GetXaxis()->SetTitleSize( /*1.4**/ratio_Data_to_Data->GetXaxis()->GetTitleSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
  ratio_Data_to_Data->GetXaxis()->SetLabelSize( /*1.4**/ratio_Data_to_Data->GetXaxis()->GetLabelSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
  ratio_Data_to_Data->GetXaxis()->SetTickLength( (1.0-modifiedRatioFactor)/modifiedRatioFactor* 1.2 *ratio_Data_to_Data->GetXaxis()->GetTickLength() );
  
  ratio_Data_to_Data->Draw("AXIS");
  ratio_Data_to_Data->Draw("AXIG SAME");
  
  
  TLine *line = new TLine(opt.mXmin, 1.0, opt.mXmax, 1.0);
  line->SetLineWidth(2);
  line->SetLineStyle(7);
  line->SetLineColor(kBlack);
  line->Draw();
  
  for(unsigned int i=0; i<histMC_ratio.size(); ++i){
    histMC_ratio[i]->SetMarkerColor( histMC_ratio[i]->GetLineColor() );
    histMC_ratio[i]->SetMarkerStyle( opt.mMarkerStyle );
    histMC_ratio[i]->SetMarkerSize( opt.mMarkerSize );
    histMC_ratio[i]->Draw( /*opt.mHistTH1F_DrawingOpt[i]*/"PE0 ][ SAME" );
  }
  
//   ratio_Data_to_Data->DrawCopy("PEX0 SAME", "");
  gPad->RedrawAxis();
  
  c2->Print( TString("PDF/")+"Ratio_"+opt.mPdfName+".pdf", "EmbedFonts");
//   c2->Print( TString("PDF/")+"Ratio_"+opt.mPdfName+".png");
  
  
  //--
  
  
  c->Clear();
  
  c->SetLeftMargin(opt.mLeftMargin);
  c->SetRightMargin(opt.mRightMargin);
  c->SetTopMargin(opt.mTopMargin);
  c->SetBottomMargin(0.4);
  
  legend = new TLegend(0.0, 0.02, 1.0, 0.3);
  legend->SetBorderSize(0);
  legend->SetFillStyle(0);
  legend->SetMargin(0.18);
  legend->SetNColumns(3);
       
  TBox *b, *b2;
  TH1F *mainRatio;
  
  double sumUp = 0;
  double sumDown = 0;
  int nBinsHMain = 0;
  for(int i=1; i<=hMain->GetNbinsX(); ++i){
    if( hMain->GetBinContent(i) == 0 || hMain->GetXaxis()->GetBinCenter(i)<opt.mXmin || hMain->GetXaxis()->GetBinCenter(i)>opt.mXmax ) continue;
    ++nBinsHMain;
    sumDown += /*1./*/(1.0 - sqrt(pow(syst.mhSystErrorDown->GetBinContent(i),2) + pow(lumiError/(1.0+lumiError),2)));
    sumUp += /*1./*/(1.0 + sqrt(pow(syst.mhSystErrorUp->GetBinContent(i),2) + pow(lumiError/(1.0-lumiError),2)));
  }
  
  double averageSystUp = sumUp / nBinsHMain;
  double averageSystDown = sumDown / nBinsHMain;
  
  
//   const double minY = averageSystDown > 0.9 ? 0.95 : (0.95 - ((0.95-averageSystDown)/0.05+1)*0.05 );
//   const double maxY = averageSystUp < 1.11 ? 1.11 : (((averageSystUp-1.05)/0.05+1)*0.1 + 1.11);
  
  const double minY = averageSystDown > 0.9 ? 0.92 : (0.95 - ((0.95-averageSystDown)/0.05+1)*0.05 );
  const double maxY = averageSystUp < 1.11 ? 1.16 : (((averageSystUp-1.05)/0.05+1)*0.1 + 1.11);
  
  int nColumn = 0;
  for(int i=0; i<CepUtil::nSystChecks; ++i){
    TH1F* ratio = new TH1F( *hMain );
    if(i==0) mainRatio = ratio;
    ratio->SetName("SystCheckRatio_"+TString(hMain->GetName())+"_"+mSystCheckName[i]);
    ratio->Divide( syst.mSystCheckHistVec[i], syst.mSystCheckHistVec[CepUtil::nSystChecks] );
    
    
    for(int bin=1; bin<=ratio->GetNbinsX(); ++bin ){
      if( !(ratio->GetBinContent(bin) > 0) )
        ratio->SetBinContent(bin, 1.0); // for better drawing
    }
    
    int lineColor = i/2+2;
    if( lineColor > 10 ){
      switch(lineColor){
        case 11: lineColor = kOrange-7; break;
        case 12: lineColor = 37; break;
        case 13: lineColor = 30; break;
        case 14: lineColor = 49; break;
        case 15: lineColor = 46; break;
        case 16: lineColor = kSpring+10; break;
      }
    }
    
    ratio->SetLineWidth(4/*opt.mLineWidth*/);
    ratio->SetLineColor( lineColor );
//     ratio->SetLineStyle( i%2==0 ? 1 : 7 ); // ALERT this was used to present separately up and down syst. check effect
    
    if(i==0){
      ratio->GetXaxis()->SetTitle(opt.mXaxisTitle);
      ratio->GetXaxis()->SetTitleSize(opt.mXaxisTitleSize);
      ratio->GetXaxis()->SetTitleOffset(opt.mXaxisTitleOffset);
      ratio->GetXaxis()->SetLabelSize(opt.mXaxisLabelSize);
      ratio->GetXaxis()->SetLabelOffset(opt.mXaxisLabelOffset);
      ratio->GetXaxis()->SetTickLength(opt.mXaxisTickLength);
      ratio->GetXaxis()->SetRangeUser(opt.mXmin, opt.mXmax);
      
      if( opt.mSetDivisionsX )
        ratio->GetXaxis()->SetNdivisions( opt.mXnDivisionA, opt.mXnDivisionB, opt.mXnDivisionC, !opt.mForceDivisionsX );
      
      ratio->GetYaxis()->SetTitleSize(opt.mYaxisTitleSize);
      ratio->GetYaxis()->SetTitle(opt.mYaxisTitle);
      ratio->GetYaxis()->SetTitleOffset(opt.mYaxisTitleOffset+0.06);
      ratio->GetYaxis()->SetLabelSize(opt.mYaxisLabelSize);
      ratio->GetYaxis()->SetLabelOffset(opt.mYaxisLabelOffset);
      ratio->GetYaxis()->SetTickLength(opt.mYaxisTickLength);
      
      if( opt.mSetDivisionsY )
        ratio->GetYaxis()->SetNdivisions( opt.mYnDivisionA, opt.mYnDivisionB, opt.mYnDivisionC, !opt.mForceDivisionsY );
      
      ratio->Draw("AXIS");
      
      for(int j=1; j<=hMain->GetNbinsX(); ++j){
        if( hMain->GetBinContent(j) == 0 || hMain->GetXaxis()->GetBinCenter(j)<opt.mXmin || hMain->GetXaxis()->GetBinCenter(j)>opt.mXmax ) continue;
        
        double yMinSystErrorBox = /*1./*/(1.0 - sqrt(pow(syst.mhSystErrorDown->GetBinContent(j),2) + pow(lumiError/(1.0+lumiError),2)));
        double yMaxSystErrorBox = /*1./*/(1.0 + sqrt(pow(syst.mhSystErrorUp->GetBinContent(j),2) + pow(lumiError/(1.0-lumiError),2)));
        double xMinSystErrorBox = hMain->GetXaxis()->GetBinLowEdge(j);
        double xMaxSystErrorBox = hMain->GetXaxis()->GetBinUpEdge(j);
        if( yMinSystErrorBox<minY ) yMinSystErrorBox = minY;
        if( yMaxSystErrorBox>maxY ) yMaxSystErrorBox = maxY;
        b = new TBox( xMinSystErrorBox, yMinSystErrorBox, xMaxSystErrorBox, yMaxSystErrorBox );
        b->SetFillColor(opt.mSystErrorTotalBoxColor);
        b->SetLineWidth(0);
        b->Draw("l");
        
        yMinSystErrorBox = /*1./*/(1.0 - syst.mhSystErrorDown->GetBinContent(j));
        yMaxSystErrorBox = /*1./*/(1.0 + syst.mhSystErrorUp->GetBinContent(j));
        xMinSystErrorBox = hMain->GetXaxis()->GetBinLowEdge(j);
        xMaxSystErrorBox = hMain->GetXaxis()->GetBinUpEdge(j);
        if( yMinSystErrorBox<minY ) yMinSystErrorBox = minY;
        if( yMaxSystErrorBox>maxY ) yMaxSystErrorBox = maxY;
        b2 = new TBox( xMinSystErrorBox, yMinSystErrorBox, xMaxSystErrorBox, yMaxSystErrorBox );
        b2->SetFillColor(opt.mSystErrorBoxColor);
        b2->SetLineWidth(0);
        b2->Draw("l");
      }
      
    }
    
    
    ratio->Draw("HIST ][ SAME");
    ratio->GetYaxis()->SetRangeUser(minY, maxY);
    ratio->GetYaxis()->SetTitle("Ratio to nominal");
    ratio->GetYaxis()->CenterTitle();
    
    
    
    //     legend->AddEntry(ratio, mSystCheckShortName[i], "l"); //ALERT this was used to present separately up and down syst. check effect
    
//     if( i%2==1 ){
//       if( i<(nSystChecks-2) ){
//         TObject *emptyPtr = nullptr;
//         legend->AddEntry(emptyPtr, "", "");
//       } else
//         if( i==(nSystChecks-1) ){
//           legend->AddEntry(b2, "Total (w/o lumi.)", "f");
//         } 
//     }
    
    TBox* boxForLegend = new TBox();
    boxForLegend->SetFillColor( opt.mSystErrorBoxColor );
    boxForLegend->SetLineColor( lineColor );
    boxForLegend->SetLineWidth( 5 );
    
    
    if( nColumn%3==2 ){
      if( i==(CepUtil::nSystChecks-3) ){
        legend->AddEntry(b2, "Total (w/o lumi.)", "f");
        ++nColumn;
      } else{
        TObject *emptyPtr = nullptr;
        legend->AddEntry(emptyPtr, "", "");
        ++nColumn;
      }
    } else{
      if( i%2==0 ){
        legend->AddEntry(boxForLegend, mSystCheckShortName[i], "fl");
        ++nColumn;
      }
    }
    
    
    
  }
  
  
  TLine *lumiDownLine;
  lumiDownLine = new TLine(mainRatio->GetXaxis()->GetXmin(), 1./(1.0+lumiError), mainRatio->GetXaxis()->GetXmax(), 1./(1.0+lumiError));
  lumiDownLine->SetLineWidth(4/*opt.mLineWidth*/);
  lumiDownLine->SetLineStyle(7);
  lumiDownLine->SetLineColor(kTeal-9);
  lumiDownLine->Draw();
  
//   for( int i=1; i<=mainRatio->GetNbinsX(); ++i ){
//     if( mainRatio->GetBinError(i) > 0 && mainRatio->GetXaxis()->GetBinLowEdge(i) >= opt.mXmin && mainRatio->GetXaxis()->GetBinUpEdge(i) <= opt.mXmax ){
//       lumiDownLine = new TLine(mainRatio->GetXaxis()->GetBinLowEdge(i), 1./(1.0+lumiError), mainRatio->GetXaxis()->GetBinUpEdge(i), 1./(1.0+lumiError));
//       lumiDownLine->SetLineWidth(4/*opt.mLineWidth*/);
//       lumiDownLine->SetLineStyle(2);
//       lumiDownLine->SetLineColor(kTeal-9);
//       lumiDownLine->Draw();
//     }
//   }
  TBox* boxForLegend = new TBox();
  boxForLegend->SetFillColor( opt.mSystErrorBoxColor );
  boxForLegend->SetLineColor( kTeal-9 );
  boxForLegend->SetLineStyle( 7 );
  boxForLegend->SetLineWidth( 5 );
  legend->AddEntry(boxForLegend, "#DeltaLuminosity", "fl");
  ++nColumn;
  
  
  TLine *lumiUpLine;
  lumiUpLine = new TLine(mainRatio->GetXaxis()->GetXmin(), 1./(1.0-lumiError), mainRatio->GetXaxis()->GetXmax(), 1./(1.0-lumiError));
  lumiUpLine->SetLineWidth(4/*opt.mLineWidth*/);
  lumiUpLine->SetLineStyle(7);
  lumiUpLine->SetLineColor(kTeal-9);
  lumiUpLine->Draw();
  
//   for( int i=1; i<=mainRatio->GetNbinsX(); ++i ){
//     if( mainRatio->GetBinError(i) > 0 && mainRatio->GetXaxis()->GetBinLowEdge(i) >= opt.mXmin && mainRatio->GetXaxis()->GetBinUpEdge(i) <= opt.mXmax ){
//       lumiUpLine = new TLine(mainRatio->GetXaxis()->GetBinLowEdge(i), 1./(1.0-lumiError), mainRatio->GetXaxis()->GetBinUpEdge(i), 1./(1.0-lumiError));
//       lumiUpLine->SetLineWidth(4/*opt.mLineWidth*/);
//       lumiUpLine->SetLineStyle(2);
//       lumiUpLine->SetLineColor(kTeal-9);
//       lumiUpLine->Draw();
//     }
//   }
//   legend->AddEntry(lumiUpLine, "Luminosity#downarrow", "l");
  
  while( nColumn%3!=2 ){
    TObject *emptyPtr = nullptr;
    legend->AddEntry(emptyPtr, "", "");
    ++nColumn;
  }
  legend->AddEntry(b, "Total (w/ lumi.)", "f"); 
  
  legend->SetTextSize(opt.mLegendTextSize);
  legend->Draw();
  
  if( opt.mTextToDraw.size() > 0 ){
    for(unsigned int i=0; i<opt.mTextToDraw.size(); ++i){
      opt.mTextToDraw[i].SetNDC(kTRUE);
      opt.mTextToDraw[i].Draw();
    }
  }
  
  line = new TLine(opt.mXmin, 1.0, opt.mXmax, 1.0);
  line->SetLineWidth(3);
  line->SetLineStyle(1);
  line->SetLineColor(kBlack);
  line->Draw();
  gPad->RedrawAxis();
  
  c->Print( TString("PDF/")+opt.mPdfName+"_Systematics.pdf", "EmbedFonts");
  
  
  mainRatio->GetXaxis()->SetTitleSize( mainRatio->GetXaxis()->GetTitleSize() * opt.mCanvasWidth );
  mainRatio->GetYaxis()->SetTitleSize( mainRatio->GetYaxis()->GetTitleSize() * opt.mCanvasWidth );
  mainRatio->GetXaxis()->SetLabelSize( mainRatio->GetXaxis()->GetLabelSize() * opt.mCanvasWidth );
  mainRatio->GetYaxis()->SetLabelSize( mainRatio->GetYaxis()->GetLabelSize() * opt.mCanvasWidth );
  
  mainRatio->GetXaxis()->SetTitleFont( 10*(mainRatio->GetXaxis()->GetTitleFont()/10)+3 );
  mainRatio->GetYaxis()->SetTitleFont( 10*(mainRatio->GetYaxis()->GetTitleFont()/10)+3 );
  mainRatio->GetXaxis()->SetLabelFont( 10*(mainRatio->GetXaxis()->GetLabelFont()/10)+3 );
  mainRatio->GetYaxis()->SetLabelFont( 10*(mainRatio->GetYaxis()->GetLabelFont()/10)+3 );
  
  mainRatio->GetYaxis()->SetTitleOffset( (1.0-0.4+opt.mBottomMargin + 0.05)* opt.mYaxisTitleOffset);
  
  if( opt.mTextToDraw.size() > 0 ){
    for(unsigned int i=0; i<opt.mTextToDraw.size(); ++i){
      opt.mTextToDraw[i].SetX( 0.98*opt.mTextToDraw[i].GetX() );
      opt.mTextToDraw[i].SetY( opt.mTextToDraw[i].GetY() - 0.3*(1.0-opt.mTextToDraw[i].GetY()) );
      opt.mTextToDraw[i].SetTextSize( opt.mTextToDraw[i].GetTextSize() * opt.mCanvasWidth  );
      opt.mTextToDraw[i].SetTextFont( 10*(opt.mTextToDraw[i].GetTextFont()/10)+3 );
    }
  }
  
  c->SetFixedAspectRatio(kFALSE);
  c->SetCanvasSize( opt.mCanvasWidth, (1.0-0.4+opt.mBottomMargin + 0.05)*opt.mCanvasHeight );
  c->GetListOfPrimitives()->Remove(legend);
  c->SetBottomMargin( opt.mBottomMargin );
  c->Modified();
  c->Update();
  c->Print( TString("PDF/")+opt.mPdfName+"_Systematics2.pdf", "EmbedFonts");
  
  
  delete c;
  delete c2;
}

Double_t etaVsPt(Double_t *x, Double_t *par){
  return par[0]*acosh( par[1]/x[0] );
}




void CepAnalysis::getListOfAnalysedRuns(TFile *file){
    
    CepHistograms_EventFlow * hEF = new CepHistograms_EventFlow("EventFlow", false, file);
    mhNEventsVsLumiBlockVsRunNumber = new TH2F( *hEF->mhNEventsVsLumiBlockVsRunNumber );
    delete hEF;
    
    for(int i=1; i<=mhNEventsVsLumiBlockVsRunNumber->GetNbinsX(); ++i){
        TH1D *dNdLumiBlock = mhNEventsVsLumiBlockVsRunNumber->ProjectionY("py", i, i);
        int runNum = static_cast<int>( mhNEventsVsLumiBlockVsRunNumber->GetXaxis()->GetBinCenter(i) );
        if( dNdLumiBlock->GetEntries() > 0 )
            runNumber.push_back( runNum );
        delete dNdLumiBlock;
        
        for(int j=1; j<=mhNEventsVsLumiBlockVsRunNumber->GetNbinsY(); ++j){
            if( mhNEventsVsLumiBlockVsRunNumber->GetBinContent(i,j) > 0 )
                analysedLumiBlockVector_RunMap[runNum].push_back( static_cast<int>( mhNEventsVsLumiBlockVsRunNumber->GetYaxis()->GetBinCenter(j) ) );
        }
    }
}

void CepAnalysis::getTotalIntegratedLuminosity(){
    double integral = 0.0;
    map<int, vector<int> >::iterator it;
    for( it = analysedLumiBlockVector_RunMap.begin(); it != analysedLumiBlockVector_RunMap.end(); it++ ){
        int runNum = it->first;
        for(unsigned int i=0; i<it->second.size(); ++i){
            integral += lumiPerLumiBlockVector_RunMap[runNum][it->second[i]-1] * lumiblockDurationVector_RunMap[runNum][it->second[i]-1]; // mub^-1
            if( !isPhysicsLumiBlockVector_RunMap[runNum][it->second[i]-1] )
                cout << Form("ERROR in CepAnalysis::getTotalIntegratedLuminosity(): LumiBlock %d in run %d was not marked as \"PHYSICS\", but was analysed and contributes to cross sections!", runNum, it->second[i]) << endl;
        }
    }
    totalIntegratedLuminosity = integral;
}

void CepAnalysis::readLumiFiles(){
    for(unsigned int i=0; i<runNumber.size(); ++i){
        string line;
        ifstream lumiFile( Form("offlineLumiInfo/%d.dat", runNumber[i]) );
        if (lumiFile.is_open()){
            while ( getline(lumiFile, line) ){
                string val2, val5;
                int val1;
                double val3, val4;
                stringstream ss(line);
                ss >> val1 >> val2 >> val3 >> val4 >> val5;
                lumiblockDurationVector_RunMap[runNumber[i]].push_back( val3 );
                lumiPerLumiBlockVector_RunMap[runNumber[i]].push_back( val4 );
                isPhysicsLumiBlockVector_RunMap[runNumber[i]].push_back( val5 == "T" );
//                 cout << val1 << " " << val2 << " " << val3 << " " << val4 << " " << val5 << endl;
            }
            lumiFile.close();
        } else{
            cerr << "\nERROR: Problems with opening a file: " << Form("offlineLumiInfo/%d.dat", runNumber[i]) << endl;
        }
    }
}


void CepAnalysis::calculateDeltaZ0Eff(){
    //ALERT
//     gStyle->SetPadTickX(1);
//     gStyle->SetPadTickY(1);
//     
//     double leftMargin = 0.17;
//     double rightMargin = 0.03;
//     double topMargin = 0.03;
//     double bottomMargin = 0.19;
//     double divisionY = 0.4;
//     
//     //------------------ ------------------ ------------------ ------------------ ------------------ ------------------ 
//     TCanvas *c = new TCanvas("c12314r2", "c", 900, 600);
//     TGaxis::SetMaxDigits(3);
//     c->SetFillStyle(4000);
//     c->SetFillColor(0);
//     c->SetFrameFillStyle(4000);
//     c->SetLeftMargin(0);
//     c->SetRightMargin(0);
//     c->SetTopMargin(0);
//     c->SetBottomMargin(0);
//     
//     // Upper plot will be in pad1
//     TPad *pad1 = new TPad("pad1", "pad1", 0, divisionY, 1, 1.0);
//     pad1->SetBottomMargin(0); // Upper and lower plot are joined
//     pad1->SetFillStyle(4000);
//     pad1->SetFillColor(0);
//     pad1->SetFrameFillStyle(4000);
//     pad1->Draw();             // Draw the upper pad: pad1
//     pad1->cd();               // pad1 becomes the current pad
//     gPad->SetLeftMargin(leftMargin);
//     gPad->SetRightMargin(rightMargin);
//     gPad->SetTopMargin(topMargin/(1.0-divisionY));
//     gPad->SetBottomMargin(0);
//     c->cd();          // Go back to the main canvas before defining pad2
//     TPad *pad2 = new TPad("pad2", "pad2", 0, 0, 1, divisionY);
//     pad2->SetFillStyle(4000);
//     pad2->SetFillColor(0);
//     pad2->SetFrameFillStyle(4000);
//     pad2->Draw();
//     pad2->cd();       // pad2 becomes the current pad
//     gPad->SetLeftMargin(leftMargin);
//     gPad->SetRightMargin(rightMargin);
//     gPad->SetTopMargin(0.0);
//     gPad->SetBottomMargin(bottomMargin/divisionY);
//     gStyle->SetGridColor(kGray);
//     gPad->SetGridy();
//     
//     
//     pad1->cd();
//     
//     
//     TGraphAsymmErrors *gDeltaZ0Eff_noBkgdSub[4];
//     TGraphAsymmErrors *gDeltaZ0Eff_bkgdSub[4];
//     
//     for(int i=0; i<3; ++i){
//         
//         
//         TH3F *h3 = nullptr;
//         switch(i){
//             case 0: h3 = mhNSigmaMissingPtVsDeltaZ0VsMinPt_2Pi; break;
//             case 1: h3 = mhNSigmaMissingPtVsDeltaZ0VsMinPt_4Pi; break;
//             case 2: h3 = mhNSigmaMissingPtVsDeltaZ0VsMinPt_6Pi; break;
//             case 3: h3 = mhNSigmaMissingPtVsDeltaZ0VsMinPt_8Pi; break;
//             default: break;
//         }
//         
//         TH2D *mhNSigmaMissingPtVsMinPt_2Pi_All = dynamic_cast<TH2D*>( h3->Project3D("zx") );
//         mhNSigmaMissingPtVsMinPt_2Pi_All->SetName("mhNSigmaMissingPtVsMinPt_2Pi_All");
//         h3->GetYaxis()->SetRange(0, h3->GetYaxis()->FindBin(19.99) );
//         TH2D *mhNSigmaMissingPtVsMinPt_2Pi_DeltaZ0LessThan20 = dynamic_cast<TH2D*>( h3->Project3D("zx") );
//         mhNSigmaMissingPtVsMinPt_2Pi_DeltaZ0LessThan20->SetName("mhNSigmaMissingPtVsMinPt_2Pi_DeltaZ0LessThan20");
//     
//         //-------------------------------------
//         // no bkgd subtraction
//         TH1D *all = mhNSigmaMissingPtVsMinPt_2Pi_All->ProjectionX("lowerPt_All", 0, mhNSigmaMissingPtVsMinPt_2Pi_All->GetYaxis()->FindBin( 2.99 ) );
//         TH1D *vertexed = mhNSigmaMissingPtVsMinPt_2Pi_DeltaZ0LessThan20->ProjectionX("lowerPt_DeltaZ0LessThan2", 0, mhNSigmaMissingPtVsMinPt_2Pi_DeltaZ0LessThan20->GetYaxis()->FindBin( 2.99 ) );
//         TEfficiency *deltaZ0Eff_noBkgdSub = new TEfficiency( *vertexed, *all );
//         gDeltaZ0Eff_noBkgdSub[i] = deltaZ0Eff_noBkgdSub->CreateGraph();
//         //-------------------------------------
//         
//         //-------------------------------------
//         // with bkgd subtraction
//         TH1D *bkgdHistogram_all = new TH1D();
//         TH1D *bkgdHistogram_vertexed = new TH1D();
//         mCepUtil->bkgdHistogram(mhNSigmaMissingPtVsMinPt_2Pi_All, 3 )->Copy( *bkgdHistogram_all );
//         bkgdHistogram_all->SetName("bkgdHistogram_all");
//         mCepUtil->bkgdHistogram(mhNSigmaMissingPtVsMinPt_2Pi_DeltaZ0LessThan20, 3 )->Copy( *bkgdHistogram_vertexed );
//         bkgdHistogram_vertexed->SetName("bkgdHistogram_vertexed");
//         
//         //     TH1D *bkgdToAll = new TH1D(*bkgdHistogram_all);
//         //     bkgdToAll->SetName("bkgdToAll");
//         
//         TH1D *allBkgdSubtracted = new TH1D( *all );
//         allBkgdSubtracted->Add( bkgdHistogram_all, -1 );
//         TH1D *vertexedBkgdSubtracted = new TH1D( *vertexed );
//         vertexedBkgdSubtracted->Add( bkgdHistogram_vertexed, -1 );
//         
//         
//         for(int i=0; i<=allBkgdSubtracted->GetNbinsX()+1; ++i){
//             if( allBkgdSubtracted->GetBinContent(i) < vertexedBkgdSubtracted->GetBinContent(i) ){
//                 allBkgdSubtracted->SetBinContent(i, vertexedBkgdSubtracted->GetBinContent(i));
//             }
//         }
//         
//         TEfficiency *deltaZ0Eff_bkgdSub = new TEfficiency( *vertexedBkgdSubtracted, *allBkgdSubtracted );
//         gDeltaZ0Eff_bkgdSub[i] = deltaZ0Eff_bkgdSub->CreateGraph();
//         //-------------------------------------
//     }
//         
// //     TEfficiency *mcDeltaZ0Eff = dynamic_cast<TEfficiency*>( TFile::Open("vertexingEff/DeltaZ0EfficiencyMC.root")->Get("effDeltaZ0_vs_lowerPt") );
// //     TGraphAsymmErrors *gMcDeltaZ0Eff = mcDeltaZ0Eff->CreateGraph();
//     
//     TMultiGraph *mg = new TMultiGraph();
//     
//     for(int i=0; i<3; ++i){
//         gDeltaZ0Eff_noBkgdSub[i]->SetMarkerSize(1.6);
//         gDeltaZ0Eff_noBkgdSub[i]->SetMarkerStyle(20+i);
//         gDeltaZ0Eff_noBkgdSub[i]->SetMarkerColor(i==0 ? (kGreen+2) : (i==1 ? kBlue : kRed));
//         gDeltaZ0Eff_noBkgdSub[i]->SetLineColor(i==0 ? (kGreen+2) : (i==1 ? kBlue : kRed));
//         
//         gDeltaZ0Eff_bkgdSub[i]->SetMarkerSize(1.8);
//         gDeltaZ0Eff_bkgdSub[i]->SetMarkerStyle(24+i);
//         gDeltaZ0Eff_bkgdSub[i]->SetMarkerColor(kBlack);
//         gDeltaZ0Eff_bkgdSub[i]->SetLineColor(kBlack);
//     //     
//     //     gMcDeltaZ0Eff->SetMarkerSize(1.6);
//     //     gMcDeltaZ0Eff->SetMarkerStyle(21);
//     //     gMcDeltaZ0Eff->SetMarkerColor(kRed);
//     //     gMcDeltaZ0Eff->SetLineColor(kRed);
//     //     
//     //     mg->Add(gMcDeltaZ0Eff, "PE");
//         mg->Add(gDeltaZ0Eff_noBkgdSub[i], "PE");
//         mg->Add(gDeltaZ0Eff_bkgdSub[i], "PE");
//     }
// //     
//     mg->SetTitle("; min(p_{T}) [GeV];Eff. of |#Deltaz_{0}| < 20 mm");
//     mg->Draw("A");
//     
//     
//     TLine* eff1Line = new TLine( 0.1, 1, 1, 1 );
//     eff1Line->SetLineWidth(2);
//     eff1Line->SetLineColor(kGray+2);
//     eff1Line->SetLineStyle(7);
//     eff1Line->Draw();
//     
//     
//     mg->GetYaxis()->SetTitleOffset(0.85);
//     mg->GetXaxis()->SetLimits(0.1, 1);
//     mg->GetXaxis()->SetTickLength( 0.8*mg->GetXaxis()->GetTickLength() );
//     mg->GetYaxis()->SetTickLength( 0.5*mg->GetXaxis()->GetTickLength() );
//     mg->GetYaxis()->SetRangeUser(0.984, 1.001);
//     mg->GetYaxis()->SetTitleSize( 2*mg->GetYaxis()->GetTitleSize() );
//     mg->GetYaxis()->SetLabelSize( 2*mg->GetYaxis()->GetLabelSize() );
//     mg->GetXaxis()->SetTitleSize( 2*mg->GetXaxis()->GetTitleSize() );
//     mg->GetXaxis()->SetLabelSize( 2*mg->GetXaxis()->GetLabelSize() );
// //   
//     mg->GetYaxis()->SetNdivisions(5, 5, 1);
//     mg->GetXaxis()->SetNdivisions(5, 2, 1);
// //     lATLAS.DrawLatex(0.8, 0.86, "#scale[1.2]{ATLAS}");
// //     lATLAS.DrawLatex(0.8, 0.8, "#font[42]{Internal}");
// //     
// //     
//     TLegend *legend = new TLegend(0.53, 0.235, 0.9, 0.59);
//     legend->SetFillColorAlpha(kWhite, 0.8);
//     legend->SetNColumns(2);
//     legend->SetBorderSize(-1);
//     for(int i=0; i<3; ++i){
//         TString multStr(i==0 ? "2#pi" : (i==1 ? "4#pi" : "6#pi"));
//         legend->AddEntry(gDeltaZ0Eff_noBkgdSub[i], multStr, "pel");
//         legend->AddEntry(gDeltaZ0Eff_bkgdSub[i], multStr+" (bkgd sub.)", "pel");
//     }
// //         legend->AddEntry(gMcDeltaZ0Eff, "GenEx #oplus ZeroBias", "pel");
//     legend->SetTextSize( 0.8*mg->GetYaxis()->GetTitleSize() );
//     legend->Draw();
// //     
// //     
// //     l.SetTextSize( 0.8*mg->GetYaxis()->GetTitleSize() );
// //     TString cutsStr = Form("|z_{vtx}|<%d cm,   p_{T}>%.1f GeV", static_cast<int>(mParams->maxZVertex()), mParams->minTpcTrackPt(CepUtil::PION) );
// //     l.DrawLatex( 0.32, 0.88, cutsStr );
// //     cutsStr = Form("|#eta|<%.1f",  mParams->maxEta() );
// //     l.DrawLatex( 0.539, 0.8, cutsStr );
// //     l.SetTextSize(.04);
// //     
// //     
// //     
//         pad2->cd();
//         
//         TMultiGraph *mg2 = new TMultiGraph();
//         
//         for(int j=0; j<4; ++j){
//         
//             TGraphAsymmErrors *gData_NoBkgdSub_minus_BkgdSub = new TGraphAsymmErrors( *gDeltaZ0Eff_noBkgdSub[j] );
//             
//             for(int i=0; i<gData_NoBkgdSub_minus_BkgdSub->GetN(); ++i){
//             double x, y;
//             double xMC, yMC;
//             double errX, errY;
//             gDeltaZ0Eff_bkgdSub[j]->GetPoint(i, xMC, yMC);
//             
//             gData_NoBkgdSub_minus_BkgdSub->GetPoint(i, x, y);
//             y -= yMC;
//             gData_NoBkgdSub_minus_BkgdSub->SetPoint(i, x, y);
//             errX = gData_NoBkgdSub_minus_BkgdSub->GetErrorX(i);
//             errY = 0.0;///*sqrt(pow(*/gData_NoBkgdSub_minus_BkgdSub->GetErrorY(i)/*,2) + pow(gMcDeltaZ0Eff->GetErrorY(i),2))*/;
//             gData_NoBkgdSub_minus_BkgdSub->SetPointError(i, errX, errX, errY, errY);
//             
//             
//             }
//             
//             mg2->Add(gData_NoBkgdSub_minus_BkgdSub, "PE");
//             
//         }
//         
//         mg2->SetTitle("; min(p_{T}) [GeV]; Diff.");
//         mg2->Draw("A");
//         
//         TGaxis::SetMaxDigits(5);
//         mg2->GetYaxis()->SetNdivisions(5, 2, 1);
//         mg2->GetXaxis()->SetNdivisions(5, 2, 1);
//         mg2->GetYaxis()->CenterTitle();
//         mg2->GetYaxis()->SetTitleOffset( 1.05*mg->GetYaxis()->GetTitleOffset() / ((1.0-divisionY)/divisionY) );
//         mg2->GetXaxis()->SetLimits(0.1,1);
//         mg2->GetYaxis()->SetRangeUser(-0.0035, 0.0035);
//         mg2->GetXaxis()->SetTickLength( mg->GetXaxis()->GetTickLength()*(1.0-divisionY)/divisionY );
//         mg2->GetYaxis()->SetTitleSize( 2*mg2->GetYaxis()->GetTitleSize()*(1.0-divisionY)/divisionY );
//         mg2->GetYaxis()->SetLabelSize( 2*mg2->GetYaxis()->GetLabelSize()*(1.0-divisionY)/divisionY );
//         mg2->GetXaxis()->SetTitleSize( 2*mg2->GetXaxis()->GetTitleSize()*(1.0-divisionY)/divisionY );
//         mg2->GetXaxis()->SetLabelSize( 2*mg2->GetXaxis()->GetLabelSize()*(1.0-divisionY)/divisionY );
//         
//         TLine* lineRatioEqual1 = new TLine( 0.1, 0, 1, 0 );
//         lineRatioEqual1->SetLineWidth(2);
//         lineRatioEqual1->SetLineStyle(7);
//         lineRatioEqual1->SetLineColor(kGray+2);
//         lineRatioEqual1->Draw();
// //     
//     c->Print("DeltaZ0Efficiency_2.pdf", "EmbedFonts");
// //     
// //     
// //     TFile *file = new TFile( mAnManager->outputDir()+"/DeltaZ0Eff.root", "RECREATE" );
// //     deltaZ0Eff_bkgdSub->SetName("deltaZ0Eff_bkgdSub");
// //     deltaZ0Eff_bkgdSub->SetDirectory(file);
// //     deltaZ0Eff_noBkgdSub->SetName("deltaZ0Eff_noBkgdSub");
// //     deltaZ0Eff_noBkgdSub->SetDirectory(file);
// //     file->Write();
// //     file->Close();
  
  
  
}



TCanvas* CepAnalysis::drawComparisonAndRatio(TH1* h1, TH1* h2, TH1* g1, TH1* g2, TString xAxisTitle,  TString yAxisTitle,
							TString h1Legend, TString h2Legend, TString g1Legend, TString g2Legend, TString yAxisRatioTitle, TString name,
							double xLegend, double yLegend,
							double minRatio, double maxRatio, TLine* line1, TLine* line2, TPad *padPtr, TString cutsStr, TString validatedSystemStr){
  return drawComparisonAndRatio( h1, h2, g1, g2, nullptr, nullptr, xAxisTitle, yAxisTitle, h1Legend, h2Legend, g1Legend, g2Legend, TString(""), TString(""), yAxisRatioTitle, name, xLegend, yLegend, minRatio, maxRatio, line1, line2, padPtr, cutsStr, validatedSystemStr);
}


TCanvas* CepAnalysis::drawComparisonAndRatio(TH1* h1, TH1* h2, TString xAxisTitle,  TString yAxisTitle,
							TString h1Legend, TString h2Legend, TString yAxisRatioTitle, TString name,
							double xLegend, double yLegend,
							double minRatio, double maxRatio, TLine* line1, TLine* line2, TPad *padPtr, TString cutsStr, TString validatedSystemStr){
  return drawComparisonAndRatio( h1, h2, nullptr, nullptr, nullptr, nullptr, xAxisTitle, yAxisTitle, h1Legend, h2Legend, TString(""), TString(""), TString(""), TString(""), yAxisRatioTitle, name, xLegend, yLegend, minRatio, maxRatio, line1, line2, padPtr, cutsStr, validatedSystemStr);
}


TCanvas* CepAnalysis::drawComparisonAndRatio(TH1* h1, TH1* h2, TH1* g1, TH1* g2,  TH1* j1, TH1* j2, TString xAxisTitle,  TString yAxisTitle,
							TString h1Legend, TString h2Legend, TString g1Legend, TString g2Legend, TString j1Legend, TString j2Legend, TString yAxisRatioTitle, TString name,
							double xLegend, double yLegend,
							double minRatio, double maxRatio, TLine* line1, TLine* line2, TPad *padPtr, TString cutsStr, TString validatedSystemStr){
  TLine* lineRatioEqual0 = 0;
  TH1F* ratio_MCMinusData_to_Data = 0;
  TH1F* ratio_MCMinusData_to_Data_2 = 0;
  TH1F* ratio_MCMinusData_to_Data_3 = 0;
  TLatex lATLAS; lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.065);
  TLatex l; l.SetNDC(); l.SetTextFont(42); l.SetTextSize(.1);
  TLegend *legend;
  
  const double labelTextSize = 0.055;
  const double axisTitleTextSize = 0.055;
  const double divisionY = 0.3;
  const double leftMargin = 0.14;
  const double rightMargin = 0.01;
  const double topMargin = 0.042;
  const double bottomMargin = 0.13;
  
  //------------------ ------------------ ------------------ ------------------ ------------------ ------------------ 
  TCanvas *c = new TCanvas("c", "c", 800, 800);
  TGaxis::SetMaxDigits(3);
  c->SetFillStyle(4000);
  c->SetFillColor(0);
  c->SetFrameFillStyle(4000);
  c->SetLeftMargin(0);
  c->SetRightMargin(0);
  c->SetTopMargin(0);
  c->SetBottomMargin(0);
  const double specialTextSize = 0.03;
  TLatex lSpecial; lSpecial.SetNDC(); lSpecial.SetTextFont(42); lSpecial.SetTextSize(specialTextSize);
  lSpecial.SetTextColor(kGray+2);
  lSpecial.DrawLatex( 0.25, 1.-topMargin+0.01, cutsStr);
  lSpecial.DrawLatex( xLegend+0.02, yLegend + 0.07, validatedSystemStr);
  
  // Upper plot will be in pad1
  TPad *pad1 = new TPad("pad1", "pad1", 0, divisionY, 1, 1.0);
  pad1->SetBottomMargin(0); // Upper and lower plot are joined
  pad1->SetFillStyle(4000);
  pad1->SetFillColor(0);
  pad1->SetFrameFillStyle(4000);
  pad1->Draw();             // Draw the upper pad: pad1
  pad1->cd();               // pad1 becomes the current pad
  gPad->SetLeftMargin(leftMargin);
  gPad->SetRightMargin(rightMargin);
  gPad->SetTopMargin(topMargin/(1.0-divisionY));
  gPad->SetBottomMargin(0);
  c->cd();          // Go back to the main canvas before defining pad2
  TPad *pad2 = new TPad("pad2", "pad2", 0, 0, 1, divisionY);
  pad2->SetFillStyle(4000);
  pad2->SetFillColor(0);
  pad2->SetFrameFillStyle(4000);
  pad2->Draw();
  pad2->cd();       // pad2 becomes the current pad
  gPad->SetLeftMargin(leftMargin);
  gPad->SetRightMargin(rightMargin);
  gPad->SetTopMargin(0.0);
  gPad->SetBottomMargin(bottomMargin/divisionY);
  gStyle->SetGridColor(kGray);
  gPad->SetGridy();
    
  
  pad1->cd();
  
  h1->GetYaxis()->SetTitleOffset(1.2);
//   h1->GetXaxis()->SetRangeUser(-1.5, 1.5);
  h1->GetXaxis()->SetTitle(xAxisTitle);
  h1->GetXaxis()->SetTitleSize(axisTitleTextSize);
  h1->GetYaxis()->SetTitle(yAxisTitle);
  h1->GetYaxis()->SetTitleSize(axisTitleTextSize);
  h1->GetXaxis()->SetTitleOffset(1.4);
  h1->GetXaxis()->SetNdivisions(10, 4, 1);
  h1->GetXaxis()->SetTickLength(0.015);
  h1->GetYaxis()->SetTickLength(0.015);
  h1->GetXaxis()->SetLabelSize(labelTextSize);
  h1->GetYaxis()->SetLabelSize(labelTextSize);
  h1->GetXaxis()->SetLabelOffset(0.045);
  h1->SetLineWidth(2);
  h1->SetMarkerColor( kBlack );
  h1->SetMarkerStyle( 24 );
  h1->SetMarkerSize( 1.7 );
  h1->SetLineColor( kBlack );
  h1->SetMinimum(0.1);
  h1->SetMaximum( 1.2*h1->GetMaximum() );
  h1->Draw(/*"PE0"*/"HIST");
  
  if(g1){
    g1->GetYaxis()->SetTitleOffset(1.06);
  //   g1->GetXaxis()->SetRangeUser(-1.5, 1.5);
    g1->GetXaxis()->SetTitle(xAxisTitle);
    g1->GetXaxis()->SetTitleSize(axisTitleTextSize);
    g1->GetYaxis()->SetTitle(yAxisTitle);
    g1->GetYaxis()->SetTitleSize(axisTitleTextSize);
    g1->GetXaxis()->SetTitleOffset(1.08);
    g1->GetXaxis()->SetNdivisions(10, 4, 1);
    g1->GetXaxis()->SetTickLength(0.015);
    g1->GetXaxis()->SetLabelSize(labelTextSize);
    g1->GetYaxis()->SetLabelSize(labelTextSize);
    g1->GetXaxis()->SetLabelOffset(0.045);
    g1->SetLineWidth(2);
    g1->SetMarkerColor( kBlack );
    g1->SetMarkerStyle( 24 );
    g1->SetMarkerSize( 1.7 );
    g1->SetLineColor( kRed-6 );
    g1->SetFillColorAlpha( kRed-6, 0.3 );
    g1->SetMinimum(0.1);
    g1->SetMaximum( 1.2*g1->GetMaximum() );
    g1->Draw(/*"PE0 SAME"*/"HIST SAME");
  }
  
  h2->Draw("PE0 SAME");
  h2->SetMarkerStyle(20);
  h2->SetMarkerSize( 1.3 );
  h2->SetMarkerColor(kRed);
  h2->SetLineWidth(2);
  h2->SetLineColor(kRed);
  
  if(g2){
    g2->Draw("PE0 SAME");
    g2->SetMarkerStyle(22);
    g2->SetMarkerSize( 1.3 );
    g2->SetMarkerColor(kBlue);
    g2->SetLineColor(kBlue);
  }
  if(j2){
    j2->Draw("PE0 SAME");
    j2->SetMarkerStyle(23);
    j2->SetMarkerSize( 1.3 );
    j2->SetMarkerColor(kGreen+2);
    j2->SetLineColor(kGreen+2);
  }
  
  legend = new TLegend(xLegend, yLegend, xLegend+mLegendWidth, yLegend+mLegendHeight);
  legend->SetFillStyle(0);
  legend->SetBorderSize(0);
  legend->SetTextSize(0.8*labelTextSize);
  legend->AddEntry( h1, h1Legend, "f" );
  if(g1) legend->AddEntry( g1, g1Legend, "f" );
  legend->AddEntry( h2, h2Legend, "pel" );
//   if(g2) legend->AddEntry( g2, g2Legend, "pel" );
  if(j2) legend->AddEntry( j2, j2Legend, "pel" );
  legend->Draw();

  if( line1 ){
    line1->SetY2( 0.8*h1->GetMaximum() );
    line1->SetLineColor(kBlack);
    line1->SetLineWidth(6);
    line1->SetLineStyle(7);
    line1->Draw();
  }
  if( line2 ){
    line2->SetY2( 0.8*h1->GetMaximum() );
    line2->SetLineColor(kBlack);
    line2->SetLineWidth(6);
    line2->SetLineStyle(7);
    line2->Draw();
  }

  lATLAS.DrawLatex(0.18, 0.85, "ATLAS #font[42]{Internal}");
  
  gPad->RedrawAxis();
    
  pad2->cd();
  
  double chiSq = 0; int ndf = 0;
  double chiSq_2 = 0; int ndf_2 = 0;
  double chiSq_3 = 0; int ndf_3 = 0;
  int minBinForChiSq = 0;
  int maxBinForChiSq = h1->GetNbinsX()+1;
  if( line1 ){
    double minX = line1->GetX1();
    minBinForChiSq = h1->FindBin(minX);
  }
  if( line2 ){
    double maxX = line2->GetX1();
    maxBinForChiSq = h1->FindBin(maxX);
    if( fabs(maxX - h1->GetXaxis()->GetBinLowEdge(maxBinForChiSq)) < 1e-6 ){ //ALERT!!!!!!!!!!!!
//       cout << "AAAAAAAAA\t\t\t" << maxX  << "  " << h1->GetXaxis()->GetBinLowEdge(maxBinForChiSq) << endl;
      --maxBinForChiSq;
    }
  }
  
  ratio_MCMinusData_to_Data = new TH1F( *dynamic_cast<TH1F*>(h1) );
  if(g1&&g2) ratio_MCMinusData_to_Data_2 = new TH1F( *dynamic_cast<TH1F*>(g1) );
  if(j1&&j2) ratio_MCMinusData_to_Data_3 = new TH1F( *dynamic_cast<TH1F*>(j1) );
  for(int i=0; i<=(ratio_MCMinusData_to_Data->GetNbinsX()+1); ++i){
    if( ratio_MCMinusData_to_Data->GetBinContent(i) > 0 ){
      if( h2->GetBinContent(i)>0 && i>=minBinForChiSq && i<=maxBinForChiSq ){
	chiSq += (h2->GetBinContent(i)-ratio_MCMinusData_to_Data->GetBinContent(i))*(h2->GetBinContent(i)-ratio_MCMinusData_to_Data->GetBinContent(i)) / (h2->GetBinError(i)*h2->GetBinError(i));
	++ndf;
      }
      ratio_MCMinusData_to_Data->SetBinError(i, h2->GetBinError(i)/ratio_MCMinusData_to_Data->GetBinContent(i));
      ratio_MCMinusData_to_Data->SetBinContent(i, (h2->GetBinContent(i)-ratio_MCMinusData_to_Data->GetBinContent(i))/ratio_MCMinusData_to_Data->GetBinContent(i) );
    } else{ // to prevent showing up of points without any entries when using E0 drawing option
      ratio_MCMinusData_to_Data->SetBinError(i, 0);
      ratio_MCMinusData_to_Data->SetBinContent(i, 1e9 );      
    }
  }
  if(ratio_MCMinusData_to_Data_2){
    for(int i=0; i<=(ratio_MCMinusData_to_Data_2->GetNbinsX()+1); ++i){
      if( ratio_MCMinusData_to_Data_2->GetBinContent(i) > 0 ){
	if( g2->GetBinContent(i)>0 && i>=minBinForChiSq && i<=maxBinForChiSq ){
	  chiSq_2 += (g2->GetBinContent(i)-ratio_MCMinusData_to_Data_2->GetBinContent(i))*(g2->GetBinContent(i)-ratio_MCMinusData_to_Data_2->GetBinContent(i)) / (g2->GetBinError(i)*g2->GetBinError(i));
	  ++ndf_2;
	}
	ratio_MCMinusData_to_Data_2->SetBinError(i, g2->GetBinError(i)/ratio_MCMinusData_to_Data_2->GetBinContent(i));
	ratio_MCMinusData_to_Data_2->SetBinContent(i, (g2->GetBinContent(i)-ratio_MCMinusData_to_Data_2->GetBinContent(i))/ratio_MCMinusData_to_Data_2->GetBinContent(i) );
      } else{ // to prevent showing up of points without any entries when using E0 drawing option
	ratio_MCMinusData_to_Data_2->SetBinError(i, 0);
	ratio_MCMinusData_to_Data_2->SetBinContent(i, 1e9 );      
      }
    }
  }
  if(ratio_MCMinusData_to_Data_3){
    for(int i=0; i<=(ratio_MCMinusData_to_Data_3->GetNbinsX()+1); ++i){
      if( ratio_MCMinusData_to_Data_3->GetBinContent(i) > 0 ){
	if( j2->GetBinContent(i)>0 && i>=minBinForChiSq && i<=maxBinForChiSq ){
	  chiSq_3 += (j2->GetBinContent(i)-ratio_MCMinusData_to_Data_3->GetBinContent(i))*(j2->GetBinContent(i)-ratio_MCMinusData_to_Data_3->GetBinContent(i)) / (j2->GetBinError(i)*j2->GetBinError(i));
	  ++ndf_3;
	}
	ratio_MCMinusData_to_Data_3->SetBinError(i, j2->GetBinError(i)/ratio_MCMinusData_to_Data_3->GetBinContent(i));
	ratio_MCMinusData_to_Data_3->SetBinContent(i, (j2->GetBinContent(i)-ratio_MCMinusData_to_Data_3->GetBinContent(i))/ratio_MCMinusData_to_Data_3->GetBinContent(i) );
      } else{ // to prevent showing up of points without any entries when using E0 drawing option
	ratio_MCMinusData_to_Data_3->SetBinError(i, 0);
	ratio_MCMinusData_to_Data_3->SetBinContent(i, 1e9 );      
      }
    }
  }
  
  ratio_MCMinusData_to_Data->SetMarkerStyle(h2->GetMarkerStyle());
  ratio_MCMinusData_to_Data->SetMarkerColor(h2->GetMarkerColor());
  ratio_MCMinusData_to_Data->SetLineColor(h2->GetLineColor());
  ratio_MCMinusData_to_Data->GetYaxis()->SetRangeUser(minRatio, maxRatio);
  ratio_MCMinusData_to_Data->GetYaxis()->SetNdivisions(5, 2, 0);
  ratio_MCMinusData_to_Data->GetYaxis()->CenterTitle();
  ratio_MCMinusData_to_Data->GetYaxis()->SetTitle(yAxisRatioTitle);
  ratio_MCMinusData_to_Data->GetYaxis()->SetTitleOffset( h1->GetYaxis()->GetTitleOffset() / ((1.0-divisionY)/divisionY) );
  ratio_MCMinusData_to_Data->GetYaxis()->SetTitleSize( h1->GetYaxis()->GetTitleSize()*(1.0-divisionY)/divisionY );
  ratio_MCMinusData_to_Data->GetYaxis()->SetLabelSize( h1->GetYaxis()->GetLabelSize()*(1.0-divisionY)/divisionY );
  ratio_MCMinusData_to_Data->GetYaxis()->SetTickLength( h1->GetYaxis()->GetTickLength() );
  ratio_MCMinusData_to_Data->GetXaxis()->SetTitleSize( h1->GetXaxis()->GetTitleSize()*(1.0-divisionY)/divisionY );
  ratio_MCMinusData_to_Data->GetXaxis()->SetLabelSize( h1->GetXaxis()->GetLabelSize()*(1.0-divisionY)/divisionY );
  ratio_MCMinusData_to_Data->GetXaxis()->SetTickLength( h1->GetXaxis()->GetTickLength()*(1.0-divisionY)/divisionY );
  
  if( ratio_MCMinusData_to_Data_2 ){
    ratio_MCMinusData_to_Data_2->SetMarkerStyle(22);
    ratio_MCMinusData_to_Data_2->SetMarkerColor(kBlue);
    ratio_MCMinusData_to_Data_2->SetLineColor(kBlue);
    ratio_MCMinusData_to_Data_2->GetYaxis()->SetRangeUser(minRatio, maxRatio);
    ratio_MCMinusData_to_Data_2->GetYaxis()->SetNdivisions(5, 2, 0);
    ratio_MCMinusData_to_Data_2->GetYaxis()->CenterTitle();
    ratio_MCMinusData_to_Data_2->GetYaxis()->SetTitle(yAxisRatioTitle);
    ratio_MCMinusData_to_Data_2->GetYaxis()->SetTitleOffset( h1->GetYaxis()->GetTitleOffset() / ((1.0-divisionY)/divisionY) );
    ratio_MCMinusData_to_Data_2->GetYaxis()->SetTitleSize( h1->GetYaxis()->GetTitleSize()*(1.0-divisionY)/divisionY );
    ratio_MCMinusData_to_Data_2->GetYaxis()->SetLabelSize( h1->GetYaxis()->GetLabelSize()*(1.0-divisionY)/divisionY );
    ratio_MCMinusData_to_Data_2->GetYaxis()->SetTickLength( h1->GetYaxis()->GetTickLength() );
    ratio_MCMinusData_to_Data_2->GetXaxis()->SetTitleSize( h1->GetXaxis()->GetTitleSize()*(1.0-divisionY)/divisionY );
    ratio_MCMinusData_to_Data_2->GetXaxis()->SetLabelSize( h1->GetXaxis()->GetLabelSize()*(1.0-divisionY)/divisionY );
    ratio_MCMinusData_to_Data_2->GetXaxis()->SetTickLength( h1->GetXaxis()->GetTickLength()*(1.0-divisionY)/divisionY );
  }
  
  if( ratio_MCMinusData_to_Data_3 ){
    ratio_MCMinusData_to_Data_3->SetMarkerStyle(23);
    ratio_MCMinusData_to_Data_3->SetMarkerColor(kGreen+2);
    ratio_MCMinusData_to_Data_3->SetLineColor(kGreen+2);
    ratio_MCMinusData_to_Data_3->GetYaxis()->SetRangeUser(minRatio, maxRatio);
    ratio_MCMinusData_to_Data_3->GetYaxis()->SetNdivisions(5, 2, 0);
    ratio_MCMinusData_to_Data_3->GetYaxis()->CenterTitle();
    ratio_MCMinusData_to_Data_3->GetYaxis()->SetTitle(yAxisRatioTitle);
    ratio_MCMinusData_to_Data_3->GetYaxis()->SetTitleOffset( h1->GetYaxis()->GetTitleOffset() / ((1.0-divisionY)/divisionY) );
    ratio_MCMinusData_to_Data_3->GetYaxis()->SetTitleSize( h1->GetYaxis()->GetTitleSize()*(1.0-divisionY)/divisionY );
    ratio_MCMinusData_to_Data_3->GetYaxis()->SetLabelSize( h1->GetYaxis()->GetLabelSize()*(1.0-divisionY)/divisionY );
    ratio_MCMinusData_to_Data_3->GetYaxis()->SetTickLength( h1->GetYaxis()->GetTickLength() );
    ratio_MCMinusData_to_Data_3->GetXaxis()->SetTitleSize( h1->GetXaxis()->GetTitleSize()*(1.0-divisionY)/divisionY );
    ratio_MCMinusData_to_Data_3->GetXaxis()->SetLabelSize( h1->GetXaxis()->GetLabelSize()*(1.0-divisionY)/divisionY );
    ratio_MCMinusData_to_Data_3->GetXaxis()->SetTickLength( h1->GetXaxis()->GetTickLength()*(1.0-divisionY)/divisionY );
  }
  
  lineRatioEqual0 = new TLine( ratio_MCMinusData_to_Data->GetXaxis()->GetBinLowEdge(ratio_MCMinusData_to_Data->GetXaxis()->GetFirst()), 0, ratio_MCMinusData_to_Data->GetXaxis()->GetBinUpEdge(ratio_MCMinusData_to_Data->GetXaxis()->GetLast()), 0 );
  lineRatioEqual0->SetLineWidth(2);
  lineRatioEqual0->SetLineStyle(7);
  lineRatioEqual0->SetLineColor(kGray+2);
  
  ratio_MCMinusData_to_Data->Draw("E0");
  if(ratio_MCMinusData_to_Data_2) ratio_MCMinusData_to_Data_2->Draw("E0 SAME");
  if(ratio_MCMinusData_to_Data_3) ratio_MCMinusData_to_Data_3->Draw("E0 SAME"); 
  lineRatioEqual0->Draw();
//   l.SetTextColor(kBlack);
//   l.DrawLatex(0.1+0.045, 0.32+0.15, Form("#chi^{2}/ndf = %.1f/%d", chiSq, ndf) );
//   if(ratio_MCMinusData_to_Data_2){
//     l.SetTextColor(kBlue);
//     l.DrawLatex(0.34+0.045, 0.32+0.15, Form("#chi^{2}/ndf = %.1f/%d", chiSq_2, ndf_2) );
//   }
//   if(ratio_MCMinusData_to_Data_3) {
//     l.SetTextColor(kGreen+2);
//     l.DrawLatex(0.58+0.045, 0.32+0.15, Form("#chi^{2}/ndf = %.1f/%d", chiSq_3, ndf_3) );
//   }
  
  
  
    
  if( mPrint ){
    c->Print(mPdfName+".pdf"+mPdfNameSuffix);
//     c->Print(name);
  }
  
  if( padPtr ){
    padPtr->cd();
    pad1->Draw();
    pad2->Draw();
    lSpecial.DrawLatex( 0.39, 0.975, cutsStr + "     " + validatedSystemStr );
  }
  
  return c;
}




void CepAnalysis::setupDrawingStyle() const{
    
    gROOT->SetStyle("ATLAS");
    gStyle->SetErrorX(0.5); //to draw horizontal lines despite the ATLAS style
    
  // setup the drawing style
  gStyle->SetLineScalePS();
  gStyle->SetFrameBorderMode(0);
  gStyle->SetFrameFillColor(0);
  gStyle->SetFrameLineWidth(2);
  gStyle->SetLineWidth(2);
  gStyle->SetHatchesLineWidth(2);
  gStyle->SetCanvasBorderMode(0);
  gStyle->SetCanvasColor(0);
  gStyle->SetPadBorderMode(0);
  gStyle->SetPadColor(0);
  gStyle->SetStatColor(0);
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
  gStyle->SetNumberContours(99);
  gStyle->SetPalette(55);
  gStyle->SetHistLineWidth(3.);
  gStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes
  
//   setBlackAndWhiteColorScale();
}

void CepAnalysis::setBlackAndWhiteColorScale(){
  const int nSteps = 100;
  int acceptancePalette[nSteps];
  const unsigned int Number = 2;
  double Red[Number]   = { 1.00, 0.00 };
  double Green[Number] = { 1.00, 0.00 };
  double Blue[Number]  = { 1.00, 0.00 };
  double Stops[Number] = { 0.00, 1.00 };
  
  int fi = TColor::CreateGradientColorTable(Number,Stops,Red,Green,Blue, nSteps);
  for(int i=0; i<nSteps; ++i)
    acceptancePalette[i] = fi+i;
  gStyle->SetPalette(nSteps, acceptancePalette);
}

void CepAnalysis::setWhiteAndBlackColorScale(){
  const int nSteps = 100;
  int acceptancePalette[nSteps];
  const unsigned int Number = 2;
  double Red[Number]   = { 0.00, 1.00 };
  double Green[Number] = { 0.00, 1.00 };
  double Blue[Number]  = { 0.00, 1.00 };
  double Stops[Number] = { 0.00, 1.00 };
  
  int fi = TColor::CreateGradientColorTable(Number,Stops,Red,Green,Blue, nSteps);
  for(int i=0; i<nSteps; ++i)
    acceptancePalette[i] = fi+i;
  gStyle->SetPalette(nSteps, acceptancePalette);
}

void CepAnalysis::setBlueWhiteRedColorScale(){
  const int nSteps = 100;
  int acceptancePalette[nSteps];
  const unsigned int Number = 3;
  double Red[Number]   = { 0.00, 1.00, 1.00 };
  double Green[Number] = { 0.00, 1.00, 0.00 };
  double Blue[Number]  = { 1.00, 1.00, 0.00 };
  double Stops[Number] = { 0.00, 0.50, 1.00 };
  
  int fi = TColor::CreateGradientColorTable(Number,Stops,Red,Green,Blue, nSteps);
  for(int i=0; i<nSteps; ++i)
    acceptancePalette[i] = fi+i;
  gStyle->SetPalette(nSteps, acceptancePalette);
}
