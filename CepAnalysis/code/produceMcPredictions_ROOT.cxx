#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TF2.h>
#include <TFile.h>
#include <TTree.h>
#include "TMath.h"
#include <TLorentzVector.h>
#include <TVector2.h>
#include <cmath>
#include <complex>
#include <iostream>
#include <fstream>
#include <TRandom3.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TObjString.h>
#include <TObjArray.h>
#include <TLorentzRotation.h>
#include <TEfficiency.h>
#include <TFitResult.h>
#include <TCanvas.h>
#include "Math/DistFunc.h"
#include "Math/SpecFunc.h"
#include "produceMcPredictions_ROOT.hh"
#include "CepRecoEventAdresses.h"
#include "CepObservableObject.h"

using namespace std;



produceMcPredictions_ROOT::produceMcPredictions_ROOT(TString /*option*/){
  mCepUtil = CepUtil::instance();
  mGen = new TRandom3(36064);
  
//   runMode = 0; // 2prong
  runMode = 1; // 4prong

  readInDetResolution();
}

produceMcPredictions_ROOT::~produceMcPredictions_ROOT(){
}

void produceMcPredictions_ROOT::run(){
  
    minPt[CepUtil::PION] = 0.1/*0.15*/;
    minPt[CepUtil::KAON] = 0.3;
    minPt[CepUtil::PROTON] = 0.4;
    
    oneTrackMaxPt[CepUtil::PION] = 999;
    oneTrackMaxPt[CepUtil::KAON] = 0.7;
    oneTrackMaxPt[CepUtil::PROTON] = 1.1;
  
    TH1::SetDefaultSumw2();
    vector<TString> fileNameVec[CepUtil::nDefinedParticles];
    vector<double> weightsVec[CepUtil::nDefinedParticles];
    
    vector<TString> fileName4ProngVec[CepUtil::nDefinedParticles];
    vector<double> weights4ProngVec[CepUtil::nDefinedParticles];
    
    
    
    // ------- angular studies -------
    TF2 *angularDist_S0m = new TF2("angularDist_S0m", this, &produceMcPredictions_ROOT::intensityFromWaves, -TMath::Pi(), TMath::Pi(), 0, TMath::Pi(), 12);
    TF2 *angularDist_D0m = new TF2("angularDist_D0m", this, &produceMcPredictions_ROOT::intensityFromWaves, -TMath::Pi(), TMath::Pi(), 0, TMath::Pi(), 12);
    angularDist_S0m->SetNpx(5e2); angularDist_S0m->SetNpy(5e2);
    angularDist_D0m->SetNpx(5e2); angularDist_D0m->SetNpy(5e2);
    for(int i=0; i<nWaveParameters; ++i){
        angularDist_S0m->FixParameter(i, 0.0);
        angularDist_D0m->FixParameter(i, 0.0);
    }
    //S-wave
    angularDist_S0m->FixParameter(S0m_MAG, 1.0);
    //D-wave
    angularDist_D0m->FixParameter(D0m_MAG, 1.0);
    // ------- ------- ------- -------
    
    
//     createBinnings();

    
    fileNameVec[CepUtil::PION].push_back( "ROOT_files/inputTreesMC/Pythia8244_fullPhi.root" ); weightsVec[CepUtil::PION].push_back( 0.25 );
//     fileNameVec[CepUtil::PION].push_back( "ROOT_files/inputTreesMC/Pythia8244.root" ); weightsVec[CepUtil::PION].push_back( 0.25 );
//     fileNameVec[CepUtil::KAON].push_back( "ROOT_files/inputTreesMC/Pythia8244.root" ); weightsVec[CepUtil::KAON].push_back( 0.25 );
//     fileNameVec[CepUtil::PROTON].push_back( "ROOT_files/inputTreesMC/Pythia8244.root" ); weightsVec[CepUtil::PROTON].push_back( 0.25 );
    
    fileNameVec[CepUtil::PION].push_back( "ROOT_files/inputTreesMC/GenEx_Lambda1p0_fullPhi.root" ); weightsVec[CepUtil::PION].push_back( 0.1 );
    fileNameVec[CepUtil::PION].push_back( "ROOT_files/inputTreesMC/GenEx_Lambda1p0_Extended.root" ); weightsVec[CepUtil::PION].push_back( 0.1 );
    fileNameVec[CepUtil::PION].push_back( "ROOT_files/inputTreesMC/GenEx_Lambda1p0.root" ); weightsVec[CepUtil::PION].push_back( 0.1 );
    fileNameVec[CepUtil::PION].push_back( "ROOT_files/inputTreesMC/DiMe_CepTree_pipi.root" ); weightsVec[CepUtil::PION].push_back( 1.0 );
    fileNameVec[CepUtil::PION].push_back( "ROOT_files/inputTreesMC/DiMe_CepTree_pipi_fullPhi.root" ); weightsVec[CepUtil::PION].push_back( 1.0 );

    fileName4ProngVec[CepUtil::PION].push_back( "ROOT_files/inputTreesMC/Pythia8244_4Prong.root" ); weights4ProngVec[CepUtil::PION].push_back( 1.0 );
    fileName4ProngVec[CepUtil::PION].push_back( "ROOT_files/inputTreesMC/DiMe_CepTree_rhorho.root" ); weights4ProngVec[CepUtil::PION].push_back( 100.0 );
    fileName4ProngVec[CepUtil::PION].push_back( "ROOT_files/inputTreesMC/DiMe_CepTree_rhorho_b_0p45.root" ); weights4ProngVec[CepUtil::PION].push_back( 5.0 );
    
    
    
    fileNameVec[CepUtil::PION].push_back( "ROOT_files/inputTreesMC/GenEx_Lambda1p0_noPtEtaCut.root" ); weightsVec[CepUtil::PION].push_back( 0.1 );
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    TFile* inputFile = TFile::Open( "ROOT_files/inputTreesMC/Output_pion_GenEx_Lambda1p0_noPtEtaCut.root", "READ" );
    TH1F *invMassFromGenEx_S0m[CepUtil::nDeltaPhiRanges];
    TH1F *invMassFromGenEx_D0m[CepUtil::nDeltaPhiRanges];
    for(int i=0; i<CepUtil::nDeltaPhiRanges; ++i){
        TString deltaPhiRangeShortStr;
        invMassFromGenEx_S0m[i] = dynamic_cast<TH1F*>( inputFile->Get( Form("h_2PiInvMass_S0m_DeltaPhiBins%d_ChSum0_Weighted", i) ) );
        invMassFromGenEx_S0m[i]->SetDirectory(0);
        invMassFromGenEx_D0m[i] = dynamic_cast<TH1F*>( inputFile->Get( Form("h_2PiInvMass_D0m_DeltaPhiBins%d_ChSum0_Weighted", i) ) );
        invMassFromGenEx_D0m[i]->SetDirectory(0);
    }
    inputFile->Close();
    
    inputFile = TFile::Open( "ROOT_files/inputTreesMC/XSec2Pi.root", "READ" );
    TH1F *invMassFromData[CepUtil::nDeltaPhiRanges];
    for(int i=0; i<CepUtil::nDeltaPhiRanges; ++i){        
        invMassFromData[i] = dynamic_cast<TH1F*>( inputFile->Get( Form("tmph_2PiInvMass_DeltaPhiBins%d_ChSum0_Weighted", i) ) );
        invMassFromData[i]->SetDirectory(0);
    }
    inputFile->Close();
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    auto fileVec = runMode==0 ? fileNameVec : fileName4ProngVec;
    auto wVec = runMode==0 ? weightsVec : weights4ProngVec;
    
    for(int particle=0; particle<CepUtil::nDefinedParticles; ++particle){
        mParticlesId = particle;
        
        for(unsigned int file=0; file<fileVec[particle].size(); ++file){
            
            const double weightPid = wVec[particle][file];
            totalNEvents = 0;
            totalLumi = 0;
            
            TFile *fInput = new TFile(fileVec[particle][file], "READ");
            
            TTreeReader cepTreeReader("CepTree", fInput);
            TTreeReaderValue<int> pid(cepTreeReader, "pid");
            TTreeReaderValue<float> luminosity(cepTreeReader, "luminosity");
            TTreeReaderValue<float> pxProton1(cepTreeReader, "pxProton1");
            TTreeReaderValue<float> pxProton2(cepTreeReader, "pxProton2");
            TTreeReaderValue<float> pyProton1(cepTreeReader, "pyProton1");
            TTreeReaderValue<float> pyProton2(cepTreeReader, "pyProton2");
            TTreeReaderValue<float> pzProton1(cepTreeReader, "pzProton1");
            TTreeReaderValue<float> pzProton2(cepTreeReader, "pzProton2");
            
            TTreeReaderValue<float> * pxPlus;
            TTreeReaderValue<float> * pxMinus;
            TTreeReaderValue<float> * pyPlus;
            TTreeReaderValue<float> * pyMinus;
            TTreeReaderValue<float> * pzPlus;
            TTreeReaderValue<float> * pzMinus;
            
            TTreeReaderValue<float> * pxMinus1;
            TTreeReaderValue<float> * pyMinus1;
            TTreeReaderValue<float> * pzMinus1;
            TTreeReaderValue<float> * pxMinus2;
            TTreeReaderValue<float> * pyMinus2;
            TTreeReaderValue<float> * pzMinus2;
            TTreeReaderValue<float> * pxPlus1;
            TTreeReaderValue<float> * pyPlus1;
            TTreeReaderValue<float> * pzPlus1;
            TTreeReaderValue<float> * pxPlus2;
            TTreeReaderValue<float> * pyPlus2;
            TTreeReaderValue<float> * pzPlus2;
            
            if( runMode == 0){
                pxPlus = new TTreeReaderValue<float>(cepTreeReader, "pxPlus");
                pxMinus = new TTreeReaderValue<float>(cepTreeReader, "pxMinus");
                pyPlus = new TTreeReaderValue<float>(cepTreeReader, "pyPlus");
                pyMinus = new TTreeReaderValue<float>(cepTreeReader, "pyMinus");
                pzPlus = new TTreeReaderValue<float>(cepTreeReader, "pzPlus");
                pzMinus = new TTreeReaderValue<float>(cepTreeReader, "pzMinus");
            } else if( runMode == 1){
                pxMinus1 = new TTreeReaderValue<float>(cepTreeReader, "pxMinus1");
                pyMinus1 = new TTreeReaderValue<float>(cepTreeReader, "pyMinus1");
                pzMinus1 = new TTreeReaderValue<float>(cepTreeReader, "pzMinus1");
                pxMinus2 = new TTreeReaderValue<float>(cepTreeReader, "pxMinus2");
                pyMinus2 = new TTreeReaderValue<float>(cepTreeReader, "pyMinus2");
                pzMinus2 = new TTreeReaderValue<float>(cepTreeReader, "pzMinus2");
                pxPlus1 = new TTreeReaderValue<float>(cepTreeReader, "pxPlus1");
                pyPlus1 = new TTreeReaderValue<float>(cepTreeReader, "pyPlus1");
                pzPlus1 = new TTreeReaderValue<float>(cepTreeReader, "pzPlus1");
                pxPlus2 = new TTreeReaderValue<float>(cepTreeReader, "pxPlus2");
                pyPlus2 = new TTreeReaderValue<float>(cepTreeReader, "pyPlus2");
                pzPlus2 = new TTreeReaderValue<float>(cepTreeReader, "pzPlus2");
            } else cout << "ERROR Wrong runMode!" << endl;
            
            TObjArray *tokenizedName = fileVec[particle][file].Tokenize("/");
            TString nameStr = ((TObjString *)(tokenizedName->At(tokenizedName->GetLast())))->String();
            TFile *fHistFile = new TFile("ROOT_files/producedCrossSectionPredictions/Output_"+mCepUtil->mParticleName[particle]+"_"+nameStr, "RECREATE");
            
            //BEGIN - - - - - - - - - - - - -  setting up adresses  - - - - - - - - - - - - - - - - - 
            CepRecoEventAdresses *mCepRecoEventAdresses = new CepRecoEventAdresses(false);
            
            CepUtil::CEP_CHANNELS mPid = static_cast<CepUtil::CEP_CHANNELS>( mParticlesId );
            CepUtil::TOTAL_CHARGE chSum = CepUtil::CH_SUM_0;
            double totalWeight = weightPid*specialWeightFactor;
            double nSigmaMissingPt = 0;
            double nSigmaMissingPt_other = 9e9;
            double mMass;
            double mDeltaPhiDegrees;
            double mPairRapidity;
            double mMandelstamTSum;
            
            mCepRecoEventAdresses->mChargeSum = &chSum;
            mCepRecoEventAdresses->mPID = &mPid;
            
            mCepRecoEventAdresses->eventWeight = &totalWeight;
            for(int i=0; i<CepUtil::nCepChannels; ++i){
                mCepRecoEventAdresses->eventWeightPid[i] = &totalWeight;
                for(int j=0; j<CepUtil::nSystChecks; ++j){
                    mCepRecoEventAdresses->eventWeightPidSystCheck[i][j] = &totalWeight;
                }
            }
            
            for(int i=0; i<CepUtil::nCepChannels; ++i){
                mCepRecoEventAdresses->mMass[i] = &mMass;
                mCepRecoEventAdresses->mRapidity[i] = &mPairRapidity;
            }
            mCepRecoEventAdresses->mDeltaPhi = &mDeltaPhiDegrees;
            mCepRecoEventAdresses->mMandelstamTSum = &mMandelstamTSum;
            
//             for(int i=0; i<CepUtil::kNumCuts; ++i){
//                 mCepRecoEventAdresses->analysisCut[i] = &(mCepRecoEvent->analysisCut[i]);
//             }
//             
//             for(int i=0; i<CepUtil::nCepChannels; ++i){
//                 mCepRecoEventAdresses->mPidArray[i] = &(mCepRecoEvent->mPidArray[i]);
//             }
//             
//             for(int i=0; i<CepUtil::nCepChannels; ++i){
//                 for(int j=0; j<CepUtil::nCoordinates; ++j){
//                     mCepRecoEventAdresses->mTotalMomentum[i][j] = &(mCepRecoEvent->mTotalMomentum[i][j]);
//                 }
//             }
            
            for(int i=0; i<CepUtil::nCepChannels; ++i){
                if( runMode == 0 ){
                    mCepRecoEventAdresses->mNSigmaTotalMomentum[i] = i<=CepUtil::PPBAR ? &nSigmaMissingPt : &nSigmaMissingPt_other;
                } else{
                    mCepRecoEventAdresses->mNSigmaTotalMomentum[i] = i==CepUtil::FOURPI ? &nSigmaMissingPt : &nSigmaMissingPt_other;
                }
            }
            //END  - - - - - - - - - - - - - - - - setting up adresses - - - - - - - - - - - - - - 
            
            mCepPhys = new CepPhysicsContainer( "Physics", fHistFile, &mCepRecoEventAdresses );
            createHistograms();
            
            std::cout << "Now running " << mCepUtil->mParticleName[particle] << ", file " << (file+1) << "/" << fileVec[particle].size() << ":  " << nameStr << std::endl;
            
            while( cepTreeReader.Next() ){
                if( *pid == particle ){
                    ++totalNEvents;
                    
                    TLorentzVector proton4Vec[CepUtil::nSides];
                    TLorentzVector centralParticle4Vec[CepUtil::nSigns];
                    TLorentzVector centralParticle4Vec_S0m[CepUtil::nSigns];
                    TLorentzVector centralParticle4Vec_D0m[CepUtil::nSigns];
                    TLorentzVector centralParticle4Prong4Vec[CepUtil::nSigns][2];
                    
                    proton4Vec[CepUtil::C] = TLorentzVector( *pxProton2, *pyProton2, *pzProton2, sqrt( pow(*pxProton2,2)+pow(*pyProton2,2)+pow(*pzProton2,2) + pow(mCepUtil->mMass[CepUtil::PROTON],2) ) );
                    proton4Vec[CepUtil::A] = TLorentzVector( *pxProton1, *pyProton1, *pzProton1, sqrt( pow(*pxProton1,2)+pow(*pyProton1,2)+pow(*pzProton1,2) + pow(mCepUtil->mMass[CepUtil::PROTON],2) ) );

                    if( runMode == 0 ){
                        centralParticle4Vec[CepUtil::PLUS] = TLorentzVector( **pxPlus, **pyPlus, **pzPlus, sqrt( pow(**pxPlus,2)+pow(**pyPlus,2)+pow(**pzPlus,2) + pow(mCepUtil->mMass[static_cast<CepUtil::PARTICLE_NAME>(mParticlesId)],2) ) );
                        centralParticle4Vec[CepUtil::MINUS] = TLorentzVector( **pxMinus, **pyMinus, **pzMinus, sqrt( pow(**pxMinus,2)+pow(**pyMinus,2)+pow(**pzMinus,2) + pow(mCepUtil->mMass[static_cast<CepUtil::PARTICLE_NAME>(mParticlesId)],2) ) );
                        trackPt->Fill( centralParticle4Vec[CepUtil::PLUS].Pt() );
                        trackPt->Fill( centralParticle4Vec[CepUtil::MINUS].Pt() );
                        
                        centralParticle4Vec_S0m[CepUtil::PLUS] = centralParticle4Vec[CepUtil::PLUS];
                        centralParticle4Vec_S0m[CepUtil::MINUS] = centralParticle4Vec[CepUtil::MINUS];
                        centralParticle4Vec_D0m[CepUtil::PLUS] = centralParticle4Vec[CepUtil::PLUS];
                        centralParticle4Vec_D0m[CepUtil::MINUS] = centralParticle4Vec[CepUtil::MINUS];
                        
                        transformCentralTrksToWave( centralParticle4Vec_S0m[CepUtil::PLUS], centralParticle4Vec_S0m[CepUtil::MINUS], proton4Vec[CepUtil::A], proton4Vec[CepUtil::C], angularDist_S0m );
                        transformCentralTrksToWave( centralParticle4Vec_D0m[CepUtil::PLUS], centralParticle4Vec_D0m[CepUtil::MINUS], proton4Vec[CepUtil::A], proton4Vec[CepUtil::C], angularDist_D0m );
                        
                    } else if( runMode == 1 ){
                        centralParticle4Prong4Vec[CepUtil::PLUS][0] = TLorentzVector( **pxPlus1, **pyPlus1, **pzPlus1, sqrt( pow(**pxPlus1,2)+pow(**pyPlus1,2)+pow(**pzPlus1,2) + pow(mCepUtil->mMass[static_cast<CepUtil::PARTICLE_NAME>(mParticlesId)],2) ) );
                        centralParticle4Prong4Vec[CepUtil::MINUS][0] = TLorentzVector( **pxMinus1, **pyMinus1, **pzMinus1, sqrt( pow(**pxMinus1,2)+pow(**pyMinus1,2)+pow(**pzMinus1,2) + pow(mCepUtil->mMass[static_cast<CepUtil::PARTICLE_NAME>(mParticlesId)],2) ) );
                        centralParticle4Prong4Vec[CepUtil::PLUS][1] = TLorentzVector( **pxPlus2, **pyPlus2, **pzPlus2, sqrt( pow(**pxPlus2,2)+pow(**pyPlus2,2)+pow(**pzPlus2,2) + pow(mCepUtil->mMass[static_cast<CepUtil::PARTICLE_NAME>(mParticlesId)],2) ) );
                        centralParticle4Prong4Vec[CepUtil::MINUS][1] = TLorentzVector( **pxMinus2, **pyMinus2, **pzMinus2, sqrt( pow(**pxMinus2,2)+pow(**pyMinus2,2)+pow(**pzMinus2,2) + pow(mCepUtil->mMass[static_cast<CepUtil::PARTICLE_NAME>(mParticlesId)],2) ) );
                        trackPt->Fill( centralParticle4Prong4Vec[CepUtil::PLUS][0].Pt() );
                        trackPt->Fill( centralParticle4Prong4Vec[CepUtil::PLUS][1].Pt() );
                        trackPt->Fill( centralParticle4Prong4Vec[CepUtil::MINUS][0].Pt() );
                        trackPt->Fill( centralParticle4Prong4Vec[CepUtil::MINUS][1].Pt() );
                    }

                    //--------- --------- --------- --------- --------- --------- --------- --------- ---------

                    TLorentzVector total4Momentum = runMode==0 ? (centralParticle4Vec[0] + centralParticle4Vec[1]) : (centralParticle4Prong4Vec[0][0] + centralParticle4Prong4Vec[1][0] + centralParticle4Prong4Vec[0][1] + centralParticle4Prong4Vec[1][1]);
                    mMass = total4Momentum.M();
//                     const int mMassRange = mCepUtil->massRange( mMass );
//                     const int mMassRange4Pi = mMass<1.5 ? CepUtil::MASS_1 : (mMass<2.5 ? CepUtil::MASS_2 : CepUtil::MASS_3);
                    mPairRapidity = total4Momentum.Rapidity();
                    const double mdPt = (proton4Vec[CepUtil::A]-proton4Vec[CepUtil::C]).Pt();
                    
                    TVector2 v1(proton4Vec[0].Px(), proton4Vec[0].Py());
                    TVector2 v2(proton4Vec[1].Px(), proton4Vec[1].Py());
                    mDeltaPhiDegrees = acos(v1.Unit()*v2.Unit()) * 180. / CepUtil::PI;
                    const int mDeltaPhiRange = mDeltaPhiDegrees<90 ? CepUtil::DELTAPHI_1 : CepUtil::DELTAPHI_2;//mCepUtil->deltaPhiRange( mDeltaPhiDegrees );
                    const int mNarrowerDeltaPhiRange = (mDeltaPhiDegrees<45. ? CepUtil::DELTAPHI_1 : (mDeltaPhiDegrees>135. ? CepUtil::DELTAPHI_2 : CepUtil::nDeltaPhiRanges));
                    double dPt = (proton4Vec[CepUtil::A].Vect()-proton4Vec[CepUtil::C].Vect()).Pt();
                    
                    double t[CepUtil::nSides];
                    for(int side=0; side<CepUtil::nSides; ++side)
                      t[side] = -(proton4Vec[side] - TLorentzVector(0,0, (side==0 ? (-1) : 1)*initialMomentum, sqrt(pow(initialMomentum,2) + pow(mCepUtil->mMass[CepUtil::PROTON],2)) )).Mag2();
                    mMandelstamTSum = fabs(t[CepUtil::C] + t[CepUtil::A]);
                    
                    
                    int mDptDeltaPhiRange = CepUtil::nDptDeltaPhiRanges;
                    if( mDeltaPhiRange==CepUtil::DELTAPHI_1)
                      mDptDeltaPhiRange = (mdPt<0.12) ? CepUtil::SMALLDPT_DELTAPHI_1 : CepUtil::LARGEDPT_DELTAPHI_1;
                    else
                      mDptDeltaPhiRange = (mdPt<0.56) ? CepUtil::SMALLDPT_DELTAPHI_2 : CepUtil::LARGEDPT_DELTAPHI_2;
                    
                    const int mMandelstamTSumRange = (mMandelstamTSum < 0.16) ? CepUtil::MANDELSTAMTSUM_1 : CepUtil::MANDELSTAMTSUM_2;
                    const int mPairRapidityRange = (fabs(mPairRapidity) < 0.2 ) ? CepUtil::PAIRRAPIDITY_1 : CepUtil::PAIRRAPIDITY_2;
                    
                    const double fabsEta1 = fabs( centralParticle4Vec[CepUtil::PLUS].Eta() );
                    const double fabsEta2 = fabs( centralParticle4Vec[CepUtil::MINUS].Eta() );
                    const int mCentralTracksEtaRange = (fabsEta1<1.0 && fabsEta2<1.0) ? CepUtil::MID_MID : ( ((fabsEta1<1.0 && fabsEta2>1.0) || (fabsEta1>1.0 && fabsEta2<1.0)) ? CepUtil::MID_FOR : CepUtil::FOR_FOR );
                    //--------- --------- --------- --------- --------- --------- --------- --------- ---------
                    
                    
                    
                    bool centralTracksPassCuts = false;
                    bool centralTracksPassCuts_S0m = false;
                    bool centralTracksPassCuts_D0m = false;
                    
                    if( runMode == 0 ){
                        centralTracksPassCuts = centralParticle4Vec[0].Pt() > minPt[mParticlesId] && centralParticle4Vec[1].Pt() > minPt[mParticlesId] &&
                                                (centralParticle4Vec[0].Pt() > 0.2 || centralParticle4Vec[1].Pt() > 0.2) &&
                                                fabs(centralParticle4Vec[0].Eta()) < maxEta && fabs(centralParticle4Vec[1].Eta()) < maxEta;
                        centralTracksPassCuts_S0m = centralParticle4Vec_S0m[0].Pt() > minPt[mParticlesId] && centralParticle4Vec_S0m[1].Pt() > minPt[mParticlesId] &&
                                                    (centralParticle4Vec_S0m[0].Pt() > 0.2 || centralParticle4Vec_S0m[1].Pt() > 0.2) &&
                                                    fabs(centralParticle4Vec_S0m[0].Eta()) < maxEta && fabs(centralParticle4Vec_S0m[1].Eta()) < maxEta;
                        centralTracksPassCuts_D0m = centralParticle4Vec_D0m[0].Pt() > minPt[mParticlesId] && centralParticle4Vec_D0m[1].Pt() > minPt[mParticlesId] &&
                                                    (centralParticle4Vec_D0m[0].Pt() > 0.2 || centralParticle4Vec_D0m[1].Pt() > 0.2) &&
                                                    fabs(centralParticle4Vec_D0m[0].Eta()) < maxEta && fabs(centralParticle4Vec_D0m[1].Eta()) < maxEta;
                    } else{
                        centralTracksPassCuts = centralParticle4Prong4Vec[0][0].Pt() > minPt[mParticlesId] && centralParticle4Prong4Vec[1][0].Pt() > minPt[mParticlesId] &&
                                                centralParticle4Prong4Vec[0][1].Pt() > minPt[mParticlesId] && centralParticle4Prong4Vec[1][1].Pt() > minPt[mParticlesId] &&
                                                (centralParticle4Prong4Vec[0][0].Pt() > 0.2 || centralParticle4Prong4Vec[1][0].Pt() > 0.2 ||
                                                 centralParticle4Prong4Vec[0][1].Pt() > 0.2 || centralParticle4Prong4Vec[1][1].Pt() > 0.2) &&
                                                fabs(centralParticle4Prong4Vec[0][0].Eta()) < maxEta && fabs(centralParticle4Prong4Vec[1][0].Eta()) < maxEta &&
                                                fabs(centralParticle4Prong4Vec[0][1].Eta()) < maxEta && fabs(centralParticle4Prong4Vec[1][1].Eta()) < maxEta;
                    }

                    const bool forwardProtonsPassCuts = fabs(proton4Vec[0].Py()) > 0.17 && fabs(proton4Vec[0].Py()) < 0.5 &&
                                                        fabs(proton4Vec[1].Py()) > 0.17 && fabs(proton4Vec[1].Py()) < 0.5;
                    
                                                        
                    if( centralTracksPassCuts && fabs(mPairRapidity) < 2.0 ){
                        int localDeltaPhiBin = -1;
                        if(mDeltaPhiDegrees<=45.)
                            localDeltaPhiBin = 0; else
                        if(mDeltaPhiDegrees<=90.)
                            localDeltaPhiBin = 1; else
                        if(mDeltaPhiDegrees<=135.)
                            localDeltaPhiBin = 2; else
                            localDeltaPhiBin = 3;
                        
                        mAcceptanceForDeltaPhiCuts_Vs_t1t2[localDeltaPhiBin]->Fill( forwardProtonsPassCuts, t[CepUtil::C], t[CepUtil::A] );
                        mAcceptanceForDeltaPhiCuts_Vs_t1t2_TOTAL[localDeltaPhiBin]->Fill( t[CepUtil::C], t[CepUtil::A] );
                        if( forwardProtonsPassCuts )
                            mAcceptanceForDeltaPhiCuts_Vs_t1t2_PASSED[localDeltaPhiBin]->Fill( t[CepUtil::C], t[CepUtil::A] );
                    }
                    
                    
                    
                    //extra angular studies
                    if( runMode == 0 ){
                        if( forwardProtonsPassCuts ){
                            
                            
//                             // S0m wave
//                             if(centralTracksPassCuts_S0m){
//                                 bool extraPtCutPassed = true;
//                                 if( min(centralParticle4Vec_S0m[0].Pt(), centralParticle4Vec_S0m[1].Pt()) > oneTrackMaxPt[mParticlesId] )
//                                     extraPtCutPassed = false;
//                             
//                                 if( extraPtCutPassed ){
//                                     CosTheta_Phi mAngles[CepUtil::nReferenceFrames];
//                                     for(unsigned int rf=0; rf<CepUtil::nReferenceFrames; ++rf)
//                                         mAngles[rf] = transformCentralTrks( centralParticle4Vec_S0m[CepUtil::PLUS], centralParticle4Vec_S0m[CepUtil::MINUS], proton4Vec[CepUtil::A], proton4Vec[CepUtil::C], rf );
//                                     
//                                     const double massDensityGenEx = invMassFromGenEx_S0m[mDeltaPhiRange]->GetBinContent( invMassFromGenEx_S0m[mDeltaPhiRange]->FindBin(mMass) );
//                                     const double massDensityData = invMassFromData[mDeltaPhiRange]->GetBinContent( invMassFromData[mDeltaPhiRange]->FindBin(mMass) );
//                                     
//                                     if( massDensityData > 0 && massDensityGenEx > 0 ){
//                                         
//                                         const double weight_S0m = ( massDensityData / massDensityGenEx );
//                                     
//                                         oo2PiInvMass_S0m[mDeltaPhiRange]->hQ_Weighted->Fill( mMass, weightPid*specialWeightFactor * weight_S0m );
//                                         
//                                         for(unsigned int rf=0; rf<CepUtil::nReferenceFrames; ++rf){
//                                             oo2PiCosTheta_MassBins_S0m[rf][mMassRange]->hQ_Weighted->Fill( mAngles[rf].mCosTheta, weightPid*specialWeightFactor * weight_S0m );
//                                             oo2PiPhi_MassBins_S0m[rf][mMassRange]->hQ_Weighted->Fill( mAngles[rf].mPhiDegrees, weightPid*specialWeightFactor * weight_S0m );
//                                         }
//                                     
//                                     }
//                                 }
//                             }
//                             
//                             
//                             // D0m wave
//                             if(centralTracksPassCuts_D0m){
//                                 bool extraPtCutPassed = true;
//                                 if( min(centralParticle4Vec_D0m[0].Pt(), centralParticle4Vec_D0m[1].Pt()) > oneTrackMaxPt[mParticlesId] )
//                                     extraPtCutPassed = false;
//                             
//                                 if( extraPtCutPassed ){
//                                     CosTheta_Phi mAngles[CepUtil::nReferenceFrames];
//                                     for(unsigned int rf=0; rf<CepUtil::nReferenceFrames; ++rf)
//                                         mAngles[rf] = transformCentralTrks( centralParticle4Vec_D0m[CepUtil::PLUS], centralParticle4Vec_D0m[CepUtil::MINUS], proton4Vec[CepUtil::A], proton4Vec[CepUtil::C], rf );
//                                     
//                                     const double massDensityGenEx = invMassFromGenEx_D0m[mDeltaPhiRange]->GetBinContent( invMassFromGenEx_D0m[mDeltaPhiRange]->FindBin(mMass) );
//                                     const double massDensityData = invMassFromData[mDeltaPhiRange]->GetBinContent( invMassFromData[mDeltaPhiRange]->FindBin(mMass) );
//                                     
//                                     if( massDensityData > 0 && massDensityGenEx > 0 ){
//                                         
//                                         const double weight_D0m = ( massDensityData / massDensityGenEx );
//                                     
//                                         oo2PiInvMass_D0m[mDeltaPhiRange]->hQ_Weighted->Fill( mMass, weightPid*specialWeightFactor * weight_D0m );
//                                         
//                                         for(unsigned int rf=0; rf<CepUtil::nReferenceFrames; ++rf){
//                                             oo2PiCosTheta_MassBins_D0m[rf][mMassRange]->hQ_Weighted->Fill( mAngles[rf].mCosTheta, weightPid*specialWeightFactor * weight_D0m );
//                                             oo2PiPhi_MassBins_D0m[rf][mMassRange]->hQ_Weighted->Fill( mAngles[rf].mPhiDegrees, weightPid*specialWeightFactor * weight_D0m );
//                                         }
//                                     
//                                     }
//                                 }
//                             }
                            
                            
                        }
                    }
                                                        
                                                        
                    if( centralTracksPassCuts && forwardProtonsPassCuts ){
                        
                        if( runMode == 0 ){
                            bool extraPtCutPassed = true;
                            if( min(centralParticle4Vec[0].Pt(), centralParticle4Vec[1].Pt()) > oneTrackMaxPt[mParticlesId] )
                                extraPtCutPassed = false;
                        
                        
                            if( extraPtCutPassed ){
                                
                                CosTheta_Phi mAngles[CepUtil::nReferenceFrames];
                                for(unsigned int rf=0; rf<CepUtil::nReferenceFrames; ++rf)
                                    mAngles[rf] = transformCentralTrks( centralParticle4Vec[CepUtil::PLUS], centralParticle4Vec[CepUtil::MINUS], proton4Vec[CepUtil::A], proton4Vec[CepUtil::C], rf );
                                
                                //BEGIN -------------------------------------------- filling histograms --------------------------------------------
//                                 oo2PiInvMass->hQ_Weighted->Fill( mMass, weightPid*specialWeightFactor );
//                                 oo2PiRapidity->hQ_Weighted->Fill( mPairRapidity, weightPid*specialWeightFactor );
//                                 oo2PiDeltaPhi->hQ_Weighted->Fill( mDeltaPhiDegrees, weightPid*specialWeightFactor );
//                                 oo2PiMandelstamTSum->hQ_Weighted->Fill( mMandelstamTSum, weightPid*specialWeightFactor );
//                                 
//                                 oo2PiInvMass_DeltaPhiBins[mDeltaPhiRange]->hQ_Weighted->Fill( mMass, weightPid*specialWeightFactor );
//                                 
//                                 if(mDeltaPhiRange == CepUtil::DELTAPHI_1)
//                                     oo2PiInvMass_DPtBins_DeltaPhiLessThan90Deg[dPt<0.2 ? 0 : 1]->hQ_Weighted->Fill( mMass, weightPid*specialWeightFactor );
//                                 
//                                 oo2PiInvMass_CentralTracksEtaBins[mCentralTracksEtaRange]->hQ_Weighted->Fill( mMass, weightPid*specialWeightFactor );
//                                 
//                                 oo2PiRapidity_MassBins[mMassRange]->hQ_Weighted->Fill( mPairRapidity, weightPid*specialWeightFactor );
//                                 oo2PiDeltaPhi_MassBins[mMassRange]->hQ_Weighted->Fill( mDeltaPhiDegrees, weightPid*specialWeightFactor );
//                                 oo2PiMandelstamTSum_MassBins[mMassRange]->hQ_Weighted->Fill( mMandelstamTSum, weightPid*specialWeightFactor );
//                                 
//                                 for(unsigned int rf=0; rf<CepUtil::nReferenceFrames; ++rf){
//                                     oo2PiCosTheta[rf]->hQ_Weighted->Fill( mAngles[rf].mCosTheta, weightPid*specialWeightFactor );
//                                     oo2PiCosTheta_MassBins[rf][mMassRange]->hQ_Weighted->Fill( mAngles[rf].mCosTheta, weightPid*specialWeightFactor );
//                                     
//                                     oo2PiPhi[rf]->hQ_Weighted->Fill( mAngles[rf].mPhiDegrees, weightPid*specialWeightFactor );
//                                     oo2PiPhi_MassBins[rf][mMassRange]->hQ_Weighted->Fill( mAngles[rf].mPhiDegrees, weightPid*specialWeightFactor );
//                                 }
                                
                                for(auto x: mCepPhys->mCepObsObj){
                                    x->FillQ();
                                }
                                
                                
                                //END -------------------------------------------- -------------------------------------------- ---------------------------
                            }
                        } else{
                                //BEGIN -------------------------------------------- filling histograms --------------------------------------------
                            
//                                 TLorentzVector neutralState1[2], neutralState2[2];
//                             
//                                 neutralState1[0] = centralParticle4Prong4Vec[CepUtil::PLUS][0] + centralParticle4Prong4Vec[CepUtil::MINUS][0];
//                                 neutralState2[0] = centralParticle4Prong4Vec[CepUtil::PLUS][1] + centralParticle4Prong4Vec[CepUtil::MINUS][1];
//                                 neutralState1[1] = centralParticle4Prong4Vec[CepUtil::PLUS][0] + centralParticle4Prong4Vec[CepUtil::MINUS][1];
//                                 neutralState2[1] = centralParticle4Prong4Vec[CepUtil::PLUS][1] + centralParticle4Prong4Vec[CepUtil::MINUS][0];
//                             
//                                 double invMassA[2], invMassB[2];
//                                 for(int i=0; i<2; ++i){
//                                     invMassA[i] = neutralState1[i].M();
//                                     invMassB[i] = neutralState2[i].M();
//                                     double lowerMass = invMassA[i] < invMassB[i] ? invMassA[i] : invMassB[i];
//                                     double higherMass = invMassA[i] < invMassB[i] ? invMassB[i] : invMassA[i];
//                                     
//                                     oo4PiInvMass_MinMassOf2Pi->hQ_Weighted->Fill( lowerMass, 0.5*weightPid*specialWeightFactor );
//                                     oo4PiInvMass_MaxMassOf2Pi->hQ_Weighted->Fill( higherMass, 0.5*weightPid*specialWeightFactor );
// 
//                                     oo4PiInvMass_MinMassOf2Pi_DeltaPhiBins[mDeltaPhiRange]->hQ_Weighted->Fill( lowerMass, 0.5*weightPid*specialWeightFactor );
//                                     oo4PiInvMass_MaxMassOf2Pi_DeltaPhiBins[mDeltaPhiRange]->hQ_Weighted->Fill( higherMass, 0.5*weightPid*specialWeightFactor );
//                                 }
                            
                                TLorentzVector neutralState1, neutralState2;
                                TLorentzVector neutralStateTMP1[2], neutralStateTMP2[2];
                                
                                neutralStateTMP1[0] = centralParticle4Prong4Vec[CepUtil::PLUS][0] + centralParticle4Prong4Vec[CepUtil::MINUS][0];
                                neutralStateTMP2[0] = centralParticle4Prong4Vec[CepUtil::PLUS][1] + centralParticle4Prong4Vec[CepUtil::MINUS][1];
                                neutralStateTMP1[1] = centralParticle4Prong4Vec[CepUtil::PLUS][0] + centralParticle4Prong4Vec[CepUtil::MINUS][1];
                                neutralStateTMP2[1] = centralParticle4Prong4Vec[CepUtil::PLUS][1] + centralParticle4Prong4Vec[CepUtil::MINUS][0];
                                double deltaPt_combination1 = (neutralStateTMP1[0] - neutralStateTMP2[0]).Pt();
                                double deltaPt_combination2 = (neutralStateTMP1[1] - neutralStateTMP2[1]).Pt();
                                neutralState1 = deltaPt_combination1 > deltaPt_combination2 ? neutralStateTMP1[0] : neutralStateTMP1[1];
                                neutralState2 = deltaPt_combination1 > deltaPt_combination2 ? neutralStateTMP2[0] : neutralStateTMP2[1];
                            
                                double invMassA, invMassB;
                                invMassA = neutralState1.M();
                                invMassB = neutralState2.M();
                                double lowerMass = invMassA < invMassB ? invMassA : invMassB;
                                double higherMass = invMassA < invMassB ? invMassB : invMassA;
                                
                                for(auto x: mCepPhys->mCepObsObj){
                                    x->FillQ();
                                }
                                
//                                 oo4PiInvMass_MinMassOf2Pi->hQ_Weighted->Fill( lowerMass, weightPid*specialWeightFactor );
//                                 oo4PiInvMass_MaxMassOf2Pi->hQ_Weighted->Fill( higherMass, weightPid*specialWeightFactor );
//                                 
//                                 oo4PiInvMass_MinMassOf2Pi_DeltaPhiBins[mDeltaPhiRange]->hQ_Weighted->Fill( lowerMass, weightPid*specialWeightFactor );
//                                 oo4PiInvMass_MaxMassOf2Pi_DeltaPhiBins[mDeltaPhiRange]->hQ_Weighted->Fill( higherMass, weightPid*specialWeightFactor );
//                                 
//                                 
//                                 
//                                 oo4PiInvMass->hQ_Weighted->Fill( mMass, weightPid*specialWeightFactor );
//                                 oo4PiRapidity->hQ_Weighted->Fill( mPairRapidity, weightPid*specialWeightFactor );
//                                 oo4PiDeltaPhi->hQ_Weighted->Fill( mDeltaPhiDegrees, weightPid*specialWeightFactor );
//                                 oo4PiMandelstamTSum->hQ_Weighted->Fill( mMandelstamTSum, weightPid*specialWeightFactor );
//                                 
//                                 oo4PiInvMass_DeltaPhiBins[mDeltaPhiRange]->hQ_Weighted->Fill( mMass, weightPid*specialWeightFactor );
//                                 
//                                 oo4PiRapidity_MassBins[mMassRange4Pi]->hQ_Weighted->Fill( mPairRapidity, weightPid*specialWeightFactor );
//                                 oo4PiDeltaPhi_MassBins[mMassRange4Pi]->hQ_Weighted->Fill( mDeltaPhiDegrees, weightPid*specialWeightFactor );
//                                 oo4PiMandelstamTSum_MassBins[mMassRange4Pi]->hQ_Weighted->Fill( mMandelstamTSum, weightPid*specialWeightFactor );
                                //END -------------------------------------------- -------------------------------------------- ---------------------------
                        }
                    }
                    
                    
                    
                    // resolutions
                    if( centralTracksPassCuts ){
                        
                        double pxReco[CepUtil::nSides];
                        double pyReco[CepUtil::nSides];
                        TLorentzVector proton4Vec_reco[CepUtil::nSides];
                        
                        for(int i=0; i<CepUtil::nSides; ++i){
                            pxReco[i] = proton4Vec[i].Px() + mGen->Gaus(0, pxResolution);
                            pyReco[i] = proton4Vec[i].Py() + mGen->Gaus(0, pyResolution);
                            
                            proton4Vec_reco[i] = TLorentzVector( pxReco[i], pyReco[i], (i==CepUtil::C ? (-1) : 1) * sqrt(initialMomentum*initialMomentum - pxReco[i]*pxReco[i] + pyReco[i]*pyReco[i]), sqrt( initialMomentum*initialMomentum + pow(mCepUtil->mMass[CepUtil::PROTON],2) ) );
                        }
                        
                        TVector2 v1_reco(proton4Vec_reco[0].Px(), proton4Vec_reco[0].Py());
                        TVector2 v2_reco(proton4Vec_reco[1].Px(), proton4Vec_reco[1].Py());
                        double mDeltaPhiDegrees_reco = acos(v1_reco.Unit()*v2_reco.Unit()) * 180. / CepUtil::PI;
                        
                        double t_reco[CepUtil::nSides];
                        for(int side=0; side<CepUtil::nSides; ++side)
                            t_reco[side] = fabs( -(proton4Vec_reco[side] - TLorentzVector(0,0, (side==0 ? (-1) : 1)*initialMomentum, sqrt(pow(initialMomentum,2) + pow(mCepUtil->mMass[CepUtil::PROTON],2)) )).Mag2() );
                        const double mMandelstamTSum_reco = fabs(t_reco[CepUtil::C] + t_reco[CepUtil::A]);
                        

                        for(int i=0; i<CepUtil::nSides; ++i){
                            
                            bool trueInsideFiducial = fabs(proton4Vec[i].Py()) > 0.17 && fabs(proton4Vec[i].Py()) < 0.5;
                            bool recoInsideFiducial = fabs(proton4Vec_reco[i].Py()) > 0.17 && fabs(proton4Vec_reco[i].Py()) < 0.5;
                            
                            if( recoInsideFiducial )
                                mhPy_RecoInsideFiducial->Fill( proton4Vec_reco[i].Py() );
                            if( trueInsideFiducial && !recoInsideFiducial)
                                mhPy_TrueInsideFiducial_RecoOutsideFiducial->Fill( proton4Vec[i].Py() );
                            if( !trueInsideFiducial && recoInsideFiducial)
                                mhPy_TrueOutsideFiducial_RecoInsideFiducial->Fill( proton4Vec_reco[i].Py() );
                            
                            bool tWithinFiducial_reco = t_reco[i] > 0.05 && t_reco[i] < 0.25 && recoInsideFiducial;
                            bool tWithinFiducial_true = t[i] > 0.05 && t[i] < 0.25 && trueInsideFiducial;

                            if( tWithinFiducial_reco )
                                mhMandelstamT_RecoInsideFiducial_LimitedMandelstamT->Fill( t_reco[i] );
                            if( tWithinFiducial_true && !tWithinFiducial_reco )
                                mhMandelstamT_TrueInsideFiducial_RecoOutsideFiducial_LimitedMandelstamT->Fill( t[i] );
                            if( !tWithinFiducial_true && tWithinFiducial_reco )
                                mhMandelstamT_TrueOutsideFiducial_RecoInsideFiducial_LimitedMandelstamT->Fill( t_reco[i] );
                            
                            
                            
                            mhResponseMatrix_MandelstamTSum->Fill( t_reco[0]+t_reco[1], mMandelstamTSum );
                            mhResponseMatrix_DeltaPhi->Fill( mDeltaPhiDegrees_reco, mDeltaPhiDegrees );
                        }
                    }
                    
                    
                    if( forwardProtonsPassCuts ){
                        
                        double ptTrue[CepUtil::nSigns];
                        double etaTrue[CepUtil::nSigns];
                        
                        double ptReco[CepUtil::nSigns];
                        double etaReco[CepUtil::nSigns];
                        
                        for(int i=0; i<CepUtil::nSigns; ++i){
                            ptTrue[i] = centralParticle4Vec[i].Pt();
                            etaTrue[i] = centralParticle4Vec[i].Eta();
                            
                            
                            double ptForWidth = ptTrue[i]<0.1 ? 0.101 : ptTrue[i];
                            double etaForWidth = fabs(etaTrue[i])>2.5 ? 2.499 : fabs(etaTrue[i]);
                            
                            ptReco[i] = ptTrue[i] + mGen->Gaus(0, mhPtRecoMinusTrueWidthVsPtVsEta->GetBinContent( mhPtRecoMinusTrueWidthVsPtVsEta->GetXaxis()->FindBin(etaForWidth), 
                                                                                                                  mhPtRecoMinusTrueWidthVsPtVsEta->GetYaxis()->FindBin(ptForWidth) ) );
                            etaReco[i] = etaTrue[i] + mGen->Gaus(0, mhEtaRecoMinusTrueWidthVsPtVsEta->GetBinContent( mhEtaRecoMinusTrueWidthVsPtVsEta->GetXaxis()->FindBin(etaForWidth), 
                                                                                                                    mhEtaRecoMinusTrueWidthVsPtVsEta->GetYaxis()->FindBin(ptForWidth) ) );
                            
//                             cout << "pt = " << ptTrue[i] << " smear = " <<  mhPtRecoMinusTrueWidthVsPtVsEta->GetBinContent( mhPtRecoMinusTrueWidthVsPtVsEta->GetXaxis()->FindBin(etaForWidth), 
//                                                                                                                   mhPtRecoMinusTrueWidthVsPtVsEta->GetYaxis()->FindBin(ptForWidth) ) << endl;
//                             cout << "eta = " << etaTrue[i] << " smear = " <<  mhEtaRecoMinusTrueWidthVsPtVsEta->GetBinContent( mhEtaRecoMinusTrueWidthVsPtVsEta->GetXaxis()->FindBin(etaForWidth), 
//                                                                                                                   mhEtaRecoMinusTrueWidthVsPtVsEta->GetYaxis()->FindBin(ptForWidth) ) << endl;
                            
                            bool trueInsideFiducial = ptTrue[i] > 0.1 && fabs(etaTrue[i]) < 2.5;
                            bool recoInsideFiducial = ptReco[i] > 0.1 && fabs(etaReco[i]) < 2.5;
                            
                            if( recoInsideFiducial )
                                mhPtVsEta_0p1_RecoInsideFiducial->Fill( etaReco[i], ptReco[i] );
                            if( trueInsideFiducial && !recoInsideFiducial)
                                mhPtVsEta_0p1_TrueInsideFiducial_RecoOutsideFiducial->Fill( etaTrue[i], ptTrue[i] );
                            if( !trueInsideFiducial && recoInsideFiducial)
                                mhPtVsEta_0p1_TrueOutsideFiducial_RecoInsideFiducial->Fill( etaReco[i], ptReco[i] );
                            
                            trueInsideFiducial = ptTrue[i] > 0.2 && fabs(etaTrue[i]) < 2.5;
                            recoInsideFiducial = ptReco[i] > 0.2 && fabs(etaReco[i]) < 2.5;
                            
                            if( recoInsideFiducial )
                                mhPtVsEta_0p2_RecoInsideFiducial->Fill( etaReco[i], ptReco[i] );
                            if( trueInsideFiducial && !recoInsideFiducial)
                                mhPtVsEta_0p2_TrueInsideFiducial_RecoOutsideFiducial->Fill( etaTrue[i], ptTrue[i] );
                            if( !trueInsideFiducial && recoInsideFiducial)
                                mhPtVsEta_0p2_TrueOutsideFiducial_RecoInsideFiducial->Fill( etaReco[i], ptReco[i] );
                        }
                        
                        
                        
                        
                        
                    }
                    
                    
                } else if( *pid == CepUtil::nDefinedParticles && *luminosity > 0 ){
                    totalLumi += *luminosity * 1.e3; // nb^-1 -> mub^-1
                }
            }
            
            fInput->Close();
            
            convertToCrossSection();
            
            // determine migrations corrections
            migrations( TString(mCepUtil->mParticleName[particle]+"_"+nameStr) );
            
            fHistFile->Write();
            fHistFile->Close();
            
            delete mCepRecoEventAdresses;
        }
    }

}


void produceMcPredictions_ROOT::createHistograms(){
    
    trackPt = new TH1F("trackPt", "trackPt", 200, 0, 5);
    
    //   special binning
    double lowMassThreshold;
    double massBinWidth;
    int index = 0;
    double massBinBoundary = 0;
    
    // pipi
    lowMassThreshold = 2*mCepUtil->mMass[CepUtil::PION];
    std::vector<double> pionMassBinsVec;
    pionMassBinsVec.push_back( lowMassThreshold );
    massBinBoundary = 0;
    index = 0;
    massBinWidth = 0.02;
    while( massBinBoundary < 0.7 ){
        ++index;
        massBinBoundary = 0.3 + massBinWidth*index;
        pionMassBinsVec.push_back( massBinBoundary );
    }
    index = 0;
    massBinWidth = 0.03;
    while( massBinBoundary < 1.2999 ){
        ++index;
        massBinBoundary = 0.7 + massBinWidth*index;
        pionMassBinsVec.push_back( massBinBoundary );
    }
    index = 0;
    massBinWidth = 0.05;
    while( massBinBoundary < 2.9999 ){
        ++index;
        massBinBoundary = 1.3 + massBinWidth*index;
        pionMassBinsVec.push_back( massBinBoundary );
    }
    index = 0;
    massBinWidth = 0.10;
    while( massBinBoundary < 5.0 ){
        ++index;
        massBinBoundary = 3.0 + massBinWidth*index;
        pionMassBinsVec.push_back( massBinBoundary );
    }
    index = 0;
    massBinWidth = 0.25;
    while( massBinBoundary < 10.0 ){
        ++index;
        massBinBoundary = 5.0 + massBinWidth*index;
        pionMassBinsVec.push_back( massBinBoundary );
    }
    
    //----------------------------------------------------------
    
   std::vector<double> mandelstamTSumBinVec;
    mandelstamTSumBinVec.push_back( 0.0 );
    mandelstamTSumBinVec.push_back( 0.05 );
    mandelstamTSumBinVec.push_back( 0.20 );
    mandelstamTSumBinVec.push_back( 0.40 );
    mandelstamTSumBinVec.push_back( 0.70 );
    mandelstamTSumBinVec.push_back( 1.20 );
    mandelstamTSumBinVec.push_back( 2.00 );
    
    std::vector<double> rapidityBinVec = createBinning(30, -3, 3);
    std::vector<double> deltaPhiBinVec = createBinning(4, 0, 180);
    std::vector<double> cosThetaBinVec = createBinning(20, -1, 1);
    std::vector<double> phiBinVec = createBinning(18, -180, 180);
    
    // 2Pi
  /*  
    oo2PiInvMass = new ObservableObjectMC(this, "2PiInvMass", pionMassBinsVec);
    oo2PiRapidity = new ObservableObjectMC(this, "2PiRapidity", rapidityBinVec);
    oo2PiDeltaPhi = new ObservableObjectMC(this, "2PiDeltaPhi", deltaPhiBinVec);
    oo2PiMandelstamTSum = new ObservableObjectMC(this, "2PiMandelstamTSum", mandelstamTSumBinVec);
    
    for(int rf=0; rf<CepUtil::nReferenceFrames; ++rf){
        oo2PiCosTheta[rf] = new ObservableObjectMC(this, Form("2PiCosTheta_RF%d", rf), cosThetaBinVec);
        oo2PiPhi[rf] = new ObservableObjectMC(this, Form("2PiPhi_RF%d", rf), phiBinVec);
    }
    
    for(int i=0; i<CepUtil::nDeltaPhiRanges; ++i){
        oo2PiInvMass_DeltaPhiBins[i] = new ObservableObjectMC(this, Form("2PiInvMass_DeltaPhiBins%d", i), pionMassBinsVec);
        oo2PiInvMass_S0m[i] = new ObservableObjectMC(this, Form("2PiInvMass_S0m_DeltaPhiBins%d", i), pionMassBinsVec);
        oo2PiInvMass_D0m[i] = new ObservableObjectMC(this, Form("2PiInvMass_D0m_DeltaPhiBins%d", i), pionMassBinsVec);
    }
    
    for(int i=0; i<2; ++i)
        oo2PiInvMass_DPtBins_DeltaPhiLessThan90Deg[i] = new ObservableObjectMC(this, Form("2PiInvMass_DPtBins%d_DeltaPhiLessThan90Deg", i), pionMassBinsVec);
  
    for(int i=0; i<CepUtil::nCentralTracksEtaConf; ++i)
        oo2PiInvMass_CentralTracksEtaBins[i] = new ObservableObjectMC(this, Form("2PiInvMass_CentralTracksEtaBins%d", i), pionMassBinsVec);
    
    for(int i=0; i<CepUtil::nMassRanges; ++i){
        oo2PiRapidity_MassBins[i] = new ObservableObjectMC(this, Form("2PiRapidity_MassBins%d", i), rapidityBinVec);
        oo2PiDeltaPhi_MassBins[i] = new ObservableObjectMC(this, Form("2PiDeltaPhi_MassBins%d", i), deltaPhiBinVec);
        oo2PiMandelstamTSum_MassBins[i] = new ObservableObjectMC(this, Form("2PiMandelstamTSum_MassBins%d", i), mandelstamTSumBinVec);
        for(int rf=0; rf<CepUtil::nReferenceFrames; ++rf){
            oo2PiCosTheta_MassBins[rf][i] = new ObservableObjectMC(this, Form("2PiCosTheta_RF%d_MassBins%d", rf, i), cosThetaBinVec);
            oo2PiPhi_MassBins[rf][i] = new ObservableObjectMC(this, Form("2PiPhi_RF%d_MassBins%d", rf, i), phiBinVec);
            
            oo2PiCosTheta_MassBins_S0m[rf][i] = new ObservableObjectMC(this, Form("2PiCosTheta_RF%d_MassBins%d_S0m", rf, i), cosThetaBinVec);
            oo2PiPhi_MassBins_S0m[rf][i] = new ObservableObjectMC(this, Form("2PiPhi_RF%d_MassBins%d_S0m", rf, i), phiBinVec);
            
            oo2PiCosTheta_MassBins_D0m[rf][i] = new ObservableObjectMC(this, Form("2PiCosTheta_RF%d_MassBins%d_D0m", rf, i), cosThetaBinVec);
            oo2PiPhi_MassBins_D0m[rf][i] = new ObservableObjectMC(this, Form("2PiPhi_RF%d_MassBins%d_D0m", rf, i), phiBinVec);
        }
    }
    
    
    // 4Pi
    
    index = -1;
    lowMassThreshold = 4*mCepUtil->mMass[CepUtil::PION];
    std::vector<double> fourPionMassBinsVec;
    fourPionMassBinsVec.push_back( lowMassThreshold );
    massBinBoundary = 0;
    index = 0;
    massBinWidth = 0.05;
    while( massBinBoundary < 1.1 ){
        ++index;
        massBinBoundary = 0.6 + massBinWidth*index;
        fourPionMassBinsVec.push_back( massBinBoundary );
    }
    index = 0;
    massBinWidth = 0.025;
    while( massBinBoundary < 1.2999 ){
        ++index;
        massBinBoundary = 1.1 + massBinWidth*index;
        fourPionMassBinsVec.push_back( massBinBoundary );
    }
    index = 0;
    massBinWidth = 0.05;
    while( massBinBoundary < 2.9999 ){
        ++index;
        massBinBoundary = 1.3 + massBinWidth*index;
        fourPionMassBinsVec.push_back( massBinBoundary );
    }
    index = 0;
    massBinWidth = 0.10;
    while( massBinBoundary < 5.0 ){
        ++index;
        massBinBoundary = 3.0 + massBinWidth*index;
        fourPionMassBinsVec.push_back( massBinBoundary );
    }
    index = 0;
    massBinWidth = 1.0;
    while( massBinBoundary < 20.0 ){
        ++index;
        massBinBoundary = 5.0 + massBinWidth*index;
        fourPionMassBinsVec.push_back( massBinBoundary );
    }
    
    
    
    std::vector<double> fourPionMassBinsVec_2;
    fourPionMassBinsVec_2.push_back( lowMassThreshold );
    massBinBoundary = 0;
    index = 0;
    massBinWidth = 0.05;
    while( massBinBoundary < 1.1 ){
        ++index;
        massBinBoundary = 0.6 + massBinWidth*index;
        fourPionMassBinsVec_2.push_back( massBinBoundary );
    }
    index = 0;
    massBinWidth = 0.025;
    while( massBinBoundary < 1.2999 ){
        ++index;
        massBinBoundary = 1.1 + massBinWidth*index;
        fourPionMassBinsVec_2.push_back( massBinBoundary );
    }
    index = 0;
    massBinWidth = 0.05;
    while( massBinBoundary < 1.4999 ){
        ++index;
        massBinBoundary = 1.3 + massBinWidth*index;
        fourPionMassBinsVec_2.push_back( massBinBoundary );
    }
    fourPionMassBinsVec_2.push_back( 1.6 );
    fourPionMassBinsVec_2.push_back( 1.7 );
    fourPionMassBinsVec_2.push_back( 2.0 );
    fourPionMassBinsVec_2.push_back( 2.5 );
    fourPionMassBinsVec_2.push_back( 3.0 );
    fourPionMassBinsVec_2.push_back( 5.0 );
    fourPionMassBinsVec_2.push_back( 7.0 );
    fourPionMassBinsVec_2.push_back( 10.0 );
    
    
    oo4PiInvMass = new ObservableObjectMC(this, "4PiInvMass", fourPionMassBinsVec);
    oo4PiInvMass_MinMassOf2Pi = new ObservableObjectMC(this, "4PiInvMass_MinMassOf2Pi", fourPionMassBinsVec_2);
    oo4PiInvMass_MaxMassOf2Pi = new ObservableObjectMC(this, "4PiInvMass_MaxMassOf2Pi", fourPionMassBinsVec_2);
    oo4PiRapidity = new ObservableObjectMC(this, "4PiRapidity", rapidityBinVec);
    oo4PiDeltaPhi = new ObservableObjectMC(this, "4PiDeltaPhi", deltaPhiBinVec);
    oo4PiMandelstamTSum = new ObservableObjectMC(this, "4PiMandelstamTSum", mandelstamTSumBinVec);
    for(int i=0; i<CepUtil::nDeltaPhiRanges; ++i){
        oo4PiInvMass_DeltaPhiBins[i] = new ObservableObjectMC(this, Form("4PiInvMass_DeltaPhiBins%d", i), fourPionMassBinsVec);
        oo4PiInvMass_MinMassOf2Pi_DeltaPhiBins[i] = new ObservableObjectMC(this, Form("4PiInvMass_MinMassOf2Pi_DeltaPhiBins%d", i), fourPionMassBinsVec_2);
        oo4PiInvMass_MaxMassOf2Pi_DeltaPhiBins[i] = new ObservableObjectMC(this, Form("4PiInvMass_MaxMassOf2Pi_DeltaPhiBins%d", i), fourPionMassBinsVec_2);
    }
    for(int i=0; i<CepUtil::nMassRanges; ++i){
        oo4PiRapidity_MassBins[i] = new ObservableObjectMC(this, Form("4PiRapidity_MassBins%d", i), rapidityBinVec);
        oo4PiDeltaPhi_MassBins[i] = new ObservableObjectMC(this, Form("4PiDeltaPhi_MassBins%d", i), deltaPhiBinVec);
        oo4PiMandelstamTSum_MassBins[i] = new ObservableObjectMC(this, Form("4PiMandelstamTSum_MassBins%d", i), mandelstamTSumBinVec);
    }*/
    
    
    mhPy_RecoInsideFiducial = new TH1F("mhPy_RecoInsideFiducial", "mhPy_RecoInsideFiducial", 55, 0.17, 0.5);
    mhPy_TrueInsideFiducial_RecoOutsideFiducial = new TH1F("mhPy_TrueInsideFiducial_RecoOutsideFiducial", "mhPy_TrueInsideFiducial_RecoOutsideFiducial", 55, 0.17, 0.5);
    mhPy_TrueOutsideFiducial_RecoInsideFiducial = new TH1F("mhPy_TrueOutsideFiducial_RecoInsideFiducial", "mhPy_TrueOutsideFiducial_RecoInsideFiducial", 55, 0.17, 0.5);
    
    std::vector<double> etaBinVec;
    etaBinVec.push_back(-3.0);
    etaBinVec.push_back(-2.7);
    etaBinVec.push_back(-2.6);
    etaBinVec.push_back(-2.5);
    etaBinVec.push_back(-2.45);
    etaBinVec.push_back(-2.4);
    etaBinVec.push_back(-2.3);
    etaBinVec.push_back(-2.0);
    etaBinVec.push_back(-1.0);
    etaBinVec.push_back( 0.0);
    etaBinVec.push_back(1.0);
    etaBinVec.push_back(2.0);
    etaBinVec.push_back(2.3);
    etaBinVec.push_back(2.4);
    etaBinVec.push_back(2.45);
    etaBinVec.push_back(2.5);
    etaBinVec.push_back(2.6);
    etaBinVec.push_back(2.7);
    etaBinVec.push_back(3.0);
    
    std::vector<double> ptBinVec;
    ptBinVec.push_back(0.0);
    ptBinVec.push_back(0.1);
    ptBinVec.push_back(0.12);
    ptBinVec.push_back(0.15);
    ptBinVec.push_back(0.2);
    ptBinVec.push_back(0.3);
    ptBinVec.push_back(0.5);
    ptBinVec.push_back(0.8);
    ptBinVec.push_back(1.15);
    ptBinVec.push_back(1.5);
    ptBinVec.push_back(2.0);
    
    std::vector<double> ptBinVec2;
    ptBinVec2.push_back(0.0);
    ptBinVec2.push_back(0.2);
    ptBinVec2.push_back(0.22);
    ptBinVec2.push_back(0.25);
    ptBinVec2.push_back(0.3);
    ptBinVec2.push_back(0.5);
    ptBinVec2.push_back(0.8);
    ptBinVec2.push_back(1.15);
    ptBinVec2.push_back(1.5);
    ptBinVec2.push_back(2.0);
        
    mhPtVsEta_0p1_RecoInsideFiducial = new TH2F("mhPtVsEta_0p1_RecoInsideFiducial", "mhPtVsEta_0p1_RecoInsideFiducial", etaBinVec.size()-1, &etaBinVec[0], ptBinVec.size()-1, &ptBinVec[0]);
    mhPtVsEta_0p1_TrueInsideFiducial_RecoOutsideFiducial = new TH2F("mhPtVsEta_0p1_TrueInsideFiducial_RecoOutsideFiducial", "mhPtVsEta_0p1_TrueInsideFiducial_RecoOutsideFiducial", etaBinVec.size()-1, &etaBinVec[0], ptBinVec.size()-1, &ptBinVec[0]);
    mhPtVsEta_0p1_TrueOutsideFiducial_RecoInsideFiducial = new TH2F("mhPtVsEta_0p1_TrueOutsideFiducial_RecoInsideFiducial", "mhPtVsEta_0p1_TrueOutsideFiducial_RecoInsideFiducial", etaBinVec.size()-1, &etaBinVec[0], ptBinVec.size()-1, &ptBinVec[0]);
    
    mhPtVsEta_0p2_RecoInsideFiducial = new TH2F("mhPtVsEta_0p2_RecoInsideFiducial", "mhPtVsEta_0p2_RecoInsideFiducial", etaBinVec.size()-1, &etaBinVec[0], ptBinVec2.size()-1, &ptBinVec2[0]);
    mhPtVsEta_0p2_TrueInsideFiducial_RecoOutsideFiducial = new TH2F("mhPtVsEta_0p2_TrueInsideFiducial_RecoOutsideFiducial", "mhPtVsEta_0p2_TrueInsideFiducial_RecoOutsideFiducial", etaBinVec.size()-1, &etaBinVec[0], ptBinVec2.size()-1, &ptBinVec2[0]);
    mhPtVsEta_0p2_TrueOutsideFiducial_RecoInsideFiducial = new TH2F("mhPtVsEta_0p2_TrueOutsideFiducial_RecoInsideFiducial", "mhPtVsEta_0p2_TrueOutsideFiducial_RecoInsideFiducial", etaBinVec.size()-1, &etaBinVec[0], ptBinVec2.size()-1, &ptBinVec2[0]);

    mhMandelstamT_RecoInsideFiducial_LimitedMandelstamT = new TH1F("mhMandelstamT_RecoInsideFiducial_LimitedMandelstamT", "mhMandelstamT_RecoInsideFiducial_LimitedMandelstamT", 50, 0.05, 0.25);
    mhMandelstamT_TrueInsideFiducial_RecoOutsideFiducial_LimitedMandelstamT = new TH1F("mhMandelstamT_TrueInsideFiducial_RecoOutsideFiducial_LimitedMandelstamT", "mhMandelstamT_TrueInsideFiducial_RecoOutsideFiducial_LimitedMandelstamT", 50, 0.05, 0.25);
    mhMandelstamT_TrueOutsideFiducial_RecoInsideFiducial_LimitedMandelstamT = new TH1F("mhMandelstamT_TrueOutsideFiducial_RecoInsideFiducial_LimitedMandelstamT", "mhMandelstamT_TrueOutsideFiducial_RecoInsideFiducial_LimitedMandelstamT", 50, 0.05, 0.25);
    
    mhResponseMatrix_MandelstamTSum = new TH2F("mhResponseMatrix_MandelstamTSum", "mhResponseMatrix_MandelstamTSum", mandelstamTSumBinVec.size()-1, &mandelstamTSumBinVec[0], mandelstamTSumBinVec.size()-1, &mandelstamTSumBinVec[0]);
    mhResponseMatrix_DeltaPhi = new TH2F("mhResponseMatrix_DeltaPhi", "mhResponseMatrix_DeltaPhi", deltaPhiBinVec.size()-1, &deltaPhiBinVec[0], deltaPhiBinVec.size()-1, &deltaPhiBinVec[0]);
    
    
    for(int i=0; i<4; ++i){
        mAcceptanceForDeltaPhiCuts_Vs_t1t2[i] = new TEfficiency("mAcceptanceForDeltaPhiCuts_Vs_t1t2"+TString(Form("_deltaPhiBin_%d", i+1)), "mAcceptanceForDeltaPhiCuts_Vs_t1t2"+TString(Form("_deltaPhiBin_%d", i+1)), 50, 0, 0.5, 50, 0, 0.5);
        mAcceptanceForDeltaPhiCuts_Vs_t1t2_PASSED[i] = new TH2F("mAcceptanceForDeltaPhiCuts_Vs_t1t2_PASSED"+TString(Form("_deltaPhiBin_%d", i+1)), "mAcceptanceForDeltaPhiCuts_Vs_t1t2_PASSED"+TString(Form("_deltaPhiBin_%d", i+1)), 50, 0, 0.5, 50, 0, 0.5);
        mAcceptanceForDeltaPhiCuts_Vs_t1t2_TOTAL[i] = new TH2F("mAcceptanceForDeltaPhiCuts_Vs_t1t2_TOTAL"+TString(Form("_deltaPhiBin_%d", i+1)), "mAcceptanceForDeltaPhiCuts_Vs_t1t2_TOTAL"+TString(Form("_deltaPhiBin_%d", i+1)), 50, 0, 0.5, 50, 0, 0.5);
    }
}

void produceMcPredictions_ROOT::convertToCrossSection(){
    
    for(auto x: mCepPhys->mCepObsObj){
        for(auto y: x->getObjectsToWrite()){
            dynamic_cast<TH1*>(y)->Scale(1./(totalLumi*specialWeightFactor), "width");
        }
    }
    
//     // 2Pi
//     oo2PiInvMass->hQ_Weighted->;
//     oo2PiRapidity->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//     oo2PiDeltaPhi->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//     oo2PiMandelstamTSum->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//     for(int rf=0; rf<CepUtil::nReferenceFrames; ++rf){
//         oo2PiCosTheta[rf]->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//         oo2PiPhi[rf]->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//     }
//     for(int i=0; i<CepUtil::nDeltaPhiRanges; ++i){
//         oo2PiInvMass_DeltaPhiBins[i]->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//         oo2PiInvMass_S0m[i]->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//         oo2PiInvMass_D0m[i]->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//     }
//     for(int i=0; i<CepUtil::nDeltaPhiRanges; ++i)
//         oo2PiInvMass_DPtBins_DeltaPhiLessThan90Deg[i]->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//     for(int i=0; i<CepUtil::nCentralTracksEtaConf; ++i)
//         oo2PiInvMass_CentralTracksEtaBins[i]->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//   
//     for(int i=0; i<CepUtil::nMassRanges; ++i){
//         oo2PiRapidity_MassBins[i]->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//         oo2PiDeltaPhi_MassBins[i]->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//         oo2PiMandelstamTSum_MassBins[i]->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//         for(int rf=0; rf<CepUtil::nReferenceFrames; ++rf){
//             oo2PiCosTheta_MassBins[rf][i]->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//             oo2PiCosTheta_MassBins_S0m[rf][i]->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//             oo2PiCosTheta_MassBins_D0m[rf][i]->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//             oo2PiPhi_MassBins[rf][i]->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//             oo2PiPhi_MassBins_S0m[rf][i]->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//             oo2PiPhi_MassBins_D0m[rf][i]->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//         }
//     }
//     
//     // 4Pi
//     oo4PiInvMass->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//     oo4PiInvMass_MinMassOf2Pi->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//     oo4PiInvMass_MaxMassOf2Pi->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//     oo4PiRapidity->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//     oo4PiDeltaPhi->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//     oo4PiMandelstamTSum->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//     for(int i=0; i<CepUtil::nDeltaPhiRanges; ++i){
//         oo4PiInvMass_DeltaPhiBins[i]->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//         oo4PiInvMass_MinMassOf2Pi_DeltaPhiBins[i]->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//         oo4PiInvMass_MaxMassOf2Pi_DeltaPhiBins[i]->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//     }
//   
//     for(int i=0; i<CepUtil::nMassRanges; ++i){
//         oo4PiRapidity_MassBins[i]->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//         oo4PiDeltaPhi_MassBins[i]->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//         oo4PiMandelstamTSum_MassBins[i]->hQ_Weighted->Scale(1./(totalLumi*specialWeightFactor), "width");
//     }
    
}


void produceMcPredictions_ROOT::migrations(TString filename) const{
    
    TFile *fOut = new TFile("MigrationsCorr_"+filename, "RECREATE");
    
    TH1F *mhPy_MigIn = new TH1F( *mhPy_TrueOutsideFiducial_RecoInsideFiducial );
    mhPy_MigIn->SetName("mhPy_MigIn");
    mhPy_MigIn->Divide( mhPy_RecoInsideFiducial );
    TH1F *mhPy_MigOut = new TH1F( *mhPy_TrueInsideFiducial_RecoOutsideFiducial );
    mhPy_MigOut->SetName("mhPy_MigOut");
    mhPy_MigOut->Divide( mhPy_RecoInsideFiducial );
    TH1F *mhPy_MigCorr = new TH1F( *mhPy_RecoInsideFiducial );
    mhPy_MigCorr->SetName("mhPy_MigCorr");
    mhPy_MigCorr->Add( mhPy_TrueOutsideFiducial_RecoInsideFiducial, -1 );
    mhPy_MigCorr->Add( mhPy_TrueInsideFiducial_RecoOutsideFiducial,  1 );
    mhPy_MigCorr->Divide( mhPy_RecoInsideFiducial );
    

    TH1F *mhMandelstamT_MigIn = new TH1F( *mhMandelstamT_TrueOutsideFiducial_RecoInsideFiducial_LimitedMandelstamT );
    mhMandelstamT_MigIn->SetName("mhMandelstamT_MigIn");
    mhMandelstamT_MigIn->Divide( mhMandelstamT_RecoInsideFiducial_LimitedMandelstamT );
    TH1F *mhMandelstamT_MigOut = new TH1F( *mhMandelstamT_TrueInsideFiducial_RecoOutsideFiducial_LimitedMandelstamT );
    mhMandelstamT_MigOut->SetName("mhMandelstamT_MigOut");
    mhMandelstamT_MigOut->Divide( mhMandelstamT_RecoInsideFiducial_LimitedMandelstamT );
    TH1F *mhMandelstamT_MigCorr = new TH1F( *mhMandelstamT_RecoInsideFiducial_LimitedMandelstamT );
    mhMandelstamT_MigCorr->SetName("mhMandelstamT_MigCorr");
    mhMandelstamT_MigCorr->Add( mhMandelstamT_TrueOutsideFiducial_RecoInsideFiducial_LimitedMandelstamT, -1 );
    mhMandelstamT_MigCorr->Add( mhMandelstamT_TrueInsideFiducial_RecoOutsideFiducial_LimitedMandelstamT,  1 );
    mhMandelstamT_MigCorr->Divide( mhMandelstamT_RecoInsideFiducial_LimitedMandelstamT );
    
    
    TH2F *mhPtVsEta_0p1_MigIn = new TH2F( *mhPtVsEta_0p1_TrueOutsideFiducial_RecoInsideFiducial );
    mhPtVsEta_0p1_MigIn->SetName("mhPtVsEta_0p1_MigIn");
    mhPtVsEta_0p1_MigIn->Divide( mhPtVsEta_0p1_RecoInsideFiducial );
    TH2F *mhPtVsEta_0p1_MigOut = new TH2F( *mhPtVsEta_0p1_TrueInsideFiducial_RecoOutsideFiducial );
    mhPtVsEta_0p1_MigOut->SetName("mhPtVsEta_0p1_MigOut");
    mhPtVsEta_0p1_MigOut->Divide( mhPtVsEta_0p1_RecoInsideFiducial );
    TH2F *mhPtVsEta_0p1_MigCorr = new TH2F( *mhPtVsEta_0p1_RecoInsideFiducial );
    mhPtVsEta_0p1_MigCorr->SetName("mhPtVsEta_0p1_MigCorr");
    mhPtVsEta_0p1_MigCorr->Add( mhPtVsEta_0p1_TrueOutsideFiducial_RecoInsideFiducial, -1 );
    mhPtVsEta_0p1_MigCorr->Add( mhPtVsEta_0p1_TrueInsideFiducial_RecoOutsideFiducial,  1 );
    mhPtVsEta_0p1_MigCorr->Divide( mhPtVsEta_0p1_RecoInsideFiducial );
    
    TH2F *mhPtVsEta_0p2_MigIn = new TH2F( *mhPtVsEta_0p2_TrueOutsideFiducial_RecoInsideFiducial );
    mhPtVsEta_0p2_MigIn->SetName("mhPtVsEta_0p2_MigIn");
    mhPtVsEta_0p2_MigIn->Divide( mhPtVsEta_0p2_RecoInsideFiducial );
    TH2F *mhPtVsEta_0p2_MigOut = new TH2F( *mhPtVsEta_0p2_TrueInsideFiducial_RecoOutsideFiducial );
    mhPtVsEta_0p2_MigOut->SetName("mhPtVsEta_0p2_MigOut");
    mhPtVsEta_0p2_MigOut->Divide( mhPtVsEta_0p2_RecoInsideFiducial );
    TH2F *mhPtVsEta_0p2_MigCorr = new TH2F( *mhPtVsEta_0p2_RecoInsideFiducial );
    mhPtVsEta_0p2_MigCorr->SetName("mhPtVsEta_0p2_MigCorr");
    mhPtVsEta_0p2_MigCorr->Add( mhPtVsEta_0p2_TrueOutsideFiducial_RecoInsideFiducial, -1 );
    mhPtVsEta_0p2_MigCorr->Add( mhPtVsEta_0p2_TrueInsideFiducial_RecoOutsideFiducial,  1 );
    mhPtVsEta_0p2_MigCorr->Divide( mhPtVsEta_0p2_RecoInsideFiducial );
    
    fOut->Write();
    fOut->Close();
}


produceMcPredictions_ROOT::CosTheta_Phi produceMcPredictions_ROOT::transformCentralTrks(TLorentzVector trackPlus, TLorentzVector trackMinus, TLorentzVector protonA, TLorentzVector protonB, UInt_t referenceFrame){
    CosTheta_Phi result;
    if( !(referenceFrame<CepUtil::nReferenceFrames) ){
        std::cerr << "ERROR in CosTheta_Phi transformCentralTrks(...) - wrong reference frame ID provided." << std::endl;
        return result;
    }
    
    const double cmsEnergySq = (trackPlus+trackMinus+protonA+protonB).Mag2();
    const double p0 = sqrt( cmsEnergySq/4. - mCepUtil->mMass[CepUtil::PROTON]*mCepUtil->mMass[CepUtil::PROTON] );
    TLorentzVector beamProton[CepUtil::nSides];
    for(int side=0; side<CepUtil::nSides; ++side)
        beamProton[side] = TLorentzVector(0, 0, (side==CepUtil::C?-1:1)*p0/*mCepUtil->p0()*/, sqrt(/*mCepUtil->p0()*mCepUtil->p0()*/p0*p0 + mCepUtil->mMass[CepUtil::PROTON]*mCepUtil->mMass[CepUtil::PROTON]));
    TLorentzVector trk[CepUtil::nSigns];
    trk[CepUtil::PLUS] = trackPlus;
    trk[CepUtil::MINUS] = trackMinus;
    TLorentzVector trkPair = trk[CepUtil::PLUS] + trk[CepUtil::MINUS];
    TVector3 trkPairBoost = trkPair.BoostVector();
    TLorentzRotation l2(trkPairBoost);
    TLorentzRotation l;
    TVector3 newX, newY, newZ;
    
    //--------------------------------------------------
    if( referenceFrame == CepUtil::GOTTFRIED_JACKSON || referenceFrame == CepUtil::HELICITY ){
        
        TLorentzVector pomeron[CepUtil::nSides];
        for(int side=0; side<CepUtil::nSides; ++side)
            pomeron[side] = beamProton[side] - (side==CepUtil::C ? protonB : protonA);
        
        newY = pomeron[CepUtil::A].Vect().Cross( /*pomeron[CepUtil::C].Vect()*/trkPairBoost ).Unit();
        pomeron[CepUtil::A].Transform(l2.Inverse());
        pomeron[CepUtil::C].Transform(l2.Inverse());
        /*
        if( referenceFrame == CepUtil::GOTTFRIED_JACKSON && totalNEvents<10 ){
            TLorentzVector total4Momentum = trkPair + protonA + protonB;
            cout << "------------" << endl;
            cout << "Total 4-momentum in the LAB:" << endl;
            total4Momentum.Print();
            cout << "Pomeron WEST in the CMS of pi+pi-:" << endl;
            pomeron[CepUtil::A].Print();
            cout << "Pomeron EAST in the CMS of pi+pi-:" << endl;
            pomeron[CepUtil::C].Print();
        }
        */
        switch(referenceFrame){
            case CepUtil::GOTTFRIED_JACKSON:{ newZ = pomeron[CepUtil::A].Vect().Unit(); break;}
            case CepUtil::HELICITY:{ newZ = trkPair.Vect().Unit(); break;}
            default:{ break;}
        }
        
        newX = newY.Cross( newZ ).Unit();
    }
    
    else
    
    if( referenceFrame == CepUtil::COLLINS_SOPER ){
        for(int side=0; side<CepUtil::nSides; ++side)
            beamProton[side].Transform(l2.Inverse());
        
        newY = beamProton[CepUtil::A].Vect().Unit().Cross( beamProton[CepUtil::C].Vect().Unit() );
        newZ = (beamProton[CepUtil::A].Vect().Unit() - beamProton[CepUtil::C].Vect().Unit()).Unit();
        newX = newY.Cross( newZ ).Unit();
        
        //     //cross-check
        //     TVector3 newX_2 = (beamProton[CepUtil::A].Vect().Unit() + beamProton[CepUtil::C].Vect().Unit()).Unit();
        //     cout << "-------" << endl;
        //     newX.Print();
        //     newX_2.Print();
    }
    //--------------------------------------------------
    
    TRotation r;
    r.SetZAxis(newZ, newX);
    TLorentzRotation l1(r);
    l=l2*l1;
    
    trk[CepUtil::PLUS].Transform(l.Inverse());
    trk[CepUtil::MINUS].Transform(l.Inverse());
    
    result.mCosTheta = cos( trk[CepUtil::PLUS].Theta() );
    result.mPhi = trk[CepUtil::PLUS].Phi();
    result.mPhiDegrees = result.mPhi*180./CepUtil::PI;
    return result;
}


std::vector<double> produceMcPredictions_ROOT::createBinning(int nBins, double xMin, double xMax) const{
    assert(nBins>0);
    assert(xMax>xMin);
    std::vector<double> binning;
    const double binWidth = (xMax - xMin)/nBins;
    for(int i=0; i<=nBins; ++i)
        binning.push_back(xMin + i*binWidth);
    return binning;
}


void produceMcPredictions_ROOT::readInDetResolution(){
    TH3F *mhPtRecoMinusTrueVsPtVsEta;
    TH3F *mhEtaRecoMinusTrueVsPtVsEta;
    
    TFile *fInput = new TFile("ROOT_files/inputTreesMC/hist-user.rsikora.mc15_13TeV.361276.Pythia8_A3_ALT_NNPDF23LO_CD_minbias.evgen.STDM6.e7046.v5_EXT0.v1_EXT0_EXT0.root", "READ");
    
    mhPtRecoMinusTrueVsPtVsEta = dynamic_cast<TH3F*>( fInput->Get("mhPtRecoMinusTrueVsPtVsEta") );
    mhPtRecoMinusTrueVsPtVsEta->SetDirectory(0);
    mhEtaRecoMinusTrueVsPtVsEta = dynamic_cast<TH3F*>( fInput->Get("mhEtaRecoMinusTrueVsPtVsEta") );
    mhEtaRecoMinusTrueVsPtVsEta->SetDirectory(0);
    
    fInput->Close();
    
//     TFile *fOut = new TFile("fOut.root", "RECREATE");
    
    mhPtRecoMinusTrueWidthVsPtVsEta = new TH2F("mhPtRecoMinusTrueWidthVsPtVsEta", "mhPtRecoMinusTrueWidthVsPtVsEta", 30, 0, 3.0, 50, 0.0, 5.0 );
    mhEtaRecoMinusTrueWidthVsPtVsEta = new TH2F("mhEtaRecoMinusTrueWidthVsPtVsEta", "mhEtaRecoMinusTrueWidthVsPtVsEta", 30, 0, 3.0, 50, 0.0, 5.0 );
    
    for(int etaBin=1; etaBin<=mhPtRecoMinusTrueVsPtVsEta->GetNbinsX(); ++etaBin){
        for(int ptBin=1; ptBin<=mhPtRecoMinusTrueVsPtVsEta->GetNbinsY(); ++ptBin){
            TH1D *hDeltaPt = mhPtRecoMinusTrueVsPtVsEta->ProjectionZ("tmp1", etaBin, etaBin, ptBin, ptBin);
            
            if(hDeltaPt->GetEntries()==0) continue;
            
            TF1 fGaus("fGaus", "gaus");
            hDeltaPt->Fit( &fGaus, "QN" );
            mhPtRecoMinusTrueWidthVsPtVsEta->SetBinContent(etaBin, ptBin, fGaus.GetParameter(2));
            mhPtRecoMinusTrueWidthVsPtVsEta->SetBinError(etaBin, ptBin, fGaus.GetParError(2));
            
//             cout << fGaus.GetParameter(2) << " +/- " << fGaus.GetParError(2) << endl;
            
            TH1D *hDeltaEta = mhEtaRecoMinusTrueVsPtVsEta->ProjectionZ("tmp2", etaBin, etaBin, ptBin, ptBin);
            
            hDeltaEta->Fit( &fGaus, "QN" );
            mhEtaRecoMinusTrueWidthVsPtVsEta->SetBinContent(etaBin, ptBin, fGaus.GetParameter(2));
            mhEtaRecoMinusTrueWidthVsPtVsEta->SetBinError(etaBin, ptBin, fGaus.GetParError(2));
            
            
        }
    }
    
//     fOut->Write();
//     fOut->Close();
 
 
}





Double_t produceMcPredictions_ROOT::intensityFromWaves(const Double_t *x,const  Double_t *par){
  const double theta = x[1];
  const double phi = x[0];
  complex<double> S0m, P0m, D0m, P1m, D1m, P1p, D1p;
  S0m = polar( par[0], 0.0 ) * ROOT::Math::sph_legendre(0,0, theta);
  P0m = polar( par[1], par[2] ) * ROOT::Math::sph_legendre(1,0, theta);
  D0m = polar( par[3], par[4] ) * ROOT::Math::sph_legendre(2,0, theta);
  P1m = polar( par[5], par[6] ) * ROOT::Math::sph_legendre(1,1, theta) * complex<double>(cos(phi), 0.0) * polar( 2.0/sqrt(2.0), 0.);
  D1m = polar( par[7], par[8] ) * ROOT::Math::sph_legendre(2,1, theta) * complex<double>(cos(phi), 0.0) * polar( 2.0/sqrt(2.0), 0.);
  P1p = polar( par[9], 0.0 ) * ROOT::Math::sph_legendre(1,1, theta) * complex<double>(0.0, sin(phi)) * polar( 2.0/sqrt(2.0), 0.);
  D1p = polar( par[10], par[11] ) * ROOT::Math::sph_legendre(2,1, theta) * complex<double>(0.0, sin(phi)) * polar( 2.0/sqrt(2.0), 0.);
  double intensity = norm(S0m + P0m + D0m + P1m + D1m) + norm(P1p + D1p);
  return intensity * sin(theta);
}



void produceMcPredictions_ROOT::transformCentralTrksToWave(TLorentzVector &trackPlus, TLorentzVector &trackMinus, TLorentzVector protonA, TLorentzVector protonB, TF2 * const intensityFunc){
  const double cmsEnergySq = (trackPlus+trackMinus+protonA+protonB).Mag2();
  const double p0 = sqrt( cmsEnergySq/4. - mCepUtil->mMass[CepUtil::PROTON]*mCepUtil->mMass[CepUtil::PROTON] );
  TLorentzVector beamProton[CepUtil::nSides];
  for(int side=0; side<CepUtil::nSides; ++side)
    beamProton[side] = TLorentzVector(0, 0, (side==CepUtil::C?-1:1)*p0/*mCepUtil->p0()*/, sqrt(/*mCepUtil->p0()*mCepUtil->p0()*/p0*p0 + mCepUtil->mMass[CepUtil::PROTON]*mCepUtil->mMass[CepUtil::PROTON]));
  TLorentzVector trk[CepUtil::nSigns];
  trk[CepUtil::PLUS] = trackPlus;
  trk[CepUtil::MINUS] = trackMinus;
  TLorentzVector trkPair = trk[CepUtil::PLUS] + trk[CepUtil::MINUS];
  TVector3 trkPairBoost = trkPair.BoostVector();
  TLorentzRotation l2(trkPairBoost);
  TLorentzRotation l;
  TVector3 newX, newY, newZ;
  
  TLorentzVector pomeron[CepUtil::nSides];
  for(int side=0; side<CepUtil::nSides; ++side)
    pomeron[side] = beamProton[side] - (side==CepUtil::C ? protonB : protonA);
  
  newY = pomeron[CepUtil::A].Vect().Cross( /*pomeron[CepUtil::C].Vect()*/trkPairBoost ).Unit();
  pomeron[CepUtil::A].Transform(l2.Inverse());
  pomeron[CepUtil::C].Transform(l2.Inverse());
  newZ = pomeron[CepUtil::A].Vect().Unit();
  newX = newY.Cross( newZ ).Unit();
  
  //----
  
  TRotation r;
  r.SetZAxis(newZ, newX);
  TLorentzRotation l1(r);
  l=l2*l1;
  
  trk[CepUtil::PLUS].Transform(l.Inverse());
  trk[CepUtil::MINUS].Transform(l.Inverse());
  
  //-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
  TRotation r_zeroOrientation;
  r_zeroOrientation.RotateZ( trk[CepUtil::PLUS].Phi() );
  TVector3 rotationAxis = TVector3(0,0,1).Cross( trk[CepUtil::PLUS].Vect() ).Unit();
  double theta = trk[CepUtil::PLUS].Theta();
  r_zeroOrientation.Rotate( theta, rotationAxis );
  trk[CepUtil::PLUS].Transform(r_zeroOrientation.Inverse());
  trk[CepUtil::MINUS].Transform(r_zeroOrientation.Inverse());
  //-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
  
  double randomPhi;
  double randomTheta;
  intensityFunc->GetRandom2(randomPhi, randomTheta);
  
  TRotation r_Wave;
  r_Wave.RotateZ( randomPhi );
  r_Wave.RotateX( randomTheta );
  
  trk[CepUtil::PLUS].Transform(r_Wave.Inverse());
  trk[CepUtil::MINUS].Transform(r_Wave.Inverse());
  
  // back to LAB frame
  trk[CepUtil::PLUS].Transform(l);
  trk[CepUtil::MINUS].Transform(l);
  
  trackPlus = trk[CepUtil::PLUS];
  trackMinus = trk[CepUtil::MINUS];
}


