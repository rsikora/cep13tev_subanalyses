#ifndef DataVsMC_hh
#define DataVsMC_hh

#include <TROOT.h>
#include <TFile.h>
#include <TString.h>
#include <TLatex.h>
#include <vector>
#include "CepUtil.h"
#include "DrawingOptions.hh"

class TH2;
class TH1;
class TH1F;
class TH2F;
class TAxis;
class CepParams;

class DataVsMC{
public:

  DataVsMC(CepUtil::CEP_CHANNELS);
  ~DataVsMC();

  void analyze();
 
  static void setBlackAndWhiteColorScale();
  static void setWhiteAndBlackColorScale();
  static void setBlueWhiteRedColorScale();

  
private:

  enum FILES {  DATA,
                MC_SIGNAL,
                MC_PYTHIA_CD_BKGD_NEUTRALS,
                MC_PYTHIA_CD_BKGD,
                nFiles
             };  
  static constexpr double kEps = 1e-6;

  void loadHistograms();
  void setupDrawingStyle() const;
  Double_t integratePtMiss(const TH1*, Double_t) const;
  void drawFinalResult(TH1F*, DrawingOptions &, TH1F* = nullptr);
  void readTH1F( TH1F **, TH1F *[], int, std::vector<double> = std::vector<double>() );
  void readTH2F( TH2F **, TH2F *[], int);
  void mergeTopologies(TH1 &, TH1 &, int, int) const;
  
  CepUtil * mUtil;
  CepParams * mParams;
  const CepUtil::CEP_CHANNELS mChannel;
  const TString mDirName;
  const TString mChannelStr;
  const TString mGeneralChannelStr;
  const TString mTotalChargeStr_OPPO;
  const TString mTotalChargeStr_SAME;
  
  TH1F * hNSigmaMissingPt[nFiles][CepUtil::nChargeSum];
  TH1F * hNSigmaMissingPt_weighted[nFiles][CepUtil::nChargeSum];
  TH1F * hMissingPx[nFiles][CepUtil::nChargeSum];
  TH1F * hMissingPy[nFiles][CepUtil::nChargeSum];
  
  TH2F * hHitMap[nFiles][CepUtil::nRomanPots];
  TH2F * hFiberMult[nFiles][CepUtil::nRomanPots];
  TH2F * hPxPy[nFiles][CepUtil::nSides];
  
  TH1F * hInvMass[nFiles][CepUtil::nChargeSum];
  TH1F * hInvMass_weighted[nFiles][CepUtil::nChargeSum];
  TH2F * hNSigmaPtMissVsInvMass[nFiles][CepUtil::nChargeSum];
  TH2F * hNSigmaPtMissVsInvMass_weighted[nFiles][CepUtil::nChargeSum];
  TH1F * hDelta_z0[nFiles];
  TH1F * hd0[nFiles];
  TH1F * hd0_WithOrWithoutVertex[2][nFiles];
  TH1F * hz0SinTheta[nFiles];
  TH1F * hNExtraTracks[nFiles];
  
  /*  
  TH1F *mh_YminusYExtr_BeforeMomentumBalanceCut[nFiles][CepUtil::nBranches]; //!
  TH1F *mh_YminusYExtr_BeforeMomentumBalanceCut_VsCentralStateTopology[nFiles][CepUtil::nBranches][CepUtil::nCentralStateTopologies]; //!
  TH1F *mh_YminusYExtr[nFiles][CepUtil::nBranches]; //!
  TH1F *mh_YminusYExtr_VsCentralStateTopology[nFiles][CepUtil::nBranches][CepUtil::nCentralStateTopologies]; //!
  */
  
  double mSpecialLegendMarginFactor;
  bool mInverseStackDrawing;
  bool mSpecialDataDrawing;
  bool mSpecialDataDrawing2;
  double mRatioFactor;
  double mRatioYmin;
  double mRatioYmax;
  double mRatioYTitleOffsetFactor;
  TString mRatioYTitle;
  
  bool mSubtractNeutrals;
  double mNeutralsFracToPreserve;
  bool mBkgdSubtractionDemonstration;
  TString mSpecialDataLegendDesctiption;
  bool mDrawSpecialRatio;
  bool mDrawSpecialRatio2;
  bool mDrawSpecialRatio3;
  TH1F *mAuxiliaryHist;
  
  ClassDef(DataVsMC,0);
};

#endif
