#ifndef CrossSectionAnalysis_hh
#define CrossSectionAnalysis_hh

#include <TROOT.h>
#include <TFile.h>
#include <TString.h>
#include "CepUtil.h"
#include "CepObservableObject.h"
#include "DrawingOptions.hh"
#include "SystematicsOutput.hh"

class TH2;
class TF2;
class TH1F;
class TH2F;
class CepPhysicsContainer;

TF2 *fExponent2D;
Double_t exponent1D(Double_t *x, Double_t *par);
double maxMandT;
    

class CrossSectionAnalysis{
public:
    
    CrossSectionAnalysis(TString opt="");
    ~CrossSectionAnalysis();
    
    void analyze();
    
private:
    
    CepUtil *mUtil;
    //   Parameters *mParams;
    //   Efficiencies *mEff;
    CepPhysicsContainer *mCepPhys;
    
    const double nSigmaMomentumBalanceCut = 3.0;
    static constexpr int nFilesWithAcceptanceCorr = 3;
    
    void getListOfAnalysedRuns(TFile *);
    void readLumiFiles();
    void getTotalIntegratedLuminosity();

    void transformToCrossSection(TH1*) const;
    SystematicsOutput calculateSystematicErrors( TH1F* const, TH1F** const, TH1F* const) const;
    void drawFinalResult(TH1F*, DrawingOptions &, SystematicsOutput &) const;
    void drawDataMcComparison2(TH1F*, TH1F*, DrawingOptions & opt);
    TH2F* getTH2F_from_TH1F_t1t2( const TH1F*, bool = true );
    
    Double_t mSpecialLegendMarginFactor;
    Bool_t mSpecialLabelRatio;
    Double_t mSpecialLabelOffsetFactor;
    Bool_t mEnableLegend;
    
    TString* mSystCheckName;
    TString* mSystCheckShortName;
    
    double totalIntegratedLuminosity; // mub^-1
    vector<int> runNumber;
    vector<int> colors;
    
    // all LBs are numbered starting from 1 and there are no gaps between LBs
    map<int, vector<double> > lumiPerLumiBlockVector_RunMap;
    map<int, vector<double> > lumiblockDurationVector_RunMap;
    map<int, vector<bool> > isPhysicsLumiBlockVector_RunMap;
    
    map<int, vector<int> > analysedLumiBlockVector_RunMap;
    
    TH2F *mhNEventsVsLumiBlockVsRunNumber;
    
    ClassDef(CrossSectionAnalysis,0);
};

#endif
