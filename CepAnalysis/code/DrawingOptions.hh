#ifndef DrawingOptions_hh
#define DrawingOptions_hh

#include <TROOT.h>
#include <TStyle.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TProfile.h>
#include <TFile.h>
#include <TString.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TLatex.h>
#include <cmath>
#include <TLegend.h>
#include <TPaveText.h>
#include <TEllipse.h>
#include <algorithm>
#include <vector>
#include <map>
#include <fstream>
#include <sstream>
#include <string>
#include <TGaxis.h>
#include <TObjArray.h>
#include <TObjString.h>
#include <TEfficiency.h>
#include <TMultiGraph.h>
#include <TGraphAsymmErrors.h>
#include <TFitResult.h>
#include <TArrow.h>


struct DrawingOptions{
      DrawingOptions(): mCanvasWidth(800.), mCanvasHeight(800.), mScale(kFALSE), mMaxDigits(3), mSetDivisionsX(kFALSE), mForceDivisionsX(kFALSE), mYlog(kFALSE), mSetDivisionsY(kFALSE), mForceDivisionsY(kFALSE), mInsert(kFALSE), mYMaxInstert(-9999), mYMinInstert(0.001), mYlogInsert(kFALSE), mDataLegendText_1(TString("")), mDataLegendText_2(TString("")), mNColumns(1), mSetLegendMargin(kFALSE), mLegendTitle(TString("")), mLegendTitle2(TString("")), mFillLegendWithColor(kFALSE), mSystErrorBoxWidthAdjustmentFactor(1.0) {};
      
      Double_t mCanvasWidth;
      Double_t mCanvasHeight;
      
      Bool_t mScale;
      Double_t mScalingFactor;
      
      Double_t mLeftMargin;
      Double_t mRightMargin;
      Double_t mTopMargin;
      Double_t mBottomMargin;
      
      Int_t mMaxDigits;
      
      //x axis
      Double_t mXmin;
      Double_t mXmax;
      TString mXaxisTitle;
      Double_t mXaxisTitleSize;
      Double_t mXaxisTitleOffset;
      Double_t mXaxisLabelSize;
      Double_t mXaxisLabelOffset;
      Double_t mXaxisTickLength;
      Bool_t mSetDivisionsX;
      Bool_t mForceDivisionsX;
      Int_t mXnDivisionA;
      Int_t mXnDivisionB;
      Int_t mXnDivisionC;
      
      //y axis
      Bool_t mYlog;
      Double_t mYmin;
      Double_t mYmax;
      TString mYaxisTitle;
      Double_t mYaxisTitleSize;
      Double_t mYaxisTitleOffset;
      Double_t mYaxisLabelSize;
      Double_t mYaxisLabelOffset;
      Double_t mYaxisTickLength;
      Bool_t mSetDivisionsY;
      Bool_t mForceDivisionsY;
      Int_t mYnDivisionA;
      Int_t mYnDivisionB;
      Int_t mYnDivisionC;
      
      
      Bool_t mInsert;
      Double_t mX1Insert;
      Double_t mX2Insert;
      Double_t mY1Insert;
      Double_t mY2Insert;
      Double_t mXMinInstert;
      Double_t mXMaxInstert;
      Double_t mYMaxInstert;
      Double_t mYMinInstert;
      Bool_t mYlogInsert;
      
      Int_t mMarkerStyle;
      Int_t mMarkerColor;
      Double_t mMarkerSize;
      Int_t mLineStyle;
      Int_t mLineColor;
      Double_t mLineWidth;
      
      Int_t mMarkerStyle2;
      Int_t mMarkerColor2;
      Double_t mMarkerSize2;
      Int_t mLineStyle2;
      Int_t mLineColor2;
      Double_t mLineWidth2;
      
      TString mPdfName;
      
      std::vector<TH1F*> mHistTH1F;
      std::vector<TString> mHistTH1F_DrawingOpt;
      
      std::vector<TH1F*> mHistTH1F2;
      std::vector<TString> mHistTH1F_DrawingOpt2;
      
      TString mDataLegendText_1;
      TString mDataLegendText_2;
      
      Int_t mNColumns;
      Double_t mLegendX;
      Double_t mLegendXwidth;
      Double_t mLegendY;
      Double_t mLegendYwidth;
      Double_t mLegendTextSize;
      Bool_t mSetLegendMargin;
      Double_t mLegendMargin;
      std::vector<TObject*> mObjForLegend;
      std::vector<TString> mDesriptionForLegend;
      std::vector<TString> mDrawTypeForLegend;
      std::vector<TObject*> mObjToDraw;
      std::vector<TString> mObjToDrawOption;
      TString mLegendTitle;
      
      Double_t mLegendX2;
      Double_t mLegendXwidth2;
      Double_t mLegendY2;
      Double_t mLegendYwidth2;
      Double_t mLegendTextSize2;
      std::vector<TObject*> mObjForLegend2;
      std::vector<TString> mDesriptionForLegend2;
      std::vector<TString> mDrawTypeForLegend2;
      TString mLegendTitle2;
      Bool_t mFillLegendWithColor;
      
      Double_t mSystErrorBoxWidthAdjustmentFactor;
      
      Double_t mXwidthSystErrorBox;
      Int_t mSystErrorBoxColor;
      Int_t mSystErrorTotalBoxColor;
      Double_t mSystErrorBoxOpacity;
      std::vector<Double_t> mXpositionSystErrorBox;
      
      Double_t mXwidthSystErrorBox2;
      Int_t mSystErrorBoxColor2;
      Double_t mSystErrorBoxOpacity2;
      std::vector<Double_t> mXpositionSystErrorBox2;
      
      std::vector<TLatex> mTextToDraw;
      std::vector<TObject*> mObjectToDraw;
};

#endif
