#ifndef produceMcPredictions_ROOT_hh
#define produceMcPredictions_ROOT_hh

#include <vector>
#include <TROOT.h>
#include <TFile.h>
#include <TString.h>
#include "CepUtil.h"
#include "CepPhysicsContainer.h"

class TH2;
class TH1F;
class TH2F;
class TGraphAsymmErrors;
class TLine;
class TVector3;
class TF2;
class TLorentzVector;
class TRandom3;
class TEfficiency;

class produceMcPredictions_ROOT{
public:
    
    produceMcPredictions_ROOT(TString opt="");
    ~produceMcPredictions_ROOT();
    
    void run();
    std::vector<double> createBinning(int, double, double) const;
    
    CepUtil *mCepUtil;
    TRandom3 *mGen;
    
    CepPhysicsContainer *mCepPhys;
    
    struct CosTheta_Phi{
        CosTheta_Phi(): mCosTheta(-1000), mPhi(-1000), mPhiDegrees(-1000){}
        Double_t mCosTheta;
        Double_t mPhi;
        Double_t mPhiDegrees;
    };
    
    enum WAVES_ENUM {   S0m_MAG, P0m_MAG, P0m_PH, P1m_MAG, P1m_PH, D0m_MAG, D0m_PH, D1m_MAG, D1m_PH,
                        P1p_MAG, D1p_MAG, D1p_PH,      nWaveParameters };
    
    double minPt[CepUtil::nDefinedParticles];
    double oneTrackMaxPt[CepUtil::nDefinedParticles];
    const double maxEta = 2.5;
    const double initialMomentum = 6500.0;
    
    const double pxResolution = 0.050; //GeV
    const double pyResolution = 0.018; //GeV
    
    
    int mParticlesId = CepUtil::nDefinedParticles;
    int totalNEvents = 0;
    double totalLumi = 0.0;
    double specialWeightFactor = 0.01;
    
    int runMode;
    

    void createHistograms();
    void convertToCrossSection();
    void migrations(TString) const;
    void readInDetResolution();
    CosTheta_Phi transformCentralTrks(TLorentzVector trackPlus, TLorentzVector trackMinus, TLorentzVector protonA, TLorentzVector protonB, UInt_t rf);
    Double_t intensityFromWaves(const Double_t *x,const  Double_t *par);
    void transformCentralTrksToWave(TLorentzVector &trackPlus, TLorentzVector &trackMinus, TLorentzVector protonA, TLorentzVector protonB, TF2 * const intensityFunc);
    
    TH1F *trackPt; //!
 
    TH1F *mhPy_RecoInsideFiducial;
    TH1F *mhPy_TrueInsideFiducial_RecoOutsideFiducial;
    TH1F *mhPy_TrueOutsideFiducial_RecoInsideFiducial;
    
    TH2F *mhPtVsEta_0p1_RecoInsideFiducial;
    TH2F *mhPtVsEta_0p1_TrueInsideFiducial_RecoOutsideFiducial;
    TH2F *mhPtVsEta_0p1_TrueOutsideFiducial_RecoInsideFiducial;
    
    TH2F *mhPtVsEta_0p2_RecoInsideFiducial;
    TH2F *mhPtVsEta_0p2_TrueInsideFiducial_RecoOutsideFiducial;
    TH2F *mhPtVsEta_0p2_TrueOutsideFiducial_RecoInsideFiducial;

    TH1F *mhMandelstamT_RecoInsideFiducial_LimitedMandelstamT;
    TH1F *mhMandelstamT_TrueInsideFiducial_RecoOutsideFiducial_LimitedMandelstamT;
    TH1F *mhMandelstamT_TrueOutsideFiducial_RecoInsideFiducial_LimitedMandelstamT;
    
    TH2F *mhResponseMatrix_MandelstamTSum;
    TH2F *mhResponseMatrix_DeltaPhi;
    
    TH2F *mhPtRecoMinusTrueWidthVsPtVsEta;
    TH2F *mhEtaRecoMinusTrueWidthVsPtVsEta;
    
    TEfficiency *mAcceptanceForDeltaPhiCuts_Vs_t1t2[4];
    TH2F *mAcceptanceForDeltaPhiCuts_Vs_t1t2_PASSED[4];
    TH2F *mAcceptanceForDeltaPhiCuts_Vs_t1t2_TOTAL[4];
    
    ClassDef(produceMcPredictions_ROOT,0);
};

#endif
