#ifndef CepAnalysis_hh
#define CepAnalysis_hh

#include <TROOT.h>
#include <TFile.h>
#include <TString.h>
#include "CepUtil.h"
// #include "ObservableObject.hh"
#include "CepParams.h"
// #include "Efficiencies.hh"
#include "DrawingOptions.hh"
#include "SystematicsOutput.hh"

class TH2;
class TF2;
class TH1F;
class TH2F;
class TCanvas;


Double_t etaVsPt(Double_t *x, Double_t *par);



class CepAnalysis{
public:
    
    CepAnalysis(TString opt="");
    ~CepAnalysis();
    
    void analyze();
    
private:
    
    CepUtil *mCepUtil;
    CepParams *mParams;
    //   Efficiencies *mEff;
    
    const double nSigmaMomentumBalanceCut = 3.0;
    static constexpr int nFilesWithAcceptanceCorr = 3;
    
    void setupDrawingStyle() const;
    static void setBlackAndWhiteColorScale();
    static void setWhiteAndBlackColorScale();
    static void setBlueWhiteRedColorScale();
    
    void getListOfAnalysedRuns(TFile *);
    void readLumiFiles();
    void getTotalIntegratedLuminosity();
    
    void pullAnalysis(TFile *);
    
    void drawInDetEff();
    
    void drawPtCorrections(TFile *);
    
    void closureTests(TFile *, TFile *, TFile *, TFile *);
    
    void transformToCrossSection(TH1*) const;
    SystematicsOutput calculateSystematicErrors( TH1F* const, TH1F** const, TH1F* const) const;
    void drawFinalResult(TH1F*, DrawingOptions &, SystematicsOutput &) const;
    void drawDataMcComparison2(TH1F*, TH1F*, DrawingOptions & opt, bool = false);
    TH2F* getTH2F_from_TH1F_t1t2( const TH1F*, bool = true );
    void correctClosureTestErrors(TH1F*, TH1F*, TH1F*) const;
    
    TCanvas* drawComparisonAndRatio(TH1*, TH1*, TString, TString, TString, TString, TString, TString, double, double, double, double, TLine* = nullptr, TLine* = nullptr, TPad* = nullptr, TString = TString(""), TString = TString(""));
    TCanvas* drawComparisonAndRatio(TH1*, TH1*, TH1*, TH1*, TString, TString, TString, TString, TString, TString, TString, TString, double, double, double, double, TLine* = nullptr, TLine* = nullptr, TPad* = nullptr, TString = TString(""), TString = TString(""));
    TCanvas* drawComparisonAndRatio(TH1*, TH1*, TH1*, TH1*, TH1*, TH1*, TString, TString, TString, TString, TString, TString, TString, TString, TString, TString, double, double, double, double, TLine* = nullptr, TLine* = nullptr, TPad* = nullptr, TString = TString(""), TString = TString(""));
    double mLegendWidth;
    double mLegendHeight;
    bool mPrint;
    TString mPdfName;
    TString mPdfNameSuffix;
    
    void drawResolutions();
    
    void drawRel21VsRel20();
    
    void drawMbtsEff(TFile *, TFile *);
    
    void drawDEdx(TFile *, TFile * =nullptr, TFile * =nullptr, TFile * =nullptr, TFile * =nullptr);
    
    void drawInDetPlots(TFile *);
    
    void drawNSigmaMissingPt(TFile *);
    
    void drawMomentumBalancePxPy(TFile *, TFile *);
    
    void drawRecoMethodsComparison(TFile *, TFile *, TFile * = nullptr);
    
    void calculateDeltaZ0Eff();
    
    Double_t mSpecialLegendMarginFactor;
    Bool_t mSpecialLabelRatio;
    Double_t mSpecialLabelOffsetFactor;
    Bool_t mEnableLegend;
    
    TString* mSystCheckName;
    TString* mSystCheckShortName;
    
    double totalIntegratedLuminosity; // mub^-1
    vector<int> runNumber;
    vector<int> colors;
    
    // all LBs are numbered starting from 1 and there are no gaps between LBs
    map<int, vector<double> > lumiPerLumiBlockVector_RunMap;
    map<int, vector<double> > lumiblockDurationVector_RunMap;
    map<int, vector<bool> > isPhysicsLumiBlockVector_RunMap;
    
    map<int, vector<int> > analysedLumiBlockVector_RunMap;
    
    TH2F *mhNEventsVsLumiBlockVsRunNumber;

    
    
    TH3F *mhNSigmaMissingPtVsDeltaZ0VsMinPt_2Pi;
    TH3F *mhNSigmaMissingPtVsDeltaZ0VsMinPt_4Pi;
    TH3F *mhNSigmaMissingPtVsDeltaZ0VsMinPt_6Pi;
    TH3F *mhNSigmaMissingPtVsDeltaZ0VsMinPt_8Pi;
    
    ClassDef(CepAnalysis,0);
};

#endif
