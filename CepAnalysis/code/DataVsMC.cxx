#define DataVsMC_cxx

#include "DataVsMC.hh"
#include <TH2.h> 
#include <TF1.h> 
#include <TF2.h> 
#include <THStack.h> 
#include <TStyle.h> 
#include <TMultiGraph.h> 
#include <TGraph.h> 
#include <TGraph2D.h> 
#include <TGraphAsymmErrors.h> 
#include <TEfficiency.h> 
#include <TFractionFitter.h>
#include <TObjArray.h>
#include <iostream>
#include <utility>
#include <sstream> 
#include <algorithm> 
#include <stdio.h> 
#include <stdlib.h> 
#include <TCanvas.h> 
#include <TNamed.h>
#include <TLegend.h> 
#include <TBox.h>
#include <TGaxis.h> 
#include <vector> 
#include <fstream> 
#include <TString.h> 
#include <TColor.h> 
#include <TLine.h> 
#include <cmath> 
#include <TExec.h>
#include <TEllipse.h>
#include <TFitResultPtr.h> 
#include <TFitResult.h> 
#include <TLatex.h> 
#include <TExec.h>
#include <TMath.h>
#include <TArrow.h>
#include <TPaletteAxis.h>
#include <TRandom3.h>
#include <sys/stat.h>
#include "CepParams.h"
#include "CepHistograms_DataVsMC.h"

ClassImp(DataVsMC)


DataVsMC::DataVsMC(CepUtil::CEP_CHANNELS ch):   mChannel{ch},
                                                mUtil{CepUtil::instance()},
                                                mParams{CepParams::instance()},
                                                mDirName{TString("PDF_DataVsMC/")+mUtil->mCepChannelName[mChannel]+TString("/")},
                                                mChannelStr{mUtil->mCepChannelLatexName[mChannel]},
                                                mGeneralChannelStr{ Form("%sh^{+}%sh^{-}", mUtil->mCepChannelMult[mChannel]>2 ? Form("%d", mUtil->mCepChannelMult[mChannel]/2) : "", mUtil->mCepChannelMult[mChannel]>2 ? Form("%d", mUtil->mCepChannelMult[mChannel]/2) : "") },
                                                mTotalChargeStr_OPPO{ mUtil->mCepChannelMult[mChannel]>2 ? Form("Q_{tot} = 0:") : "Opposite sign:" },
                                                mTotalChargeStr_SAME{ mUtil->mCepChannelMult[mChannel]>2 ? Form("Q_{tot} #neq 0:") : "Same sign:" }{
    
    mkdir("PDF_DataVsMC", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir(mDirName.Data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    
    mSubtractNeutrals = (mChannel==CepUtil::PIPI /*|| mChannel==CepUtil::KK || mChannel==CepUtil::PPBAR */|| mChannel==CepUtil::FOURPI) ? true : false;
    //   mNeutralsFracToPreserve = 1.0;
    //   mNeutralsFracToPreserve = 0.6;
    //   mNeutralsFracToPreserve = 0.3;
    mNeutralsFracToPreserve = mChannel==CepUtil::FOURPI ? 0.65 : 0.4;
    mBkgdSubtractionDemonstration = /*true*/false;
}



DataVsMC::~DataVsMC(){
    if(mUtil) delete mUtil;
}

  
  
void DataVsMC::analyze(){
    
    int myBlueIndex = TColor::GetFreeColorIndex();
    TColor *myBlue = new TColor( myBlueIndex, 1.15*136./255, 1.15*175./255, 1.0 );
    
    TString singleTrkStr[2];
    singleTrkStr[CepUtil::MINUS] = TString((mChannel!=CepUtil::KK && mChannel!=CepUtil::PPBAR) ? "#pi^{-}" : (mChannel==CepUtil::KK ? "K^{-}" : "#bar{#it{p}}") );
    singleTrkStr[CepUtil::PLUS] = TString((mChannel!=CepUtil::KK && mChannel!=CepUtil::PPBAR) ? "#pi^{+}" : (mChannel==CepUtil::KK ? "K^{+}" : "#it{p}") );
    
    //---
    
    const double maxNSigmaMissingPt = mParams->nSigmaMomentumBalanceCut;
    const double maxEta = 2.5;
    const double minInDetTrackPt = mChannel==CepUtil::KK ? mParams->minPt[CepUtil::KAON] : (mChannel==CepUtil::PPBAR ? mParams->minPt[CepUtil::PROTON] : mParams->minPt[CepUtil::PION]) ;
    
    loadHistograms();
    setupDrawingStyle();
    
    mInverseStackDrawing = true;
    mSpecialDataLegendDesctiption = TString("");
    mDrawSpecialRatio = false;
    mDrawSpecialRatio2 = false;
    mDrawSpecialRatio3 = false;
    mSpecialDataDrawing = false;
    mSpecialDataDrawing2 = false;
    mRatioFactor = 0.25;
    mRatioYmin = 0.2;
    mRatioYmax = 1.8;
    mRatioYTitleOffsetFactor = 1.0;
    mRatioYTitle = TString("Data / MC");
    
    mAuxiliaryHist = nullptr;
    
    TH1F* referenceHist;
    TH1F* referenceHist_SameSign;
    
    int histColor[nFiles];
    histColor[DATA] = kBlack;
    histColor[MC_SIGNAL] = mChannel==CepUtil::PIPI ? (kYellow-7) : ( mChannel==CepUtil::KK ? (/*kTeal-8*/kOrange+1) : ( mChannel==CepUtil::PPBAR ? myBlueIndex : (kRed-5)));
    histColor[MC_PYTHIA_CD_BKGD] =  mChannel==CepUtil::FOURPI ? (kGray+2) : (kGreen-1);
    histColor[MC_PYTHIA_CD_BKGD_NEUTRALS] = mChannel==CepUtil::FOURPI ? kGray : (kGreen+1);
    
    int histColor2[nFiles];
    histColor2[DATA] = kRed;
    histColor2[MC_SIGNAL] =  mChannel==CepUtil::PIPI ? (kYellow-7) : (kRed-9);
    histColor2[MC_PYTHIA_CD_BKGD] = kCyan;
    histColor2[MC_PYTHIA_CD_BKGD_NEUTRALS] = kWhite;
    
    TString legendText[nFiles];
    legendText[DATA] = TString("Data");
    legendText[MC_SIGNAL] = TString("Exclusive "+mChannelStr);
    legendText[MC_PYTHIA_CD_BKGD] = TString("CD w/o "+mGeneralChannelStr+"N");
    legendText[MC_PYTHIA_CD_BKGD_NEUTRALS] = TString( (mSubtractNeutrals && mNeutralsFracToPreserve<1.0) ? Form("CD %sN #times %g", mGeneralChannelStr.Data(), mNeutralsFracToPreserve) : Form("CD %sN", mGeneralChannelStr.Data()) );
    
    TString legendText2[nFiles];
    legendText2[DATA] = TString(mBkgdSubtractionDemonstration ? "Data (SS)" : "Data");
    legendText2[MC_SIGNAL] = TString("no such process");
    legendText2[MC_PYTHIA_CD_BKGD] = TString(mBkgdSubtractionDemonstration ? "CD (SS)" : "CD");
    legendText2[MC_PYTHIA_CD_BKGD_NEUTRALS] = TString("CD "+mGeneralChannelStr);
    
    
    double normalizationFactor[nFiles];
    double normalizationFactor_weighted[nFiles];
    double ss_scaleFactor;
    double ss_scaleFactor_weighted;
    
    const double missingPtToNormalizeCD_min = 5;
    const double missingPtToNormalizeCD_max = 10;
    
    for(int i=0; i<2; ++i){
        auto normArr = i==0 ? normalizationFactor : normalizationFactor_weighted;
        auto h = i==0 ? hNSigmaMissingPt : hNSigmaMissingPt_weighted;
        
        normArr[DATA] = 1.0;
        
        TH1F *hData_missingPt = h[DATA][CepUtil::OPPO];
        TH1F *hCEP_missingPt = h[MC_SIGNAL][CepUtil::OPPO];
        TH1F *hCD_missingPt = new TH1F( *h[MC_PYTHIA_CD_BKGD][CepUtil::OPPO] );
        hCD_missingPt->Add( h[MC_PYTHIA_CD_BKGD_NEUTRALS][CepUtil::OPPO] );
        
        normArr[MC_PYTHIA_CD_BKGD] = (hData_missingPt->Integral(hData_missingPt->GetXaxis()->FindBin(missingPtToNormalizeCD_min), hData_missingPt->GetXaxis()->FindBin(missingPtToNormalizeCD_max-kEps))) / hCD_missingPt->Integral(hCD_missingPt->GetXaxis()->FindBin(missingPtToNormalizeCD_min), hCD_missingPt->GetXaxis()->FindBin(missingPtToNormalizeCD_max-kEps));
        normArr[MC_PYTHIA_CD_BKGD_NEUTRALS] = normArr[MC_PYTHIA_CD_BKGD];
        
        
        normArr[MC_SIGNAL] = ( hData_missingPt->Integral(0, hData_missingPt->GetXaxis()->FindBin(maxNSigmaMissingPt)-kEps)
        - normArr[MC_PYTHIA_CD_BKGD] * hCD_missingPt->Integral(0, hCD_missingPt->GetXaxis()->FindBin(maxNSigmaMissingPt-kEps)) )
        / hCEP_missingPt->Integral(0, hCEP_missingPt->GetXaxis()->FindBin(maxNSigmaMissingPt-kEps));
    }
    
    
    //   TFile *fOutput = new TFile("DataVsMCDataVsMC.root", "RECREATE");
    //BEGIN ---------------- ---------------- ---------------- ---------------- ---------------- ---------------- ---------------- ---------------- ---------------- ---------------- ----------------
    
    TH1F *totalData;
    TH1F *totalData_SameSign;
    TH1F *totalMC[nFiles];
    TH1F *totalMC_SameSign[nFiles];
    
    
    
    double integralInSignalRegion[nFiles][CepUtil::nChargeSum];
    double integralNSigmaMissingPt[nFiles][CepUtil::nChargeSum];
    //   
    //   
    //BEGIN                                       MISSING PT                                                        
    
    {
        referenceHist = hNSigmaMissingPt[DATA][CepUtil::OPPO];
        integralInSignalRegion[DATA][CepUtil::OPPO] = integratePtMiss( referenceHist, maxNSigmaMissingPt );
        integralNSigmaMissingPt[DATA][CepUtil::OPPO] = referenceHist->Integral();
        referenceHist->Scale(referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) ), "width");
        
        totalData = new TH1F( *referenceHist );
        
        referenceHist_SameSign = hNSigmaMissingPt[DATA][CepUtil::SAME];
        integralInSignalRegion[DATA][CepUtil::SAME] = integratePtMiss( referenceHist_SameSign, maxNSigmaMissingPt );
        integralNSigmaMissingPt[DATA][CepUtil::SAME] = referenceHist_SameSign->Integral();
        referenceHist_SameSign->Scale(referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) ), "width");
        
        totalData_SameSign = new TH1F( *referenceHist_SameSign );
        
        DrawingOptions opts;
        
        for(unsigned int fileId=DATA+1; fileId<nFiles; ++fileId){
            
            TH1F *mcHist = hNSigmaMissingPt[fileId][CepUtil::OPPO];
            integralInSignalRegion[fileId][CepUtil::OPPO] = normalizationFactor[fileId] * integratePtMiss( mcHist, maxNSigmaMissingPt );
            integralNSigmaMissingPt[fileId][CepUtil::OPPO] = normalizationFactor[fileId] * mcHist->Integral();
            mcHist->Scale( normalizationFactor[fileId] * mcHist->GetBinWidth( mcHist->FindBin(0.0001) ), "width");
            
            mcHist->SetLineColor( kBlack );
            mcHist->SetLineWidth( 2 );
            mcHist->SetFillColor( histColor[fileId] );
            
            totalMC[fileId] = new TH1F( *mcHist );
            
            opts.mHistTH1F.push_back( mcHist );
            opts.mHistTH1F_DrawingOpt.push_back( "HIST" );
            
            opts.mObjForLegend.push_back( mcHist );
            opts.mDesriptionForLegend.push_back( legendText[fileId] );
            opts.mDrawTypeForLegend.push_back( "f" );
            
            if( fileId==MC_PYTHIA_CD_BKGD_NEUTRALS ) continue;
            
            TH1F *mcHist_SameSign = hNSigmaMissingPt[fileId][CepUtil::SAME];
            integralInSignalRegion[fileId][CepUtil::SAME] = normalizationFactor[fileId] * integratePtMiss( mcHist_SameSign, maxNSigmaMissingPt );
            integralNSigmaMissingPt[fileId][CepUtil::SAME] = normalizationFactor[fileId] * mcHist_SameSign->Integral();
            mcHist_SameSign->Scale( normalizationFactor[fileId] * mcHist->GetBinWidth( mcHist->FindBin(0.0001) ), "width");
            
            mcHist_SameSign->SetLineColor( histColor2[fileId]/*+2*/ );
            mcHist_SameSign->SetLineWidth( 2 );
            mcHist_SameSign->SetFillColor( histColor2[fileId] );
            mcHist_SameSign->SetFillStyle( 3345 );
            
            totalMC_SameSign[fileId] = new TH1F( *mcHist_SameSign );
            
            opts.mHistTH1F2.push_back( mcHist_SameSign );
            opts.mHistTH1F_DrawingOpt2.push_back( "HIST" );
            
            if( mBkgdSubtractionDemonstration ){
                if( fileId==MC_SIGNAL )
                    continue;
                opts.mObjForLegend.push_back( mcHist_SameSign );
                opts.mDesriptionForLegend.push_back( legendText2[fileId] );
                opts.mDrawTypeForLegend.push_back( "f" );
            } else{
                opts.mObjForLegend2.push_back( fileId==MC_SIGNAL ? static_cast<TObject*>(nullptr) : mcHist_SameSign );
                opts.mDesriptionForLegend2.push_back( fileId==MC_SIGNAL ? "" : legendText2[fileId] );
                opts.mDrawTypeForLegend2.push_back( fileId==MC_SIGNAL ? "" : "f" );
            }
        }
        
        opts.mCanvasWidth = mBkgdSubtractionDemonstration ? 1200 : 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = mBkgdSubtractionDemonstration ? 0.09 : 0.12;
        opts.mRightMargin = 0.035;
        opts.mTopMargin = 0.06;
        opts.mBottomMargin = 0.12;
        
        //x axis
        opts.mXmin = 0;
        opts.mXmax = mBkgdSubtractionDemonstration ? 9.9 : 30;
        opts.mXaxisTitle = TString("n#sigma(p_{T}^{miss})");
        opts.mXaxisTitleSize = 0.05;
        opts.mXaxisTitleOffset = 1.04;
        opts.mXaxisLabelSize = 0.05;
        opts.mXaxisLabelOffset = 0.007;
        opts.mXaxisTickLength = 0.015;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = (mBkgdSubtractionDemonstration ? 0.105 : 1.35 ) * referenceHist->GetMaximum();
        opts.mYaxisTitle = TString(Form("Number of events / %g", referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) )) );
        opts.mYaxisTitleSize = 0.05;
        opts.mYaxisTitleOffset = mBkgdSubtractionDemonstration ? 0.9 : 1.25;
        opts.mYaxisLabelSize = 0.05;
        opts.mYaxisLabelOffset = 0.007;
        opts.mYaxisTickLength = 0.015;
        opts.mYlog = kFALSE;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = mBkgdSubtractionDemonstration ? 2.4 : 1.4;
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mMarkerStyle2 = 20;
        opts.mMarkerColor2 = kRed;
        opts.mMarkerSize2 = mBkgdSubtractionDemonstration ? 2.4 : 1.4;
        opts.mLineStyle2 = 1;
        opts.mLineColor2 = kRed;
        opts.mLineWidth2 = 2;
        
        opts.mPdfName = mBkgdSubtractionDemonstration ? TString("BkgdSubtractionDemonstration_NSigmaMissingPt" ) : TString("NSigmaMissingPt");
        
        opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        opts.mNColumns = 2;
        opts.mLegendX = mBkgdSubtractionDemonstration ? 0.5 : 0.42;
        opts.mLegendXwidth = mBkgdSubtractionDemonstration ? 0.43 : 0.55;
        opts.mLegendY = 0.45;
        opts.mLegendYwidth = mBkgdSubtractionDemonstration ? 0.35 : 0.14;
        mSpecialLegendMarginFactor = mBkgdSubtractionDemonstration ? 0.65 : 1.2;
        
        if( !mBkgdSubtractionDemonstration ){
            opts.mLegendTextSize2 = 0.7 * opts.mXaxisTitleSize;
            opts.mLegendX2 = opts.mLegendX;
            opts.mLegendXwidth2 = opts.mLegendXwidth;
            opts.mLegendY2 = opts.mLegendY - 0.1;
            opts.mLegendYwidth2 = opts.mLegendYwidth;
        }
        
        
        if( mBkgdSubtractionDemonstration ){
            TF1 *bkgdFunc = new TF1();
            double bkgdFraction = mUtil->bkgdFraction(referenceHist, mParams->nSigmaMomentumBalanceCut, 5, 10, bkgdFunc);
            bkgdFunc->SetRange(5, 10);
            bkgdFunc->SetLineColorAlpha(kMagenta, 0.5);
            bkgdFunc->SetLineWidth(20);
            opts.mObjectToDraw.push_back(bkgdFunc);
            TF1 *bkgdFuncExtrapolated = new TF1(*bkgdFunc);
            bkgdFuncExtrapolated->SetRange(0, 5);
            bkgdFuncExtrapolated->SetLineColor(kMagenta);
            bkgdFuncExtrapolated->SetLineWidth(10);
            bkgdFuncExtrapolated->SetLineStyle(7);
            opts.mObjectToDraw.push_back(bkgdFuncExtrapolated);
            
            opts.mObjForLegend.push_back( bkgdFunc );
            opts.mDesriptionForLegend.push_back( "Non-excl. bkgd (fit)" );
            opts.mDrawTypeForLegend.push_back( "l" );
            opts.mObjForLegend.push_back( bkgdFuncExtrapolated );
            opts.mDesriptionForLegend.push_back( "#splitline{Non-excl. bkgd}{(fit extrapolation)}" );
            opts.mDrawTypeForLegend.push_back( "l" );
            
            TH1F *ss_scaled = new TH1F( *referenceHist_SameSign );
            ss_scaled->SetName("ss_scaled_DRAW_SAME");
            
            ss_scaleFactor = referenceHist->Integral(referenceHist->FindBin(5.01), referenceHist->FindBin(9.99)) / referenceHist_SameSign->Integral(referenceHist_SameSign->FindBin(5.01), referenceHist_SameSign->FindBin(9.99));
            ss_scaled->Scale( ss_scaleFactor );
            ss_scaled->SetMarkerStyle(34);
            ss_scaled->SetMarkerSize(3.5);
            ss_scaled->SetLineWidth(2);
            ss_scaled->SetLineColor(/*kRed*/kBlue);
            ss_scaled->SetMarkerColorAlpha(/*kRed*/kBlue, 0.7);
            opts.mObjectToDraw.push_back(ss_scaled);
            
            opts.mObjForLegend.push_back( ss_scaled );
            opts.mDesriptionForLegend.push_back( "#splitline{Non-excl. bkgd}{(SS data scaled)}" );
            opts.mDrawTypeForLegend.push_back( "pe" );
            
//             opts.mXmin = 0;
//             opts.mXmax = 0.3;
            
//             opts.mYmin = 0;
//             opts.mYmax = 0.23 * referenceHist->GetMaximum();
            
//             opts.mLegendYwidth *= 1.6;
//             opts.mLegendXwidth *= 1.03;
//             opts.mLegendY -= 0.32;
            
//             mSpecialLegendMarginFactor = 0.8;
        }
        
        
        
        double labelShiftDown = 0.04;
        
        mInverseStackDrawing = true;
        
        
        TLine *cutLine = new TLine( maxNSigmaMissingPt, 0, maxNSigmaMissingPt, (mBkgdSubtractionDemonstration ? 0.07 : 0.4)*referenceHist->GetMaximum() );
        cutLine->SetLineWidth(4);
        cutLine->SetLineColor(kRed);
        cutLine->SetLineStyle(7);
        cutLine->Draw();
        opts.mObjectToDraw.push_back(cutLine);
        
        TArrow *cutArrow = new TArrow( maxNSigmaMissingPt, (mBkgdSubtractionDemonstration ? 0.07 : 0.4)*referenceHist->GetMaximum(), maxNSigmaMissingPt-2, (mBkgdSubtractionDemonstration ? 0.07 : 0.4)*referenceHist->GetMaximum(), 0.035, "|>");
        cutArrow->SetAngle(30);
        cutArrow->SetLineWidth(4);
        cutArrow->SetLineColor( kRed );
        cutArrow->SetFillColor( kRed );
        opts.mObjectToDraw.push_back(cutArrow);    
        
        TLatex lReaction(.32 + (mBkgdSubtractionDemonstration ? 0.18 : 0), .91-labelShiftDown, "#kern[-0.18]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+"+mChannelStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
//         TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
        //opts.mTextToDraw.push_back( lATLAS );
        
        if( !mBkgdSubtractionDemonstration ){
            TString tpcMinPt; tpcMinPt.Form("p_{T} > %g GeV", minInDetTrackPt );
            TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
            TString rpKinematicRegion1("0.18 GeV < |p_{y}| < 0.46 GeV");
            //     TString rpKinematicRegion3("p_{x} > -0.2 GeV");
            //     TString rpKinematicRegion2("(p_{x} + 0.3 GeV)^{2} + p_{y}^{2} < 0.25 GeV^{2}"); //opts.mTextToDraw.push_back( lATLAS );
            TLatex lCutsA(.16,.84-labelShiftDown, Form("%s, %s:", singleTrkStr[CepUtil::PLUS].Data(), singleTrkStr[CepUtil::MINUS].Data())); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
            TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
            TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
            TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
            TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
            TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
            //     TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
            TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
            //     TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );   
            
            //     TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
            //     TLatex lDeltaPhi(.25,.677, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
        }
        
        drawFinalResult( referenceHist, opts );
        
        TLatex lOppo(mUtil->mCepChannelMult[mChannel]==2 ? 0.22 : .28, .635-labelShiftDown, mTotalChargeStr_OPPO); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); 
        TLatex lSame(mUtil->mCepChannelMult[mChannel]==2 ? .256 : .28, .635-labelShiftDown - 0.15, mTotalChargeStr_SAME); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize);
        if(!mBkgdSubtractionDemonstration){
            opts.mTextToDraw.push_back( lOppo );
            opts.mTextToDraw.push_back( lSame );
        }
        
        double hatchesSpacing = gStyle->GetHatchesSpacing();
        double hatchesLineWidth = gStyle->GetHatchesLineWidth();
        gStyle->SetHatchesSpacing(3);
        gStyle->SetHatchesLineWidth(3);
        opts.mPdfName = mBkgdSubtractionDemonstration ? TString("BkgdSubtractionDemonstration_NSigmaMissingPt_OppositeAndSameSign") : TString("NSigmaMissingPt_OppositeAndSameSign");
        opts.mLegendY = 0.5;
        if(mBkgdSubtractionDemonstration)
            opts.mLegendY -= 0.23;
        drawFinalResult( referenceHist, opts, referenceHist_SameSign );
        
        {
            opts.mTopMargin -= 0.04;
            opts.mLegendY += 0.04 + 0.04;
            opts.mLegendY2 = opts.mLegendY - 0.15;
            mRatioFactor = 0.15;
            mRatioYmin = 0.62;
            mRatioYmax = 1.38;
            mRatioYTitleOffsetFactor = 0.5;
            mRatioYTitle = TString("#kern[-0.2]{Data/MC}");
            //log-y scale
            opts.mPdfName = mBkgdSubtractionDemonstration ? TString("BkgdSubtractionDemonstration_LogY_NSigmaMissingPt_OppositeAndSameSign") : TString("LogY_NSigmaMissingPt_OppositeAndSameSign");
            opts.mYlog = kTRUE;
            opts.mYmin = 20;
            opts.mYmax = 10000 * referenceHist->GetMaximum();
            opts.mObjectToDraw.clear();
            cutLine = new TLine( maxNSigmaMissingPt, opts.mYmin, maxNSigmaMissingPt, 2*referenceHist->GetMaximum() );
            cutLine->SetLineWidth(4);
            cutLine->SetLineColor(kRed);
            cutLine->SetLineStyle(7);
            cutLine->Draw();
            opts.mObjectToDraw.push_back(cutLine);
            cutArrow = new TArrow( maxNSigmaMissingPt, 2*referenceHist->GetMaximum(), maxNSigmaMissingPt-2, 2*referenceHist->GetMaximum(), 0.035, "|>");
            cutArrow->SetAngle(30);
            cutArrow->SetLineWidth(4);
            cutArrow->SetLineColor( kRed );
            cutArrow->SetFillColor( kRed );
            opts.mObjectToDraw.push_back(cutArrow);  
            
            opts.mTextToDraw.clear();
            
            labelShiftDown -= 0.04;
            
            TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.18]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+"+mChannelStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
            opts.mTextToDraw.push_back( lReaction );
            TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
            //opts.mTextToDraw.push_back( lATLAS );
            
            TString tpcMinPt; tpcMinPt.Form("p_{T} > %g GeV", minInDetTrackPt );
            TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
            TString rpKinematicRegion1("0.18 GeV < |p_{y}| < 0.46 GeV");
            //       TString rpKinematicRegion3("p_{x} > -0.2 GeV");
            //       TString rpKinematicRegion2("(p_{x} + 0.3 GeV)^{2} + p_{y}^{2} < 0.25 GeV^{2}"); //opts.mTextToDraw.push_back( lATLAS );
            TLatex lCutsA(.16,.84-labelShiftDown, Form("%s, %s:", singleTrkStr[CepUtil::PLUS].Data(), singleTrkStr[CepUtil::MINUS].Data())); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
            TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
            TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
            TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
            TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
            TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
            //       TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
            TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
            //       TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );
            
            
            TLatex lOppo(mUtil->mCepChannelMult[mChannel]==2 ? 0.22 : .236, .635-labelShiftDown + 0.04, mTotalChargeStr_OPPO); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
            TLatex lSame(mUtil->mCepChannelMult[mChannel]==2 ? .256 : .236, .635-labelShiftDown - 0.15 + 0.04, mTotalChargeStr_SAME); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
            
            
            //       TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
            //       TLatex lDeltaPhi(/*.25*/.16,.677 + 0.04 + 0.02, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
            
            drawFinalResult( referenceHist, opts, referenceHist_SameSign );
            
            // back to default
            mRatioFactor = 0.25;
            mRatioYmin = 0.2;
            mRatioYmax = 1.8;
            mRatioYTitleOffsetFactor = 1.0;
            mRatioYTitle = TString("Data / MC");
        }
        
        gStyle->SetHatchesSpacing(hatchesSpacing);
        gStyle->SetHatchesLineWidth(hatchesLineWidth);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    //BEGIN                                       INVARIANT MASS                                                        
    
    for(int ddd=0; ddd<2; ++ddd){
        
        TString extraStr(ddd==0?"":"_weighted");
        
        referenceHist = (ddd==0 ? hInvMass : hInvMass_weighted)[DATA][CepUtil::OPPO];
        referenceHist->Scale(referenceHist->GetBinWidth( referenceHist->FindBin(0.3) ), "width");
        
        totalData = new TH1F( *referenceHist );
        
        referenceHist_SameSign = (ddd==0 ? hInvMass : hInvMass_weighted)[DATA][CepUtil::SAME];
        referenceHist_SameSign->Scale(referenceHist->GetBinWidth( referenceHist->FindBin(0.3) ), "width");
        
        totalData_SameSign = new TH1F( *referenceHist_SameSign );
        
        DrawingOptions opts;
        
        for(unsigned int fileId=DATA+1; fileId<nFiles; ++fileId){
            
            TH1F *mcHist = (ddd==0 ? hInvMass : hInvMass_weighted)[fileId][CepUtil::OPPO];
            mcHist->Scale( (ddd==0 ? normalizationFactor : normalizationFactor_weighted)[fileId] * mcHist->GetBinWidth( mcHist->FindBin(0.3) ), "width");
            
            mcHist->SetLineColor( kBlack );
            mcHist->SetLineWidth( 2 );
            mcHist->SetFillColor( histColor[fileId] );
            
            totalMC[fileId] = new TH1F( *mcHist );
            
            opts.mHistTH1F.push_back( mcHist );
            opts.mHistTH1F_DrawingOpt.push_back( "HIST" );
            
            opts.mObjForLegend.push_back( mcHist );
            opts.mDesriptionForLegend.push_back( legendText[fileId] );
            opts.mDrawTypeForLegend.push_back( "f" );
            
            if( fileId==MC_PYTHIA_CD_BKGD_NEUTRALS ) continue;
            
            TH1F *mcHist_SameSign = (ddd==0 ? hInvMass : hInvMass_weighted)[fileId][CepUtil::SAME];
            mcHist_SameSign->Scale( (ddd==0 ? normalizationFactor : normalizationFactor_weighted)[fileId] * mcHist->GetBinWidth( mcHist->FindBin(0.3) ), "width");
            
            mcHist_SameSign->SetLineColor( histColor2[fileId]/*+2*/ );
            mcHist_SameSign->SetLineWidth( 2 );
            mcHist_SameSign->SetFillColor( histColor2[fileId] );
            mcHist_SameSign->SetFillStyle( 3345 );
            
            totalMC_SameSign[fileId] = new TH1F( *mcHist_SameSign );
            
            opts.mHistTH1F2.push_back( mcHist_SameSign );
            opts.mHistTH1F_DrawingOpt2.push_back( "HIST" );
            
            if( mBkgdSubtractionDemonstration ){
                if( fileId==MC_SIGNAL )
                    continue;
                opts.mObjForLegend.push_back( mcHist_SameSign );
                opts.mDesriptionForLegend.push_back( legendText2[fileId] );
                opts.mDrawTypeForLegend.push_back( "f" );
            } else{
                opts.mObjForLegend2.push_back( fileId==MC_SIGNAL ? static_cast<TObject*>(nullptr) : mcHist_SameSign );
                opts.mDesriptionForLegend2.push_back( fileId==MC_SIGNAL ? "" : legendText2[fileId] );
                opts.mDrawTypeForLegend2.push_back( fileId==MC_SIGNAL ? "" : "f" );
            }
        }
        
        opts.mCanvasWidth = mBkgdSubtractionDemonstration ? 1200 : 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = mBkgdSubtractionDemonstration ? 0.09 : 0.12;
        opts.mRightMargin = 0.035;
        opts.mTopMargin = 0.06;
        opts.mBottomMargin = 0.12;
        
        //x axis
        opts.mXmin = 2*mUtil->mPion;
        opts.mXmax = /*mBkgdSubtractionDemonstration ? 9.9 : 30*/3.0;
        opts.mXaxisTitle = TString("m [GeV]");
        opts.mXaxisTitleSize = 0.05;
        opts.mXaxisTitleOffset = 1.04;
        opts.mXaxisLabelSize = 0.05;
        opts.mXaxisLabelOffset = 0.007;
        opts.mXaxisTickLength = 0.015;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = (mBkgdSubtractionDemonstration ? (ddd==0 ? 0.105 : 0.14) : 1.6 ) * referenceHist->GetMaximum();
        opts.mYaxisTitle = TString(Form("Number of %s / %g MeV", ddd==0 ? "events" : "weighted events", 1e3*referenceHist->GetBinWidth( referenceHist->FindBin(0.3) )) );
        opts.mYaxisTitleSize = 0.05;
        opts.mYaxisTitleOffset = mBkgdSubtractionDemonstration ? 0.9 : 1.25;
        opts.mYaxisLabelSize = 0.05;
        opts.mYaxisLabelOffset = 0.007;
        opts.mYaxisTickLength = 0.015;
        opts.mYlog = kFALSE;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = mBkgdSubtractionDemonstration ? 2.4 : 1.4;
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mMarkerStyle2 = 20;
        opts.mMarkerColor2 = kRed;
        opts.mMarkerSize2 = mBkgdSubtractionDemonstration ? 2.4 : 1.4;
        opts.mLineStyle2 = 1;
        opts.mLineColor2 = kRed;
        opts.mLineWidth2 = 2;
        
        opts.mPdfName = mBkgdSubtractionDemonstration ? TString("BkgdSubtractionDemonstration_InvMass" ) : TString("InvMass");
        opts.mPdfName += extraStr;
        
        opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        opts.mNColumns = 2;
        opts.mLegendX = mBkgdSubtractionDemonstration ? 0.45 : 0.42;
        opts.mLegendXwidth = mBkgdSubtractionDemonstration ? 0.43 : 0.55;
        opts.mLegendY = 0.45;
        opts.mLegendYwidth = mBkgdSubtractionDemonstration ? 0.35 : 0.14;
        mSpecialLegendMarginFactor = mBkgdSubtractionDemonstration ? 0.65 : 1.2;
        
        if( !mBkgdSubtractionDemonstration ){
            opts.mLegendTextSize2 = 0.7 * opts.mXaxisTitleSize;
            opts.mLegendX2 = opts.mLegendX + 0.28;
            opts.mLegendXwidth2 = opts.mLegendXwidth;
            opts.mLegendY2 = opts.mLegendY - 0.15;
            opts.mLegendYwidth2 = opts.mLegendYwidth;
        }
        
        
        if( mBkgdSubtractionDemonstration ){
            
//             TF1 *bkgdFunc = new TF1();
//             double bkgdFraction = mUtil->bkgdFraction(referenceHist, mParams->nSigmaMomentumBalanceCut, 5, 10, bkgdFunc);
//             bkgdFunc->SetRange(5, 10);
//             bkgdFunc->SetLineColorAlpha(kMagenta, 0.5);
//             bkgdFunc->SetLineWidth(20);
//             opts.mObjectToDraw.push_back(bkgdFunc);
//             TF1 *bkgdFuncExtrapolated = new TF1(*bkgdFunc);
//             bkgdFuncExtrapolated->SetRange(0, 5);
//             bkgdFuncExtrapolated->SetLineColor(kMagenta);
//             bkgdFuncExtrapolated->SetLineWidth(10);
//             bkgdFuncExtrapolated->SetLineStyle(7);
//             opts.mObjectToDraw.push_back(bkgdFuncExtrapolated);
//             
//             opts.mObjForLegend.push_back( bkgdFunc );
//             opts.mDesriptionForLegend.push_back( "Non-excl. bkgd (fit)" );
//             opts.mDrawTypeForLegend.push_back( "l" );
//             opts.mObjForLegend.push_back( bkgdFuncExtrapolated );
//             opts.mDesriptionForLegend.push_back( "#splitline{Non-excl. bkgd}{(fit extrapolation)}" );
//             opts.mDrawTypeForLegend.push_back( "l" );
            
            TH1F *ss_scaled = new TH1F( *referenceHist_SameSign );
            ss_scaled->SetName("ss_scaled_2_DRAW_SAME");
            
            ss_scaled->Scale( ss_scaleFactor );
            ss_scaled->SetMarkerStyle(34);
            ss_scaled->SetMarkerSize(3.5);
            ss_scaled->SetLineWidth(2);
            ss_scaled->SetLineColor(/*kRed*/kBlue);
            ss_scaled->SetMarkerColorAlpha(/*kRed*/kBlue, 0.7);
            opts.mObjectToDraw.push_back(ss_scaled);
            
            opts.mObjForLegend.push_back( ss_scaled );
            opts.mDesriptionForLegend.push_back( "SS data scaled" );
            opts.mDrawTypeForLegend.push_back( "pe" );
            
            //---
            
            auto h = ddd==0 ? hNSigmaPtMissVsInvMass : hNSigmaPtMissVsInvMass_weighted;
                        
            TH2F * hNSigmaMissingPtVsMass_totalMC_oppo = new TH2F(*h[1][CepUtil::CH_SUM_0]);
            hNSigmaMissingPtVsMass_totalMC_oppo->Reset("ICESM");
            TH2F * hNSigmaMissingPtVsMass_totalMC_same = new TH2F(*h[1][CepUtil::CH_SUM_NOT_0]);
            hNSigmaMissingPtVsMass_totalMC_same->Reset("ICESM");
            for(int j=DATA+1; j<nFiles; ++j){
                hNSigmaMissingPtVsMass_totalMC_oppo->Add( h[j][CepUtil::CH_SUM_0], (ddd==0 ? normalizationFactor : normalizationFactor_weighted)[j] );
                hNSigmaMissingPtVsMass_totalMC_same->Add( h[j][CepUtil::CH_SUM_NOT_0], (ddd==0 ? normalizationFactor : normalizationFactor_weighted)[j] );
            }
            
            
            TH1F *bkgdHistogram = dynamic_cast<TH1F*>( mUtil->bkgdHistogram( hNSigmaMissingPtVsMass_totalMC_oppo, mParams->nSigmaMomentumBalanceCut ) );
            TH1F *bkgdHistogram2 = mUtil->backgroundFromSameSignTemplate( hNSigmaMissingPtVsMass_totalMC_oppo, mParams->nSigmaMomentumBalanceCut, hNSigmaMissingPtVsMass_totalMC_same->ProjectionY() );
            TH1F *bkgdDifferenceHistogram = new TH1F( *bkgdHistogram ); bkgdDifferenceHistogram->SetName("bkgdDifferenceHistogram_" + TString(bkgdHistogram->GetName()));
            bkgdDifferenceHistogram->Add( bkgdHistogram2, -1 );

            bkgdHistogram->SetName( "bkgdHistogram_HIST_SAME" );
            bkgdHistogram->SetLineWidth(4);
            bkgdHistogram->SetLineStyle(2);
            bkgdHistogram->SetLineColor(/*kRed*/kGreen);
            opts.mObjectToDraw.push_back(bkgdHistogram);
            
            TH1F *ttt = new TH1F(*bkgdHistogram);
            ttt->SetName("E2_SAME");
            ttt->SetFillColorAlpha(/*kRed*/kGreen, 0.7);
            for(int j=1; j<=ttt->GetNbinsX(); ++j)
                ttt->SetBinError(j, /*10**/fabs(bkgdDifferenceHistogram->GetBinContent(j)));
            opts.mObjectToDraw.push_back(ttt);
            
            opts.mObjForLegend.push_back( bkgdHistogram );
            opts.mDesriptionForLegend.push_back( "#splitline{Non-excl. bkgd}{in MC}" );
            opts.mDrawTypeForLegend.push_back( "f" );
            
            //----
            
            TH1F *bkgdHistogram_data = dynamic_cast<TH1F*>( mUtil->bkgdHistogram( h[DATA][CepUtil::CH_SUM_0], mParams->nSigmaMomentumBalanceCut ) );
            TH1F *bkgdHistogram2_data = mUtil->backgroundFromSameSignTemplate( h[DATA][CepUtil::CH_SUM_0], mParams->nSigmaMomentumBalanceCut, h[DATA][CepUtil::CH_SUM_NOT_0]->ProjectionY() );
            TH1F *bkgdDifferenceHistogram_data = new TH1F( *bkgdHistogram_data ); bkgdDifferenceHistogram_data->SetName("bkgdDifferenceHistogram_data_" + TString(bkgdHistogram_data->GetName()));
            bkgdDifferenceHistogram_data->Add( bkgdHistogram2_data, -1 );

            bkgdHistogram_data->SetName( "bkgdHistogram_HIST_SAME" );
            bkgdHistogram_data->SetLineWidth(4);
            bkgdHistogram_data->SetLineColor(/*kRed*/kMagenta);
            opts.mObjectToDraw.push_back(bkgdHistogram_data);
            
            TH1F *ttt_data = new TH1F(*bkgdHistogram_data);
            ttt_data->SetName("E2_SAME");
            ttt_data->SetFillColorAlpha(/*kRed*/kMagenta, 0.3);
            for(int j=1; j<=ttt_data->GetNbinsX(); ++j)
                ttt_data->SetBinError(j, /*10**/fabs(bkgdDifferenceHistogram_data->GetBinContent(j)));
            opts.mObjectToDraw.push_back(ttt_data);
            
            opts.mObjForLegend.push_back( bkgdHistogram_data );
            opts.mDesriptionForLegend.push_back( "#splitline{Non-excl. bkgd}{in data}" );
            opts.mDrawTypeForLegend.push_back( "f" );
            
        }
        
        
        
        double labelShiftDown = 0.04;
        
        mInverseStackDrawing = true;
        
        TLatex lReaction(.32 + (mBkgdSubtractionDemonstration ? 0.18 : 0), .91-labelShiftDown, "#kern[-0.18]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+"+mChannelStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
//         TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
        //opts.mTextToDraw.push_back( lATLAS );
        
        if( !mBkgdSubtractionDemonstration ){
            TString tpcMinPt; tpcMinPt.Form("p_{T} > %g GeV", minInDetTrackPt );
            TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
            TString rpKinematicRegion1("0.18 GeV < |p_{y}| < 0.46 GeV");
            //     TString rpKinematicRegion3("p_{x} > -0.2 GeV");
            //     TString rpKinematicRegion2("(p_{x} + 0.3 GeV)^{2} + p_{y}^{2} < 0.25 GeV^{2}"); //opts.mTextToDraw.push_back( lATLAS );
            TLatex lCutsA(.16,.84-labelShiftDown, Form("%s, %s:", singleTrkStr[CepUtil::PLUS].Data(), singleTrkStr[CepUtil::MINUS].Data())); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
            TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
            TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
            TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
            TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
            TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
            //     TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
            TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
            //     TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );   
            
            //     TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
            //     TLatex lDeltaPhi(.25,.677, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
        }
        
        drawFinalResult( referenceHist, opts );
        
        TLatex lOppo(mUtil->mCepChannelMult[mChannel]==2 ? (0.22+0.3) : .28, .635-labelShiftDown+0.07, mTotalChargeStr_OPPO); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); 
        TLatex lSame(mUtil->mCepChannelMult[mChannel]==2 ? (.256+0.3) : .28, .635-labelShiftDown - 0.15, mTotalChargeStr_SAME); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize);
        if(!mBkgdSubtractionDemonstration){
            opts.mTextToDraw.push_back( lOppo );
            opts.mTextToDraw.push_back( lSame );
        }
        
        double hatchesSpacing = gStyle->GetHatchesSpacing();
        double hatchesLineWidth = gStyle->GetHatchesLineWidth();
        gStyle->SetHatchesSpacing(3);
        gStyle->SetHatchesLineWidth(3);
        opts.mPdfName = mBkgdSubtractionDemonstration ? TString("BkgdSubtractionDemonstration_InvMass_OppositeAndSameSign") : TString("InvMass_OppositeAndSameSign");
        opts.mPdfName += extraStr;
        opts.mLegendY = mBkgdSubtractionDemonstration ? 0.6 : 0.5;
        if(mBkgdSubtractionDemonstration)
            opts.mLegendY -= 0.23;
        drawFinalResult( referenceHist, opts, referenceHist_SameSign );
        
        {
            opts.mTopMargin -= 0.04;
            opts.mLegendY += 0.04 + 0.04;
            opts.mLegendY2 = opts.mLegendY - 0.15;
            mRatioFactor = 0.15;
            mRatioYmin = 0.62;
            mRatioYmax = 1.38;
            mRatioYTitleOffsetFactor = 0.5;
            mRatioYTitle = TString("#kern[-0.2]{Data/MC}");
            //log-y scale
            opts.mPdfName = mBkgdSubtractionDemonstration ? TString("BkgdSubtractionDemonstration_LogY_InvMass_OppositeAndSameSign") : TString("LogY_InvMass_OppositeAndSameSign");
            opts.mPdfName += extraStr;
            opts.mYlog = kTRUE;
            opts.mYmin = 20;
            opts.mYmax = 10000 * referenceHist->GetMaximum();
            opts.mObjectToDraw.clear(); 
            
            opts.mTextToDraw.clear();
            
            labelShiftDown -= 0.04;
            
            TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.18]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+"+mChannelStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
            opts.mTextToDraw.push_back( lReaction );
            TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
            //opts.mTextToDraw.push_back( lATLAS );
            
            TString tpcMinPt; tpcMinPt.Form("p_{T} > %g GeV", minInDetTrackPt );
            TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
            TString rpKinematicRegion1("0.18 GeV < |p_{y}| < 0.46 GeV");
            //       TString rpKinematicRegion3("p_{x} > -0.2 GeV");
            //       TString rpKinematicRegion2("(p_{x} + 0.3 GeV)^{2} + p_{y}^{2} < 0.25 GeV^{2}"); //opts.mTextToDraw.push_back( lATLAS );
            TLatex lCutsA(.16,.84-labelShiftDown, Form("%s, %s:", singleTrkStr[CepUtil::PLUS].Data(), singleTrkStr[CepUtil::MINUS].Data())); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
            TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
            TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
            TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
            TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
            TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
            //       TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
            TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
            //       TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );
            
            
            TLatex lOppo(mUtil->mCepChannelMult[mChannel]==2 ? 0.22 : .236, .635-labelShiftDown + 0.04, mTotalChargeStr_OPPO); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
            TLatex lSame(mUtil->mCepChannelMult[mChannel]==2 ? .256 : .236, .635-labelShiftDown - 0.15 + 0.04, mTotalChargeStr_SAME); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
            
            
            //       TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
            //       TLatex lDeltaPhi(/*.25*/.16,.677 + 0.04 + 0.02, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
            
            drawFinalResult( referenceHist, opts, referenceHist_SameSign );
            
            // back to default
            mRatioFactor = 0.25;
            mRatioYmin = 0.2;
            mRatioYmax = 1.8;
            mRatioYTitleOffsetFactor = 1.0;
            mRatioYTitle = TString("Data / MC");
        }
        
        gStyle->SetHatchesSpacing(hatchesSpacing);
        gStyle->SetHatchesLineWidth(hatchesLineWidth);
    }
    
    
    
    
    
    
    
    
    
    
    //BEGIN                                       MISSING PX                                                        
    
    {
        referenceHist = hMissingPx[DATA][CepUtil::OPPO];
        referenceHist->Scale(referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) ), "width");
        
        totalData = new TH1F( *referenceHist );
        
        referenceHist_SameSign = hMissingPx[DATA][CepUtil::SAME];
        referenceHist_SameSign->Scale(referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) ), "width");
        
        totalData_SameSign = new TH1F( *referenceHist_SameSign );
        
        DrawingOptions opts;
        
        for(unsigned int fileId=DATA+1; fileId<nFiles; ++fileId){
            
            TH1F *mcHist = hMissingPx[fileId][CepUtil::OPPO];
            mcHist->Scale( normalizationFactor[fileId] * mcHist->GetBinWidth( mcHist->FindBin(0.0001) ), "width");
            
            mcHist->SetLineColor( kBlack );
            mcHist->SetLineWidth( 2 );
            mcHist->SetFillColor( histColor[fileId] );
            
            totalMC[fileId] = new TH1F( *mcHist );
            
            opts.mHistTH1F.push_back( mcHist );
            opts.mHistTH1F_DrawingOpt.push_back( "HIST" );
            
            opts.mObjForLegend.push_back( mcHist );
            opts.mDesriptionForLegend.push_back( legendText[fileId] );
            opts.mDrawTypeForLegend.push_back( "f" );
            
            if( fileId==MC_PYTHIA_CD_BKGD_NEUTRALS || fileId==MC_SIGNAL ) continue;
            
            TH1F *mcHist_SameSign = hMissingPx[fileId][CepUtil::SAME];
            mcHist_SameSign->Scale( normalizationFactor[fileId] * mcHist->GetBinWidth( mcHist->FindBin(0.0001) ), "width");
            
            mcHist_SameSign->SetLineColor( histColor2[fileId]/*+2*/ );
            mcHist_SameSign->SetLineWidth( 2 );
            mcHist_SameSign->SetFillColor( histColor2[fileId] );
            mcHist_SameSign->SetFillStyle( 3345 );
            
            totalMC_SameSign[fileId] = new TH1F( *mcHist_SameSign );
            
            opts.mHistTH1F2.push_back( mcHist_SameSign );
            opts.mHistTH1F_DrawingOpt2.push_back( "HIST" );
            
            opts.mObjForLegend2.push_back( /*fileId==MC_SIGNAL ? static_cast<TObject*>(nullptr) :*/ mcHist_SameSign );
            opts.mDesriptionForLegend2.push_back( /*fileId==MC_SIGNAL ? "" :*/ legendText2[fileId] );
            opts.mDrawTypeForLegend2.push_back( /*fileId==MC_SIGNAL ? "" :*/ "f" );
        }
        
        
        
        for(int t=0; t<2; ++t){
            opts.mObjForLegend2.push_back( static_cast<TObject*>(nullptr) );
            opts.mDesriptionForLegend2.push_back(  "" );
            opts.mDrawTypeForLegend2.push_back( "" );
        }
        
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.12;
        opts.mRightMargin = 0.035;
        opts.mTopMargin = 0.06;
        opts.mBottomMargin = 0.12;
        
        //x axis
        opts.mXmin = -1;
        opts.mXmax = 1;
        opts.mXaxisTitle = TString("p_{x}^{miss} [GeV]");
        opts.mXaxisTitleSize = 0.05;
        opts.mXaxisTitleOffset = 1.04;
        opts.mXaxisLabelSize = 0.05;
        opts.mXaxisLabelOffset = 0.007;
        opts.mXaxisTickLength = 0.015;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 1.35 * referenceHist->GetMaximum();
        opts.mYaxisTitle = TString(Form("Number of events / %d MeV", static_cast<int>(1e3*referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) ))));
        opts.mYaxisTitleSize = 0.05;
        opts.mYaxisTitleOffset = 1.25;
        opts.mYaxisLabelSize = 0.05;
        opts.mYaxisLabelOffset = 0.007;
        opts.mYaxisTickLength = 0.015;
        opts.mYlog = kFALSE;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.4;
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mMarkerStyle2 = 20;
        opts.mMarkerColor2 = kRed;
        opts.mMarkerSize2 = 1.4;
        opts.mLineStyle2 = 1;
        opts.mLineColor2 = kRed;
        opts.mLineWidth2 = 2;
        
        opts.mPdfName = TString("MissingPx");
        
        opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        opts.mNColumns = 1;
        opts.mLegendX = true ? 0.15 : 0.4;
        opts.mLegendXwidth = 0.2;
        opts.mLegendY = 0.3;
        opts.mLegendYwidth = 0.14*2.2;
        mSpecialLegendMarginFactor = 1.2;
        
        
        opts.mLegendTextSize2 = 0.7 * opts.mXaxisTitleSize;
        opts.mLegendX2 = opts.mLegendX + 0.5;
        opts.mLegendXwidth2 = opts.mLegendXwidth;
        opts.mLegendY2 = opts.mLegendY;
        opts.mLegendYwidth2 = opts.mLegendYwidth;
        
        
        double labelShiftDown = 0.04;
        
        mInverseStackDrawing = true;
        
        TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.18]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+"+mChannelStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
        TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
        //opts.mTextToDraw.push_back( lATLAS );
        
        TString tpcMinPt; tpcMinPt.Form("p_{T} > %g GeV", minInDetTrackPt );
        TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
        TString rpKinematicRegion1("0.18 GeV < |p_{y}| < 0.46 GeV");
        TLatex lCutsA(.16,.84-labelShiftDown, Form("%s, %s:", singleTrkStr[CepUtil::PLUS].Data(), singleTrkStr[CepUtil::MINUS].Data())); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
        TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
        TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
        TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
        TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
        TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
        //     TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
        TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        //     TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );   
        
        //     TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
        //     TLatex lDeltaPhi(.25,.677, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
        
        drawFinalResult( referenceHist, opts );
        
        TLatex lOppo(mUtil->mCepChannelMult[mChannel]==2 ? 0.18 : .16, .67-labelShiftDown, mTotalChargeStr_OPPO); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
        TLatex lSame(mUtil->mCepChannelMult[mChannel]==2 ? .65 : .65, .67-labelShiftDown, mTotalChargeStr_SAME); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
        
        double hatchesSpacing = gStyle->GetHatchesSpacing();
        double hatchesLineWidth = gStyle->GetHatchesLineWidth();
        gStyle->SetHatchesSpacing(3);
        gStyle->SetHatchesLineWidth(3);
        opts.mPdfName = TString("MissingPx_OppositeAndSameSign");
        drawFinalResult( referenceHist, opts, referenceHist_SameSign );
        
        {
            opts.mTopMargin -= 0.04;
            opts.mLegendY += 0.13;
            opts.mLegendY2 = opts.mLegendY;
            mRatioFactor = 0.15;
            mRatioYmin = 0.62;
            mRatioYmax = 1.38;
            mRatioYTitleOffsetFactor = 0.5;
            mRatioYTitle = TString("#kern[-0.2]{Data/MC}");
            //log-y scale
            opts.mPdfName = TString("LogY_MissingPx_OppositeAndSameSign");
            opts.mYlog = kTRUE;
            opts.mYmin = 200;
            opts.mYmax = 1000 * referenceHist->GetMaximum();
            opts.mObjectToDraw.clear(); 
            
            opts.mTextToDraw.clear();
            
            labelShiftDown -= 0.04;
            
            TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.18]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+"+mChannelStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
            opts.mTextToDraw.push_back( lReaction );
            TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
            //opts.mTextToDraw.push_back( lATLAS );
            
            TString tpcMinPt; tpcMinPt.Form("p_{T} > %g GeV", minInDetTrackPt );
            TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
            TString rpKinematicRegion1("0.18 GeV < |p_{y}| < 0.46 GeV");
            TLatex lCutsA(.16,.84-labelShiftDown, Form("%s, %s:", singleTrkStr[CepUtil::PLUS].Data(), singleTrkStr[CepUtil::MINUS].Data())); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
            TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
            TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
            TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
            TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
            TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
            //       TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
            TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
            //       TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );
            
            
            TLatex lOppo(mUtil->mCepChannelMult[mChannel]==2 ? 0.18 : .22, .67-labelShiftDown + 0.07, mTotalChargeStr_OPPO); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
            TLatex lSame(mUtil->mCepChannelMult[mChannel]==2 ? .65 : .65, .67-labelShiftDown + 0.07, mTotalChargeStr_SAME); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
            
            
            //       TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
            //       TLatex lDeltaPhi(/*.25*/.16,.677 + 0.04 + 0.02, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
            
            drawFinalResult( referenceHist, opts, referenceHist_SameSign );
            
            // back to default
            mRatioFactor = 0.25;
            mRatioYmin = 0.2;
            mRatioYmax = 1.8;
            mRatioYTitleOffsetFactor = 1.0;
            mRatioYTitle = TString("Data / MC");
        }
        
        gStyle->SetHatchesSpacing(hatchesSpacing);
        gStyle->SetHatchesLineWidth(hatchesLineWidth);
    }
    
    
    
    
    
    
    
    //BEGIN                                       MISSING PY                                                        
    
    {
        referenceHist = hMissingPy[DATA][CepUtil::OPPO];
        referenceHist->Scale(referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) ), "width");
        
        totalData = new TH1F( *referenceHist );
        
        referenceHist_SameSign = hMissingPy[DATA][CepUtil::SAME];
        referenceHist_SameSign->Scale(referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) ), "width");
        
        totalData_SameSign = new TH1F( *referenceHist_SameSign );
        
        DrawingOptions opts;
        
        for(unsigned int fileId=DATA+1; fileId<nFiles; ++fileId){
            
            TH1F *mcHist = hMissingPy[fileId][CepUtil::OPPO];
            mcHist->Scale( normalizationFactor[fileId] * mcHist->GetBinWidth( mcHist->FindBin(0.0001) ), "width");
            
            mcHist->SetLineColor( kBlack );
            mcHist->SetLineWidth( 2 );
            mcHist->SetFillColor( histColor[fileId] );
            
            totalMC[fileId] = new TH1F( *mcHist );
            
            opts.mHistTH1F.push_back( mcHist );
            opts.mHistTH1F_DrawingOpt.push_back( "HIST" );
            
            opts.mObjForLegend.push_back( mcHist );
            opts.mDesriptionForLegend.push_back( legendText[fileId] );
            opts.mDrawTypeForLegend.push_back( "f" );
            
            if( fileId==MC_PYTHIA_CD_BKGD_NEUTRALS || fileId==MC_SIGNAL ) continue;
            
            TH1F *mcHist_SameSign = hMissingPy[fileId][CepUtil::SAME];
            mcHist_SameSign->Scale( normalizationFactor[fileId] * mcHist->GetBinWidth( mcHist->FindBin(0.0001) ), "width");
            
            mcHist_SameSign->SetLineColor( histColor2[fileId]/*+2*/ );
            mcHist_SameSign->SetLineWidth( 2 );
            mcHist_SameSign->SetFillColor( histColor2[fileId] );
            mcHist_SameSign->SetFillStyle( 3345 );
            
            totalMC_SameSign[fileId] = new TH1F( *mcHist_SameSign );
            
            opts.mHistTH1F2.push_back( mcHist_SameSign );
            opts.mHistTH1F_DrawingOpt2.push_back( "HIST" );
            
            opts.mObjForLegend2.push_back( /*fileId==MC_SIGNAL ? static_cast<TObject*>(nullptr) :*/ mcHist_SameSign );
            opts.mDesriptionForLegend2.push_back( /*fileId==MC_SIGNAL ? "" :*/ legendText2[fileId] );
            opts.mDrawTypeForLegend2.push_back( /*fileId==MC_SIGNAL ? "" :*/ "f" );
        }
        
        
        
        for(int t=0; t<2; ++t){
            opts.mObjForLegend2.push_back( static_cast<TObject*>(nullptr) );
            opts.mDesriptionForLegend2.push_back(  "" );
            opts.mDrawTypeForLegend2.push_back( "" );
        }
        
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.12;
        opts.mRightMargin = 0.035;
        opts.mTopMargin = 0.06;
        opts.mBottomMargin = 0.12;
        
        //x axis
        opts.mXmin = -1;
        opts.mXmax = 1;
        opts.mXaxisTitle = TString("p_{y}^{miss} [GeV]");
        opts.mXaxisTitleSize = 0.05;
        opts.mXaxisTitleOffset = 1.04;
        opts.mXaxisLabelSize = 0.05;
        opts.mXaxisLabelOffset = 0.007;
        opts.mXaxisTickLength = 0.015;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 1.35 * referenceHist->GetMaximum();
        opts.mYaxisTitle = TString(Form("Number of events / %d MeV", static_cast<int>(1e3*referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) ))));
        opts.mYaxisTitleSize = 0.05;
        opts.mYaxisTitleOffset = 1.25;
        opts.mYaxisLabelSize = 0.05;
        opts.mYaxisLabelOffset = 0.007;
        opts.mYaxisTickLength = 0.015;
        opts.mYlog = kFALSE;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.4;
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mMarkerStyle2 = 20;
        opts.mMarkerColor2 = kRed;
        opts.mMarkerSize2 = 1.4;
        opts.mLineStyle2 = 1;
        opts.mLineColor2 = kRed;
        opts.mLineWidth2 = 2;
        
        opts.mPdfName = TString("MissingPy");
        
        opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        opts.mNColumns = 1;
        opts.mLegendX = true ? 0.2 : 0.4;
        opts.mLegendXwidth = 0.2;
        opts.mLegendY = 0.3;
        opts.mLegendYwidth = 0.14*2.2;
        mSpecialLegendMarginFactor = 1.2;
        
        
        opts.mLegendTextSize2 = 0.7 * opts.mXaxisTitleSize;
        opts.mLegendX2 = opts.mLegendX + 0.45;
        opts.mLegendXwidth2 = opts.mLegendXwidth;
        opts.mLegendY2 = opts.mLegendY;
        opts.mLegendYwidth2 = opts.mLegendYwidth;
        
        
        double labelShiftDown = 0.04;
        
        mInverseStackDrawing = true;
        
        TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.18]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+"+mChannelStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
        TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
        //opts.mTextToDraw.push_back( lATLAS );
        
        TString tpcMinPt; tpcMinPt.Form("p_{T} > %g GeV", minInDetTrackPt );
        TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
        TString rpKinematicRegion1("0.18 GeV < |p_{y}| < 0.46 GeV");
        TLatex lCutsA(.16,.84-labelShiftDown, Form("%s, %s:", singleTrkStr[CepUtil::PLUS].Data(), singleTrkStr[CepUtil::MINUS].Data())); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
        TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
        TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
        TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
        TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
        TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
        //     TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
        TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        //     TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );   
        
        //     TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
        //     TLatex lDeltaPhi(.25,.677, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
        
        drawFinalResult( referenceHist, opts );
        
        TLatex lOppo(mUtil->mCepChannelMult[mChannel]==2 ? 0.18 : .22, .67-labelShiftDown, mTotalChargeStr_OPPO); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
        TLatex lSame(mUtil->mCepChannelMult[mChannel]==2 ? .65 : .65, .67-labelShiftDown, mTotalChargeStr_SAME); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
        
        double hatchesSpacing = gStyle->GetHatchesSpacing();
        double hatchesLineWidth = gStyle->GetHatchesLineWidth();
        gStyle->SetHatchesSpacing(3);
        gStyle->SetHatchesLineWidth(3);
        opts.mPdfName = TString("MissingPy_OppositeAndSameSign");
        drawFinalResult( referenceHist, opts, referenceHist_SameSign );
        
        {
            opts.mTopMargin -= 0.04;
            opts.mLegendY += 0.13;
            opts.mLegendY2 = opts.mLegendY;
            mRatioFactor = 0.15;
            mRatioYmin = 0.62;
            mRatioYmax = 1.38;
            mRatioYTitleOffsetFactor = 0.5;
            mRatioYTitle = TString("#kern[-0.2]{Data/MC}");
            //log-y scale
            opts.mPdfName = TString("LogY_MissingPy_OppositeAndSameSign");
            opts.mYlog = kTRUE;
            opts.mYmin = 200;
            opts.mYmax = 1000 * referenceHist->GetMaximum();
            opts.mObjectToDraw.clear(); 
            
            opts.mTextToDraw.clear();
            
            labelShiftDown -= 0.04;
            
            TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.18]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+"+mChannelStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
            opts.mTextToDraw.push_back( lReaction );
            TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
            //opts.mTextToDraw.push_back( lATLAS );
            
            TString tpcMinPt; tpcMinPt.Form("p_{T} > %g GeV", minInDetTrackPt );
            TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
            TString rpKinematicRegion1("0.18 GeV < |p_{y}| < 0.46 GeV");
            TLatex lCutsA(.16,.84-labelShiftDown, Form("%s, %s:", singleTrkStr[CepUtil::PLUS].Data(), singleTrkStr[CepUtil::MINUS].Data())); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
            TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
            TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
            TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
            TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
            TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
            //       TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
            TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
            //       TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );
            
            
            TLatex lOppo(mUtil->mCepChannelMult[mChannel]==2 ? 0.18 : .22, .67-labelShiftDown + 0.07, mTotalChargeStr_OPPO); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
            TLatex lSame(mUtil->mCepChannelMult[mChannel]==2 ? .65 : .65, .67-labelShiftDown + 0.07, mTotalChargeStr_SAME); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
            
            
            //       TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
            //       TLatex lDeltaPhi(/*.25*/.16,.677 + 0.04 + 0.02, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
            
            drawFinalResult( referenceHist, opts, referenceHist_SameSign );
            
            // back to default
            mRatioFactor = 0.25;
            mRatioYmin = 0.2;
            mRatioYmax = 1.8;
            mRatioYTitleOffsetFactor = 1.0;
            mRatioYTitle = TString("Data / MC");
        }
        
        gStyle->SetHatchesSpacing(hatchesSpacing);
        gStyle->SetHatchesLineWidth(hatchesLineWidth);
    }
    
    
    
    
    
    
    
    
    
    
    
    //BEGIN                                     X and Y hit position                                                   
    
    //-----------------------------------------------------------------------------
    // ALFA Vertical Acceptance //ALERT code copied from ALFAAlignment.cxx (and further modified to adopt for lacking methods etc.)- make sure it hasn't changed in the origin
    double acceptVerRangeArr[CepUtil::nRomanPots][2] = {
            {  5.9539, 19.6486},
            {-19.4184, -6.2280},
            {  6.6483, 21.7046},
            {-21.5306, -6.9069},
            {  6.5348, 21.3633},
            {-21.4923, -6.9514},
            {  5.9063, 19.3318},
            {-19.3573, -6.2141}
    };

    for(unsigned int i = 0; i<CepUtil::nRomanPots; ++i){
        double lowCorrection = static_cast<bool>(i % 2) ? +1.5 : +0.5;
        double uppCorrection = static_cast<bool>(i % 2) ? -0.5 : -1.5;
        acceptVerRangeArr[i][0] += lowCorrection;
        acceptVerRangeArr[i][1] += uppCorrection;
    }
    //-----------------------------------------------------------------------------
    
    
    for(int rp=0; rp<CepUtil::nRomanPots; ++rp){
        
        bool isALFAnear = !static_cast<bool>(rp < 2 || rp > 5);
        
        for(int coor=0; coor<CepUtil::nCoordinates; ++coor){
            
            if(coor == CepUtil::X)
                referenceHist = CepUtil::TH1F_from_TH1( hHitMap[DATA][rp]->ProjectionX( Form("%s_xyproj_%d_%d", hHitMap[DATA][rp]->GetName(), rp, coor) ) );
            else
                referenceHist = CepUtil::TH1F_from_TH1( hHitMap[DATA][rp]->ProjectionY( Form("%s_xyproj_%d_%d", hHitMap[DATA][rp]->GetName(), rp, coor) ) );
            //       referenceHist->Rebin(coor==CepUtil::X ? 4 : 10);
            referenceHist->Scale( normalizationFactor[DATA] );
            
            DrawingOptions opts;
            
            for(unsigned int fileId=DATA+1; fileId<nFiles; ++fileId){
                
                TH1F *mcHist;
                if(coor == CepUtil::X)
                    mcHist = CepUtil::TH1F_from_TH1( hHitMap[fileId][rp]->ProjectionX( Form("%s_xyproj_%d_%d", hHitMap[fileId][rp]->GetName(), rp, coor) ) );
                else
                    mcHist = CepUtil::TH1F_from_TH1( hHitMap[fileId][rp]->ProjectionY( Form("%s_xyproj_%d_%d", hHitMap[fileId][rp]->GetName(), rp, coor) ) );
                //         mcHist->Rebin(coor==CepUtil::X ? 4 : 10);
                mcHist->Scale( normalizationFactor[fileId] );
                //         mcHist->Scale( mcHist->GetBinWidth( mcHist->FindBin(0.) ), "width");
                
                mcHist->SetLineColor( kBlack );
                mcHist->SetLineWidth( 2 );
                mcHist->SetFillColor( histColor[fileId] );
                
                opts.mHistTH1F.push_back( mcHist );
                opts.mHistTH1F_DrawingOpt.push_back( "HIST" );
                
                opts.mObjForLegend.push_back( mcHist );
                opts.mDesriptionForLegend.push_back( legendText[fileId] );
                opts.mDrawTypeForLegend.push_back( "f" );
            }
            
            opts.mMaxDigits = 3;
            
            opts.mCanvasWidth = 900;
            opts.mCanvasHeight = 800;
            
            opts.mLeftMargin = 0.17;
            opts.mRightMargin = 0.025;
            opts.mTopMargin = 0.075;
            opts.mBottomMargin = 0.14;
            
            //x axis
            opts.mXmin = coor==CepUtil::X ? -8 : ((rp==0 || rp==2 || rp==4 || rp==6) ? 0 : -25);
            opts.mXmax = coor==CepUtil::X ? 8 : ((rp==0 || rp==2 || rp==4 || rp==6) ? 25 : 0);
            opts.mXaxisTitle = TString(coor==CepUtil::X ? "x [mm]" : "y [mm]");
            opts.mXaxisTitleSize = 0.07;
            opts.mXaxisTitleOffset = 0.97;
            opts.mXaxisLabelSize = 0.07;
            opts.mXaxisLabelOffset = 0.007;
            opts.mXaxisTickLength = 0.015;
            
            //y axis
            opts.mYmin = 5;
            opts.mYmax = 10000 * referenceHist->GetMaximum();
            opts.mYaxisTitle = TString(Form("N. of track points / %g mm#kern[-4.0]{ }",  referenceHist->GetBinWidth( referenceHist->FindBin(0.) )  ));
            opts.mYaxisTitleSize = 0.07;
            opts.mYaxisTitleOffset = 1.145;
            opts.mYaxisLabelSize = 0.07;
            opts.mYaxisLabelOffset = 0.01;
            opts.mYaxisTickLength = 0.015;
            opts.mYlog = kTRUE;
            
            opts.mMarkerStyle = 20;
            opts.mMarkerColor = kBlack;
            opts.mMarkerSize = 1.4;
            
            opts.mLineStyle = 1;
            opts.mLineColor = kBlack;
            opts.mLineWidth = 2;
            
            opts.mPdfName = TString(Form("%s_%d", TString(coor==CepUtil::X ? "x" : "y").Data(), rp));
            
            opts.mLegendTextSize = 0.7 * 0.05/*opts.mXaxisTitleSize*/;
            
            if( coor == CepUtil::X ){
                opts.mNColumns = 1;
                opts.mLegendX = 0.21;
                opts.mLegendXwidth = 0.25;
                opts.mLegendY = 0.24;
                opts.mLegendYwidth = 0.4;
                mSpecialLegendMarginFactor = 1.0;
            } else{
                opts.mNColumns = 1;
                opts.mLegendX = (rp==0 || rp==2 || rp==4 || rp==6) ? 0.73 : 0.19;
                opts.mLegendXwidth = 0.25*0.8;
                opts.mLegendY = 0.4;
                opts.mLegendYwidth = 0.4*0.6;
                mSpecialLegendMarginFactor = 1.0;
            }
            
            
            
            if( isALFAnear && coor==CepUtil::Y ){
                for(int tt=0; tt<2; ++tt){
                    TLine *cutLine = new TLine( acceptVerRangeArr[rp][tt], 0, acceptVerRangeArr[rp][tt], 0.4*referenceHist->GetMaximum() );
                    cutLine->SetLineWidth(4);
                    cutLine->SetLineColor(kRed);
                    cutLine->SetLineStyle(7);
                    cutLine->Draw();
                    opts.mObjectToDraw.push_back(cutLine);
                    
                    TArrow *cutArrow = new TArrow( acceptVerRangeArr[rp][tt] + (tt==0 ? 0 : -2), 0.4*referenceHist->GetMaximum(), acceptVerRangeArr[rp][tt] + (tt==0 ? 2 : 0), 0.4*referenceHist->GetMaximum(), 0.035, tt==0?"|>":"<|");
                    cutArrow->SetAngle(30);
                    cutArrow->SetLineWidth(4);
                    cutArrow->SetLineColor( kRed );
                    cutArrow->SetFillColor( kRed );
                    opts.mObjectToDraw.push_back(cutArrow);   
                }
            }
            
            
            mInverseStackDrawing = true;
            
            const double labelShiftDown = 0.04;
            double offs = 0.06;
            
            TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.18]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+"+mChannelStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.7 * opts.mXaxisTitleSize);
            opts.mTextToDraw.push_back( lReaction );
            //     TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
            //opts.mTextToDraw.push_back( lATLAS );
            
            TString tpcMinPt; tpcMinPt.Form("p_{T} > %g GeV", minInDetTrackPt );
            TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
            TString rpKinematicRegion1("0.18 GeV < |p_{y}| < 0.46 GeV");
            TLatex lCutsA(.16+offs,.84-labelShiftDown, Form("%s, %s:", singleTrkStr[CepUtil::PLUS].Data(), singleTrkStr[CepUtil::MINUS].Data())); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
            TLatex lCutsB(.44+offs,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
            TLatex lCuts1(.26+offs,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
            TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
            TLatex lCuts12(.26+offs,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
            TLatex lCuts11(.26+offs,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
            //     TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
            TLatex lCuts3(.54+offs,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
            //     TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );   
            
            //       
            
            TLatex lRpStr( (coor==CepUtil::Y) ? ((rp==0 || rp==2 || rp==4 || rp==6) ? 0.23 : 0.8 ) : 0.78 , .46, Form("RP %d", rp)); lRpStr.SetNDC(); lRpStr.SetTextFont(42);  lRpStr.SetTextSize(1.5 * 0.05); opts.mTextToDraw.push_back( lRpStr );
            
            drawFinalResult( referenceHist, opts );
            
            
            //linear y scale
            opts.mPdfName = TString(Form("Linear_%s_%d", TString(coor==CepUtil::X ? "x" : "y").Data(), rp));
            opts.mYlog = kFALSE;
            opts.mYmin = 0.0;
            opts.mYmax = 1.65 * referenceHist->GetMaximum();
            
            //       mRatioYTitleOffsetFactor = 0.82;
            
            drawFinalResult( referenceHist, opts );
        }
    }
    
    mRatioYTitleOffsetFactor = 1.0;
    
    




    //BEGIN                                     p_{x} and p_{y}  of proton track                                                   
    
    for(int side=0; side<CepUtil::nSides; ++side){
        
        for(int coor=0; coor<CepUtil::nCoordinates; ++coor){
            
            if(coor == CepUtil::X)
                referenceHist = CepUtil::TH1F_from_TH1( hPxPy[DATA][side]->ProjectionX( Form("%s_xyproj_%d_%d", hPxPy[DATA][side]->GetName(), side, coor) ) );
            else
                referenceHist = CepUtil::TH1F_from_TH1( hPxPy[DATA][side]->ProjectionY( Form("%s_xyproj_%d_%d", hPxPy[DATA][side]->GetName(), side, coor) ) );
            referenceHist->Rebin(coor==CepUtil::X ? 5 : 1);
            referenceHist->Scale( normalizationFactor[DATA] );
            
            DrawingOptions opts;
            
            for(unsigned int fileId=DATA+1; fileId<nFiles; ++fileId){
                
                TH1F *mcHist;
                if(coor == CepUtil::X)
                    mcHist = CepUtil::TH1F_from_TH1( hPxPy[fileId][side]->ProjectionX( Form("%s_xyproj_%d_%d", hPxPy[fileId][side]->GetName(), side, coor) ) );
                else
                    mcHist = CepUtil::TH1F_from_TH1( hPxPy[fileId][side]->ProjectionY( Form("%s_xyproj_%d_%d", hPxPy[fileId][side]->GetName(), side, coor) ) );
                mcHist->Rebin(coor==CepUtil::X ? 5 : 1);
                mcHist->Scale( normalizationFactor[fileId] );
                //         mcHist->Scale( mcHist->GetBinWidth( mcHist->FindBin(0.) ), "width");
                
                mcHist->SetLineColor( kBlack );
                mcHist->SetLineWidth( 2 );
                mcHist->SetFillColor( histColor[fileId] );
                
                opts.mHistTH1F.push_back( mcHist );
                opts.mHistTH1F_DrawingOpt.push_back( "HIST" );
                
                opts.mObjForLegend.push_back( mcHist );
                opts.mDesriptionForLegend.push_back( legendText[fileId] );
                opts.mDrawTypeForLegend.push_back( "f" );
            }
            
            opts.mMaxDigits = 3;
            
            opts.mCanvasWidth = 900;
            opts.mCanvasHeight = 800;
            
            opts.mLeftMargin = 0.17;
            opts.mRightMargin = 0.025;
            opts.mTopMargin = 0.075;
            opts.mBottomMargin = 0.14;
            
            //x axis
            opts.mXmin = coor==CepUtil::X ? -1 : -0.5;
            opts.mXmax = coor==CepUtil::X ? 1 : 0.5;
            opts.mXaxisTitle = TString(coor==CepUtil::X ? "p_{x} [GeV]" : "p_{y} [GeV]");
            opts.mXaxisTitleSize = 0.07;
            opts.mXaxisTitleOffset = 0.87;
            opts.mXaxisLabelSize = 0.05;
            opts.mXaxisLabelOffset = 0.007;
            opts.mXaxisTickLength = 0.015;
            
            opts.mSetDivisionsX = kTRUE;
            opts.mXnDivisionA = 5;
            opts.mXnDivisionB = 5;
            opts.mXnDivisionC = 1;
            
            //y axis
            opts.mYmin = 5;
            opts.mYmax = 10000 * referenceHist->GetMaximum();
            opts.mYaxisTitle = TString(Form("N. of tracks / %d MeV#kern[-2.0]{ }",  static_cast<int>(std::round(1e3*referenceHist->GetBinWidth( referenceHist->FindBin(0.) )))  ));
            opts.mYaxisTitleSize = 0.07;
            opts.mYaxisTitleOffset = 1.145;
            opts.mYaxisLabelSize = 0.05;
            opts.mYaxisLabelOffset = 0.01;
            opts.mYaxisTickLength = 0.015;
            opts.mYlog = kTRUE;
            
            opts.mMarkerStyle = 20;
            opts.mMarkerColor = kBlack;
            opts.mMarkerSize = 1.4;
            
            opts.mLineStyle = 1;
            opts.mLineColor = kBlack;
            opts.mLineWidth = 2;
            
            opts.mPdfName = TString(Form("%s_%s", TString(coor==CepUtil::X ? "p_x" : "p_y").Data(), side==CepUtil::A ? "A" : "C"));
            
            opts.mLegendTextSize = 0.7 * 0.05/*opts.mXaxisTitleSize*/;
            
            
            opts.mNColumns = 1;
            opts.mLegendX = coor==CepUtil::X ? 0.19 : 0.45;
            opts.mLegendXwidth = 0.25*0.8;
            opts.mLegendY = 0.4;
            opts.mLegendYwidth = 0.4*0.6;
            mSpecialLegendMarginFactor = 1.0;
            
            
            
            if( coor==CepUtil::Y ){
                for(int rr=0; rr<2; ++rr){
                    for(int tt=0; tt<2; ++tt){
                        TLine *cutLine = new TLine( (rr==0?1:-1)*(tt==0?0.18:0.46), 0, (rr==0?1:-1)*(tt==0?0.18:0.46), 1.1*referenceHist->GetMaximum() );
                        cutLine->SetLineWidth(4);
                        cutLine->SetLineColor(kRed);
                        cutLine->SetLineStyle(7);
                        cutLine->Draw();
                        opts.mObjectToDraw.push_back(cutLine);
                        
                        TArrow *cutArrow = new TArrow( (rr==0?1:-1)*(tt==0?0.18:0.46) + (tt==rr ? 0 : -0.05), 1.1*referenceHist->GetMaximum(), (rr==0?1:-1)*(tt==0?0.18:0.46) + (tt==rr ? 0.05 : 0), 1.1*referenceHist->GetMaximum(), 0.035, tt==rr?"|>":"<|");
                        cutArrow->SetAngle(30);
                        cutArrow->SetLineWidth(4);
                        cutArrow->SetLineColor( kRed );
                        cutArrow->SetFillColor( kRed );
                        opts.mObjectToDraw.push_back(cutArrow);   
                    }
                }
            }
            
            
            mInverseStackDrawing = true;
            
            const double labelShiftDown = 0.04;
            double offs = 0.06;
            
            TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.18]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+"+mChannelStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.7 * opts.mXaxisTitleSize);
            opts.mTextToDraw.push_back( lReaction );
            //     TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
            //opts.mTextToDraw.push_back( lATLAS );
            
            TString tpcMinPt; tpcMinPt.Form("p_{T} > %g GeV", minInDetTrackPt );
            TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
            TString sideKinematicRegion1("0.18 GeV < |p_{y}| < 0.46 GeV");
            TLatex lCutsA(.16+offs,.84-labelShiftDown, Form("%s, %s:", singleTrkStr[CepUtil::PLUS].Data(), singleTrkStr[CepUtil::MINUS].Data())); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
            TLatex lCutsB(.44+offs,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
            TLatex lCuts1(.26+offs,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
            TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
            TLatex lCuts12(.26+offs,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
            TLatex lCuts11(.26+offs,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
            //     TLatex lCuts2(.54,.84-labelShiftDown, sideKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
            TLatex lCuts3(.54+offs,.84-labelShiftDown, sideKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
            //     TLatex lCuts4(.54,.75-labelShiftDown, sideKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );   
            
            //       
            
            TLatex lSideStr( 0.78 , .94, Form("Side %s", side==CepUtil::A ? "A" : "C")); lSideStr.SetNDC(); lSideStr.SetTextFont(42);  lSideStr.SetTextSize(1.5 * 0.05); opts.mTextToDraw.push_back( lSideStr );
            
            drawFinalResult( referenceHist, opts );
            
            
            //linear y scale
            opts.mPdfName = TString(Form("Linear_%s_%s", TString(coor==CepUtil::X ? "p_x" : "p_y").Data(), side==CepUtil::A ? "A" : "C"));
            opts.mYlog = kFALSE;
            opts.mYmin = 0.0;
            opts.mYmax = 1.65 * referenceHist->GetMaximum();
            
            //       mRatioYTitleOffsetFactor = 0.82;
            
            drawFinalResult( referenceHist, opts );
        }
    }
    
    mRatioYTitleOffsetFactor = 1.0;

    
    
    
    
    
    
    //BEGIN                                     U and V fiber multiplicity                                                   
   
    for(int rp=0; rp<CepUtil::nRomanPots; ++rp){
        
        for(int coor=0; coor<CepUtil::nCoordinates; ++coor){
            
            if(coor == CepUtil::X)
                referenceHist = CepUtil::TH1F_from_TH1( hFiberMult[DATA][rp]->ProjectionX( Form("%s_xyproj_%d_%d", hFiberMult[DATA][rp]->GetName(), rp, coor) ) );
            else
                referenceHist = CepUtil::TH1F_from_TH1( hFiberMult[DATA][rp]->ProjectionY( Form("%s_xyproj_%d_%d", hFiberMult[DATA][rp]->GetName(), rp, coor) ) );
            //       referenceHist->Rebin(coor==CepUtil::X ? 4 : 10);
            referenceHist->Scale( normalizationFactor[DATA] );
            
            DrawingOptions opts;
            
            for(unsigned int fileId=DATA+1; fileId<nFiles; ++fileId){
                
                TH1F *mcHist;
                if(coor == CepUtil::X)
                    mcHist = CepUtil::TH1F_from_TH1( hFiberMult[fileId][rp]->ProjectionX( Form("%s_xyproj_%d_%d", hFiberMult[fileId][rp]->GetName(), rp, coor) ) );
                else
                    mcHist = CepUtil::TH1F_from_TH1( hFiberMult[fileId][rp]->ProjectionY( Form("%s_xyproj_%d_%d", hFiberMult[fileId][rp]->GetName(), rp, coor) ) );
                //         mcHist->Rebin(coor==CepUtil::X ? 4 : 10);
                mcHist->Scale( normalizationFactor[fileId] );
                //         mcHist->Scale( mcHist->GetBinWidth( mcHist->FindBin(0.) ), "width");
                
                mcHist->SetLineColor( kBlack );
                mcHist->SetLineWidth( 2 );
                mcHist->SetFillColor( histColor[fileId] );
                
                opts.mHistTH1F.push_back( mcHist );
                opts.mHistTH1F_DrawingOpt.push_back( "HIST" );
                
                opts.mObjForLegend.push_back( mcHist );
                opts.mDesriptionForLegend.push_back( legendText[fileId] );
                opts.mDrawTypeForLegend.push_back( "f" );
            }
            
            opts.mMaxDigits = 3;
            
            opts.mCanvasWidth = 900;
            opts.mCanvasHeight = 800;
            
            opts.mLeftMargin = 0.17;
            opts.mRightMargin = 0.025;
            opts.mTopMargin = 0.075;
            opts.mBottomMargin = 0.14;
            
            //x axis
            opts.mXmin = 5.5;
            opts.mXmax = 10.5;
            opts.mXaxisTitle = TString(coor==CepUtil::X ? "Fiber multiplicity in U" : "Fiber multiplicity in V");
            opts.mXaxisTitleSize = 0.07;
            opts.mXaxisTitleOffset = 0.97;
            opts.mXaxisLabelSize = 0.07;
            opts.mXaxisLabelOffset = 0.007;
            opts.mXaxisTickLength = 0.015;
            
            opts.mSetDivisionsX = kTRUE;
            opts.mXnDivisionA = 5;
            opts.mXnDivisionB = 1;
            opts.mXnDivisionC = 1;
            
            //y axis
            opts.mYmin = 5;
            opts.mYmax = 10000 * referenceHist->GetMaximum();
            opts.mYaxisTitle = TString("N. of track points #kern[-4.0]{ }");
            opts.mYaxisTitleSize = 0.07;
            opts.mYaxisTitleOffset = 1.2;
            opts.mYaxisLabelSize = 0.07;
            opts.mYaxisLabelOffset = 0.01;
            opts.mYaxisTickLength = 0.015;
            opts.mYlog = kTRUE;
            
            opts.mMarkerStyle = 20;
            opts.mMarkerColor = kBlack;
            opts.mMarkerSize = 1.4;
            
            opts.mLineStyle = 1;
            opts.mLineColor = kBlack;
            opts.mLineWidth = 2;
            
            opts.mPdfName = TString(Form("%s_%d", TString(coor==CepUtil::X ? "fiberMult_u" : "fiberMult_v").Data(), rp));
            
            opts.mLegendTextSize = 0.7 * 0.05/*opts.mXaxisTitleSize*/;

            opts.mNColumns = 1;
            opts.mLegendX = 0.19;
            opts.mLegendXwidth = 0.25*0.8;
            opts.mLegendY = 0.46;
            opts.mLegendYwidth = 0.4*0.6;
            mSpecialLegendMarginFactor = 1.0;
            
            mInverseStackDrawing = true;
            
            const double labelShiftDown = 0.04;
            double offs = 0.06;
            
            TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.18]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+"+mChannelStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.7 * opts.mXaxisTitleSize);
            opts.mTextToDraw.push_back( lReaction );
            //     TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
            //opts.mTextToDraw.push_back( lATLAS );
            
            TString tpcMinPt; tpcMinPt.Form("p_{T} > %g GeV", minInDetTrackPt );
            TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
            TString rpKinematicRegion1("0.18 GeV < |p_{y}| < 0.46 GeV");
            TLatex lCutsA(.16+offs,.84-labelShiftDown, Form("%s, %s:", singleTrkStr[CepUtil::PLUS].Data(), singleTrkStr[CepUtil::MINUS].Data())); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
            TLatex lCutsB(.44+offs,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
            TLatex lCuts1(.26+offs,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
            TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
            TLatex lCuts12(.26+offs,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
            TLatex lCuts11(.26+offs,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
            //     TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
            TLatex lCuts3(.54+offs,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
            //     TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.5 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );   
            
            //       
            
            TLatex lRpStr( 0.78 , .66, Form("RP %d", rp)); lRpStr.SetNDC(); lRpStr.SetTextFont(42);  lRpStr.SetTextSize(1.5 * 0.05); opts.mTextToDraw.push_back( lRpStr );
            
            drawFinalResult( referenceHist, opts );
            
            
            //linear y scale
            opts.mPdfName = TString(Form("Linear_%s_%d", TString(coor==CepUtil::X ? "fiberMult_u" : "fiberMult_v").Data(), rp));
            opts.mYlog = kFALSE;
            opts.mYmin = 0.0;
            opts.mYmax = 1.65 * referenceHist->GetMaximum();
            
            //       mRatioYTitleOffsetFactor = 0.82;
            
            drawFinalResult( referenceHist, opts );
        }
    }
    
    mRatioYTitleOffsetFactor = 1.0;
    
    
    
    
    
    
    
    
    
    gStyle->SetPalette(69);
    TColor::InvertPalette();
    {
    //BEGIN                                     Y vs. X hit map (2D)                                                   
    
    TH2F *referenceHist2D[CepUtil::nRomanPots] = {nullptr,nullptr,nullptr,nullptr,nullptr,nullptr,nullptr,nullptr};
    TH2F *mcHist2D[CepUtil::nRomanPots] = {nullptr,nullptr,nullptr,nullptr,nullptr,nullptr,nullptr,nullptr};
    
    for(int rp=0; rp<CepUtil::nRomanPots; ++rp){
        
        referenceHist2D[rp] = new TH2F( *hHitMap[DATA][rp] );
        
        for(unsigned int fileId=DATA+1; fileId<nFiles; ++fileId){
            TH2F *mcHist2D_single = new TH2F( *hHitMap[fileId][rp] );
            mcHist2D_single->Scale( normalizationFactor[fileId] );
            if(!mcHist2D[rp])
                mcHist2D[rp] = new TH2F( *mcHist2D_single );
            else
                mcHist2D[rp]->Add( mcHist2D_single );
        }
        
    }
    
    
    TCanvas *c = new TCanvas("c", "c", 800, 800);
    c->SetBottomMargin(0);
    c->SetTopMargin(0);
    c->SetLeftMargin(0);
    c->SetRightMargin(0);
    
    c->Divide(2);
    
    c->Print(mDirName+"HitMap.pdf[");
    
    TGaxis::SetMaxDigits(4);
    
    for(int rp=0; rp<CepUtil::nRomanPots; rp+=2){
        
        bool isALFAnear = !static_cast<bool>(rp < 2 || rp > 5);
        
        referenceHist2D[rp]->Rebin2D(2,2);
        referenceHist2D[rp+1]->Rebin2D(2,2);
        mcHist2D[rp]->Rebin2D(2,2);
        mcHist2D[rp+1]->Rebin2D(2,2);
        
        const double labelsize = 0.07;
        
        double zAxisMax = max( referenceHist2D[rp]->GetMaximum(),  mcHist2D[rp]->GetMaximum() );
        
        TH2F *histToDraw = referenceHist2D[rp];
        TH2F *histToDraw2 = referenceHist2D[rp+1];
        
        c->cd(1);
        gPad->SetBottomMargin(0.08);
        gPad->SetTopMargin(0.26);
        gPad->SetLeftMargin(0.19);
        gPad->SetRightMargin(0.0);
        
        histToDraw->GetXaxis()->SetLabelSize(labelsize );
        histToDraw->GetXaxis()->SetTitleSize( labelsize);
        histToDraw->GetYaxis()->SetLabelSize( labelsize);
        histToDraw->GetYaxis()->SetTitleSize(labelsize );
        histToDraw->GetZaxis()->SetLabelSize( labelsize );
        histToDraw->GetZaxis()->SetTitleSize( labelsize );
        histToDraw->GetXaxis()->SetTickLength( 0.5 * histToDraw->GetXaxis()->GetTickLength() );
        histToDraw->GetXaxis()->SetNdivisions(18,4,1);
        histToDraw->GetXaxis()->SetLabelOffset( -0.02 );
        histToDraw->GetYaxis()->SetLabelOffset( 0.02 );
        
        histToDraw->GetXaxis()->SetRangeUser(-7.5, 7.5);
        histToDraw->GetXaxis()->SetTitle("x [mm]");
        histToDraw->GetYaxis()->SetRangeUser(-80, 80);
        histToDraw->GetYaxis()->SetTitle("y [mm]");
        histToDraw->GetYaxis()->SetTitleOffset(1.35);
        histToDraw->GetXaxis()->SetTitleOffset(0.6);
        histToDraw->GetZaxis()->SetTitle("");
        histToDraw->Draw("col");
        histToDraw2->Draw("col same");
        
        histToDraw->GetZaxis()->SetRangeUser(0, zAxisMax);
        histToDraw2->GetZaxis()->SetRangeUser(0, zAxisMax);
        
        TLatex lRpStr( 0.25, .38, Form("%s %s, DATA", rp<4 ? "A" : "C", (rp==2 || rp==4) ? "near" : "far" ) ); lRpStr.SetNDC(); lRpStr.SetTextFont(42); lRpStr.SetTextSize(2.0 * 0.05); lRpStr.Draw();
        lRpStr.DrawLatex( 0.73, 0.6, Form("RP %d", rp));
        lRpStr.DrawLatex( 0.73, 0.17, Form("RP %d", rp+1));
        
        
        if( isALFAnear ){
            for(int rr=0; rr<2; ++rr){
                for(int tt=0; tt<2; ++tt){
                        TLine *cutLine = new TLine( -6, acceptVerRangeArr[rp+rr][tt], 6, acceptVerRangeArr[rp+rr][tt] );
                        cutLine->SetLineWidth(4);
                        cutLine->SetLineColor(kRed);
                        cutLine->SetLineStyle(7);
                        cutLine->Draw();
                        
                        TArrow *cutArrow = new TArrow( -5, acceptVerRangeArr[rp+rr][tt] + (tt==0 ? 0 : -2), -5, acceptVerRangeArr[rp+rr][tt] + (tt==0 ? 2 : 0), 0.025, tt==0?"|>":"<|");
                        cutArrow->SetAngle(30);
                        cutArrow->SetLineWidth(4);
                        cutArrow->SetLineColor( kRed );
                        cutArrow->SetFillColor( kRed );
                        cutArrow->Draw();   
                }
            }
        }
        
        gPad->RedrawAxis();
        //---
        
        
        histToDraw = mcHist2D[rp];
        histToDraw2 = mcHist2D[rp+1];
        
        
        c->cd(2);
        gPad->SetBottomMargin(0.08);
        gPad->SetTopMargin(0.26);
        gPad->SetLeftMargin(0.0);
        gPad->SetRightMargin(0.23);
        
        histToDraw->GetXaxis()->SetLabelSize( labelsize );
        histToDraw->GetXaxis()->SetTitleSize( labelsize);
        histToDraw->GetYaxis()->SetLabelSize( labelsize);
        histToDraw->GetYaxis()->SetTitleSize(labelsize );
        histToDraw->GetZaxis()->SetLabelSize( labelsize );
        histToDraw->GetZaxis()->SetTitleSize( labelsize );
        histToDraw->GetXaxis()->SetTickLength( 0.5 * histToDraw->GetXaxis()->GetTickLength() );
        histToDraw->GetXaxis()->SetNdivisions(18,4,1);
        histToDraw->GetXaxis()->SetLabelOffset( -0.02 );
        histToDraw->GetYaxis()->SetLabelOffset( 0.01 );
        
        histToDraw->GetXaxis()->SetRangeUser(-7.5, 7.5);
        histToDraw->GetXaxis()->SetTitle("x [mm]");
        histToDraw->GetYaxis()->SetRangeUser(-80, 80);
        histToDraw->GetYaxis()->SetTitle("");
        histToDraw->GetYaxis()->SetLabelSize(0);
        histToDraw->GetXaxis()->SetTitleOffset(0.6);
        TString xWidth(Form("%.g", (histToDraw->GetXaxis()->GetBinWidth(1)) ) );
        TString yWidth(Form("%.g", (histToDraw->GetYaxis()->GetBinWidth(1)) ) );
        histToDraw->GetZaxis()->SetTitle("Track points / ("+xWidth+" mm #times "+yWidth+" mm)");
        histToDraw->GetZaxis()->SetTitleOffset(1.6);
        histToDraw->GetZaxis()->SetLabelOffset(0.02);
        histToDraw->Draw("colz");
        histToDraw2->Draw("col same");
        
        histToDraw->GetZaxis()->SetRangeUser(0, zAxisMax);
        histToDraw2->GetZaxis()->SetRangeUser(0, zAxisMax);
        
        gPad->Update();
        TPaletteAxis* palette = (TPaletteAxis*)histToDraw->GetListOfFunctions()->FindObject("palette");
        if(palette){
            palette->SetX1NDC(0.825-0.05);
            palette->SetX2NDC(0.85-0.05);
        }
        gPad->Modified();
        
        TLatex lRpStr2( 0.2, .38, Form("%s %s, MC", rp<4 ? "A" : "C", (rp==2 || rp==4) ? "near" : "far" ) ); lRpStr2.SetNDC(); lRpStr2.SetTextFont(42); lRpStr2.SetTextSize(2.0 * 0.05); lRpStr2.Draw();
        lRpStr2.DrawLatex( 0.5, 0.6, Form("RP %d", rp));
        lRpStr2.DrawLatex( 0.5, 0.17, Form("RP %d", rp+1));
        
        if( isALFAnear ){
            for(int rr=0; rr<2; ++rr){
                for(int tt=0; tt<2; ++tt){
                        TLine *cutLine = new TLine( -6, acceptVerRangeArr[rp+rr][tt], 6, acceptVerRangeArr[rp+rr][tt] );
                        cutLine->SetLineWidth(4);
                        cutLine->SetLineColor(kRed);
                        cutLine->SetLineStyle(7);
                        cutLine->Draw();
                        
                        TArrow *cutArrow = new TArrow( -5, acceptVerRangeArr[rp+rr][tt] + (tt==0 ? 0 : -2), -5, acceptVerRangeArr[rp+rr][tt] + (tt==0 ? 2 : 0), 0.025, tt==0?"|>":"<|");
                        cutArrow->SetAngle(30);
                        cutArrow->SetLineWidth(4);
                        cutArrow->SetLineColor( kRed );
                        cutArrow->SetFillColor( kRed );
                        cutArrow->Draw();   
                }
            }
        }
        
        const double labelShiftDown = -0.02;
        double offs = -0.04;
        
        c->cd();
        
        TLatex lReaction(.22, .91-labelShiftDown, "#kern[-0.18]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+"+mChannelStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.7 * labelsize);
        lReaction.Draw();
        //     TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
        //lATLAS.Draw();
        
        TString tpcMinPt; tpcMinPt.Form("p_{T} > %g GeV", minInDetTrackPt );
        TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
        TString rpKinematicRegion1("0.18 GeV < |p_{y}| < 0.46 GeV");
        TLatex lCutsA(.16+offs,.84-labelShiftDown, Form("%s, %s:", singleTrkStr[CepUtil::PLUS].Data(), singleTrkStr[CepUtil::MINUS].Data())); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.5 * labelsize); lCutsA.Draw();
        TLatex lCutsB(.44+offs,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.5 * labelsize); lCutsB.Draw();
        TLatex lCuts1(.26+offs,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.5 * labelsize); lCuts1.Draw();
        TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
        TLatex lCuts12(.26+offs,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.5 * labelsize); lCuts12.Draw();
        TLatex lCuts11(.26+offs,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.5 * labelsize); lCuts11.Draw();
        //     TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.5 * labelsize); lCuts2.Draw();
        TLatex lCuts3(.54+offs,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.5 * labelsize); lCuts3.Draw();
        //     TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.5 * labelsize); lCuts4.Draw();
        
        
        
        c->Print(mDirName+"HitMap.pdf");
        
    }
    c->Print(mDirName+"HitMap.pdf]");
    }
    
    
    
    
    
    
    
    //BEGIN                                     p_y vs. p_x  map (2D)                                                   
    {
    TH2F *referenceHist2D[CepUtil::nSides] = {nullptr,nullptr};
    TH2F *mcHist2D[CepUtil::nSides] = {nullptr,nullptr};
    
    for(int side=0; side<CepUtil::nSides; ++side){
        
        referenceHist2D[side] = new TH2F( *hPxPy[DATA][side] );
        
        for(unsigned int fileId=DATA+1; fileId<nFiles; ++fileId){
            TH2F *mcHist2D_single = new TH2F( *hPxPy[fileId][side] );
            mcHist2D_single->Scale( normalizationFactor[fileId] );
            if(!mcHist2D[side])
                mcHist2D[side] = new TH2F( *mcHist2D_single );
            else
                mcHist2D[side]->Add( mcHist2D_single );
        }
        
    }
    
    
    TCanvas *c = new TCanvas("c", "c", 800, 800);
    c->SetBottomMargin(0);
    c->SetTopMargin(0);
    c->SetLeftMargin(0);
    c->SetRightMargin(0);
    
    c->Divide(1,2);
    
    c->Print(mDirName+"PxPy.pdf[");
    
    TGaxis::SetMaxDigits(4);
    
    double zAxisMax = max( max( referenceHist2D[CepUtil::A]->GetMaximum(),  mcHist2D[CepUtil::A]->GetMaximum() ), 
                           max( referenceHist2D[CepUtil::C]->GetMaximum(),  mcHist2D[CepUtil::C]->GetMaximum() ) );
        
    for(int side=0; side<CepUtil::nSides; ++side){
        
//         referenceHist2D[side]->Rebin2D(2,2);
//         mcHist2D[side]->Rebin2D(2,2);
        
        const double labelsize = 0.07;
        
        
        TH2F *histToDraw = referenceHist2D[side];
        
        c->cd(1);
        gPad->SetBottomMargin(0.03);
        gPad->SetTopMargin(0.24);
        gPad->SetLeftMargin(0.13);
        gPad->SetRightMargin(0.13);
        
        histToDraw->GetXaxis()->SetLabelSize( 0 );
        histToDraw->GetXaxis()->SetTitleSize( 0 );
        histToDraw->GetYaxis()->SetLabelSize( labelsize);
        histToDraw->GetYaxis()->SetTitleSize(labelsize );
        histToDraw->GetZaxis()->SetLabelSize( 0.8*labelsize );
        histToDraw->GetZaxis()->SetTitleSize( labelsize );
        histToDraw->GetXaxis()->SetTickLength( 0.5 * histToDraw->GetXaxis()->GetTickLength() );
        histToDraw->GetXaxis()->SetNdivisions(10,5,1);
        histToDraw->GetXaxis()->SetLabelOffset( -0.02 );
        histToDraw->GetYaxis()->SetLabelOffset( 0.01 );
        
        histToDraw->GetXaxis()->SetRangeUser(-1, 1);
        histToDraw->GetXaxis()->SetTitle("");
        histToDraw->GetYaxis()->SetRangeUser(-0.5, 0.5);
        histToDraw->GetYaxis()->SetTitle("p_{y} [GeV]");
        histToDraw->GetYaxis()->SetTitleOffset(0.9);
        histToDraw->GetYaxis()->SetTickLength(0.5 * histToDraw->GetYaxis()->GetTickLength() );
        TString xWidth(Form("%d", static_cast<int>(1e3*histToDraw->GetXaxis()->GetBinWidth(1)) ) );
        TString yWidth(Form("%d", static_cast<int>(1e3*histToDraw->GetYaxis()->GetBinWidth(1)) ) );
        histToDraw->GetZaxis()->SetTitle("Tracks/("+xWidth+" MeV#times "+yWidth+" MeV)");
        histToDraw->GetZaxis()->SetTitleOffset(0.76);
//         histToDraw->GetZaxis()->SetLabelOffset(0.02);
        histToDraw->Draw("colz");
        
        histToDraw->GetZaxis()->SetRangeUser(0, zAxisMax);
        
        gPad->Update();
        TPaletteAxis* palette = (TPaletteAxis*)histToDraw->GetListOfFunctions()->FindObject("palette");
        if(palette){
            palette->SetX1NDC(0.925-0.05);
            palette->SetX2NDC(0.95-0.05);
        }
        gPad->Modified();
        
        TLatex lSideStr( 0.25, .36, Form("Side %s, DATA", side==CepUtil::A ? "A" : "C") ); lSideStr.SetNDC(); lSideStr.SetTextFont(42); lSideStr.SetTextSize(2.0 * 0.05); lSideStr.Draw();
        
        
            for(int rr=0; rr<2; ++rr){
                for(int tt=0; tt<2; ++tt){
                        TLine *cutLine = new TLine( -1, (rr==0?1:-1)*(tt==0?0.18:0.46), 1, (rr==0?1:-1)*(tt==0?0.18:0.46) );
                        cutLine->SetLineWidth(4);
                        cutLine->SetLineColor(kRed);
                        cutLine->SetLineStyle(7);
                        cutLine->Draw();
                        
                        TArrow *cutArrow = new TArrow( -0.8, (rr==0?1:-1)*(tt==0?0.18:0.46) + (tt==rr ? 0 : -0.08), -0.8, (rr==0?1:-1)*(tt==0?0.18:0.46) + (tt==rr ? 0.08 : 0), 0.025, tt==rr?"|>":"<|");
                        cutArrow->SetAngle(30);
                        cutArrow->SetLineWidth(4);
                        cutArrow->SetLineColor( kRed );
                        cutArrow->SetFillColor( kRed );
                        cutArrow->Draw();   
                }
            }

        
        gPad->RedrawAxis();
        //---
        
        
        histToDraw = mcHist2D[side];
        
        
        c->cd(2);
        gPad->SetBottomMargin(0.23);
        gPad->SetTopMargin(0.04);
        gPad->SetLeftMargin(0.13);
        gPad->SetRightMargin(0.13);
        
        histToDraw->GetXaxis()->SetLabelSize( labelsize );
        histToDraw->GetXaxis()->SetTitleSize( labelsize);
        histToDraw->GetYaxis()->SetLabelSize( labelsize);
        histToDraw->GetYaxis()->SetTitleSize(labelsize );
        histToDraw->GetZaxis()->SetLabelSize( 0.8*labelsize );
        histToDraw->GetZaxis()->SetTitleSize( labelsize );
        histToDraw->GetXaxis()->SetTickLength( 0.5 * histToDraw->GetXaxis()->GetTickLength() );
        histToDraw->GetXaxis()->SetNdivisions(10,5,1);
        histToDraw->GetXaxis()->SetLabelOffset(  0.02 );
        histToDraw->GetYaxis()->SetLabelOffset( 0.01 );
        
        histToDraw->GetXaxis()->SetRangeUser(-1, 1);
        histToDraw->GetXaxis()->SetTitle("p_{x} [GeV]");
        histToDraw->GetYaxis()->SetRangeUser(-0.5, 0.5);
        histToDraw->GetYaxis()->SetTitle("p_{y} [GeV]");
        histToDraw->GetYaxis()->SetTitleOffset(0.9);
        histToDraw->GetYaxis()->SetTickLength(0.5 * histToDraw->GetYaxis()->GetTickLength() );
        histToDraw->GetXaxis()->SetTitleOffset(1.2);
        histToDraw->GetZaxis()->SetTitle("Tracks/("+xWidth+" MeV#times "+yWidth+" MeV)");
        histToDraw->GetZaxis()->SetTitleOffset(0.76);
//         histToDraw->GetZaxis()->SetLabelOffset(0.02);
        histToDraw->Draw("colz");
        
        histToDraw->GetZaxis()->SetRangeUser(0, zAxisMax);
        
        gPad->Update();
        palette = (TPaletteAxis*)histToDraw->GetListOfFunctions()->FindObject("palette");
        if(palette){
            palette->SetX1NDC(0.925-0.05);
            palette->SetX2NDC(0.95-0.05);
        }
        gPad->Modified();
        
        TLatex lSideStr2( 0.25, .54, Form("Side %s, MC", side==CepUtil::A ? "A" : "C") ); lSideStr2.SetNDC(); lSideStr2.SetTextFont(42); lSideStr2.SetTextSize(2.0 * 0.05); lSideStr2.Draw();
        
        
            for(int rr=0; rr<2; ++rr){
                for(int tt=0; tt<2; ++tt){
                        TLine *cutLine = new TLine( -1, (rr==0?1:-1)*(tt==0?0.17:0.45), 1, (rr==0?1:-1)*(tt==0?0.17:0.45) );
                        cutLine->SetLineWidth(4);
                        cutLine->SetLineColor(kRed);
                        cutLine->SetLineStyle(7);
                        cutLine->Draw();
                        
                        TArrow *cutArrow = new TArrow( -0.8, (rr==0?1:-1)*(tt==0?0.17:0.45) + (tt==rr ? 0 : -0.08), -0.8, (rr==0?1:-1)*(tt==0?0.17:0.45) + (tt==rr ? 0.08 : 0), 0.025, tt==rr?"|>":"<|");
                        cutArrow->SetAngle(30);
                        cutArrow->SetLineWidth(4);
                        cutArrow->SetLineColor( kRed );
                        cutArrow->SetFillColor( kRed );
                        cutArrow->Draw();   
                }
            }
        
        const double labelShiftDown = -0.05;
        double offs = -0.04;
        
        c->cd();
        
        TLatex lReaction(.22, .91-labelShiftDown, "#kern[-0.18]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+"+mChannelStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.6 * labelsize);
        lReaction.Draw();
        //     TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
        //lATLAS.Draw();
        
        TString tpcMinPt; tpcMinPt.Form("p_{T} > %g GeV", minInDetTrackPt );
        TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
        TString sideKinematicRegion1("0.18 GeV < |p_{y}| < 0.46 GeV");
        TLatex lCutsA(.16+offs,.84-labelShiftDown+0.04, Form("%s, %s:", singleTrkStr[CepUtil::PLUS].Data(), singleTrkStr[CepUtil::MINUS].Data())); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.4 * labelsize); lCutsA.Draw();
        TLatex lCutsB(.44+offs,.84-labelShiftDown+0.04, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.4 * labelsize); lCutsB.Draw();
        TLatex lCuts1(.26+offs,.84-labelShiftDown+0.04, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.4 * labelsize); lCuts1.Draw();
        TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
        TLatex lCuts12(.26+offs,.795-labelShiftDown+0.06, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.4 * labelsize); lCuts12.Draw();
        TLatex lCuts11(.26+offs,.75-labelShiftDown+0.08, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.4 * labelsize); lCuts11.Draw();
        //     TLatex lCuts2(.54,.84-labelShiftDown, sideKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.4 * labelsize); lCuts2.Draw();
        TLatex lCuts3(.54+offs,.84-labelShiftDown+0.04, sideKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.4 * labelsize); lCuts3.Draw();
        //     TLatex lCuts4(.54,.75-labelShiftDown, sideKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.4 * labelsize); lCuts4.Draw();
        
        
        
        c->Print(mDirName+"PxPy.pdf");
        
    }
    c->Print(mDirName+"PxPy.pdf]");
    }
    
    
    
    
    
    
    
    
    //BEGIN                                       d_0                                                        
    
    for(int r=0; r<3; ++r){
        auto refHistArr = r==0 ? hd0 : hd0_WithOrWithoutVertex[r-1];
        referenceHist = refHistArr[DATA];
        referenceHist->Scale(referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) ), "width");
        
        totalData = new TH1F( *referenceHist );
        
        
        DrawingOptions opts;
        
        for(unsigned int fileId=DATA+1; fileId<nFiles; ++fileId){
            
            TH1F *mcHist = refHistArr[fileId];
            mcHist->Scale( normalizationFactor[fileId] );
            mcHist->Scale(mcHist->GetBinWidth( mcHist->FindBin(0.0001) ), "width");
            
            mcHist->SetLineColor( kBlack );
            mcHist->SetLineWidth( 2 );
            mcHist->SetFillColor( histColor[fileId] );
            
            totalMC[fileId] = new TH1F( *mcHist );
            
            opts.mHistTH1F.push_back( mcHist );
            opts.mHistTH1F_DrawingOpt.push_back( "HIST" );
            
            opts.mObjForLegend.push_back( mcHist );
            opts.mDesriptionForLegend.push_back( legendText[fileId] );
            opts.mDrawTypeForLegend.push_back( "f" );
            
        }
        
        
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.12;
        opts.mRightMargin = 0.035;
        opts.mTopMargin = 0.06;
        opts.mBottomMargin = 0.12;
        
        //x axis
        opts.mXmin = -4;
        opts.mXmax = 4;
        opts.mXaxisTitle = TString("d_{0} [mm]");
        opts.mXaxisTitleSize = 0.05;
        opts.mXaxisTitleOffset = 1.04;
        opts.mXaxisLabelSize = 0.05;
        opts.mXaxisLabelOffset = 0.007;
        opts.mXaxisTickLength = 0.015;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 1.35 * referenceHist->GetMaximum();
        opts.mYaxisTitle = TString(Form("Number of tracks / %g mm", referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) )) );
        opts.mYaxisTitleSize = 0.05;
        opts.mYaxisTitleOffset = 1.25;
        opts.mYaxisLabelSize = 0.05;
        opts.mYaxisLabelOffset = 0.007;
        opts.mYaxisTickLength = 0.015;
        opts.mYlog = kFALSE;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.4;
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString(r==0 ? "d0" : (r==1 ? "d0_noVertex" : "d0_withVertex" ));
        
        opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        opts.mNColumns = 1;
        opts.mLegendX = true ? 0.2 : 0.4;
        opts.mLegendXwidth = 0.2;
        opts.mLegendY = 0.3;
        opts.mLegendYwidth = 0.14*2.2;
        mSpecialLegendMarginFactor = 1.2;
        
        {
            TLine *cutLine[2];
            TArrow *cutArrow[2];
            
            for(int i=0; i<2; ++i){
                cutLine[i] = new TLine( (i==0?1:-1)*1.5, opts.mYmin, (i==0?1:-1)*1.5, 0.15*opts.mYmax );
                cutLine[i]->SetLineWidth(4);
                cutLine[i]->SetLineColor(kRed);
                cutLine[i]->SetLineStyle(7);
                opts.mObjectToDraw.push_back( cutLine[i] );
                
                cutArrow[i] = new TArrow( (i==0?1:-1)*1.5, 0.1*opts.mYmax, (i==0?1:-1)*(1.5 - 0.8), 0.1*opts.mYmax, 0.04, "|>");
                cutArrow[i]->SetAngle(30);
                cutArrow[i]->SetLineWidth(4);
                cutArrow[i]->SetLineColor( kRed );
                cutArrow[i]->SetFillColor( kRed );
                opts.mObjectToDraw.push_back( cutArrow[i] );
            }
        }
        
        double labelShiftDown = 0.04;
        
        mInverseStackDrawing = true;
        
        TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.18]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+"+mChannelStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
        TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
        //opts.mTextToDraw.push_back( lATLAS );
        
        TString tpcMinPt; tpcMinPt.Form("p_{T} > %g GeV", minInDetTrackPt );
        TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
        TString rpKinematicRegion1("0.18 GeV < |p_{y}| < 0.46 GeV");
        TLatex lCutsA(.16,.84-labelShiftDown, Form("%s, %s:", singleTrkStr[CepUtil::PLUS].Data(), singleTrkStr[CepUtil::MINUS].Data())); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
        TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
        TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
        TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
        TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
        TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
        //     TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
        TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        //     TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );   
        
        //     TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
        //     TLatex lDeltaPhi(.25,.677, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
        
        drawFinalResult( referenceHist, opts );
        
        //     TLatex lOppo(mUtil->mCepChannelMult[mChannel]==2 ? 0.18 : .22, .67-labelShiftDown, mTotalChargeStr_OPPO); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
        //     TLatex lSame(mUtil->mCepChannelMult[mChannel]==2 ? .65 : .65, .67-labelShiftDown, mTotalChargeStr_SAME); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
        
        {
            opts.mTopMargin -= 0.04;
            opts.mLegendY += 0.1;
            opts.mLegendX -= 0.06;
            opts.mLegendY2 = opts.mLegendY;
            mRatioFactor = 0.15;
            mRatioYmin = 0.62;
            mRatioYmax = 1.38;
            mRatioYTitleOffsetFactor = 0.5;
            mRatioYTitle = TString("#kern[-0.2]{Data/MC}");
            //log-y scale
            opts.mPdfName = TString("LogY_") + opts.mPdfName;
            opts.mYlog = kTRUE;
            opts.mYmin = 20 / 4;
            opts.mYmax = 100 * referenceHist->GetMaximum() / 4;
            opts.mObjectToDraw.clear(); 
            
            opts.mTextToDraw.clear();
            
            
            TLine *cutLine[2];
            TArrow *cutArrow[2];
            
            for(int i=0; i<2; ++i){
                cutLine[i] = new TLine( (i==0?1:-1)*1.5, opts.mYmin, (i==0?1:-1)*1.5, 0.005*opts.mYmax );
                cutLine[i]->SetLineWidth(4);
                cutLine[i]->SetLineColor(kRed);
                cutLine[i]->SetLineStyle(7);
                opts.mObjectToDraw.push_back( cutLine[i] );
                
                cutArrow[i] = new TArrow( (i==0?1:-1)*1.5, 0.005*opts.mYmax, (i==0?1:-1)*(1.5 - 0.8), 0.005*opts.mYmax, 0.04, "|>");
                cutArrow[i]->SetAngle(30);
                cutArrow[i]->SetLineWidth(4);
                cutArrow[i]->SetLineColor( kRed );
                cutArrow[i]->SetFillColor( kRed );
                opts.mObjectToDraw.push_back( cutArrow[i] );
            }
            
            labelShiftDown -= 0.04;
            
            TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.18]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+"+mChannelStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
            opts.mTextToDraw.push_back( lReaction );
            TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
            //opts.mTextToDraw.push_back( lATLAS );
            
            TString tpcMinPt; tpcMinPt.Form("p_{T} > %g GeV", minInDetTrackPt );
            TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
            TString rpKinematicRegion1("0.18 GeV < |p_{y}| < 0.46 GeV");
            TLatex lCutsA(.16,.84-labelShiftDown, Form("%s, %s:", singleTrkStr[CepUtil::PLUS].Data(), singleTrkStr[CepUtil::MINUS].Data())); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
            TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
            TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
            TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
            TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
            TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
            //       TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
            TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
            //       TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );
            
            
            //       TLatex lOppo(mUtil->mCepChannelMult[mChannel]==2 ? 0.18 : .22, .67-labelShiftDown + 0.07, mTotalChargeStr_OPPO); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
            //       TLatex lSame(mUtil->mCepChannelMult[mChannel]==2 ? .65 : .65, .67-labelShiftDown + 0.07, mTotalChargeStr_SAME); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
            
            
            //       TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
            //       TLatex lDeltaPhi(/*.25*/.16,.677 + 0.04 + 0.02, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
            
            drawFinalResult( referenceHist, opts/*, referenceHist_SameSign*/ );
            
            // back to default
            mRatioFactor = 0.25;
            mRatioYmin = 0.2;
            mRatioYmax = 1.8;
            mRatioYTitleOffsetFactor = 1.0;
            mRatioYTitle = TString("Data / MC");
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    //BEGIN                                       z_0 * sin theta                                                      
    
    {
        referenceHist = hz0SinTheta[DATA];
        referenceHist->Scale(referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) ), "width");
        
        totalData = new TH1F( *referenceHist );
        
        
        DrawingOptions opts;
        
        for(unsigned int fileId=DATA+1; fileId<nFiles; ++fileId){
            
            TH1F *mcHist = hz0SinTheta[fileId];
            mcHist->Scale( normalizationFactor[fileId] );
            mcHist->Scale(mcHist->GetBinWidth( mcHist->FindBin(0.0001) ), "width");
            
            mcHist->SetLineColor( kBlack );
            mcHist->SetLineWidth( 2 );
            mcHist->SetFillColor( histColor[fileId] );
            
            totalMC[fileId] = new TH1F( *mcHist );
            
            opts.mHistTH1F.push_back( mcHist );
            opts.mHistTH1F_DrawingOpt.push_back( "HIST" );
            
            opts.mObjForLegend.push_back( mcHist );
            opts.mDesriptionForLegend.push_back( legendText[fileId] );
            opts.mDrawTypeForLegend.push_back( "f" );
            
        }
        
        
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.12;
        opts.mRightMargin = 0.035;
        opts.mTopMargin = 0.06;
        opts.mBottomMargin = 0.12;
        
        //x axis
        opts.mXmin = -4;
        opts.mXmax = 4;
        opts.mXaxisTitle = TString("z_{0} sin #theta [mm]");
        opts.mXaxisTitleSize = 0.05;
        opts.mXaxisTitleOffset = 1.04;
        opts.mXaxisLabelSize = 0.05;
        opts.mXaxisLabelOffset = 0.007;
        opts.mXaxisTickLength = 0.015;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 1.35 * referenceHist->GetMaximum();
        opts.mYaxisTitle = TString(Form("Number of tracks / %g mm", referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) )) );
        opts.mYaxisTitleSize = 0.05;
        opts.mYaxisTitleOffset = 1.25;
        opts.mYaxisLabelSize = 0.05;
        opts.mYaxisLabelOffset = 0.007;
        opts.mYaxisTickLength = 0.015;
        opts.mYlog = kFALSE;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.4;
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString("z0SinTheta");
        
        opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        opts.mNColumns = 1;
        opts.mLegendX = true ? 0.2 : 0.4;
        opts.mLegendXwidth = 0.2;
        opts.mLegendY = 0.3;
        opts.mLegendYwidth = 0.14*2.2;
        mSpecialLegendMarginFactor = 1.2;
        
        
        
        
        
        
        double labelShiftDown = 0.04;
        
        mInverseStackDrawing = true;
        
        TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.18]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+"+mChannelStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
        TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
        //opts.mTextToDraw.push_back( lATLAS );
        
        TString tpcMinPt; tpcMinPt.Form("p_{T} > %g GeV", minInDetTrackPt );
        TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
        TString rpKinematicRegion1("0.18 GeV < |p_{y}| < 0.46 GeV");
        TLatex lCutsA(.16,.84-labelShiftDown, Form("%s, %s:", singleTrkStr[CepUtil::PLUS].Data(), singleTrkStr[CepUtil::MINUS].Data())); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
        TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
        TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
        TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
        TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
        TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
        //     TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
        TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        //     TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );   
        
        //     TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
        //     TLatex lDeltaPhi(.25,.677, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
        
        drawFinalResult( referenceHist, opts );
        
        //     TLatex lOppo(mUtil->mCepChannelMult[mChannel]==2 ? 0.18 : .22, .67-labelShiftDown, mTotalChargeStr_OPPO); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
        //     TLatex lSame(mUtil->mCepChannelMult[mChannel]==2 ? .65 : .65, .67-labelShiftDown, mTotalChargeStr_SAME); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
        
        {
            opts.mTopMargin -= 0.04;
            if( mChannel==CepUtil::PIPI || mChannel==CepUtil::KK || mChannel==CepUtil::PPBAR ){
                opts.mLegendY += 0.1;
                opts.mLegendX -= 0.04;
            } else{
                opts.mLegendY += 0.15;
                opts.mLegendX -= 0.07;
            }
            opts.mLegendY2 = opts.mLegendY;
            mRatioFactor = 0.15;
            mRatioYmin = 0.62;
            mRatioYmax = 1.38;
            mRatioYTitleOffsetFactor = 0.5;
            mRatioYTitle = TString("#kern[-0.2]{Data/MC}");
            //log-y scale
            opts.mPdfName = TString("LogY_z0SinTheta");
            opts.mYlog = kTRUE;
            opts.mYmin = 20 / 4;
            opts.mYmax = 100 * referenceHist->GetMaximum() / 4;
            opts.mObjectToDraw.clear(); 
            
            opts.mTextToDraw.clear();
            
            labelShiftDown -= 0.04;
            
            
            TLine *cutLine[2];
            TArrow *cutArrow[2];
            
            if( !(mChannel==CepUtil::PIPI || mChannel==CepUtil::KK || mChannel==CepUtil::PPBAR) ){
                for(int i=0; i<2; ++i){
                    cutLine[i] = new TLine( (i==0?1:-1)*1.5, opts.mYmin, (i==0?1:-1)*1.5, 0.005*opts.mYmax );
                    cutLine[i]->SetLineWidth(4);
                    cutLine[i]->SetLineColor(kRed);
                    cutLine[i]->SetLineStyle(7);
                    opts.mObjectToDraw.push_back( cutLine[i] );
                    
                    cutArrow[i] = new TArrow( (i==0?1:-1)*1.5, 0.005*opts.mYmax, (i==0?1:-1)*(1.5 - 0.8), 0.005*opts.mYmax, 0.04, "|>");
                    cutArrow[i]->SetAngle(30);
                    cutArrow[i]->SetLineWidth(4);
                    cutArrow[i]->SetLineColor( kRed );
                    cutArrow[i]->SetFillColor( kRed );
                    opts.mObjectToDraw.push_back( cutArrow[i] );
                }
            }
            
            TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.18]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+"+mChannelStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
            opts.mTextToDraw.push_back( lReaction );
            TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
            //opts.mTextToDraw.push_back( lATLAS );
            
            TString tpcMinPt; tpcMinPt.Form("p_{T} > %g GeV", minInDetTrackPt );
            TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
            TString rpKinematicRegion1("0.18 GeV < |p_{y}| < 0.46 GeV");
            TLatex lCutsA(.16,.84-labelShiftDown, Form("%s, %s:", singleTrkStr[CepUtil::PLUS].Data(), singleTrkStr[CepUtil::MINUS].Data())); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
            TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
            TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
            TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
            TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
            TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
            //       TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
            TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
            //       TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );
            
            
            //       TLatex lOppo(mUtil->mCepChannelMult[mChannel]==2 ? 0.18 : .22, .67-labelShiftDown + 0.07, mTotalChargeStr_OPPO); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
            //       TLatex lSame(mUtil->mCepChannelMult[mChannel]==2 ? .65 : .65, .67-labelShiftDown + 0.07, mTotalChargeStr_SAME); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
            
            
            //       TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
            //       TLatex lDeltaPhi(/*.25*/.16,.677 + 0.04 + 0.02, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
            
            drawFinalResult( referenceHist, opts/*, referenceHist_SameSign*/ );
            
            // back to default
            mRatioFactor = 0.25;
            mRatioYmin = 0.2;
            mRatioYmax = 1.8;
            mRatioYTitleOffsetFactor = 1.0;
            mRatioYTitle = TString("Data / MC");
        }
        
    }
    
    
    
    
    
    
    
    //BEGIN                                       Delta z_0
    
    {
        referenceHist = hDelta_z0[DATA];
        referenceHist->Scale(referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) ), "width");
        
        totalData = new TH1F( *referenceHist );
        
        
        DrawingOptions opts;
        
        for(unsigned int fileId=DATA+1; fileId<nFiles; ++fileId){
            
            TH1F *mcHist = hDelta_z0[fileId];
            mcHist->Scale( normalizationFactor[fileId] );
            mcHist->Scale(mcHist->GetBinWidth( mcHist->FindBin(0.0001) ), "width");
            
            mcHist->SetLineColor( kBlack );
            mcHist->SetLineWidth( 2 );
            mcHist->SetFillColor( histColor[fileId] );
            
            totalMC[fileId] = new TH1F( *mcHist );
            
            opts.mHistTH1F.push_back( mcHist );
            opts.mHistTH1F_DrawingOpt.push_back( "HIST" );
            
            opts.mObjForLegend.push_back( mcHist );
            opts.mDesriptionForLegend.push_back( legendText[fileId] );
            opts.mDrawTypeForLegend.push_back( "f" );
            
        }
        
        
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.12;
        opts.mRightMargin = 0.035;
        opts.mTopMargin = 0.06;
        opts.mBottomMargin = 0.12;
        
        //x axis
        opts.mXmin = 0;
        opts.mXmax = 30;
        opts.mXaxisTitle = TString("|#Deltaz_{0}| [mm]");
        opts.mXaxisTitleSize = 0.05;
        opts.mXaxisTitleOffset = 1.04;
        opts.mXaxisLabelSize = 0.05;
        opts.mXaxisLabelOffset = 0.007;
        opts.mXaxisTickLength = 0.015;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 1.35 * referenceHist->GetMaximum();
        opts.mYaxisTitle = TString(Form("Number of events / %g mm", referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) )) );
        opts.mYaxisTitleSize = 0.05;
        opts.mYaxisTitleOffset = 1.25;
        opts.mYaxisLabelSize = 0.05;
        opts.mYaxisLabelOffset = 0.007;
        opts.mYaxisTickLength = 0.015;
        opts.mYlog = kFALSE;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.4;
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString("Delta_z0");
        
        opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        opts.mNColumns = 1;
        opts.mLegendX = 0.6;
        opts.mLegendXwidth = 0.2;
        opts.mLegendY = 0.45;
        opts.mLegendYwidth = 0.14*2.2;
        mSpecialLegendMarginFactor = 1.2;
        
        
        
        
        
        
        double labelShiftDown = 0.04;
        
        mInverseStackDrawing = true;
        
        TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.18]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+"+mChannelStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
        TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
        //opts.mTextToDraw.push_back( lATLAS );
        
        TString tpcMinPt; tpcMinPt.Form("p_{T} > %g GeV", minInDetTrackPt );
        TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
        TString rpKinematicRegion1("0.18 GeV < |p_{y}| < 0.46 GeV");
        TLatex lCutsA(.16,.84-labelShiftDown, Form("%s, %s:", singleTrkStr[CepUtil::PLUS].Data(), singleTrkStr[CepUtil::MINUS].Data())); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
        TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
        TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
        TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
        TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
        TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
        //     TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
        TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        //     TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );   
        
        //     TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
        //     TLatex lDeltaPhi(.25,.677, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
        
        drawFinalResult( referenceHist, opts );
        
        //     TLatex lOppo(mUtil->mCepChannelMult[mChannel]==2 ? 0.18 : .22, .67-labelShiftDown, mTotalChargeStr_OPPO); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
        //     TLatex lSame(mUtil->mCepChannelMult[mChannel]==2 ? .65 : .65, .67-labelShiftDown, mTotalChargeStr_SAME); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
        
        {
            opts.mTopMargin -= 0.04;
            opts.mLegendY -= 0.05;
            opts.mLegendX -= 0.04;
            opts.mLegendY2 = opts.mLegendY;
            mRatioFactor = 0.15;
            mRatioYmin = 0.62;
            mRatioYmax = 1.38;
            mRatioYTitleOffsetFactor = 0.5;
            mRatioYTitle = TString("#kern[-0.2]{Data/MC}");
            //log-y scale
            opts.mPdfName = TString("LogY_Delta_z0");
            opts.mYlog = kTRUE;
            opts.mYmin = 20 / 4;
            opts.mYmax = 100 * referenceHist->GetMaximum() / 4;
            opts.mObjectToDraw.clear(); 
            
            opts.mTextToDraw.clear();
            
            labelShiftDown -= 0.04;
            
            if( mChannel==CepUtil::PIPI || mChannel==CepUtil::KK || mChannel==CepUtil::PPBAR ){
                TLine *cutLine[2];
                TArrow *cutArrow[2];
                
                for(int i=0; i<1/*2*/; ++i){
                    cutLine[i] = new TLine( (i==0?1:-1)*mParams->maxDeltaZ0, opts.mYmin, (i==0?1:-1)*mParams->maxDeltaZ0, 0.0006*opts.mYmax );
                    cutLine[i]->SetLineWidth(4);
                    cutLine[i]->SetLineColor(kRed);
                    cutLine[i]->SetLineStyle(7);
                    opts.mObjectToDraw.push_back( cutLine[i] );
                    
                    cutArrow[i] = new TArrow( (i==0?1:-1)*mParams->maxDeltaZ0, 0.0006*opts.mYmax, (i==0?1:-1)*(mParams->maxDeltaZ0 - 3), 0.0006*opts.mYmax, 0.04, "|>");
                    cutArrow[i]->SetAngle(30);
                    cutArrow[i]->SetLineWidth(4);
                    cutArrow[i]->SetLineColor( kRed );
                    cutArrow[i]->SetFillColor( kRed );
                    opts.mObjectToDraw.push_back( cutArrow[i] );
                }
            }
            
            TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.18]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+"+mChannelStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
            opts.mTextToDraw.push_back( lReaction );
            TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
            //opts.mTextToDraw.push_back( lATLAS );
            
            TString tpcMinPt; tpcMinPt.Form("p_{T} > %g GeV", minInDetTrackPt );
            TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
            TString rpKinematicRegion1("0.18 GeV < |p_{y}| < 0.46 GeV");
            TLatex lCutsA(.16,.84-labelShiftDown, Form("%s, %s:", singleTrkStr[CepUtil::PLUS].Data(), singleTrkStr[CepUtil::MINUS].Data())); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
            TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
            TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
            TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
            TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
            TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
            //       TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
            TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
            //       TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );
            
            
            //       TLatex lOppo(mUtil->mCepChannelMult[mChannel]==2 ? 0.18 : .22, .67-labelShiftDown + 0.07, mTotalChargeStr_OPPO); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
            //       TLatex lSame(mUtil->mCepChannelMult[mChannel]==2 ? .65 : .65, .67-labelShiftDown + 0.07, mTotalChargeStr_SAME); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
            
            
            //       TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
            //       TLatex lDeltaPhi(/*.25*/.16,.677 + 0.04 + 0.02, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
            
            drawFinalResult( referenceHist, opts/*, referenceHist_SameSign*/ );
            
            // back to default
            mRatioFactor = 0.25;
            mRatioYmin = 0.2;
            mRatioYmax = 1.8;
            mRatioYTitleOffsetFactor = 1.0;
            mRatioYTitle = TString("Data / MC");
        }
        
    }
    
    
    
    
    
    
    
    
    
    //BEGIN                                       N extra tracks                                                            
    
    {
        referenceHist = hNExtraTracks[DATA];
        referenceHist->Scale(referenceHist->GetBinWidth( referenceHist->FindBin(0.0001) ), "width");
        
        totalData = new TH1F( *referenceHist );
        
        
        DrawingOptions opts;
        
        for(unsigned int fileId=DATA+1; fileId<nFiles; ++fileId){
            
            TH1F *mcHist = hNExtraTracks[fileId];
            mcHist->Scale( normalizationFactor[fileId] );
            mcHist->Scale(mcHist->GetBinWidth( mcHist->FindBin(0.0001) ), "width");
            
            mcHist->SetLineColor( kBlack );
            mcHist->SetLineWidth( 2 );
            mcHist->SetFillColor( histColor[fileId] );
            
            totalMC[fileId] = new TH1F( *mcHist );
            
            opts.mHistTH1F.push_back( mcHist );
            opts.mHistTH1F_DrawingOpt.push_back( "HIST" );
            
            opts.mObjForLegend.push_back( mcHist );
            opts.mDesriptionForLegend.push_back( legendText[fileId] );
            opts.mDrawTypeForLegend.push_back( "f" );
            
        }
        
        
        
        opts.mCanvasWidth = 900;
        opts.mCanvasHeight = 800;
        
        opts.mLeftMargin = 0.12;
        opts.mRightMargin = 0.035;
        opts.mTopMargin = 0.06;
        opts.mBottomMargin = 0.12;
        
        //x axis
        opts.mXmin = -0.5;
        opts.mXmax = 2.5;
        opts.mXaxisTitle = TString("Number of extra ID tracks");
        opts.mXaxisTitleSize = 0.05;
        opts.mXaxisTitleOffset = 1.04;
        opts.mXaxisLabelSize = 0.05;
        opts.mXaxisLabelOffset = 0.007;
        opts.mXaxisTickLength = 0.015;
        
        opts.mSetDivisionsX = kTRUE;
        opts.mXnDivisionA = 5;
        opts.mXnDivisionB = 1;
        opts.mXnDivisionC = 1;
        
        //y axis
        opts.mYmin = 0;
        opts.mYmax = 1.6 * referenceHist->GetMaximum();
        opts.mYaxisTitle = TString("Number of events");
        opts.mYaxisTitleSize = 0.05;
        opts.mYaxisTitleOffset = 1.25;
        opts.mYaxisLabelSize = 0.05;
        opts.mYaxisLabelOffset = 0.007;
        opts.mYaxisTickLength = 0.015;
        opts.mYlog = kFALSE;
        
        opts.mMarkerStyle = 20;
        opts.mMarkerColor = kBlack;
        opts.mMarkerSize = 1.4;
        opts.mLineStyle = 1;
        opts.mLineColor = kBlack;
        opts.mLineWidth = 2;
        
        opts.mPdfName = TString("NExtraTracks");
        
        opts.mLegendTextSize = 0.7 * opts.mXaxisTitleSize;
        opts.mNColumns = 1;
        opts.mLegendX = 0.6;
        opts.mLegendXwidth = 0.2;
        opts.mLegendY = 0.45;
        opts.mLegendYwidth = 0.14*2.2;
        mSpecialLegendMarginFactor = 1.2;
        
        
        
        TLine *cutLine = new TLine( 0.5, 0, 0.5, (mBkgdSubtractionDemonstration ? 0.07 : 1.1)*referenceHist->GetMaximum() );
        cutLine->SetLineWidth(4);
        cutLine->SetLineColor(kRed);
        cutLine->SetLineStyle(7);
        cutLine->Draw();
        opts.mObjectToDraw.push_back(cutLine);
            
        TArrow *cutArrow = new TArrow( 0.5, (mBkgdSubtractionDemonstration ? 0.07 : 1.1)*referenceHist->GetMaximum(), 0.5-0.2, (mBkgdSubtractionDemonstration ? 0.07 : 1.1)*referenceHist->GetMaximum(), 0.035, "|>");
        cutArrow->SetAngle(30);
        cutArrow->SetLineWidth(4);
        cutArrow->SetLineColor( kRed );
        cutArrow->SetFillColor( kRed );
        opts.mObjectToDraw.push_back(cutArrow);   
        
        
        double labelShiftDown = 0.04;
        
        mInverseStackDrawing = true;
        
        TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.18]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+"+mChannelStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
        opts.mTextToDraw.push_back( lReaction );
        TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
        //opts.mTextToDraw.push_back( lATLAS );
        
        TString tpcMinPt; tpcMinPt.Form("p_{T} > %g GeV", minInDetTrackPt );
        TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
        TString rpKinematicRegion1("0.18 GeV < |p_{y}| < 0.46 GeV");
        TLatex lCutsA(.16,.84-labelShiftDown, Form("%s, %s:", singleTrkStr[CepUtil::PLUS].Data(), singleTrkStr[CepUtil::MINUS].Data())); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
        TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
        TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
        TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
        TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
        TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
        //     TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
        TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
        //     TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );   
        
        //     TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
        //     TLatex lDeltaPhi(.25,.677, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
        
        drawFinalResult( referenceHist, opts );
        
        //     TLatex lOppo(mUtil->mCepChannelMult[mChannel]==2 ? 0.18 : .22, .67-labelShiftDown, mTotalChargeStr_OPPO); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
        //     TLatex lSame(mUtil->mCepChannelMult[mChannel]==2 ? .65 : .65, .67-labelShiftDown, mTotalChargeStr_SAME); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
        
        {
            opts.mTopMargin -= 0.04;
            opts.mLegendY -= 0.05;
            opts.mLegendX -= 0.04;
            opts.mLegendY2 = opts.mLegendY;
            mRatioFactor = 0.15;
            mRatioYmin = 0.62;
            mRatioYmax = 1.38;
            mRatioYTitleOffsetFactor = 0.5;
            mRatioYTitle = TString("#kern[-0.2]{Data/MC}");
            //log-y scale
            opts.mPdfName = TString("LogY_NExtraTracks");
            opts.mYlog = kTRUE;
            opts.mYmin = 20 / 4;
            opts.mYmax = 100 * referenceHist->GetMaximum() / 4;
            opts.mObjectToDraw.clear(); 
            
            opts.mTextToDraw.clear();
            
            labelShiftDown -= 0.04;
            
            TLine *cutLine = new TLine( 0.5, 0, 0.5, (mBkgdSubtractionDemonstration ? 0.07 : 0.4)*referenceHist->GetMaximum() );
            cutLine->SetLineWidth(4);
            cutLine->SetLineColor(kRed);
            cutLine->SetLineStyle(7);
            cutLine->Draw();
            opts.mObjectToDraw.push_back(cutLine);
            
            TArrow *cutArrow = new TArrow( 0.5, (mBkgdSubtractionDemonstration ? 0.07 : 0.4)*referenceHist->GetMaximum(), 0.5-0.2, (mBkgdSubtractionDemonstration ? 0.07 : 0.4)*referenceHist->GetMaximum(), 0.035, "|>");
            cutArrow->SetAngle(30);
            cutArrow->SetLineWidth(4);
            cutArrow->SetLineColor( kRed );
            cutArrow->SetFillColor( kRed );
            opts.mObjectToDraw.push_back(cutArrow);   
            
            TLatex lReaction(.32, .91-labelShiftDown, "#kern[-0.18]{#font[72]{ATLAS} Internal   #it{p}+#it{p}#rightarrow#it{p'}+"+mChannelStr+"+#it{p'}  #sqrt{#it{s}} = 13 TeV}"); lReaction.SetNDC(); lReaction.SetTextFont(42);  lReaction.SetTextSize(0.8 * opts.mXaxisTitleSize);
            opts.mTextToDraw.push_back( lReaction );
            TLatex lATLAS(0.15, 0.9-labelShiftDown, "ATLAS #font[42]{Internal}"); lATLAS.SetNDC(); lATLAS.SetTextFont(72);  lATLAS.SetTextSize(.06);
            //opts.mTextToDraw.push_back( lATLAS );
            
            TString tpcMinPt; tpcMinPt.Form("p_{T} > %g GeV", minInDetTrackPt );
            TString tpcEta; tpcEta.Form("|#eta| < %.1f", maxEta );
            TString rpKinematicRegion1("0.18 GeV < |p_{y}| < 0.46 GeV");
            TLatex lCutsA(.16,.84-labelShiftDown, Form("%s, %s:", singleTrkStr[CepUtil::PLUS].Data(), singleTrkStr[CepUtil::MINUS].Data())); lCutsA.SetNDC(); lCutsA.SetTextFont(42);  lCutsA.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsA );
            TLatex lCutsB(.44,.84-labelShiftDown, "    #it{p'}:"); lCutsB.SetNDC(); lCutsB.SetTextFont(42);  lCutsB.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCutsB );
            TLatex lCuts1(.26,.84-labelShiftDown, tpcMinPt); lCuts1.SetNDC(); lCuts1.SetTextFont(42);  lCuts1.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts1 );
            TString tpcMinMaxPt("max(p_{T}) > 0.2 GeV");
            TLatex lCuts12(.26,.795-labelShiftDown, tpcMinMaxPt); lCuts12.SetNDC(); lCuts12.SetTextFont(42);  lCuts12.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts12 );
            TLatex lCuts11(.26,.75-labelShiftDown, tpcEta); lCuts11.SetNDC(); lCuts11.SetTextFont(42);  lCuts11.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts11 );
            //       TLatex lCuts2(.54,.84-labelShiftDown, rpKinematicRegion2); lCuts2.SetNDC(); lCuts2.SetTextFont(42);  lCuts2.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts2 );
            TLatex lCuts3(.54,.84-labelShiftDown, rpKinematicRegion1); lCuts3.SetNDC(); lCuts3.SetTextFont(42);  lCuts3.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts3 ); 
            //       TLatex lCuts4(.54,.75-labelShiftDown, rpKinematicRegion3); lCuts4.SetNDC(); lCuts4.SetTextFont(42);  lCuts4.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lCuts4 );
            
            
            //       TLatex lOppo(mUtil->mCepChannelMult[mChannel]==2 ? 0.18 : .22, .67-labelShiftDown + 0.07, mTotalChargeStr_OPPO); lOppo.SetNDC(); lOppo.SetTextFont(42);  lOppo.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lOppo );
            //       TLatex lSame(mUtil->mCepChannelMult[mChannel]==2 ? .65 : .65, .67-labelShiftDown + 0.07, mTotalChargeStr_SAME); lSame.SetNDC(); lSame.SetTextFont(42);  lSame.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lSame );
            
            
            //       TString deltaPhiStr( true ? "#Delta#varphi < 90^{#circ}" : "#Delta#varphi > 90^{#circ}" );
            //       TLatex lDeltaPhi(/*.25*/.16,.677 + 0.04 + 0.02, deltaPhiStr); lDeltaPhi.SetNDC(); lDeltaPhi.SetTextFont(42);  lDeltaPhi.SetTextSize(0.7 * opts.mXaxisTitleSize); opts.mTextToDraw.push_back( lDeltaPhi );
            
            drawFinalResult( referenceHist, opts/*, referenceHist_SameSign*/ );
            
            // back to default
            mRatioFactor = 0.25;
            mRatioYmin = 0.2;
            mRatioYmax = 1.8;
            mRatioYTitleOffsetFactor = 1.0;
            mRatioYTitle = TString("Data / MC");
        }
        
    }
    
    
    
    
    
    

    
}





void DataVsMC::drawFinalResult(TH1F* hMainInput, DrawingOptions & opt, TH1F* hMainInput_2){
    
    TH1F *hMain = new TH1F( *hMainInput );
    hMain->SetName("FinalResult_"+TString(hMain->GetName()));
    
    TH1F *hMain_2 = nullptr;
    if(hMainInput_2){
        hMain_2 = new TH1F( *hMainInput_2 );
        hMain_2->SetName("FinalResult_"+TString(hMain_2->GetName()));
    }
    
    TGaxis::SetMaxDigits(opt.mMaxDigits);
    
    TCanvas *c = new TCanvas("cccccc", "cccccc", opt.mCanvasWidth, opt.mCanvasHeight);
    //   c->SetFrameFillStyle(0);
    //   c->SetFrameLineWidth(2);
    //   c->SetFillColor(-1);
    c->SetLeftMargin(opt.mLeftMargin);
    c->SetRightMargin(opt.mRightMargin);
    c->SetTopMargin(opt.mTopMargin);
    c->SetBottomMargin(opt.mBottomMargin);
    
    c->SetLogy(opt.mYlog);
    
    //   systematicErrorsGraph->SetFillColor( 18 );
    //   //systematicErrorsGraph->SetFillStyle(/*3013*//*3345*/3005);
    //   systematicErrorsGraph->SetLineWidth(0);
    //   systematicErrorsGraph->SetLineColor(kWhite);
    
    if(opt.mScale){
        hMain->Scale( opt.mScalingFactor );
    }
    
    hMain->SetMarkerStyle(opt.mMarkerStyle);
    hMain->SetMarkerSize(opt.mMarkerSize);
    hMain->SetMarkerColor(opt.mMarkerColor);
    hMain->SetLineStyle(opt.mLineStyle);
    hMain->SetLineColor(opt.mLineColor);
    hMain->SetLineWidth(opt.mLineWidth);
    
    if( hMain_2 ){
        hMain_2->SetMarkerStyle(opt.mMarkerStyle2);
        hMain_2->SetMarkerSize(opt.mMarkerSize2);
        hMain_2->SetMarkerColor(opt.mMarkerColor2);
        hMain_2->SetLineStyle(opt.mLineStyle2);
        hMain_2->SetLineColor(opt.mLineColor2);
        hMain_2->SetLineWidth(opt.mLineWidth2);
    }
    
    hMain->GetXaxis()->SetTitle(opt.mXaxisTitle);
    hMain->GetXaxis()->SetTitleSize(opt.mXaxisTitleSize);
    hMain->GetXaxis()->SetTitleOffset(opt.mXaxisTitleOffset);
    hMain->GetXaxis()->SetLabelSize(opt.mXaxisLabelSize);
    hMain->GetXaxis()->SetLabelOffset(opt.mXaxisLabelOffset);
    hMain->GetXaxis()->SetTickLength(opt.mXaxisTickLength);
    hMain->GetXaxis()->SetRangeUser(opt.mXmin, opt.mXmax);
    
    if( opt.mSetDivisionsX )
        hMain->GetXaxis()->SetNdivisions( opt.mXnDivisionA, opt.mXnDivisionB, opt.mXnDivisionC, !opt.mForceDivisionsX );
    
    hMain->GetYaxis()->SetTitleSize(opt.mYaxisTitleSize);
    hMain->GetYaxis()->SetTitle(opt.mYaxisTitle);
    hMain->GetYaxis()->SetTitleOffset(opt.mYaxisTitleOffset);
    hMain->GetYaxis()->SetLabelSize(opt.mYaxisLabelSize);
    hMain->GetYaxis()->SetLabelOffset(opt.mYaxisLabelOffset);
    hMain->GetYaxis()->SetTickLength(opt.mYaxisTickLength);
    hMain->GetYaxis()->SetRangeUser(opt.mYmin, (opt.mScale ? opt.mScalingFactor : 1.0) * opt.mYmax);
    
    if( opt.mSetDivisionsY )
        hMain->GetYaxis()->SetNdivisions( opt.mYnDivisionA, opt.mYnDivisionB, opt.mYnDivisionC, !opt.mForceDivisionsY );
    
    hMain->Draw(mSpecialDataDrawing ? "HIST E" : "PE");
    if(hMain_2){
        hMain_2->GetXaxis()->SetRangeUser(opt.mXmin, opt.mXmax);
        hMain_2->Draw(mSpecialDataDrawing ? "HIST E SAME" : "PE SAME");
    }
    
    
    const double systErrorBoxWidth = (opt.mMarkerSize * 8.0  * opt.mSystErrorBoxWidthAdjustmentFactor / opt.mCanvasWidth*( 1.0 - opt.mLeftMargin - opt.mRightMargin )) * (hMain->GetXaxis()->GetXmax() - hMain->GetXaxis()->GetXmin());
    
    
    TBox *typicalBox = nullptr;
    //   if( opt.mXpositionSystErrorBox.size() > 0 ){
    //     for(unsigned int i=0; i<opt.mXpositionSystErrorBox.size(); ++i){
    //       int binNumber = hMain->GetXaxis()->FindBin( opt.mXpositionSystErrorBox[i] );
    //       if( !(hMain->GetBinContent(binNumber)>0) )
    //         continue;
    //       
    //       double xMinSystErrorBox = hMain->GetXaxis()->GetBinCenter(binNumber) - systErrorBoxWidth/2.;
    //       double xMaxSystErrorBox = hMain->GetXaxis()->GetBinCenter(binNumber) + systErrorBoxWidth/2.;
    //       double yMinSystErrorBox = hMain->GetBinContent(binNumber) * (1.0 - sqrt(pow(syst.mhSystErrorDown->GetBinContent(binNumber),2) + pow(mParams->luminosityUncertainty()*1.0e5,2)));
    //       double yMaxSystErrorBox = hMain->GetBinContent(binNumber) * (1.0 + sqrt(pow(syst.mhSystErrorUp->GetBinContent(binNumber),2) + pow(mParams->luminosityUncertainty()*1.0e5,2)));
    //       TBox *b = new TBox( xMinSystErrorBox, yMinSystErrorBox, xMaxSystErrorBox, yMaxSystErrorBox );
    //       b->SetFillColor(opt.mSystErrorTotalBoxColor);
    //       b->SetLineWidth(0);
    //       b->Draw("l");
    //       
    //       yMinSystErrorBox = hMain->GetBinContent(binNumber) * (1.0 - syst.mhSystErrorDown->GetBinContent(binNumber));
    //       yMaxSystErrorBox = hMain->GetBinContent(binNumber) * (1.0 + syst.mhSystErrorUp->GetBinContent(binNumber));
    //       b = new TBox( xMinSystErrorBox, yMinSystErrorBox, xMaxSystErrorBox, yMaxSystErrorBox );
    //       b->SetFillColor(opt.mSystErrorBoxColor);
    //       b->SetLineWidth(0);
    //       b->Draw("l");
    //       
    //       if( !typicalBox ){
    //         typicalBox = new TBox( xMinSystErrorBox, yMinSystErrorBox, xMaxSystErrorBox, yMaxSystErrorBox );
    //         typicalBox->SetFillColor(opt.mSystErrorTotalBoxColor);
    //         typicalBox->SetLineColor(opt.mSystErrorBoxColor);
    //         typicalBox->SetLineWidth(8);
    //       }
    //     }
    //   }
    
    
    
    if( opt.mHistTH1F.size()>0 ){
        THStack *hs = new THStack( Form("hs_%d", static_cast<int>(gRandom->Uniform()*100000)), "THStack title" );
        
        if( mInverseStackDrawing ){
            for(int i=opt.mHistTH1F.size()-1; i>=0; --i){
                if(opt.mScale){
                    opt.mHistTH1F[i]->Scale( opt.mScalingFactor );
                }
                opt.mHistTH1F[i]->GetXaxis()->SetRangeUser(opt.mXmin, opt.mXmax);
                hs->Add( opt.mHistTH1F[i], opt.mHistTH1F_DrawingOpt[i] );
            }
        } else{
            for(unsigned int i=0; i<opt.mHistTH1F.size(); ++i){
                if(opt.mScale){
                    opt.mHistTH1F[i]->Scale( opt.mScalingFactor );
                }
                opt.mHistTH1F[i]->GetXaxis()->SetRangeUser(opt.mXmin, opt.mXmax);
                hs->Add( opt.mHistTH1F[i], opt.mHistTH1F_DrawingOpt[i] );
            }
        }
        
        hs->Draw("HIST SAME");
        
        
        
        
        TH1F* histMC = nullptr;
        if(opt.mHistTH1F.size() > 0){
            histMC = new TH1F(*opt.mHistTH1F[0]);
            for(unsigned int fileId=1; fileId<opt.mHistTH1F.size(); ++fileId) 
                histMC->Add( opt.mHistTH1F[fileId] );
        }
        
        TGraphAsymmErrors *graphErr = new TGraphAsymmErrors( histMC );
        graphErr->SetFillStyle( 3005 );
        graphErr->SetFillColor( kBlack );
        graphErr->Draw("02");
    }
    
    
    std::vector<TH1F*> hToDrawVec;
    if( hMain_2 && opt.mHistTH1F2.size()>0 ){
        TH1F *stackedUpTo_i = nullptr;
        for(int i=opt.mHistTH1F2.size()-1; i>=0; --i){
            if(opt.mScale){
                opt.mHistTH1F2[i]->Scale( opt.mScalingFactor );
            }
            TH1F *stackedUpTo_i_tmp = new TH1F( *opt.mHistTH1F2[i] );
            stackedUpTo_i_tmp->SetName( Form("stackedUpTo_%d", i) );
            if(i<(static_cast<int>(opt.mHistTH1F2.size())-1))
                stackedUpTo_i_tmp->Add( stackedUpTo_i );
            hToDrawVec.push_back( stackedUpTo_i_tmp );
            stackedUpTo_i = stackedUpTo_i_tmp;
        }
        
        for(int j=hToDrawVec.size()-1; j>=0; --j)
            hToDrawVec[j]->Draw("HIST SAME");
    }
    
    
    TLegend *legend = new TLegend( opt.mLegendX, opt.mLegendY, opt.mLegendX+opt.mLegendXwidth, opt.mLegendY+opt.mLegendYwidth );
    legend->SetNColumns(opt.mNColumns);
    legend->SetBorderSize(0);
    if( mBkgdSubtractionDemonstration ){
        legend->SetFillStyle(1001);
        legend->SetFillColorAlpha(kWhite, 0.8);
    } else
        legend->SetFillStyle(0);
    legend->SetTextSize(opt.mLegendTextSize);
    legend->AddEntry(hMain, mSpecialDataLegendDesctiption==TString("") ? "Data" : mSpecialDataLegendDesctiption, mSpecialDataDrawing ? "f" : "pel");
    if(mBkgdSubtractionDemonstration)
        legend->AddEntry(hMain_2, mSpecialDataLegendDesctiption==TString("") ? "Data (SS)" : mSpecialDataLegendDesctiption, mSpecialDataDrawing ? "f" : "pel");
    //   legend->AddEntry(hMain, "Stat. uncertainty", "el");
    if( mSpecialDataDrawing2 && hMain_2 )
        legend->AddEntry(hMain_2, "Data (non-excl. bkgd)", mSpecialDataDrawing ? "f" : "pel");
    if(typicalBox)
        legend->AddEntry(typicalBox, "Syst. uncertainty", "fl");
    for(unsigned int i=0; i<opt.mObjForLegend.size(); ++i)
        legend->AddEntry( opt.mObjForLegend[i], opt.mDesriptionForLegend[i], opt.mDrawTypeForLegend[i]);
    legend->SetMargin(mSpecialLegendMarginFactor * 0.3 * opt.mLegendXwidth * legend->GetNRows() / legend->GetNColumns() );
    if(!mBkgdSubtractionDemonstration)
        legend->Draw();
    
    TLegend *legend2 = nullptr;
    if( hMain_2 && !mSpecialDataDrawing2 ){
        legend2 = new TLegend( opt.mLegendX2, opt.mLegendY2, opt.mLegendX2+opt.mLegendXwidth2, opt.mLegendY2+opt.mLegendYwidth2 );
        legend2->SetNColumns(opt.mNColumns);
        legend2->SetBorderSize(0);
        if( mBkgdSubtractionDemonstration ){
            legend2->SetFillStyle(1001);
            legend2->SetFillColorAlpha(kWhite, 0.6);
        } else
            legend2->SetFillStyle(0);
        legend2->SetTextSize(opt.mLegendTextSize2);
        legend2->AddEntry(hMain_2, "Data", "pel");
        //   legend2->AddEntry(hMain, "Stat. uncertainty", "el");
        if(typicalBox)
            legend2->AddEntry(typicalBox, "Syst. uncertainty", "fl");
        for(unsigned int i=0; i<opt.mObjForLegend2.size(); ++i)
            legend2->AddEntry( opt.mObjForLegend2[i], opt.mDesriptionForLegend2[i], opt.mDrawTypeForLegend2[i]);
        legend2->SetMargin(mSpecialLegendMarginFactor * 0.3 * opt.mLegendXwidth2 * legend2->GetNRows() / legend2->GetNColumns() );
        legend2->Draw();
    }
    
    
    if( opt.mTextToDraw.size() > 0 ){
        for(unsigned int i=0; i<opt.mTextToDraw.size(); ++i){
            opt.mTextToDraw[i].SetNDC(kTRUE);
            opt.mTextToDraw[i].Draw();
        }
    }
    
    //   TASImage *img = new TASImage("miscellaneous/ATLAS_logo.pdf");
    //   //   TASImage *img = new TASImage("miscellaneous/ATLAS_logo.eps");
    //   img->SetImageQuality(TAttImage::kImgBest);
    
    
    hMain->DrawCopy(mSpecialDataDrawing ? "HIST E SAME" : "PE SAME", "");
    if( hMain_2 )
        hMain_2->DrawCopy("PE SAME", "");
    
    if(mBkgdSubtractionDemonstration)
        legend->Draw();
    
    
    for(int i=opt.mObjectToDraw.size()-1; i>=0; --i){
        if( TString(opt.mObjectToDraw[i]->ClassName())==TString("TF1") || TString(opt.mObjectToDraw[i]->GetName()).Contains("DRAW_SAME") )
            opt.mObjectToDraw[i]->Draw("SAME");
        else if( TString(opt.mObjectToDraw[i]->GetName()).Contains("HIST_SAME") )
            opt.mObjectToDraw[i]->Draw("HIST SAME");
        else if( TString(opt.mObjectToDraw[i]->GetName()).Contains("E2_SAME") )
            opt.mObjectToDraw[i]->Draw("E2 SAME");
        else 
            opt.mObjectToDraw[i]->Draw();
    }
    
    gPad->RedrawAxis();
    
    
    if( opt.mInsert ){
        TPad *insert = new TPad("insert", "insert", opt.mX1Insert, opt.mY1Insert, opt.mX2Insert, opt.mY2Insert);
        insert->SetBottomMargin(0); // Upper and lower plot are joined
        insert->SetFrameFillStyle(0);
        insert->SetFrameLineWidth(2);
        insert->SetFillColor(-1);
        insert->Draw();             // Draw the upper pad: insert
        insert->cd();               // insert becomes the current pad
        insert->SetLeftMargin(opt.mLeftMargin);
        insert->SetRightMargin(opt.mRightMargin);
        insert->SetTopMargin(opt.mTopMargin);
        insert->SetBottomMargin(opt.mBottomMargin);
        
        TH1F *hMainCopy = new TH1F( *hMain );
        hMainCopy->GetXaxis()->SetTitle("");
        hMainCopy->GetXaxis()->SetLabelSize( 0.9*hMainCopy->GetXaxis()->GetLabelSize() * ( 1. - opt.mLeftMargin - opt.mRightMargin) / (opt.mX2Insert - opt.mX1Insert) );
        hMainCopy->GetYaxis()->SetLabelSize( 0.9*hMainCopy->GetYaxis()->GetLabelSize() * ( 1. - opt.mLeftMargin - opt.mRightMargin) / (opt.mX2Insert - opt.mX1Insert) );
        hMainCopy->GetYaxis()->SetTitle("");
        hMainCopy->GetXaxis()->SetRange( hMainCopy->FindBin(opt.mXMinInstert ), hMainCopy->FindBin(opt.mXMaxInstert ) );
        int binMaxY;
        binMaxY = hMainCopy->GetMaximumBin();
        hMainCopy->GetYaxis()->SetRangeUser(0.01, opt.mYMaxInstert<0 ? (1.1*hMainCopy->GetBinContent( binMaxY )) : opt.mYMaxInstert);
        
        int n1 = hMainCopy->GetYaxis()->GetNdivisions()%100;
        int n2 = hMainCopy->GetYaxis()->GetNdivisions()%10000 - n1;
        int n3 = hMainCopy->GetYaxis()->GetNdivisions() - 100*n2 - n1;
        
        hMainCopy->GetYaxis()->SetNdivisions( n1/2, 2, 1 );
        hMainCopy->GetXaxis()->SetRangeUser(opt.mXMinInstert, opt.mXMaxInstert);
        hMainCopy->GetXaxis()->SetNdivisions(10,5,1);
        
        hMainCopy->GetXaxis()->SetTickLength(0.04);
        hMainCopy->GetYaxis()->SetTickLength(0.02);
        
        hMainCopy->Draw(mSpecialDataDrawing ? "HIST E" : "PE");
        for(unsigned int i=0; i<opt.mHistTH1F.size(); ++i){
            if(opt.mScale){
                opt.mHistTH1F[i]->Scale( opt.mScalingFactor );
            }
            opt.mHistTH1F[i]->DrawCopy( opt.mHistTH1F_DrawingOpt[i], "" );
        }
        
        //     if( opt.mXpositionSystErrorBox.size() > 0 ){
        //       for(unsigned int i=0; i<opt.mXpositionSystErrorBox.size(); ++i){
        //         int binNumber = hMainCopy->GetXaxis()->FindBin( opt.mXpositionSystErrorBox[i] );
        //         if( !(hMainCopy->GetBinContent(binNumber)>0) || opt.mXpositionSystErrorBox[i]>opt.mXMaxInstert || opt.mXpositionSystErrorBox[i]<opt.mXMinInstert )
        //           continue;
        //         
        //         double xMinSystErrorBox = hMainCopy->GetXaxis()->GetBinCenter(binNumber) - systErrorBoxWidth/2.;
        //         double xMaxSystErrorBox = hMainCopy->GetXaxis()->GetBinCenter(binNumber) + systErrorBoxWidth/2.;
        //         double yMinSystErrorBox = hMainCopy->GetBinContent(binNumber) * (1.0 - sqrt(pow(syst.mhSystErrorDown->GetBinContent(binNumber),2) + pow(mParams->luminosityUncertainty()*1.0e5,2)));
        //         double yMaxSystErrorBox = hMainCopy->GetBinContent(binNumber) * (1.0 + sqrt(pow(syst.mhSystErrorUp->GetBinContent(binNumber),2) + pow(mParams->luminosityUncertainty()*1.0e5,2)));
        //         TBox *b = new TBox( xMinSystErrorBox, yMinSystErrorBox, xMaxSystErrorBox, yMaxSystErrorBox );
        //         b->SetFillColor(opt.mSystErrorTotalBoxColor);
        //         b->SetLineWidth(0);
        //         b->Draw("l");
        //         
        //         yMinSystErrorBox = hMainCopy->GetBinContent(binNumber) * (1.0 - syst.mhSystErrorDown->GetBinContent(binNumber));
        //         yMaxSystErrorBox = hMainCopy->GetBinContent(binNumber) * (1.0 + syst.mhSystErrorUp->GetBinContent(binNumber));
        //         b = new TBox( xMinSystErrorBox, yMinSystErrorBox, xMaxSystErrorBox, yMaxSystErrorBox );
        //         b->SetFillColor(opt.mSystErrorBoxColor);
        //         b->SetLineWidth(0);
        //         b->Draw("l");
        //       }
        //     }
        
        hMainCopy->DrawCopy(mSpecialDataDrawing ? "HIST E SAME" : "PE SAME", "");
        gPad->RedrawAxis();
    }
    
    c->cd();
    c->Modified();
    c->Update();
    
    c->Print(mDirName + opt.mPdfName+".pdf", "EmbedFonts");
    //   c->Print(opt.mPdfName+".png");
    
    TCanvas *c2 = new TCanvas("cccccc2", "cccccc2", opt.mCanvasWidth, (mRatioFactor + 1.0)*opt.mCanvasHeight);
    //   c->SetFrameFillStyle(0);
    //   c->SetFrameLineWidth(2);
    //   c->SetFillColor(-1);
    c2->SetLeftMargin(0);
    c2->SetRightMargin(0);
    c2->SetTopMargin(0);
    c2->SetBottomMargin(0);
    
    // Upper plot will be in pad1
    TPad *pad1 = new TPad("pad1", "pad1", 0, mRatioFactor, 1.0, 1.0);
    pad1->SetBottomMargin(0); // Upper and lower plot are joined
    pad1->SetFrameFillStyle(0);
    pad1->SetFillColor(-1);
    pad1->Draw();             // Draw the upper pad: pad1
    pad1->cd();               // pad1 becomes the current pad
    gPad->SetLeftMargin(0);
    gPad->SetRightMargin(0);
    gPad->SetTopMargin(0);
    gPad->SetBottomMargin(0);
    
    TH1F *ratio_Data_to_Data = new TH1F( *hMain );
    TH1F *ratio_Data_to_Data_2 = nullptr;
    if(hMain_2)
        ratio_Data_to_Data_2 = new TH1F( *hMain_2 );
    hMain->GetXaxis()->SetLabelSize( 0 );
    hMain->GetXaxis()->SetTitle("");
    c->Modified();
    c->Update();
    
    c->DrawClonePad();
    
    const double modifiedRatioFactor = mRatioFactor + 0.54*opt.mBottomMargin;
    
    c2->cd();          // Go back to the main canvas before defining pad2
    TPad *pad2 = new TPad("pad2", "pad2", 0, 0, 1.0, mRatioFactor + 0.65*opt.mBottomMargin);
    pad2->SetFrameFillStyle(0);
    pad2->SetFillColor(-1);
    //   gStyle->SetGridColor(kGray);
    pad2->SetGridy(kTRUE);
    pad2->Draw();
    pad2->cd();       // pad2 becomes the current pad
    gPad->SetLeftMargin(opt.mLeftMargin);
    gPad->SetRightMargin(opt.mRightMargin);
    gPad->SetTopMargin(0.005);
    gPad->SetBottomMargin(opt.mBottomMargin * (1.0-modifiedRatioFactor)/modifiedRatioFactor);
    
    if( !mDrawSpecialRatio2 && !mDrawSpecialRatio3 ){
        
        TH1F* histMC_ratio = nullptr;
        if(opt.mHistTH1F.size() > 0){
            histMC_ratio = new TH1F(*opt.mHistTH1F[0]);
            for(unsigned int fileId=1; fileId<opt.mHistTH1F.size(); ++fileId) 
                histMC_ratio->Add( opt.mHistTH1F[fileId] );
        }
        
        TH1F* histMC_ratio2 = nullptr;
        if( hMain_2 && hToDrawVec.size()>0 )
            histMC_ratio2 = hToDrawVec[ hToDrawVec.size()-1 ];
        
        if( histMC_ratio )
            ratio_Data_to_Data->Divide( histMC_ratio );
        if( ratio_Data_to_Data_2 )
            ratio_Data_to_Data_2->Divide( histMC_ratio2 );
        
        TGraphAsymmErrors *graphErr = new TGraphAsymmErrors( ratio_Data_to_Data );
        if(ratio_Data_to_Data){
            for(int i=1; i<=ratio_Data_to_Data->GetNbinsX(); ++i){
                graphErr->SetPointEYlow( i-1, histMC_ratio->GetBinError(i)/histMC_ratio->GetBinContent(i) );
                graphErr->SetPointEYhigh( i-1, histMC_ratio->GetBinError(i)/histMC_ratio->GetBinContent(i) );
                graphErr->SetPointY( i-1, 1);
            }
        }
        TGraphAsymmErrors *graphErr_2 = new TGraphAsymmErrors( ratio_Data_to_Data_2 );
        if(ratio_Data_to_Data_2){
            for(int i=1; i<=ratio_Data_to_Data_2->GetNbinsX(); ++i){
                graphErr_2->SetPointEYlow( i-1, histMC_ratio2->GetBinError(i)/histMC_ratio2->GetBinContent(i) );
                graphErr_2->SetPointEYhigh( i-1, histMC_ratio2->GetBinError(i)/histMC_ratio2->GetBinContent(i) );
                graphErr_2->SetPointY( i-1, 1);
            }
        }
        
        if( histMC_ratio && ratio_Data_to_Data ){
            for(int i=1; i<=ratio_Data_to_Data->GetNbinsX(); ++i){
                if( ratio_Data_to_Data->GetBinError(i)>0 ){
                    ratio_Data_to_Data->SetBinError( i, hMain->GetBinError(i)/histMC_ratio->GetBinContent(i) );
                }
            }
        }
        
        if( histMC_ratio2 && ratio_Data_to_Data_2 ){
            for(int i=1; i<=ratio_Data_to_Data_2->GetNbinsX(); ++i){
                if( ratio_Data_to_Data_2->GetBinError(i)>0 ){
                    ratio_Data_to_Data_2->SetBinError( i, hMain_2->GetBinError(i)/histMC_ratio2->GetBinContent(i) );
                }
            }
        }
        
        
        TH1F *ratioratio = nullptr;
        if( mDrawSpecialRatio ){
            ratioratio = new TH1F( *hMain );
            ratioratio->SetName("ratioratio");
            TH1F *tmpratioratio = new TH1F( *hMain_2 );
            tmpratioratio->SetName("tmpratioratio");
            tmpratioratio->Scale( ratioratio->Integral(0,-1) / tmpratioratio->Integral(0,-1) );
            ratioratio->Divide( tmpratioratio );
        }
        
        ratio_Data_to_Data->GetYaxis()->SetRangeUser(mRatioYmin, mRatioYmax);
        ratio_Data_to_Data->GetYaxis()->SetTitle(mRatioYTitle);
        ratio_Data_to_Data->GetYaxis()->SetTitleOffset( 0.56 * mRatioYTitleOffsetFactor );
        ratio_Data_to_Data->GetYaxis()->CenterTitle();
        ratio_Data_to_Data->GetYaxis()->SetNdivisions(4,4,1);
        ratio_Data_to_Data->GetYaxis()->SetTitleSize( /*1.6**/ratio_Data_to_Data->GetYaxis()->GetTitleSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
        ratio_Data_to_Data->GetYaxis()->SetLabelSize( /*1.2**/ratio_Data_to_Data->GetYaxis()->GetLabelSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
        ratio_Data_to_Data->GetXaxis()->SetLabelOffset( 0.01 );
        ratio_Data_to_Data->GetYaxis()->SetTickLength( ratio_Data_to_Data->GetYaxis()->GetTickLength() );
        ratio_Data_to_Data->GetXaxis()->SetTitleSize( /*1.4**/ratio_Data_to_Data->GetXaxis()->GetTitleSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
        ratio_Data_to_Data->GetXaxis()->SetLabelSize( /*1.4**/ratio_Data_to_Data->GetXaxis()->GetLabelSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
        ratio_Data_to_Data->GetXaxis()->SetTickLength( (1.0-modifiedRatioFactor)/modifiedRatioFactor* 1.2 *ratio_Data_to_Data->GetXaxis()->GetTickLength() );
        
        ratio_Data_to_Data->Draw("PE0");
        
        //     TMultiGraph *mg = new TMultiGraph();
        if( ratio_Data_to_Data ){
            
            graphErr->SetFillStyle( 3005 );
            graphErr->SetFillColor( kBlack );
            graphErr->Draw("02");
        }
        
        if( ratio_Data_to_Data_2 ){
            
            graphErr_2->SetFillStyle( 3004 );
            graphErr_2->SetFillColor( kRed );
            graphErr_2->Draw("02");
        }
        
        for(unsigned int i=0; i<opt.mObjectToDraw.size(); ++i){
            if( TString(opt.mObjectToDraw[i]->ClassName())==TString("TBox") ){
                TBox *b = dynamic_cast<TBox*>( opt.mObjectToDraw[i] );
                double x1 = b->GetX1();
                double x2 = b->GetX2();
                double y1 = b->GetY2() / histMC_ratio->GetBinContent( histMC_ratio->FindBin(0.5*(x1+x2)) );
                double y2 = b->GetY1() / histMC_ratio->GetBinContent( histMC_ratio->FindBin(0.5*(x1+x2)) );
                if(y1>mRatioYmax) y1 = mRatioYmax; else if(y1<mRatioYmin) y1 = mRatioYmin;
                if(y2>mRatioYmax) y2 = mRatioYmax; else if(y2<mRatioYmin) y2 = mRatioYmin;
                if((y1>=mRatioYmax && y2>=mRatioYmax) || (y1<=mRatioYmin && y2<=mRatioYmin)) continue;
                TBox *bb = new TBox( x1, y1, x2, y2 );
                bb->SetFillColorAlpha( b->GetFillColor(), 0.3 );
                bb->Draw();
            }
        }
        
        
        if( ratio_Data_to_Data_2 )
            ratio_Data_to_Data_2->Draw("PE0 SAME");
        if( mDrawSpecialRatio && ratioratio ){
            ratioratio->SetMarkerColor(kBlack);
            ratioratio->SetLineColor(kBlack);
            ratioratio->SetMarkerStyle(24);
            ratioratio->Draw("PE0 SAME");
        }
        
        TLine *line = new TLine(opt.mXmin, 1.0, opt.mXmax, 1.0);
        line->SetLineWidth(2);
        line->SetLineStyle(7);
        line->SetLineColor(kBlack);
        line->Draw();
        
        //   for(unsigned int i=0; i<histMC_ratio.size(); ++i){
        //     histMC_ratio[i]->SetMarkerColor( histMC_ratio[i]->GetLineColor() );
        //     histMC_ratio[i]->SetMarkerStyle( opt.mMarkerStyle );
        //     histMC_ratio[i]->SetMarkerSize( opt.mMarkerSize );
        //     histMC_ratio[i]->Draw( /*opt.mHistTH1F_DrawingOpt[i]*/"PE0 ][ SAME" );
        //   }
        
    } else{
        
        if( mDrawSpecialRatio2 ){
            TString titleX = ratio_Data_to_Data->GetXaxis()->GetTitle();
            double labelSize = ratio_Data_to_Data->GetXaxis()->GetLabelSize();
            ratio_Data_to_Data = new TH1F( *hMain_2 );
            ratio_Data_to_Data->Divide( hMain );
            
            ratio_Data_to_Data->GetYaxis()->SetRangeUser(mRatioYmin, mRatioYmax);
            ratio_Data_to_Data->GetYaxis()->SetTitle(mRatioYTitle);
            ratio_Data_to_Data->GetXaxis()->SetTitle( titleX );
            ratio_Data_to_Data->GetYaxis()->SetTitleOffset( 0.56 * mRatioYTitleOffsetFactor );
            ratio_Data_to_Data->GetYaxis()->CenterTitle();
            ratio_Data_to_Data->GetYaxis()->SetNdivisions(4,4,1);
            ratio_Data_to_Data->GetYaxis()->SetTitleSize( /*1.6**/hMain->GetYaxis()->GetTitleSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
            ratio_Data_to_Data->GetYaxis()->SetLabelSize( /*1.2**/hMain->GetYaxis()->GetLabelSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
            ratio_Data_to_Data->GetXaxis()->SetLabelOffset( 0.01 );
            ratio_Data_to_Data->GetYaxis()->SetTickLength( hMain->GetYaxis()->GetTickLength() );
            ratio_Data_to_Data->GetXaxis()->SetTitleSize( /*1.4**/hMain->GetXaxis()->GetTitleSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
            ratio_Data_to_Data->GetXaxis()->SetLabelSize( /*1.4**/labelSize*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
            ratio_Data_to_Data->GetXaxis()->SetTickLength( (1.0-modifiedRatioFactor)/modifiedRatioFactor* 1.2 *hMain->GetXaxis()->GetTickLength() );
            
            if( opt.mSetDivisionsX )
                ratio_Data_to_Data->GetXaxis()->SetNdivisions( opt.mXnDivisionA, opt.mXnDivisionB, opt.mXnDivisionC, !opt.mForceDivisionsX );
            
            ratio_Data_to_Data->Draw("PE");
            
            
            if( opt.mHistTH1F.size()>0 ){
                THStack *hs = new THStack( Form("hs_%d", static_cast<int>(gRandom->Uniform()*100000)), "THStack title" );
                
                if( mInverseStackDrawing ){
                    for(int i=opt.mHistTH1F.size()-1; i>=0; --i){
                        TH1F *histCopy = new TH1F( *opt.mHistTH1F[i] );
                        //           if(opt.mScale){
                        //             histCopy->Scale( opt.mScalingFactor );
                        //           }
                        histCopy->Divide( hMain );
                        hs->Add( histCopy, opt.mHistTH1F_DrawingOpt[i] );
                    }
                } else{
                    for(unsigned int i=0; i<opt.mHistTH1F.size(); ++i){
                        TH1F *histCopy = new TH1F( *opt.mHistTH1F[i] );
                        //           if(opt.mScale){
                        //             histCopy->Scale( opt.mScalingFactor );
                        //           }
                        histCopy->Divide( hMain );
                        hs->Add( histCopy, opt.mHistTH1F_DrawingOpt[i] );
                    }
                }
                
                hs->Draw("HIST SAME");
            }
        } else     
            if( mDrawSpecialRatio3 ){
                //       TString titleX = ratio_Data_to_Data->GetXaxis()->GetTitle();
                //       double labelSize = ratio_Data_to_Data->GetXaxis()->GetLabelSize();
                //       ratio_Data_to_Data = new TH1F( *hMain_2 );
                //       ratio_Data_to_Data->Divide( hMain );
                //       
                //       ratio_Data_to_Data->GetYaxis()->SetRangeUser(mRatioYmin, mRatioYmax);
                //       ratio_Data_to_Data->GetYaxis()->SetTitle(mRatioYTitle);
                //       ratio_Data_to_Data->GetXaxis()->SetTitle( titleX );
                //       ratio_Data_to_Data->GetYaxis()->SetTitleOffset( 0.56 * mRatioYTitleOffsetFactor );
                //       ratio_Data_to_Data->GetYaxis()->CenterTitle();
                //       ratio_Data_to_Data->GetYaxis()->SetNdivisions(4,4,1);
                //       ratio_Data_to_Data->GetYaxis()->SetTitleSize( /*1.6**/hMain->GetYaxis()->GetTitleSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
                //       ratio_Data_to_Data->GetYaxis()->SetLabelSize( /*1.2**/hMain->GetYaxis()->GetLabelSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
                //       ratio_Data_to_Data->GetXaxis()->SetLabelOffset( 0.01 );
                //       ratio_Data_to_Data->GetYaxis()->SetTickLength( hMain->GetYaxis()->GetTickLength() );
                //       ratio_Data_to_Data->GetXaxis()->SetTitleSize( /*1.4**/hMain->GetXaxis()->GetTitleSize()*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
                //       ratio_Data_to_Data->GetXaxis()->SetLabelSize( /*1.4**/labelSize*(1.0-modifiedRatioFactor)/modifiedRatioFactor );
                //       ratio_Data_to_Data->GetXaxis()->SetTickLength( (1.0-modifiedRatioFactor)/modifiedRatioFactor* 1.2 *hMain->GetXaxis()->GetTickLength() );
                //       
                //       if( opt.mSetDivisionsX )
                //         ratio_Data_to_Data->GetXaxis()->SetNdivisions( opt.mXnDivisionA, opt.mXnDivisionB, opt.mXnDivisionC, !opt.mForceDivisionsX );
                //       
                //       ratio_Data_to_Data->Draw("PE");
                //       
                //       
                //       if( opt.mHistTH1F.size()>0 ){
                //         THStack *hs = new THStack( Form("hs_%d", static_cast<int>(gRandom->Uniform()*100000)), "THStack title" );
                //         
                //         if( mInverseStackDrawing ){
                //           for(int i=opt.mHistTH1F.size()-1; i>=0; --i){
                //             TH1F *histCopy = new TH1F( *opt.mHistTH1F[i] );
                //   //           if(opt.mScale){
                //   //             histCopy->Scale( opt.mScalingFactor );
                //   //           }
                //             histCopy->Divide( hMain );
                //             hs->Add( histCopy, opt.mHistTH1F_DrawingOpt[i] );
                //           }
                //         } else{
                //           for(unsigned int i=0; i<opt.mHistTH1F.size(); ++i){
                //             TH1F *histCopy = new TH1F( *opt.mHistTH1F[i] );
                //   //           if(opt.mScale){
                //   //             histCopy->Scale( opt.mScalingFactor );
                //   //           }
                //             histCopy->Divide( hMain );
                //             hs->Add( histCopy, opt.mHistTH1F_DrawingOpt[i] );
                //           }
                //         }
                //         
                //         hs->Draw("HIST SAME");
                //       }
            }
            
    }
    
    ratio_Data_to_Data->DrawCopy("PE SAME", "");
    gPad->RedrawAxis();
    
    c2->Print(mDirName + "Ratio_"+opt.mPdfName+".pdf", "EmbedFonts");
    //   c2->Print("Ratio_"+opt.mPdfName+".png");
    
    
    delete c;
    delete c2;
}








void DataVsMC::loadHistograms(){
    
    cout << "Loading histograms..." << endl;
    
    TString fileNames[nFiles];
    
    
    fileNames[DATA] =                       "ROOT_files/master/data.root";
    switch( mChannel ){
        case CepUtil::PIPI: fileNames[MC_SIGNAL] = "ROOT_files/master/dime_pipi.root"; break; //NOTE CHANGE IF HAVE OTHER MC SAMPLES
        case CepUtil::KK: fileNames[MC_SIGNAL] = "ROOT_files/master/dime_kk.root"; break;
        case CepUtil::PPBAR: fileNames[MC_SIGNAL] = "ROOT_files/master/graniitti_ppbar.root"; break;
        default:            fileNames[MC_SIGNAL] = "ROOT_files/master/pythia_cd.root"; break;
    }
    fileNames[MC_PYTHIA_CD_BKGD] =          "ROOT_files/master/pythia_cd.root";
    fileNames[MC_PYTHIA_CD_BKGD_NEUTRALS] = "ROOT_files/master/pythia_cd.root";
    
    
    
    
    std::vector<double> missingPtBinningVec[CepUtil::nChargeSum];
    if( mBkgdSubtractionDemonstration ){
        double binEdge = 0.0;
        do{
            missingPtBinningVec[CepUtil::CH_SUM_0].push_back( binEdge );
            missingPtBinningVec[CepUtil::CH_SUM_NOT_0].push_back( binEdge );
            binEdge += 0.2;
        } while( binEdge <= 10 );
    } else{
        if(mChannel==CepUtil::PIPI){
            for(int i=0; i<=15; ++i){
                missingPtBinningVec[CepUtil::CH_SUM_0].push_back(  i*0.2 );
            }
        } else{
            missingPtBinningVec[CepUtil::CH_SUM_0].push_back(  0 );
            missingPtBinningVec[CepUtil::CH_SUM_0].push_back(  0.4 );
            missingPtBinningVec[CepUtil::CH_SUM_0].push_back(  0.8 );
            missingPtBinningVec[CepUtil::CH_SUM_0].push_back(  1.2 );
            missingPtBinningVec[CepUtil::CH_SUM_0].push_back(  1.6 );
            missingPtBinningVec[CepUtil::CH_SUM_0].push_back(  2.0 );
            missingPtBinningVec[CepUtil::CH_SUM_0].push_back(  2.4 );
            missingPtBinningVec[CepUtil::CH_SUM_0].push_back(  3 );
        }
        
        missingPtBinningVec[CepUtil::CH_SUM_0].push_back(  4 );
        missingPtBinningVec[CepUtil::CH_SUM_0].push_back(  5 );
        missingPtBinningVec[CepUtil::CH_SUM_0].push_back(  7 );
        missingPtBinningVec[CepUtil::CH_SUM_0].push_back(  10 );
        missingPtBinningVec[CepUtil::CH_SUM_0].push_back(  13 );
        missingPtBinningVec[CepUtil::CH_SUM_0].push_back(  16 );
        missingPtBinningVec[CepUtil::CH_SUM_0].push_back(  20 );
        missingPtBinningVec[CepUtil::CH_SUM_0].push_back(  30 );
        
        missingPtBinningVec[CepUtil::CH_SUM_NOT_0].push_back(  0 );
        missingPtBinningVec[CepUtil::CH_SUM_NOT_0].push_back(  3 );
        missingPtBinningVec[CepUtil::CH_SUM_NOT_0].push_back(  10 );
        missingPtBinningVec[CepUtil::CH_SUM_NOT_0].push_back(  20 );
        missingPtBinningVec[CepUtil::CH_SUM_NOT_0].push_back(  30 );
    }
    
    
    std::vector<double> missingPxBinningVec[CepUtil::nChargeSum];
    if( mBkgdSubtractionDemonstration ){
        double binEdge = 0.0;
        do{
            missingPxBinningVec[CepUtil::CH_SUM_0].push_back( binEdge );
            binEdge += 0.01;
        } while( binEdge <= 1.0 );
    } else{
        
        missingPxBinningVec[CepUtil::CH_SUM_0].push_back( -1.0 );
        missingPxBinningVec[CepUtil::CH_SUM_0].push_back( -0.6 );
        missingPxBinningVec[CepUtil::CH_SUM_0].push_back( -0.4 );
        missingPxBinningVec[CepUtil::CH_SUM_0].push_back( -0.3 );
        missingPxBinningVec[CepUtil::CH_SUM_0].push_back( -0.2 );
        for(int i=1; i<20; ++i){
            missingPxBinningVec[CepUtil::CH_SUM_0].push_back( -0.2+i*0.02 );
        }
//         missingPxBinningVec[CepUtil::CH_SUM_0].push_back( -0.15 );
//         missingPxBinningVec[CepUtil::CH_SUM_0].push_back( -0.125 );
//         missingPxBinningVec[CepUtil::CH_SUM_0].push_back( -0.1 );
//         missingPxBinningVec[CepUtil::CH_SUM_0].push_back( -0.075 );
//         missingPxBinningVec[CepUtil::CH_SUM_0].push_back( -0.05 );
//         missingPxBinningVec[CepUtil::CH_SUM_0].push_back( -0.025 );
//         missingPxBinningVec[CepUtil::CH_SUM_0].push_back(  0.0 );
//         missingPxBinningVec[CepUtil::CH_SUM_0].push_back(  0.025 );
//         missingPxBinningVec[CepUtil::CH_SUM_0].push_back(  0.05 );
//         missingPxBinningVec[CepUtil::CH_SUM_0].push_back(  0.075 );
//         missingPxBinningVec[CepUtil::CH_SUM_0].push_back(  0.1 );
//         missingPxBinningVec[CepUtil::CH_SUM_0].push_back(  0.125 );
//         missingPxBinningVec[CepUtil::CH_SUM_0].push_back(  0.15 );
        missingPxBinningVec[CepUtil::CH_SUM_0].push_back(  0.2 );
        missingPxBinningVec[CepUtil::CH_SUM_0].push_back(  0.3 );
        missingPxBinningVec[CepUtil::CH_SUM_0].push_back(  0.4 );
        missingPxBinningVec[CepUtil::CH_SUM_0].push_back(  0.6 );
        missingPxBinningVec[CepUtil::CH_SUM_0].push_back(  1.0 );
        
        
        missingPxBinningVec[CepUtil::CH_SUM_NOT_0].push_back( -1.0 );
        //     missingPxBinningVec[CepUtil::CH_SUM_NOT_0].push_back( -0.6 );
        missingPxBinningVec[CepUtil::CH_SUM_NOT_0].push_back( -0.4 );
        //     missingPxBinningVec[CepUtil::CH_SUM_NOT_0].push_back( -0.3 );
        //     missingPxBinningVec[CepUtil::CH_SUM_NOT_0].push_back( -0.2 );
        //     missingPxBinningVec[CepUtil::CH_SUM_NOT_0].push_back( -0.1 );
        missingPxBinningVec[CepUtil::CH_SUM_NOT_0].push_back(  0.0 );
        //     missingPxBinningVec[CepUtil::CH_SUM_NOT_0].push_back(  0.1 );
        //     missingPxBinningVec[CepUtil::CH_SUM_NOT_0].push_back(  0.2 );
        //     missingPxBinningVec[CepUtil::CH_SUM_NOT_0].push_back(  0.3 );
        missingPxBinningVec[CepUtil::CH_SUM_NOT_0].push_back(  0.4 );
        //     missingPxBinningVec[CepUtil::CH_SUM_NOT_0].push_back(  0.6 );
        missingPxBinningVec[CepUtil::CH_SUM_NOT_0].push_back(  1.0 );
        
    }
    
    
    std::vector<double> missingPyBinningVec[CepUtil::nChargeSum];
    if( mBkgdSubtractionDemonstration ){
        double binEdge = 0.0;
        do{
            missingPyBinningVec[CepUtil::CH_SUM_0].push_back( binEdge );
            binEdge += 0.01;
        } while( binEdge <= 1.0 );
    } else{
        
        missingPyBinningVec[CepUtil::CH_SUM_0].push_back( -1.0 );
        missingPyBinningVec[CepUtil::CH_SUM_0].push_back( -0.6 );
        missingPyBinningVec[CepUtil::CH_SUM_0].push_back( -0.4 );
        missingPyBinningVec[CepUtil::CH_SUM_0].push_back( -0.3 );
        missingPyBinningVec[CepUtil::CH_SUM_0].push_back( -0.2 );
        missingPyBinningVec[CepUtil::CH_SUM_0].push_back( -0.1 );
        for(int i=1; i<10; ++i){
            missingPyBinningVec[CepUtil::CH_SUM_0].push_back( -0.1+i*0.02 );
        }
//         missingPyBinningVec[CepUtil::CH_SUM_0].push_back( -0.05 );
//         missingPyBinningVec[CepUtil::CH_SUM_0].push_back( -0.025 );
//         missingPyBinningVec[CepUtil::CH_SUM_0].push_back(  0.0 );
//         missingPyBinningVec[CepUtil::CH_SUM_0].push_back(  0.025 );
//         missingPyBinningVec[CepUtil::CH_SUM_0].push_back(  0.05 );
        missingPyBinningVec[CepUtil::CH_SUM_0].push_back(  0.1 );
        missingPyBinningVec[CepUtil::CH_SUM_0].push_back(  0.2 );
        missingPyBinningVec[CepUtil::CH_SUM_0].push_back(  0.3 );
        missingPyBinningVec[CepUtil::CH_SUM_0].push_back(  0.4 );
        missingPyBinningVec[CepUtil::CH_SUM_0].push_back(  0.6 );
        missingPyBinningVec[CepUtil::CH_SUM_0].push_back(  1.0 );
        
        
        missingPyBinningVec[CepUtil::CH_SUM_NOT_0].push_back( -1.0 );
        //     missingPyBinningVec[CepUtil::CH_SUM_NOT_0].push_back( -0.6 );
        missingPyBinningVec[CepUtil::CH_SUM_NOT_0].push_back( -0.4 );
        //     missingPyBinningVec[CepUtil::CH_SUM_NOT_0].push_back( -0.3 );
        //     missingPyBinningVec[CepUtil::CH_SUM_NOT_0].push_back( -0.2 );
        //     missingPyBinningVec[CepUtil::CH_SUM_NOT_0].push_back( -0.1 );
        missingPyBinningVec[CepUtil::CH_SUM_NOT_0].push_back(  0.0 );
        //     missingPyBinningVec[CepUtil::CH_SUM_NOT_0].push_back(  0.1 );
        //     missingPyBinningVec[CepUtil::CH_SUM_NOT_0].push_back(  0.2 );
        //     missingPyBinningVec[CepUtil::CH_SUM_NOT_0].push_back(  0.3 );
        missingPyBinningVec[CepUtil::CH_SUM_NOT_0].push_back(  0.4 );
        //     missingPyBinningVec[CepUtil::CH_SUM_NOT_0].push_back(  0.6 );
        missingPyBinningVec[CepUtil::CH_SUM_NOT_0].push_back(  1.0 );
        
    }
    
    
    
      for(int f=0; f<nFiles; ++f){
        
        TFile *file = TFile::Open(fileNames[f], "READ");
        cout << "\tOpening file " << fileNames[f] << endl;
        
        CepHistograms_DataVsMC * hDvsMC = new CepHistograms_DataVsMC( "DataVsMC", false, file, f!=DATA );
        
        file->Close();
        
        cout << "\tNow processing histograms... ";

        
        for(int i=0; i<CepUtil::nChargeSum; ++i){
            readTH1F( &hNSigmaMissingPt[f][i], hDvsMC->mh_NSigmaMissingPt[mChannel][i], f, missingPtBinningVec[i] );
            readTH1F( &hNSigmaMissingPt_weighted[f][i], hDvsMC->mh_NSigmaMissingPt_weighted[mChannel][i], f, missingPtBinningVec[i] );
            readTH1F( &hMissingPx[f][i], hDvsMC->mh_MissingPx[mChannel][i], f, missingPxBinningVec[i] );
            readTH1F( &hMissingPy[f][i], hDvsMC->mh_MissingPy[mChannel][i], f, missingPyBinningVec[i] );
            
            readTH1F( &hInvMass[f][i], hDvsMC->mh_Exclusive_InvMass[mChannel][i], f );
            readTH1F( &hInvMass_weighted[f][i], hDvsMC->mh_Exclusive_InvMass_weighted[mChannel][i], f );
            readTH2F( &hNSigmaPtMissVsInvMass[f][i], hDvsMC->mh_Exclusive_NSigmaPtMissVsInvMass[mChannel][i], f );
            readTH2F( &hNSigmaPtMissVsInvMass_weighted[f][i], hDvsMC->mh_Exclusive_NSigmaPtMissVsInvMass_weighted[mChannel][i], f );
        }
        
        for(int i=0; i<CepUtil::nRomanPots; ++i){
            readTH2F( &hHitMap[f][i], hDvsMC->mh_Exclusive_HitMap[mChannel][i], f );
            readTH2F( &hFiberMult[f][i], hDvsMC->mh_Exclusive_FiberMult[mChannel][i], f );
        }
        
        for(int i=0; i<CepUtil::nSides; ++i){
            readTH2F( &hPxPy[f][i], hDvsMC->mh_Exclusive_PxPy[mChannel][i], f );
        }
        
        readTH1F( &hDelta_z0[f], hDvsMC->mh_Exclusive_Delta_z0[mChannel], f );
        readTH1F( &hd0[f], hDvsMC->mh_Exclusive_d0[mChannel], f );
        for(int i=0; i<2; ++i)
            readTH1F( &hd0_WithOrWithoutVertex[i][f], hDvsMC->mh_Exclusive_d0_WithOrWithoutVertex[mChannel][i], f );
        readTH1F( &hz0SinTheta[f], hDvsMC->mh_Exclusive_z0SinTheta[mChannel], f );
        readTH1F( &hNExtraTracks[f], hDvsMC->mh_Exclusive_NExtraTracks[mChannel], f );
        
        delete hDvsMC;
        
        cout << "done." << endl;
      
      }
    
    
    cout << "Histograms in the memory - done!" << endl;
}



//BEGIN------- the two methods below are siblings - keep in mind when introducing changes ------
void DataVsMC::readTH1F( TH1F ** hOut, TH1F * hIn[], int f, std::vector<double> binVec ){
    // create copy of histogram
    (*hOut) = new TH1F( *hIn[ f==DATA ? CepUtil::kData : CepUtil::kUnqualified ] );
    // rebin if required
    if( binVec.size()>0 ){
        (*hOut) = CepUtil::rebinAndGetTH1F( *hOut, binVec );
    }
    
    // for MC only
    if( f!=DATA ){
        // clear all entries
        (*hOut)->Reset("ICESM");
        // loop over various topologies e.g. to change normalization of specific contributions
        for(int j=0; j<CepUtil::nCentralStateTopologies; ++j){
            if( j==CepUtil::kData ) continue;                
            
            if( binVec.size()>0 )
                hIn[j] = CepUtil::rebinAndGetTH1F( hIn[j], binVec );
            
            mergeTopologies(**hOut, *hIn[j], j, f);
        }
    }
}
void DataVsMC::readTH2F( TH2F ** hOut, TH2F * hIn[], int f){
    // create copy of histogram
    (*hOut) = new TH2F( *hIn[ f==DATA ? CepUtil::kData : CepUtil::kUnqualified ] );
    
    // for MC only
    if( f!=DATA ){
        // clear all entries
        (*hOut)->Reset("ICESM");
        // loop over various topologies e.g. to change normalization of specific contributions
        for(int j=0; j<CepUtil::nCentralStateTopologies; ++j){
            if( j==CepUtil::kData ) continue;                
            
            mergeTopologies(**hOut, *hIn[j], j, f);
        }
    }
}
//END ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- -----


void DataVsMC::mergeTopologies(TH1 & hOut, TH1 & hIn, int j, int f) const{
    //BEGIN                         PIPI                                    
    if( mChannel==CepUtil::PIPI ||  mChannel==CepUtil::KK || mChannel==CepUtil::PPBAR ){
        switch(f){
                //----------------------------------------------------
                case MC_SIGNAL: 
                    if( j==CepUtil::k1ChargedPairNoNeutrals ){
                        hOut.Add( &hIn );
                    }
                    break;
                //----------------------------------------------------
                case MC_PYTHIA_CD_BKGD_NEUTRALS:
                    if( j==CepUtil::k1ChargedPairAndNeutrals ){
                        hOut.Add( &hIn, mSubtractNeutrals ? mNeutralsFracToPreserve : 1.0 );
                    }
                    break;
                //----------------------------------------------------
                default: 
                    if( j!=CepUtil::k1ChargedPairNoNeutrals && j!=CepUtil::k1ChargedPairAndNeutrals && j!=CepUtil::kUnqualified ){
                        hOut.Add( &hIn );
                    }
                    break;
                //----------------------------------------------------
        }
    } else
    //BEGIN                         FOURPI                                    
    if( mChannel==CepUtil::FOURPI ){
        switch(f){
                //----------------------------------------------------
                case MC_SIGNAL: 
                    if( j==CepUtil::k2ChargedPairsNoNeutrals ){
                        hOut.Add( &hIn );
                    }
                    break;
                //----------------------------------------------------
                case MC_PYTHIA_CD_BKGD_NEUTRALS:
                    if( j==CepUtil::k2ChargedPairsAndNeutrals ){
                        hOut.Add( &hIn, mSubtractNeutrals ? mNeutralsFracToPreserve : 1.0 );
                    }
                    break;
                //----------------------------------------------------
                default: 
                    if( j!=CepUtil::k2ChargedPairsNoNeutrals && j!=CepUtil::k2ChargedPairsAndNeutrals && j!=CepUtil::kUnqualified ){
                        hOut.Add( &hIn );
                    }
                    break;
                //----------------------------------------------------
        }
    }
}



void DataVsMC::setBlackAndWhiteColorScale(){
    const int nSteps = 100;
    int acceptancePalette[nSteps];
    const unsigned int Number = 2;
    double Red[Number]   = { 1.00, 0.00 };
    double Green[Number] = { 1.00, 0.00 };
    double Blue[Number]  = { 1.00, 0.00 };
    double Stops[Number] = { 0.00, 1.00 };
    
    int fi = TColor::CreateGradientColorTable(Number,Stops,Red,Green,Blue, nSteps);
    for(int i=0; i<nSteps; ++i)
        acceptancePalette[i] = fi+i;
    gStyle->SetPalette(nSteps, acceptancePalette);
}

void DataVsMC::setWhiteAndBlackColorScale(){
    const int nSteps = 100;
    int acceptancePalette[nSteps];
    const unsigned int Number = 2;
    double Red[Number]   = { 0.00, 1.00 };
    double Green[Number] = { 0.00, 1.00 };
    double Blue[Number]  = { 0.00, 1.00 };
    double Stops[Number] = { 0.00, 1.00 };
    
    int fi = TColor::CreateGradientColorTable(Number,Stops,Red,Green,Blue, nSteps);
    for(int i=0; i<nSteps; ++i)
        acceptancePalette[i] = fi+i;
    gStyle->SetPalette(nSteps, acceptancePalette);
}

void DataVsMC::setBlueWhiteRedColorScale(){
    const int nSteps = 100;
    int acceptancePalette[nSteps];
    const unsigned int Number = 3;
    double Red[Number]   = { 0.00, 1.00, 1.00 };
    double Green[Number] = { 0.00, 1.00, 0.00 };
    double Blue[Number]  = { 1.00, 1.00, 0.00 };
    double Stops[Number] = { 0.00, 0.50, 1.00 };
    
    int fi = TColor::CreateGradientColorTable(Number,Stops,Red,Green,Blue, nSteps);
    for(int i=0; i<nSteps; ++i)
        acceptancePalette[i] = fi+i;
    gStyle->SetPalette(nSteps, acceptancePalette);
}


void DataVsMC::setupDrawingStyle() const{
    // setup the drawing style
    gStyle->SetLineScalePS();
    gStyle->SetFrameBorderMode(0);
    gStyle->SetFrameFillColor(0);
    gStyle->SetFrameLineWidth(2);
    gStyle->SetLineWidth(2);
    gStyle->SetHatchesLineWidth(2);
    gStyle->SetCanvasBorderMode(0);
    gStyle->SetCanvasColor(0);
    gStyle->SetPadBorderMode(0);
    gStyle->SetPadColor(0);
    gStyle->SetStatColor(0);
    gStyle->SetPadTickX(1);
    gStyle->SetPadTickY(1);
    gStyle->SetOptStat(0);
    gStyle->SetOptTitle(0);
    gStyle->SetNumberContours(99);
    gStyle->SetPalette(55);
    gStyle->SetHistLineWidth(3.);
    gStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes
    
    setBlackAndWhiteColorScale();
}

Double_t DataVsMC::integratePtMiss(const TH1* hNSigmaMissingPt, Double_t nSigmaCut) const{
    int binNum = 1; 
    for(int i=1; i<=hNSigmaMissingPt->GetNbinsX(); ++i) if(hNSigmaMissingPt->GetXaxis()->GetBinUpEdge(i) >= nSigmaCut ){ binNum = i; break; }
    double integral = hNSigmaMissingPt->Integral(1, binNum-1);
    integral += hNSigmaMissingPt->GetBinContent(binNum) * ( nSigmaCut - hNSigmaMissingPt->GetXaxis()->GetBinLowEdge(binNum) ) / hNSigmaMissingPt->GetXaxis()->GetBinWidth(binNum);
    return integral;
}
