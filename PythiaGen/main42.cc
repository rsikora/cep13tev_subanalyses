#include "Pythia8/Pythia.h"

#include "TROOT.h"
#include "TSystem.h"
#include "TApplication.h"
#include "TTree.h"
#include "TBranch.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TStyle.h"
#include "TObjArray.h"
#include "TAxis.h"
#include "TVector3.h"
#include "TRotation.h"
#include "TLorentzRotation.h"
#include "TString.h"
#include "TGaxis.h"
#include "TRandom3.h"
#include "TF1.h"
#include "../McAnalyser/Util.hh"

using namespace Pythia8;

// Util *mUtil;


// const int runMode = 0; // 2 prong
// const int runMode = 1; // 4 prong
const int runMode = 2; // 4 prong

const int nAnalyzedCentralTracks = runMode == 0 ? 2 : 4;

TH1F *mhInvMassPid[Util::nCharges2Trks][Util::nDefinedParticles];

TH2F *forwardProtonPyVsPx[2];
TH1F *pionEta[2];
TH1F *pionPt[2];

void createHistograms();
void convertToCrossSection();

int mParticlesId = Util::nDefinedParticles;
float totalLumi = 0;
double totalNEvents = 0;
double signalNEvents = 0;

float pCentralParticle2Prong[2][3];
float pCentralParticle4Prong[4][3];
float pForwardProton[2][3];

static const double weightPid = 1.0;
static const double initialMomentum = 6500.0;
static const double protonMass = 0.938272;




TH1F *oo6PiInvMass; //!
TH1F *oo6PiRapidity; //!
TH1F *oo6PiDeltaPhi; //!
TH1F *oo6PiMandelstamTSum; //!

TH1F *oo8PiInvMass; //!
TH1F *oo8PiRapidity; //!
TH1F *oo8PiDeltaPhi; //!
TH1F *oo8PiMandelstamTSum; //!


//==========================================================================

int main(/*int argc, char *argv[]*/) {
  
  TH1::SetDefaultSumw2();
//   mUtil = Util::instance();
  
  totalNEvents = 0;
  for(int s=0;s<2; ++s){
      for(int ss=0; ss<3; ++ss){
          pCentralParticle2Prong[s][ss] = 0;
          pForwardProton[s][ss] = 0;
      }
  }
  for(int s=0;s<4; ++s){
      for(int ss=0; ss<3; ++ss){
          pCentralParticle4Prong[s][ss] = 0;
      }
  }
  
  // Generator. Shorthand for the event.
  Pythia pythia;
  Event& event = pythia.event;
  
  // Read in commands from external file.
  pythia.readFile("main04.cmnd");
  
  // Extract settings to be used in the main program.
  int    nEvent    = pythia.mode("Main:numberOfEvents");
  int    nAbort    = pythia.mode("Main:timesAllowErrors");
  
  
  // Initialize.
  pythia.init();
  
  cout << "After init" << endl;
  
  // Book histograms: multiplicities and mean transverse momenta.
  TRandom3 gen(0);
  TFile *file = new TFile(Form("Pythia8_CepTree_%d.root", static_cast<int>(1000000*gen.Uniform())), "RECREATE");
  TTree *pythiaCepTree = new TTree("CepTree", "Tree with Pythia 8 events of CEP");
  
  pythiaCepTree->Branch("pid", &mParticlesId, "pid/I" );
  pythiaCepTree->Branch("luminosity", &totalLumi, "luminosity/F" );
  
  pythiaCepTree->Branch("pxProton1", &(pForwardProton[0][0]), "pxProton1/F" );
  pythiaCepTree->Branch("pxProton2", &(pForwardProton[1][0]), "pxProton2/F" );
  pythiaCepTree->Branch("pyProton1", &(pForwardProton[0][1]), "pyProton1/F" );
  pythiaCepTree->Branch("pyProton2", &(pForwardProton[1][1]), "pyProton2/F" );
  pythiaCepTree->Branch("pzProton1", &(pForwardProton[0][2]), "pzProton1/F" );
  pythiaCepTree->Branch("pzProton2", &(pForwardProton[1][2]), "pzProton2/F" );
  
  if( runMode==0 ){
    pythiaCepTree->Branch("pxPlus", &(pCentralParticle2Prong[0][0]), "pxPlus/F" );
    pythiaCepTree->Branch("pxMinus", &(pCentralParticle2Prong[1][0]), "pxMinus/F" );
    pythiaCepTree->Branch("pyPlus", &(pCentralParticle2Prong[0][1]), "pyPlus/F" );
    pythiaCepTree->Branch("pyMinus", &(pCentralParticle2Prong[1][1]), "pyMinus/F" );
    pythiaCepTree->Branch("pzPlus", &(pCentralParticle2Prong[0][2]), "pzPlus/F" );
    pythiaCepTree->Branch("pzMinus", &(pCentralParticle2Prong[1][2]), "pzMinus/F" );
  } else
  if( runMode==1 ){
    pythiaCepTree->Branch("pxMinus1", &(pCentralParticle4Prong[0][0]), "pxMinus1/F" );
    pythiaCepTree->Branch("pyMinus1", &(pCentralParticle4Prong[0][1]), "pyMinus1/F" );
    pythiaCepTree->Branch("pzMinus1", &(pCentralParticle4Prong[0][2]), "pzMinus1/F" );
    pythiaCepTree->Branch("pxMinus2", &(pCentralParticle4Prong[1][0]), "pxMinus2/F" );
    pythiaCepTree->Branch("pyMinus2", &(pCentralParticle4Prong[1][1]), "pyMinus2/F" );
    pythiaCepTree->Branch("pzMinus2", &(pCentralParticle4Prong[1][2]), "pzMinus2/F" );
    pythiaCepTree->Branch("pxPlus1", &(pCentralParticle4Prong[2][0]), "pxPlus1/F" );
    pythiaCepTree->Branch("pyPlus1", &(pCentralParticle4Prong[2][1]), "pyPlus1/F" );
    pythiaCepTree->Branch("pzPlus1", &(pCentralParticle4Prong[2][2]), "pzPlus1/F" );
    pythiaCepTree->Branch("pxPlus2", &(pCentralParticle4Prong[3][0]), "pxPlus2/F" );
    pythiaCepTree->Branch("pyPlus2", &(pCentralParticle4Prong[3][1]), "pyPlus2/F" );
    pythiaCepTree->Branch("pzPlus2", &(pCentralParticle4Prong[3][2]), "pzPlus2/F" );
  } else{
//       cout << "ERROR Wrong runMode!" << endl;
//       return 0;
  }
  
  createHistograms();
  
  // Begin event loop.
  int iAbort = 0;
  for (int iEvent = 0; iEvent < nEvent; ++iEvent) {

    // Generate events. Quit if too many failures.
    if (!pythia.next()) {
      if (++iAbort < nAbort) continue;
      cout << " Event generation aborted prematurely, owing to error!\n";
      break;
    }

//     // Extract event classification.
//     int code = pythia.info.code();

    TLorentzVector total4Vec;
    TLorentzVector proton4Vec[2];
    int nFinalPions = 0;
    int nFinalKK = 0;
    int nFinalPPbar = 0;
    int nFinalTotal = 0;
    int nFinalProtons = 0;
    int nPionsPassed = 0;
    int nFinalPionsPassedEtaCut = 0;
    int nFinalPionsPassedPtCut = 0;
    int nCentral[Util::nSigns] = {0, 0};
    bool allPionsPassed = true;
    bool allKaonsPassed = true;
    bool allProtonsPassed = true;
    bool bothForwardProtonsPassed = true;
    const double minPt[Util::nDefinedParticles] = {0.1, 0.2, 0.3};
    const double maxEta = 2.5;
    double eta[2];
    double pt[2];
    
    if ( pythia.info.isDiffractiveC() && !pythia.info.isDiffractiveA() && !pythia.info.isDiffractiveB() ) {
      for (int i = 0; i < event.size(); ++i){
        if(event[i].isFinal() ){
          
          if( event[i].isCharged() ){
            if( event[i].idAbs() == 211 ){
              
              if( nFinalPions<2 ){
                eta[nFinalPions] = event[i].eta();
                pt[nFinalPions] = event[i].pT();
              }
                
              ++nFinalPions;
              ++nCentral[event[i].id()<0 ? Util::MINUS : Util::PLUS];
              int arrayId = event[i].id()<0 ? (nCentral[Util::MINUS]-1) : (nCentral[Util::PLUS]+1);
              
              if( !( event[i].pT()>minPt[Util::PION] && fabs(event[i].eta())<maxEta ) )
                  allPionsPassed = false;
              else
                  ++nPionsPassed;
              if( fabs(event[i].eta())<maxEta )
                  ++nFinalPionsPassedEtaCut;
              if( event[i].pT()>minPt[Util::PION] )
                  ++nFinalPionsPassedPtCut;
              total4Vec += TLorentzVector( TVector3( event[i].px(), event[i].py(), event[i].pz() ), event[i].e() );
              
              pCentralParticle2Prong[event[i].id()<0][0] = static_cast<float>(event[i].px());
              pCentralParticle2Prong[event[i].id()<0][1] = static_cast<float>(event[i].py());
              pCentralParticle2Prong[event[i].id()<0][2] = static_cast<float>(event[i].pz());
              if( arrayId<4 ){
                pCentralParticle4Prong[arrayId][0] = static_cast<float>(event[i].px());
                pCentralParticle4Prong[arrayId][1] = static_cast<float>(event[i].py());
                pCentralParticle4Prong[arrayId][2] = static_cast<float>(event[i].pz());
              }
            } else if( event[i].idAbs() == 321 ){
                ++nFinalKK;
                ++nCentral[event[i].id()<0 ? Util::MINUS : Util::PLUS];
                int arrayId = event[i].id()<0 ? (nCentral[Util::MINUS]-1) : (nCentral[Util::PLUS]+1);
                
                if( !( event[i].pT()>minPt[Util::KAON] && fabs(event[i].eta())<maxEta ) )
                    allKaonsPassed = false;
                total4Vec += TLorentzVector( TVector3( event[i].px(), event[i].py(), event[i].pz() ), event[i].e() );
                
                pCentralParticle2Prong[event[i].id()<0][0] = static_cast<float>(event[i].px());
                pCentralParticle2Prong[event[i].id()<0][1] = static_cast<float>(event[i].py());
                pCentralParticle2Prong[event[i].id()<0][2] = static_cast<float>(event[i].pz());
                if( arrayId<4 ){
                    pCentralParticle4Prong[arrayId][0] = static_cast<float>(event[i].px());
                    pCentralParticle4Prong[arrayId][1] = static_cast<float>(event[i].py());
                    pCentralParticle4Prong[arrayId][2] = static_cast<float>(event[i].pz());
                }
            } else if( event[i].idAbs() == 2212 && fabs(event[i].pz()) < 10 /*GeV*/){
                ++nFinalPPbar;
                ++nCentral[event[i].id()<0 ? Util::MINUS : Util::PLUS];
                int arrayId = event[i].id()<0 ? (nCentral[Util::MINUS]-1) : (nCentral[Util::PLUS]+1);
                
                if( !( event[i].pT()>minPt[Util::PROTON] && fabs(event[i].eta())<maxEta ) )
                    allProtonsPassed = false;
                total4Vec += TLorentzVector( TVector3( event[i].px(), event[i].py(), event[i].pz() ), event[i].e() );
                
                pCentralParticle2Prong[event[i].id()<0][0] = static_cast<float>(event[i].px());
                pCentralParticle2Prong[event[i].id()<0][1] = static_cast<float>(event[i].py());
                pCentralParticle2Prong[event[i].id()<0][2] = static_cast<float>(event[i].pz());
                if( arrayId<4 ){
                    pCentralParticle4Prong[arrayId][0] = static_cast<float>(event[i].px());
                    pCentralParticle4Prong[arrayId][1] = static_cast<float>(event[i].py());
                    pCentralParticle4Prong[arrayId][2] = static_cast<float>(event[i].pz());
                }
            } else if( event[i].id() == 2212 && fabs(event[i].pz()) > 6000 /*GeV*/ ){
              ++nFinalProtons;
              if(nFinalProtons<3){
                  proton4Vec[nFinalProtons-1] = TLorentzVector( TVector3( event[i].px(), event[i].py(), event[i].pz() ), event[i].e() );
                  pForwardProton[event[i].pz()<0][0] = static_cast<float>(event[i].px());
                  pForwardProton[event[i].pz()<0][1] = static_cast<float>(event[i].py());
                  pForwardProton[event[i].pz()<0][2] = static_cast<float>(event[i].pz());
              }
              if( !( fabs(event[i].py()) > 0.17 && fabs(event[i].py()) < 0.5 ) )
                  bothForwardProtonsPassed = false;
            }
          }
          
          ++nFinalTotal;
        }
      }
      

      if( nFinalProtons==2 && (runMode==2 ? (nFinalTotal==8 || nFinalTotal==10) : (nFinalTotal==(2+nAnalyzedCentralTracks))) && bothForwardProtonsPassed ){
        const double mMass = total4Vec.M();

        mParticlesId = Util::nDefinedParticles;
        if( allPionsPassed && (runMode==2 ? (nFinalPions==6 || nFinalPions==8) : (nFinalPions==nAnalyzedCentralTracks)) )
            mParticlesId = Util::PION; else
        if( allKaonsPassed && nFinalKK==nAnalyzedCentralTracks )
            mParticlesId = Util::KAON; else
        if( allProtonsPassed && nFinalPPbar==nAnalyzedCentralTracks )
            mParticlesId = Util::PROTON;
        
        
        if( mParticlesId < Util::nDefinedParticles ){
            
            for(int j=0; j<2; ++j)
                forwardProtonPyVsPx[j]->Fill( pForwardProton[j][0], pForwardProton[j][1] );
                
            for(int j=0; j<2; ++j){
                pionEta[j]->Fill( eta[j] );
                pionPt[j]->Fill( pt[j] );
            }
                
            mhInvMassPid[Util::OPPO][mParticlesId]->Fill( mMass );
            
            if(runMode<2){
                ++signalNEvents;
                pythiaCepTree->Fill();
                
                // clean
                for(int s=0;s<2; ++s){
                    for(int ss=0; ss<3; ++ss){
                        pCentralParticle2Prong[s][ss] = 0;
                        pForwardProton[s][ss] = 0;
                    }
                }
                for(int s=0;s<4; ++s){
                    for(int ss=0; ss<3; ++ss){
                        pCentralParticle4Prong[s][ss] = 0;
                    }
                }
            } else{
                
//                 const double mPairPt = total4Vec.Pt();
                const double mPairRapidity = total4Vec.Rapidity();
                
                TVector2 v1(proton4Vec[0].Px(), proton4Vec[0].Py());
                TVector2 v2(proton4Vec[1].Px(), proton4Vec[1].Py());
                double mDeltaPhiDegrees = acos(v1.Unit()*v2.Unit()) * 180. / TMath::Pi();
                
                double t[Util::nSides];
                for(int side=0; side<Util::nSides; ++side)
                    t[side] = -(proton4Vec[side] - TLorentzVector(0,0, (side==1 ? (-1) : 1)*initialMomentum, sqrt(initialMomentum*initialMomentum + protonMass*protonMass) )).Mag2();
                const double mTSum = fabs( t[0] + t[1] );
                
                if( nFinalPions==6 ){
                    oo6PiInvMass->Fill( mMass );
                    oo6PiRapidity->Fill( mPairRapidity );
                    oo6PiDeltaPhi->Fill( mDeltaPhiDegrees );
                    oo6PiMandelstamTSum->Fill( mTSum );
                } else if( nFinalPions==8 ){
                    oo8PiInvMass->Fill( mMass );
                    oo8PiRapidity->Fill( mPairRapidity );
                    oo8PiDeltaPhi->Fill( mDeltaPhiDegrees );
                    oo8PiMandelstamTSum->Fill( mTSum );
                }
                
            }
            
        }
      }
    }

    ++totalNEvents;
  // End of event loop.
  }
  
  totalLumi = totalNEvents / (pythia.info.sigmaGen() * (runMode<2 ? 1e6 /*mb -> nb*/ : 1e3 /*mb -> ub*/) ); 
  convertToCrossSection();
    
  mParticlesId = Util::nDefinedParticles;
  pythiaCepTree->Fill(); // last event is lumi info event, can be recognized by pid
    
       
  file->Write();
  file->Close();
  
  // Done.
  return 0;
}


void createHistograms(){

  int nMassBins[Util::nDefinedParticles];
  
  nMassBins[Util::PION] = 250;
  nMassBins[Util::KAON] = 100;
  nMassBins[Util::PROTON] = 25;
  
  for(int i=0; i<Util::SAME; ++i)
    for(int j=0; j<Util::nDefinedParticles; ++j)
      mhInvMassPid[i][j] = new TH1F(Form("InvMassPid_%d%d", i,j), "Inv. mass, ", nMassBins[j], 0, 5);
    
  forwardProtonPyVsPx[0] = new TH2F("forwardProtonPyVsPx_EAST", "p_{y} vs. p_{x}", 120, -0.6, 0.6, 120, -0.6, 0.6);
  forwardProtonPyVsPx[1] = new TH2F("forwardProtonPyVsPx_WEST", "p_{y} vs. p_{x}", 120, -0.6, 0.6, 120, -0.6, 0.6);
  
  pionEta[0] = new TH1F("pionEta_PLUS", "#eta of pion", 100, -2.5, 2.5);
  pionEta[1] = new TH1F("pionEta_MINUS", "#eta of pion", 100, -2.5, 2.5);
  
  pionPt[0] = new TH1F("pionPt_PLUS", "p_{T} of pion", 30, 0, 1.5);
  pionPt[1] = new TH1F("pionPt_MINUS", "p_{T} of pion", 30, 0, 1.5);
  
  
  
  const double mPion = 0.13957;
  
  
      //----------------------------------------------------------
    int index = -1;
    std::vector<double> mandelstamTSumBinVec;
    mandelstamTSumBinVec.push_back( 0.0 );
    mandelstamTSumBinVec.push_back( 0.05 );
    mandelstamTSumBinVec.push_back( 0.20 );
    mandelstamTSumBinVec.push_back( 0.40 );
    mandelstamTSumBinVec.push_back( 0.70 );
    mandelstamTSumBinVec.push_back( 1.20 );
    mandelstamTSumBinVec.push_back( 2.00 );
    
    // 6Pi
    index = -1;
    double lowMassThreshold = 6*mPion;
    std::vector<double> sixPionMassBinsVec;
    sixPionMassBinsVec.push_back( lowMassThreshold );
    double massBinBoundary = 0;
    index = 0;
    double massBinWidth = 0.05;
    while( massBinBoundary < 1.0 ){
        ++index;
        massBinBoundary = 0.9 + massBinWidth*index;
        sixPionMassBinsVec.push_back( massBinBoundary );
    }
    index = 0;
    massBinWidth = 0.4;
    while( massBinBoundary < 5.0 ){
        ++index;
        massBinBoundary = 1.0 + massBinWidth*index;
        sixPionMassBinsVec.push_back( massBinBoundary );
    }
    index = 0;
    massBinWidth = 1.0;
    while( massBinBoundary < 20.0 ){
        ++index;
        massBinBoundary = 5.0 + massBinWidth*index;
        sixPionMassBinsVec.push_back( massBinBoundary );
    }
    
    oo6PiInvMass = new TH1F("h_6PiInvMass_ChSum0_Weighted", "h_6PiInvMass_ChSum0_Weighted", sixPionMassBinsVec.size()-1, &(sixPionMassBinsVec[0]));
    oo6PiRapidity = new TH1F("h_6PiRapidity_ChSum0_Weighted", "h_6PiRapidity_ChSum0_Weighted", 10, -2.5, 2.5);
    oo6PiDeltaPhi = new TH1F("h_6PiDeltaPhi_ChSum0_Weighted", "h_6PiDeltaPhi_ChSum0_Weighted", 4, 0, 180);
    oo6PiMandelstamTSum = new TH1F("h_6PiMandelstamTSum_ChSum0_Weighted", "h_6PiMandelstamTSum_ChSum0_Weighted", mandelstamTSumBinVec.size()-1, &(mandelstamTSumBinVec[0]));
    
 
    // 8Pi
    index = -1;
    lowMassThreshold = 8*mPion;
    std::vector<double> eightPionMassBinsVec;
    eightPionMassBinsVec.push_back( lowMassThreshold );
    eightPionMassBinsVec.push_back( 1.5 );
    eightPionMassBinsVec.push_back( 2 );
    eightPionMassBinsVec.push_back( 3 );
    eightPionMassBinsVec.push_back( 4 );
    eightPionMassBinsVec.push_back( 5 );
    eightPionMassBinsVec.push_back( 6 );
    eightPionMassBinsVec.push_back( 7 );
    eightPionMassBinsVec.push_back( 8 );
    eightPionMassBinsVec.push_back( 9 );
    eightPionMassBinsVec.push_back( 10 );
    eightPionMassBinsVec.push_back( 15 );
    eightPionMassBinsVec.push_back( 20 );

    
    oo8PiInvMass = new TH1F("h_8PiInvMass_ChSum0_Weighted", "h_8PiInvMass_ChSum0_Weighted", eightPionMassBinsVec.size()-1, &(eightPionMassBinsVec[0]));
    oo8PiRapidity = new TH1F("h_8PiRapidity_ChSum0_Weighted", "h_8PiRapidity_ChSum0_Weighted", 5, -2.5, 2.5);
    oo8PiDeltaPhi = new TH1F("h_8PiDeltaPhi_ChSum0_Weighted", "h_8PiDeltaPhi_ChSum0_Weighted", 4, 0, 180);
    oo8PiMandelstamTSum = new TH1F("h_8PiMandelstamTSum_ChSum0_Weighted", "h_8PiMandelstamTSum_ChSum0_Weighted", mandelstamTSumBinVec.size()-1, &(mandelstamTSumBinVec[0]));
  
}


void convertToCrossSection(){
    
    oo6PiInvMass->Scale(1./totalLumi, "width");
    oo6PiRapidity->Scale(1./totalLumi, "width");
    oo6PiDeltaPhi->Scale(1./totalLumi, "width");
    oo6PiMandelstamTSum->Scale(1./totalLumi, "width");

    oo8PiInvMass->Scale(1./totalLumi, "width");
    oo8PiRapidity->Scale(1./totalLumi, "width");
    oo8PiDeltaPhi->Scale(1./totalLumi, "width");
    oo8PiMandelstamTSum->Scale(1./totalLumi, "width");
}




