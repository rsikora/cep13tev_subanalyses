# PYTHIA configuration file.
# Generated on pon, 16 mar 2020, 14:27:29 CET with the user supplied options:
# --enable-shared
# --enable-64bit

# Install directory prefixes.
PREFIX_BIN=/usr/hep/pythia8244/bin
PREFIX_INCLUDE=/usr/hep/pythia8244/include
PREFIX_LIB=/usr/hep/pythia8244/lib
PREFIX_SHARE=/usr/hep/pythia8244/share/Pythia8

# Compilation flags (see ./configure --help for further documentation).
ENABLE_SHARED=true
CXX=g++
CXX_COMMON=-O2 -m64  -pedantic -W -Wall -Wshadow -fPIC
CXX_SHARED=-shared
CXX_SONAME=-Wl,-soname,
LIB_SUFFIX=.so

# EVTGEN configuration.
EVTGEN_USE=false
EVTGEN_BIN=
EVTGEN_INCLUDE=
EVTGEN_LIB=

# FASTJET3 configuration.
FASTJET3_USE=true
FASTJET3_BIN=/usr/hep/rivet/bin/
FASTJET3_INCLUDE=/usr/hep/rivet/include/fastjet/
FASTJET3_LIB=/usr/hep/rivet/lib/

# HEPMC2 configuration.
HEPMC2_USE=true
HEPMC2_BIN=/usr/hep/rivet/bin/
HEPMC2_INCLUDE=/usr/hep/rivet/include/HepMC/
HEPMC2_LIB=/usr/hep/rivet/lib/

# HEPMC3 configuration.
HEPMC3_USE=false
HEPMC3_BIN=
HEPMC3_INCLUDE=
HEPMC3_LIB=

# LHAPDF5 configuration.
LHAPDF5_USE=false
LHAPDF5_BIN=
LHAPDF5_INCLUDE=
LHAPDF5_LIB=

# LHAPDF6 configuration.
LHAPDF6_USE=false
LHAPDF6_BIN=
LHAPDF6_INCLUDE=
LHAPDF6_LIB=

# POWHEG configuration.
POWHEG_USE=false
POWHEG_BIN=
POWHEG_INCLUDE=
POWHEG_LIB=

# PROMC configuration.
PROMC_USE=false
PROMC_BIN=
PROMC_INCLUDE=
PROMC_LIB=

# ROOT configuration.
ROOT_USE=true
ROOT_BIN=/usr/hep/root-6.20.00-build/bin/
ROOT_INCLUDE=/usr/hep/root-6.20.00-build/include/
ROOT_LIBS=/usr/hep/root-6.20.00-build/lib/
CXX_ROOT=

# GZIP configuration.
GZIP_USE=false
GZIP_BIN=
GZIP_INCLUDE=
GZIP_LIB=

# BOOST configuration.
BOOST_USE=false
BOOST_BIN=
BOOST_INCLUDE=
BOOST_LIB=

# PYTHON configuration.
PYTHON_USE=false
PYTHON_BIN=
PYTHON_INCLUDE=
PYTHON_LIB=
