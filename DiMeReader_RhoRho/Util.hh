#ifndef Util_hh
#define Util_hh

#include "TString.h"
#include <iostream>

class TH1;
class TF1;

class Util{
  public:
    static Util* instance();

    // Enumerations - very helpful and convenient !
    enum SIDE { E=0, East=0, W=1, West=1, nSides };
    enum XY_COORDINATE { X, Y, nCoordinates };
    enum TRIGGER_ID { SD, CPT2, SDT, RPZMU, RP2MU, ET, CP, CPT, RP2E, Zerobias, CPX, SDZ, CPEI, ZE, nTriggers };
    enum TRIGGER_BIT_LABEL { BIT_ET, BIT_IT, BIT_EOR, BIT_WOR, BIT_EU, BIT_ED, BIT_WU, BIT_WD, nTriggerBits };
    enum RP_ID {E1U, E1D, E2U, E2D, W1U, W1D, W2U, W2D, nRomanPots };
    enum PLANE_ID {A, B, C, D, nPlanes };
    enum BRANCH_ID { EU, ED, WU, WD, nBranches };
    enum ARM_ID { EU_WD, ED_WU, nArms };
    enum STATION_ID { E1, E2, W1, W2, nStations };
    enum STATION_ORDER { RP1, RP2, nStationPerSide};
    enum PROTONS_CONFIGURATION { ALL, ELA, INE, nProtonsConfigurations };
    enum PARTICLE_NAME { /*ELECTRON, */PION, /*MUON,*/ KAON, PROTON, nDefinedParticles };
    enum TPC_TRACK_TYPE { GLO, PRI, TOF, QUA, nTpcTrkTypes }; // GLO=global(all), PRI=primary, TOF=PRI&TofMatched, QUA=TOF&QualityCuts
    enum BUNCH_CROSSING { CB, AG, nBnchXngsTypes }; // CB=colliding bunches, AG=abort gaps
    enum BBC_TILE { SMALL_BBC, LARGE_BBC, nBbcTileTypes };
    enum QSUM_2TRKS { OPPO, SAME, nCharges2Trks };
    enum QSUM_4TRKS { QSUM_ZERO, QSUM_NON0, nCharges4Trks };
    enum SIGN { PLUS, MINUS, nSigns };
    enum RP_TRACK_TYPE_COMBINATION { LL, LG, GG, nRpTrackTypeCombinations }; // LL=local+local, LG=local+global, GG=global+global
    enum LIST_OF_EFF_CORRECTIONS { RPACC, TPCRECOEFF, TOFMATCHEFF, nEffCorrections };
    enum ANALYSIS_CUT { TRIG, TWORPTRKS, ONETOFVX, ZVERTEX, TWOTOFTRKS, TWOQUATRKS, OPPOSITE, TPCRPVX_MATCHED, BBCCLEAN, TOFHITSRECO, EXCLUSIVE, PIPI, KK, PPBAR, nAnalysisCuts };
    enum REFERENCE_FRAME { GOTTFRIED_JACKSON, HELICITY, nReferenceFrames };
    enum RANGE_LIMIT { MIN, MAX };
    enum MASS_RANGE { MASS_1, MASS_2, MASS_3, nMassRanges };
    enum DELTAPHI_RANGE { DELTAPHI_1, DELTAPHI_2, /*DELTAPHI_3, */nDeltaPhiRanges };
    enum PAIRPT_RANGE { PAIRPT_1, PAIRPT_2, PAIRPT_3, nPairPtRanges };
    enum MANDELSTAMTSINGLESIDE_RANGE { MANDELSTAMTSINGLESIDE_1, MANDELSTAMTSINGLESIDE_2, nMandelstamTSingleSideRanges };
    enum MANDELSTAMTBOTHSIDES_RANGE { MANDELSTAMTBOTHSIDES_1, MANDELSTAMTBOTHSIDES_2, MANDELSTAMTBOTHSIDES_3, nMandelstamTBothSidesRanges };
    
    inline TString sideName(UInt_t id) const { if(id<nSides) return mSideName[id]; else{ std::cerr << "ERROR in Util::sideName(UInt_t id): id out of range" << std::endl; return TString("");} }
    inline TString coordinateName(UInt_t id) const { if(id<nCoordinates) return mCoordinateName[id]; else{ std::cerr << "ERROR in Util::coordinateName(UInt_t id): id out of range" << std::endl; return TString("");} }
    inline TString triggerName(UInt_t id) const { if(id<nTriggers) return mTriggerName[id]; else{ std::cerr << "ERROR in Util::triggerName(UInt_t id): id out of range" << std::endl; return TString("");} }
    inline TString triggerBitLabel(UInt_t id) const { if(id<nTriggerBits) return mTriggerBitLabel[id]; else{ std::cerr << "ERROR in Util::triggerBitLabel(UInt_t id): id out of range" << std::endl; return TString("");} }
    inline TString armName(UInt_t id) const { if(id<nArms) return mArmName[id]; else{ std::cerr << "ERROR in Util::armName(UInt_t id): id out of range" << std::endl; return TString("");} }
    inline TString branchName(UInt_t id) const { if(id<nBranches) return mBranchName[id]; else{ std::cerr << "ERROR in Util::branchName(UInt_t id): id out of range" << std::endl; return TString("");} }
    inline TString rpName(UInt_t id) const { if(id<nRomanPots) return mRpName[id]; else{ std::cerr << "ERROR in Util::rpName(UInt_t id): id out of range" << std::endl; return TString("");} }
    inline TString planeName(UInt_t id) const { if(id<nPlanes) return mPlaneName[id]; else{ std::cerr << "ERROR in Util::planeName(UInt_t id): id out of range" << std::endl; return TString("");} }
    inline TString stationName(UInt_t id) const { if(id<nStations) return mStationName[id]; else{ std::cerr << "ERROR in Util::stationName(UInt_t id): id out of range" << std::endl; return TString("");} }
    inline TString protonsConfigurationName(UInt_t id) const { if(id<nProtonsConfigurations) return mProtonsConfiguration[id]; else{ std::cerr << "ERROR in Util::protonsConfigurationName(UInt_t id): id out of range" << std::endl; return TString("");} }
    inline TString particleName(UInt_t id) const { if(id<nDefinedParticles) return mParticleName[id]; else{ std::cerr << "ERROR in Util::particleName(UInt_t id): id out of range" << std::endl; return TString("");} }
    inline TString tpcTrackTypeName(UInt_t id) const { if(id<nTpcTrkTypes) return mTpcTrackTypeName[id]; else{ std::cerr << "ERROR in Util::tpcTrackTypeName(UInt_t id): id out of range" << std::endl; return TString("");} }
    inline TString bunchXngTypeName(UInt_t id) const { if(id<nBnchXngsTypes) return mBunchCrossingTypeName[id]; else{ std::cerr << "ERROR in Util::bunchXngTypeName(UInt_t id): id out of range" << std::endl; return TString("");} }
    inline TString qSum2TrksName(UInt_t id) const { if(id<nCharges2Trks) return mChargeSum2TrksName[id]; else{ std::cerr << "ERROR in Util::qSum2TrksName(UInt_t id): id out of range" << std::endl; return TString("");} }
    inline TString qSum4TrksName(UInt_t id) const { if(id<nCharges4Trks) return mChargeSum4TrksName[id]; else{ std::cerr << "ERROR in Util::qSum4TrksName(UInt_t id): id out of range" << std::endl; return TString("");} }
    inline TString signName(UInt_t id) const { if(id<nSigns) return mSignName[id]; else{ std::cerr << "ERROR in Util::signName(UInt_t id): id out of range" << std::endl; return TString("");} }
    inline TString rpTrackCombinationName(UInt_t id) const { if(id<nRpTrackTypeCombinations) return mRpTrackCombinationName[id]; else{ std::cerr << "ERROR in Util::rpTrackCombinationName(UInt_t id): id out of range" << std::endl; return TString("");} }
    inline TString efficiencyName(UInt_t id) const { if(id<nEffCorrections) return mEfficiencyName[id]; else{ std::cerr << "ERROR in Util::efficiencyName(UInt_t id): id out of range" << std::endl; return TString("");} }
    inline TString analysisCutName(UInt_t id) const { if(id<nAnalysisCuts) return mCutName[id]; else{ std::cerr << "ERROR in Util::analysisCutName(UInt_t id): id out of range" << std::endl; return TString("");} }
    inline TString analysisCutShortName(UInt_t id) const { if(id<nAnalysisCuts) return mCutShortName[id]; else{ std::cerr << "ERROR in Util::analysisCutShortName(UInt_t id): id out of range" << std::endl; return TString("");} }
    inline TString referenceFrameName(UInt_t id) const { if(id<nReferenceFrames) return mReferenceFrameName[id]; else{ std::cerr << "ERROR in Util::referenceFrameName(UInt_t id): id out of range" << std::endl; return TString("");} }
    inline TString referenceFrameShortName(UInt_t id) const { if(id<nReferenceFrames) return mReferenceFrameShortName[id]; else{ std::cerr << "ERROR in Util::referenceFrameShortName(UInt_t id): id out of range" << std::endl; return TString("");} }
    
    Double_t massRangeLimit(UInt_t, UInt_t) const;
    Double_t deltaPhiRangeLimit(UInt_t, UInt_t) const;
    Double_t pairPtRangeLimit(UInt_t, UInt_t) const;
    Double_t mandelstamTSingleSideRangeLimit(UInt_t, UInt_t) const;
    
    MASS_RANGE massRange(Double_t) const;
    DELTAPHI_RANGE deltaPhiRange(Double_t) const;
    PAIRPT_RANGE pairPtRange(Double_t) const;
    MANDELSTAMTSINGLESIDE_RANGE mandelstamTSingleSideRange(Double_t) const;
    MANDELSTAMTBOTHSIDES_RANGE mandelstamTBothSidesRange(Double_t, Double_t) const;
    
    inline Double_t mass(PARTICLE_NAME name) const{ return mParticleMass[name]; }
    inline Double_t c() const{ return mSpeedOfLight; }
    inline Double_t p0() const{ return mBeamMomentum; }
    
    inline BRANCH_ID branchPerRp(RP_ID rpId) const { return mBranchPerRp[rpId]; };
    inline BRANCH_ID oppositeBranch(BRANCH_ID br) const { return mOppositeBranch[br]; };
    inline SIDE sidePerRp(RP_ID rpId) const { return mSidePerRp[rpId]; };
    inline STATION_ORDER stationOrderPerRp(RP_ID rpId) const { return mStationOrderPerRp[rpId]; };
    inline RP_ID rpPerBranchStationOrder(BRANCH_ID br, STATION_ORDER st) const { return mRpPerBranchStationOrder[br][st]; };
    
    Double_t bkgdFraction(const TH1*, Double_t=0.1, TF1* =nullptr) const;
    Double_t integratePtMiss(const TH1*, Double_t=0.1) const;

    static const Double_t PI;
    
  private:
    
    Util();
    ~Util();

    // Labels, names etc. (defined as TString to gain higher functionality than const char*, e.g. defined "+" operator)
    TString* mSideName;
    TString* mCoordinateName;
    TString* mTriggerName;
    TString* mTriggerBitLabel;
    TString* mArmName;
    TString* mBranchName;
    TString* mRpName;
    TString* mPlaneName;
    TString* mStationName;
    TString* mProtonsConfiguration;
    TString* mParticleName;
    TString* mTpcTrackTypeName;
    TString* mBunchCrossingTypeName;
    TString* mChargeSum2TrksName;
    TString* mChargeSum4TrksName;
    TString* mSignName;
    TString* mRpTrackCombinationName;
    TString* mEfficiencyName;
    TString* mCutName;
    TString* mCutShortName;
    TString* mReferenceFrameName;
    TString* mReferenceFrameShortName;
    
    Double_t mMassRangeLimits[nMassRanges][2]; // GeV/c^2
    Double_t mDeltaPhiLimits[nDeltaPhiRanges][2]; // deg
    Double_t mPairPtRangeLimits[nPairPtRanges][2]; // GeV/c
    Double_t mMandelstamTRangeLimits[nMandelstamTSingleSideRanges][2]; // GeV^2/c^2
    
    Double_t mParticleMass[nDefinedParticles]; // GeV/c^2
    const Double_t mSpeedOfLight; // cm/ns
    const Double_t mBeamMomentum; // GeV/c
    
    BRANCH_ID mBranchPerRp[Util::nRomanPots];
    BRANCH_ID mOppositeBranch[Util::nBranches];
    SIDE mSidePerRp[Util::nRomanPots];
    STATION_ORDER mStationOrderPerRp[Util::nRomanPots];
    RP_ID mRpPerBranchStationOrder[Util::nBranches][Util::nStationPerSide];
    
    static Util* mInst; 
};

#endif