#define Util_cxx

#include <limits>
#include "Util.hh"
#include "TString.h"
#include "TH1.h"
#include "TF1.h"

Util* Util::mInst = nullptr;
const Double_t Util::PI = 3.14159265359;

Double_t Util::massRangeLimit(UInt_t massRangeId, UInt_t opt) const{
  if(opt<2 && massRangeId<nMassRanges) return mMassRangeLimits[massRangeId][opt];
  else{ std::cerr << "ERROR in Util::massRangeLimit(UInt_t, UInt_t)" << std::endl; return 0; }
}

Double_t Util::deltaPhiRangeLimit(UInt_t deltaPhiRangeId, UInt_t opt) const{
  if(opt<2 && deltaPhiRangeId<nDeltaPhiRanges) return mDeltaPhiLimits[deltaPhiRangeId][opt];
  else{ std::cerr << "ERROR in Util::deltaPhiRangeLimit(UInt_t, UInt_t)" << std::endl; return 0; }
}

Double_t Util::pairPtRangeLimit(UInt_t pairPtRangeId, UInt_t opt) const{
  if(opt<2 && pairPtRangeId<nPairPtRanges) return mPairPtRangeLimits[pairPtRangeId][opt];
  else{ std::cerr << "ERROR in Util::pairPtRangeLimit(UInt_t, UInt_t)" << std::endl; return 0; }
}

Double_t Util::mandelstamTSingleSideRangeLimit(UInt_t mandelstamTSingleSideRangeId, UInt_t opt) const{
  if(opt<2 && mandelstamTSingleSideRangeId<nMandelstamTSingleSideRanges) return mMandelstamTRangeLimits[mandelstamTSingleSideRangeId][opt];
  else{ std::cerr << "ERROR in Util::mandelstamTSingleSideRangeLimit(UInt_t, UInt_t)" << std::endl; return 0; }
}


Util::Util(): mSpeedOfLight(29.9792), mBeamMomentum(100.14){

  mSideName = new TString[nSides];
  mSideName[E] = TString("East");
  mSideName[W] = TString("West");
  
  mCoordinateName = new TString[nCoordinates];
  mCoordinateName[X] = TString("x");
  mCoordinateName[Y] = TString("y");
  
  mTriggerName = new TString[nTriggers];
  mTriggerName[SD] = TString("SD");
  mTriggerName[CPT2] = TString("CPT2");
  mTriggerName[SDT] = TString("SDT");
  mTriggerName[RPZMU] = TString("RPZMU");
  mTriggerName[RP2MU] = TString("RP2MU");
  mTriggerName[ET] = TString("ET");
  mTriggerName[CP] = TString("CP");
  mTriggerName[CPT] = TString("CPT");
  mTriggerName[RP2E] = TString("RP2E");
  mTriggerName[Zerobias] = TString("Zerobias");
  mTriggerName[CPX] = TString("CPX");
  mTriggerName[SDZ] = TString("SDZ");
  mTriggerName[CPEI] = TString("CPEI");
  mTriggerName[ZE] = TString("ZE");
  
  mTriggerBitLabel = new TString[nTriggerBits];
  mTriggerBitLabel[BIT_ET] = TString("ET");
  mTriggerBitLabel[BIT_IT] = TString("IT");
  mTriggerBitLabel[BIT_EOR] = TString("EOR");
  mTriggerBitLabel[BIT_WOR] = TString("WOR");
  mTriggerBitLabel[BIT_EU] = TString("EU");
  mTriggerBitLabel[BIT_ED] = TString("ED");
  mTriggerBitLabel[BIT_WU] = TString("WU");
  mTriggerBitLabel[BIT_WD] = TString("WD");
  
  mArmName = new TString[nArms];
  mArmName[EU_WD] = TString("EU-WD");
  mArmName[ED_WU] = TString("ED-WU");
  
  mBranchName = new TString[nBranches];
  mBranchName[EU] = TString("EU");
  mBranchName[ED] = TString("ED");
  mBranchName[WU] = TString("WU");
  mBranchName[WD] = TString("WD");
  
  mRpName = new TString[nRomanPots];
  mRpName[E1U] = TString("E1U");
  mRpName[E1D] = TString("E1D");
  mRpName[E2U] = TString("E2U");
  mRpName[E2D] = TString("E2D");
  mRpName[W1U] = TString("W1U");
  mRpName[W1D] = TString("W1D");
  mRpName[W2U] = TString("W2U");
  mRpName[W2D] = TString("W2D");
  
  mPlaneName = new TString[nPlanes];
  mPlaneName[A] = TString("A");
  mPlaneName[B] = TString("B");
  mPlaneName[C] = TString("C");
  mPlaneName[D] = TString("D");
  
  mStationName = new TString[nStations];
  mStationName[E1] = TString("E1");
  mStationName[E2] = TString("E2");
  mStationName[W1] = TString("W1");
  mStationName[W2] = TString("W2");
  
  mProtonsConfiguration = new TString[nProtonsConfigurations];
  mProtonsConfiguration[ALL] = TString("");
  mProtonsConfiguration[ELA] = TString("ET");
  mProtonsConfiguration[INE] = TString("IT");
  
  mParticleName = new TString[nDefinedParticles];
//   mParticleName[ELECTRON] = TString("electron");
//   mParticleName[MUON] = TString("muon");
  mParticleName[PION] = TString("pion");
  mParticleName[KAON] = TString("kaon");
  mParticleName[PROTON] = TString("proton");
  
  mTpcTrackTypeName = new TString[nTpcTrkTypes];
  mTpcTrackTypeName[GLO] = TString("global");
  mTpcTrackTypeName[PRI] = TString("primary");
  mTpcTrackTypeName[TOF] = TString("TofMatched");
  mTpcTrackTypeName[QUA] = TString("goodQuality");

  mBunchCrossingTypeName = new TString[nBnchXngsTypes];
  mBunchCrossingTypeName[CB] = TString("collidingBunches");
  mBunchCrossingTypeName[AG] = TString("abortGaps");
  
  mChargeSum2TrksName = new TString[nCharges2Trks];
  mChargeSum2TrksName[OPPO] = TString("oppositeSign");
  mChargeSum2TrksName[SAME] = TString("sameSign");
  
  mChargeSum4TrksName = new TString[nCharges4Trks];
  mChargeSum4TrksName[QSUM_ZERO] = TString("QSum0");
  mChargeSum4TrksName[QSUM_NON0] = TString("QSumNon0");

  mSignName = new TString[nSigns];
  mSignName[PLUS] = TString("Plus");
  mSignName[MINUS] = TString("Minus");
  
  mRpTrackCombinationName = new TString[nRpTrackTypeCombinations];
  mRpTrackCombinationName[LL] = TString("LL");
  mRpTrackCombinationName[LG] = TString("LG");
  mRpTrackCombinationName[GG] = TString("GG");
  
  mEfficiencyName = new TString[nEffCorrections];
  mEfficiencyName[RPACC] = TString("RpAcc");
  mEfficiencyName[TPCRECOEFF] = TString("TpcRecoEff");
  mEfficiencyName[TOFMATCHEFF] = TString("TofMatchEff");
  
  mCutName = new TString[nAnalysisCuts];
  mCutName[TRIG] = TString("Triggers");
  mCutName[TWORPTRKS] = TString("2 RP trks");
  mCutName[ONETOFVX] = TString("1 TOF vrtx");
  mCutName[ZVERTEX] = TString("|z_{vx}| < 100 cm");
  mCutName[TWOTOFTRKS] = TString("2 TOF-mtchd trks");
  mCutName[TWOQUATRKS] = TString("TPC trks quality");
  mCutName[OPPOSITE] = TString("Opposite-sign");
  mCutName[TPCRPVX_MATCHED] = TString("TPC-RP vrtx match");
  mCutName[BBCCLEAN] = TString("BBC-L clean");
  mCutName[TOFHITSRECO] = TString("2 N_{TOF}^{clusters}");
  mCutName[EXCLUSIVE] = TString("Exclusive (p_{T}^{miss} cut)");
  mCutName[PIPI] = TString("#pi^{+}#pi^{-}");
  mCutName[KK] = TString("K^{+}K^{-}");
  mCutName[PPBAR] = TString("p#bar{p}");

  mCutShortName = new TString[nAnalysisCuts];
  mCutShortName[TRIG] = TString("TRIG");
  mCutShortName[TWORPTRKS] = TString("TWORPTRKS");
  mCutShortName[ONETOFVX] = TString("ONETOFVX");
  mCutShortName[ZVERTEX] = TString("ZVERTEX");
  mCutShortName[TWOTOFTRKS] = TString("TWOTOFTRKS");
  mCutShortName[TWOQUATRKS] = TString("TWOQUATRKS");
  mCutShortName[OPPOSITE] = TString("OPPOSITE");
  mCutShortName[TPCRPVX_MATCHED] = TString("TPCRPVX_MATCHED");
  mCutShortName[BBCCLEAN] = TString("BBCCLEAN");
  mCutShortName[TOFHITSRECO] = TString("TOFHITSRECO");
  mCutShortName[EXCLUSIVE] = TString("EXCLUSIVE");
  mCutShortName[PIPI] = TString("PIPI");
  mCutShortName[KK] = TString("KK");
  mCutShortName[PPBAR] = TString("PPBAR");
  
  mReferenceFrameName = new TString[nReferenceFrames];
  mReferenceFrameName[GOTTFRIED_JACKSON] = TString("Gottfried-Jackson");
  mReferenceFrameName[HELICITY] = TString("Helicity");
  
  mReferenceFrameShortName = new TString[nReferenceFrames];
  mReferenceFrameShortName[GOTTFRIED_JACKSON] = TString("GJ");
  mReferenceFrameShortName[HELICITY] = TString("H");
  
  mMassRangeLimits[MASS_1][MIN] = 0.0;
  mMassRangeLimits[MASS_1][MAX] = 1.0;
  mMassRangeLimits[MASS_2][MIN] = 1.0;
  mMassRangeLimits[MASS_2][MAX] = 1.5;
  mMassRangeLimits[MASS_3][MIN] = 1.5;
  mMassRangeLimits[MASS_3][MAX] = std::numeric_limits<Double_t>::max();
  
//   mDeltaPhiLimits[DELTAPHI_1][MIN] = 0.0;
//   mDeltaPhiLimits[DELTAPHI_1][MAX] = 60.0;
//   mDeltaPhiLimits[DELTAPHI_2][MIN] = 60.0;
//   mDeltaPhiLimits[DELTAPHI_2][MAX] = 120.0;
//   mDeltaPhiLimits[DELTAPHI_3][MIN] = 120.0;
//   mDeltaPhiLimits[DELTAPHI_3][MAX] = 180.0;

  mDeltaPhiLimits[DELTAPHI_1][MIN] = 0.0;
  mDeltaPhiLimits[DELTAPHI_1][MAX] = 90.0;
  mDeltaPhiLimits[DELTAPHI_2][MIN] = 90.0;
  mDeltaPhiLimits[DELTAPHI_2][MAX] = 180.0;
  
  mPairPtRangeLimits[PAIRPT_1][MIN] = 0.0;
  mPairPtRangeLimits[PAIRPT_1][MAX] = 0.25;
  mPairPtRangeLimits[PAIRPT_2][MIN] = 0.25;
  mPairPtRangeLimits[PAIRPT_2][MAX] = 0.45;
  mPairPtRangeLimits[PAIRPT_3][MIN] = 0.45;
  mPairPtRangeLimits[PAIRPT_3][MAX] = std::numeric_limits<Double_t>::max();
  
  mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_1][MIN] = 0.0;
  mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_1][MAX] = 0.08;
  mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_2][MIN] = 0.08;
  mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_2][MAX] =  std::numeric_limits<Double_t>::max();
 
//   mParticleMass[ELECTRON] = 0.000511;
//   mParticleMass[MUON] = 0.10565837;
  mParticleMass[PION] = 0.13957018;
  mParticleMass[KAON] = 0.493667;
  mParticleMass[PROTON] = 0.93827208;
  
  mBranchPerRp[E1U] = EU;
  mBranchPerRp[E2U] = EU;
  mBranchPerRp[E1D] = ED;
  mBranchPerRp[E2D] = ED;
  mBranchPerRp[W1U] = WU;
  mBranchPerRp[W2U] = WU;
  mBranchPerRp[W1D] = WD;
  mBranchPerRp[W2D] = WD;
  
  mOppositeBranch[EU] = WD;
  mOppositeBranch[ED] = WU;
  mOppositeBranch[WU] = ED;
  mOppositeBranch[WD] = EU;
  
  mSidePerRp[E1U] = E;
  mSidePerRp[E2U] = E;
  mSidePerRp[E1D] = E;
  mSidePerRp[E2D] = E;
  mSidePerRp[W1U] = W;
  mSidePerRp[W2U] = W;
  mSidePerRp[W1D] = W;
  mSidePerRp[W2D] = W;
  
  mStationOrderPerRp[E1U] = RP1;
  mStationOrderPerRp[E2U] = RP2;
  mStationOrderPerRp[E1D] = RP1;
  mStationOrderPerRp[E2D] = RP2;
  mStationOrderPerRp[W1U] = RP1;
  mStationOrderPerRp[W2U] = RP2;
  mStationOrderPerRp[W1D] = RP1;
  mStationOrderPerRp[W2D] = RP2;
  
  mRpPerBranchStationOrder[EU][RP1] = E1U;
  mRpPerBranchStationOrder[EU][RP2] = E2U;
  mRpPerBranchStationOrder[ED][RP1] = E1D;
  mRpPerBranchStationOrder[ED][RP2] = E2D;
  mRpPerBranchStationOrder[WU][RP1] = W1U;
  mRpPerBranchStationOrder[WU][RP2] = W2U;
  mRpPerBranchStationOrder[WD][RP1] = W1D;
  mRpPerBranchStationOrder[WD][RP2] = W2D;
}

Util::~Util(){
  if(mSideName) delete [] mSideName;
  if(mTriggerName) delete [] mTriggerName;
  if(mTriggerBitLabel) delete [] mTriggerBitLabel;
  if(mArmName) delete [] mArmName;
  if(mBranchName) delete [] mBranchName;
  if(mRpName) delete [] mRpName;
  if(mPlaneName) delete [] mPlaneName;
  if(mStationName) delete [] mStationName;
  if(mProtonsConfiguration) delete [] mProtonsConfiguration;
  if(mParticleName) delete [] mParticleName;
  if(mTpcTrackTypeName) delete [] mTpcTrackTypeName;
  if(mBunchCrossingTypeName) delete [] mBunchCrossingTypeName;
  if(mChargeSum2TrksName) delete [] mChargeSum2TrksName;
  if(mChargeSum4TrksName) delete [] mChargeSum4TrksName;
  if(mSignName) delete [] mSignName;
  if(mRpTrackCombinationName) delete [] mRpTrackCombinationName;
  if(mEfficiencyName) delete [] mEfficiencyName;
  if(mCutName) delete [] mCutName;
  if(mCutShortName) delete [] mCutShortName;
  if(mReferenceFrameName) delete [] mReferenceFrameName;
  if(mReferenceFrameShortName) delete [] mReferenceFrameShortName;
  
}

Util* Util::instance(){
  if(!mInst) mInst = new Util();
  return mInst;
}

Util::MASS_RANGE Util::massRange(Double_t mass) const{
  for(int i=0; i<nMassRanges; ++i)
    if(mass>=mMassRangeLimits[i][MIN] && mass<mMassRangeLimits[i][MAX])
      return static_cast<MASS_RANGE>(i);
  std::cerr << "ERROR in Util::massRange(Double_t): argument not within expected limits!" << std::endl;
  return nMassRanges;
}

Util::DELTAPHI_RANGE Util::deltaPhiRange(Double_t deltaPhi) const{
  for(int i=0; i<nDeltaPhiRanges; ++i)
    if(deltaPhi>=mDeltaPhiLimits[i][MIN] && deltaPhi<mDeltaPhiLimits[i][MAX])
      return static_cast<DELTAPHI_RANGE>(i);
  std::cerr << "ERROR in Util::deltaPhiRange(Double_t): argument not within expected limits!" << std::endl;
  return nDeltaPhiRanges;
}
Util::PAIRPT_RANGE Util::pairPtRange(Double_t pairPt) const{
  for(int i=0; i<nPairPtRanges; ++i)
    if(pairPt>=mPairPtRangeLimits[i][MIN] && pairPt<mPairPtRangeLimits[i][MAX])
      return static_cast<PAIRPT_RANGE>(i);
  std::cerr << "ERROR in Util::pairPtRange(Double_t): argument not within expected limits!" << std::endl;
  return nPairPtRanges;
}

Util::MANDELSTAMTSINGLESIDE_RANGE Util::mandelstamTSingleSideRange(Double_t t) const{
  for(int i=0; i<nMandelstamTSingleSideRanges; ++i)
    if(t>=mMandelstamTRangeLimits[i][MIN] && t<mMandelstamTRangeLimits[i][MAX])
      return static_cast<MANDELSTAMTSINGLESIDE_RANGE>(i);
  std::cerr << "ERROR in Util::mandelstamTSingleSideRange(Double_t): argument not within expected limits!" << std::endl;
  return nMandelstamTSingleSideRanges;
}

Util::MANDELSTAMTBOTHSIDES_RANGE Util::mandelstamTBothSidesRange(Double_t t1, Double_t t2) const{
  if(t1>=mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_1][MIN] && t1<mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_1][MAX] && t2>=mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_1][MIN] && t2<mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_1][MAX])
    return MANDELSTAMTBOTHSIDES_1; else
  if((t1<t2?t1:t2)>=mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_1][MIN] && (t1<t2?t1:t2)<mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_1][MAX] && (t1<t2?t2:t1)>=mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_2][MIN] && (t1<t2?t2:t1)<mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_2][MAX])
    return MANDELSTAMTBOTHSIDES_2; else
  if(t1>=mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_2][MIN] && t1<mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_2][MAX] && t2>=mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_2][MIN] && t2<mMandelstamTRangeLimits[MANDELSTAMTSINGLESIDE_2][MAX])
    return MANDELSTAMTBOTHSIDES_3; else{
      std::cerr << "ERROR in Util::mandelstamTBothSidesRange(Double_t, Double_t): at least one argument not within expected limits!" << std::endl;
      return nMandelstamTBothSidesRanges;
  }
}

Double_t Util::bkgdFraction(const TH1* hPtMiss, Double_t ptMissCut, TF1 *funcReturn) const{
  static int funcId; ++funcId;
  TString idStr; idStr.Form("func%d", funcId);
  TF1* func = new TF1(idStr, "[0]*x+[1]*x*x", 0, 1);
  func->SetNpx(1e3);
  func->SetParameter(0, 1);
  func->SetParameter(1, -1);
  double bkgdFrac = 1e9;
  if(hPtMiss){
    const_cast<TH1*>(hPtMiss)->Fit( func, "MQN", "", 0.16, 0.2);
    double nBkgdEvents = func->Integral(0, ptMissCut) / hPtMiss->GetBinWidth(1);
    double nAllEvents = integratePtMiss(hPtMiss, ptMissCut);
    bkgdFrac = nBkgdEvents/nAllEvents;
  } else std::cerr << "ERROR in getBkgdFraction()" << std::endl;
  
  if(funcReturn) *funcReturn = *func;
  return bkgdFrac;
}


Double_t Util::integratePtMiss(const TH1* hPtMiss, Double_t ptMissCut) const{
  int binNum = 1; 
  for(int i=1; i<=hPtMiss->GetNbinsX(); ++i) if(hPtMiss->GetXaxis()->GetBinUpEdge(i) >= ptMissCut ){ binNum = i; break; }
  double integral = hPtMiss->Integral(1, binNum-1);
  integral += hPtMiss->GetBinContent(binNum) * ( ptMissCut - hPtMiss->GetXaxis()->GetBinLowEdge(binNum) ) / hPtMiss->GetXaxis()->GetBinWidth(binNum);
  return integral;
}
