// #define NO_CUT

#include <TH1.h>
#include <TH2.h>
#include <TFile.h>
#include <TTree.h>
#include "TMath.h"
#include <TLorentzVector.h>
#include <TVector2.h>
#include <cmath>
#include <iostream>
#include <fstream>
#include <TRandom3.h>
#include <TObjString.h>
#include <TObjArray.h>
#include "Util.hh"

using namespace std;

// Util *mUtil;

// double minPt[Util::nDefinedParticles];

const double minPt_1 = 0.10;
const double minPt_2 = 0.20;

// static const int n = 400000;
float totalLumi = 0;
int signalNEvents = 0;
int totalNEvents = 0;


int main(int argc, char *argv[]){
  
  TH1::SetDefaultSumw2();
//   mUtil = Util::instance();
  
  int mParticlesId = Util::nDefinedParticles;
  if( TString(argv[1])==TString("pipi") )
    mParticlesId = Util::PION;
  else
  if( TString(argv[1])==TString("kk") )
    mParticlesId = Util::KAON;

  TString path = argv[2];
    
  TString extraStr;
  if(argv[3])
    extraStr = argv[3];
  
  TString mcFileStr("exrec.dat");
  if(argv[4])
    mcFileStr = argv[4];
  
  TString logFileStr("log.txt");
//   if(argv[5])
//     logFileStr = argv[5];
    
  float pCentralParticle4Prong[4][3];
  float pForwardProton[2][3];
  totalLumi = 0;
  totalNEvents = 0;
  
  //..Output file for histograms
  
  TFile *fHistFile = new TFile(Form("DiMe_CepTree_%s"+(extraStr==""?extraStr:("_"+extraStr))+".root", argv[1]), "RECREATE");
  TTree *dimeCepTree = new TTree("CepTree", "Tree with DiMe events of CEP");
  
  dimeCepTree->Branch("pid", &mParticlesId, "pid/I" );
  dimeCepTree->Branch("luminosity", &totalLumi, "luminosity/F" );
  
  dimeCepTree->Branch("pxMinus1", &(pCentralParticle4Prong[0][0]), "pxMinus1/F" );
  dimeCepTree->Branch("pyMinus1", &(pCentralParticle4Prong[0][1]), "pyMinus1/F" );
  dimeCepTree->Branch("pzMinus1", &(pCentralParticle4Prong[0][2]), "pzMinus1/F" );
  dimeCepTree->Branch("pxMinus2", &(pCentralParticle4Prong[1][0]), "pxMinus2/F" );
  dimeCepTree->Branch("pyMinus2", &(pCentralParticle4Prong[1][1]), "pyMinus2/F" );
  dimeCepTree->Branch("pzMinus2", &(pCentralParticle4Prong[1][2]), "pzMinus2/F" );
  dimeCepTree->Branch("pxPlus1", &(pCentralParticle4Prong[2][0]), "pxPlus1/F" );
  dimeCepTree->Branch("pyPlus1", &(pCentralParticle4Prong[2][1]), "pyPlus1/F" );
  dimeCepTree->Branch("pzPlus1", &(pCentralParticle4Prong[2][2]), "pzPlus1/F" );
  dimeCepTree->Branch("pxPlus2", &(pCentralParticle4Prong[3][0]), "pxPlus2/F" );
  dimeCepTree->Branch("pyPlus2", &(pCentralParticle4Prong[3][1]), "pyPlus2/F" );
  dimeCepTree->Branch("pzPlus2", &(pCentralParticle4Prong[3][2]), "pzPlus2/F" );
  
  dimeCepTree->Branch("pxProton1", &(pForwardProton[0][0]), "pxProton1/F" );
  dimeCepTree->Branch("pxProton2", &(pForwardProton[1][0]), "pxProton2/F" );
  dimeCepTree->Branch("pyProton1", &(pForwardProton[0][1]), "pyProton1/F" );
  dimeCepTree->Branch("pyProton2", &(pForwardProton[1][1]), "pyProton2/F" );
  dimeCepTree->Branch("pzProton1", &(pForwardProton[0][2]), "pzProton1/F" );
  dimeCepTree->Branch("pzProton2", &(pForwardProton[1][2]), "pzProton2/F" );
  
  const double pionMass = mParticlesId==Util::PION ? 0.13957 : 0.493677;
  const double protonMass = 0.938272;
//   const double weightPid = 1.0;
  
  Int_t           ntrack;
  Int_t           evt;
  Int_t           id[4];  
  Int_t           dummy_i;
  Double_t        dummy_d; 
  Double_t        px[4];   
  Double_t        py[4];   
  Double_t        pz[4];   
  Double_t        e[4];   
  Double_t        Prpx[2];
  Double_t        Prpy[2];
  Double_t        Prpz[2];
  Double_t        Pre[2];
  
  
  
  TH2F *forwardProtonPyVsPx[2];
  TH1F *pionEta[2];
  TH1F *pionPt[2];
  
  forwardProtonPyVsPx[0] = new TH2F("forwardProtonPyVsPx_EAST", "p_{y} vs. p_{x}", 120, -0.6, 0.6, 120, -0.6, 0.6);
  forwardProtonPyVsPx[1] = new TH2F("forwardProtonPyVsPx_WEST", "p_{y} vs. p_{x}", 120, -0.6, 0.6, 120, -0.6, 0.6);
  
  pionEta[0] = new TH1F("pionEta_PLUS", "#eta of pion", 100, -2.5, 2.5);
  pionEta[1] = new TH1F("pionEta_MINUS", "#eta of pion", 100, -2.5, 2.5);
  
  pionPt[0] = new TH1F("pionPt_PLUS", "p_{T} of pion", 30, 0, 1.5);
  pionPt[1] = new TH1F("pionPt_MINUS", "p_{T} of pion", 30, 0, 1.5);
  
  ifstream data(path+"/"+mcFileStr,ifstream::in);
  
  const double maxEta = 2.5;
//   const double initialMomentum = 6500.;
//   minPt[Util::PION] = 0.0;
//   minPt[Util::KAON] = 0.1;
//   minPt[Util::PROTON] = 0.3;
  
    while( !data.eof() ){
        data >> evt >> ntrack;
        
        data >> dummy_i >> dummy_i >> dummy_i >> dummy_i >> dummy_i >> dummy_i >> dummy_i >>
        dummy_d >> dummy_d >> dummy_d >> dummy_d >> dummy_d >> dummy_d >> dummy_d >> dummy_d >> dummy_d;
        
        data >> dummy_i >> dummy_i >> dummy_i >> dummy_i >> dummy_i >> dummy_i >> dummy_i >>
        dummy_d >> dummy_d >> dummy_d >> dummy_d >> dummy_d >> dummy_d >> dummy_d >> dummy_d >> dummy_d;
        
        data >> dummy_i >> dummy_i >> dummy_i >> dummy_i >> dummy_i >> dummy_i >> dummy_i >>
        Prpx[0] >> Prpy[0] >> Prpz[0] >>  Pre[0] >> dummy_d >> dummy_d >> dummy_d >> dummy_d >> dummy_d;
        
        data >> dummy_i >> dummy_i >> dummy_i >> dummy_i >> dummy_i >> dummy_i >> dummy_i >>
        Prpx[1] >> Prpy[1] >> Prpz[1] >>  Pre[1] >> dummy_d >> dummy_d >> dummy_d >> dummy_d >> dummy_d;
        
        data >> dummy_i >> dummy_i >> dummy_i >> dummy_i >> dummy_i >> dummy_i >> dummy_i >>
        dummy_d >> dummy_d >> dummy_d >>  dummy_d >> dummy_d >> dummy_d >> dummy_d >> dummy_d >> dummy_d;
        
        data >> dummy_i >> dummy_i >> dummy_i >> dummy_i >> dummy_i >> dummy_i >> dummy_i >>
        dummy_d >> dummy_d >> dummy_d >>  dummy_d >> dummy_d >> dummy_d >> dummy_d >> dummy_d >> dummy_d;
        
        data >> dummy_i >> id[0] >> dummy_i >> dummy_i >> dummy_i >> dummy_i >> dummy_i >>
        px[0] >> py[0] >> pz[0] >>  e[0] >> dummy_d >> dummy_d >> dummy_d >> dummy_d >> dummy_d;
        
        data >> dummy_i >> id[1] >> dummy_i >> dummy_i >> dummy_i >> dummy_i >> dummy_i >>
        px[1] >> py[1] >> pz[1] >>  e[1] >> dummy_d >> dummy_d >> dummy_d >> dummy_d >> dummy_d;
        
        data >> dummy_i >> id[2] >> dummy_i >> dummy_i >> dummy_i >> dummy_i >> dummy_i >>
        px[2] >> py[2] >> pz[2] >>  e[2] >> dummy_d >> dummy_d >> dummy_d >> dummy_d >> dummy_d;
        
        data >> dummy_i >> id[3] >> dummy_i >> dummy_i >> dummy_i >> dummy_i >> dummy_i >>
        px[3] >> py[3] >> pz[3] >>  e[3] >> dummy_d >> dummy_d >> dummy_d >> dummy_d >> dummy_d;

        
        //if(totalNEvents%100==0) cout << evt<< " " << id[0] << " " <<  px[0] << " " << py[0] << " " << pz[0] << " " << id[1] << " " << px[1] << " " << py[1] << " " << pz[1] << " " <<  Prpx[0] << " " << Prpy[0] << " " << Prpz[0]  << " " << Prpx[1] << " " << Prpy[1] << " " << Prpz[1] << endl;
        
        double proton_px[2], proton_py[2], proton_pz[2];
        TLorentzVector pion4Vec[4];
        TLorentzVector proton4Vec[2];
        
        if(Prpz[0] > 0){
          proton_px[1] = Prpx[0];
          proton_py[1] = Prpy[0];
          proton_pz[1] = Prpz[0];
          proton_px[0] = Prpx[1];
          proton_py[0] = Prpy[1];
          proton_pz[0] = Prpz[1];
          
            pForwardProton[0][0] = static_cast<float>(Prpx[0]);
            pForwardProton[0][1] = static_cast<float>(Prpy[0]);
            pForwardProton[0][2] = static_cast<float>(Prpz[0]);
            pForwardProton[1][0] = static_cast<float>(Prpx[1]);
            pForwardProton[1][1] = static_cast<float>(Prpy[1]);
            pForwardProton[1][2] = static_cast<float>(Prpz[1]);
        } else  {
          proton_px[1] = Prpx[1];
          proton_py[1] = Prpy[1];
          proton_pz[1] = Prpz[1];
          proton_px[0] = Prpx[0];
          proton_py[0] = Prpy[0];
          proton_pz[0] = Prpz[0];
            
            pForwardProton[1][0] = static_cast<float>(Prpx[0]);
            pForwardProton[1][1] = static_cast<float>(Prpy[0]);
            pForwardProton[1][2] = static_cast<float>(Prpz[0]);
            pForwardProton[0][0] = static_cast<float>(Prpx[1]);
            pForwardProton[0][1] = static_cast<float>(Prpy[1]);
            pForwardProton[0][2] = static_cast<float>(Prpz[1]);
        }
        
        for(int i=0; i<Util::nSides; ++i)
          proton4Vec[i] = TLorentzVector( proton_px[i], proton_py[i], proton_pz[i], sqrt( proton_px[i]*proton_px[i] + proton_py[i]*proton_py[i] + proton_pz[i]*proton_pz[i] + protonMass*protonMass ) );
        
        
        pion4Vec[0] = TLorentzVector( px[0], py[0], pz[0], sqrt(px[0]*px[0]+py[0]*py[0]+pz[0]*pz[0] + pionMass*pionMass) );
        pion4Vec[1] = TLorentzVector( px[1], py[1], pz[1], sqrt(px[1]*px[1]+py[1]*py[1]+pz[1]*pz[1] + pionMass*pionMass) );
        pion4Vec[2] = TLorentzVector( px[2], py[2], pz[2], sqrt(px[2]*px[2]+py[2]*py[2]+pz[2]*pz[2] + pionMass*pionMass) );
        pion4Vec[3] = TLorentzVector( px[3], py[3], pz[3], sqrt(px[3]*px[3]+py[3]*py[3]+pz[3]*pz[3] + pionMass*pionMass) );
        
        pCentralParticle4Prong[0][0] = static_cast<float>(px[3]);
        pCentralParticle4Prong[0][1] = static_cast<float>(py[3]);
        pCentralParticle4Prong[0][2] = static_cast<float>(pz[3]);
        pCentralParticle4Prong[1][0] = static_cast<float>(px[1]);
        pCentralParticle4Prong[1][1] = static_cast<float>(py[1]);
        pCentralParticle4Prong[1][2] = static_cast<float>(pz[1]);
        pCentralParticle4Prong[2][0] = static_cast<float>(px[2]);
        pCentralParticle4Prong[2][1] = static_cast<float>(py[2]);
        pCentralParticle4Prong[2][2] = static_cast<float>(pz[2]);
        pCentralParticle4Prong[3][0] = static_cast<float>(px[0]);
        pCentralParticle4Prong[3][1] = static_cast<float>(py[0]);
        pCentralParticle4Prong[3][2] = static_cast<float>(pz[0]);
        
        
        
        if( 
          #ifdef NO_CUT
            true
          #else
            pion4Vec[0].Pt() > minPt_1 && pion4Vec[1].Pt() > minPt_1 && (pion4Vec[0].Pt() > minPt_2 || pion4Vec[1].Pt() > minPt_2) &&
            fabs(pion4Vec[0].Eta()) < maxEta && fabs(pion4Vec[1].Eta()) < maxEta &&
            fabs(proton_py[0]) > 0.17 && fabs(proton_py[0]) < 0.5 &&
            fabs(proton_py[1]) > 0.17 && fabs(proton_py[1]) < 0.5
          #endif
        ){
          
          ++signalNEvents;
          dimeCepTree->Fill();
          
          for(int j=0; j<2; ++j)
            forwardProtonPyVsPx[j]->Fill( proton_px[j], proton_py[j] );
          
          for(int j=0; j<4; ++j){
            pionEta[j%2]->Fill( pion4Vec[j].Eta() );
            pionPt[j%2]->Fill( pion4Vec[j].Pt() );
          }
          
//           const double mMass = (pion4Vec[0] + pion4Vec[1]).M();
//           const double mPairPt = (pion4Vec[0] + pion4Vec[1]).Pt();
//           const double mPairRapidity = (pion4Vec[0] + pion4Vec[1]).Rapidity();
//           
//           TVector2 v1(proton_px[0], proton_py[0]);
//           TVector2 v2(proton_px[1], proton_py[1]);
//           double mDeltaPhiDegrees = acos(v1.Unit()*v2.Unit()) * 180. / Util::PI;
//           
//           double t[Util::nSides];
//           for(int side=0; side<Util::nSides; ++side)
//             t[side] = -(proton4Vec[side] - TLorentzVector(0,0, (side==0 ? (-1) : 1)*initialMomentum, sqrt(initialMomentum*initialMomentum + protonMass*protonMass) )).Mag2();
          
        }
        ++totalNEvents;
    }

    ifstream lumiInfo(path+"/"+logFileStr, ifstream::in);
    
    bool lookForLumi = false;
    
do{
        char line[1024];
        lumiInfo.getline(line, 1024);
        
        TString lineStr(line);
        
        if( lookForLumi ){
            if( lineStr.Contains("sigma (nb)") ){
                TObjArray *tokenizedLine = lineStr.Tokenize(":");
                TString lumiStr = ((TObjString *)(tokenizedLine->At(1)))->String();
                TObjArray *tokenizedLumi = lumiStr.Tokenize("+-");
                lumiStr = ((TObjString *)(tokenizedLumi->At(0)))->String();
                double crossSec = atof( lumiStr.Data() );
                
                mParticlesId = Util::nDefinedParticles;
                totalLumi = totalNEvents / crossSec;
                
                dimeCepTree->Fill();
            }
        }
        
        if( lineStr.Contains("HEPEVT output") )
            lookForLumi = true;
        
    } while( !lumiInfo.eof() );
    
//   fHistFile->cd();
  fHistFile->Write();
  fHistFile->Close();
}
