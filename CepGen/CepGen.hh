#ifndef CepGen_hh
#define CepGen_hh

#include <vector>
#include <TROOT.h>
#include <TFile.h>
#include <TString.h>
#include "../CrossSectionAnalysis/Util.hh"

class TH2;
class TH1F;
class TH2F;
class TGraphAsymmErrors;
class TLine;
class TVector3;
class TF2;
class TLorentzVector;
class TRandom3;

class CepGen{
public:

  CepGen(TString opt="");
  ~CepGen();

  void run();
  
  enum WAVES_ENUM { S0m_MAG, P0m_MAG, P0m_PH, P1m_MAG, P1m_PH, D0m_MAG, D0m_PH, D1m_MAG, D1m_PH,
                    P1p_MAG, D1p_MAG, D1p_PH,      nWaveParameters };
  double intensityFromWaves(const double *x, const double *par) const;
  
private:
  
  static const double mSlopeB;
  static const double mPion;
  static const double mProton;
  static const double mSqrtS;
  static const double PI;
  static const double mMinT;
  static const double mMaxT;
  static const double mMaxY;
  static const double mBeamP;
  
  enum PARAMS_ENUM { PAR_t1, PAR_t2, PAR_phi1, PAR_phi2, PAR_m, PAR_y, nParams };
  enum PARAMS2_ENUM { PAR_px1, PAR_py1, PAR_pz1, PAR_px2, PAR_py2, PAR_pz2 };

  double t1Func(const double *x, const double *p ) const;
  double t2Func(const double *x, const double *p ) const;
  double phi1Func(const double *x, const double *p ) const;
  double phi2Func(const double *x, const double *p ) const;
  double mFunc(const double *x, const double *p ) const;
  double yFunc(const double *x, const double *p ) const;
  
  void transformCentralTrksToWave(TLorentzVector &trackPlus, TLorentzVector &trackMinus, TLorentzVector protonA, TLorentzVector protonB, TF2 * const intensityFunc) const;
  
  bool isWithinFiducialRegion(const TVector3 & piPlus, const TVector3 & piMinus, const TVector3 & protonA, const TVector3 & protonC) const;
  bool isForwardProtonWithinFiducialRegion(const TVector3 & protonA, const TVector3 & protonC) const;
  bool isCentralTrackWithinFiducialRegion(const TVector3 &, const TVector3 & ) const;
  void setupDrawingStyle() const;
  
  Bool_t mLoadAccaptance;
  
  TRandom3 *mGen;
  TH1F* mDSigmaDm;
  
  Util *mUtil;
   
  ClassDef(CepGen,0);
};

#endif
