#define CepGen_cxx

#include "CepGen.hh"
#include <TH1.h> 
#include <TH2.h> 
#include <TF1.h> 
#include <TF2.h> 
#include <TVector3.h> 
#include <THStack.h> 
#include <TStyle.h> 
#include <TMultiGraph.h> 
#include <TGraph.h> 
#include <TGraph2D.h> 
#include <TGraphAsymmErrors.h> 
#include <TEfficiency.h> 
#include <TFractionFitter.h>
#include <TObjArray.h>
#include <iostream>
#include <utility>
#include <sstream> 
#include <algorithm> 
#include <stdio.h> 
#include <stdlib.h> 
#include <TCanvas.h> 
#include <TNamed.h>
#include <TLegend.h> 
#include <TBox.h>
#include <TGaxis.h> 
#include <vector> 
#include <fstream> 
#include <TString.h> 
#include <TColor.h> 
#include <TLine.h> 
#include <cmath> 
#include <TExec.h>
#include <TEllipse.h>
#include <TFitResultPtr.h> 
#include <TFitResult.h> 
#include <TLatex.h> 
#include <TExec.h>
#include <TMath.h>
#include <TArrow.h>
#include <TPaletteAxis.h>
#include <TRandom3.h>
#include <TLorentzVector.h>
#include <TLorentzRotation.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include "Math/DistFunc.h"
#include "Math/SpecFunc.h"
#include "Math/IntegratorMultiDim.h"
#include "Math/WrappedMultiTF1.h"
#include "Math/Functor.h"
#include <Math/Factory.h>
#include <Math/Minimizer.h>
#include "Math/MultiRootFinder.h"
#include <complex>

ClassImp(CepGen)

const double CepGen::mSlopeB = 6.0;
const double CepGen::mPion = 0.13957;
const double CepGen::mProton = 0.9383;
const double CepGen::mSqrtS = 13e3;
const double CepGen::PI = TMath::Pi();
// const double CepGen::mMinT = 0.18*0.18;
// const double CepGen::mMaxT = 0.22;
const double CepGen::mMinT = 0.01;
const double CepGen::mMaxT = 0.5;
const double CepGen::mMaxY = 2.0;
// const double CepGen::mMaxY = 0.65;
const double CepGen::mBeamP = sqrt(mSqrtS*mSqrtS/4. - mProton*mProton);


CepGen::CepGen(TString option){
  mUtil = Util::instance(option);
  
  mGen = new TRandom3(0);
  
//   mLoadAccaptance = kTRUE;
  mLoadAccaptance = kFALSE;
  
  setupDrawingStyle();
}

CepGen::~CepGen(){
}

void CepGen::run(){
  
  
  int nMassBins[Util::nDefinedParticles];
  nMassBins[Util::PION] = 250;
  nMassBins[Util::KAON] = 100;
  nMassBins[Util::PROTON] = 25;
  
    //   special binning
  //KK
  double lowMassThreshold = 2*mUtil->mass(Util::KAON);
  double massBinWidth = 5.0 / nMassBins[Util::KAON];
  vector<double> kaonMassBinsVec;
  kaonMassBinsVec.push_back( lowMassThreshold );
  int index = 0;
  double massBinBoundary = 0;
  while( massBinBoundary < 2 ){
    ++index;
    massBinBoundary = 1.0 + massBinWidth*index;
    kaonMassBinsVec.push_back( massBinBoundary );
  }
  index = 0;
  massBinWidth = 0.1;
  while( massBinBoundary < 5 ){
    ++index;
    massBinBoundary = 2.0 + massBinWidth*index;
    kaonMassBinsVec.push_back( massBinBoundary );
  }
  
  // ppbar
  lowMassThreshold = 2*mUtil->mass(Util::PROTON);
  vector<double> protonMassBinsVec;
  protonMassBinsVec.push_back( lowMassThreshold );
  protonMassBinsVec.push_back( 2.0 );
  protonMassBinsVec.push_back( 2.2 );
  protonMassBinsVec.push_back( 2.4 );
  protonMassBinsVec.push_back( 2.6 );
  protonMassBinsVec.push_back( 2.8 );
  protonMassBinsVec.push_back( 3.0 );
  protonMassBinsVec.push_back( 3.2 );
  protonMassBinsVec.push_back( 3.4 );
  protonMassBinsVec.push_back( 3.6 );
  protonMassBinsVec.push_back( 3.8 );
  
  // pipi
  lowMassThreshold = 2*mUtil->mass(Util::PION);
  vector<double> pionMassBinsVec;
  pionMassBinsVec.push_back( lowMassThreshold );
  massBinBoundary = 0;
  index = 0;
  massBinWidth = 0.02;
  while( massBinBoundary < 0.7 ){
    ++index;
    massBinBoundary = 0.3 + massBinWidth*index;
    pionMassBinsVec.push_back( massBinBoundary );
  }
  index = 0;
  massBinWidth = 0.03;
  while( massBinBoundary < 1.2999 ){
    ++index;
    massBinBoundary = 0.7 + massBinWidth*index;
    pionMassBinsVec.push_back( massBinBoundary );
  }
  index = 0;
  massBinWidth = 0.05;
  while( massBinBoundary < 5.0 ){
    ++index;
    massBinBoundary = 1.3 + massBinWidth*index;
    pionMassBinsVec.push_back( massBinBoundary );
  }
  
  
  index = -1;
  double missingPtBinBoundary = 0;
  const double missingPtBinWidth = 1.0 / 100;
  vector<double> missingPtBinsVec;
  while( missingPtBinBoundary < 1 ){
    ++index;
    missingPtBinBoundary = missingPtBinWidth*index;
    missingPtBinsVec.push_back( missingPtBinBoundary );
  }
  
  
  
  index = -1;
  missingPtBinBoundary = 0;
  const double narrowWissingPtBinWidth = 0.5 / 200;
  vector<double> narrowMissingPtBinsVec;
  while( missingPtBinBoundary < 0.5 ){
    ++index;
    missingPtBinBoundary = narrowWissingPtBinWidth*index;
    narrowMissingPtBinsVec.push_back( missingPtBinBoundary );
  }
  
  
  //---------------------------------- ---------------------------------- ---------------------------------- ----------------------------------
  const int nEvts = 2e7;
  const Util::PARTICLE_NAME PID = Util::PION;
//   const Util::PARTICLE_NAME PID = Util::KAON;
//   const Util::PARTICLE_NAME PID = Util::PROTON;
  const double particleMass = mUtil->mass(PID);
  
  TEfficiency *mMassAcceptance;
  
  TEfficiency *mMassAcceptance_ForwardProtonsInFiducialRegion_S0m;
  TEfficiency *mMassAcceptance_ForwardProtonsInFiducialRegion_D0m;
  
  TEfficiency *mMassAcceptance_DeltaPhiNarrowerBin1;
  TEfficiency *mMassAcceptance_DeltaPhiNarrowerBin2;
  
  TEfficiency *mMassAcceptance_LimitedMandelstamT_S0m[Util::nDeltaPhiRanges];
  TEfficiency *mMassAcceptance_LimitedMandelstamT_D0m[Util::nDeltaPhiRanges];
  
  TEfficiency *mAcceptanceForDeltaPhiCuts_Vs_t1t2[4];
  TH2F *mAcceptanceForDeltaPhiCuts_Vs_t1t2_PASSED[4];
  TH2F *mAcceptanceForDeltaPhiCuts_Vs_t1t2_TOTAL[4];
  
  TFile *inputFile;
  
  if( mLoadAccaptance ){
    inputFile = new TFile( "ROOT_FILES/"+mUtil->particleName(PID)+".root", "READ" );
    mMassAcceptance = dynamic_cast<TEfficiency*>( inputFile->Get("mMassAcceptance_" + mUtil->particleName(PID)) );
    mMassAcceptance->SetDirectory(0);
    inputFile->Close();
  }
  
  inputFile = new TFile( "ROOT_FILES/PaperFigures.root", "READ" );
  mDSigmaDm = dynamic_cast<TH1F*>( inputFile->Get("dsdm_"+mUtil->particleName(PID)) );
  mDSigmaDm->SetDirectory(0);
  
  for(int i=0; i<=mDSigmaDm->GetNbinsX()+1; ++i){
    double acc = mLoadAccaptance ? mMassAcceptance->GetEfficiency( mMassAcceptance->FindFixBin( mDSigmaDm->GetXaxis()->GetBinCenter(i) ) ) : 1.0;
    mDSigmaDm->SetBinContent( i, mDSigmaDm->GetBinContent(i) * mDSigmaDm->GetXaxis()->GetBinWidth(i) / acc );
  }
  inputFile->Close();
  
  
//   inputFile = new TFile( "massAccForFit.root", "READ" );
  TEfficiency *input_mMassAcceptance_LimitedMandelstamT_S0m[Util::nDeltaPhiRanges];
  TEfficiency *input_mMassAcceptance_LimitedMandelstamT_D0m[Util::nDeltaPhiRanges];
  TEfficiency *input_mAcceptanceForDeltaPhiCuts_Vs_t1t2[Util::nDeltaPhiRanges];
//   for(int i=0; i<Util::nDeltaPhiRanges; ++i){
//     input_mMassAcceptance_LimitedMandelstamT_S0m[i] = dynamic_cast<TEfficiency*>( inputFile->Get(Form("mMassAcceptance_LimitedMandelstamT_DeltaPhiNarrowerBin%d_S0m", i+1)) );
//     input_mMassAcceptance_LimitedMandelstamT_S0m[i]->SetDirectory(0);
//     input_mMassAcceptance_LimitedMandelstamT_S0m[i]->SetName( input_mMassAcceptance_LimitedMandelstamT_S0m[i]->GetName()+TString("_input") );
//     input_mMassAcceptance_LimitedMandelstamT_D0m[i] = dynamic_cast<TEfficiency*>( inputFile->Get(Form("mMassAcceptance_LimitedMandelstamT_DeltaPhiNarrowerBin%d_D0m", i+1)) );
//     input_mMassAcceptance_LimitedMandelstamT_D0m[i]->SetDirectory(0);
//     input_mMassAcceptance_LimitedMandelstamT_D0m[i]->SetName( input_mMassAcceptance_LimitedMandelstamT_D0m[i]->GetName()+TString("_input") );
//     input_mAcceptanceForDeltaPhiCuts_Vs_t1t2[i] = dynamic_cast<TEfficiency*>( inputFile->Get("mAcceptanceForDeltaPhiCuts_Vs_t1t2"+TString(Form("_deltaPhiBin_%d", i+1))) );
//     input_mAcceptanceForDeltaPhiCuts_Vs_t1t2[i]->SetDirectory(0);
//   }
//   inputFile->Close();
  
  
  TF1* massFunc = new TF1("massFunc", "[0]*TMath::Power(x-2*0.938, [1]) * TMath::Exp(-[2]*x-[3]*x*x)", 2*particleMass, 10);
  mDSigmaDm->Fit( massFunc );
  
 //-------------------
  
  TFile *outputFile = new TFile( Form("CepMC_output_%d.root", static_cast<int>(10000*mGen->Uniform())), "RECREATE" );
 
  TH1F* mhInvMassPid[Util::nCharges2Trks][Util::nDefinedParticles];
  TH1F* mhMissingPtPid[Util::nCharges2Trks][Util::nDefinedParticles];
  
  for(int i=0; i<Util::nCharges2Trks; ++i)
    mhMissingPtPid[i][PID] = new TH1F("MissingPtPid_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(PID), "Missing p_{T}, "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(PID), 600, 0, 3);
  
  for(int i=0; i<Util::nCharges2Trks; ++i)
    if( PID==Util::KAON )
        mhInvMassPid[i][PID] = new TH1F("InvMassPid_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(PID), "Inv. mass, "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(PID), kaonMassBinsVec.size()-1, &(kaonMassBinsVec[0]));
      else if( PID==Util::PROTON )
        mhInvMassPid[i][PID] = new TH1F("InvMassPid_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(PID), "Inv. mass, "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(PID), protonMassBinsVec.size()-1, &(protonMassBinsVec[0]));
      else if( PID==Util::PION )
        mhInvMassPid[i][PID] = new TH1F("InvMassPid_"+mUtil->qSum2TrksName(i)+"_"+mUtil->particleName(PID), "Inv. mass, "+mUtil->qSum2TrksName(i)+", "+mUtil->particleName(PID), pionMassBinsVec.size()-1, &(pionMassBinsVec[0]));
  

  if( !mLoadAccaptance ){
    if( PID==Util::KAON )
      mMassAcceptance = new TEfficiency("mMassAcceptance_kaon", "mMassAcceptance", kaonMassBinsVec.size()-1, &(kaonMassBinsVec[0]));
    else if( PID==Util::PROTON )
      mMassAcceptance = new TEfficiency("mMassAcceptance_proton", "mMassAcceptance", protonMassBinsVec.size()-1, &(protonMassBinsVec[0]));
    else if( PID==Util::PION )
      mMassAcceptance = new TEfficiency("mMassAcceptance_pion", "mMassAcceptance", pionMassBinsVec.size()-1, &(pionMassBinsVec[0]));
  }
  
  
  mMassAcceptance_ForwardProtonsInFiducialRegion_S0m = new TEfficiency("mMassAcceptance_ForwardProtonsInFiducialRegion_S0m", "mMassAcceptance_ForwardProtonsInFiducialRegion_S0m", 300, 0, 3);
  mMassAcceptance_ForwardProtonsInFiducialRegion_D0m = new TEfficiency("mMassAcceptance_ForwardProtonsInFiducialRegion_D0m", "mMassAcceptance_ForwardProtonsInFiducialRegion_D0m", 300, 0, 3);
  
  mMassAcceptance_DeltaPhiNarrowerBin1 = new TEfficiency("mMassAcceptance_DeltaPhiNarrowerBin1", "mMassAcceptance_DeltaPhiNarrowerBin1", 200, 0, 2);
  mMassAcceptance_DeltaPhiNarrowerBin2 = new TEfficiency("mMassAcceptance_DeltaPhiNarrowerBin2", "mMassAcceptance_DeltaPhiNarrowerBin2", 200, 0, 2);
  
  for(int i=0; i<Util::nDeltaPhiRanges; ++i){
    mMassAcceptance_LimitedMandelstamT_S0m[i] = new TEfficiency(Form("mMassAcceptance_LimitedMandelstamT_DeltaPhiNarrowerBin%d_S0m", i+1), Form("mMassAcceptance_LimitedMandelstamT_DeltaPhiNarrowerBin%d_S0m", i+1), 200, 0, 2);
    mMassAcceptance_LimitedMandelstamT_D0m[i] = new TEfficiency(Form("mMassAcceptance_LimitedMandelstamT_DeltaPhiNarrowerBin%d_D0m", i+1), Form("mMassAcceptance_LimitedMandelstamT_DeltaPhiNarrowerBin%d_D0m", i+1), 200, 0, 2);
  }
  
  for(int i=0; i<4; ++i){
    mAcceptanceForDeltaPhiCuts_Vs_t1t2[i] = new TEfficiency("mAcceptanceForDeltaPhiCuts_Vs_t1t2"+TString(Form("_deltaPhiBin_%d", i+1)), "mAcceptanceForDeltaPhiCuts_Vs_t1t2"+TString(Form("_deltaPhiBin_%d", i+1)), 50, 0, 0.5, 50, 0, 0.5);
    mAcceptanceForDeltaPhiCuts_Vs_t1t2_PASSED[i] = new TH2F("mAcceptanceForDeltaPhiCuts_Vs_t1t2_PASSED"+TString(Form("_deltaPhiBin_%d", i+1)), "mAcceptanceForDeltaPhiCuts_Vs_t1t2_PASSED"+TString(Form("_deltaPhiBin_%d", i+1)), 50, 0, 0.5, 50, 0, 0.5);
    mAcceptanceForDeltaPhiCuts_Vs_t1t2_TOTAL[i] = new TH2F("mAcceptanceForDeltaPhiCuts_Vs_t1t2_TOTAL"+TString(Form("_deltaPhiBin_%d", i+1)), "mAcceptanceForDeltaPhiCuts_Vs_t1t2_TOTAL"+TString(Form("_deltaPhiBin_%d", i+1)), 50, 0, 0.5, 50, 0, 0.5);
  }
  
//   TH2F *mhPmaxPMin_PtMissAllPassed[2];
//   mhPmaxPMin_PtMissAllPassed[0] = new TH2F("PmaxPMin_PtMissAll", "PmaxPMin_PtMissAll", 30, 0, 3, 30, 0, 3);
//   mhPmaxPMin_PtMissAllPassed[1] = new TH2F("PmaxPMin_PtMissPassed", "PmaxPMin_PtMissPassed", 30, 0, 3, 30, 0, 3);
  
  
  TH1F *mhInvMass_True = new TH1F("mhInvMass_True", "mhInvMass_True", 200, 0, 2);
  TH1F *mhInvMass_Reco_S0m_Corrected = new TH1F("mhInvMass_Reco_S0m_Corrected", "mhInvMass_Reco_S0m_Corrected", 200, 0, 2);
  TH1F *mhInvMass_Reco_D0m_Corrected = new TH1F("mhInvMass_Reco_D0m_Corrected", "mhInvMass_Reco_D0m_Corrected", 200, 0, 2);
  TH1F *mhInvMass_True_DeltaPhiBins[Util::nDeltaPhiRanges];
  TH1F *mhInvMass_Reco_S0m_Corrected_DeltaPhiBins[Util::nDeltaPhiRanges];
  TH1F *mhInvMass_Reco_D0m_Corrected_DeltaPhiBins[Util::nDeltaPhiRanges];
  for(int i=0; i<Util::nDeltaPhiRanges; ++i){
    mhInvMass_True_DeltaPhiBins[i] = new TH1F(Form("mhInvMass_True_DeltaPhiBin_%d", i+1), "mhInvMass_True", 200, 0, 2);
    mhInvMass_Reco_S0m_Corrected_DeltaPhiBins[i] = new TH1F(Form("mhInvMass_Reco_S0m_Corrected_DeltaPhiBin_%d", i+1), "mhInvMass_Reco_S0m_Corrected", 200, 0, 2);
    mhInvMass_Reco_D0m_Corrected_DeltaPhiBins[i] = new TH1F(Form("mhInvMass_Reco_D0m_Corrected_DeltaPhiBin_%d", i+1), "mhInvMass_Reco_D0m_Corrected", 200, 0, 2);
  }
  
  // ------- ------- ------- -------
  TF2 *angularDist_D0m = new TF2("angularDist_D0m", this, &CepGen::intensityFromWaves, -TMath::Pi(), TMath::Pi(), 0, TMath::Pi(), 12);
  angularDist_D0m->SetNpx(5e2); angularDist_D0m->SetNpy(5e2);
  for(int i=0; i<nWaveParameters; ++i)
    angularDist_D0m->FixParameter(i, 0.0);
  //D-wave
  angularDist_D0m->FixParameter(D0m_MAG, 1.0);
  // ------- ------- ------- -------
  
  
  float px_piPlus; //!
  float px_piMinus; //!
  float py_piPlus; //!
  float py_piMinus; //!
  float pz_piPlus; //!
  float pz_piMinus; //!
  float px_protonA; //!
  float py_protonA; //!
  float pz_protonA; //!
  float px_protonC; //!
  float py_protonC; //!
  float pz_protonC; //!
  
  
  TF1 *solveTF1[nParams];
  solveTF1[PAR_t1] = new TF1("solveTF1_t1", this, &CepGen::t1Func, 0, 0, nParams);
  solveTF1[PAR_t2] = new TF1("solveTF1_t2", this, &CepGen::t2Func, 0, 0, nParams);
  solveTF1[PAR_phi1] = new TF1("solveTF1_phi1", this, &CepGen::phi1Func, 0, 0, nParams);
  solveTF1[PAR_phi2] = new TF1("solveTF1_phi2", this, &CepGen::phi2Func, 0, 0, nParams);
  solveTF1[PAR_m] = new TF1("solveTF1_m", this, &CepGen::mFunc, 0, 0, nParams);
  solveTF1[PAR_y] = new TF1("solveTF1_y", this, &CepGen::yFunc, 0, 0, nParams);
  
  
  //------------------------
  
  for(long int evt=0; evt<nEvts; ++evt){
    if( evt % 100000 == 0 )
      cout << ((double)evt)/nEvts * 100 << "%        \r" << flush;
    
    double m;
    do{
//       if(evt/nEvts/2){
//         m = massFunc->GetRandom(2*particleMass, 5.0);
//       } else{
//         m = mDSigmaDm->GetRandom();
        m = mGen->Uniform(2*particleMass, 2.0);
//       }
    } while(m<2*particleMass);
    double y = mGen->Uniform(-mMaxY, mMaxY);
    double t1, t2;
    do{ t1 = mGen->Exp( 1./mSlopeB ); } while( t1 < mMinT || t1 > mMaxT );
    do{ t2 = mGen->Exp( 1./mSlopeB ); } while( t2 < mMinT || t2 > mMaxT );
    double phi1 = mGen->Uniform(-PI, PI);
    double phi2 = mGen->Uniform(-PI, PI);
    
    
    const double parA = exp(2.*y);
    const double parC = -m*m/(mSqrtS*mSqrtS);
    const double sqrt_delta = sqrt(-4.*parA*parC);
    const double xi2 = sqrt_delta/(2.*parA);
    const double xi1 = xi2 * exp(2.*y);
    
    double px1 = sqrt(t1) * cos(phi1) * (1.-xi1);
    double py1 = sqrt(t1) * sin(phi1) * (1.-xi1);
    double pz1 = sqrt(pow(mBeamP*(1.-xi1),2) - px1*px1 - py1*py1);
    double px2 = sqrt(t2) * cos(phi2) * (1.-xi2);
    double py2 = sqrt(t2) * sin(phi2) * (1.-xi2);
    double pz2 = -sqrt(pow(mBeamP*(1.-xi2),2) - px2*px2 - py2*py2);
    
    //-----------------
    
    double x0[nParams];
    x0[PAR_px1] = px1;
    x0[PAR_py1] = py1;
    x0[PAR_pz1] = pz1;
    x0[PAR_px2] = px2;
    x0[PAR_py2] = py2;
    x0[PAR_pz2] = pz2;
    double x[nParams];
    
    int status;
    
    double parameterArray[nParams];
    parameterArray[PAR_t1] = t1;
    parameterArray[PAR_t2] = t2;
    parameterArray[PAR_phi1] = phi1;
    parameterArray[PAR_phi2] = phi2;
    parameterArray[PAR_m] = m;
    parameterArray[PAR_y] = y;
    
    for(int i=0; i<nParams; ++i)
      solveTF1[i]->SetParameters( parameterArray );
    
    ROOT::Math::MultiRootFinder *rootFinder = new ROOT::Math::MultiRootFinder( ROOT::Math::GSLMultiRootFinder::kDNewton );
    rootFinder->SetDefaultMaxIterations(2e6);
    rootFinder->SetDefaultTolerance(1e-3);
    
    // wrap the functions and add to root finder
    ROOT::Math::WrappedMultiTF1 *hFunctionWrapped[nParams];
    for(int i=0; i<nParams; ++i){
      hFunctionWrapped[i] = new ROOT::Math::WrappedMultiTF1( *solveTF1[i], nParams );
      rootFinder->AddFunction( *hFunctionWrapped[i] );
    }
    
    int temp = gErrorIgnoreLevel;
    gErrorIgnoreLevel = kWarning;
    rootFinder->SetPrintLevel(-1);
    rootFinder->Solve(x0);
    gErrorIgnoreLevel = temp;
    
    if( rootFinder->Status() > 0 ){
      --evt;
      continue;
    }
    
    //-------------------------
    
    TVector3 proton1( rootFinder->X()[PAR_px1], rootFinder->X()[PAR_py1], rootFinder->X()[PAR_pz1] );
    TVector3 proton2( rootFinder->X()[PAR_px2], rootFinder->X()[PAR_py2], rootFinder->X()[PAR_pz2] );
    TLorentzVector proton4Vec_1(proton1, sqrt(proton1.Mag2() + mProton*mProton));
    TLorentzVector proton4Vec_2(proton2, sqrt(proton2.Mag2() + mProton*mProton));
    
    TVector3 p0Vec_1( 0., 0., mBeamP );
    TLorentzVector p04Vec_1( p0Vec_1, sqrt(p0Vec_1.Mag2() + mProton*mProton) );
    TVector3 p0Vec_2( 0., 0., -mBeamP );
    TLorentzVector p04Vec_2( p0Vec_2, sqrt(p0Vec_2.Mag2() + mProton*mProton) );
    
    TLorentzVector pomeron4Vec_1 = p04Vec_1 - proton4Vec_1;
    TLorentzVector pomeron4Vec_2 = p04Vec_2 - proton4Vec_2;
    TVector3 boostVec = (pomeron4Vec_1 + pomeron4Vec_2).BoostVector();
    TLorentzRotation l(boostVec);
    
    double pInRestFrame = sqrt( m*m/4. - particleMass*particleMass );
    double randomSphere_px, randomSphere_py, randomSphere_pz;
    mGen->Sphere( randomSphere_px, randomSphere_py, randomSphere_pz, pInRestFrame );
    TLorentzVector piPlus4Vec( randomSphere_px, randomSphere_py, randomSphere_pz, sqrt(pInRestFrame*pInRestFrame + particleMass*particleMass));
    TLorentzVector piMinus4Vec( -randomSphere_px, -randomSphere_py, -randomSphere_pz, sqrt(pInRestFrame*pInRestFrame + particleMass*particleMass));
    
    piPlus4Vec.Transform(l);
    piMinus4Vec.Transform(l);
    
    #ifdef DEBUG
    cout << "----" << endl;
    piPlus4Vec.Print();
    piMinus4Vec.Print();
    proton4Vec_1.Print();
    proton4Vec_2.Print();
    cout << "Total px=" << (piPlus4Vec+piMinus4Vec+proton4Vec_1+proton4Vec_2).Px() << " Total py=" << (piPlus4Vec+piMinus4Vec+proton4Vec_1+proton4Vec_2).Py() << " Total pz=" << (piPlus4Vec+piMinus4Vec+proton4Vec_1+proton4Vec_2).Pz() << " Total sqrt(s)=" << sqrt((piPlus4Vec+piMinus4Vec+proton4Vec_1+proton4Vec_2).Mag2()) << endl;
    #endif
    
    TLorentzVector piPlus4Vec_D0m = piPlus4Vec;
    TLorentzVector piMinus4Vec_D0m = piMinus4Vec;
    transformCentralTrksToWave( piPlus4Vec_D0m, piMinus4Vec_D0m, proton4Vec_1, proton4Vec_2, angularDist_D0m );
    
    
    for(int i=0; i<nParams; ++i)
      if(hFunctionWrapped[i]) delete hFunctionWrapped[i];
      delete rootFinder;
    
    
    px_piPlus = piPlus4Vec.Px();
    px_piMinus = piMinus4Vec.Px();
    py_piPlus = piPlus4Vec.Py();
    py_piMinus = piMinus4Vec.Py();
    pz_piPlus = piPlus4Vec.Pz();
    pz_piMinus = piMinus4Vec.Pz();
    px_protonA = proton4Vec_1.Px();
    py_protonA = proton4Vec_1.Py(); 
    pz_protonA = proton4Vec_1.Pz();
    px_protonC = proton4Vec_2.Px();
    py_protonC = proton4Vec_2.Py();
    pz_protonC = proton4Vec_2.Pz();
    
    double ptPiPlus = piPlus4Vec.Pt();
    double ptPiMinus = piMinus4Vec.Pt();
    
    double newPtPiPlus = ptPiPlus + mGen->Gaus(0.0, ptPiPlus<0.3 ? 0.006 : (0.0024 + 0.012*ptPiPlus) );
    double newPtPiMinus = ptPiMinus + mGen->Gaus(0.0, ptPiMinus<0.3 ? 0.006 : (0.0024 + 0.012*ptPiMinus) );
    
    TVector3 newPiPlus3Vec = (newPtPiPlus/ptPiPlus) * piPlus4Vec.Vect();
    TVector3 newPiMinus3Vec = (newPtPiMinus/ptPiMinus) * piMinus4Vec.Vect();
    
    TVector3 newProton1_3Vec = proton1;
    TVector3 newProton2_3Vec = proton2;
    
    
    TVector2 v1(px_protonA, py_protonA);
    TVector2 v2(px_protonC, py_protonC);
    double deltaPhiDegrees = acos(v1.Unit()*v2.Unit()) * 180. / Util::PI;

    const bool areProtonsInFidRegion = isForwardProtonWithinFiducialRegion(proton4Vec_1.Vect(), proton4Vec_2.Vect());
    const bool areCentralTracksInFiducialRegion = isCentralTrackWithinFiducialRegion( piPlus4Vec.Vect(), piMinus4Vec.Vect() );
    const bool areCentralTracksInFiducialRegion_D0m = isCentralTrackWithinFiducialRegion( piPlus4Vec_D0m.Vect(), piMinus4Vec_D0m.Vect() );
    const bool areAllInFiducialRegion = isWithinFiducialRegion(piPlus4Vec.Vect(), piMinus4Vec.Vect(), proton4Vec_1.Vect(), proton4Vec_2.Vect());    
    
    int deltaPhiBin = -1;
    if(deltaPhiDegrees<=45.)
        deltaPhiBin = 0; else
    if(deltaPhiDegrees<=90.)
        deltaPhiBin = 1; else
    if(deltaPhiDegrees<=135.)
        deltaPhiBin = 2; else
        deltaPhiBin = 3;
    
    if( t1>0.03 && t2>0.03 && t1<0.25 && t2<0.25 && fabs(y) < 2.0 ){
//       mMassAcceptance_LimitedMandelstamT_S0m[deltaPhiBin]->Fill( areCentralTracksInFiducialRegion, m );
//       mMassAcceptance_LimitedMandelstamT_D0m[deltaPhiBin]->Fill( areCentralTracksInFiducialRegion_D0m, m );
      
//       const double deltaPhiAcceptance = mEff->deltaPhiCutEff( deltaPhiBin, t1, t2 );
//       const double massAcceptance_S0m = input_mMassAcceptance_LimitedMandelstamT_S0m[deltaPhiBin]->GetEfficiency( input_mMassAcceptance_LimitedMandelstamT_S0m[deltaPhiBin]->FindFixBin( m ) );
//       const double massAcceptance_D0m = input_mMassAcceptance_LimitedMandelstamT_D0m[deltaPhiBin]->GetEfficiency( input_mMassAcceptance_LimitedMandelstamT_D0m[deltaPhiBin]->FindFixBin( m ) );
      
      
      
      mhInvMass_True->Fill( m );
//       mhInvMass_True_DeltaPhiBins[deltaPhiBin]->Fill( m );
      
//       if( areProtonsInFidRegion && areCentralTracksInFiducialRegion ){
//         mhInvMass_Reco_S0m_Corrected->Fill( m, 1./(deltaPhiAcceptance * massAcceptance_S0m) );
//         mhInvMass_Reco_S0m_Corrected_DeltaPhiBins[deltaPhiBin]->Fill( m, 1./(deltaPhiAcceptance * massAcceptance_S0m) );
//       }
//       
//       if( areProtonsInFidRegion && areCentralTracksInFiducialRegion_D0m ){
//         mhInvMass_Reco_D0m_Corrected->Fill( m, 1./(deltaPhiAcceptance * massAcceptance_D0m) );
//         mhInvMass_Reco_D0m_Corrected_DeltaPhiBins[deltaPhiBin]->Fill( m, 1./(deltaPhiAcceptance * massAcceptance_D0m) );
//       }
    }

    
    if( fabs(y) < 2.0 ){
      mAcceptanceForDeltaPhiCuts_Vs_t1t2_TOTAL[deltaPhiBin]->Fill( t1, t2 );
      if( areProtonsInFidRegion )
        mAcceptanceForDeltaPhiCuts_Vs_t1t2_PASSED[deltaPhiBin]->Fill( t1, t2 );
      
      mAcceptanceForDeltaPhiCuts_Vs_t1t2[deltaPhiBin]->Fill( areProtonsInFidRegion, t1, t2 );
    }
    
    
    if( areProtonsInFidRegion ){
      if( deltaPhiDegrees < 45. )
        mMassAcceptance_DeltaPhiNarrowerBin1->Fill( areAllInFiducialRegion, m );
      else if( deltaPhiDegrees > 135. )
        mMassAcceptance_DeltaPhiNarrowerBin2->Fill( areAllInFiducialRegion, m );
    }
    
    
    if( areProtonsInFidRegion ){
      mMassAcceptance_ForwardProtonsInFiducialRegion_S0m->Fill( areAllInFiducialRegion, m );
    }
    
    
    bool accepted = false;
    if( isWithinFiducialRegion(newPiPlus3Vec, newPiMinus3Vec, newProton1_3Vec, newProton2_3Vec) ){
      accepted = true;
      /*
      double zVertex;
      do{
        zVertex = mGen->Gaus(0, 50);
      } while( fabs(zVertex) > 80 );
      double evtWeight = 1.0;
      
      evtWeight *= mEff->tpcRecoEff3D( Util::PLUS, PID, 0, zVertex, newPiPlus3Vec.Eta(), newPiPlus3Vec.Pt() );
      evtWeight *= mEff->tofEff3D( Util::PLUS, PID, zVertex, newPiPlus3Vec.Eta(), newPiPlus3Vec.Pt() ) + mEff->tofEff2DCorrection(PID, newPiPlus3Vec.Eta(), newPiPlus3Vec.Pt()) + mEff->tofEff2DExtraCorrection(PID, newPiPlus3Vec.Eta(), newPiPlus3Vec.Pt());
      evtWeight *= mEff->tpcRecoEff3D( Util::MINUS, PID, 0, zVertex, newPiMinus3Vec.Eta(), newPiMinus3Vec.Pt() );
      evtWeight *= mEff->tofEff3D( Util::MINUS, PID, zVertex, newPiMinus3Vec.Eta(), newPiMinus3Vec.Pt() ) + mEff->tofEff2DCorrection(PID, newPiMinus3Vec.Eta(), newPiMinus3Vec.Pt()) + mEff->tofEff2DExtraCorrection(PID, newPiMinus3Vec.Eta(), newPiMinus3Vec.Pt());
      
      int branch = (newProton2_3Vec.Py() > 0) ? Util::CU : Util::CD;
      evtWeight *= mEff->trackEffZVtxPxPy_FullRecoEfficiency( branch, zVertex, newProton2_3Vec[Util::X], newProton2_3Vec[Util::Y], true);
      evtWeight *= mEff->trackEffZVtxPxPy_DeadMaterialVetoEff( branch, zVertex, newProton2_3Vec[Util::X], newProton2_3Vec[Util::Y], true);
      
      branch = (newProton1_3Vec.Py() > 0) ? Util::AU : Util::AD;
      evtWeight *= mEff->trackEffZVtxPxPy_FullRecoEfficiency( branch, zVertex, newProton1_3Vec[Util::X], newProton1_3Vec[Util::Y], true);
      evtWeight *= mEff->trackEffZVtxPxPy_DeadMaterialVetoEff( branch, zVertex, newProton1_3Vec[Util::X], newProton1_3Vec[Util::Y], true);
      
      //       TLorentzVector newPiPlus4Vec( newPiPlus3Vec, sqrt(newPiPlus3Vec.Mag2() + particleMass*particleMass) );
      //       TLorentzVector newPiMinus4Vec( newPiMinus3Vec, sqrt(newPiMinus3Vec.Mag2() + particleMass*particleMass) );
      
      const double totalPx = (newPiPlus3Vec + newPiMinus3Vec + newProton1_3Vec + newProton2_3Vec).Px() + mGen->Gaus(-0.0014, 0.0274);
      const double totalPy = (newPiPlus3Vec + newPiMinus3Vec + newProton1_3Vec + newProton2_3Vec).Py() + mGen->Gaus(-0.0009, 0.0281);
      const double totalPt = sqrt( totalPx*totalPx + totalPy*totalPy );
      
      
      mhMissingPtPid[Util::OPPO][PID]->Fill( totalPt, evtWeight );
      mhInvMassPid[Util::OPPO][PID]->Fill( m, evtWeight );
      
//       mhPmaxPMin_PtMissAllPassed[0]->Fill( std::max(newPiPlus3Vec.Mag(), newPiMinus3Vec.Mag()), std::min(newPiPlus3Vec.Mag(), newPiMinus3Vec.Mag()) );
//       if( totalPt < 0.075 )
//         mhPmaxPMin_PtMissAllPassed[1]->Fill( std::max(newPiPlus3Vec.Mag(), newPiMinus3Vec.Mag()), std::min(newPiPlus3Vec.Mag(), newPiMinus3Vec.Mag()) );*/
      
    }
    
    if(!mLoadAccaptance)
      mMassAcceptance->Fill( accepted, m );
    
  }
  
//   TH1D *pmaxHist_all = mhPmaxPMin_PtMissAllPassed[0]->ProjectionX("pmax_all");
//   TH1D *pmaxHist_passed = mhPmaxPMin_PtMissAllPassed[1]->ProjectionX("pmax_passed");
//   
//   TH1D *pminHist_all = mhPmaxPMin_PtMissAllPassed[0]->ProjectionY("pmin_all");
//   TH1D *pminHist_passed = mhPmaxPMin_PtMissAllPassed[1]->ProjectionY("pmin_passed");
//   
//   TH2F *ptMissEffTH2 = new TH2F( *mhPmaxPMin_PtMissAllPassed[1] );
//   ptMissEffTH2->SetName("ptMissEffTH2");
//   ptMissEffTH2->Divide( mhPmaxPMin_PtMissAllPassed[0]/*, 1., "B"*/ );
//   
//   TEfficiency *ptMissEff = new TEfficiency( *mhPmaxPMin_PtMissAllPassed[1], *mhPmaxPMin_PtMissAllPassed[0] );
//   ptMissEff->SetDirectory(outputFile);
//   TEfficiency *ptMissEff_pmax = new TEfficiency( *pmaxHist_passed, *pmaxHist_all );
//   ptMissEff_pmax->SetDirectory(outputFile);
//   TEfficiency *ptMissEff_pmin = new TEfficiency( *pminHist_passed, *pminHist_all );
//   ptMissEff_pmin->SetDirectory(outputFile);

  for(int i=0; i<Util::nDeltaPhiRanges; ++i){
    TH2F *ratio = new TH2F( *mAcceptanceForDeltaPhiCuts_Vs_t1t2_PASSED[i] );
    ratio->SetName( mAcceptanceForDeltaPhiCuts_Vs_t1t2[i]->GetName() + TString("_TH2F") );
    ratio->Divide( mAcceptanceForDeltaPhiCuts_Vs_t1t2_TOTAL[i] );
  }


  outputFile->Write();
  outputFile->Close();
}

void CepGen::setupDrawingStyle() const{
  // setup the drawing style
  gStyle->SetFrameBorderMode(0);
  gStyle->SetFrameFillColor(0);
  gStyle->SetCanvasBorderMode(0);
  gStyle->SetCanvasColor(0);
  gStyle->SetPadBorderMode(0);
  gStyle->SetPadColor(0);
  gStyle->SetStatColor(0);
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
  gStyle->SetNumberContours(99);
  gStyle->SetPalette(55);
  
//   setBlackAndWhiteColorScale();
}






double CepGen::t1Func(const double *x, const double *p ) const{
  TVector3 p0Vec( 0., 0., mBeamP );
  TVector3 pVec( x[PAR_px1], x[PAR_py1], x[PAR_pz1] );
  TLorentzVector p04Vec( p0Vec, sqrt(p0Vec.Mag2() + mProton*mProton) );
  TLorentzVector p4Vec( pVec, sqrt(pVec.Mag2() + mProton*mProton) );
  double t1 = fabs((p4Vec - p04Vec).Mag2());
  return t1 - p[PAR_t1];
}

double CepGen::t2Func(const double *x, const double *p ) const{
  TVector3 p0Vec( 0., 0., -mBeamP );
  TVector3 pVec( x[PAR_px2], x[PAR_py2], x[PAR_pz2] );
  TLorentzVector p04Vec( p0Vec, sqrt(p0Vec.Mag2() + mProton*mProton) );
  TLorentzVector p4Vec( pVec, sqrt(pVec.Mag2() + mProton*mProton) );
  double t2 = fabs((p4Vec - p04Vec).Mag2());
  return t2 - p[PAR_t2];
}

double CepGen::phi1Func(const double *x, const double *p ) const{
  return atan2(x[PAR_py1], x[PAR_px1]) - p[PAR_phi1];
}

double CepGen::phi2Func(const double *x, const double *p ) const{
  return atan2(x[PAR_py2], x[PAR_px2]) - p[PAR_phi2];
}

double CepGen::mFunc(const double *x, const double *p ) const{
  TVector3 p0Vec_1( 0., 0., mBeamP );
  TVector3 pVec_1( x[PAR_px1], x[PAR_py1], x[PAR_pz1] );
  TLorentzVector p04Vec_1( p0Vec_1, sqrt(p0Vec_1.Mag2() + mProton*mProton) );
  TLorentzVector p4Vec_1( pVec_1, sqrt(pVec_1.Mag2() + mProton*mProton) );
  TLorentzVector pomeron4Vec_1 = p04Vec_1 - p4Vec_1;
  
  TVector3 p0Vec_2( 0., 0., -mBeamP );
  TVector3 pVec_2( x[PAR_px2], x[PAR_py2], x[PAR_pz2] );
  TLorentzVector p04Vec_2( p0Vec_2, sqrt(p0Vec_2.Mag2() + mProton*mProton) );
  TLorentzVector p4Vec_2( pVec_2, sqrt(pVec_2.Mag2() + mProton*mProton) );
  TLorentzVector pomeron4Vec_2 = p04Vec_2 - p4Vec_2;
  
  return (pomeron4Vec_1 + pomeron4Vec_2).M() - p[PAR_m];
}

double CepGen::yFunc(const double *x, const double *p ) const{
  TVector3 p0Vec_1( 0., 0., mBeamP );
  TVector3 pVec_1( x[PAR_px1], x[PAR_py1], x[PAR_pz1] );
  TLorentzVector p04Vec_1( p0Vec_1, sqrt(p0Vec_1.Mag2() + mProton*mProton) );
  TLorentzVector p4Vec_1( pVec_1, sqrt(pVec_1.Mag2() + mProton*mProton) );
  TLorentzVector pomeron4Vec_1 = p04Vec_1 - p4Vec_1;
  
  TVector3 p0Vec_2( 0., 0., -mBeamP );
  TVector3 pVec_2( x[PAR_px2], x[PAR_py2], x[PAR_pz2] );
  TLorentzVector p04Vec_2( p0Vec_2, sqrt(p0Vec_2.Mag2() + mProton*mProton) );
  TLorentzVector p4Vec_2( pVec_2, sqrt(pVec_2.Mag2() + mProton*mProton) );
  TLorentzVector pomeron4Vec_2 = p04Vec_2 - p4Vec_2;
  
  return (pomeron4Vec_1 + pomeron4Vec_2).Rapidity() - p[PAR_y];
}


bool CepGen::isWithinFiducialRegion(const TVector3 & piPlus, const TVector3 & piMinus, const TVector3 & protonA, const TVector3 & protonC) const{
  if( fabs(piPlus.Eta()) > 2.5 ) return false;
  if( fabs(piMinus.Eta()) > 2.5 ) return false;
  if( piPlus.Perp() < 0.1 ) return false;
  if( piMinus.Perp() < 0.1 ) return false;
  if( piMinus.Perp() < 0.2 && piPlus.Perp() < 0.2 ) return false;
  if( fabs(protonA.Py())<0.17 || fabs(protonA.Py())>0.5 ) return false;
  if( fabs(protonC.Py())<0.17 || fabs(protonC.Py())>0.5 ) return false;
  return true;
}


bool CepGen::isForwardProtonWithinFiducialRegion(const TVector3 & protonA, const TVector3 & protonC) const{
  if( fabs(protonA.Py())<0.17 || fabs(protonA.Py())>0.5 ) return false;
  if( fabs(protonC.Py())<0.17 || fabs(protonC.Py())>0.5 ) return false;
  return true;
}

bool CepGen::isCentralTrackWithinFiducialRegion(const TVector3 & piPlus, const TVector3 & piMinus) const{
  if( fabs(piPlus.Eta()) > 2.5 ) return false;
  if( fabs(piMinus.Eta()) > 2.5 ) return false;
  if( piPlus.Perp() < 0.1 ) return false;
  if( piMinus.Perp() < 0.1 ) return false;
  if( piMinus.Perp() < 0.2 && piPlus.Perp() < 0.2 ) return false;
  return true;
}

void CepGen::transformCentralTrksToWave(TLorentzVector &trackPlus, TLorentzVector &trackMinus, TLorentzVector protonA, TLorentzVector protonB, TF2 * const intensityFunc) const{
  const double cmsEnergySq = (trackPlus+trackMinus+protonA+protonB).Mag2();
  const double p0 = sqrt( cmsEnergySq/4. - mUtil->mass(Util::PROTON)*mUtil->mass(Util::PROTON) );
  TLorentzVector beamProton[Util::nSides];
  for(int side=0; side<Util::nSides; ++side)
    beamProton[side] = TLorentzVector(0, 0, (side==Util::C?-1:1)*p0/*mUtil->p0()*/, sqrt(/*mUtil->p0()*mUtil->p0()*/p0*p0 + mUtil->mass(Util::PROTON)*mUtil->mass(Util::PROTON)));
  TLorentzVector trk[Util::nSigns];
  trk[Util::PLUS] = trackPlus;
  trk[Util::MINUS] = trackMinus;
  TLorentzVector trkPair = trk[Util::PLUS] + trk[Util::MINUS];
  TVector3 trkPairBoost = trkPair.BoostVector();
  TLorentzRotation l2(trkPairBoost);
  TLorentzRotation l;
  TVector3 newX, newY, newZ;
  
  TLorentzVector pomeron[Util::nSides];
  for(int side=0; side<Util::nSides; ++side)
    pomeron[side] = beamProton[side] - (side==Util::C ? protonB : protonA);
  
  newY = pomeron[Util::A].Vect().Cross( /*pomeron[Util::C].Vect()*/trkPairBoost ).Unit();
  pomeron[Util::A].Transform(l2.Inverse());
  pomeron[Util::C].Transform(l2.Inverse());
  newZ = pomeron[Util::A].Vect().Unit();
  newX = newY.Cross( newZ ).Unit();
  
  //----
  
  TRotation r;
  r.SetZAxis(newZ, newX);
  TLorentzRotation l1(r);
  l=l2*l1;
  
  trk[Util::PLUS].Transform(l.Inverse());
  trk[Util::MINUS].Transform(l.Inverse());
  
  //-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
  TRotation r_zeroOrientation;
  r_zeroOrientation.RotateZ( trk[Util::PLUS].Phi() );
  TVector3 rotationAxis = TVector3(0,0,1).Cross( trk[Util::PLUS].Vect() ).Unit();
  double theta = trk[Util::PLUS].Theta();
  r_zeroOrientation.Rotate( theta, rotationAxis );
  trk[Util::PLUS].Transform(r_zeroOrientation.Inverse());
  trk[Util::MINUS].Transform(r_zeroOrientation.Inverse());
  //-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
  
  double randomPhi;
  double randomTheta;
  intensityFunc->GetRandom2(randomPhi, randomTheta);
  
  TRotation r_Wave;
  r_Wave.RotateZ( randomPhi );
  r_Wave.RotateX( randomTheta );
  
  trk[Util::PLUS].Transform(r_Wave.Inverse());
  trk[Util::MINUS].Transform(r_Wave.Inverse());
  
  // back to LAB frame
  trk[Util::PLUS].Transform(l);
  trk[Util::MINUS].Transform(l);
  
  trackPlus = trk[Util::PLUS];
  trackMinus = trk[Util::MINUS];
}

double CepGen::intensityFromWaves(const double *x, const double *par) const{
  const double theta = x[1];
  const double phi = x[0];
  complex<double> S0m, P0m, D0m, P1m, D1m, P1p, D1p;
  S0m = polar( par[0], 0.0 ) * ROOT::Math::sph_legendre(0,0, theta);
  P0m = polar( par[1], par[2] ) * ROOT::Math::sph_legendre(1,0, theta);
  D0m = polar( par[3], par[4] ) * ROOT::Math::sph_legendre(2,0, theta);
  P1m = polar( par[5], par[6] ) * ROOT::Math::sph_legendre(1,1, theta) * complex<double>(cos(phi), 0.0) * polar( 2.0/sqrt(2.0), 0.);
  D1m = polar( par[7], par[8] ) * ROOT::Math::sph_legendre(2,1, theta) * complex<double>(cos(phi), 0.0) * polar( 2.0/sqrt(2.0), 0.);
  P1p = polar( par[9], 0.0 ) * ROOT::Math::sph_legendre(1,1, theta) * complex<double>(0.0, sin(phi)) * polar( 2.0/sqrt(2.0), 0.);
  D1p = polar( par[10], par[11] ) * ROOT::Math::sph_legendre(2,1, theta) * complex<double>(0.0, sin(phi)) * polar( 2.0/sqrt(2.0), 0.);
  double intensity = norm(S0m + P0m + D0m + P1m + D1m) + norm(P1p + D1p);
  return intensity * sin(theta);
}
